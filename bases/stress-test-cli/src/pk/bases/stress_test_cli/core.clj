(ns pk.bases.stress-test-cli.core
  "A simple stress-testing tool for HTTP services."
  (:require
   [babashka.cli :as cli]
   [clojure.string :as s]
   [criterium.core :as bench]
   [pk.components.stress.api :as sapi]
   [pk.components.stress.core :as score]))


(defn available-cores
  "Returns number of processor cores available (including hyperthreading)."
  []
  (-> (Runtime/getRuntime)
      .availableProcessors))

(defn format-requests-opt-validation-error
  "Returns a validation error message for `requests` option."
  [requests-value]
  (format "--requests: not a positive number (%s)"
          (:value requests-value)))

(defn format-threads-opt-validation-error
  "Returns a validation error message for `threads` option."
  [threads-value]
  (format "--threads: not a positive number (%s)"
          (:value threads-value)))

(defn format-method-opt-validation-error
  "Returns a validation error message for `threads` option."
  [method-value]
  (format "--method: HTTP method invalid or not supported (%s)"
          (:value method-value)))

(defn format-url-opt-validation-error
  "Returns a validation error message for `url` option."
  [url-value]
  (format "url: invalid HTTP(S) URL (%s)"
          (:value url-value)))

(defn formatted-result-line-seq
  "Formats a result map returned by a stress-test run.
  Returns a seq of formatted strings."
  [result-map]
  (for [[status count] result-map]
    (format "%s\t%s" status count)))

(defn coerce-method-opt
  "Coerces a `method` option value to the required type and form."
  [method]
  (keyword (s/lower-case method)))

(defn coerce-url-opt
  "Coerces an `url` option value to the required type and form."
  [url]
  (str url))

(def parse-options-spec
  "Description of the supported commandline options and arguments."
  {:requests {:ref          "<number>"
              :desc         "the number of requests to make"
              :alias        :r
              :default-desc "100"
              :default      100}
   :threads  {:ref          "<number>"
              :desc         "the number of threads to use"
              :alias        :t
              :default-desc "available cores"
              :default      (available-cores)}
   :method   {:ref          "<string>"
              :desc         "HTTP method to use"
              :alias        :m
              :default-desc "get"
              :default      :get}
   :url      {:ref     "<url>"
              :desc    "the URL to use"}})

(defn format-help
  "Format a help on usage."
  []
  (format "Usage:\n%s" (cli/format-opts {:spec parse-options-spec})))

(defn parse-error-handler
  "Handles errors that occur during parsing of commandline args.
  Prints an appropriate error message and exits."
  [{:keys [spec type cause msg option] :as data}]
  (println msg)
  (println (format-help))
  (System/exit 1))

(defn show-help
  "Print a program description with a help on usage."
  []
  (println "A simple HTTP stress-test tool.")
  (println (format-help))
  (System/exit 0))

(def parse-options
  "Configuration of the commandline parser (supported options, arguments,
  rules of type coercion and validation)."
  {:alias      {:r :requests
                :t :threads
                :m :method}
   :restrict   [:requests :threads :method :url]
   :coerce     {:requests :long
                :threads  :long
                :method   coerce-method-opt
                :url      coerce-url-opt}
   :validate   {:requests {:pred   pos?
                           :ex-msg format-requests-opt-validation-error}
                :threads  {:pred   pos?
                           :ex-msg format-threads-opt-validation-error}
                :method   {:pred   score/http-method?
                           :ex-msg format-method-opt-validation-error}
                :url      {:pred   score/http-url?
                           :ex-msg format-url-opt-validation-error}}
   :args->opts [:url]
   :exec-args  {:requests 1000
                :threads  (available-cores)
                :method   :get}
   :error-fn   parse-error-handler
   :spec       parse-options-spec})



(defn main
  "Invoke HTTP stress-test CLI."
  [& cli-args]
  (when-not cli-args
    (show-help))
  (let [{:keys [requests threads method url]}
        (cli/parse-opts cli-args parse-options)]
    (println "Options:\n---")
    (printf "url: %s\nrequests: %s\nthreads: %s\nmethod: %s\n"
            url requests threads (s/upper-case (name method)))
    (flush)
    (try
      (let [[nsec _]
            (bench/time-body
             (let [responses (sapi/run url {:requests requests
                                            :threads  threads
                                            :method   method})
                   status-freqs (->> responses
                                     (map :status)
                                     frequencies)]
               (println "\nResult (status count):\n---")
               (doseq [line (formatted-result-line-seq status-freqs)]
                 (println line))))]
        (printf "\nElapsed time: %.2fs.\n" (/ nsec 1.0e9)))
      (catch Exception e (printf "Exception: %s\n" (.getMessage e))))))


(comment

  ;; happy path
  (main "-r" "10" "http://localhost:8000/whoami")
  (main "-r" "10000" "http://localhost:8000/whoami")
  (main "-r" "10000" "-m" "post" "http://localhost:8000/whoami")
  (main "-r" "1000" "-m" "post" "http://localhost:8000/api/v1/alias")

  ;; specifying number of threads
  (time (main "-r" "10000" "-t" "1" "http://localhost:8000/whoami"))
  (time (main "-r" "10000" "-t" "2" "http://localhost:8000/whoami"))
  (time (main "-r" "10000" "-t" "4" "http://localhost:8000/whoami"))
  (time (main "-r" "10000" "http://localhost:8000/whoami"))
  (time (main "-r" "10000" "-t" "32" "http://localhost:8000/whoami"))

  (time (main "-r" "10000" "-t" "1" "http://localhost:8000/api/v1/alias"))
  (time (main "-r" "10000" "-t" "2" "http://localhost:8000/api/v1/alias"))
  (time (main "-r" "10000" "-t" "4" "http://localhost:8000/api/v1/alias"))
  (time (main "-r" "10000" "http://localhost:8000/api/v1/alias"))
  (time (main "-r" "10000" "-t" "32" "http://localhost:8000/api/v1/alias"))

  ;; help
  (println (cli/format-opts {:spec parse-options-spec}))


  ;; can't pass negative numbers as options values; it is fine though
  (cli/parse-args [":r" "-1" "http://localhost:8000/whoami"] parse-options)

  (cli/parse-args [":r" "1" "-q" "2" "http://localhost:8000/whoami"]
                  parse-options)
  (cli/parse-args ["-r" "0" "http://localhost:8000/whoami"] parse-options)
  (cli/parse-args ["-r" "2" "--threads" "4" "-m" "POST"
                   "http://localhost:8000/whoami"]
                  parse-options)
  (cli/parse-args ["-r" "2" "-m" "POST"
                   "http://localhost:8000/whoami"]
                  parse-options)
  (cli/parse-args ["-r" "2" "-m" "POST"]
                  parse-options)

  (cli/parse-opts ["-r" "2" "--threads" "4" "-m" "POS"
                   "http://localhost:8000/whoami"]
                  parse-options)


  (main "-r" "-1" "http://localhost:8000/whoami")

  (main "-r" "10" "http://localhost:8000/whoami")
  (main "-n" "100" "http://localhost:8000/api/v1/alias")

  (sapi/run "http://localhost:8000/whoami" {:requests 2
                                            :threads  2
                                            :method   :get})

  (let [[ns res] (bench/time-body (do (Thread/sleep 1000) 12))]
    [(/ ns 1e9) res])
  (bench/scale-time 2)

  (format "%e" 0.00012345)

)
