(ns flow-storm
  (:require
   [flow-storm.api :as fs-api]))


;; run the debugger GUI
(fs-api/local-connect)


;; instrument and run some code
#rtrace (reduce + (map inc (range 10)))


(defn dot
  [xs ys]
  (reduce + (map * xs ys)))

#rtrace (dot [1 2 3] [4 5 6])
