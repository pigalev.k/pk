(ns ^:dev/once fixes
  "Fixes for various issues with running devcards, e.g.
  https://github.com/bhauman/devcards/issues/168#issuecomment-640220567.

  This namespace should be loaded just once in each build (not be
  hot-reloaded)."
  (:require
   ["marked" :refer (marked)]
   ["highlight.js/lib/core" :as hljs]
   ["highlight.js/lib/languages/clojure" :as clj]))


(js/goog.exportSymbol "DevcardsMarked" marked)
(js/goog.exportSymbol "DevcardsSyntaxHighlighter" hljs)
(hljs/registerLanguage "clojure", clj)
