(ns core.core
  "Entry point for default devcards build (`core`)."
  (:require
   ;; devcards, its npm deps and fixes
   [devcards.core :as dc]
   [fixes]
   ;; other namespaces where devcards that you want to include in the build are
   ;; defined
   [core.basics]
   [core.examples.bmi]
   [core.examples.uix])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defn init
  "Initialize devcards."
  []
  (dc/start-devcard-ui!))


(defcard "# Core

A default [devcards](https://github.com/bhauman/devcards) build. Usage basics,
examples, etc.")
