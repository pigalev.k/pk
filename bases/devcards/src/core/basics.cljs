(ns core.basics
  "Devcards on the basics of using devcards. Do we need to go deeper?"
  (:require
   [devcards.core :as dc]
   [sablono.core :as sab])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Basic capabilities
          Displaying different objects with `defcard`.")

(defcard
  "## `defcard` takes 5 arguments

   - **name**: an optional symbol name for the card to be used as a heading and to locate it in the Devcards interface
   - **documentation**: an optional string literal of markdown documentation
   - **main object**: a required object for the card to display
   - **initial data**: an optional Atom, RAtom or Clojure data structure (normally a Map), used to initialize the a state that the devcard will pass back to your code examples
   - **devcard options**: an optional map of options for the underlying devcard
")

(defcard "## Devcard options")

(defcard
  "```
    {:frame true         ;; whether to enclose the card in a padded frame
     :heading true       ;; whether to add a heading panel to the card
     :padding true       ;; whether to have padding around the body of the card
     :hidden false       ;; whether to diplay the card or not
     :inspect-data false ;; whether to display the data in the card atom
     :watch-atom true    ;; whether to watch the atom and render on change
     :history false      ;; whether to record a change history of the atom
     :classname \"\"       ;; provide card with a custom classname
     :projection identity};; provide a projection function for card state
```")


(defcard "## Displaying markdown")

(defcard markdown-card
  (sab/html [:h1 "Devcards is freaking awesome!!"]))

(defcard markdown-code-card
  "```
(defn foo [x y z]
  \"Returns the product of x y and z.\"
  (* x y z))
```")


(defcard  "## Displaying data")

(defcard {:this "is a map"})

(defcard ["This" "is" "a" "vector"])

(defcard (list 1 2 3))

(defcard #js {:beetle "juice"})


(defcard "## Displaying atoms")

(defonce observed-atom
  (let [a (atom 0)]
    (js/setInterval (fn [] (swap! observed-atom inc)) 1000)
   a))

(defcard atom-observing-card observed-atom)


(defcard "## Displaying functions (that return React elements)")

(defcard
  (fn [data-atom owner]
    (sab/html
      [:div
       [:h3 "Example Counter w/Initial Data: " (:count @data-atom)]
       [:button
        {:onClick (fn [] (swap! data-atom update-in [:count] inc))}
        "inc"]]))
  {:count 50})


(defcard "## Using shared state")

(defonce first-example-state (atom {:count 55}))

(defcard
  example-counter
  (fn [data-atom owner]
    (sab/html
      [:h3
       "Example Counter w/Shared Initial Atom: "
       (:count @data-atom)]))
  first-example-state)

(defcard
  example-incrementer
  (fn [data-atom owner]
    (sab/html
      [:button
       {:onClick (fn [] (swap! data-atom update-in [:count] inc))}
       "increment"]))
  first-example-state)

(defcard
  example-decrementer
  (fn [data-atom owner]
    (sab/html
      [:button
       {:onClick (fn [] (swap! data-atom update-in [:count] dec))}
       "decrement"]))
  first-example-state)


(defcard "## Accessing the DOM with `dom-node`")

(defcard example-dom-node
  (dc/dom-node
    (fn [data-atom node]
      (set! (.-innerHTML node) "<h2>Example Dom Node</h2>"))))
