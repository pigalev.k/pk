(ns core.examples.uix
  "UIx v2 components."
  (:require
   [devcards.core :as dc]
   [uix.core :refer [defui $]]
   #_[uix.dom])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defui button [{:keys [on-click children]}]
  ($ :button.btn {:on-click on-click}
    children))

(defui app [{:keys [state data-atom] :as props}]
  (let [[counter set-counter!] (uix.core/use-state state)]
    ($ :<>
       #_($ button {:on-click #(set-counter! dec)} "-")
       ($ button {:on-click #(reset! data-atom {:state (dec state)})} "-")
       ($ :span state)
       #_($ button {:on-click #(set-counter! inc)} "+")
       ($ button {:on-click #(reset! data-atom {:state (inc state)})} "+"))))

#_(defonce root
  (uix.dom/create-root (js/document.getElementById "root")))

#_(uix.dom/render-root ($ app) root)

(defcard
  "# UIx v2 components

   Questions:
   - state management?")

(defcard my-first-card
  "An UIx component."
  (fn [data-atom _]
    (let [props @data-atom]
      ($ app (assoc props :data-atom data-atom))))
  {:state 180}
  {:inspect-data true
   :frame true
   :history true})
