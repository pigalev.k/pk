(ns mui.theming.basics
  (:require
   [devcards.core :as dc]
   [reagent.core :as r]
   [reagent-mui.colors :refer [pink indigo]]
   [reagent-mui.material.button :refer [button]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.styles :refer [theme-provider create-theme]])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Theming

Themes allow to achieve consistency and ease of change in the look and feel of
an application or across applications.

Light (default) and dark theme types are available to choose from.

Documentation:
- [Theming](https://mui.com/material-ui/customization/theming/)

Component APIs:
- [ThemeProvider](https://mui.com/material-ui/customization/theming/#themeprovider), [createTheme](https://mui.com/material-ui/customization/theming/#createtheme-options-args-theme): `[reagent-mui.styles :refer [theme-provider* theme-provider create-theme]]` ")

(defcard "## Theme

A theme is a collection (a map) of configuration variables. There are standard
keys in this map, such as `:palette`, `:typography`, `:spacing`, `:breakpoints`,
`:transitions`, `:components` and `:z-index`. Arbitrary custom keys may also be
added (except `vars`, which is reserved for CSS variables support).")

(defcard "## Constructing a theme

A new theme can be created using `create-theme` function, that accepts a map
with a partial theme configuration, overrides values in the default theme with
values specified in the partial configuration and returns the result.
```
(create-theme {:palette {:primary   pink
                         :secondary indigo}})
```
")

(defcard "## Applying a theme

To apply a theme you need to use the `theme-provider*` component in order to
inject the theme into its children components. However, this is optional; MUI
components come with a default theme.

Theme can be applied to the entire app or certain subtree of components;
different themes can be applied to different subtrees.

Themes can be nested using multiple theme providers. The inner theme will
override the outer theme.

Theme provider's `:theme` prop accepts a theme or a function returning a theme,
that can be used to extend or modify an existing theme at runtime.

There is also a `theme-provider` helper function that
- takes a theme as its first argument instead of props map
- transforms its other arguments (children) into React elements
- returns a theme provider component.

```
(let [theme (create-theme {:palette {:primary   pink
                                        :secondary indigo}})]
     [theme-provider theme
      [stack {:direction :row :spacing 2}
       [button {:variant :contained} \"Primary\"]
       [button {:variant :contained
                :color   :secondary} \"Secondary\"]
       [button {:variant :contained
                :color   :success} \"Success\"]]])
```"
  (r/as-element
   (let [theme (create-theme {:palette {:primary   pink
                                        :secondary indigo}})]
     [theme-provider theme
      [stack {:direction :row :spacing 2}
       [button {:variant :contained} "Primary"]
       [button {:variant :contained
                :color   :secondary} "Secondary"]
       [button {:variant :contained
                :color   :success} "Success"]]])))

(defcard "## Example theme

The default theme, produced with `(create-theme {})`:"
  (into (sorted-map) (create-theme {})))
