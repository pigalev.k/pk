(ns mui.theming.palette
  (:require
   [devcards.core :as dc]
   [reagent.core :as r]
   [reagent-mui.colors :refer [pink indigo]]
   [reagent-mui.material.button :refer [button]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.styles :refer [theme-provider create-theme]])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Palette

The palette enables you to modify the color of the components to suit your
brand.

A **palette** is a collection of **palette colors**.

Documentation:
- [Palette](https://mui.com/material-ui/customization/palette/)")

(defcard "## Palette Colors, Color sets and Colors

>Note: this terminology may differ from that used in official MUI documentation,
 for clarity's sake.

A **color** is a string value representing a color (e.g., color name or
hexadecimal string like `green` or `#aabb57`.)

A **color set** is a **hue** (color set name) associated with a collection
of **shades** (map from shade names, numbers or keywords, to hexadecimal color
strings).

For example, `red` color set looks like
this:

```
(def red {50    \"#ffebee\"
          100   \"#ffcdd2\"
          200   \"#ef9a9a\"
          300   \"#e57373\"
          400   \"#ef5350\"
          500   \"#f44336\"
          600   \"#e53935\"
          700   \"#d32f2f\"
          800   \"#c62828\"
          900   \"#b71c1c\"
          :A100 \"#ff8a80\"
          :A200 \"#ff5252\"
          :A400 \"#ff1744\"
          :A700 \"#d50000\"})
```

A **palette color** can either be a color set or a map of the following shape:

```
{:main <color string>
 :light <color string>
 :dark <color string>
 :contrast-text <color string>}
```

Only `:main` key is required, others are optional and will be computed if
absent.

A single **color** can be obtained
- from the color set: by hue (color set name), such as `red`, and shade name, such as `500`. So `(red 50)` is the lightest shade of red, while `(red 900)` is the darkest. In addition, most hues come with **accent** shades, prefixed with an A (e.g. `(red :A100)`).
- from the palette color map: as a value of one of it's keys, e.g. `(get-in (create-theme {}) [:palette :primary :main])`:"
  (get-in (create-theme {}) [:palette :primary :main]))

(defcard "## Theme palette colors

The theme exposes the following standard palette colors (accessible under
`:palette` key of a theme):

- `:primary`: used to represent primary interface elements for a user. It's the color displayed most frequently across your app's screens and components.
- `:secondary`: used to represent secondary interface elements for a user. It provides more ways to accent and distinguish your product. Having it is optional.
- `:error`: used to represent interface elements that the user should be made aware of.
- `:warning`: used to represent potentially dangerous actions or important messages.
- `:info`: used to present information to the user that is neutral and not necessarily important.
- `:success`: used to indicate the successful completion of an action that user triggered."
  (r/as-element
   [stack {:direction :row :spacing 2}
    [button {:variant :contained} "Primary"]
    [button {:variant :contained
             :color   :secondary} "Secondary"]
    [button {:variant :contained
             :color   :error} "Error"]
    [button {:variant :contained
             :color   :warning} "Warning"]
    [button {:variant :contained
             :color   :info} "Info"]
    [button {:variant :contained
             :color   :success} "Success"]]))

(defcard "## Customization

You may override the default theme palette colors by including a (partial)
palette map as part of your theme. If any of the palette color values are
provided, they will replace the default ones.")

(defcard "### Using color sets

The simplest way to customize palette colors is to import one or more of the
provided color sets and use them as palette colors:

```
(let [theme (create-theme {:palette {:primary   pink
                                     :secondary indigo}})]
  [theme-provider theme
   [stack {:direction :row :spacing 2}
    [button {:variant :contained} \"Primary\"]
    [button {:variant :contained
             :color   :secondary} \"Secondary\"]
    [button {:variant :contained
             :color   :success} \"Success\"]]])
```
"
(r/as-element
 (let [theme (create-theme {:palette {:primary   pink
                                      :secondary indigo}})]
   [theme-provider theme
    [stack {:direction :row :spacing 2}
     [button {:variant :contained} "Primary"]
     [button {:variant :contained
              :color   :secondary} "Secondary"]
     [button {:variant :contained
              :color   :success} "Success"]]])))

(defcard "### Using color maps

>Note: created theme's palette seems not to be affected by `:tonal-offset` and
 `:contrast-threshold` keys of partial palette, they remain at default values

```
(let [theme (create-theme {:palette
                            {:primary {:main \"#aabbcc\"}
                             :secondary {:dark \"#002233\"
                                         :main \"#5544ff\"
                                         :contrast-text \"#aaaaaa\"}}
                            :tonal-offset 0.5
                            :contrast-threshold 4.5})]
   [theme-provider theme
    [stack {:direction :row :spacing 2}
     [button {:variant :contained} \"Primary\"]
     [button {:variant :contained
              :color   :secondary} \"Secondary\"]
     [button {:variant :contained
              :color   :success} \"Success\"]]])
```
"
(r/as-element
 (let [theme (create-theme {:palette
                            {:primary {:main "#aabbcc"}
                             :secondary {:dark "#002233"
                                         :main "#5544ff"
                                         :contrast-text "#aaaaaa"}}
                            :tonal-offset 0.5
                            :contrast-threshold 4.5})]
   [theme-provider theme
    [stack {:direction :row :spacing 2}
     [button {:variant :contained} "Primary"]
     [button {:variant :contained
              :color   :secondary} "Secondary"]
     [button {:variant :contained
              :color   :success} "Success"]]])))

(defcard "As in the example above, if the palette color contains custom colors
using any of the `:main`, `:light`, `:dark` or `:contrast-text` keys, these map
as follows:

- If the `:dark` and/or `:light` keys are omitted, their value(s) will be
calculated from `:main`, according to the `:tonal-offset` value.
- If `:contrast-text` is omitted, its value will be calculated to contrast with `:main`, according to the `:contrast-threshold` value.  Both the `:tonal-offset` and `:contrast-threshold` values may be customized as needed.

The `:tonal-offset` value can either be a number between 0 and 1, which will
apply to both light and dark variants, or an map with values for `:light` and
`:dark` variants. A higher value for `:tonal-offset` will make calculated values
for `:light` lighter, and for `:dark` darker.

A higher value for `:contrast-threshold` increases the point at which a
background color is considered light, and given a dark `:contrast-text`. Note
that `:contrast-threshold` follows a non-linear curve, and starts with a value of
3 (requiring a minimum contrast ratio of 3:1).")
