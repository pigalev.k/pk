(ns mui.theming.dark-mode
  (:require
   [devcards.core :as dc]
   [reagent.core :as r]
   [reagent-mui.colors :refer [indigo amber deep-orange grey]]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.button :refer [button]]
   [reagent-mui.material.paper :refer [paper]]
   [reagent-mui.material.scoped-css-baseline :refer [scoped-css-baseline]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.material.typography :refer [typography]]
   [reagent-mui.styles :refer [theme-provider create-theme]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard "# Dark mode

Material UI comes with two palette modes: light (the default) and dark.

Documentation:
- [Dark mode](https://mui.com/material-ui/customization/dark-mode/)

Component APIs:
- [CssBaseline](https://mui.com/material-ui/api/css-baseline/), [ScopedCssBaseline](https://mui.com/material-ui/api/scoped-css-baseline/): `[reagent-mui.material.css-baseline :refer [css-baseline]]`
`[reagent-mui.material.scoped-css-baseline :refer [scoped-css-baseline]]`")

(defcard "## Dark mode by default

You can make your application use the dark theme as the default --- regardless
of the user's preference --- by adding `:mode :dark` to the `create-theme`
parameter:

```
(let [dark-theme (create-theme {:palette {:mode :dark}})]
  ...
  (r/as-element
    [theme-provider dark-theme
     [scoped-css-baseline
       [stack {:sx {:height 100}
               :direction :row
               :spacing 2
               :align-items :center
               :justify-content :center}
        [typography {:variant :subtitle2} \"Dark\"]
        [button {:variant :contained} \"Primary\"]
        [button {:variant :contained
                 :color   :secondary} \"Secondary\"]
        [button {:variant :contained
                 :color   :warning} \"Warning\"]
        [button {:variant :contained
               :color   :error} \"Error\"]
        [button {:variant :contained
                 :color   :success} \"Success\"]]]]))
```

Adding `css-baseline` (`scoped-css-baseline`) inside of the `theme-provider`
component will also enable dark mode for the app's (component subtree's)
background.

>Note: setting the dark mode this way only works if you are using the default
 palette. If you have a custom palette, make sure that you have the correct
 values based on the mode."
  (let [dark-theme (create-theme {:palette {:mode :dark}})]
    (r/as-element
     [stack {:direction :column
             :spacing 2
             :align-items :stretch
             :justify-content :center}
      [scoped-css-baseline
       [stack {:sx {:height 100}
               :direction :row
               :spacing 2
               :align-items :center
               :justify-content :center}
        [typography {:variant :subtitle2} "Light"]
        [button {:variant :contained} "Primary"]
        [button {:variant :contained
                 :color   :secondary} "Secondary"]
        [button {:variant :contained
                 :color   :warning} "Warning"]
        [button {:variant :contained
                 :color   :error} "Error"]
        [button {:variant :contained
                 :color   :success} "Success"]]]
      [theme-provider dark-theme
       [scoped-css-baseline
        [stack {:sx {:height 100}
                :direction :row
                :spacing 2
                :align-items :center
                :justify-content :center}
         [typography {:variant :subtitle2} "Dark"]
         [button {:variant :contained} "Primary"]
         [button {:variant :contained
                  :color   :secondary} "Secondary"]
         [button {:variant :contained
                  :color   :warning} "Warning"]
         [button {:variant :contained
                  :color   :error} "Error"]
         [button {:variant :contained
                  :color   :success} "Success"]]]]])))

(defcard "## Dark mode with a custom palette

To use custom palettes for light and dark modes, you can create different themes
for dark and light (and possibly other) modes.

```
(let [mode      (:mode @data-atom)
      modes     {\"light\" \"dark\"
                 \"dark\"  \"light\"}
      next-mode (fn [mode] (modes mode))
      on-click  (fn [mode] (swap! data-atom update-in [:mode] next-mode))]
  (r/as-element
   [theme-provider (if (= \"dark\" mode)
                     dark-custom-theme
                     light-custom-theme)
    [box {:sx       {:display         :flex
                     :width           :auto
                     :align-items     :center
                     :justify-content :center
                     :bgcolor         \"background.default\"
                     :color           \"text.primary\"
                     :border-radius   1
                     :p               3}
                     :on-click on-click}
     [paper {:sx {:p 1}}
      \"This is a \" mode \" mode theme with custom palette\"]]]))
```
"
  (let [light-custom-theme
        (create-theme
         {:palette {:primary amber
                    :divider (amber 200)
                    :background {:default (indigo 100)}
                    :text    {:primary   (grey 700)
                              :secondary (grey 800)}}})
        dark-custom-theme
        (create-theme
         {:palette {:primary    deep-orange
                    :divider    (deep-orange 700)
                    :background {:default (deep-orange 700)
                                 :paper   (deep-orange 900)}
                    :text       {:primary   "#fff"
                                 :secondary (grey 500)}}})]
    (fn [data-atom owner]
      (let [mode      (:mode @data-atom)
            modes     {"light" "dark"
                       "dark"  "light"}
            next-mode (fn [mode] (modes mode))
            on-click  (fn [] (swap! data-atom update-in [:mode] next-mode))]
        (r/as-element
         [theme-provider (if (= "dark" mode)
                           dark-custom-theme
                           light-custom-theme)
          [box {:sx       {:display         :flex
                           :width           :auto
                           :align-items     :center
                           :justify-content :center
                           :bgcolor         "background.default"
                           :color           "text.primary"
                           :border-radius   1
                           :p               3}
                :on-click on-click}

           [paper {:sx {:p 1}}
            "This is a " mode " mode theme with custom palette"]]]))))
  {:mode "dark"}
  {:inspect-data true})

(defcard "## System preference

Users might have a preference for light or dark mode that they've set in their
OS or browser.

```
(let [prefers-dark? (.-matches (js/matchMedia \"(prefers-color-scheme: dark)\"))
      theme (create-theme (if prefers-dark? {:palette {:mode :dark}} {}))]
  (r/as-element
   [stack {:direction :column
           :spacing 2
           :align-items :stretch
           :justify-content :center}
    [theme-provider theme
     [scoped-css-baseline
      [stack {:sx {:height 100}
              :direction :row
              :spacing 2
              :align-items :center
              :justify-content :center}
       [typography {:variant :subtitle2}
        (str \"User prefers dark? \" (if prefers-dark? \"yes\" \"no\"))]
       [button {:variant :contained} \"Primary\"]
       [button {:variant :contained
                :color   :secondary} \"Secondary\"]
       [button {:variant :contained
                :color   :warning} \"Warning\"]
       [button {:variant :contained
                :color   :error} \"Error\"]
       [button {:variant :contained
                :color   :success} \"Success\"]]]]]))
```

>Note: dark mode preference (either OS- or browser-wide) may not work in Chrome
 on Linux."
  (let [prefers-dark? (.-matches (js/matchMedia "(prefers-color-scheme: dark)"))
        theme (create-theme (if prefers-dark? {:palette {:mode :dark}} {}))]
    (r/as-element
     [stack {:direction :column
             :spacing 2
             :align-items :stretch
             :justify-content :center}
      [theme-provider theme
       [scoped-css-baseline
        [stack {:sx {:height 100}
                :direction :row
                :spacing 2
                :align-items :center
                :justify-content :center}
         [typography {:variant :subtitle2}
          (str "User prefers dark? " (if prefers-dark? "yes" "no"))]
         [button {:variant :contained} "Primary"]
         [button {:variant :contained
                  :color   :secondary} "Secondary"]
         [button {:variant :contained
                  :color   :warning} "Warning"]
         [button {:variant :contained
                  :color   :error} "Error"]
         [button {:variant :contained
                  :color   :success} "Success"]]]]])))
