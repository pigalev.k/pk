(ns mui.common
  "Common functions and data for all MUI devcards."
  (:require
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.paper :refer [paper]]
   [reagent-mui.styles :refer [styled]]))


(defn dark-mode?
  "Given a theme, returns true if a dark mode is selected."
  [theme]
  (= "dark" (get-in theme [:palette :mode])))

(def example
  "An area to place component examples in."
  (styled box (fn [{:keys [theme]}]
                (let [spacing (get-in theme [:spacing])]
                  {:background-color "#e7ebf0"
                   :border-radius    (spacing 1)
                   :padding          (spacing 2)}))))

(def item
  "A rounded rectangle that represents layout (stack, grid, etc.) items in
  examples."
  (styled paper (fn [{:keys [theme]}]
                  (let [dark?      (dark-mode? theme)
                        spacing    (get-in theme [:spacing])
                        text-attrs (get-in theme [:typography :body2])
                        text-color (get-in theme [:palette
                                                  :text
                                                  :secondary])]
                    (merge text-attrs
                           {:background-color (if dark? "#1a2027"
                                                  "#fff")
                            :padding          (spacing 1)
                            :text-align       :center
                            :color            text-color})))))
