(ns mui.core
  "Entry point for `mui` devcards build."
  (:require
   ;; devcards, its npm deps and fixes
   [devcards.core :as dc]
   [fixes]
   ;; other namespaces where devcards that you want to include in the build are
   ;; defined
   [mui.components.display.datagrid]
   [mui.components.input.fab]
   [mui.components.input.form]
   [mui.components.input.autocomplete]
   [mui.components.input.button]
   [mui.components.input.button-group]
   [mui.components.input.checkbox]
   [mui.components.input.select]
   [mui.components.layout.box]
   [mui.components.layout.grid]
   [mui.components.layout.container]
   [mui.components.layout.stack]
   [mui.components.layout.image-list]
   [mui.components.navigation.drawer]
   [mui.components.surfaces.app-bar]
   [mui.components.surfaces.paper]
   [mui.components.utils.css-baseline]
   [mui.components.utils.modal]
   [mui.theming.basics]
   [mui.theming.dark-mode]
   [mui.theming.palette])
  (:require-macros
   [devcards.core :refer [defcard]]))


(set! *warn-on-infer* true)


(defn init
  "Initialize devcards."
  []
  (dc/start-devcard-ui!))


(defcard "# Material UI (MUI) v5

[Material UI](https://mui.com/) is a library of React UI components that
implements Google's [Material Design](https://m2.material.io/).

## Material Design Guidelines

- [Material 2](https://m2.material.io/) (supported by MUI v5)

- [Material 3](https://m3.material.io/) (support planned for MUI v6)

## ClojureScript integration

Provided by [reagent-material-ui](https://github.com/arttuka/reagent-material-ui) wrapper library.

## MUI

### Documentation

Core

- [Material UI](https://mui.com/material-ui/getting-started/overview/): a
  comprehensive collection of prebuilt Material Design components that are ready
  for use in production right out of the box.

- [MUI Base](https://mui.com/base/getting-started/overview/): a collection of
  headless (unstyled) React UI components and low-level hooks.

- [MUI System](https://mui.com/system/getting-started/overview/): a collection
  of CSS utilities for rapidly laying out custom designs with MUI component
  libraries.

- [Joy UI](https://mui.com/joy-ui/getting-started/overview/): a collection of
  components that don't adhere to Material Design specifications.

MUI X

- [MUI X](https://mui.com/x/introduction/): a collection of advanced React UI
  components for complex use cases. It is **open core** - base components are
  MIT-licensed, while more advanced features require a Pro or Premium commercial
  license.

### Components

- [Core](https://mui.com/core/)

- [Advanced](https://mui.com/x/)

- [All](https://mui.com/material-ui/)

### Customization

- [MUI Treasury](https://mui-treasury.com/)")
