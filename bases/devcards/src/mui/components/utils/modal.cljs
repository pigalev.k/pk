(ns mui.components.utils.modal
  (:require
   [devcards.core :as dc]
   [mui.common :refer [example]]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.button :refer [button]]
   [reagent-mui.material.fade :refer [fade]]
   [reagent-mui.material.modal :refer [modal]]
   [reagent-mui.material.typography :refer [typography]]
   [reagent-mui.styles :refer [styled]]
   [reagent.core :as r])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Modal

Provides a foundation for creating dialogs, drawers, menus, popovers,
lightboxes, or other modal UI elements (that block interaction with the rest of
the application).

The component renders its child node in front of a backdrop component. Features:

- Manages modal stacking when one-at-a-time just isn't enough.

- Creates a backdrop, for disabling interaction below the modal.

- Disables scrolling of the page content while open.

- Properly manages focus --- moving it to the modal content, and keeping it
  there until the modal is closed.

- Adds the appropriate ARIA roles automatically.

Documentation:
- [Modal](https://mui.com/material-ui/react-modal/)

Component APIs:
- [Modal](https://mui.com/material-ui/api/modal/): `[reagent-mui.material.modal :refer [modal]]`")

(def a-modal-box
  "A simple surface with text to use in modal examples."
  (styled box (fn [{:keys [theme]}]
                (let [bg-color (get-in theme [:palette :background :paper])
                      spacing  (get-in theme [:spacing])
                      shadow   (nth (get-in theme [:shadows]) 24)]
                  {:position         :absolute
                   :top              "50%"
                   :left             "50%"
                   :transform        "translate(-50%, -50%)"
                   :width            400
                   :background-color bg-color
                   :border           "2px solid #000"
                   :box-shadow       shadow
                   :padding          (spacing 4)}))))

(defcard "## Basic modal"
  (fn [data-atom _]
    (let [open?       (:open @data-atom)
          open-modal  #(swap! data-atom assoc :open true)
          close-modal #(swap! data-atom assoc :open false)]
      (r/as-element
       [example
        [:div
         [button {:on-click open-modal} "Open modal"]
         [modal {:open             open?
                 :on-close         close-modal
                 :aria-labelledby  "modal-modal-title"
                 :aria-describedby "modal-modal-description"}
          [a-modal-box
           [typography {:id        "modal-title"
                        :variant   :h6
                        :component :h2}
            "Text in a modal"]
           [typography {:id "modal-description"
                        :sx {:mt 2}}
            "Duis mollis, est non commodo luctus, "
            "nisi erat porttitor ligula."]]]]])))
  {:open false}
  {:inspect-data true})

(defcard "## Nested modal

Modals can be nested, for example a select within a dialog, but stacking of more
than two modals, or any two modals with a backdrop is discouraged."
  (fn [data-atom _]
    (let [parent-open? (get-in @data-atom [:parent :open])
          child-open?  (get-in @data-atom [:child :open])
          open-parent  #(swap! data-atom assoc-in [:parent :open] true)
          close-parent #(swap! data-atom assoc-in [:parent :open] false)
          open-child   #(swap! data-atom assoc-in [:child :open] true)
          close-child  #(swap! data-atom assoc-in [:child :open] false)
          child-modal  (fn child-modal
                         [{:keys [open on-close]}]
                         [:div
                          [button {:sx       {:mt 2}
                                   :on-click open-child} "Open child"]
                          [modal {:open             open
                                  :on-close         on-close
                                  :aria-labelledby  "child-modal-title"
                                  :aria-describedby "child-modal-description"}
                           [a-modal-box {:sx {:width 200}}
                            [typography {:id        "child-modal-box-title"
                                         :variant   :h6
                                         :component :h2}
                             "Text in a modal"]
                            [typography {:id "child-modal-description"
                                         :sx {:mt 2}}
                             "Lorem ipsum, dolor sit amet consectetur "
                             "adipisicing elit."]
                            [button {:on-click on-close
                                     :sx       {:mt 2}} "Close child"]]]])]
      (r/as-element
       [example
        [:div
         [button {:on-click open-parent} "Open parent"]
         [modal {:open             parent-open?
                 :on-close         close-parent
                 :aria-labelledby  "parent-modal-title"
                 :aria-describedby "parent-modal-description"}
          [a-modal-box {:sx {:width 400}}
           [typography {:id        "parent-modal-title"
                        :variant   :h6
                        :component :h2}
            "Text in a modal"]
           [typography {:id "parent-modal-description"
                        :sx {:mt 2}}
            "Duis mollis, est non commodo luctus, "
            "nisi erat porttitor ligula."]
           [child-modal {:open     child-open?
                         :on-close close-child}]]]]])))
  {:parent {:open false}
   :child  {:open false}}
  {:inspect-data true})

(defcard "## Transitions

The open/close state of the modal can be animated with a transition
component. This component should respect the following conditions:

- Be a direct child descendent of the modal.

- Have an `:in` prop. This corresponds to the open/close state.

- Call the `:on-enter` callback prop when the enter transition starts.

- Call the `:on-exited` callback prop when the exit transition is
  completed. These two callbacks allow the modal to unmount the child content
  when closed and fully transitioned.

Modal has support
for [react-transition-group](https://github.com/reactjs/react-transition-group). Alternatively,
you can use [react-spring](https://github.com/pmndrs/react-spring)."
  (fn [data-atom _]
    (let [open?       (:open @data-atom)
          open-modal  #(swap! data-atom assoc :open true)
          close-modal #(swap! data-atom assoc :open false)]
      (r/as-element
       [example
        [:div
         [button {:on-click open-modal} "Open modal"]
         [modal {:open                   open?
                 :on-close               close-modal
                 :close-after-transition true
                 :slot-props             {:backdrop {:timeout 300}}
                 :aria-labelledby        "modal-modal-title"
                 :aria-describedby       "modal-modal-description"}
          [fade {:in open?}
           [a-modal-box
            [typography {:id        "modal-title"
                         :variant   :h6
                         :component :h2}
             "Text in a modal"]
            [typography {:id "modal-description"
                         :sx {:mt 2}}
             "Duis mollis, est non commodo luctus, "
             "nisi erat porttitor ligula."]]]]]])))
  {:open false}
  {:inspect-data true})

(defcard "## Performance

The content of modal is unmounted when closed. If you need to make the content
available to search engines or render expensive component trees inside your
modal while optimizing for interaction responsiveness it might be a good idea to
change this default behavior by enabling the `:keep-mounted` prop.

```
[modal {:keep-mounted true} ...]
```
")

(defcard "## Server-side modal

React doesn't support the `createPortal()` API on the server. In order to
display the modal, you need to disable the portal feature with the
`:disable-portal` prop.")

(defcard "## Limitations

### Focus trap

The modal moves the focus back to the body of the component if the focus tries
to escape it.

This is done for accessibility purposes. However, it might create issues. In the
event the users need to interact with another part of the page, e.g. with a
chatbot window, you can disable the behavior:

```
[modal {:disable-enforce-focus true} ...]
```
")

(defcard "## Unstyled

MUI Base provides a headless (unstyled) version of this React Modal
component. Try it if you need more flexibility in customization and a smaller
bundle size.")
