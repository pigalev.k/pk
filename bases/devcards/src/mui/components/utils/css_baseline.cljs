(ns mui.components.utils.css-baseline
  (:require
   [devcards.core :as dc])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard "# CSS Baseline

Helps to kickstart an elegant, consistent, and simple baseline to build upon.

Documentation:
- [CssBaseline](https://mui.com/material-ui/react-css-baseline/)

Component APIs:
- [CssBaseline](https://mui.com/material-ui/api/css-baseline/): `[reagent-mui.material.css-baseline :refer [css-baseline]]`
- [ScopedCssBaseline](https://mui.com/material-ui/api/scoped-css-baseline/): `[reagent-mui.material.scoped-css-baseline :refer [scoped-css-baseline]]`")

(defcard "## Global reset

You might be familiar
with [normalize.css](https://github.com/necolas/normalize.css), a collection of
HTML element and attribute style-normalizations.

```
[css-baseline]
[app ...]
```
")

(defcard "## Scoping on children

However, you might be progressively migrating a website to MUI, using a global
reset might not be an option. It's possible to apply the baseline only to the
children by using the `scoped-css-baseline` component.

```
[existing-app
 ...
 [scoped-css-baseline
  [new-app ...]]]
```
")

(defcard "## Approach

### Page

The `<html>` and `<body>` elements are updated to provide these page-wide
defaults:

- The margin is removed.

- The default Material Design background color is applied. It's using `(get-in
  theme [:palette :background :default])` for standard devices and a white
  background for print devices.

- If `:enable-color-scheme` is provided to `css-baseline`, native components
  colors will be set by applying [color-scheme](https://web.dev/color-scheme/)
  CSS property on `<html>`. The value used is provided by the theme
  property `(get-in theme [:palette :mode.])`")

(defcard "### Layout

- `:box-sizing` is set globally on the `<html>` element to `:border-box`. Every
  element—including `*::before` and `*::after` are declared to inherit this
  property, which ensures that the declared width of the element is never
  exceeded due to padding or border.")

(defcard "### Color scheme

The `color-scheme` CSS property can be used for switching between `:light` and
`:dark` modes of native components such as scrollbar. To use it, you can set
`:enable-color-scheme`:

```
[css-baseline {:enable-color-scheme true}]
[app ...]
```
or

```
[scoped-css-baseline {:enable-color-scheme true}
 [app ...]]
```
")

(defcard "### Typography

- No base `:font-size` is declared on the `<html>`, but `16px` is assumed (the
  browser default).

- Set the `(get-in theme [:typography :body-1])` style on the `<body>` element.

- Set the `:font-weight` to `(get-in theme [:typography :font-weight-bold])` for
  the `<b>` and `<strong>` elements.

- Custom font-smoothing is enabled for better display of the Roboto font.")
