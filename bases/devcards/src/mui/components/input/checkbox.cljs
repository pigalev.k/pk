(ns mui.components.input.checkbox
  (:require
   [devcards.core :as dc]
   [reagent.core :as r]
   [reagent-mui.colors :refer [pink]]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.checkbox :refer [checkbox]]
   [reagent-mui.material.form-control :refer [form-control]]
   [reagent-mui.material.form-control-label :refer [form-control-label]]
   [reagent-mui.material.form-group :refer [form-group]]
   [reagent-mui.material.form-label :refer [form-label]]
   [reagent-mui.material.form-helper-text :refer [form-helper-text]]
   [reagent-mui.icons.favorite :refer [favorite]]
   [reagent-mui.icons.favorite-border :refer [favorite-border]]
   [reagent-mui.icons.bookmark :refer [bookmark]]
   [reagent-mui.icons.bookmark-border :refer [bookmark-border]]
   [reagent-mui.material.stack :refer [stack]])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Checkbox

Checkboxes allow the user to select one or more items from a set or to turn an
option on or off.

If you have multiple options appearing in a list, you can preserve space by
using checkboxes instead of on/off switches. If you have a single option, avoid
using a checkbox and use an on/off switch instead.

Documentation:
- [Checkbox](https://mui.com/material-ui/react-checkbox/)

Component APIs:
- [Checkbox](https://mui.com/material-ui/api/checkbox/): `[reagent-mui.material.checkbox :refer [checkbox]]`
- [FormControl](https://mui.com/material-ui/api/form-control/): `[reagent-mui.material.form-control :refer [form-control]]`
- [FormControlLabel](https://mui.com/material-ui/api/form-control-label/): ` [reagent-mui.material.form-control-label :refer [form-control-label]]`
- [FormGroup](https://mui.com/material-ui/api/form-group/): `[reagent-mui.material.form-group :refer [form-group]]`
- [FormLabel](https://mui.com/material-ui/api/form-label/): `[reagent-mui.material.form-label :refer [form-label]]`
")

(defcard "## Basic checkboxes"
  (let [default-props {:aria-label "Checkbox-demo"}]
    (r/as-element [stack {:direction :row :spacing 2}
                   [checkbox (r/merge-props
                              default-props {:default-checked true})]
                   [checkbox default-props]
                   [checkbox (r/merge-props default-props {:disabled true})]
                   [checkbox (r/merge-props default-props {:disabled true
                                                           :checked  true})]])))

(defcard "## Label

You can provide a label to the checkbox using the `form-control-label`
component."
  (r/as-element [stack {:direction :row :spacing 2}
                 [form-group
                  [form-control-label
                   {:control (r/as-element [checkbox {:default-checked true}])
                    :label   "Label"}]
                  [form-control-label
                   {:control  (r/as-element [checkbox {:default-checked true}])
                    :disabled true
                    :label    "Disabled"}]]]))

(defcard "## Size

Use the `:size` prop or customize the font size of the svg icons to change the
size of the checkboxes."
  (r/as-element [stack {:direction :row :spacing 2 :align-items :center}
                 [checkbox {:default-checked true
                            :size            :small}]
                 [checkbox {:default-checked true}]
                 [checkbox {:default-checked true
                            :size            :large}]
                 [checkbox {:default-checked true
                            :sx {"& .MuiSvgIcon-root" {:font-size 48}}}]]))

(defcard "## Color"
  (r/as-element [stack {:direction :row :spacing 2}
                 [checkbox {:default-checked true}]
                 [checkbox {:default-checked true
                            :color           :secondary}]
                 [checkbox {:default-checked true
                            :color           :success}]
                 [checkbox {:default-checked true
                            :color           :default}]
                 [checkbox {:default-checked true
                            :sx
                            {:color          (pink 800)
                             "&.Mui-checked" {:color (pink 600)}}}]]))

(defcard "## Icon"
  (r/as-element [stack {:direction :row :spacing 2}
                 [checkbox {:icon (r/as-element [favorite-border])
                            :checked-icon (r/as-element [favorite])}]
                 [checkbox {:icon (r/as-element [bookmark-border])
                            :checked-icon (r/as-element [bookmark])}]]))

(defcard "## Controlled

You can control the checkbox with the `:checked` and `:on-change` props."
  (fn [data-atom _]
    (r/as-element
     (let [on-change #(swap! data-atom update :checked not)]
       [stack {:direction :row :spacing 2}
        [checkbox {:checked   (:checked @data-atom)
                   :on-change on-change}]])))
  {:checked false}
  {:inspect-data true})

(defcard "## Indeterminate

A checkbox input can only have two states in a form: checked or unchecked. It
either submits its value or doesn't. Visually, there are **three** states a
checkbox can be in: checked, unchecked, or indeterminate."
  (fn [data-atom _]
    (r/as-element
     (let [on-change-parent     #(swap!
                                  data-atom
                                  update :children-checked
                                  (fn [xs]
                                    (mapv not xs)))
           on-change-child-1    #(swap!
                                  data-atom
                                  update :children-checked
                                  (fn [xs]
                                    (update xs 0 not)))
           on-change-child-2    #(swap!
                                  data-atom
                                  update :children-checked
                                  (fn [xs]
                                    (update xs 1 not)))
           children-checked     (:children-checked @data-atom)
           child-1-checked      (first children-checked)
           child-2-checked      (second children-checked)
           parent-checked       (every? true? children-checked)
           parent-indeterminate (and (some true? children-checked)
                                     (some false? children-checked))]
       [:div
        [form-control-label
         {:control (r/as-element [checkbox {:checked       parent-checked
                                            :indeterminate parent-indeterminate
                                            :on-change     on-change-parent}])
          :label   "Parent"}]
        [box {:sx {:display        :flex
                   :flex-direction :column
                   "ml"             3}}
         [form-control-label
          {:control (r/as-element [checkbox {:checked   child-1-checked
                                             :on-change on-change-child-1}])
           :label   "Child 1"}]
         [form-control-label
          {:control (r/as-element [checkbox {:checked   child-2-checked
                                             :on-change on-change-child-2}])
           :label   "Child 2"}]]])))
  {:children-checked [false false]}
  {:inspect-data true})

(defcard "## FormGroup

A `form-group` component is a helpful wrapper used to group selection control
components."
  (fn [data-atom _]
    (r/as-element
     (let [on-change (fn [event]
                       (let [a-name (keyword (.-name (.-target event)))]
                         (swap! data-atom update a-name not)))
           gilad     (:gilad @data-atom)
           jason     (:jason @data-atom)
           antoine   (:antoine @data-atom)
           error? (not= 2 (count (filter true? (vals @data-atom))))]
       [box {:sx {:display :flex}}
        [form-control {:sx        {:m 3}
                       :component :fieldset
                       :variant   :standard}
         [form-label {:component :legend}
          "Assign responsibility"]
         [form-group
          [form-control-label {:control (r/as-element
                                         [checkbox {:checked   gilad
                                                    :on-change on-change
                                                    :name      :gilad}])
                               :label   "Gilad Gray"}]
          [form-control-label {:control (r/as-element
                                         [checkbox {:checked   jason
                                                    :on-change on-change
                                                    :name      :jason}])
                               :label   "Jason Killian"}]
          [form-control-label {:control (r/as-element
                                         [checkbox {:checked   antoine
                                                    :on-change on-change
                                                    :name      :antoine}])
                               :label   "Antoine Llorca"}]]
         [form-helper-text "Be careful"]]
        [form-control {:required  true
                       :error     error?
                       :sx        {:m 3}
                       :component :fieldset
                       :variant   :standard}
         [form-label {:component :legend}
          "Pick two"]
         [form-group
          [form-control-label {:control (r/as-element
                                         [checkbox {:checked   gilad
                                                    :on-change on-change
                                                    :name      :gilad}])
                               :label   "Gilad Gray"}]
          [form-control-label {:control (r/as-element
                                         [checkbox {:checked   jason
                                                    :on-change on-change
                                                    :name      :jason}])
                               :label   "Jason Killian"}]
          [form-control-label {:control (r/as-element
                                         [checkbox {:checked   antoine
                                                    :on-change on-change
                                                    :name      :antoine}])
                               :label   "Antoine Llorca"}]]
         [form-helper-text "You can display an error"]]])))
  {:gilad   true
   :jason   false
   :antoine false}
  {:inspect-data true})

(defcard "## Label placement

You can change the placement of the label."
  (r/as-element [form-control {:component :fieldset}
                 [form-label {:component :legend} "Label placement"]
                 [form-group {:row true}
                  [form-control-label {:value           :start
                                       :control         (r/as-element [checkbox])
                                       :label           "Start"
                                       :label-placement :start}]
                  [form-control-label {:value           :top
                                       :control         (r/as-element [checkbox])
                                       :label           "Top"
                                       :label-placement :top}]
                  [form-control-label {:value           :bottom
                                       :control         (r/as-element [checkbox])
                                       :label           "Bottom"
                                       :label-placement :bottom}]
                  [form-control-label {:value           :end
                                       :control         (r/as-element [checkbox])
                                       :label           "End"
                                       :label-placement :end}]]]))

(defcard "## Customization

TODO: read the docs first")
