(ns mui.components.input.autocomplete
  (:require
   [cljs.core.async :refer [timeout take!] :as async]
   [devcards.core :as dc]
   [reagent.core :as r]
   ["@mui/material/Autocomplete" :refer [createFilterOptions]]
   [reagent-mui.material.autocomplete :refer [autocomplete]]
   [reagent-mui.material.circular-progress :refer [circular-progress]]
   [reagent-mui.material.container :refer [container]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.material.text-field :refer [text-field]]
   [reagent-mui.util :refer [js->clj']]
   [reagent-mui.styles :refer [styled]])
  (:require-macros
   [devcards.core :refer [defcard]]))


(def options [{:label "Denmark"
               :code  "dk"
               :flag  "\uD83C\uDDE9\uD83C\uDDF0"}
              {:label "Finland"
               :code  "fi"
               :flag  "\uD83C\uDDEB\uD83C\uDDEE"}
              {:label "France"
               :code  "fr"
               :flag  "\uD83C\uDDEB\uD83C\uDDF7"}
              {:label "Germany"
               :code  "de"
               :flag  "\uD83C\uDDE9\uD83C\uDDEA"}
              {:label "Iceland"
               :code  "is"
               :flag  "\uD83C\uDDEE\uD83C\uDDF8"}
              {:label "Italy"
               :code  "it"
               :flag  "\uD83C\uDDEE\uD83C\uDDF9"}
              {:label "Norway"
               :code  "no"
               :flag  "\uD83C\uDDF3\uD83C\uDDF4"}
              {:label "Spain"
               :code  "es"
               :flag  "\uD83C\uDDEA\uD83C\uDDF8"}
              {:label "Sweden"
               :code  "se"
               :flag  "\uD83C\uDDF8\uD83C\uDDEA"}
              {:label "United Kingdom"
               :code  "gb"
               :flag  "\uD83C\uDDEC\uD83C\uDDE7"}])

(def options-flat (mapv :label options))

;; Options and value will be converted to JS objects and they are compared by
;; strict equality, meaning that they won't compare as equal after conversion
;; from JS to CLJ and back.  For this reason it's better to keep them as JS
;; objects or to supply a comparison function via :get-option-selected prop
(def js-options (clj->js options))
(def js-options-flat (clj->js options-flat))

(defcard "# Autocomplete

The autocomplete is a normal text input enhanced by a panel of suggested
options.

The widget is useful for setting the value of a single-line textbox in one of
two types of scenarios:

1. The value for the textbox must be chosen from a predefined set of allowed
   values: **combo box**.

2. The textbox may contain any arbitrary value, but it is advantageous to
   suggest possible values to the user, e.g., a search field may suggest similar
   or previous searches to save the user time: **free solo**.

Documentation:

- [Autocomplete](https://mui.com/material-ui/react-autocomplete/)

Component APIs:

- [Autocomplete](https://mui.com/material-ui/api/autocomplete/): `[reagent-mui.material.autocomplete :refer [autocomplete]]`
- [Popper](https://mui.com/material-ui/api/popper/): `[reagent-mui.material.popper :refer [popper]]`
- [TextField](https://mui.com/material-ui/api/text-field/): `[reagent-mui.material.text-field :refer [text-field]]`")

(defcard "## Combo box

The value must be chosen from a predefined set of allowed values."
  (let [render-input  (fn [js-props]
                        (r/as-element
                         [text-field (r/merge-props
                                      (js->clj' js-props)
                                      {:label "Country"})]))
        render-option (fn [js-props js-option]
                        (r/as-element
                         [:li (js->clj' js-props)
                          (:flag (js->clj' js-option))
                          (:label (js->clj' js-option))]))]
    (r/as-element
     [stack {:direction :row :spacing 1}
      [autocomplete {:id             "combo-box-demo"
                     :sx             {:width 300}
                     :options        js-options
                     :disable-portal true
                     :render-input   render-input
                     :render-option  render-option}]])))

(defcard "### Options structure

By default, the component accepts the following options structures:

```
{:label <string>}
```

However, you can use different structures by providing a `:get-option-label`
prop.")

(defcard "### Playground

Each of the following examples demonstrates one feature of the Autocomplete
component."
  (fn [data-atom _]
    (let [render-input  (fn [label]
                          (fn [js-props]
                            (r/as-element
                             [text-field (r/merge-props
                                          (js->clj' js-props)
                                          {:label   label
                                           :variant :standard})])))
          render-option (fn [js-props js-option]
                          (r/as-element
                           [:li (js->clj' js-props)
                            (:flag (js->clj' js-option))
                            (:label (js->clj' js-option))]))]
      (r/as-element
       [stack {:direction :column :spacing 1 :sx {:width 300}}
        [autocomplete {:id                      "disable-close-on-select"
                       :options                 js-options
                       :disable-close-on-select true
                       :render-input            (render-input
                                                 ":disable-close-on-select")
                       :render-option           render-option}]
        [autocomplete {:id              "clear-on-escape"
                       :options         js-options
                       :clear-on-escape true
                       :render-input    (render-input
                                         ":clear-on-escape")
                       :render-option   render-option}]
        [autocomplete {:id                "disable-clearable"
                       :options           js-options
                       :disable-clearable true
                       :render-input      (render-input
                                           ":disable-clearable")
                       :render-option     render-option}]
        [autocomplete {:id                    "include-input-in-list"
                       :options               js-options
                       :include-input-in-list true
                       :render-input          (render-input
                                               ":include-input-in-list")
                       :render-option         render-option}]
        [autocomplete {:id            "flat-options"
                       :options       js-options-flat
                       :render-input  (render-input
                                       ":flat-options")}]
        [autocomplete {:id            "controlled-demo"
                       :options       js-options-flat
                       :value         (:value @data-atom)
                       :on-change     (fn [_ new-val]
                                        (swap! data-atom assoc :value new-val))
                       :render-input  (render-input
                                       ":controlled-demo")}]
        [autocomplete {:id                    "auto-complete"
                       :options               js-options
                       :include-input-in-list true
                       :render-input          (render-input
                                               ":auto-complete")
                       :render-option         render-option}]
        [autocomplete {:id                "disable-list-wrap"
                       :options           js-options
                       :disable-list-wrap true
                       :render-input      (render-input
                                           ":disable-list-wrap")
                       :render-option     render-option}]
        [autocomplete {:id            "open-on-focus"
                       :options       js-options
                       :open-on-focus true
                       :render-input  (render-input
                                       ":open-on-focus")
                       :render-option render-option}]
        [autocomplete {:id             "auto-highlight"
                       :options        js-options
                       :auto-highlight true
                       :render-input   (render-input
                                        ":auto-highlight")
                       :render-option  render-option}]
        [autocomplete {:id            "auto-select"
                       :options       js-options
                       :auto-select   true
                       :render-input  (render-input
                                       ":auto-select")
                       :render-option render-option}]
        [autocomplete {:id            "disabled"
                       :options       js-options
                       :disabled      true
                       :render-input  (render-input
                                       ":disabled")
                       :render-option render-option}]
        [autocomplete {:id             "disable-portal"
                       :options        js-options
                       :disable-portal true
                       :render-input   (render-input
                                        ":disable-portal")
                       :render-option  render-option}]
        [autocomplete {:id             "blur-on-select"
                       :options        js-options
                       :blur-on-select true
                       :render-input   (render-input
                                        ":blur-on-select")
                       :render-option  render-option}]
        [autocomplete {:id            "clear-on-blur"
                       :options       js-options
                       :clear-on-blur true
                       :render-input  (render-input
                                       ":clear-on-blur")
                       :render-option render-option}]
        [autocomplete {:id              "select-on-focus"
                       :options         js-options
                       :select-on-focus true
                       :render-input    (render-input
                                         ":select-on-focus")
                       :render-option   render-option}]
        [autocomplete {:id            "read-only"
                       :options       js-options-flat
                       :read-only     true
                       :default-value (get js-options-flat 3)
                       :render-input  (render-input
                                       ":read-only")
                       :render-option render-option}]])))
  {:value "Finland"}
  {:inspect-data true})

(defcard "### Controlled states

The component has two states that can be controlled:

- the `value` state with the `:value`/`:on-change` props combination. This state
  represents the value selected by the user, for instance when pressing Enter.

- the `input value` state with the `:input-value`/`:on-input-change` props
  combination. This state represents the value displayed in the textbox.

These two states are isolated, and should be controlled independently."
  (fn [data-atom _]
    (let [options ["Option 1" "Option 2"]]
      (r/as-element
       [container {:sx {:width :auto}}
        [stack {:direction   :column
                :align-items :center
                :spacing     1}
         [autocomplete {:id              "controlled-states"
                        :sx              {:width 300}
                        :options         options
                        :value           (:value @data-atom)
                        :on-change
                        (fn [_ new-val]
                          (swap! data-atom
                                 assoc :value (str new-val)))
                        :on-input-change
                        (fn [_ new-val]
                          (swap! data-atom
                                 assoc :input-value (str new-val)))
                        :render-input
                        (fn [js-props]
                          (r/as-element
                           [text-field (r/merge-props
                                        (js->clj' js-props)
                                        {:label   "Controlled states"
                                         :variant :standard})]))}]]])))
  {:value       ""
   :input-value ""}
  {:inspect-data true})

(defcard "## Free solo

Set `:free-solo` to true so the textbox can contain any arbitrary value.

### Search input

The prop is designed to cover the primary use case of a search input with
suggestions."
  (r/as-element
   [container {:sx {:width :auto}}
    [stack {:direction   :column
            :spacing     1}
     [autocomplete {:id        "free-solo"
                    :sx        {:width 300}
                    :free-solo true
                    :options   options
                    :render-input
                    (fn [js-props]
                      (r/as-element
                       [text-field (r/merge-props
                                    (js->clj' js-props)
                                    {:label   ":free-solo"})]))}]
     [autocomplete {:id                "free-solo-search"
                    :sx                {:width 300}
                    :free-solo         true
                    :disable-clearable true
                    :options           options
                    :render-input
                    (fn [js-props]
                      (r/as-element
                       [text-field (r/merge-props
                                    (js->clj' js-props)
                                    {:label   "Search"
                                     :type    :search})]))}]]]))

(defcard "### Creatable

If you intend to use this mode for a combo box like experience (an enhanced
version of a select element) we recommend setting:

- `:select-on-focus` to help the user clear the selected value.
- `:clear-on-blur` to help the user enter a new value.
- `:handle-home-end-keys` to move focus inside the popup with the Home and End keys.
- A last option, for instance: `Add \"YOUR SEARCH\"`."
  (fn [data-atom _]
    (let [on-change        (fn [_ new-val]
                             (swap! data-atom assoc :value (js->clj new-val)))
          render-input     (fn [js-props]
                             (r/as-element
                              [text-field (r/merge-props
                                           (js->clj' js-props)
                                           {:label "Free solo with text"})]))
          render-option    (fn [js-props js-option]
                             (let [props  (js->clj' js-props)
                                   option (js->clj' js-option)]
                               (if (string? option)
                                 (r/as-element
                                  [:li props option])
                                 (r/as-element
                                  [:li props
                                   (:flag option)
                                   (:label option)]))))
          get-option-label (fn [option]
                             (cond
                               (string? option)      option
                               (.-inputValue option) (.-inputValue option)
                               :else                 (.-label option)))
          filter-options   (fn [options params]
                             (let [filter   (createFilterOptions)
                                   filtered (filter options params)
                                   input    (.-inputValue params)]
                               (when (and (seq input)
                                          (not (some #(= input %) options)))
                                 (.push filtered
                                        (clj->js
                                         {:inputValue input
                                          :label (str "Add '" input "'")})))
                               filtered))]
      (r/as-element
       [container {:sx {:width :auto}}
        [stack {:direction   :column
                :align-items :center
                :spacing     1}
         [autocomplete {:id                   "free-solo-with-text"
                        :sx                   {:width 300}
                        :free-solo            true
                        :clear-on-blur        true
                        :handle-home-end-keys true
                        :select-on-focus      true
                        :filter-options       filter-options
                        :get-option-label     get-option-label
                        :on-change            on-change
                        :options              js-options
                        :render-input         render-input
                        :render-option        render-option}]]])))
  {:value ""}
  {:inspect-data true})

(defcard "

You could also display a dialog when the user wants to add a new value.

TODO: add the example with a dialog")

(defcard "## Grouped options

You can group the options with the `:group-by` prop. If you do so, make sure
that the options are also sorted with the same dimension that they are grouped
by, otherwise, you will notice duplicate headers.

To control how the groups are rendered, provide a custom `:render-group`
prop. This is a function that accepts an object with two fields:

- `:group` --- a string representing a group name
- `:children` --- a collection of list items that belong to the group

TODO: import and use lighten/darken helpers from '@mui/system' "
  (let [sorted-options-js  (clj->js (sort-by :label options))
        label-first-letter (fn [option] (str (first (.-label option))))
        group-header       (styled "div"
                                   {:position :sticky
                                    :top      "-8px"
                                    :padding  "4px 10px"})
        group-items        (styled "ul" {:padding 0})
        render-input       (fn [js-props]
                             (r/as-element
                              [text-field (r/merge-props
                                           (js->clj' js-props)
                                           {:label "Country (grouped)"})]))
        render-option      (fn [js-props js-option]
                             (r/as-element
                              [:li (js->clj' js-props)
                               (:flag (js->clj' js-option))
                               (:label (js->clj' js-option))]))
        render-group       (fn [params]
                             (r/as-element
                              [:li {:key (.-key params)}
                               [group-header
                                {:sx {:color            "primary.dark"
                                      :background-color "aliceblue"}}
                                (.-group params)]
                               [group-items (.-children params)]]))]
    (r/as-element
     [stack {:direction :row :spacing 1}
      [autocomplete {:id            "grouping-demo"
                     :sx            {:width 300}
                     :options       sorted-options-js
                     :group-by      label-first-letter
                     :render-input  render-input
                     :render-option render-option
                     :render-group  render-group}]])))

(defcard "## Disabled options"
  (let [get-option-disabled (fn [option]
                              (= (.-label option) "Italy"))
        render-input        (fn [js-props]
                              (r/as-element
                               [text-field (r/merge-props
                                            (js->clj' js-props)
                                            {:label "Country"})]))
        render-option       (fn [js-props js-option]
                              (r/as-element
                               [:li (js->clj' js-props)
                                (:flag (js->clj' js-option))
                                (:label (js->clj' js-option))]))]
    (r/as-element
     [stack {:direction :row :spacing 1}
      [autocomplete {:id                  "combo-box-demo"
                     :sx                  {:width 300}
                     :options             js-options
                     :get-option-disabled get-option-disabled
                     :render-input        render-input
                     :render-option       render-option}]])))

(defcard "## Asynchronous requests

The component supports two different asynchronous use-cases:

- **Load on open**: it waits for the component to be interacted with to load the
    options.

- **Search as you type**: a new request is made for each keystroke.")

(defcard "### Load on open

It displays a progress state as long as the network request is pending."
  (fn [data-atom _]
    (let [loading          (:loading @data-atom)
          async-options-js (clj->js (:options @data-atom))
          set-loading      #(swap! data-atom assoc :loading %)
          fetch-options    (fn []
                             (take! (timeout 1000)
                                    (fn []
                                      (swap! data-atom
                                             assoc :options options)
                                      (set-loading false))))
          unfetch-options  #(swap! data-atom assoc :options [])
          on-open          (fn [_]
                             (unfetch-options)
                             (set-loading true)
                             (fetch-options))
          on-close         (fn [_]
                             (set-loading false))
          render-input     (fn [js-props]
                             (let [props (js->clj' js-props)]
                               (r/as-element
                                [text-field
                                 (r/merge-props
                                  props
                                  {:label      "Country"
                                   :InputProps
                                   (r/merge-props
                                    (:InputProps props)
                                    {:end-adornment
                                     (r/as-element
                                      [:<>
                                       (when loading
                                         [circular-progress {:color :inherit
                                                             :size  20}])
                                       (:end-adornment
                                        (:InputProps props))])})})])))
          render-option    (fn [js-props js-option]
                             (r/as-element
                              [:li (js->clj' js-props)
                               (:flag (js->clj' js-option))
                               (:label (js->clj' js-option))]))]
      (r/as-element
       [stack {:direction :row :spacing 1 :justify-content :center}
        [autocomplete {:id            "async-onload-demo"
                       :sx            {:width 300}
                       :options       async-options-js
                       :loading       loading
                       :on-open       on-open
                       :on-close      on-close
                       :render-input  render-input
                       :render-option render-option}]])))
  {:options []
   :loading false}
  {:inspect-data false})

(defcard "### Search as you type

If your logic is fetching new options on each keystroke and using the current
value of the textbox to filter on the server, you may want to consider
throttling requests.

Additionally, you will need to disable the built-in filtering of the
autocomplete component by overriding the `:filter-options` prop.

TODO: add search-as-you-type example without using third-party APIs, especially
those that require tokens.")

(defcard "## Multiple values

Also known as tags, the user is allowed to enter more than one value."
  (let [render-input  (fn [js-props]
                        (r/as-element
                         [text-field (r/merge-props
                                      (js->clj' js-props)
                                      {:label "To visit"})]))
        render-option (fn [js-props js-option]
                        (r/as-element
                         [:li (js->clj' js-props)
                          (:flag (js->clj' js-option))
                          (:label (js->clj' js-option))]))
        render-tag    (fn [js-option]
                        (let [option (js->clj' js-option)]
                          (str (:flag option) " " (:label option))))]
    (r/as-element
     [stack {:direction :row :spacing 1}
      [autocomplete {:id               "tags-demo"
                     :sx               {:width 320}
                     :size             :small
                     :options          js-options
                     :multiple         true
                     :limitTags        2
                     :get-option-label render-tag
                     :render-input     render-input
                     :render-option    render-option}]])))
