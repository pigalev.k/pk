(ns mui.components.input.fab
  (:require
   [devcards.core :as dc]
   [reagent.core :as r]
   [reagent-mui.material.fab :refer [fab]]
   [reagent-mui.icons.add :refer [add]]
   [reagent-mui.icons.edit :refer [edit]]
   [reagent-mui.icons.favorite :refer [favorite]]
   [reagent-mui.icons.navigation :refer [navigation]]
   [reagent-mui.material.stack :refer [stack]])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Floating Action Button

A Floating Action Button (FAB) performs the primary, or most common, action on a
screen.

A floating action button appears in front of all screen content, typically as a
circular shape with an icon in its center. FABs come in two `:variant`s:
`:circular` (default), and `:extended`.

Only use a FAB if it is the most suitable way to present a screen's primary
action. Only one component is recommended per screen to represent the most
common action.

Documentation:
- [Fab](https://mui.com/material-ui/react-floating-action-button/)

Component APIs:
- [Fab](https://mui.com/material-ui/api/fab/): `[reagent-mui.material.fab :refer [fab]]`")

(defcard "## Basic FAB"
  (r/as-element [stack {:direction :row :spacing 2 :align-items :center}
                 [fab {:color   :primary
                       :variant :circular} [add]]
                 [fab {:color :secondary} [edit]]
                 [fab {:variant :extended}
                  [navigation {:sx {:mr 1}}]
                  "Navigate"]
                 [fab {:disabled true} [favorite]]]))

(defcard "## Size

By default, the size is `:large`. Use the `:size` prop for smaller floating
action buttons (`:medium`, `:small`)."
  (r/as-element [stack {:direction :column :spacing 2}
                 [stack {:direction :row :spacing 2 :align-items :center}
                  [fab {:color :secondary
                        :size  :small} [add]]
                  [fab {:color :secondary
                        :size  :medium} [add]]
                  [fab {:color :secondary} [add]]]
                 [stack {:direction :row :spacing 2 :align-items :center}
                  [fab {:color   :primary
                        :variant :extended
                        :size    :small}
                   [navigation {:sx {:mr 1}}]
                   "Extended"]
                  [fab {:color   :primary
                        :variant :extended
                        :size    :medium}
                   [navigation {:sx {:mr 1}}]
                   "Extended"]
                  [fab {:color   :primary
                        :variant :extended}
                   [navigation {:sx {:mr 1}}]
                   "Extended"]]]))

(defcard "## Animation

The floating action button animates onto the screen as an expanding piece of
material, by default.

A floating action button that spans multiple lateral screens (such as tabbed
screens) should briefly disappear, then reappear if its action changes.

The Zoom transition can be used to achieve this. Note that since both the
exiting and entering animations are triggered at the same time, we use
`:enter-delay` to allow the outgoing Floating Action Button's animation to
finish before the new one enters.

TODO: add animation example."
  (fn [_ _]
    (r/as-element [stack])))
