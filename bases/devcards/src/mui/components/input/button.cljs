(ns mui.components.input.button
  (:require
   [devcards.core :as dc]
   [reagent.core :as r]
   [reagent-mui.material.button :refer [button]]
   [reagent-mui.icons.add-shopping-cart :refer [add-shopping-cart]]
   [reagent-mui.icons.alarm :refer [alarm]]
   [reagent-mui.icons.delete :refer [delete]]
   [reagent-mui.icons.fingerprint :refer [fingerprint]]
   [reagent-mui.icons.photo-camera :refer [photo-camera]]
   [reagent-mui.icons.save :refer [save]]
   [reagent-mui.icons.send :refer [send]]
   [reagent-mui.lab.loading-button :refer [loading-button]]
   [reagent-mui.material.icon-button :refer [icon-button]]
   [reagent-mui.material.stack :refer [stack]])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Button

Buttons allow users to take actions, and make choices, with a single tap.

Documentation:
- [Button](https://mui.com/material-ui/react-button/)

Component APIs:
- [Button](https://mui.com/material-ui/api/button/): `[reagent-mui.material.button :refer [button]]`
- [ButtonBase](https://mui.com/material-ui/api/button-base/): `[reagent-mui.material.button-base :refer [button-base]]`
- [IconButton](https://mui.com/material-ui/api/icon-button/): `[reagent-mui.material.icon-button :refer [icon-button]]`
- [LoadingButton](https://mui.com/material-ui/api/loading-button/): `[reagent-mui.lab.loading-button :refer [loading-button]]`")

(defcard "## Basic button

The Button comes with three variants: `:text` (default), `:contained`, and
`:outlined`. Variant is specified with `:variant` prop."
  (r/as-element [stack {:direction :row :spacing 2}
                 [button {:variant "text"} "Text"]
                 [button {:variant "contained"} "Contained"]
                 [button {:variant "outlined"} "Outlined"]]))

(defcard "## Text button

Text buttons are typically used for less-pronounced actions, including those
located: in dialogs, in cards. In cards, text buttons help maintain an emphasis
on card content."
  (r/as-element [stack {:direction :row :spacing 2}
                 [button "Primary"]
                 [button {:disabled true} "Disabled"]
                 [button {:href
                          "#!/pk.devcards.mui.core"} "Link"]]))

(defcard "## Contained button

Contained buttons are high-emphasis, distinguished by their use of elevation and
fill. They contain actions that are primary to your app.

You can remove the elevation with the `:disable-elevation` prop."
  (r/as-element [stack {:direction :column :spacing 2}
                 [stack {:direction :row
                         :spacing 2}
                  [button {:variant "contained"} "Primary"]
                  [button {:variant  "contained"
                           :disabled true} "Disabled"]
                  [button {:variant "contained"
                           :href
                           "#!/pk.devcards.mui.core"} "Link"]]
                 [stack {:direction :row
                         :spacing 2}
                  [button {:variant           "contained"
                           :disable-elevation true} "Disable elevation"]]]))

(defcard "## Outlined button

Outlined buttons are medium-emphasis buttons. They contain actions that are
important but aren't the primary action in an app.

Outlined buttons are also a lower emphasis alternative to contained buttons, or
a higher emphasis alternative to text buttons."
  (r/as-element [stack {:direction :row :spacing 2}
                 [button {:variant "outlined"} "Primary"]
                 [button {:variant  "outlined"
                          :disabled true} "Disabled"]
                 [button {:variant "outlined"
                          :href
                          "#!/pk.devcards.mui.core"} "Link"]]))

(defcard "## Handling clicks

All components accept an `:on-click` handler that is applied to the root DOM
element."
  (r/as-element [stack {:direction :row :spacing 2}
                 [button {:variant "contained"
                          :on-click #(js/alert "Button clicked!")} "Click me"]]))



(defcard  "## Color

Color is set with the `:color` prop. In addition to using the default button
colors, you can add custom ones, or disable any you don't need."
  (r/as-element [stack {:direction :column :spacing 2}
                 [stack {:direction :row :spacing 2}
                  [button {:color   :primary
                           :variant :contained} "Primary"]
                  [button {:color   :secondary
                           :variant :contained} "Secondary"]
                  [button {:color   :success
                           :variant :contained} "Success"]
                  [button {:color   :warning
                           :variant :contained} "Warning"]
                  [button {:color   :error
                           :variant :contained} "Error"]]
                 [stack {:direction :row :spacing 2}
                  [button {:color   :primary
                           :variant :outlined} "Primary"]
                  [button {:color   :secondary
                           :variant :outlined} "Secondary"]
                  [button {:color   :success
                           :variant :outlined} "Success"]
                  [button {:color   :warning
                           :variant :outlined} "Warning"]
                  [button {:color   :error
                           :variant :outlined} "Error"]]
                 [stack {:direction :row :spacing 2}
                  [button {:color :primary} "Primary"]
                  [button {:color :secondary} "Secondary"]
                  [button {:color :success} "Success"]
                  [button {:color :warning} "Warning"]
                  [button {:color :error} "Error"]]]))

(defcard "## Button sizes

For larger or smaller buttons, use the `:size` prop."
  (r/as-element [stack {:direction :column :spacing 2}
                 [stack {:direction :row :spacing 2 :align-items :center}
                  [button {:variant :contained} "Default"]
                  [button {:size    :small
                           :variant :contained} "Small"]
                  [button {:size    :medium
                           :variant :contained} "Medium"]
                  [button {:size    :large
                           :variant :contained} "Large"]]
                 [stack {:direction :row :spacing 2 :align-items :center}
                  [button {:variant :outlined} "Default"]
                  [button {:size    :small
                           :variant :outlined} "Small"]
                  [button {:size    :medium
                           :variant :outlined} "Medium"]
                  [button {:size    :large
                           :variant :outlined} "Large"]]
                 [stack {:direction :row :spacing 2 :align-items :center}
                  [button {} "Default"]
                  [button {:size :small} "Small"]
                  [button {:size :medium} "Medium"]
                  [button {:size :large} "Large"]]]))

(defcard "## Upload button

Includes hidden input within and `:component` prop set to `:label` to allow
clicks propagate to it."
  (r/as-element [stack {:direction :row :spacing 2}
                 [button {:variant   :contained
                          :component :label}
                  "Upload"
                  [:input {:hidden true
                           :accept "image/*"
                           :type   :file}]]
                 [icon-button {:color     :primary
                               :component :label}
                  [:input {:hidden true
                           :accept "image/*"
                           :type   :file}]
                  [photo-camera]]]))

(defcard "## Button with icon and label

Sometimes you might want to have icons for certain buttons to enhance the UX of
the application as we recognize logos more easily than plain text. Props
`:start-icon` and `:end-icon` are responsible for this."
  (r/as-element [stack {:direction :row :spacing 2}
                 [button {:variant    :outlined
                          :start-icon (r/as-element [delete])} "Delete"]
                 [button {:variant    :contained
                          :end-icon (r/as-element [send])} "Send"]]))

(defcard "## Icon button

Icon buttons are commonly found in app bars and toolbars.

Icons are also appropriate for toggle buttons that allow a single choice to be
selected or deselected, such as adding or removing a star to an item."
  (r/as-element [stack {:direction :row :spacing 2}
                 [icon-button {:aria-label :delete} [delete]]
                 [icon-button {:aria-label :delete
                               :color      :primary
                               :disabled   true} [delete]]
                 [icon-button {:aria-label "add an alarm"
                               :color      :secondary} [alarm]]
                 [icon-button {:aria-label "add to shopping cart"
                               :color      :primary} [add-shopping-cart]]]))

(defcard "## Icon button sizes

For larger or smaller icon buttons, use the `:size` prop of button, to control
the size of the icons within use the `:font-size` prop of icon."
  (r/as-element [stack {:direction :row :spacing 2 :align-items :center}
                 [icon-button {:aria-label :delete
                               :size       :small}
                  [delete {:font-size :inherit}]]
                 [icon-button {:aria-label :delete
                               :size       :small}
                  [delete {:font-size :small}]]
                 [icon-button {:aria-label :delete
                               :size       :large}
                  [delete]]
                 [icon-button {:aria-label :delete
                               :size       :large}
                  [delete {:font-size :inherit}]]]))

(defcard "## Icon button colors

Use color prop to apply theme color palette to component."
  (r/as-element [stack {:direction :row :spacing 2 :align-items :center}
                 [icon-button {:aria-label :fingerprint
                               :color      :secondary}
                  [fingerprint]]
                 [icon-button {:aria-label :fingerprint
                               :color      :success}
                  [fingerprint]]]))

(defcard "## Loading button

Can show loading state and disable interactions."
  (fn [data-atom _]
    (let [switch-loading #(swap! data-atom update :loading not)
          on-click       (fn [_]
                           (switch-loading)
                           (js/setTimeout switch-loading 2000))]
      (r/as-element [stack {:direction :row :spacing 2 :align-items :center}
                     [loading-button {:loading  (:loading @data-atom)
                                      :variant  :outlined
                                      :on-click on-click}
                      "Disabled"]
                     [loading-button {:loading  (:loading @data-atom)
                                      :variant  :outlined
                                      :on-click on-click}
                      "Submit"]
                     [loading-button {:loading           (:loading @data-atom)
                                      :variant           :text
                                      :loading-indicator "Loading..."
                                      :on-click          on-click}
                      "Fetch data"]
                     [loading-button {:loading          (:loading @data-atom)
                                      :variant          :outlined
                                      :color            :secondary
                                      :loading-position :start
                                      :start-icon       (r/as-element [save])
                                      :on-click         on-click}
                      "Save"]
                     [loading-button {:loading          (:loading @data-atom)
                                      :variant          :contained
                                      :loading-position :end
                                      :end-icon         (r/as-element [send])
                                      :on-click         on-click}
                      "Send"]])))
  {:loading false}
  {:inspect-data true})

(defcard "## Unstyled button

MUI Base provides a headless (unstyled) version of this React Button
component. Try it if you need more flexibility in customization and a smaller
bundle size.")

(defcard "## Customization

TODO: read the docs first")

(defcard "## Complex button

TODO: read the docs first, add static example images")
