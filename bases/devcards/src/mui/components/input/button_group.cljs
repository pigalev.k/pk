(ns mui.components.input.button-group
  (:require
   [devcards.core :as dc]
   [reagent.core :as r]
   [reagent-mui.material.button :refer [button]]
   [reagent-mui.material.button-group :refer [button-group]]
   [reagent-mui.material.click-away-listener :refer [click-away-listener]]
   [reagent-mui.material.grow :refer [grow]]
   [reagent-mui.material.menu-list :refer [menu-list]]
   [reagent-mui.material.menu-item :refer [menu-item]]
   [reagent-mui.material.paper :refer [paper]]
   [reagent-mui.material.popper :refer [popper]]
   [reagent-mui.icons.arrow-drop-down :refer [arrow-drop-down]]
   [reagent-mui.material.stack :refer [stack]])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Button Group

Button group can be used to group related buttons.

Documentation:
- [ButtonGroup](https://mui.com/material-ui/react-button-group/)

Component APIs:
- [Button](https://mui.com/material-ui/api/button/): `[reagent-mui.material.button :refer [button]]`
- [ButtonGroup](https://mui.com/material-ui/api/button-group/): `[reagent-mui.material.button-group :refer [button-group]]`")

(defcard "## Basic button group

The buttons can be grouped by wrapping them with the ButtonGroup component. They
need to be immediate children."
  (r/as-element [stack {:direction :row :spacing 2}
                 [button-group {:variant :contained}
                  [button "One"]
                  [button "Two"]
                  [button "Three"]]]))

(defcard "## Button group variants

All the standard button variants are supported."
  (r/as-element [stack {:direction :row :spacing 2}
                 [button-group {:variant :contained}
                  [button "One"]
                  [button "Two"]
                  [button "Three"]]
                 [button-group {:variant :outlined}
                  [button "One"]
                  [button "Two"]
                  [button "Three"]]
                 [button-group {:variant :text}
                  [button "One"]
                  [button "Two"]
                  [button "Three"]]]))

(defcard "## Sizes and colors

The `:size` and `:color` props can be used to control the appearance of the
button group."
  (r/as-element [stack {:direction :column :spacing 2}
                 [stack {:direction :row :spacing 2}
                  [button-group {:variant :contained
                                 :size    :large
                                 :color   :success}
                   [button "One"]
                   [button "Two"]
                   [button "Three"]]]
                 [stack {:direction :row :spacing 2}
                  [button-group {:variant :outlined
                                 :size    :medium
                                 :color   :secondary}
                   [button "One"]
                   [button "Two"]
                   [button "Three"]]]
                 [stack {:direction :row :spacing 2}
                  [button-group {:variant :text
                                 :size    :small
                                 :color   :warning}
                   [button "One"]
                   [button "Two"]
                   [button "Three"]]]]))

(defcard "## Vertical button group

The button group can be displayed vertically using the `:orientation` prop."
  (r/as-element [stack {:direction :row :spacing 2}
                 [stack {:direction :column :spacing 2}
                  [button-group {:variant     :outlined
                                 :orientation :vertical}
                   [button "One"]
                   [button "Two"]
                   [button "Three"]]]
                 [stack {:direction :row :spacing 2}
                  [button-group {:variant     :contained
                                 :orientation :vertical}
                   [button "One"]
                   [button "Two"]
                   [button "Three"]]]
                 [stack {:direction :row :spacing 2}
                  [button-group {:variant     :outlined
                                 :orientation :vertical}
                   [button "One"]
                   [button "Two"]
                   [button "Three"]]]]))

(defcard "## Split button

Button group can also be used to create a split button. The dropdown can change
the button action (as in this example) or be used to immediately trigger a
related action."
  ;; place the ref in outer scope to avoid constant reset on rerenders
  (let [button-group-ref (atom nil)]
    (fn [data-atom _]
      (let [options            ["Create a merge commit"
                                "Squash and merge"
                                "Rebase and merge"]
            on-button-click    #(js/alert
                                 (str "You clicked '"
                                      (get options (:selected-index @data-atom))
                                      "'"))
            on-dropdown-click  #(swap! data-atom update :dropdown-open not)
            on-menu-item-click (fn [index]
                                 (fn [_]
                                   (swap! data-atom assoc
                                          :selected-index index
                                          :dropdown-open  false)))
            on-away-click      (fn [event]
                                 (when (not= (.-target event) @button-group-ref)
                                   (swap! data-atom assoc :dropdown-open false)))]
        (r/as-element
         [stack {:direction :row :spacing 2}
          [button-group {:variant :contained
                         :ref     (fn [el]
                                    (reset! button-group-ref el))}
           [button {:on-click on-button-click}
            (get options (:selected-index @data-atom))]
           [button {:size     :small
                    :on-click on-dropdown-click}
            [arrow-drop-down]]]
          [popper {:sx             {:z-index 1}
                   :open           (:dropdown-open @data-atom)
                   :anchor-el      @button-group-ref
                   :role           nil
                   :transition     true
                   :disable-portal true
                   :modifiers      (clj->js
                                    [{"name"    "preventOverflow"
                                      "enabled" true
                                      "options" {"altAxis"      true
                                                 "altBoundary"  true
                                                 "tether"       true
                                                 "rootBoundary" "document"
                                                 "padding"      8}}
                                     {"name"    "offset"
                                      "options" {"offset" [0, 10]}}])}
           (fn [props]
             (let [{:keys [TransitionProps _]}
                   (js->clj props :keywordize-keys true)]
               (r/as-element
                [grow (r/merge-props
                       TransitionProps
                       {:style
                        {:transform-origin (if (= (.-placement props)
                                                  "bottom")
                                             "center top"
                                             "center bottom")}})
                 [paper
                  [click-away-listener {:on-click-away on-away-click}
                   [menu-list {:id              "split-button-menu"
                               :auto-focus-item true}
                    (for [idx (range (count options))]
                      [menu-item {:key      (get options idx)
                                  :disabled (= idx 2)
                                  :selected (= idx (:selected-index @data-atom))
                                  :on-click (on-menu-item-click idx)}
                       (get options idx)])]]]])))]]))))
  {:selected-index 0
   :dropdown-open  false}
  {:inspect-data true})

(defcard "## Disabled elevation

You can remove the elevation with the `:disable-elevation` prop."
  (r/as-element [stack {:direction :row :spacing 2}
                 [button-group {:variant           :contained
                                :disable-elevation true}
                  [button "One"]
                  [button "Two"]
                  [button "Three"]]]))
