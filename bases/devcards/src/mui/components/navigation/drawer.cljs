(ns mui.components.navigation.drawer
  (:require
   [devcards.core :as dc]
   [mui.common :refer [example]]
   [reagent-mui.material.drawer :refer [drawer]]
   [reagent-mui.material.swipeable-drawer :refer [swipeable-drawer]]
   [reagent.core :as r])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Drawer

Navigation drawers provide access to destinations in your app. Side sheets are
surfaces containing supplementary content that are anchored to the left or right
edge of the screen.

Navigation drawers (or \"sidebars\") provide access to destinations and app
functionality, such as switching accounts. They can either be permanently
on-screen or controlled by a navigation menu icon.

Documentation:
- [Drawer](https://mui.com/material-ui/react-drawer/)

Component APIs:
- [Drawer](https://mui.com/material-ui/api/drawer/): `[reagent-mui.material.drawer :refer [drawer]]`
- [SwipeableDrawer](https://mui.com/material-ui/api/swipeable-drawer/): `[reagent-mui.material.swipeable-drawer :refer [swipeable-drawer]]`")

(defcard "## Temporary drawer

Temporary navigation drawers can toggle open or closed. Closed by default, the
drawer opens temporarily above all other content until a section is selected.

The drawer can be cancelled by clicking the overlay or pressing the Esc key. It
closes when an item is selected, handled by controlling the `:open` prop."
  (r/as-element
   [example
    [drawer {:open false :on-open (fn [_]) :on-close (fn [_])}]
    [swipeable-drawer {:open false :on-open (fn [_]) :on-close (fn [_])}]]))
