(ns mui.components.layout.grid
  (:require
   [cljs.pprint :refer [pprint]]
   [devcards.core :as dc]
   [mui.common :refer [example item dark-mode?]]
   [reagent.core :as r]
   [reagent-mui.material.avatar :refer [avatar]]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.button-base :refer [button-base]]
   [reagent-mui.material.form-label :refer [form-label]]
   [reagent-mui.material.form-control :refer [form-control]]
   [reagent-mui.material.form-control-label :refer [form-control-label]]
   [reagent-mui.material.grid :refer [grid]]
   [reagent-mui.material.radio :refer [radio]]
   [reagent-mui.material.radio-group :refer [radio-group]]
   [reagent-mui.material.paper :refer [paper]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.material.typography :refer [typography]]
   [reagent-mui.styles :refer [styled]])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Grid

The Material Design responsive layout grid adapts to screen size and
orientation, ensuring consistency across layouts.

Material Design's responsive UI is based on a 12-column grid layout.

Documentation:
- [Grid](https://mui.com/material-ui/react-grid/)
- [CSS Flexbox Guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

Component APIs:
- [Grid](https://mui.com/material-ui/api/grid/): `[reagent-mui.material.grid :refer [grid]]`
- [Grid2](https://mui.com/material-ui/react-grid2/): reworked version of flexbox-based `grid` with some features added and issues addressed; as of now is considered unstable")

(defcard "How it works

The grid system is implemented with the `grid` component.

- It uses CSS Flexbox.
- There are two types of layout: **containers** and **items**.
- Item widths are set in percentages, so they're always fluid and sized relative to their parent element.
- Items have padding to create the spacing between individual items.
- There are five grid breakpoints: **xs**, **sm**, **md**, **lg**, and **xl**.
- Integer values can be given to each breakpoint, indicating how many of the 12 available columns are occupied by the component when the viewport width satisfies the breakpoint constraints.
")

(defcard "## Fluid grids

Fluid grids use columns that scale and resize content. A fluid grid's layout can
use breakpoints to determine if the layout needs to change dramatically.")

(defcard "### Basic grid

Column widths are integer values between 1 and 12; they apply at any breakpoint
and indicate how many columns are occupied by the component.

A value given to a breakpoint applies to all the other breakpoints wider than
it (unless overridden, as you can read later in this page). For example, `{:xs
12}` sizes a component to occupy the whole viewport width regardless of its
size."
  (r/as-element
   [example
    [grid {:container true
           :spacing   2}
     [grid {:item true
            :xs   8}
      [item ":xs 8"]]
     [grid {:item true
            :xs   4}
      [item ":xs 4"]]
     [grid {:item true
            :xs   4}
      [item ":xs 4"]]
     [grid {:item true
            :xs   8}
      [item ":xs 8"]]
     [grid {:item true
            :xs   12}
      [item ":xs 12"]]]]))

(defcard "### Grid with multiple breakpoints

Components may have multiple widths defined, causing the layout to change at the
defined breakpoint. Width values given to larger breakpoints override those
given to smaller breakpoints.

For example, `{:xs 12 :sm 6}` sizes a component to occupy half of the viewport
width (6 columns) when viewport width is 600 or more pixels. For smaller
viewports, the component fills all 12 available columns."
  (r/as-element
   [example
    [grid {:container true
           :spacing   2}
     [grid {:item true
            :xs   6
            :md   8}
      [item ":xs 6 :md 8"]]
     [grid {:item true
            :xs   6
            :md   4}
      [item ":xs 6 :md 4"]]
     [grid {:item true
            :xs   6
            :md   4}
      [item ":xs 6 :md 4"]]
     [grid {:item true
            :xs   6
            :md   8}
      [item ":xs 6 :md 8"]]
     [grid {:item true
            :xs   12
            :md   8}
      [item ":xs 12 :md 8"]]]]))

(defcard "## Spacing

To control space between children, use the `:spacing` prop. The spacing value
can be any positive number, including decimals and any string. The prop is
converted into a CSS property using the `(:spacing theme)` helper.

### Interactive demo
```
[grid {:container       true
       :justify-content :center
       :spacing         spacing}
 (for [index (range 3)]
   [grid {:key  index
          :item true}
    [a-card]])]
```
"
  (fn [data-atom _]
    (let [spacings    [0 0.5 1 2 3 4 8 12]
          spacing     (:spacing @data-atom)
          code        (str `[~'grid {:container true
                                     :spacing   ~spacing} ...])
          set-spacing (fn [event]
                        (let [value (-> event .-target .-value parse-double)]
                          (when value (swap! data-atom assoc :spacing value))))
          a-card      (styled paper (fn [{:keys [theme]}]
                                      (let [dark?    (dark-mode? theme)
                                            bg-color (if dark? "#1a2027" "#fff")]
                                        {:height           140
                                         :width            100
                                         :background-color bg-color})))]
      (r/as-element
       [example
        [stack {:direction :column
                :spacing   2}
         [grid {:container       true
                :justify-content :center
                :spacing         spacing}
          (for [index (range 3)]
            [grid {:key  index
                   :item true}
             [a-card]])]
         [grid {:item true
                :xs 12}
          [stack {:direction :column
                  :spacing 1}
           [paper {:sx {:p 2}}
            [grid {:container true}
             [grid {:item true}
              [form-control {:component :fieldset}
               [form-label {:component :legend} "spacing"]
               [radio-group {:name      "spacing"
                             :value     (str spacing)
                             :on-change set-spacing
                             :row       true}
                (for [spc spacings]
                  [form-control-label {:key     spc
                                       :value   (str spc)
                                       :label   (str spc)
                                       :control (r/as-element [radio])}])]]]]]
           [item code]]]]])))
  {:spacing 1}
  {:inspect-data true})

(defcard "### Row & column spacing

The `:row-spacing` and `:column-spacing` props allow for specifying the row and
column gaps independently. It's similar to the `row-gap` and `column-gap`
properties of CSS Grid."
  (r/as-element
   [example
    [grid {:container      true
           :row-spacing    1
           :column-spacing 2}
     [grid {:item true
            :xs   6}
      [item "1"]]
     [grid {:item true
            :xs   6}
      [item "2"]]
     [grid {:item true
            :xs   6}
      [item "3"]]
     [grid {:item true
            :xs   6}
      [item "4"]]]]))

(defcard "## Responsive values

Values of some properties can depend on the active breakpoint, i.e., to be maps
from breakpoint names to actual values."
  (r/as-element
   [example
    [grid {:container true
           :spacing   {:xs 2 :md 4}
           :columns   {:xs 4 :sm 8 :md 12}}
     (for [index (range 6)]
       [grid {:key  index
              :item true
              :xs   2
              :sm   4
              :md   4}
        [item ":xs 2 :sm 4 :md 4"]])]]))

(defcard
  "Responsive values are supported by:

- `:columns`
- `:column-spacing`
- `:direction`
- `:row-spacing`
- `:spacing`
- all the other props of the [system](https://mui.com/system/properties/)

>Note: when using a responsive columns prop, each grid item needs its
 corresponding breakpoint. For instance, this is not working --- the grid item
 misses the value for `:md`:

```
[grid {:container true
       :columns   {:xs 4 :md 12}}
  [grid {:item true
         :xs 2}]]
```
")

(defcard "## Interactive demo

To explore the visual results of the different settings.

```
[grid {:container       true
       :direction       direction
       :spacing         spacing
       :justify-content justify-content
       :align-items     align-items
       :sx              {:height 240
                         :width  \"100%\"}}
 (for [index (range 3)]
   [grid {:key  index
          :item true}
    [(a-card {:min-height (* 30 (inc index))
              :min-width  50}) index]])]
```
"
  (fn [data-atom _]
    (let [direction-values       ["row" "row-reverse" "column" "column-reverse"]
          spacing-values         [0 0.5 1 2 3 4 8 12]
          justify-content-values ["flex-start" "center" "flex-end"
                                  "space-between" "space-around" "space-evenly"]
          align-items-values     ["flex-start" "center" "flex-end" "stretch"
                                  "baseline"]
          direction              (:direction @data-atom)
          spacing                (:spacing @data-atom)
          justify-content        (:justify-content @data-atom)
          align-items            (:align-items @data-atom)
          code                   (with-out-str
                                   (pprint
                                    `[~'grid {:container       true
                                              :direction       ~direction
                                              :spacing         ~spacing
                                              :justify-content ~justify-content
                                              :align-items     ~align-items}
                                      ...]))
          set-direction          (fn [event]
                                   (let [value (-> event .-target .-value)]
                                     (when value
                                       (swap! data-atom assoc
                                              :direction value))))
          set-spacing            (fn [event]
                                   (let [value (-> event .-target .-value
                                                   parse-double)]
                                     (when value
                                       (swap! data-atom assoc :spacing value))))
          set-justify            (fn [event]
                                   (let [value (-> event .-target .-value)]
                                     (when value
                                       (swap! data-atom assoc
                                              :justify-content value))))
          set-align              (fn [event]
                                   (let [value (-> event .-target .-value)]
                                     (when value
                                       (swap! data-atom assoc
                                              :align-items value))))
          a-card                 (fn [{:keys [min-height min-width]}]
                                   (styled item  {:display         :flex
                                                  :justify-content :center
                                                  :align-items     :center
                                                  :min-height      min-height
                                                  :height          "100%"
                                                  :min-width       min-width
                                                  :width           "100%"
                                                  :padding         0}))]
      (r/as-element
       [example
        [stack {:direction :column
                :spacing   2}
           [grid {:container       true
                  :direction       direction
                  :spacing         spacing
                  :justify-content justify-content
                  :align-items     align-items
                  :sx              {:height 240
                                    :width  "100%"}}
            (for [index (range 3)]
              [grid {:key  index
                     :item true}
               [(a-card {:min-height (* 30 (inc index))
                         :min-width  50}) index]])]
         [grid {:item true
                :xs   12}
          [stack {:direction :column
                  :spacing   1}
           [paper {:sx {:p 2}}
            [grid {:container true}
             [grid {:item true}
              [form-control {:component :fieldset}
               [form-label {:component :legend} "spacing"]
               [radio-group {:name      "spacing"
                             :value     (str spacing)
                             :on-change set-spacing
                             :row       true}
                (for [spc spacing-values]
                  [form-control-label {:key     spc
                                       :value   (str spc)
                                       :label   (str spc)
                                       :control (r/as-element [radio])}])]]
              [form-control {:component :fieldset}
               [form-label {:component :legend} "direction"]
               [radio-group {:name      "direction"
                             :value     direction
                             :on-change set-direction
                             :row       true}
                (for [dir direction-values]
                  [form-control-label {:key     dir
                                       :value   dir
                                       :label   dir
                                       :control (r/as-element [radio])}])]]
              [form-control {:component :fieldset}
               [form-label {:component :legend} "justify-content"]
               [radio-group {:name      "justify-content"
                             :value     justify-content
                             :on-change set-justify
                             :row       true}
                (for [just justify-content-values]
                  [form-control-label {:key     just
                                       :value   just
                                       :label   just
                                       :control (r/as-element [radio])}])]]
              [form-control {:component :fieldset}
               [form-label {:component :legend} "align-items"]
               [radio-group {:name      "align-items"
                             :value     align-items
                             :on-change set-align
                             :row       true}
                (for [align align-items-values]
                  [form-control-label {:key     align
                                       :value   align
                                       :label   align
                                       :control (r/as-element [radio])}])]]]]]
           [item {:sx {:text-align :left
                       :p          2}}
            [:pre {:sx {:p 2}}code]]]]]])))
  {:spacing         1
   :direction       "row"
   :justify-content "center"
   :align-items     "center"}
  {:inspect-data true})

(defcard "## Auto-layout

The Auto-layout makes the items equitably share the available space. That also
means you can set the width of one item and the others will automatically resize
around it."
  (r/as-element
   [example
    [grid {:container true
           :spacing   3}
     [grid {:item true
            :xs   true}
      [item ":xs true"]]
     [grid {:item true
            :xs   6}
      [item ":xs 6"]]
     [grid {:item true
            :xs   true}
      [item ":xs true"]]]]))

(defcard "### Variable width content

Set one of the size breakpoint props to `:auto` instead of `true` / a `number`
to size a column based on the natural width of its content."
  (r/as-element
   [stack {:direction :column
           :spacing   1}
    [example
     [grid {:container true
            :spacing   3}
      [grid {:item true
             :xs   :auto}
       [item "Item width"]]
      [grid {:item true
             :xs   6}
       [item ":xs 6"]]
      [grid {:item true
             :xs   true}
       [item ":xs true"]]]]
    [example
     [grid {:container true
            :spacing   3}
      [grid {:item true
             :xs   :auto}
       [item "Item width from content"]]
      [grid {:item true
             :xs   6}
       [item ":xs 6"]]
      [grid {:item true
             :xs   true}
       [item ":xs true"]]]]]))

(defcard "## Complex Grid

The following demo doesn't follow the Material Design guidelines, but
illustrates how the grid can be used to build complex layouts."
  (let [img (styled "img" {:margin     :auto
                           :display    :block
                           :max-width  "100%"
                           :max-height "100%"})]
    (r/as-element
     [paper {:sx {:p                2
                  :margin           :auto
                  :max-width        500
                  :flex-grow        1
                  :background-color (fn [theme]
                                      (if (dark-mode? theme)
                                        "#1a2027" "#fff"))}}
      [grid {:container true
             :spacing   2}
       [grid {:item true}
        [button-base {:sx {:width  128
                           :height 128}}
         [img {:alt "complex"
               :src "/img/complex.jpg"}]]]
       [grid {:item      true
              :xs        12
              :sm        true
              :container true}
        [grid {:item      true
               :xs        true
               :container true
               :direction :column
               :spacing   2}
         [grid {:item true
                :xs   true}
          [typography {:gutter-bottom true
                       :variant       :subtitle1
                       :component     :div}
           "Standard license"]
          [typography {:gutter-bottom true
                       :variant       :body2}
           "Full resolution 1920x1080 • JPEG"]
          [typography {:color   "text.secondary"
                       :variant :body2}
           "ID: 1030114"]]
         [grid {:item true}
          [typography {:sx      {:cursor :pointer}
                       :variant :body-2}
           "Remove"]]]
        [grid {:item true}
         [typography {:variant       :subtitle1
                      :component     :div}
          "$19.00"]]]]])))

(defcard "## Nested Grid

The `:container` and `:item` props are two independent booleans; they can be
combined to allow a Grid component to be both a flex container and child.

```
(let [form-row (fn [props]
                 [:<>
                  (for [index (range 3)]
                    [grid {:key  index
                           :item true
                           :xs   4}
                     [item \"Item\"]])])]
  (r/as-element
   [example
    [box {:sx {:flex-grow 1}}
     [grid {:container true
            :spacing   1}
      (for [index (range 3)]
        [grid {:key       index
               :container true
               :item      true
               :spacing   3}
         [form-row]])]]]))
```

>Note: defining an explicit width to a `grid` element that is flex container,
 flex item, and has spacing at the same time leads to unexpected behavior, avoid
 doing it:
```
[grid {:spacing 1 :container true :item true :xs 12} ...]

```
If you need to do such, remove one of the props.
"
  (let [form-row (fn [_]
                   [:<>
                    (for [index (range 3)]
                      [grid {:key  index
                             :item true
                             :xs   4}
                       [item "Item"]])])]
    (r/as-element
     [example
      [box {:sx {:flex-grow 1}}
       [grid {:container true
              :spacing   1}
        (for [index (range 3)]
          [grid {:key       index
                 :container true
                 :item      true
                 :spacing   3}
           [form-row]])]]])))

(defcard "## Columns

You can change the default number of columns (12) with the `:columns` prop."
  (r/as-element
   [example
    [grid {:container true
           :spacing   2
           :columns   16}
     [grid {:item true
            :xs   8}
      [item ":xs 8"]]
     [grid {:item true
            :xs   8}
      [item ":xs 8"]]]]))

(defcard "## Limitations

### Negative margin

The spacing between items is implemented with a negative margin. This might lead
to unexpected behaviors. For instance, to apply a background color, you need to
apply `display: flex;` to the parent.

### white-space: nowrap

The initial setting on flex items is `min-width: auto`. This causes a
positioning conflict when children use `white-space: nowrap;`. You can reproduce
the issue with:

```
[grid {:item true :xs true}
 [typography {:no-wrap true}]]
```

In order for the item to stay within the container you need to set `min-width:
0;`. In practice, you can set the `:zero-min-width` prop:

```
[grid {:item true :xs true :zero-min-width true}
 [typography {:no-wrap true}]]
```
"
  (let [message (str "Truncation should be conditionally applicable "
                     "on this long lineof text as this is a much longer "
                     "line than what the container can support.")
        a-card  (styled paper (fn [{:keys [theme]}]
                                (let [spacing    (:spacing theme)
                                      color      (get-in theme [:palette
                                                                :text
                                                                :primary])
                                      text-attrs (get-in theme
                                                         [:typography :body-2])]
                                  (merge
                                   text-attrs
                                   {:background-color (if (dark-mode? theme)
                                                        "#1a2027" "#fff")
                                    :color            color
                                    :padding          (spacing 2)
                                    :max-width        400}))))]
    (r/as-element
     [example
      [box {:sx {:flex-grow 1
                 :overflow  :hidden
                 :px        3}}
       (for [index (range 3)]
         [a-card {:key index
                  :sx  {:my 1 :mx :auto :p 2}}
          [grid {:container true
                 :spacing   2
                 :wrap      :nowrap}
           [grid {:item true}
            [avatar "W"]]
           [grid {:item           true
                  :xs             true
                  :zero-min-width (= 0 index)}
            [typography {:no-wrap (not= 2 index)} message]]]])]])))

(defcard "### direction: column | column-reverse

The `xs`, `sm`, `md`, `lg`, and `xl` props are not supported within
 `{:direction :column}` and `{:direction :column-reverse}` containers.

They define the number of grids the component will use for a given
breakpoint. They are intended to control **width** using `flex-basis` in `row`
containers but they will impact **height** in `column` containers. If used,
these props may have undesirable effects on the height of the `grid` item
elements.")

(defcard "## CSS Grid Layout

The `grid` component is using CSS flexbox internally, but you can easily
use [the system](https://mui.com/system/grid/) and CSS Grid to layout your
pages."
  (r/as-element
   [example
    [box {:display               :grid
          :grid-template-columns "repeat(12,1fr)"
          :gap                   2}
     [box {:grid-column "span 8"}
      [item ":xs 8"]]
     [box {:grid-column "span 4"}
      [item ":xs 4"]]
     [box {:grid-column "span 4"}
      [item ":xs 4"]]
     [box {:grid-column "span 8"}
      [item ":xs 8"]]]]))

(defcard "## System

As a CSS utility component, `grid` supports all [system
properties](https://mui.com/system/properties/). You can use them as props
directly on the component. For instance, a padding:

```
[grid {:item true :p 2} ...]
```")
