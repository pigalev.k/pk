(ns mui.components.layout.stack
  (:require
   [cljs.pprint :refer [pprint]]
   [devcards.core :as dc]
   [mui.common :refer [example item]]
   [reagent.core :as r]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.divider :refer [divider]]
   [reagent-mui.material.form-label :refer [form-label]]
   [reagent-mui.material.form-control :refer [form-control]]
   [reagent-mui.material.form-control-label :refer [form-control-label]]
   [reagent-mui.material.grid :refer [grid]]
   [reagent-mui.material.radio :refer [radio]]
   [reagent-mui.material.radio-group :refer [radio-group]]
   [reagent-mui.material.paper :refer [paper]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.styles :refer [styled]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard "# Stack

Manages layout of immediate children along the vertical or horizontal axis with
optional spacing and/or dividers between each child.

Documentation:
- [Stack](https://mui.com/material-ui/react-stack/)

Component APIs:
- [Stack](https://mui.com/material-ui/api/stack/)")

(defcard "## Usage

`stack` is concerned with one-dimensional layouts, while `grid` handles
two-dimensional layouts. The default `:direction` is `:column`, which stacks
children vertically.

```
(r/as-element
 [example {:sx {:width :auto}}
  [stack {:spacing 2}
   [item \"Item 1\"]
   [item \"Item 2\"]
   [item \"Item 3\"]]]
```
"
  (r/as-element
   [example {:sx {:width :auto}}
    [stack {:spacing 2}
     [item "Item 1"]
     [item "Item 2"]
     [item "Item 3"]]]))

(defcard "## Direction

By default, `stack` arranges items vertically in a `:column`. However, the
`:direction` prop can be used to position items horizontally in a `:row` as
well.

```
(r/as-element
 [example {:sx {:width :auto}}
  [stack {:direction :row
          :spacing   2}
   [item \"Item 1\"]
   [item \"Item 2\"]
   [item \"Item 3\"]]]
```
"
  (r/as-element
   [example {:sx {:width :auto}}
    [stack {:direction :row
            :spacing   2}
     [item "Item 1"]
     [item "Item 2"]
     [item "Item 3"]]]))

(defcard "## Dividers

Use the `:divider` prop to insert an element between each child. This works
particularly well with the `divider` component.

```
(r/as-element
 [example {:sx {:width :auto}}
  [stack {:direction :row
          :spacing   2
          :divider   (r/as-element
                      [divider {:orientation :vertical
                                :flex-item   true}])}
   [item \"Item 1\"]
   [item \"Item 2\"]
   [item \"Item 3\"]]])
```
"
  (r/as-element
   [example {:sx {:width :auto}}
    [stack {:direction :row
            :spacing   2
            :divider   (r/as-element
                        [divider {:orientation :vertical
                                  :flex-item   true}])}
     [item "Item 1"]
     [item "Item 2"]
     [item "Item 3"]]]))

(defcard "## Responsive values

You can switch `:direction` or `:spacing` values based on the active
breakpoint."
  (r/as-element
   [example {:sx {:width :auto}}
    [stack {:direction {:xs :column :sm :row}
            :spacing   {:xs 1 :sm 2 :md 4}}
     [item "Item 1"]
     [item "Item 2"]
     [item "Item 3"]]]))

(defcard "## Interactive demo

To explore the visual results of the different settings.

```
[box {:pb     2
      :width :auto
      :height 240}
 [stack  {:direction       direction
          :spacing         spacing
          :justify-content justify-content
          :align-items     align-items
          :height \"100%\"}
  (for [index (range 3)]
    [(a-card {:min-height (* 30 (inc index))
              :min-width  50}) index])]]
```
"
  (fn [data-atom _]
    (let [direction-values       ["row" "row-reverse" "column" "column-reverse"]
          spacing-values         [0 0.5 1 2 3 4 8 12]
          justify-content-values ["flex-start" "center" "flex-end"
                                  "space-between" "space-around" "space-evenly"]
          align-items-values     ["flex-start" "center" "flex-end" "stretch"
                                  "baseline"]
          direction              (:direction @data-atom)
          spacing                (:spacing @data-atom)
          justify-content        (:justify-content @data-atom)
          align-items            (:align-items @data-atom)
          code                   (with-out-str
                                   (pprint
                                    `[~'stack {:direction       ~direction
                                               :spacing         ~spacing
                                               :justify-content ~justify-content
                                               :align-items     ~align-items}
                                      ...]))
          set-direction          (fn [event]
                                   (let [value (-> event .-target .-value)]
                                     (when value
                                       (swap! data-atom assoc
                                              :direction value))))
          set-spacing            (fn [event]
                                   (let [value (-> event .-target .-value
                                                   parse-double)]
                                     (when value
                                       (swap! data-atom assoc :spacing value))))
          set-justify            (fn [event]
                                   (let [value (-> event .-target .-value)]
                                     (when value
                                       (swap! data-atom assoc
                                              :justify-content value))))
          set-align              (fn [event]
                                   (let [value (-> event .-target .-value)]
                                     (when value
                                       (swap! data-atom assoc
                                              :align-items value))))
          a-card                 (fn [{:keys [min-height min-width]}]
                                   (styled item  {:display         :flex
                                                  :justify-content :center
                                                  :align-items     :center
                                                  :min-height      min-height
                                                  :min-width       min-width
                                                  :padding         0}))]
      (r/as-element
       [example
        [stack {:direction :column
                :spacing   2}
         [box {:pb     2
               :width  :auto
               :height 240}
          [stack  {:direction       direction
                   :spacing         spacing
                   :justify-content justify-content
                   :align-items     align-items
                   :height          "100%"}
           (for [index (range 3)]
             [(a-card {:min-height (* 30 (inc index))
                       :min-width  50})
              {:key index} index])]]]
        [grid {:item true
               :xs   12}
         [stack {:direction :column
                 :spacing   1}
          [paper {:sx {:p 2}}
           [grid {:container true}
            [grid {:item true}
             [form-control {:component :fieldset}
              [form-label {:component :legend} "spacing"]
              [radio-group {:name      "spacing"
                            :value     (str spacing)
                            :on-change set-spacing
                            :row       true}
               (for [spc spacing-values]
                 [form-control-label {:key     spc
                                      :value   (str spc)
                                      :label   (str spc)
                                      :control (r/as-element [radio])}])]]
             [form-control {:component :fieldset}
              [form-label {:component :legend} "direction"]
              [radio-group {:name      "direction"
                            :value     direction
                            :on-change set-direction
                            :row       true}
               (for [dir direction-values]
                 [form-control-label {:key     dir
                                      :value   dir
                                      :label   dir
                                      :control (r/as-element [radio])}])]]
             [form-control {:component :fieldset}
              [form-label {:component :legend} "justify-content"]
              [radio-group {:name      "justify-content"
                            :value     justify-content
                            :on-change set-justify
                            :row       true}
               (for [just justify-content-values]
                 [form-control-label {:key     just
                                      :value   just
                                      :label   just
                                      :control (r/as-element [radio])}])]]
             [form-control {:component :fieldset}
              [form-label {:component :legend} "align-items"]
              [radio-group {:name      "align-items"
                            :value     align-items
                            :on-change set-align
                            :row       true}
               (for [align align-items-values]
                 [form-control-label {:key     align
                                      :value   align
                                      :label   align
                                      :control (r/as-element [radio])}])]]]]]
          [item {:sx {:text-align :left
                      :p          2}}
           [:pre {:sx {:p 2}}code]]]]])))
  {:spacing         1
   :direction       "row"
   :justify-content "center"
   :align-items     "center"}
  {:inspect-data true})

(defcard "## System

As a CSS utility component, the Stack supports all [system
properties](https://mui.com/system/properties/). You can use them as props
directly on the component. For instance, `margin-top`:

```
[stack {:mt 2} ...]
```
")
