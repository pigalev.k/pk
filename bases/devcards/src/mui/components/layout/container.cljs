(ns mui.components.layout.container
  (:require
   [devcards.core :as dc]
   [mui.common :refer [example]]
   [reagent.core :as r]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.container :refer [container]]
   [reagent-mui.material.css-baseline :refer [css-baseline]])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Container

Centers your content horizontally. It's the most basic layout element.

While containers can be nested, most layouts do not require a nested container.

Documentation:
- [Container](https://mui.com/material-ui/react-container/)

Component APIs:
- [Container](https://mui.com/material-ui/api/container/): `[reagent-mui.material.container :refer] [container]`")

(defcard "## Fluid

A fluid container's width is bounded by the `:max=width` prop value:"
  (r/as-element
   [example
    [:<>
     [css-baseline]
     [container {:max-width :sm
                 :sx        {:bgcolor :white}}
      [box {:sx {:bgcolor "#cfe8fc" :height 300}}]]]]))


(defcard "## Fixed

If you prefer to design for a fixed set of sizes instead of trying to
accommodate a fully fluid viewport, you can set the `:fixed` prop. The
`max-width` then matches the `min-width` of the current breakpoint."
  (r/as-element
   [example
    [:<>
     [css-baseline]
     [container {:fixed true
                 :sx {:bgcolor :white}}
      [box {:sx {:bgcolor "#cfe8fc" :height 300}}]]]]))
