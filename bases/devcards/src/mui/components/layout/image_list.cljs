(ns mui.components.layout.image-list
  (:require
   [devcards.core :as dc]
   [mui.common :refer [example]]
   [reagent.core :as r]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.icon-button :refer [icon-button]]
   [reagent-mui.icons.auto-awesome :refer [auto-awesome]]
   [reagent-mui.icons.info :refer [info]]
   [reagent-mui.icons.star-border :refer [star-border]]
   [reagent-mui.material.image-list :refer [image-list]]
   [reagent-mui.material.image-list-item :refer [image-list-item]]
   [reagent-mui.material.image-list-item-bar :refer [image-list-item-bar]]
   [reagent-mui.material.list-subheader :refer [list-subheader]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard "# Image List

Displays a collection of images in an organized grid.

Documentation:
- [ImageList](https://mui.com/material-ui/react-image-list/)

Component APIs:
- [ImageList](https://mui.com/material-ui/api/image-list/): `[reagent-mui.material.image-list :refer [image-list]]`
- [ImageListItem](https://mui.com/material-ui/api/image-list-item/): `[reagent-mui.material.image-list-item :refer [image-list-item]]`
- [ImageListItemBar](https://mui.com/material-ui/api/image-list-item-bar/): `[reagent-mui.material.image-list-item-bar :refer [image-list-item-bar]]`")

(def images
  [{:img   "https://images.unsplash.com/photo-1551963831-b3b1ca40c98e",
    :title "Breakfast"}
   {:img   "https://images.unsplash.com/photo-1551782450-a2132b4ba21d",
    :title "Burger"}
   {:img   "https://images.unsplash.com/photo-1522770179533-24471fcdba45",
    :title "Camera"}
   {:img   "https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c",
    :title "Coffee"}
   {:img   "https://images.unsplash.com/photo-1533827432537-70133748f5c8",
    :title "Hats"}
   {:img   "https://images.unsplash.com/photo-1558642452-9d2a7deb7f62",
    :title "Honey"}
   {:img   "https://images.unsplash.com/photo-1516802273409-68526ee1bdd6",
    :title "Basketball"}
   {:img   "https://images.unsplash.com/photo-1518756131217-31eb79b20e8f",
    :title "Fern"}
   {:img   "https://images.unsplash.com/photo-1597645587822-e99fa5d45d25",
    :title "Mushrooms"}
   {:img   "https://images.unsplash.com/photo-1567306301408-9b74779a11af",
    :title "Tomato basil"}
   {:img   "https://images.unsplash.com/photo-1471357674240-e1a485acb3e1",
    :title "Sea star"}
   {:img   "https://images.unsplash.com/photo-1589118949245-7d38baf380d6",
    :title "Bike"}])

(def quilted-images
  [{:img   "https://images.unsplash.com/photo-1551963831-b3b1ca40c98e",
    :title "Breakfast",
    :rows  2,
    :cols  2}
   {:img   "https://images.unsplash.com/photo-1551782450-a2132b4ba21d",
    :title "Burger"}
   {:img   "https://images.unsplash.com/photo-1522770179533-24471fcdba45",
    :title "Camera"}
   {:img   "https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c",
    :title "Coffee",
    :cols  2}
   {:img   "https://images.unsplash.com/photo-1533827432537-70133748f5c8",
    :title "Hats",
    :cols  2}
   {:img    "https://images.unsplash.com/photo-1558642452-9d2a7deb7f62",
    :title  "Honey",
    :author "@arwinneil",
    :rows   2,
    :cols   2}
   {:img   "https://images.unsplash.com/photo-1516802273409-68526ee1bdd6",
    :title "Basketball"}
   {:img   "https://images.unsplash.com/photo-1518756131217-31eb79b20e8f",
    :title "Fern"}
   {:img   "https://images.unsplash.com/photo-1597645587822-e99fa5d45d25",
    :title "Mushrooms",
    :rows  2,
    :cols  2}
   {:img   "https://images.unsplash.com/photo-1567306301408-9b74779a11af",
    :title "Tomato basil"}
   {:img   "https://images.unsplash.com/photo-1471357674240-e1a485acb3e1",
    :title "Sea star"}
   {:img   "https://images.unsplash.com/photo-1589118949245-7d38baf380d6",
    :title "Bike",
    :cols  2}])

(def woven-images
  [{:img   "https://images.unsplash.com/photo-1549388604-817d15aa0110",
    :title "Bed"}
   {:img   "https://images.unsplash.com/photo-1563298723-dcfebaa392e3",
    :title "Kitchen"}
   {:img   "https://images.unsplash.com/photo-1523413651479-597eb2da0ad6",
    :title "Sink"}
   {:img   "https://images.unsplash.com/photo-1525097487452-6278ff080c31",
    :title "Books"}
   {:img   "https://images.unsplash.com/photo-1574180045827-681f8a1a9622",
    :title "Chairs"}
   {:img   "https://images.unsplash.com/photo-1597262975002-c5c3b14bbd62",
    :title "Candle"}
   {:img   "https://images.unsplash.com/photo-1530731141654-5993c3016c77",
    :title "Laptop"}
   {:img   "https://images.unsplash.com/photo-1481277542470-605612bd2d61",
    :title "Doors"}
   {:img   "https://images.unsplash.com/photo-1517487881594-2787fef5ebf7",
    :title "Coffee"}
   {:img   "https://images.unsplash.com/photo-1516455207990-7a41ce80f7ee",
    :title "Storage"}
   {:img   "https://images.unsplash.com/photo-1519710164239-da123dc03ef4",
    :title "Coffee table"}
   {:img   "https://images.unsplash.com/photo-1588436706487-9d55d73a39e3",
    :title "Blinds"}])

(def masonry-images
  [{:img   "https://images.unsplash.com/photo-1549388604-817d15aa0110",
    :title "Bed"}
   {:img   "https://images.unsplash.com/photo-1525097487452-6278ff080c31",
    :title "Books"}
   {:img   "https://images.unsplash.com/photo-1523413651479-597eb2da0ad6",
    :title "Sink"}
   {:img   "https://images.unsplash.com/photo-1563298723-dcfebaa392e3",
    :title "Kitchen"}
   {:img   "https://images.unsplash.com/photo-1588436706487-9d55d73a39e3",
    :title "Blinds"}
   {:img   "https://images.unsplash.com/photo-1574180045827-681f8a1a9622",
    :title "Chairs"}
   {:img   "https://images.unsplash.com/photo-1530731141654-5993c3016c77",
    :title "Laptop"}
   {:img   "https://images.unsplash.com/photo-1481277542470-605612bd2d61",
    :title "Doors"}
   {:img   "https://images.unsplash.com/photo-1517487881594-2787fef5ebf7",
    :title "Coffee"}
   {:img   "https://images.unsplash.com/photo-1516455207990-7a41ce80f7ee",
    :title "Storage"}
   {:img   "https://images.unsplash.com/photo-1597262975002-c5c3b14bbd62",
    :title "Candle"}
   {:img   "https://images.unsplash.com/photo-1519710164239-da123dc03ef4",
    :title "Coffee table"}])

(def titlebar-images
  [{:img      "https://images.unsplash.com/photo-1551963831-b3b1ca40c98e",
    :title    "Breakfast",
    :author   "@bkristastucchio",
    :rows     2,
    :cols     2,
    :featured true}
   {:img    "https://images.unsplash.com/photo-1551782450-a2132b4ba21d",
    :title  "Burger",
    :author "@rollelflex_graphy726"}
   {:img    "https://images.unsplash.com/photo-1522770179533-24471fcdba45",
    :title  "Camera",
    :author "@helloimnik"}
   {:img    "https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c",
    :title  "Coffee",
    :author "@nolanissac",
    :cols   2}
   {:img    "https://images.unsplash.com/photo-1533827432537-70133748f5c8",
    :title  "Hats",
    :author "@hjrc33",
    :cols   2}
   {:img      "https://images.unsplash.com/photo-1558642452-9d2a7deb7f62",
    :title    "Honey",
    :author   "@arwinneil",
    :rows     2,
    :cols     2,
    :featured true}
   {:img    "https://images.unsplash.com/photo-1516802273409-68526ee1bdd6",
    :title  "Basketball",
    :author "@tjdragotta"}
   {:img    "https://images.unsplash.com/photo-1518756131217-31eb79b20e8f",
    :title  "Fern",
    :author "@katie_wasserman"}
   {:img    "https://images.unsplash.com/photo-1597645587822-e99fa5d45d25",
    :title  "Mushrooms",
    :author "@silverdalex",
    :rows   2,
    :cols   2}
   {:img    "https://images.unsplash.com/photo-1567306301408-9b74779a11af",
    :title  "Tomato basil",
    :author "@shelleypauls"}
   {:img    "https://images.unsplash.com/photo-1471357674240-e1a485acb3e1",
    :title  "Sea star",
    :author "@peterlaster"}
   {:img    "https://images.unsplash.com/photo-1589118949245-7d38baf380d6",
    :title  "Bike",
    :author "@southside_customs",
    :cols   2}])

(def custom-images
  [{:img      "https://images.unsplash.com/photo-1551963831-b3b1ca40c98e",
    :title    "Breakfast",
    :author   "@bkristastucchio",
    :featured true}
   {:img    "https://images.unsplash.com/photo-1551782450-a2132b4ba21d",
    :title  "Burger",
    :author "@rollelflex_graphy726"}
   {:img    "https://images.unsplash.com/photo-1522770179533-24471fcdba45",
    :title  "Camera",
    :author "@helloimnik"}
   {:img    "https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c",
    :title  "Coffee",
    :author "@nolanissac"}
   {:img    "https://images.unsplash.com/photo-1533827432537-70133748f5c8",
    :title  "Hats",
    :author "@hjrc33"}
   {:img      "https://images.unsplash.com/photo-1558642452-9d2a7deb7f62",
    :title    "Honey",
    :author   "@arwinneil",
    :featured true}
   {:img    "https://images.unsplash.com/photo-1516802273409-68526ee1bdd6",
    :title  "Basketball",
    :author "@tjdragotta"}
   {:img    "https://images.unsplash.com/photo-1518756131217-31eb79b20e8f",
    :title  "Fern",
    :author "@katie_wasserman"}
   {:img    "https://images.unsplash.com/photo-1597645587822-e99fa5d45d25",
    :title  "Mushrooms",
    :author "@silverdalex"}
   {:img    "https://images.unsplash.com/photo-1567306301408-9b74779a11af",
    :title  "Tomato basil",
    :author "@shelleypauls"}
   {:img    "https://images.unsplash.com/photo-1471357674240-e1a485acb3e1",
    :title  "Sea star",
    :author "@peterlaster"}
   {:img    "https://images.unsplash.com/photo-1589118949245-7d38baf380d6",
    :title  "Bike",
    :author "@southside_customs"}])

(defcard "## Standard image list

Standard image lists are best for items of equal importance. They have a uniform
container size, ratio, and spacing.

```
[image-list {:sx         {:width 500 :height 450}
             :cols       3
             :row-height 164}
 (for [image images
       :let  [src (:img image)
              alt (:alt image)]]
   [image-list-item {:key (:img image)}
    [:img
     {:src     (str src \"?w=164&h=164&fit=crop&auto=format\")
      :src-set (str src \"?w=164&h=164&fit=crop&auto=format&dpr=2 2x\")
      :alt     alt
      :loading :lazy}]])]
```
"
  (r/as-element
   [example {:sx {:display         :flex
                  :justify-content :center}}
    [image-list {:sx         {:width 500 :height 450}
                 :cols       3
                 :row-height 164}
     (for [image images
           :let  [src (:img image)
                  alt (:alt image)]]
       [image-list-item {:key src}
        [:img
         {:src     (str src "?w=164&h=164&fit=crop&auto=format")
          :src-set (str src "?w=164&h=164&fit=crop&auto=format&dpr=2 2x")
          :alt     alt
          :loading :lazy}]])]]))

(defcard "## Quilted image list

Quilted image lists emphasize certain items over others in a collection. They
create hierarchy using varied container sizes and ratios."
  (let [src-base (fn [{:keys [img size cols rows]
                       :or   {size 121 cols 1 rows 1}}]
                   (str img "?w=" (* size cols)
                        "&h=" (* size rows)))
        src      (fn [src-base]
                   (str src-base "&fit=crop&auto=format"))
        src-set  (fn [src-base]
                   (str src-base "&fit=crop&auto=format&dpr=2 2x"))
        cols (fn [{:keys [cols] :or {cols 1}}] cols)
        rows (fn [{:keys [rows] :or {rows 1}}] rows)]
    (r/as-element
     [example {:sx {:display         :flex
                    :justify-content :center}}
      [image-list {:variant    :quilted
                   :sx         {:width 500 :height 450}
                   :cols       4
                   :row-height 121}
       (for [image quilted-images
             :let  [src-base-string (src-base image)
                    src-string (src src-base-string)
                    src-set-string (src-set src-base-string)
                    alt (:alt image)]]
         [image-list-item {:key src-base-string
                           :cols (cols image)
                           :rows (rows image)}
          [:img
           {:src     src-string
            :src-set src-set-string
            :alt     alt
            :loading :lazy}]])]])))

(defcard "## Woven image list

Woven image lists use alternating container ratios to create a rhythmic
layout. A woven image list is best for browsing peer content."
  (r/as-element
   [example {:sx {:display         :flex
                  :justify-content :center}}
    [image-list {:sx      {:width 500 :height 450}
                 :variant :woven
                 :cols    3
                 :gap     8}
     (for [image woven-images
           :let  [src (:img image)
                  alt (:title image)]]
       [image-list-item {:key src}
        [:img
         {:src     (str src "?w=161&fit=crop&auto=format")
          :src-set (str src "?w=161&fit=crop&auto=format&dpr=2 2x")
          :alt     alt
          :loading :lazy}]])]]))

(defcard "## Masonry image list

Masonry image lists use dynamically sized container heights that reflect the
aspect ratio of each image. This image list is best used for browsing uncropped
peer content."
  (r/as-element
   [example {:sx {:display         :flex
                  :justify-content :center}}
    [box {:sx {:width 500 :height 450 :overflow-y :scroll}}
     [image-list {:variant :masonry
                  :cols    3
                  :gap     8}
      (for [image masonry-images
            :let  [src (:img image)
                   alt (:title image)]]
        [image-list-item {:key src}
         [:img
          {:src     (str src "?w=248&fit=crop&auto=format")
           :src-set (str src "?w=248&fit=crop&auto=format&dpr=2 2x")
           :alt     alt
           :loading :lazy}]])]]]))

(defcard "## Image list with title bars

This example demonstrates the use of the `image-list-item-bar` to add an overlay
to each item. The overlay can accommodate a `:title`, `:subtitle` and secondary
action --- in this example an `icon-button`."
  (r/as-element
   [example {:sx {:display         :flex
                  :justify-content :center}}
    [image-list {:sx {:width 500 :height 450}}
     [image-list-item {:key "Subheader" :cols 2}
      [list-subheader {:component :div} "December"]]
     (for [image titlebar-images
           :let  [src (:img image)
                  alt (:title image)
                  author (:author image)
                  featured? (:featured image)]]
       [image-list-item {:key src}
        [:img
         {:src     (str src "?w=248&fit=crop&auto=format")
          :src-set (str src "?w=248&fit=crop&auto=format&dpr=2 2x")
          :alt     alt
          :loading :lazy}]
        [image-list-item-bar
         {:title       alt
          :subtitle    author
          :action-icon (r/as-element
                        [:<>
                         (when featured?
                           [icon-button
                            {:sx         {:color :white}
                             :aria-label (str "Featured by" author)}
                            [auto-awesome]])
                         [icon-button
                          {:sx         {:color "rgba(255, 255, 255, 0.54)"}
                           :aria-label (str "Info about " alt)}
                          [info]]])}]])]]))

(defcard "## Title bar below image (standard)

The title bar can be placed below the image."
  (r/as-element
   [example {:sx {:display         :flex
                  :justify-content :center}}
    [image-list {:sx {:width 500 :height 450}}
     (for [image titlebar-images
           :let  [src (:img image)
                  alt (:title image)
                  author (:author image)
                  featured? (:featured image)]]
       [image-list-item {:key src}
        [:img
         {:src     (str src "?w=248&fit=crop&auto=format")
          :src-set (str src "?w=248&fit=crop&auto=format&dpr=2 2x")
          :alt     alt
          :loading :lazy}]
        [image-list-item-bar
         {:sx {:color (when featured? "success.main")}
          :title       alt
          :subtitle    (r/as-element [:span "By: " author])
          :position    :below}]])]]))

(defcard "## Custom image list

In this example the items have a customized titlebar, positioned at the top and
with a custom gradient `:title-background`. The secondary action `icon-button`
is positioned on the left. The `:gap` prop is used to adjust the gap between
items."
  (let [cols     (fn [{:keys [featured]}] (if featured 2 1))
        rows     (fn [{:keys [featured]}] (if featured 2 1))
        src-base (fn [{:keys [img] :as image} width height]
                   (str img "?w=" (* width (cols image))
                        "&h=" (* height (rows image))))
        src      (fn [src-base]
                   (str src-base "&fit=crop&auto=format"))
        src-set  (fn [src-base]
                   (str src-base "&fit=crop&auto=format&dpr=2 2x"))]
    (r/as-element
     [example {:sx {:display         :flex
                    :justify-content :center}}
      [image-list {:sx         {:width     500
                                :height    450
                                :transform "translateZ(0)"}
                   :row-height 200
                   :gap        1}
       (for [image custom-images
             :let  [cols-number (cols image)
                    rows-number (rows image)
                    src-base-string (src-base image 250 200)
                    src-string (src src-base-string)
                    src-set-string (src-set src-base-string)
                    img (:img image)
                    alt (:title image)]]
         [image-list-item {:key  img
                           :cols cols-number
                           :rows rows-number}
          [:img
           {:src     src-string
            :src-set src-set-string
            :alt     alt
            :loading :lazy}]
          [image-list-item-bar
           {:sx              {:background
                              (str
                               "linear-gradient(to bottom, rgba(0,0,0,0.7) 0%,"
                               "rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)")}
            :title           alt
            :position        :top
            :action-position :left
            :action-icon     (r/as-element
                          [icon-button
                           {:sx         {:color :white}
                            :aria-label (str "star " alt)}
                           [star-border]])}]])]])))
