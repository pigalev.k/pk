(ns mui.components.layout.box
  (:require
   [devcards.core :as dc]
   [mui.common :refer [example]]
   [reagent.core :as r]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.button :refer [button]])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Box

Serves as a wrapper component for most of the CSS utility needs.

Documentation:
- [Box](https://mui.com/material-ui/react-box/)

Component APIs:
- [Box](https://mui.com/material-ui/api/box/): `[reagent-mui.material.box :refer [box]]`")

(defcard "## The `:sx` prop

All system properties are available via the `:sx` prop. In addition, the `:sx`
prop allows you to specify any other CSS rules you may need. Here's an example
of how you can use it:"
  (r/as-element
   [example {:sx {:display         :flex
                  :justify-content :center}}
    [box {:sx {:width            200
               :height           200
               :background-color "primary.dark"
               "&:hover"         {:background-color "primary.main"
                                  :opacity          [0.9, 0.8, 0.7]}}}]]))

(defcard "## Overriding MUI components

The `box` component wraps your component. It creates a new DOM element, a
`<div>` by default (can be changed with the `:component` prop). Let's say you
want to use a `<span>` instead:

```
[box {:component :span
      :sx        {:p 2 :border \"1px dashed grey\" :background-color :white}}
 [button \"Save\"]]
```
"
  (r/as-element
   [example {:sx {:display         :flex
                  :justify-content :center}}
    [box {:component :span
          :sx        {:p 2 :border "1px dashed grey" :background-color :white}}
     [button "Save"]]]))

(defcard
  "This works great when the changes can be isolated to a new DOM element. For instance, you can change the margin this way.

However, sometimes you have to target the underlying DOM element. As an example,
you may want to change the border of the Button. The Button component defines
its own styles. CSS inheritance doesn't help. To workaround the problem, you can
use the `:sx` prop directly on the child if it is a MUI component.

```
[button {:sx {:p 2 :border \"1px dashed grey\"}} \"Save\"]
```

For non-MUI components, use the `:component` prop.

```
[box {:component :button
      :sx        {:p 2 :border \"1px dashed grey\"}} \"Save\"]
```
")

(defcard "## System

As a CSS utility component, `box` also supports all [system
properties](https://mui.com/system/properties/). You can use them directly on
the component. For instance, `:margin-top`:

```
[box {:mt 2} ...]
```
")
