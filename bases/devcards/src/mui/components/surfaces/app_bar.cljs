(ns mui.components.surfaces.app-bar
  (:refer-clojure :exclude [list])
  (:require
   [devcards.core :as dc]
   [mui.common :refer [example]]
   [reagent-mui.icons.account-circle :refer [account-circle]]
   [reagent-mui.icons.adb :refer [adb]]
   [reagent-mui.icons.menu :refer [menu] :rename {menu menu-icon}]
   [reagent-mui.icons.search :refer [search] :rename {search search-icon}]
   [reagent-mui.material.app-bar :refer [app-bar]]
   [reagent-mui.material.avatar :refer [avatar]]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.button :refer [button]]
   [reagent-mui.material.container :refer [container]]
   [reagent-mui.material.divider :refer [divider]]
   [reagent-mui.material.drawer :refer [drawer]]
   [reagent-mui.material.form-control-label :refer [form-control-label]]
   [reagent-mui.material.form-group :refer [form-group]]
   [reagent-mui.material.icon-button :refer [icon-button]]
   [reagent-mui.material.input-base :refer [input-base]]
   [reagent-mui.material.list :refer [list]]
   [reagent-mui.material.list-item :refer [list-item]]
   [reagent-mui.material.list-item-button :refer [list-item-button]]
   [reagent-mui.material.list-item-text :refer [list-item-text]]
   [reagent-mui.material.menu :refer [menu]]
   [reagent-mui.material.menu-item :refer [menu-item]]
   [reagent-mui.material.scoped-css-baseline :refer [scoped-css-baseline]]
   [reagent-mui.material.switch :refer [switch]]
   [reagent-mui.material.toolbar :refer [toolbar]]
   [reagent-mui.material.tooltip :refer [tooltip]]
   [reagent-mui.material.typography :refer [typography]]
   [reagent-mui.styles :refer [styled]]
   [reagent.core :as r])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# App Bar

Displays information and actions relating to the current screen.

The top App bar provides content and actions related to the current screen. It's
used for branding, screen titles, navigation, and actions.

Documentation:
- [AppBar](https://mui.com/material-ui/react-app-bar/)

Component APIs:
- [AppBar](https://mui.com/material-ui/api/app-bar/): `[reagent-mui.material.app-bar :refer [app-bar]]`
- [Menu](https://mui.com/material-ui/api/menu/): `[reagent-mui.material.menu :refer [menu]]`
- [Toolbar](https://mui.com/material-ui/api/toolbar/): `[reagent-mui.material.toolbar :refer [toolbar]]`")

(defcard "## Basic App bar"
  (r/as-element
   [example
    [box {:sx {:flex-grow 1}}
     [app-bar {:position :static}
      [toolbar
       [icon-button {:size       :large
                     :edge       :start
                     :color      :inherit
                     :aria-label "menu"
                     :sx         {:mr 2}}
        [menu-icon]]
       [typography {:variant   :h6
                    :component :div
                    :sx {:flex-grow 1}}
        "News"]
       [button {:color :inherit} "Login"]]]]]))

(defcard "## App bar with menu"
  (fn [data-atom _]
    (let [logout?       (:logout @data-atom)
          toggle-logout (fn [_] (swap! data-atom update :logout not))
          menu-target   (:menu-target @data-atom)
          menu-position (clj->js {:vertical :top :horizontal :right})
          show-menu     (fn [event]
                          (swap! data-atom
                                 assoc :menu-target (.-currentTarget event)))
          hide-menu     (fn [_] (swap! data-atom assoc :menu-target nil))
          logout-switch [switch {:checked    logout?
                                 :on-change  toggle-logout
                                 :aria-label "login switch"}]
          profile-menu  [:div
                         [icon-button {:size          :large
                                       :aria-label    "account of current user"
                                       :aria-controls "menu-appbar"
                                       :aria-haspopup "true"
                                       :on-click      show-menu
                                       :color         :inherit}
                          [account-circle]]
                         [menu {:id               "menu-appbar"
                                :anchor-el        menu-target
                                :anchor-origin    menu-position
                                :keep-mounted     true
                                :transform-origin menu-position
                                :open             (boolean menu-target)
                                :on-close         hide-menu}
                          [menu-item {:on-click hide-menu} "Profile"]
                          [menu-item {:on-click hide-menu} "My Account"]]]]
      (r/as-element
       [example
        [box {:sx {:flex-grow 1}}
         [form-group
          [form-control-label {:control (r/as-element logout-switch)
                               :label   (if logout? "Logout" "Login")}]]
         [app-bar {:position :static}
          [toolbar
           [icon-button {:size       :large
                         :edge       :start
                         :color      :inherit
                         :aria-label "menu"
                         :sx         {:mr 2}}
            [menu-icon]]
           [typography {:variant   :h6
                        :component :div
                        :sx        {:flex-grow 1}}
            "Photos"]
           (when logout? profile-menu)]]]])))
  {:logout      true
   :menu-target nil})

(defcard "## App bar with responsive menu

Resize the browser window to see the effect."
  (fn [data-atom _]
    (let [p-menu-tgt  (:profile-menu-target @data-atom)
          p-menu-pos  (clj->js {:vertical :top :horizontal :right})
          show-p-menu (fn [event]
                        (swap! data-atom assoc
                               :profile-menu-target (.-currentTarget event)))
          hide-p-menu (fn [_] (swap! data-atom assoc
                                     :profile-menu-target nil))
          p-menu      [:div
                       [tooltip {:title "Open settings"}
                        [icon-button {:size          :large
                                      :aria-label    "account of current user"
                                      :aria-controls "menu-appbar"
                                      :aria-haspopup "true"
                                      :on-click      show-p-menu
                                      :color         :inherit
                                      :sx            {:p 0}}
                         [avatar {:src "/img/avatar.jpg"}]]]
                       [menu {:id               "profile-menu-appbar"
                              :anchor-el        p-menu-tgt
                              :anchor-origin    p-menu-pos
                              :keep-mounted     true
                              :transform-origin p-menu-pos
                              :open             (boolean p-menu-tgt)
                              :on-close         hide-p-menu}
                        [menu-item {:on-click hide-p-menu} "Profile"]
                        [menu-item {:on-click hide-p-menu} "Account"]
                        [menu-item {:on-click hide-p-menu} "Dashboard"]
                        [menu-item {:on-click hide-p-menu} "Logout"]]]
          n-menu-tgt  (:nav-menu-target @data-atom)
          n-menu-pos  (clj->js {:vertical :top :horizontal :left})
          show-n-menu (fn [event]
                        (swap! data-atom
                               assoc :nav-menu-target (.-currentTarget event)))
          hide-n-menu (fn [_] (swap! data-atom assoc :nav-menu-target nil))
          n-menu-xs   [box {:sx {:flex-grow 1
                                 :display   {:xs :flex
                                             :md :none}}}
                       [icon-button {:size          :large
                                     :edge          :start
                                     :aria-label    "navigation"
                                     :aria-controls "menu-appbar"
                                     :aria-haspopup "true"
                                     :on-click      show-n-menu
                                     :color         :inherit
                                     :sx            {:mr 2}}
                        [menu-icon]]
                       [menu {:id               "nav-menu-appbar"
                              :anchor-el        n-menu-tgt
                              :anchor-origin    n-menu-pos
                              :keep-mounted     true
                              :transform-origin n-menu-pos
                              :open             (boolean n-menu-tgt)
                              :on-close         hide-n-menu
                              :sx               {:display {:xs :flex
                                                           :md :none}}}
                        [menu-item {:on-click hide-p-menu} "Products"]
                        [menu-item {:on-click hide-p-menu} "Pricing"]
                        [menu-item {:on-click hide-p-menu} "Blog"]]]
          n-menu-md   [box {:sx {:flex-grow 1
                                 :display   {:xs :none
                                             :md :flex}}}
                       [button {:on-click hide-p-menu
                                :sx       {:my      2
                                           :color   :white
                                           :display :block}} "Products"]
                       [button {:on-click hide-p-menu
                                :sx       {:my      2
                                           :color   :white
                                           :display :block}} "Pricing"]
                       [button {:on-click hide-p-menu
                                :sx       {:my      2
                                           :color   :white
                                           :display :block}} "Blog"]]
          title-xs    [:<>
                       [adb {:sx {:display {:xs :flex :md :none}
                                  :mr      1}}]
                       [typography {:variant   :h5
                                    :no-wrap   true
                                    :component :a
                                    :href      "/"
                                    :sx        {:mr              2
                                                :display
                                                {:xs :flex :md :none}
                                                :flex-grow       1
                                                :font-family     :monospace
                                                :font-weight     700
                                                :letter-spacing  ".3rem"
                                                :color
                                                "white !important"
                                                :text-decoration :none}}
                        "LOGO"]]
          title-md    [:<>
                       [adb {:sx {:display {:xs :none :md :flex}
                                  :mr      1}}]
                       [typography {:variant   :h6
                                    :no-wrap   true
                                    :component :a
                                    :href      "/"
                                    :sx        {:mr              2
                                                :display
                                                {:xs :none :md :flex}
                                                :font-family     :monospace
                                                :font-weight     700
                                                :letter-spacing  ".3rem"
                                                :color
                                                "white !important"
                                                :text-decoration :none}}
                        "LOGO"]]]
      (r/as-element
       [example
        [box {:sx {:flex-grow 1}}
         [app-bar {:position :static}
          [container {:max-width :xl}
           [toolbar {:disable-gutters true}
            n-menu-xs
            title-md
            title-xs
            n-menu-md
            p-menu]]]]])))
  {:profile-menu-target nil
   :nav-menu-target     nil})


(def search
  "A search field for application bar."
  (styled "div"
          (fn [{{{:keys [border-radius]} :shape
                 {:keys [up]}            :breakpoints
                 spacing                 :spacing} :theme}]
            {:position         :relative
             :border-radius    border-radius
             :background-color "rgb(255,255,255,0.15)"
             "&:hover"
             {:background-color "rgb(255,255,255,0.25)"}
               :margin-left      0
             :width            "100%"
             (up "sm")         {:margin-left (spacing 1)
                                :width       :auto}})))

(def search-icon-wrapper
  "A wrapper for search icon in an app-bar search field."
  (styled "div" (fn [{{:keys [spacing]} :theme}]
                  {:position        :absolute
                   :height          "100%"
                   :padding         (spacing 0 2)
                   :pointer-events  :none
                   :display         :flex
                   :align-items     :center
                   :justify-content :center})))

(def search-input
  "A text input component for an app bar search field."
  (styled input-base
          (fn [{{spacing          :spacing
                 {create :create} :transitions
                 {up :up}         :breakpoints} :theme}]
            {:color :inherit
             "& .MuiInputBase-input"
             {:padding      (spacing 1 1 1 0)
              :padding-left (str "calc(1em + " (spacing 4) ")")
              :transition   (create "width")
              :width        "100%"
              (up "sm")     {:width    "12ch"
                             "&:focus" {:width "20ch"}}}})))

(defcard "## App bar with search field"
  (r/as-element
   [example
    [box {:sx {:flex-grow 1}}
     [app-bar {:position :static}
      [toolbar
       [icon-button {:size       :large
                     :edge       :start
                     :color      :inherit
                     :aria-label "open drawer"
                     :sx         {:mr 2}}
        [menu-icon]]
       [typography {:variant   :h6
                    :no-wrap   true
                    :component :div
                    :sx        {:flex-grow 1
                                :display {:xs :none
                                          :sm :block}}}
        "MUI"]
       [search
        [search-icon-wrapper
         [search-icon]]
        [search-input {:placeholder "Search..."}]]]]]]))


(def lorem-ipsum
  "Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique unde fugit
   veniam eius, perspiciatis sunt? Corporis qui ducimus quibusdam, aliquam
   dolore excepturi quae. Distinctio enim at eligendi perferendis in cum
   quibusdam sed quae, accusantium et aperiam? Quod itaque exercitationem, at ab
   sequi qui modi delectus quia corrupti alias distinctio nostrum.  Minima ex
   dolor modi inventore sapiente necessitatibus aliquam fuga et. Sed numquam
   quibusdam at officia sapiente porro maxime corrupti perspiciatis asperiores,
   exercitationem eius nostrum consequuntur iure aliquam itaque, assumenda et!
   Quibusdam temporibus beatae doloremque voluptatum doloribus soluta accusamus
   porro reprehenderit eos inventore facere, fugit, molestiae ab officiis illo
   voluptates recusandae. Vel dolor nobis eius, ratione atque soluta, aliquam
   fugit qui iste architecto perspiciatis. Nobis, voluptatem!  Cumque, eligendi
   unde aliquid minus quis sit debitis obcaecati error, delectus quo eius
   exercitationem tempore. Delectus sapiente, provident corporis dolorum
   quibusdam aut beatae repellendus est labore quisquam praesentium repudiandae
   non vel laboriosam quo ab perferendis velit ipsa deleniti modi! Ipsam, illo
   quod. Nesciunt commodi nihil corrupti cum non fugiat praesentium doloremque
   architecto laborum aliquid. Quae, maxime recusandae? Eveniet dolore molestiae
   dicta blanditiis est expedita eius debitis cupiditate porro sed aspernatur
   quidem, repellat nihil quasi praesentium quia eos, quibusdam
   provident. Incidunt tempore vel placeat voluptate iure labore, repellendus
   beatae quia unde est aliquid dolor molestias libero. Reiciendis similique
   exercitationem consequatur, nobis placeat illo laudantium! Enim perferendis
   nulla soluta magni error, provident repellat similique cupiditate ipsam, et
   tempore cumque quod! Qui, iure suscipit tempora unde rerum autem saepe nisi
   vel cupiditate iusto.  Illum, corrupti? Fugiat quidem accusantium
   nulla. Aliquid inventore commodi reprehenderit rerum reiciendis! Quidem alias
   repudiandae eaque eveniet cumque nihil aliquam in expedita, impedit quas
   ipsum nesciunt ipsa ullam consequuntur dignissimos numquam at nisi porro a,
   quaerat rem repellendus.  Voluptates perspiciatis, in pariatur impedit, nam
   facilis libero dolorem dolores sunt inventore perferendis, aut sapiente modi
   nesciunt.")

(defn main
  "A main component of the application with lots of text."
  [text]
  [box {:component :main
        :sx        {:p          3
                    :height     400
                    :overflow-y :scroll}}
   [box
    [typography text]]])

(defn nav-drawer
  "A navigation drawer for app bar."
  [{:keys [on-click items]}]
  [box {:on-click on-click
        :sx       {:text-align :center}}
   [typography {:variant :h6
                :sx      {:my 2}}
    "MUI"]
   [divider]
   [list
    (for [item items]
      [list-item {:key             item
                  :disable-padding true}
       [list-item-button {:sx {:text-align :center}}
        [list-item-text {:primary item}]]])]])

(defn nav-buttons
  "Navigation buttons for app bar."
  [{:keys [nav-items]}]
  [box {:sx {:display {:xs :none :sm :block}}}
   (for [item nav-items]
     [button {:key item
              :sx {:color :white}}
      item])])

(defn responsive-app-bar-with-drawer-example
  "An example of responsive app bar with a mobile drawer."
  [{:keys [drawer-width nav-items open-drawer close-drawer drawer-open?]}]
  [example {:id "resp-app-bar-with-drawer-example"}
   [box {:sx {:display    :flex
              :overflow-y :scroll}}
    [scoped-css-baseline
     [app-bar {:component :nav
               :position  :static}
      [toolbar
       [icon-button {:color      :inherit
                     :aria-label "open drawer"
                     :edge       :start
                     :on-click   open-drawer
                     :sx         {:mr      2
                                  :display {:sm :none}}}
        [menu-icon]]
       [typography {:variant   :h6
                    :component :div
                    :sx        {:flex-grow 1
                                :display   {:xs :none
                                            :sm :block}}}
        "MUI"]
       [nav-buttons {:nav-items nav-items}]]]
     [box {:component :nav}
      [drawer {:variant    :temporary
               :open       drawer-open?
               :on-close   close-drawer
               :ModalProps {:keep-mounted true
                            :container
                            (fn []
                              (let [el (.querySelector
                                        js/document
                                        "#resp-app-bar-with-drawer-example")]
                                (js/console.log el)
                                el))}
               :sx
               {:display             {:xs :block :sm :none}
                "& .MuiDrawer-paper" {:box-sizing :border-box
                                      :width      drawer-width}}}
       [nav-drawer {:items    nav-items
                    :on-click close-drawer}]]]
     [main lorem-ipsum]]]])

(defcard "## Responsive App bar with Drawer

Resize the browser window to see the effect.

TODO: make drawer fit example, not the whole viewport, preferably without
iframe"
  (fn [data-atom _]
    (let [example-props
          {:drawer-width 240
           :nav-items    ["Home" "About" "Contact"]
           :drawer-open? (:drawer-open? @data-atom)
           :open-drawer  (fn [_] (swap! data-atom assoc :drawer-open? true))
           :close-drawer (fn [_] (swap! data-atom assoc :drawer-open? false))}]
      (r/as-element
        [responsive-app-bar-with-drawer-example example-props])))
  {:drawer-open? false})
