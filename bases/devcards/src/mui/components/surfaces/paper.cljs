(ns mui.components.surfaces.paper
  (:require
   [devcards.core :as dc]
   [mui.common :refer [example item]]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.grid :refer [grid]]
   [reagent-mui.material.paper :refer [paper]]
   [reagent-mui.material.scoped-css-baseline :refer [scoped-css-baseline]]
   [reagent-mui.styles :refer [create-theme styled theme-provider]]
   [reagent.core :as r])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Paper

In Material Design, the physical properties of paper are translated to the
screen.

The background of an application resembles the flat, opaque texture of a sheet
of paper, and an application's behavior mimics paper's ability to be re-sized,
shuffled, and bound together in multiple sheets.

Documentation:
- [Paper](https://mui.com/material-ui/react-paper/)

Component APIs:
- [Paper](https://mui.com/material-ui/api/paper/): `[reagent-mui.material.paper :refer [paper]]`")


(defcard "## Basic paper

```
[paper]
[paper {:elevation 0}]
[paper {:elevation 3}]
```
"
  (r/as-element
   [example
    [box {:sx {:display          :flex
               :flex-wrap        :wrap
               "& > :not(style)" {:m      1
                                  :width  128
                                  :height 128}}}
     [paper]
     [paper {:elevation 0}]
     [paper {:elevation 3}]]]))

(defcard "## Variants

If you need an outlined surface, use the `:variant` prop.

```
 [paper]
 [paper {:variant :outlined}]
 [paper {:variant :outlined :square  true}]
```
"
  (r/as-element
   [example
    [box {:sx {:display          :flex
               :flex-wrap        :wrap
               :border-radius    1
               :background-color :white
               "& > :not(style)" {:m      1
                                  :width  128
                                  :height 128}}}
     [paper]
     [paper {:variant :outlined}]
     [paper {:variant :outlined
             :square  true}]]]))


(def a-paper
  "An example paper."
  (styled paper
          (fn [{{{{:keys [secondary]} :text} :palette
                 {:keys [body-2]}            :typography} :theme}]
            (merge body-2 {:text-align  :center
                           :color       secondary
                           :height      60
                           :line-height "60px"}))))

(defcard "## Elevation

The elevation can be used to establish a hierarchy between other content. In
practical terms, the elevation controls the size of the shadow applied to the
surface. In dark mode, raising the elevation also makes the surface lighter.

The change of shade in dark mode is done by applying a semi-transparent gradient
to the `:background-image` property. This can lead to confusion when overriding
the styles of `paper`, as setting just the `:background-color` property will not
affect the elevation-related shading. To ignore the shading and set the
background color that is not affected by elevation in dark mode, override the
`:background` property (or both `:background-color` and `:background-image`)."
  (let [dark-theme  (create-theme {:palette {:mode :dark}})
        light-theme (create-theme {:palette {:mode :light}})
        elevations  [0 1 2 3 4 6 8 12 16 24]]
    (r/as-element
     [example
      [grid {:container true
             :spacing   2}
       (for [[index theme] [[0 light-theme] [1 dark-theme]]]
         [grid {:item true :xs 6 :key index}
          [theme-provider theme
           [scoped-css-baseline
            [box {:sx {:p                     2
                       :bg-color              "background.default"
                       :display               :grid
                       :grid-template-columns {:md "1fr 1fr"}
                       :gap                   2}}
             (for [elevation elevations]
               [item {:key       elevation
                      :elevation elevation}
                (str ":elevation " elevation)])]]]])]])))
