(ns short-url-spa.core
  "Entry point for `short-url-spa` devcards build."
  (:require
   ;; devcards, its npm deps and fixes
   [devcards.core :as dc]
   [fixes]
   ;; other namespaces where devcards that you want to include in the build are
   ;; defined
   [short-url-spa.components.app-header]
   [short-url-spa.components.app-shell]
   [short-url-spa.pages.main])
  (:require-macros
   [devcards.core :refer [defcard]]))


(set! *warn-on-infer* true)


(defn init
  "Initialize devcards."
  []
  (dc/start-devcard-ui!))


(defcard "# URL Shortener SPA

A Reagent/re-frame frontend for the URL Shortener API service.")
