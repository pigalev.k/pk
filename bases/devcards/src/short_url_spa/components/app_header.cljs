(ns short-url-spa.components.app-header
  (:require
   [devcards.core :as dc]
   [mui.common :refer [example]]
   [pk.bases.short-url-spa.views.components.app-header :refer [app-header]]
   [reagent-mui.components :refer [box]]
   [reagent.core :as r])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Application header

An application header with a drawer button, title and profile menu."
  (r/as-element
   [example
    [box {:sx {:background-color :white}}
     [app-header {:title "Test"}]]]))
