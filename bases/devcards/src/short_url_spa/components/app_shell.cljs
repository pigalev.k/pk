(ns short-url-spa.components.app-shell
  (:require
   [devcards.core :as dc]
   [mui.common :refer [example]]
   [pk.bases.short-url-spa.views.components.app-shell :refer [app-shell]]
   [reagent-mui.components :refer [box]]
   [reagent.core :as r])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Application shell

Basic responsive layout for the application: application header and the content
area."
  (r/as-element
   [example
    [box {:sx {:background-color :white}}
     [app-shell {:header  [box {:sx {:width            "100%"
                                     :height           64
                                     :background-color :lightsteelblue
                                     :display          :flex
                                     :align-items      :center
                                     :justify-content  :center}}
                           "header"]
                 :drawer  nil
                 :content [box {:sx {:width            "100%"
                                     :height           200
                                     :background-color :powderblue
                                     :display          :flex
                                     :align-items      :center
                                     :justify-content  :center}}
                           "content"]}]]]))
