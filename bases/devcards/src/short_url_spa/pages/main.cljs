(ns short-url-spa.pages.main
  (:require
   [devcards.core :as dc]
   [mui.common :refer [example]]
   [pk.bases.short-url-spa.views.pages.main :refer [main]]
   [reagent-mui.components :refer [box]]
   [reagent.core :as r])
  (:require-macros
   [devcards.core :refer [defcard]]))


(defcard "# Main page

Application's main page with URL input field."
  (r/as-element
   [example
    [box {:sx {:background-color :white
               :p                1}}
     [main]]]))
