;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; configure default Cider cljs REPL
((nil . (;; (cider-shadow-default-options . "build/core")
         (cider-preferred-build-tool . shadow-cljs)
         (cider-default-cljs-repl . shadow)
         (cider-clojure-cli-aliases . ":dev:cljc")
         (cider-clojure-cli-parameters . ""))))
