# devcards

Live documentation and visual testing for UI components.

## Prepare

1. Install `node`
2. Install npm dependencies

```
yarn
```

## Develop

Source paths and dependencies are managed through `deps.edn`, npm dependencies
are specified in `package.json`, build configuration is in the
`shadow-cljs.edn`. See these files to learn more.

### Commands

Builds can be run by

- `yarn shadow-cljs [action] [build]*`
- `clojure -M:shadow-cljs [action] [build]*`

All examples will use the first command, modify as needed before using.

### Builds (sets of devcards)

There is currently one build (one set of devcards) --- `core`, more may be added
later.

All examples will use the `core` build, modify as needed before using.

### Build and watch for changes

```
yarn shadow-cljs watch core
```

In emacs, you can start a build (with watch/hot-reload) and attach a cljs REPL
to it by
- running `cider-jack-in-cljs` (`C-u C-c M-J`)
- selecting `shadow-cljs` as a command to run
- selecting `shadow` as a REPL type
- selecting your build (e.g. `:core`)

Default values for command, REPL type and build can be specified, see
examples in `.dir-locals.el`.

### Build once

```
yarn shadow-cljs compile core
```

### Build for production

```
yarn shadow-cljs release core
yarn shadow-cljs release core --debug
```

>Please note that bases should not be used to build deployable artifacts, create
>a project that uses the base instead.

### Faster builds

A build server may be started to reduce startup times of all build commands.

```
# foreground in another terminal, C-c to stop
yarn shadow-cljs server

# background
yarn shadow-cljs start
yarn shadow-cljs stop
yarn shadow-cljs restart
```

## Use

### Serve a build

As static files.

```
clojure -M:serve :port 4444 :dir "public/core"
```
