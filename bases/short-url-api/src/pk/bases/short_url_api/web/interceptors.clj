(ns pk.bases.short-url-api.web.interceptors
  (:require
   [clojure.spec.alpha :as s]
   [io.pedestal.http :as http]
   [io.pedestal.interceptor :as i]
   [io.pedestal.interceptor.helpers :as interceptor]
   [io.pedestal.interceptor.chain :as ic]
   [pk.bases.short-url-api.spec :as specs]
   [pk.bases.short-url-api.web.handlers :as h]
   [pk.components.alias.api :as a]
   [pk.components.alias-store.api :as sapi]
   [ring.util.mime-type :as ring-util-mime]
   [ring.util.response :as ring-response]
   [taoensso.timbre :as timbre]))


(defn resource-if-not-found-interceptor
  "Returns interceptor that returns a resource response when routing
  failed to resolve a route. Resource's content type is inferred from
  the file extension. Can be used with servers hosting SPAs (e.g., to
  serve index.html on unknown routes)."
  [resource]
  (interceptor/after
   ::resource-if-not-found
   (fn [context]
     (if-not (http/response? (:response context))
       (assoc context :response
              (-> resource
                  (ring-response/resource-response)
                  (ring-response/content-type
                   (ring-util-mime/ext-mime-type resource))))
       context))))

(defn terminate-with-bad-request
  "Terminates request handling in `context`.
  Removes all following interceptors from queue, logs the error using
  `explanation-data` and sets response to 400 Bad Request using
  `explanation`."
  [context explanation explanation-data]
  (timbre/errorf
   "bad request: invalid parameters --- '%s'" explanation-data)
  (assoc-in (ic/terminate context) [:response]
            (h/bad-request
             {:error
              {:message     "Invalid request parameters"
               :explanation explanation}})))

(defn conform-params
  "Conforms request parameters found in request map under `params-key`
  with `spec` and merges the result to `:params` key in request
  map. If parameters are invalid, terminates request handling and sets
  response to 400 Bad Request."
  [params-key spec]
  (i/interceptor
   {:name ::conform-params
    :enter
    (fn [context]
      (let [params           (get-in context [:request params-key])
            conformed-params (s/conform spec params)]
        (timbre/debugf
         "conforming request params --- %s %s to spec %s, ---> %s"
         params-key params spec conformed-params)
        (if (= ::s/invalid conformed-params)
          (let [explanation-data (s/explain-data spec params)
                explanation      (specs/explain-errors explanation-data)]
            (terminate-with-bad-request context explanation explanation-data))
          (update-in context [:request :params]
                     merge conformed-params))))}))

(defn store-get-all-aliases
  "Returns an interceptor that retrieves all existing aliases from the store and
  attaches them to the `:existing-aliases` key in the request map."
  [alias-store]
  (i/interceptor
   {:name ::store-get-all-aliases
    :enter
    (fn [context]
      (let [aliases  (sapi/retrieve-all alias-store)]
        (assoc-in context [:request :existing-aliases] aliases)))}))

(defn store-get-alias-by-short
  "Returns an interceptor that looks for the short string under the
  `:short` key in the `:params` of the request map, searches for
  existing alias in the store by this short string and, if found,
  attaches it to the `:existing-alias` key in the request map."
  [alias-store]
  (i/interceptor
   {:name ::store-get-alias-by-short
    :enter
    (fn [context]
      (if-let [short (get-in context [:request :params :short])]
        (if-let [existing-alias (sapi/retrieve-by-short alias-store short)]
          (assoc-in context [:request :existing-alias] existing-alias)
          context)
        context))}))

(defn store-get-alias-by-original
  "Returns an interceptor that looks for the original string under the
  `:original` key in the `:params` of the request map, searches the
  store for an existing alias by this original string and, if found, attaches
  it to the `:existing-alias` key in the request map."
  [alias-store]
  (i/interceptor
   {:name ::store-get-alias-by-original
    :enter
    (fn [context]
      (if-let [original (get-in context [:request :params :original])]
        (if-let [existing-alias (sapi/retrieve-by-original alias-store original)]
          (assoc-in context [:request :existing-alias] existing-alias)
          context)
        context))}))

(defn store-create-alias
  "Returns an interceptor that first looks for the existing alias under
  the `:existing-alias` key in the request map, and does nothing if it
  is found; else looks for original string under the `:original` key
  in the decoded body, if found, creates a new alias for this string,
  persists it in the store and attaches it to the `:created-alias` key
  in the request map."
  [alias-store alias-options]
  (i/interceptor
   {:name ::store-create-alias
    :enter
    (fn [context]
      (let [original       (get-in context [:request :params :original])
            existing-alias (get-in context [:request :existing-alias])]
        (cond
          existing-alias context
          original       (let [new-alias
                               (a/alias original alias-options)
                               _
                               (sapi/insert alias-store new-alias)
                               persisted-new-alias
                               (sapi/retrieve-by-original alias-store original)]
                           (assoc-in context [:request :created-alias]
                                     persisted-new-alias))
          :else context)))}))

(defn store-delete-alias
  "Returns an interceptor that looks for an existing alias under the
  `:existing-alias` key in the request map and, if found, deletes this alias
  from the store and attaches it to the `:deleted-alias` key in the request
  map."
  [alias-store]
  (i/interceptor
   {:name ::store-delete-alias
    :enter
    (fn [context]
      (if-let [existing-alias (get-in context [:request :existing-alias])]
        (do
          (sapi/delete alias-store (a/short existing-alias))
          (assoc-in context [:request :deleted-alias] existing-alias))
        context))}))

(defn store-delete-aliases
  "Returns an interceptor that looks for an array of alias short strings
  under the `:short` key in the `:params` of the request map and, if
  not empty, deletes aliases with these short strings from the store
  and attaches count of deleted aliases to the
  `:deleted-aliases-count` key in the request map."
  [alias-store]
  (i/interceptor
   {:name ::store-delete-aliases
    :enter
    (fn [context]
      (if-let [shorts (get-in context [:request :params :short])]
        (do
          (let [deleted-aliases-count (sapi/delete-all alias-store shorts)]
            (assoc-in context [:request :deleted-aliases-count]
                      deleted-aliases-count)))
        context))}))
