(ns pk.bases.short-url-api.web.core
  (:require
   [io.pedestal.http :as http]
   [pk.bases.short-url-api.web.interceptors :as i]))

(defn service-map
  "Constructs Pedestal server's `service-map` according to the service
  configuration."
  [server-options]
  (let [{:keys [type host port join? routes allowed-origins secure-headers
                file-path resource-path resource-if-not-found]} server-options]
    (cond-> {::http/type type
             ::http/host host
             ::http/port port
             ::http/join? join?
             ::http/routes routes
             ::http/allowed-origins allowed-origins
             ::http/file-path file-path
             ::http/resource-path resource-path}
      secure-headers        (assoc ::http/secure-headers secure-headers)
      resource-if-not-found (assoc ::http/not-found-interceptor
                                   (i/resource-if-not-found-interceptor
                                    resource-if-not-found)))))
