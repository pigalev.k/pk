(ns pk.bases.short-url-api.web.routes
  "Request handling pipeline:

  - Interceptors that have '-cofx' postfix in the name (coeffect handlers)
    collect data necessary for handler to handle the request (coeffects), and
    store them in request. Side-effecting.

  - Handlers are functions that produce response map from request (enriched
    with coeffects). Pure.

  - Interceptors that have '-fx' postfix in the name (effect handlers) apply
    desired effects (modify the response, write to log etc). Side-effecting."
  (:require
   [io.pedestal.http.route :as route]
   [io.pedestal.interceptor :as pi]
   [io.pedestal.interceptor.helpers :as ih]
   [muuntaja.interceptor :as mi]
   [pk.bases.short-url-api.spec]
   [pk.bases.short-url-api.web.handlers :as h]
   [pk.bases.short-url-api.web.interceptors :as i]))


(defn routes
  "Returns a set of endpoints that should be served by the service."
  [{:keys [alias-store alias-options]}]
  (let [;; interceptors
        store-get-alias-by-short-cofx
        (i/store-get-alias-by-short alias-store)
        store-get-alias-by-original-cofx
        (i/store-get-alias-by-original alias-store)
        store-get-all-aliases-cofx
        (i/store-get-all-aliases alias-store)
        store-create-alias-fx
        (i/store-create-alias alias-store alias-options)
        store-delete-alias-fx
        (i/store-delete-alias alias-store)
        store-delete-aliases-fx
        (i/store-delete-aliases alias-store)
        conform-params-create-alias-cofx
        (i/conform-params :body-params
                          :request.params/create-alias)
        conform-params-get-alias-by-short-cofx
        (i/conform-params :path-params
                          :request.params/get-alias-by-short)
        conform-params-delete-aliases-cofx
        (i/conform-params :query-params
                          :request.params/delete-aliases)
        ;; handlers
        create-or-get-alias
        (ih/handler ::create-or-get-alias h/create-or-get-alias)
        get-alias
        (ih/handler ::get-alias h/get-alias)
        get-all-aliases
        (ih/handler ::get-all-aliases h/get-all-aliases)
        delete-alias
        (ih/handler ::delete-alias h/delete-alias)
        delete-aliases
        (ih/handler ::delete-aliases h/delete-aliases)
        health
        (ih/handler ::health h/health)]
    (route/expand-routes
     [[:short-url-api
       ["/" ^:interceptors [(pi/interceptor (mi/format-interceptor))]
        ["/health" {:any [::health health]}]
        ["/api/v1"
         ["/alias" {:post   [::create-or-get-alias
                             ^:interceptors
                             [conform-params-create-alias-cofx
                              store-get-alias-by-original-cofx
                              store-create-alias-fx]
                             create-or-get-alias]
                    :get    [::get-all-aliases
                             ^:interceptors
                             [store-get-all-aliases-cofx]
                             get-all-aliases]
                    :delete [::delete-aliases
                             ^:interceptors
                             [conform-params-delete-aliases-cofx
                              store-delete-aliases-fx]
                             delete-aliases]}
          ["/:short" ^:interceptors [conform-params-get-alias-by-short-cofx
                                     store-get-alias-by-short-cofx]
           {:get    [::get-alias get-alias]
            :delete [::delete-alias
                     ^:interceptors
                     [store-delete-alias-fx]
                     delete-alias]}]]]]]])))
