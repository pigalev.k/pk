(ns pk.bases.short-url-api.web.handlers
  (:require
   [clojure.string :as s]
   [pk.components.alias.api :as a]
   [taoensso.timbre :as timbre]))


(defn format-body
  "Create a response body using a template for response data."
  [data]
  {:data data})

(defn response
  "Create a Ring response map."
  [status body & {:as headers}]
  {:status status :body body :headers headers})

(defn data-response
  "Create a Ring response map with templated body."
  [status body & {:as headers}]
  (response status (format-body body) headers))

(def ok (partial response 200))
(def bad-request (partial response 400))
(def unprocessable-content (partial response 422 nil))
(def not-found (partial response 404 nil))
(def no-content (partial response 204 nil))

(def data-ok (partial data-response 200))
(def data-created (partial data-response 201))

(defn create-or-get-alias
  "Returns response with either already existing in the store or newly created
  alias."
  [{:keys [existing-alias created-alias] :as request}]
  (cond
    existing-alias (do
                     (timbre/infof "retrieved existing alias: %s"
                                   (a/short existing-alias))
                     (data-ok existing-alias))
    created-alias  (do
                     (timbre/infof "created new alias: %s"
                                   (a/short created-alias))
                     (data-created created-alias))
    :else (throw (ex-info "Cannot create alias"
                          {:short (get-in request [:params :short])}))))

(defn get-all-aliases
  "Returns response with all existing aliases from the store."
  [{:keys [existing-aliases]}]
  (timbre/infof "retrieved all existing aliases: %s" (count existing-aliases))
  (data-ok existing-aliases))

(defn get-alias
  "Returns response with an existing alias."
  [{:keys [existing-alias] :as request}]
  (if existing-alias
    (do
      (timbre/infof "retrieved existing alias: %s" (a/short existing-alias))
      (data-ok existing-alias))
    (do
      (timbre/warnf "alias not found: %s" (get-in request [:params :short]))
      (not-found))))

(defn delete-alias
  "Returns response confirming alias deletion."
  [{:keys [deleted-alias] :as request}]
  (if deleted-alias
    (do
      (timbre/infof "deleted alias: %s" (a/short deleted-alias))
      (no-content))
    (do
      (timbre/warnf "alias not found: %s" (get-in request [:params :short]))
      (not-found))))

(defn delete-aliases
  "Returns response confirming deletion of several aliases."
  [{:keys [deleted-aliases-count] :as request}]
  (let [aliases-to-delete       (get-in request [:params :short])
        aliases-to-delete-count (count aliases-to-delete)]
    (if deleted-aliases-count
      (if (= deleted-aliases-count aliases-to-delete-count)
        (do (timbre/infof "deleted %s aliases: %s"
                          deleted-aliases-count (s/join "," aliases-to-delete))
            (no-content))
        (do (timbre/warnf "deleted %s aliases, requested to delete %s: %s"
                          deleted-aliases-count
                          aliases-to-delete-count
                          (s/join "," aliases-to-delete))
            (no-content)))
      (do
        (timbre/warnf "failed to delete aliases: %s"
                      (s/join "," aliases-to-delete))
        (unprocessable-content)))))

(defn health
  "Returns a health-check response."
  [_]
  (ok "ok"))
