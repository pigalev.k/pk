(ns pk.bases.short-url-api.spec
  (:require
   [clojure.spec.alpha :as s]
   [clojure.string :as string]
   [pk.components.alias.core :as acore]))


;; TODO: extract reusable helpers & predicates (maybe conformers too) to a
;; component

;; helpers

(defn explain-errors
  "Formats (more or less) readable error messages for spec data
  validation error `explain-data`, as provided by
  `clojure.spec.alpha/explain-data`."
  [explain-data]
  (let [problems                     (::s/problems explain-data)
        problem-attrs-and-specs      (map (juxt :in :via :pred) problems)
        last-problem-attrs-and-specs (map (fn [[in via pred]]
                                            (vector (peek in)
                                                    (peek via)
                                                    (pr-str pred)))
                                          problem-attrs-and-specs)
        format-error-fn (fn [[attr spec pred]]
                          (if attr
                            (format
                             "attribute %s failed spec %s, predicate %s"
                             attr spec pred)
                            (format
                             "data failed spec: %s, predicate %s"
                             spec pred)))
        formatted-errors (map format-error-fn
                              last-problem-attrs-and-specs)]
    (string/join "; " formatted-errors)))


;; predicates and specs

;; request parameters

(defn scalar-or-vector->vector
  "Conforms scalar or vector parameter to a vector."
  [[kind value]]
  (cond
    (= :scalar kind) [value]
    (= :vector kind) value
    :else ::s/invalid))

(s/def ::vector-of-shorts (s/coll-of ::acore/short
                                     :min-count 1
                                     :kind vector?
                                     :distinct true))
(s/def :request.params/create-alias (s/keys :req-un [::acore/original]))
(s/def :request.params/get-alias-by-short (s/keys :req-un [::acore/short]))
(s/def :request.params.delete-aliases/short
  (s/and (s/or :scalar ::acore/short
               :vector ::vector-of-shorts)
         (s/conformer scalar-or-vector->vector)
         ::vector-of-shorts))
(s/def :request.params/delete-aliases
  (s/keys :req-un [:request.params.delete-aliases/short]))

(comment

  (s/explain :request.params/delete-aliases nil)
  (s/explain :request.params/delete-aliases {:short nil})
  (s/explain :request.params/delete-aliases {:short []})
  (s/explain :request.params/delete-aliases {:short [1]})
  (s/explain :request.params/delete-aliases {:short ["a" "a"]})

  (s/conform :request.params/delete-aliases {:short "a"})
  (s/conform :request.params/delete-aliases {:short ["a" "b" "c"]})

)


;; system configuration specs


(s/def ::network-port (s/int-in 1 65536))


;; alias options

(s/def :pk.components/alias (s/keys :opt-un [::acore/length]))


;; database

;; TODO: move database specs to database component

(s/def :datahike.db-spec.store/backend #{:jdbc :mem})
(s/def :datahike.db-spec.store/dbname string?)
(s/def :datahike.db-spec.store/dbtype string?)
(s/def :datahike.db-spec.store/host string?)
(s/def :datahike.db-spec.store/port ::network-port)
(s/def :datahike.db-spec.store/user string?)
(s/def :datahike.db-spec.store/password string?)
(s/def :datahike.db-spec/store
  (s/keys :req-un [:datahike.db-spec.store/backend]
          :opt-un [:datahike.db-spec.store/dbname
                   :datahike.db-spec.store/dbtype
                   :datahike.db-spec.store/host
                   :datahike.db-spec.store/port
                   :datahike.db-spec.store/user
                   :datahike.db-spec.store/password]))

(s/def :datahike.db-spec/name string?)
(s/def :datahike.db-spec/schema-flexibility? boolean?)
(s/def :datahike.db-spec/keep-history? boolean?)
(s/def :datahike.db-spec/attribute-refs? boolean?)

(s/def :datahike/db-spec
  (s/keys :req-un [:datahike.db-spec/store]
          :opt-un [:datahike.db-spec/name
                   :datahike.db-spec/schema-flexibility?
                   :datahike.db-spec/keep-history?
                   :datahike.db-spec/attribute-refs?]))

(s/def :pk.bases.short-url-api.database.datahike/delete-db boolean?)
(s/def :pk.bases.short-url-api.database/datahike
  (s/keys :req-un [:datahike/db-spec
                   :pk.bases.short-url-api.database.datahike/delete-db]))

(s/def :pk.bases.short-url-api/database
  :pk.bases.short-url-api.database/datahike)


;; alias store

;; TODO: move store specs to the store component

(defn datahike-db-connection? [value]
  (and (instance? clojure.lang.Atom value)
       (instance? datahike.db.DB @value)))

(s/def :pk.components.alias-store.datahike/schema
  (s/nilable (s/coll-of map? :kind vector?)))
(s/def :pk.components.alias-store.datahike/initial-data
  (s/nilable (s/coll-of map? :kind vector?)))
(s/def :pk.components.alias-store.datahike/connection datahike-db-connection?)
(s/def :pk.components.alias-store/datahike
  (s/keys :req-un [:pk.components.alias-store.datahike/connection]
          :opt-un [:pk.components.alias-store.datahike/schema
                   :pk.components.alias-store.datahike/initial-data]))

;; TODO: extract component options specs to components

(s/def :pk.components/alias-store :pk.components.alias-store/datahike)


;; web

;; routes

(s/def :pk.bases.short-url-api.web.routes/alias-options :pk.components/alias)
(s/def :pk.bases.short-url-api.web.routes/alias-store :pk.components/alias-store)

(s/def :pk.bases.short-url-api.web/routes
  (s/keys :req-un [:pk.bases.short-url-api.web.routes/alias-options
                   :pk.bases.short-url-api.web.routes/alias-store]))

;; server

(s/def :pk.bases.short-url-api.web.server/resource-if-not-found string?)

(s/def :pedestal.http/type #{:jetty})
(s/def :pedestal.http/host string?)
(s/def :pedestal.http/port ::network-port)
(s/def :pedestal.http/join? boolean?)
(s/def :pedestal.http/resource-path string?)
(s/def :pedestal.http/file-path string?)
(s/def :pedestal.http.allowed-origins/allowed-origins
  (s/nilable (s/coll-of string? :kind vector?)))
(s/def :pedestal.http.allowed-origins/creds (s/nilable boolean?))
(s/def :pedestal.http/allowed-origins
  (s/keys :req-un [:pedestal.http.allowed-origins/creds
                   :pedestal.http.allowed-origins/allowed-origins]))
(s/def :pedestal.http.secure-headers/content-security-policy-settings map?)
(s/def :pedestal.http/secure-headers
  (s/keys :req-un
          [:pedestal.http.secure-headers/content-security-policy-settings]))
(s/def :pedestal.http/routes (s/coll-of map?))
(s/def :pk.bases.short-url-api.web/server
  (s/keys :req-un [:pedestal.http/type
                   :pedestal.http/host
                   :pedestal.http/port
                   :pedestal.http/join?
                   :pedestal.http/routes]
          :opt-un [:pedestal.http/allowed-origins
                   :pedestal.http/secure-headers
                   :pedestal.http/file-path
                   :pedestal.http/resource-path
                   :pk.bases.short-url-api.web.server/resource-if-not-found]))
