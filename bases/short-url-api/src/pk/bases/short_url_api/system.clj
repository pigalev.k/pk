(ns pk.bases.short-url-api.system
  "Managing the lifecycle of the components from which the system is
  composed (initialize, halt, suspend, resume). Components and their
  options are described in a configuration file."
  (:require
   [datahike.api :as d]
   [integrant.core :as ig]
   [io.pedestal.http :as http]
   [pk.bases.short-url-api.web.core :as web]
   [pk.bases.short-url-api.web.routes :as routes]
   [pk.components.alias.core]
   [pk.components.alias-store.api :as asa]
   [pk.components.alias-store.datahike]
   [pk.components.datasource.api :as dsa]
   [pk.components.datasource.datahike]
   [taoensso.timbre :as timbre]))


;; alias options

(defmethod ig/init-key :pk.components/alias
  [_ options]
  (timbre/info "reading alias options")
  options)

(defmethod ig/pre-init-spec :pk.components/alias
  [_]
  :pk.components/alias)


;; alias store

(defmethod ig/init-key :pk.components/alias-store
  [_ alias-store-options]
  (timbre/info "creating Datahike alias store")
  (asa/alias-store alias-store-options))

(defmethod ig/pre-init-spec :pk.components/alias-store
  [_]
  :pk.components/alias-store)


;; databases

(derive :pk.bases.short-url-api.database/datahike
        :pk.bases.short-url-api/database)

;; Datahike connection is a reference (atom) that always points to the current
;; db value; used as a datasource
(defmethod ig/init-key :pk.bases.short-url-api.database/datahike
  [_ {:keys [db-spec delete-db]}]
  (timbre/info "connecting to Datahike db")
  (dsa/datasource {:type       :datahike
                   :db-spec    db-spec
                   :db-options {:delete-db delete-db}}))

(defmethod ig/pre-init-spec :pk.bases.short-url-api.database/datahike
  [_]
  :pk.bases.short-url-api.database/datahike)

;; the connection should be released eventually
(defmethod ig/halt-key! :pk.bases.short-url-api.database/datahike
  [_ connection]
  (timbre/info "releasing Datahike connection")
  (d/release connection))


;; web server

(defmethod ig/init-key :pk.bases.short-url-api.web/routes
  [_ options]
  (timbre/info "creating routes")
  (routes/routes options))

(defmethod ig/pre-init-spec :pk.bases.short-url-api.web/routes
  [_]
  :pk.bases.short-url-api.web/routes)

(defmethod ig/init-key :pk.bases.short-url-api.web/server
  [_ server-options]
  (let [{:keys [host port]} server-options]
    (timbre/infof "starting web server on %s:%s" host port))
  (-> server-options
      web/service-map
      http/default-interceptors
      #_http/dev-interceptors
      http/create-server
      http/start))

(defmethod ig/pre-init-spec :pk.bases.short-url-api.web/server
  [_]
  :pk.bases.short-url-api.web/server)

(defmethod ig/halt-key! :pk.bases.short-url-api.web/server
  [_ server]
  (timbre/info "stopping web server")
  (http/stop server))
