(ns scratch
  "Experimentation playground for development.
  To avoid cluttering source namespaces, dump all the temporary stuff here,
  preferably in the form of Rich comments. More scratches can be added, too."
  (:require
   [aero.core :as a]
   [clojure.java.io :as io]
   [next.jdbc :as jdbc]
   [pk.bases.short-url-api.core :as c]
   [pk.bases.short-url-api.domain.alias.store.in-memory :as mstore]
   [pk.bases.short-url-api.domain.alias.store.relational :as rstore]
   [pk.components.alias.core :as al]))


;; TODO: lots of deprecated stuff, review

;; config
(comment

  (a/read-config (io/resource "config.edn"))
  (a/read-config (io/resource "config.edn") {:profile :dev})
  (a/read-config (io/resource "config.edn") {:profile :test})
  (a/read-config (io/resource "config.edn") {:profile :prod})

  c/config

  *command-line-args*

  (System/getenv "HOME")
  (System/getenv "PROFILE")
)



;; in-memory store
(comment

  (def a0 (al/alias "http://some.site"))
  (def a1 (al/alias "http://some.other.site"))
  (def a2 (al/alias "http://another.site" {:length 10}))

  (def ims (mstore/create-alias-store (atom #{})))

  (doseq [als [a0 a1 a2]]
    (al/insert ims als))

  (al/delete ims (:short a0))

  (count
   (al/retrieve-all ims))

)


;; relational store
(comment

  (def db-spec {:dbname "short-url-api-scratch"
                :dbtype "h2:mem"})
  (def rds (jdbc/get-datasource db-spec))
  (def rs (rstore/create-alias-store rds))


  (al/insert rs (al/alias "qwe"))

  (al/retrieve-by-alias rs "xhdxk")

  (al/retrieve-by-original rs "qwe")
  (al/retrieve-by-alias rs "cpfiq")

  (al/retrieve-by-original rs "http://yet1.another.site")
  (al/retrieve-by-original rs "absolutely absent")

  ;; db-spec as datasource? yes
  (def rs1 (rstore/create-alias-store db-spec))
  (al/insert rs1 (al/alias "qwe"))
  (al/retrieve-all rs1)
)
