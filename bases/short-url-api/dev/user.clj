(ns user
  "Managing the service's system and reloading changed sources during
  development. Also allows to inspect the running system."
  (:require
   [clojure.tools.namespace.repl :refer [set-refresh-dirs]]
   [datahike.api :as d]
   [integrant.core :as ig]
   [integrant.repl :refer [clear go halt reset reset-all set-prep!]]
   [integrant.repl.state :as rs]
   [kaocha.repl :as k]
   [dev.nu.morse :as morse]
   [pk.bases.short-url-api.core :as c]
   [pk.components.alias-store.api :as sapi]
   [pk.components.alias-store.datahike :as dsapi]
   [portal.api :as pa]
   [taoensso.timbre :as timbre]))


;; morse

(comment

  ;; launch the ui

  (morse/launch-in-proc)

  ;; inspect data

  (morse/inspect {:a 1 :b 2})
  (morse/inspect rs/system)
)

;; portal

(comment

  ;; open a new inspector
  (def p (pa/open))
  ;; add portal as a tap> target
  (add-tap #'pa/submit)
  ;; start tapping out values
  (tap> :hello)
  ;; bring selected value back into repl
  (prn @p)
  ;; remove portal from tap> targetset
  (remove-tap #'pa/submit)
  ;; close the inspector when done
  (pa/close)

)


;; integrant repl

(defn config
  "Read and return service config."
  []
  (c/config (c/config-file-name) (c/profile)))

(defn init-ig-repl
  "Initialize Integrant REPL.
  Configures `ig/repl` functions, loads all needed namespaces and watches for
  changed files."
  []
  (set-refresh-dirs "src")
  (set-prep! (fn []
               (let [conf (config)]
                 (c/init-logging!
                  {:min-level         [["org.eclipse.jetty.*" :info]
                                       ["io.pedestal.http.cors" :warn]
                                       ["io.pedestal.*" :info]
                                       ["*" :debug]]
                   :color-stacktrace? true})
                 (ig/load-namespaces conf)
                 (ig/prep conf)))))

(init-ig-repl)

(comment

  ;; prepare and init the system in one step
  ;; (also `prep` and `init` can be run separately)
  (go)

  ;; the system configuration is stored in `integrant.repl.state/config`
  rs/config
  rs/system

  ;; stop the system
  (halt)

  ;; stop the system and clear the system configuration
  (clear)

  ;; reload changed source files and restart the system
  (reset)

  ;; reload all source files and restart the system
  (reset-all)

  (keys rs/system)

  (def conn (:pk.bases.short-url-api.database/datalog rs/system))
  (d/q '[:find (pull ?e [*])
         :where [?e :alias/short "abcd"]]
       @conn)
  (d/transact conn {:tx-data [{:db/id          -1
                               :alias/short    "uvga"
                               :alias/original "http://whatever"}]})
  (d/q dsapi/all-aliases-count-query @conn)

  (def store (:pk.bases.short-url-api.domain.alias.store/datalog rs/system))
  (sapi/retrieve-all store)
  (sapi/delete-all store ["uvga" "nhfv"])


  ;; to fix the alias redefinition error, so C-c C-k works again

  (ns-unalias *ns* 'c)
  (ns-unalias *ns* 'dstore)

  timbre/*config*

)


;; running tests

(comment

  ;; run a whole suite

  (k/run :unit)

  ;; run all tests

  (k/run-all)

)
