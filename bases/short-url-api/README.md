# short-url-api

A base that provides a REST API for short URLs.

Defines an Integrant system that initializes persistent storage and starts an
HTTP server.

## Develop

Start a REPL

```
clj -A:dev
```

or use your favourite editor to start one.

## Build

List available build tasks and their usage summaries

```
clojure -T:build
```

## Test

Endpoints: see `docs/api.org`.

Unit

```
clojure -M:dev:test
```

See Kaocha documentation for additional information.
