(ns pk.bases.short-url-api.config-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [integrant.core :as ig]
   [pk.bases.short-url-api.core :refer [config]])
  (:import (clojure.lang ExceptionInfo)))


(deftest configuration-spec--test
  (testing "system can't be started if configuration is invalid"
    (is (thrown? ExceptionInfo
                 (ig/init
                  (config "resources/config/invalid/invalid-web.edn" :dev)))
        "invalid web server configuration")
    (is (thrown? ExceptionInfo
                 (ig/init
                  (config "resources/config/invalid/invalid-db.edn" :dev)))
        "invalid db configuration")
    (is (thrown? ExceptionInfo
                 (ig/init
                  (config "resources/config/invalid/invalid-store.edn" :dev)))
        "invalid store configuration"))
  (testing "system can be started with valid configuration"
    (let [system (ig/init
                  (config "resources/config/valid/config.edn" :dev))]
      (is (map? system))
      (ig/halt! system))))
