(ns pk.bases.short-url-api.api-test
  (:refer-clojure :exclude [short])
  (:require
   [babashka.http-client :as http]
   [clojure.test :refer [deftest is testing use-fixtures]]
   [integrant.core :as ig]
   [jsonista.core :as json]
   [pk.bases.short-url-api.core :refer [config]]))


(defn with-system
  "A fixture that starts the system with the testing configuration
  before each test, and stops the system after the test."
  [t]
  (let [system (ig/init (config "resources/config/valid/config.edn" :test))]
      (t)
      (ig/halt! system)))

(use-fixtures :each with-system)


(def predefined-alias-short-1 "/abcd")
(def predefined-alias-short-2 "/efgh")
(def predefined-alias-short-3 "/ijkl")
(def predefined-alias-short-4 "/mnop")

(def nonexistent-alias-short-1 "/does-not-exist")
(def nonexistent-alias-short-2 "/does-not-exist-either")

(def valid-alias-original "https://test.example.com")
(def invalid-alias-original "httpq://test.example.com")

(def base-url "http://localhost:19191")
(def prefix "")
(def api-url (str base-url prefix "/api/v1"))
(def health-url (str base-url "/health"))
(def aliases-url (str api-url "/alias"))
(defn alias-url
  "Returns a URL for an alias resource identified by `short`."
  [short]
  (str aliases-url short))

(defn edn->json
  "Converts EDN to JSON string.
  Stringifies keyword map keys."
  [value]
  (json/write-value-as-string value (json/object-mapper {:encode-key-fn true})))

(defn json->edn
  "Converts JSON string to edn.
  Keywordizes string map keys."
  [string]
  (json/read-value string (json/object-mapper {:decode-key-fn true})))

(defn request-options
  "Returns HTTP client options for making a request to `url` using
  `method` (default is `:get`), `query-params` (default is nil) and
  JSON body (default is nil). Instructs the client to not throw on
  response statuses that are not 2xx/3xx."
  [url & {:keys [method body query-params]}]
  (cond-> {:uri          url
           :method       (or method :get)
           :headers      {"Content-Type" "application/json"}
           :throw        false}
    query-params (assoc :query-params query-params)
    body         (assoc :body (edn->json body))))


(deftest api-test
  (testing "short URL REST API ->"

    (testing "GET /health (a service healthcheck)"
      (is (= 200 (:status (http/request (request-options health-url))))
          "200 OK always"))

    (testing "/api/v1 ->"
      (testing "GET /alias (get all aliases) ->"
        (testing "success"
          (let [response        (http/request (request-options aliases-url))
                decoded-body    (json->edn (:body response))
                list-of-aliases (:data decoded-body)]
            (is (= 200 (:status response)) "200 OK always")
            (is (vector? list-of-aliases)
                "response body contains a vector of aliases")
            (is (= 3 (count list-of-aliases))
                "response body contains correct number of predefined aliases"))))

      (testing "GET /alias/:short (get an alias by short string) ->"
        (testing "success"
          (is (= 200 (:status
                      (http/request
                       (request-options (alias-url predefined-alias-short-1)))))
              "200 OK when the alias exists"))
        (testing "failure"
          (is (= 404 (:status
                      (http/request
                       (request-options (alias-url nonexistent-alias-short-1)))))
              "404 Not Found when the alias does not exists")))

      (testing "POST /alias (create an alias or get existing one) ->"
        (testing "success"
          (is (= 201 (:status
                      (http/request
                       (request-options aliases-url
                                        {:method :post
                                         :body
                                         {:original valid-alias-original}}))))
              "201 Created if alias for that URL did not exist")
          (is (= 200 (:status
                      (http/request
                       (request-options aliases-url
                                        {:method :post
                                         :body
                                         {:original valid-alias-original}}))))
              "200 OK if alias for that URL already exists"))
        (testing "failure"
          (is (= 400 (:status
                      (http/request
                       (request-options aliases-url
                                        {:method :post
                                         :body
                                         {:rgnl valid-alias-original}}))))
              "400 Bad Request if original field absent in request body")
          (is (= 400 (:status
                      (http/request
                       (request-options aliases-url
                                        {:method :post
                                         :body
                                         {:original invalid-alias-original}}))))
              "400 Bad Request if original is not an URL")
          (is (= 400 (:status
                      (http/request
                       (request-options aliases-url {:method :post}))))
              "400 Bad Request if request body is absent")))

      (testing "DELETE /alias/:short (delete an alias) ->"
        (testing "success"
          (is (= 204 (:status
                      (http/request
                       (request-options (alias-url predefined-alias-short-1)
                                        {:method :delete}))))
              "204 No Content if alias did exist and was deleted"))
        (testing "failure"
          (is (= 404 (:status
                      (http/request
                       (request-options (alias-url predefined-alias-short-1)
                                        {:method :delete}))))
              "404 Not Found if alias did not exist")))

      (testing "DELETE /alias (delete aliases) ->"
        (testing "success"
          (is (= 204 (:status
                      (http/request
                       (request-options aliases-url
                                        {:method :delete
                                         :query-params
                                         {"short"
                                          [predefined-alias-short-2
                                           predefined-alias-short-3]}}))))
              "204 No Content if aliases did exist and were deleted")
          (is (= 204 (:status
                      (http/request
                       (request-options aliases-url
                                        {:method :delete
                                         :query-params
                                         {"short"
                                          [predefined-alias-short-4]}}))))
              "204 No Content if a single alias was passed and was deleted")
          (is (= 204 (:status
                      (http/request
                       (request-options aliases-url
                                        {:method :delete
                                         :query-params
                                         {"short"
                                          [nonexistent-alias-short-1
                                           nonexistent-alias-short-2]}}))))
              "204 No Content if aliases did not exist and none were deleted"))
        (testing "failure"
          (is (= 400 (:status
                      (http/request
                       (request-options aliases-url
                                        {:method :delete}))))
              "400 Bad Request if no query parameters passed")
          (is (= 400 (:status
                      (http/request
                       (request-options aliases-url
                                        {:method :delete
                                         :query-params {"short" ""}}))))
              "400 Bad Request if one of short strings is empty")
          (is (= 400 (:status
                      (http/request
                       (request-options aliases-url
                                        {:method :delete
                                         :query-params
                                         {"short"
                                          [nonexistent-alias-short-1
                                           nonexistent-alias-short-1]}}))))
              "400 Bad Request if short strings are not distinct"))))))
