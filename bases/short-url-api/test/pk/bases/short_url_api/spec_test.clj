(ns ^:system pk.bases.short-url-api.spec-test
  (:require
   [clojure.test :refer [deftest testing are]]
   [pk.components.alias.core :as acore]))


(deftest http-url?--test
  (testing "http-url? is true"
    (are [s] (acore/http-url? s)
      "http://an-example.com"
      "http://another_example.com"
      "https://yet.another.example.com"
      "https://yet1.another.example.com"))
  (testing "http-url? is false"
    (are [s] (not (acore/http-url? s))
      ""
      "hello"
      "htt://example.com"
      "httf://example.com"
      "http://12*example.com")))
