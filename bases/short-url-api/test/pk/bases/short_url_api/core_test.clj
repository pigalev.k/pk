(ns pk.bases.short-url-api.core-test
  (:require
   [aero.core :as a]
   [clojure.test :refer [deftest testing is]]
   [pk.bases.short-url-api.core :as core]))


(deftest config-file-custom-tag-reader--integrant-ref--test
  (testing "result of reading custom tag '#ig/ref'"
    (let [ig-ref (a/reader {} 'ig/ref :test/integrant-config-key)]
      (is (= integrant.core.Ref (type ig-ref)) "have correct type")
      (is (= :test/integrant-config-key (:key ig-ref))
          "have correct key and value"))))

(deftest config-file-custom-tag-reader--secret-file--test
  (testing "result of reading custom tag '#secret-file'"
    (testing "if the file exists"
      (let [secret-file-contents
            (a/reader {} 'secret-file "test/resources/secret-file.txt")]
        (is (string? secret-file-contents) "is a string")
        (is (= "top-secret-password" secret-file-contents)
            "have a correct value")))
    (testing "if the file does not exist"
      (let [secret-file-contents
            (a/reader {} 'secret-file "test/resources/nonexistent.txt")]
        (is (nil? secret-file-contents) "have a correct value (nil)")))))


(deftest profile--test
  (testing "current runtime profile value"
    (let [profile (core/profile)]
      (is (keyword? profile) "is a keyword")
      (is (= :dev profile) "is ':dev' by default"))))

(deftest config-resource-name--test
  (testing "configuration resource name"
    (let [config-resource-name (core/config-file-name)]
      (is (string? config-resource-name) "is a string")
      (is (= "base/config.edn" config-resource-name)
          "is 'base/config.edn' by default"))))

(deftest config--test
  (testing "service configuration"
    (let [config-resource-name "resources/test-config.edn"
          profile :dev
          config (core/config config-resource-name profile)]
      (is (map? config) "is a map")
      (is (= 123 (:config/key config)) "have a correct key and value"))))
