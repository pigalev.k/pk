(ns pk.bases.short-url-spa.events-test
  (:require
   [cljs.test :refer-macros [deftest testing is] :refer [run-tests]]
   [pk.bases.short-url-spa.events :as e]
   [pk.bases.short-url-spa.effects :as fx]))


(deftest app-initialize-event-fx-handler-test
  (testing "::e/app-initialize event handler"
    (let [cofx        {}
          event       [::e/app-initialize]
          expected-fx [:transact ::fx/log]]
      (is (= expected-fx (->> (e/app-initialize-event-fx-handler cofx event)
                              :fx
                              (mapv first)))
          "returns correct effects"))))


(comment

  (run-tests)

)
