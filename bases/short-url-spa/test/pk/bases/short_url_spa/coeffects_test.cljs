(ns pk.bases.short-url-spa.coeffects-test
  (:require
   [cljs.test :refer-macros [deftest testing is] :refer [run-tests]]
   [pk.bases.short-url-spa.coeffects :as c]))


(deftest uuid-cofx-handler-test
  (testing "uuid-cofx-handler"
    (let [cofx {}
          event [:app/initialize]]
      (is (uuid? (:uuid (c/uuid-cofx-handler cofx event)))
          "provides UUIDs"))))


(comment

  (run-tests)

)
