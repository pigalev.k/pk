# short-url-spa

A Clojurescript/Reagent frontend base block for the `url-shortener-api` service.

## Prepare

1. Install `node`
2. Install npm dependencies

```
yarn
```

## Develop

Source paths and dependencies are managed through `deps.edn`, npm dependencies
are specified in `package.json`, build configuration is in the
`shadow-cljs.edn`. See these files to learn more.

Builds can be run by

- `yarn shadow-cljs [action] [build]*`
- `clojure -M:shadow-cljs [action] [build]*`

All examples will use the first command, modify as needed before using.

### Build and watch for changes

```
yarn shadow-cljs watch app
```

In emacs, you can start a build (with watch/hot-reload) and attach a cljs REPL
to it by
- running `cider-jack-in-cljs` (`C-u C-c M-J`)
- selecting `shadow-cljs` as a command to run
- selecting `shadow` as a REPL type
- selecting your build (e.g. `:app`)

Default values for command, REPL type and build can be specified, see
examples in `.dir-locals.el`.

### Build once

```
yarn shadow-cljs compile app
```

### Build for production

```
yarn shadow-cljs release app
yarn shadow-cljs release app --debug
```

>Please note that bases should not be used to build deployable artifacts, create
>a project that uses the base instead.

### Faster builds

A build server may be started to reduce startup times of all build commands.

```
# foreground in another terminal, C-c to stop
yarn shadow-cljs server

# background
yarn shadow-cljs start
yarn shadow-cljs stop
yarn shadow-cljs restart
```

## Test

### Unit

Compile tests

```
npx shadow-cljs compile tests
```

Run tests with Karma (using headless Chrome)

```
npx karma start --single-run
```

### Manual

Build the app

```
yarn shadow-cljs compile app
# or
yarn shadow-cljs release app
```

Serve the build as static files for inspection

```
clojure -M:serve :port 4444 :dir "public"
```

An API service should also be started for all app features to work.
