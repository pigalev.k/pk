(ns config
  "Build-time application configuration.
  Application must be rebuilt for changes in these settings to take
  effect.")

(def dev?
  "Whether the application is running in development mode."
  ^boolean goog.DEBUG)

(def app-url-prefix
  "A prefix that will be added to all application routes, and to the
  request for runtime configuration file (`config.edn`). Ensure that
  this same prefix is used for static files referenced in
  `index.html` (js, css, etc)."
  "")

(def user-settings-localstorage-key
  "A key under which to store application-wide user settings in browser
  localstorage."
  "short-url-spa")
