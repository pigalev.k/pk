(ns pk.bases.short-url-spa.subscriptions
  (:require
   [re-posh.core :as rp]))


;; generic (e.g. `rp/reg-sub`) subscriptions can be divided into the two
;; categories:
;; - extractor subscriptions: simple queries/pulls, no computations
;; - computation subscriptions (materialized views): no direct access to db, get
;;   inputs from extractors only, delegate computations to specialized functions


(def default-pull-pattern '[*])


;; the current app

(def app-id-query
  '[:find ?id .
    :where [?id :app/title]])
(rp/reg-query-sub ::app-id app-id-query)
(rp/reg-pull-sub ::app default-pull-pattern)


;; app attributes

(def app-title-query '[:find ?title .
                       :in $ ?app-id
                       :where [?app-id :app/title ?title]])
(rp/reg-query-sub ::app-title app-title-query)

(def app-theme-query '[:find ?theme .
                       :in $ ?app-id
                       :where [?app-id :app/theme ?theme]])
(rp/reg-query-sub ::app-theme app-theme-query)

(def app-original-url-query '[:find ?url .
                              :in $ ?app-id
                              :where [?app-id :app/original-url ?url]])
(rp/reg-query-sub ::app-original-url app-original-url-query)

(def app-short-url-query '[:find ?url .
                           :in $ ?app-id
                           :where [?app-id :app/short-url ?url]])
(rp/reg-query-sub ::app-short-url app-short-url-query)

(def app-base-url-query '[:find ?url .
                          :in $ ?app-id
                          :where [?app-id :app/base-url ?url]])
(rp/reg-query-sub ::app-base-url app-base-url-query)

(def app-alert-open-query '[:find ?open .
                            :in $ ?app-id
                            :where [?app-id :app/alert-open ?open]])
(rp/reg-query-sub ::app-alert-open app-alert-open-query)


;; API (backend) of the current app

(def app-url-shortening-api-id-query
  '[:find ?api-id .
    :in $ ?app-id
    :where [?app-id :app/url-shortening-api ?api-id]])
(rp/reg-query-sub ::app-url-shortening-api-id app-url-shortening-api-id-query)
(rp/reg-pull-sub ::app-url-shortening-api default-pull-pattern)


;; the current user of the app

(def app-user-id-query '[:find ?user-id .
                         :in $ ?app-id
                         :where [?app-id :app/user ?user-id]])
(rp/reg-query-sub ::app-user-id app-user-id-query)
(rp/reg-pull-sub ::app-user default-pull-pattern)

(def app-user-name-query '[:find ?name .
                           :in $ ?user-id
                           :where
                           [?user-id :user/name ?name]])
(rp/reg-query-sub ::app-user-name app-user-name-query)


;; application-wide alerts

(def app-alert-id-query '[:find ?alert-id .
                          :in $ ?app-id
                          :where [?app-id :app/alert ?alert-id]])
(rp/reg-query-sub ::app-alert-id app-alert-id-query)
(rp/reg-pull-sub ::app-alert default-pull-pattern)

(def app-alert-message-query '[:find ?message .
                               :in $ ?alert-id
                               :where [?alert-id :alert/message ?message]])
(rp/reg-query-sub ::app-alert-message app-alert-message-query)

(def app-alert-severity-query '[:find ?severity .
                                :in $ ?alert-id
                                :where [?alert-id :alert/severity ?severity]])
(rp/reg-query-sub ::app-alert-severity app-alert-severity-query)


;; the list of selected short URL IDs

(def app-selected-short-url-ids-query
  '[:find ?urls .
    :in $ ?app-id
    :where [?app-id :app/selected-short-urls ?urls]])
(rp/reg-query-sub ::app-selected-short-url-ids app-selected-short-url-ids-query)


;; the list of all short URLs

(def all-short-url-ids-query
  '[:find ?id
    :where [?id :short-url/short]])
(rp/reg-query-sub ::all-short-url-ids all-short-url-ids-query)
(rp/reg-pull-many-sub ::all-short-urls default-pull-pattern)
