(ns pk.bases.short-url-spa.coeffects
  (:require
   [clojure.edn :as edn]
   [datascript.core :as d]
   [config :as config]
   [pk.bases.short-url-spa.domain :as domain]
   [pk.bases.short-url-spa.subscriptions :as subs]
   [re-frame.core :as rf]))


;; example cofx handler

(defn uuid-cofx-handler
  "Provides an UUID."
  [cofx _]
  (assoc cofx :uuid (random-uuid)))

(rf/reg-cofx ::uuid uuid-cofx-handler)


;; application settings saved to local storage

(defn user-settings-cofx-handler
  "Provides application-wide user settings from local storage."
  [cofx _]
  (let [settings-string (or (domain/local-storage-get
                             config/user-settings-localstorage-key) "{}")
        settings        (edn/read-string settings-string)]
    (assoc cofx :user-settings settings)))

(rf/reg-cofx ::user-settings user-settings-cofx-handler)


;; current application attributes

;; the application uses DataScript database to store state, where set of current
;; application attributes is represented as an ordinary entity.

(defn app-id-cofx-handler
  "Provides a current application ID."
  [{:keys [ds] :as cofx} _]
  (let [app-id (d/q subs/app-id-query ds)]
    (assoc cofx :app-id app-id)))

(rf/reg-cofx ::app-id app-id-cofx-handler)


(defn app-alert-id-cofx-handler
  "Provides a current application's alert ID."
  [{:keys [ds app-id] :as cofx} _]
  (let [alert-id (d/q subs/app-alert-id-query ds app-id)]
    (assoc cofx :app-alert-id alert-id)))

(rf/reg-cofx ::app-alert-id app-alert-id-cofx-handler)


(defn app-base-url-cofx-handler
  "Provides current app's base URL."
  [{:keys [ds app-id] :as cofx} _]
  (let [app-base-url (d/q subs/app-base-url-query ds app-id)]
    (assoc cofx :app-base-url app-base-url)))

(rf/reg-cofx ::app-base-url app-base-url-cofx-handler)


(defn app-original-url-cofx-handler
  "Provides current app's original URL (the one to shorten), as it was
  supplied by user."
  [{:keys [ds app-id] :as cofx} _]
  (let [app-original-url (d/q subs/app-original-url-query ds app-id)]
    (assoc cofx :app-original-url app-original-url)))

(rf/reg-cofx ::app-original-url app-original-url-cofx-handler)


(defn app-short-url-cofx-handler
  "Provides current app's short URL."
  [{:keys [ds app-id] :as cofx} _]
  (let [app-short-url (d/q subs/app-short-url-query ds app-id)]
    (assoc cofx :app-short-url app-short-url)))

(rf/reg-cofx ::app-short-url app-short-url-cofx-handler)


(defn app-url-shortening-api-cofx-handler
  "Provides current app's backend (URL shortening API) entity."
  [{:keys [ds app-id] :as cofx} _]
  (let [app-url-shortening-api-id (d/q subs/app-url-shortening-api-id-query
                                       ds app-id)
        app-url-shortening-api (d/pull ds
                                       subs/default-pull-pattern
                                       app-url-shortening-api-id)]
    (assoc cofx :app-url-shortening-api app-url-shortening-api)))

(rf/reg-cofx ::app-url-shortening-api app-url-shortening-api-cofx-handler)


(defn app-selected-short-url-ids-cofx-handler
  "Provides current app's selected short URL IDs."
  [{:keys [ds app-id] :as cofx} _]
  (let [selected-short-url-ids
        (d/q subs/app-selected-short-url-ids-query ds app-id)]
    (assoc cofx :app-selected-short-url-ids selected-short-url-ids)))

(rf/reg-cofx ::app-selected-short-url-ids
             app-selected-short-url-ids-cofx-handler)


(defn all-short-url-ids-cofx-handler
  "Provides all short URL IDs."
  [{:keys [ds] :as cofx} _]
  (let [all-short-url-ids
        (reduce into [] (d/q subs/all-short-url-ids-query ds))]
    (assoc cofx :all-short-url-ids all-short-url-ids)))

(rf/reg-cofx ::all-short-url-ids
             all-short-url-ids-cofx-handler)
