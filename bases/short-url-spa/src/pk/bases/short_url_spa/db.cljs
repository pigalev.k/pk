(ns pk.bases.short-url-spa.db
  (:require
   [clojure.spec.alpha :as s]
   [datascript.core :as d]
   [re-posh.core :as rp]))


;; data specifications

(def http-url-pattern
  "Regex for valid HTTP(S) URLs."
  #"^https?://[a-zA-Z0-9._-]*")

(defn http-url?
  "Returns true if `s` is a valid HTTP(S) URL."
  [s]
  (boolean (re-matches http-url-pattern s)))

(s/def :user/id string?)
(s/def :app/title string?)
(s/def :app/user number?)
(s/def :app/original-url http-url?)
(s/def :app/short-url http-url?)


;; initial data

(def initial-app-tx-data
  {:db/id                  -3
   :app/user               -1
   :app/short-url          ""
   :app/original-url       ""
   :app/base-url           (.-origin ^js js/location)
   :app/url-shortening-api -2
   :app/selected-short-urls []})

(def initial-user-tx-data
  {:db/id     -1
   :user/name "anonymous"})

(def initial-api-tx-data
  {:db/id -2})


;; database schema

(def schema {:app/title               {:db/unique :db.unique/identity}
             :app/user                {:db/valueType :db.type/ref}
             :app/alert               {:db/valueType   :db.type/ref
                                       :db/isComponent true}
             :app/url-shortening-api  {:db/valueType   :db.type/ref
                                       :db/isComponent true}
             :app/selected-short-urls {:db/cardinality :db.cardinality/many}
             :short-url/short         {:db/unique :db.unique/identity}})


;; database instance

(def conn (d/create-conn schema))
(rp/connect! conn)
