(ns pk.bases.short-url-spa.effects
  (:require
   [config :as config]
   [pk.bases.short-url-spa.domain :as domain]
   [re-frame.core :as rf]
   [reagent.session :as session]))


(defn log-fx-handler
  "Handles the 'log to console' effect."
  [{:keys [severity message objects]}]
  (case severity
    :error   (.error js/console message objects)
    :warn    (.warn js/console message objects)
    (.log js/console message objects)))

(rf/reg-fx ::log log-fx-handler)


(defn copy-fx-handler
  "Handles the 'copy to clipboard' effect."
  [string]
  (domain/copy-to-clipboard string))

(rf/reg-fx ::copy copy-fx-handler)


(defn go-away-fx-handler
  "Handles the 'go away' effect.
  Redirects to `url`."
  [url]
  (domain/go-away url))

(rf/reg-fx ::go-away go-away-fx-handler)


(defn session-put-fx-handler
  "Handles the 'put value to Reagent session' effect."
  [{:keys [key value]}]
  (session/put! key value))

(rf/reg-fx ::session-put session-put-fx-handler)


(defn user-settings-fx-handler
  "Handles the 'save user settings' effect."
  [settings]
  (domain/local-storage-set
   config/user-settings-localstorage-key (pr-str settings)))

(rf/reg-fx ::user-settings user-settings-fx-handler)
