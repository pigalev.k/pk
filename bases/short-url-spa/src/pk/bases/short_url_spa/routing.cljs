(ns pk.bases.short-url-spa.routing
  (:require
   [clerk.core :as clerk]
   [config :as config]
   [pk.bases.short-url-spa.views.pages.about :refer [about]]
   [pk.bases.short-url-spa.views.pages.away :refer [away]]
   [pk.bases.short-url-spa.views.pages.main :refer [main]]
   [pk.bases.short-url-spa.views.pages.not-found :refer [not-found]]
   [pk.bases.short-url-spa.views.pages.short-urls :refer [short-urls]]
   [reagent.core :as r]
   [reagent.session :as session]
   [reitit.core :as reitit]
   [reitit.frontend :as rfe]
   [reitit.frontend.easy :as rfee]))


;; routing and navigation

(def router
  "Allows to find page names/parameters by given path and vice versa."
  (rfe/router
   [config/app-url-prefix
    ["/" {:name  :index
          :label "Home"}]
    ["/:short-string" {:name        :alias
                       :label       "Away"
                       :show-in-nav false}]
    ["/app"
     ["/short-urls" {:name  :short-urls
                     :label "My URLs"}]
     ["/about" {:name  :about
                :label "About"}]]]))

(defn routes
  "Returns a sequence of configured routes (pairs of route full path
  template and route data)."
  []
  (reitit/routes router))

(defn page-for
  "Maps route names to page components."
  [route-name]
  (case route-name
    :index      #'main
    :about      #'about
    :alias      #'away
    :short-urls #'short-urls
    #'not-found))

(defn init!
  "Initialize in-app routing and navigation."
  []
  (clerk/initialize!)
  (rfee/start!
   router
   (fn [{:keys [data parameters path-params path] :as match} history]
     (let [current-page             (:name  data)
           {:keys [query fragment]} parameters
           path-with-fragment       (str path
                                         (when fragment (str "#" fragment)))]
       (r/after-render clerk/after-render!)
       (session/put! :route {:current-page (page-for current-page)
                             :path-params  path-params
                             :query-params query
                             :fragment     fragment})
       (clerk/navigate-page! path-with-fragment)))
   {:use-fragment false}))


(comment

  (reitit/routes router)

)
