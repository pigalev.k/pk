(ns pk.bases.short-url-spa.domain)


(defn short-url
  "Create short URL from `base-url`, `app-url-prefix` and
  `short-string`."
  [base-url app-url-prefix short-string]
  (str base-url app-url-prefix "/" short-string))

(defn copy-to-clipboard
  "Copies the string `s` to clipboard."
  [s]
  (.writeText (.-clipboard ^js js/navigator) s))

(defn go-away
  "Redirects browser to `url`."
  [url]
  (set! (.-href ^js js/location) url))

(defn current-path?
  "Returns true if `path` represents the current location in the
  browser."
  [path]
  (= path (.-pathname ^js js/location)))

(defn local-storage-get
  "Returns a value under `key` from browser local storage."
  [key]
  (.getItem ^js js/localStorage key))

(defn local-storage-set
  "Sets `value` under `key` from browser local storage."
  [key value]
  (.setItem ^js js/localStorage key value))
