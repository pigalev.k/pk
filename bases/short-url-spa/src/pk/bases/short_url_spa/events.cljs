(ns pk.bases.short-url-spa.events
  (:require
   [ajax.core :as ajax]
   [ajax.edn :as edn]
   [config :as config]
   [day8.re-frame.http-fx]
   [pk.bases.short-url-spa.coeffects :as cofx]
   [pk.bases.short-url-spa.db :as db]
   [pk.bases.short-url-spa.effects :as fx]
   [pk.bases.short-url-spa.interceptors :as i]
   [re-frame.core :as rf]
   [re-posh.core :as rp]))


;; common interceptors (reusable between events)

(def default-interceptors
  "A chain of default interceptors."
  [(rf/inject-cofx :ds)
   (rf/inject-cofx ::cofx/app-id)
   (when config/dev? i/debug)])


;; note: as generic -fx event handlers are being registered instead of
;; specialized -ds ones (to have access to all coeffects, not just `:ds`), we
;; should inject `:ds` (datascript db) coeffect manually (or add it to default
;; interceptors), and place the effects meant to update the ds under `:transact`
;; key in returned effects map.


;; logging to console

(defn log-to-console-event-fx-handler
  "Handles the 'log to console' event."
  [_ [_ message & objects]]
  {::fx/log {:message message
             :objects objects}})

(rp/reg-event-fx ::log log-to-console-event-fx-handler)


;; loading app configuration from the app server (that serves app's static
;; files)

(defn app-load-configuration-event-fx-handler
  "Handles the 'load app configuration' event."
  [_ _]
  {:http-xhrio {:method          :get
                :uri             (str config/app-url-prefix "/config.edn")
                :timeout         5000
                :response-format (edn/edn-response-format)
                :on-success      [::initialize-app]
                :on-failure      [::log
                                  "error loading application config:"]}})

(rp/reg-event-fx
 ::load-app-configuration
 app-load-configuration-event-fx-handler)


;; initializing the app


(defn app-initialize-event-fx-handler
  "Handles the 'initialize app' event."
  [{:keys [user-settings]} [_ config]]
  {:fx [[:transact [(merge db/initial-app-tx-data
                           (:app config)
                           user-settings)
                    (merge db/initial-api-tx-data
                           (:api config))
                    db/initial-user-tx-data]]
        [::fx/log {:severity :info
                   :message  "application config loaded:"
                   :objects  (pr-str config)}]]})

(rp/reg-event-fx
 ::initialize-app
 [default-interceptors
  (rf/inject-cofx ::cofx/user-settings)]
 app-initialize-event-fx-handler)


;; saving app current URLs (original and short) to datascript db

(defn set-original-url-event-fx-handler
  "Handles the 'update original URL' event."
  [{:keys [app-id]} [_ url]]
  {:transact [[:db/add app-id :app/original-url url]]})

(rp/reg-event-fx
 ::set-original-url
 [default-interceptors]
 set-original-url-event-fx-handler)


(defn set-short-url-event-fx-handler
  "Handles the 'update short URL' event."
  [{:keys [app-id]} [_ url]]
  {:transact [[:db/add app-id :app/short-url url]]})

(rp/reg-event-fx
 ::set-short-url
 [default-interceptors]
 set-short-url-event-fx-handler)


;; main use case: shortening/unshortening a URL via API

(defn shorten-url-event-fx-handler
  "Handles the 'shorten URL' event."
  [{:keys [app-original-url app-url-shortening-api]}]
  (let [api-base-url (:api/base-url app-url-shortening-api)]
    {:http-xhrio {:method          :post
                  :uri             (str api-base-url "/alias")
                  :params          {:original app-original-url}
                  :timeout         5000
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success      [::shorten-url-ok]
                  :on-failure      [::shorten-url-fail]}}))

(rp/reg-event-fx
 ::shorten-url
 [default-interceptors
  (rf/inject-cofx ::cofx/app-url-shortening-api)
  (rf/inject-cofx ::cofx/app-original-url)]
 shorten-url-event-fx-handler)

(defn shorten-url-ok-event-handler
  "Handles the successful URL shortening."
  [cofx [_ result]]
  (let [{:keys [data]} result]
    {:fx [[:dispatch [::set-short-url data]]
          [:dispatch
           [::show-alert {:message  "Shortened!"
                          :severity :success}]]]}))

(rp/reg-event-fx
 ::shorten-url-ok
 [default-interceptors]
 shorten-url-ok-event-handler)

(defn shorten-url-fail-event-handler
  "Handles the failed URL shortening."
  [cofx [_ {:keys [status-text]}]]
  {:fx [[:dispatch
         [::show-alert {:message (str "Can't shorten URL: " status-text)
                        :severity :error}]]]})

(rp/reg-event-fx
 ::shorten-url-fail
 [default-interceptors]
 shorten-url-fail-event-handler)


(defn unshorten-url-event-fx-handler
  "Handles the 'unshorten URL' event."
  [{:keys [app-url-shortening-api]} [_ short-string]]
  (let [api-base-url (:api/base-url app-url-shortening-api)]
    {:http-xhrio {:method          :get
                  :uri             (str api-base-url "/alias/" short-string)
                  :timeout         5000
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success      [::unshorten-url-ok]
                  :on-failure      [::unshorten-url-fail]}}))

(rp/reg-event-fx
 ::unshorten-url
 [default-interceptors
  (rf/inject-cofx ::cofx/app-url-shortening-api)]
 unshorten-url-event-fx-handler)

(defn unshorten-url-ok-event-handler
  "Handles the successful URL unshortening."
  [cofx [_ result]]
  (let [{:keys [data]} result
        original-url   (:original data)]
    {:fx [[:dispatch-later {:ms       1000
                            :dispatch [::go-away original-url]}]]}))

(rp/reg-event-fx
 ::unshorten-url-ok
 [default-interceptors]
 unshorten-url-ok-event-handler)

(defn unshorten-url-fail-event-handler
  "Handles the failed URL unshortening."
  [cofx [_ {:keys [status-text]}]]
  {:fx [[:dispatch
         [::show-alert {:message (str "Can't unshorten URL: " status-text)
                        :severity :error}]]]})

(rp/reg-event-fx
 ::unshorten-url-fail
 [default-interceptors]
 unshorten-url-fail-event-handler)


;; copying the short URL to clipboard

(defn copy-short-url-fx-handler
  "Handles the 'copy short URL' event."
  [{:keys [app-short-url]}]
  {:fx [[::fx/copy app-short-url]
        [:dispatch
         [::show-alert {:message  "Copied!"
                        :severity :info}]]]})

(rp/reg-event-fx
 ::copy-short-url
 [default-interceptors
  (rf/inject-cofx ::cofx/app-short-url)]
 copy-short-url-fx-handler)


;; retrieving the list of short URLs from API

(defn get-all-short-urls-event-fx-handler
  "Handles the 'get all short URLs' event."
  [{:keys [app-url-shortening-api]}]
  (let [api-base-url (:api/base-url app-url-shortening-api)]
    {:fx [[:http-xhrio {:method          :get
                        :uri             (str api-base-url "/alias")
                        :timeout         5000
                        :format          (ajax/json-request-format)
                        :response-format (ajax/json-response-format
                                          {:keywords? true})
                        :on-success      [::get-all-short-urls-ok]
                        :on-failure      [::get-all-short-urls-fail]}]
          [:dispatch [::set-short-urls-loading true]]]}))

(rp/reg-event-fx
 ::get-all-short-urls
 [default-interceptors
  (rf/inject-cofx ::cofx/app-url-shortening-api)]
 get-all-short-urls-event-fx-handler)

(defn get-all-short-urls-ok-event-handler
  "Handles the successful retrieval of all short URLs."
  [cofx [_ result]]
  (let [{:keys [data]} result]
    {:fx [[:dispatch [::set-short-urls data]]
          [:dispatch [::set-short-urls-loading false]]]}))

(rp/reg-event-fx
 ::get-all-short-urls-ok
 [default-interceptors]
 get-all-short-urls-ok-event-handler)

(defn get-all-short-urls-fail-event-handler
  "Handles the failed retrieval of all short URLs."
  [cofx [_ {:keys [status-text]}]]
  {:fx [[:dispatch
         [::show-alert {:message  (str "Can't retrieve short URLs: " status-text)
                        :severity :error}]]
        [:dispatch [::set-short-urls-loading false]]]})

(rp/reg-event-fx
 ::get-all-short-urls-fail
 [default-interceptors]
 get-all-short-urls-fail-event-handler)


;; saving a list of short URLs to datascript db

(defn set-short-urls-event-fx-handler
  "Handles the 'set short URLs' event.
  Replaces existing short URLs with new ones."
  [{:keys [all-short-url-ids]} [_ aliases]]
  (let [alias->tx-data  (fn [{:keys [short original created-at]}]
                          (let [temp-id (- (abs (hash short)))]
                            {:db/id                temp-id
                             :short-url/short      short
                             :short-url/original   original
                             :short-url/created-at created-at}))
        add-new-tx-data (mapv alias->tx-data aliases)
        short-url-id->tx-data (fn [id]
                                [:db/retractEntity id])
        retract-old-tx-data (mapv short-url-id->tx-data all-short-url-ids)]
    {:transact (into [] (concat retract-old-tx-data add-new-tx-data))}))

(rp/reg-event-fx
 ::set-short-urls
 [default-interceptors
  (rf/inject-cofx ::cofx/all-short-url-ids)]
 set-short-urls-event-fx-handler)


;; saving a loading status of short URLs to Reagent session

(defn set-short-urls-loading-event-fx-handler
  "Handles the 'set short URLs loading status' event."
  [_ [_ status]]
  {:fx [[::fx/session-put {:key   :short-urls/loading-status
                           :value status}]]})

(rp/reg-event-fx
 ::set-short-urls-loading
 set-short-urls-loading-event-fx-handler)


;; going away to a given absolute URL

(defn go-away-fx-handler
  "Handles the 'go away' event."
  [cofx [_ url]]
  {:fx [[::fx/go-away url]]})

(rp/reg-event-fx
 ::go-away
 [default-interceptors]
 go-away-fx-handler)


;; showing/hiding application-wide alerts

(defn show-alert-fx-handler
  "Handles the 'show alert' event."
  [{:keys [app-id]} [_ {:keys [message severity]}]]
  {:transact [{:db/id          -1
               :alert/message  message
               :alert/severity severity}
              [:db/add app-id :app/alert -1]
              [:db/add app-id :app/alert-open true]]})

(rp/reg-event-fx
 ::show-alert
 [default-interceptors]
 show-alert-fx-handler)

(defn hide-alert-fx-handler
  "Handles the 'hide alert' event."
  [{:keys [app-id]} _]
  {:transact [[:db/add app-id :app/alert-open false]]})

(rp/reg-event-fx
 ::hide-alert
 [default-interceptors]
 hide-alert-fx-handler)


;; changing the application theme and saving application-wide user settings to
;; local storage

(defn app-change-theme-event-fx-handler
  "Handles the 'change application theme' event."
  [{:keys [app-id]} [_ theme-name]]
  {:fx [[:transact [[:db/add app-id :app/theme theme-name]]]
        [::fx/user-settings {:app/theme theme-name}]]})

(rp/reg-event-fx
 ::app-change-theme
 [default-interceptors]
 app-change-theme-event-fx-handler)


;; saving a list of selected short URLs to datascript db

(defn set-selected-short-url-ids-event-fx-handler
  "Handles the 'set selected short URL IDs' event."
  [{:keys [app-id]} [_ short-url-ids]]
  {:transact [[:db/retract app-id :app/selected-short-urls]
              [:db/add app-id :app/selected-short-urls short-url-ids]]})

(rp/reg-event-fx
 ::set-selected-short-url-ids
 [default-interceptors]
 set-selected-short-url-ids-event-fx-handler)


;; deleting selected short URLs via API

(defn delete-selected-short-urls-event-fx-handler
  "Handles the 'delete selected short URLs' event."
  [{:keys [app-url-shortening-api app-selected-short-url-ids]} _]
  (let [api-base-url (:api/base-url app-url-shortening-api)]
    {:fx [[:http-xhrio
           {:method          :delete
            :uri             (str api-base-url "/alias")
            :url-params          {:short app-selected-short-url-ids}
            :timeout         5000
            :format          (ajax/json-request-format)
            :response-format (ajax/text-response-format)
            :on-success      [::delete-selected-short-urls-ok]
            :on-failure      [::delete-selected-short-urls-fail]}]]}))

(rp/reg-event-fx
 ::delete-selected-short-urls
 [default-interceptors
  (rf/inject-cofx ::cofx/app-url-shortening-api)
  (rf/inject-cofx ::cofx/app-selected-short-url-ids)]
 delete-selected-short-urls-event-fx-handler)

(defn delete-selected-short-urls-ok-event-handler
  "Handles the successful deletion of selected URLs."
  [_ [_ result]]
  {:fx [[:dispatch [::set-selected-short-url-ids []]]
        [:dispatch [::get-all-short-urls]]
        [:dispatch [::set-short-urls-loading false]]]})

(rp/reg-event-fx
 ::delete-selected-short-urls-ok
 delete-selected-short-urls-ok-event-handler)

(defn delete-selected-short-urls-fail-event-handler
  "Handles the failed deletion of selected URLs."
  [_ [_ {:keys [status-text]}]]
  {:fx [[:dispatch [::set-short-urls-loading false]]
        [:dispatch
         [::show-alert {:message (str "Can't delete URLs: " status-text)
                        :severity :error}]]]})

(rp/reg-event-fx
 ::delete-selected-short-urls-fail
 delete-selected-short-urls-fail-event-handler)
