(ns pk.bases.short-url-spa.core
  (:require
   [pk.bases.short-url-spa.coeffects]
   [config :as config]
   [pk.bases.short-url-spa.db]
   [pk.bases.short-url-spa.effects]
   [pk.bases.short-url-spa.events :as events]
   [pk.bases.short-url-spa.routing :as routing]
   [pk.bases.short-url-spa.subscriptions]
   [pk.bases.short-url-spa.views.app :refer [app]]
   [re-frame.loggers :as log]
   [re-posh.core :as rp]
   [reagent.dom :as rdom]))


(set! *warn-on-infer* true)


;; application entry point

(defn mount
  "Mount the app into DOM and do initial render."
  [app]
  (reagent.dom/render app (js/document.getElementById "app")))

(defn run
  "Application entry point.
  Initializes in-app navigation, dispatches initial events, mounts the app."
  []
  (log/console :log (str "Starting the application in "
                         (if config/dev? "development" "production")
                         " mode."))
  (routing/init!)
  (rp/dispatch-sync [::events/load-app-configuration])
  (mount [app]))
