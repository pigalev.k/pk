(ns pk.bases.short-url-spa.interceptors
  (:require
   [clojure.spec.alpha :as s]
   [re-frame.interceptor :as rfi]
   [re-frame.loggers :as log]))


(def debug
  "A debugging interceptor.
  Prints the handled event, it's input (coeffects) and
  output (effects). Based on standard `re-frame.core/debug`, but
  adapted to re-posh, that uses Datascript database to store
  application state, so db diffs are not shown (yet)."
  (rfi/->interceptor
   :id     ::debug
   :before (fn debug-before
             [context]
             (log/console :group "Handling re-frame event:"
                          (rfi/get-coeffect context :event))
             (log/console :log "Coeffects:"
                          (rfi/get-coeffect context))
             (log/console :groupEnd)
             context)
   :after  (fn debug-after
             [context]
             (let [effects (rfi/get-effect context)
                   event   (rfi/get-coeffect context :event)]
               (log/console :group "Handled event:" event)
               (log/console :log "Effects:" effects)
               (log/console :groupEnd))
             context)))

;; TODO: use this for effect validation, add coeffect and event
;; validation (presumably for development use). Maybe refactor to
;; `validate-effects` (all at once)?
(defn validate-effect
  "Returns an interceptor that checks that effect description set by preceding
  handler/interceptor conforms to spec. If it does not, cancel the effect and
  log a warning. If effect is not present, do nothing."
  [effect-key effect-value-spec
   & {:keys [log-ok? ok-log-lvl fail-log-lvl]
      :or {log-ok? false ok-log-lvl :log fail-log-lvl :error}
      :as opts}]
  (rfi/->interceptor
   :id ::validate
   :after
   (fn validate-effect [ctx]
     (let [event-name (first (get-in ctx [:coeffects :event]))]
       (if-let [effect-value (rfi/get-effect ctx effect-key)]
         (if (s/valid? effect-value-spec effect-value)
           (do
             (when log-ok?
               (log/console :group "effect validation passed")
               (log/console ok-log-lvl "effect" effect-key)
               (log/console ok-log-lvl "event" event-name)
               (log/console :groupEnd))
             ctx)
           (do
             (log/console :group "effect validation failed, ignoring the effect")
             (log/console fail-log-lvl "effect" effect-key)
             (log/console fail-log-lvl "event" event-name)
             (log/console fail-log-lvl "cause"
                          (s/explain-str effect-value-spec effect-value))
             (log/console :groupEnd)
             (update-in ctx [:effects] dissoc effect-key)))
         ctx)))))
