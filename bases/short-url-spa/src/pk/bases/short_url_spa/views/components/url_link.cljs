(ns pk.bases.short-url-spa.views.components.url-link
  (:require
   ;; FIXME: remove dependency on config
   [config :as config]
   [pk.bases.short-url-spa.domain :as domain]
   [reagent-mui.icons.content-copy :refer [content-copy]]
   [reagent-mui.material.icon-button :refer [icon-button]]
   [reagent-mui.material.link :refer [link]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.material.tooltip :refer [tooltip]]
   [reagent-mui.material.typography :refer [typography]]))


(defn url-link
  "A link to URL constructed from `base-url` and `short-string`, with a
  button to copy it to clipboard. If `on-copy` is not nil, execute it
  after copying the URL to clipboard. If short string is empty, return
  a `placeholder` text instead."
  [{:keys [base-url short-string placeholder on-copy]}]
  (if (seq short-string)
    (let [url  (domain/short-url base-url config/app-url-prefix short-string)
          copy (fn [_]
                 (domain/copy-to-clipboard url)
                 (when on-copy
                   (on-copy url)))]
      [stack {:direction   :row
              :align-items :center
              :sx          {:height 36
                            :mt     1}}
       [link {:href      url
              :target    "_blank"
              :rel       :noopener
              :variant   :h6
              :underline :none} url]
       [tooltip {:title       "Copy to clipboard"
                 :enter-delay 500}
        [icon-button {:on-click copy
                      :sx       {:ml 1}}
         [content-copy]]]])
    [typography {:color "text.secondary"
                 :sx    {:height 36
                         :mt     1}}  placeholder]))
