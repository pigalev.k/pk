(ns pk.bases.short-url-spa.views.components.url-table
  (:require
   [cljc.java-time.format.date-time-formatter :as dtf]
   [cljc.java-time.local-date :as ld]
   [cljc.java-time.zoned-date-time :as zdt]
   ;; FIXME: remove dependency on config
   [config :as config]
   [reagent-mui.icons.delete :refer [delete]]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.fab :refer [fab]]
   [reagent-mui.material.linear-progress :refer [linear-progress]]
   [reagent-mui.material.link :refer [link]]
   [reagent-mui.material.zoom :refer [zoom]]
   [reagent-mui.util :refer [clj->js']]
   [reagent-mui.x.data-grid :refer [data-grid]]
   [reagent.core :as r]))


(def datetime-formatter
  "A formatter for URL creation date and time."
  (dtf/of-pattern "yyyy.MM.dd HH:mm:ss"))

(defn format-datetime
  "Formats a short URL creation time."
  [js-params]
  (let [value    (.-value js-params)
        datetime (zdt/parse
                  value dtf/iso-zoned-date-time)]
    (ld/format datetime datetime-formatter)))

(defn render-original
  "Returns a React element for rendering original URL."
  [js-params]
  (let [js-row (.-row js-params)
        {:keys [original]}
        (js->clj js-row :keywordize-keys true)]
    (r/as-element
     [link {:href      original
            :target    "_blank"
            :rel       :noopener
            :variant   :subtitle2
            :underline :none
            :sx        {:width         "100%"
                        :overflow      :hidden
                        :text-overflow :ellipsis}}
      original])))

(defn render-short
  [js-params]
  (let [js-row (.-row js-params)
        {:keys [short]}
        (js->clj js-row :keywordize-keys true)
        href   (str config/app-url-prefix "/" short)]
    (r/as-element
     [link {:href      href
            :target    "_blank"
            :rel       :noopener
            :underline :none
            :sx        {:width         "100%"
                        :overflow      :hidden
                        :text-overflow :ellipsis}}
      href])))

(def outlined-states (str
                      "& "
                      ".MuiDataGrid-cell:focus, "
                      ".MuiDataGrid-cell:focus-within, "
                      ".MuiDataGrid-cell:active, "
                      ".MuiDataGrid-columnHeader:focus, "
                      ".MuiDataGrid-columnHeader:focus-within, "
                      ".MuiDataGrid-columnHeader:active"))

(def columns  (clj->js' [{:field       :original
                          :header-name "My URL"
                          :render-cell render-original
                          :flex        1}
                         {:field       :short
                          :header-name "Short URL"
                          :render-cell render-short
                          :width       170}
                         {:field        :created-at
                          :header-name  "Created at"
                          :value-getter format-datetime
                          :width        170}]))


(defn custom-loading
  "Custom loading component for the URL table."
  [props]
  [linear-progress props])

(defn url-table
  "A table that displays a list of `short-urls` with their original URL,
  short string and the creation date. If `show-delete-button`,
  displays delete button. If `loading`, displays loading
  indicator. Executes `on-select` with a list of selected URL IDs when
  selection state changes. Executes `on-delete` when delete button is
  clicked."
  [{:keys [short-urls
           loading show-delete-button
           on-select on-delete]}]
  (let [rows (map (fn [short-url]
                    (let [id (:short-url/short short-url)]
                      (-> short-url
                          (assoc :id id)
                          (dissoc :db/id))))
                  short-urls)]
    [box
     [data-grid {:rows               rows
                 :columns            columns
                 :initial-state      (clj->js'
                                      {:pagination
                                       {:pagination-model
                                        {:page-size 5}}
                                       :sorting
                                       {:sort-model
                                        [{:field :created-at
                                          :sort  :desc}]}})
                 :page-size-options  (clj->js [5])
                 :disable-row-selection-on-click
                 true
                 :checkbox-selection true
                 :loading            loading
                 :slots
                 (clj->js' {:loading-overlay (r/reactify-component
                                              custom-loading)})
                 :on-row-selection-model-change
                 on-select
                 :auto-height        true
                 :sx                 {:border :none
                                      outlined-states
                                      {:outline :none}}}]
     [zoom {:in show-delete-button}
      [fab {:color    :primary
            :on-click on-delete
            :sx       {:position :absolute
                       :bottom   32
                       :right    32}}
       [delete]]]]))
