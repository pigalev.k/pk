(ns pk.bases.short-url-spa.views.components.app-alert
  (:require
   [reagent-mui.material.alert :refer [alert]]
   [reagent-mui.material.snackbar :refer [snackbar]]))


(defn app-alert
  "A popup alert to notify user on the outcomes on various events."
  [{:keys [message severity variant open on-close
           auto-hide-duration position]
    :or   {severity :info
           variant  :filled
           auto-hide-duration 1000
           position {:vertical   :bottom
                     :horizontal :center}}}]
  [snackbar {:open               open
             :on-close           on-close
             :auto-hide-duration auto-hide-duration
             :anchor-origin      position}
   [alert {:severity severity
           :variant  variant
           :on-close on-close}
    message]])
