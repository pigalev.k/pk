(ns pk.bases.short-url-spa.views.components.url-input-field
  (:require
   [pk.bases.short-url-spa.views.components.go-button :refer [go-button]]
   [reagent-mui.material.input-adornment :refer [input-adornment]]
   [reagent-mui.material.text-field :refer [text-field]]
   [reagent.core :as r]))


(defn url-input-field
  "A text input field for original URL with a primary action button.
  It is a managed input: receives `url` from application state and
  uses `on-change` to update the application state. Executes
  `on-action` when action button or Enter key is pressed and
  `action-disabled` is not true."
  [{:keys [url on-action on-change action-disabled]}]
  [text-field {:variant      :outlined
               :margin       :normal
               :full-width   true
               :label        "Your URL: https://..."
               :value        url
               :on-change    on-change
               :on-key-press (fn [event]
                               (when (and (not action-disabled)
                                          (= (.-key event) "Enter"))
                                 (on-action)))
               :InputProps   {:end-adornment
                              (r/as-element
                               [input-adornment {:position :end}
                                [go-button {:on-click on-action
                                            :disabled action-disabled}]])}}])
