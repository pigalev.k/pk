(ns pk.bases.short-url-spa.views.components.about-text
  (:require
   [reagent-mui.material.link :refer [link]]
   [reagent-mui.material.typography :refer [typography]]
   [reagent-mui.styles :refer [styled]]))


(defn a
  "Produces a link with some properties predefined."
  [href label]
  [link {:href      href
         :target    "_blank"
         :rel       :noopener
         :underline :none} label])

(def cljs-link
  [a "https://clojurescript.org/index" "ClojureScript"])

(def clj-link
  [a "https://clojure.org/index" "Clojure"])

(def spec-link
  [a "https://clojure.org/about/spec" "clojure.spec"])

(def re-frame-link
  [a "https://day8.github.io/re-frame/re-frame/" "re-frame"])

(def re-posh-link
  [a "https://github.com/denistakeda/re-posh" "re-posh"])

(def datascript-link
  [a "https://github.com/tonsky/datascript" "DataScript"])

(def material-ui-link
  [a "https://mui.com/" "Material UI"])

(def pedestal-link
  [a "http://pedestal.io/index" "Pedestal"])

(def datahike-link
  [a "https://datahike.io/" "Datahike"])

(def integrant-link
  [a "https://github.com/weavejester/integrant" "Integrant"])

(def p (styled typography
               (fn [{:keys [theme]}]
                 (let [spacing (get-in theme [:spacing])]
                   {:margin-bottom (spacing 1)}))))


(defn about-text
  ""
  []
  [:<>
   [p
    "A simple single-page web application (SPA) for creating short URLs from
     longer ones and retrieving the long URLs back again. It"]
   [:ul
    [:li "is written in " cljs-link " programming language"]
    [:li "uses " re-frame-link " functional web application framework to
       define the application structure"]
    [:li "uses " re-posh-link " to store application state in the "
     datascript-link " in-memory database"]
    [:li "uses " material-ui-link " as a library of React UI components"]]
   [p "As a backend it uses a REST API service that"]
   [:ul
    [:li "is written in " clj-link " programming language"]
    [:li "uses " pedestal-link " to define the HTTP request handling pipeline
     component"]
    [:li "uses " datahike-link " database as a data storage component"]
    [:li "uses " integrant-link " to compose a system from the components"]
    [:li "uses " spec-link " to coerce and validate data (system configuration,
     request parameters, etc.)"]]])
