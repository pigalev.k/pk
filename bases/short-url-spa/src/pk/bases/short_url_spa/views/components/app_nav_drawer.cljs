(ns pk.bases.short-url-spa.views.components.app-nav-drawer
  (:refer-clojure :exclude [list])
  (:require
   [pk.bases.short-url-spa.domain :as domain]
   [reagent-mui.material.box :refer [box]]
   [reagent-mui.material.drawer :refer [drawer]]
   [reagent-mui.material.list :refer [list]]
   [reagent-mui.material.list-item :refer [list-item]]
   [reagent-mui.material.list-item-button :refer [list-item-button]]
   [reagent-mui.material.list-item-text :refer [list-item-text]]
   [reagent-mui.material.typography :refer [typography]]
   [reagent-mui.styles :as styles :refer [styled]]))


(def drawer-width 240)

(def shifting-persistent-drawer
  "A shifting persistent drawer --- the one that sits on the same
  surface elevation as the content, is opened by user action and stays
  open until closed by the user. When the shifting drawer opens, it
  forces other content to change size and adapt to the smaller
  viewport (requires additional styles to be applied to the app bar
  and content area as well)."
  (styled drawer {:width               drawer-width
                  :flex-shrink         0
                  "& .MuiDrawer-paper" {:width      drawer-width
                                        :box-sizing :border-box}}))

(def shifting-persistent-drawer-header
  "A header for the shifting persistent drawer."
  (styled "div" (fn [{:keys [theme]}]
                  (let [toolbar-styles (get-in theme [:mixins :toolbar])
                        spacing        (get-in theme [:spacing])]
                    (merge {:display     :flex
                            :align-items :center
                            :justify-content :flex-end
                            :padding (spacing 0 1)}
                           toolbar-styles)))))

(def temporary-drawer
  "A temporary drawer --- the one that is closed by default, opened
  temporarily by user action, and sits above all other content until a
  section is selected. It can be cancelled (closed without selection)
  by clicking the overlay or pressing the Esc key."
  (styled drawer {:width drawer-width}))

(defn temporary-drawer-content
  "A content area for the temporary drawer --- a list of application
  sections."
  [title routes on-close]
  [box {:sx       {:width drawer-width}
        :on-click on-close}
   [list {:component :nav}
    [list-item
     [list-item-text
      [typography {:variant :overline} title]]]
    (for [[path route-data] routes]
      (let [{:keys [name label show-in-nav]} route-data]
        (when (not (false? show-in-nav))
           [list-item-button {:key        name
                              :component  :a
                              :href       path
                              :selected   (domain/current-path? path)
                              :aria-label (str "navigation-" label)}
            [list-item-text label]])))]])

(defn app-nav-drawer
  "An application navigation drawer --- is opened by user action,
  contains a list of application's sections (pages) and allows to
  navigate them."
  [{:keys [title open on-close routes]
    :or {title "Navigation"}}]
  [temporary-drawer {:anchor   :left
                     :open     open
                     :on-close on-close}
   [temporary-drawer-content title routes on-close]])
