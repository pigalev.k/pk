(ns pk.bases.short-url-spa.views.components.app-nav
  (:require
   [reagent-mui.material.button :refer [button]]
   [reagent-mui.material.stack :refer [stack]]))

(defn app-nav
  "A list of navigation buttons, one per route.
  Does not include routes with `:show-in-nav` in route data set to
  false."
  [{:keys [routes]}]
  [stack {:direction :row}
   (for [[path route-data] routes]
     (let [{:keys [name label show-in-nav]} route-data]
       (when (not (false? show-in-nav))
         [button {:key name
                  :href       path
                  :aria-label (str "navigation-" label)
                  :sx         {:ml 1}}
          label])))])


(comment

  (not (false? nil))
  (not (false? false))

)
