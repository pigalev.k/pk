(ns pk.bases.short-url-spa.views.components.app-header
  (:require
   [pk.bases.short-url-spa.events :as e]
   [re-posh.core :as rp]
   [reagent-mui.icons.dark-mode-outlined :refer [dark-mode-outlined]]
   [reagent-mui.icons.light-mode-outlined :refer [light-mode-outlined]]
   [reagent-mui.icons.menu :refer [menu] :rename {menu menu-icon}]
   [reagent-mui.material.app-bar :refer [app-bar]]
   [reagent-mui.material.button :refer [button]]
   [reagent-mui.material.icon-button :refer [icon-button]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.material.toolbar :refer [toolbar]]
   [reagent-mui.material.tooltip :refer [tooltip]]
   [reagent-mui.material.typography :refer [typography]]))


(defn coming-soon
  "Dispatches a 'coming soon' alert event."
  [_]
  (rp/dispatch [::e/show-alert {:message  "Coming soon!"
                                :severity :info}]))

(defn app-header
  "Application header with drawer button, title, profile menu and
  optional navigation buttons."
  [{:keys [title on-nav-menu-click navigation theme on-theme-change]}]
  (let [dark-theme? (= :dark theme)]
    [app-bar {:position  :static
              :color     :transparent
              :elevation 0}
     [toolbar
      [icon-button {:on-click   on-nav-menu-click
                    :size       :large
                    :edge       :start
                    :color      :inherit
                    :aria-label "menu"
                    :sx         {:mr 2}}
       [menu-icon]]
      [typography {:variant   :h6
                   :component :div}
       title]

      [stack {:direction :row
              :sx        {:flex-grow 1
                          :ml        1}}
       (when navigation navigation)]
      [stack {:direction :row
              :sx        {:ml 1}}
       [tooltip {:title       (if dark-theme? "Light mode" "Dark mode")
                 :enter-delay 500}
        [icon-button {:on-click on-theme-change}
         (if dark-theme?
           [light-mode-outlined]
           [dark-mode-outlined])]]
       [button {:color    :inherit
                :on-click coming-soon} "Login"]]]]))
