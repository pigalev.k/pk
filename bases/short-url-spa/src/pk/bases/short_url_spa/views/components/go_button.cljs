(ns pk.bases.short-url-spa.views.components.go-button
  (:require
   [reagent-mui.icons.arrow-circle-right-outlined
    :refer [arrow-circle-right-outlined]]
   [reagent-mui.material.icon-button :refer [icon-button]]))


(defn go-button
  "A primary action button (shorten the URL)."
  [{:keys [on-click disabled]}]
  [icon-button {:edge     :end
                :on-click on-click
                :disabled disabled}
   [arrow-circle-right-outlined {:color (if disabled :default :primary)}]])
