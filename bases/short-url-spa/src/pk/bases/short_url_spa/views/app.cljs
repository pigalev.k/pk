(ns pk.bases.short-url-spa.views.app
  (:require
   [pk.bases.short-url-spa.events :as e]
   [pk.bases.short-url-spa.routing :as routing]
   [pk.bases.short-url-spa.subscriptions :as s]
   [pk.bases.short-url-spa.views.components.app-alert :refer [app-alert]]
   [pk.bases.short-url-spa.views.components.app-header :refer [app-header]]
   [pk.bases.short-url-spa.views.components.app-nav-drawer
    :refer [app-nav-drawer]]
   [pk.bases.short-url-spa.views.components.app-shell :refer [app-shell]]
   [re-posh.core :as rp]
   [reagent-mui.styles :refer [create-theme theme-provider]]
   [reagent.session :as session]))


(session/put! :app-nav-drawer-open false)

(def themes {:light (create-theme {})
             :dark  (create-theme {:palette {:mode :dark}})})

(def next-theme {:light :dark
                 :dark  :light})


(defn app
  "An application view."
  []
  (let [app-id             @(rp/subscribe [::s/app-id])
        app-data           @(rp/subscribe [::s/app app-id])
        app-title          @(rp/subscribe [::s/app-title app-id])
        app-theme          @(rp/subscribe [::s/app-theme app-id])
        alert-id           @(rp/subscribe [::s/app-alert-id app-id])
        alert-message      @(rp/subscribe [::s/app-alert-message alert-id])
        alert-severity     @(rp/subscribe [::s/app-alert-severity alert-id])
        alert-open         @(rp/subscribe [::s/app-alert-open app-id])
        route-data         (session/get :route)
        change-theme       (fn [_]
                             (rp/dispatch
                              [::e/app-change-theme (next-theme app-theme)]))
        open-nav-drawer    (fn [_] (session/put! :app-nav-drawer-open true))
        close-nav-drawer   (fn [_] (session/put! :app-nav-drawer-open false))
        {:keys [current-page
                path-params
                query-params
                fragment]} route-data]
    (when app-id
      [theme-provider (app-theme themes)
       [app-shell {:header  [app-header
                             {:title             app-title
                              :theme             app-theme
                              :on-theme-change   change-theme
                              :on-nav-menu-click open-nav-drawer
                              #_#_:navigation    [app-nav
                                                  {:routes (routing/routes)}]}]
                   :drawer  [app-nav-drawer {:open
                                             (session/get :app-nav-drawer-open)
                                             :on-close close-nav-drawer
                                             :routes   (routing/routes)}]
                   :content [current-page {:path-params    path-params
                                           :query-params   query-params
                                           :fragment       fragment
                                           #_#_:debug-info app-data}]
                   :alert   [app-alert {:open     alert-open
                                        :message  alert-message
                                        :severity alert-severity
                                        :on-close
                                        #(rp/dispatch [::e/hide-alert])}]}]])))
