(ns pk.bases.short-url-spa.views.pages.short-urls
  (:refer-clojure :exclude [list])
  (:require
   [pk.bases.short-url-spa.events :as e]
   [pk.bases.short-url-spa.subscriptions :as subs]
   [pk.bases.short-url-spa.views.components.url-table :refer [url-table]]
   [re-posh.core :as rp]
   [reagent-mui.material.container :refer [container]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.material.typography :refer [typography]]
   [reagent.session :as session]))


(defn short-urls
  "A page with a list of all short URLs.
  On visit starts the process of retrieval of all existing short URLs
  from the API service."
  [_]
  (let [_ (rp/dispatch [::e/get-all-short-urls])]
    (fn [_]
      (let [short-url-ids          @(rp/subscribe [::subs/all-short-url-ids])
            short-urls             @(rp/subscribe
                                     [::subs/all-short-urls
                                      (reduce into [] short-url-ids)])
            app-id                 @(rp/subscribe [::subs/app-id])
            selected-short-url-ids @(rp/subscribe
                                     [::subs/app-selected-short-url-ids app-id])
            loading                (session/get :short-urls/loading-status)
            on-select              (fn [short-url-ids]
                                     (rp/dispatch
                                      [::e/set-selected-short-url-ids
                                       (js->clj short-url-ids)]))
            on-delete              (fn [_] (rp/dispatch
                                            [::e/delete-selected-short-urls]))]
        [container {:sx        {:height   "90vh"
                                :overflow :auto}
                    :max-width :md}
         [stack {:direction       :column
                 :align-items     :stretch
                 :justify-content :center
                 :sx              {:width  "100%"
                                   :height "100%"}}
          [typography {:color   "text.secondary"
                       :variant :h6
                       :sx      {:mb         1
                                 :text-align :center}}
           "My URLs"]
          [url-table {:short-urls         short-urls
                      :loading            loading
                      :on-select          on-select
                      :show-delete-button (boolean (seq selected-short-url-ids))
                      :on-delete          on-delete}]]]))))
