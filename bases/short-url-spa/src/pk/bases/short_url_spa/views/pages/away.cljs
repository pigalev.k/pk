(ns pk.bases.short-url-spa.views.pages.away
  (:require
   [pk.bases.short-url-spa.events :as e]
   [re-posh.core :as rp]
   [reagent-mui.material.circular-progress :refer [circular-progress]]
   [reagent-mui.material.container :refer [container]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.material.typography :refer [typography]]))


(defn away
  "A page that unshortens the short URL.
  On visit starts the process of retrieval of the original URL from
  API service by the `:short-string` from `:path-params` and
  redirection to it."
  [{:keys [path-params]}]
  (let [{:keys [short-string]} path-params
        _ (rp/dispatch [::e/unshorten-url short-string])]
    (fn [_]
      [container {:sx        {:height "90vh"}
                  :max-width :sm}
       [stack {:direction       :column
               :align-items     :center
               :justify-content :center
               :sx              {:height "100%"}}
        [typography {:color   "text.secondary"
                     :variant :h6
                     :sx {:mb 2}}
         "Redirecting"]
        [circular-progress]]])))
