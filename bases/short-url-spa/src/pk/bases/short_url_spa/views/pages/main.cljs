(ns pk.bases.short-url-spa.views.pages.main
  (:require
   [pk.bases.short-url-spa.domain :as domain]
   [pk.bases.short-url-spa.events :as e]
   [pk.bases.short-url-spa.subscriptions :as s]
   [pk.bases.short-url-spa.views.components.url-input-field
    :refer [url-input-field]]
   [pk.bases.short-url-spa.views.components.url-link :refer [url-link]]
   [pk.components.alias.core]
   [re-posh.core :as rp]
   [reagent-mui.material.container :refer [container]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.material.typography :refer [typography]]))


(defn main
  "A main page of the application."
  [_]
  (let [handle-input    (fn [event]
                          (rp/dispatch [::e/set-short-url ""])
                          (rp/dispatch [::e/set-original-url
                                        (.-value (.-target event))]))
        shorten-url     (fn [_]
                          (rp/dispatch [::e/shorten-url]))
        show-copy-alert (fn [_]
                          (rp/dispatch [::e/show-alert {:message  "Copied!"
                                                        :severity :info}]))]
    (fn [_]
      (let [app-id       @(rp/subscribe [::s/app-id])
            original-url @(rp/subscribe [::s/app-original-url app-id])
            base-url     @(rp/subscribe [::s/app-base-url app-id])
            short-url    @(rp/subscribe [::s/app-short-url app-id])]
        [container {:sx        {:height "90vh"}
                    :max-width :sm}
         [stack {:direction       :column
                 :align-items     :center
                 :justify-content :center
                 :sx              {:height "100%"}}
          [typography {:color   "text.secondary"
                       :variant :h6}
           "Shorten your URL and get it back again"]
          [url-input-field {:url             original-url
                            :on-action       shorten-url
                            :action-disabled (not (seq original-url))
                            :on-change       handle-input}]
          [url-link {:base-url     base-url
                     :short-string (:short short-url)
                     :placeholder  "paste the URL and hit enter"
                     :on-copy      show-copy-alert}]]]))))
