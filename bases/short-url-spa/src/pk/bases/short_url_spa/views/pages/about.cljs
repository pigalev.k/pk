(ns pk.bases.short-url-spa.views.pages.about
  (:require
   [pk.bases.short-url-spa.views.components.about-text :refer [about-text]]
   [reagent-mui.material.container :refer [container]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.material.typography :refer [typography]]))



(defn about
  "An about page."
  [_]
  [container {:sx        {:height "90vh"}
              :max-width :sm}
   [stack {:direction       :column
           :justify-content :center
           :sx              {:height "100%"}}
    [typography {:color   "text.secondary"
                 :variant :h6
                 :sx      {:text-align :center
                           :mb         1}}
     "About the app"]
    [about-text]]])
