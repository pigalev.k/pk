(ns pk.bases.short-url-spa.views.pages.not-found
  (:require
   [reagent-mui.material.container :refer [container]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.material.typography :refer [typography]]))


(defn not-found
  "A 404 Not Found page."
  [_]
  [container {:sx        {:height "90vh"}
              :max-width :sm}
   [stack {:direction       :column
           :align-items     :center
           :justify-content :center
           :sx              {:height "100%"}}
    [typography {:color   "text.secondary"
                 :variant :h2}
     "Page not found"]]])
