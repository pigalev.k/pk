;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; configure default Cider cljs REPL
((nil . ((cider-preferred-build-tool . shadow-cljs)
         (cider-default-cljs-repl . shadow)
         (cider-shadow-default-options . "app")
         (cider-clojure-cli-aliases . ":dev:cljs")
         (cider-clojure-cli-parameters . ""))))
