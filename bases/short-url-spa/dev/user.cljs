(ns user
  (:require
   [debux.cs.core :as dbg
    :refer-macros [clog clogn dbg dbgn break
                   clog_ clogn_ dbg_ dbgn_ break_]]
   [pk.bases.short-url-spa.subscriptions :as subs]
   [re-posh.core :as rp]
   [re-posh.db :as db]))


(println "user ns loaded")


(comment

  (def s (rp/subscribe [::subs/app-id]))
  (dbgn @s)

  db/store

)

;; debugging with debux

(comment

  ;; - `dbg`,`dbgn`: output anywhere (cljs/cljs REPLs, browser console)
  ;; - `clog`, `clogn`, `break`: only to browser console, but prettier

  ;; `n` means "nested forms".


  ;; basics

  ;; one form

  (* 2 (dbg (+ 10 20)))
  (* 2 (dbgn (+ 10 20)))
  (* 2 (clog (+ 10 20)))
  (* 2 (clogn (+ 10 20)))

  ;; multiple forms

  (defn my-fun
    [a {:keys [b c d] :or {d 10 b 20 c 30}} [e f g & h]]
    (dbg [a b c d e f g h]))

  (my-fun (take 5 (range)) {:c 50 :d 100} ["a" "b" "c" "d" "e"])

  (defn my-fun2
    [a {:keys [b c d] :or {d 10 b 20 c 30}} [e f g & h]]
    (dbgn [a b c d e f g h]))

  (my-fun2 (take 5 (range)) {:c 50 :d 100} ["a" "b" "c" "d" "e"])

  ;; thread macros

  (dbg (-> "a b c d"
           .toUpperCase
           (.replace "A" "X")
           (.split " ")
           first))

  (-> {:a [1 2]}
      (get :a)
      dbg
      (conj 3))

  ;; let

  (dbg (let [a (take 5 (range))
             {:keys [b c d] :or {d 10 b 20 c 30}} {:c 50 :d 100}
             [e f g & h] ["a" "b" "c" "d" "e"]]
         [a b c d e f g h]))

  ;; comp

  (def c (dbg (comp inc inc +)))
  (c 10 20)

  ;; nested forms

  (dbgn (defn foo [a b & [c]]
          (if c
            (* a b c)
            (* a b 100))))

  (foo 2 3)
  (foo 2 3 10)

  ;; tagged literals

  #d/dbg (+ 1 2 #d/dbg (* 3 4))


  ;; break

  (break)
  (break "hello world")
  (break :if (> 10 20) "this will not be printed")
  (break :if (< 10 20) "10 is less than 20")

)
