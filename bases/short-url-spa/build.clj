(ns build
  "Base tasks --- building, installing, etc."
  (:refer-clojure :exclude [compile])
  (:require
   [build.api :as a]
   [clojure.tools.build.api :as b]))


(def project-name "short-url-spa")
(def lib (symbol (format "pk/%s" project-name)))
(def version (format "0.1.%s" (b/git-count-revs nil)))

(def shadow-cljs-build-name "app")
(def shadow-cljs-build-mode "release")
(def cljs-build-dir "resources/public/js/compiled/")

(def src-dirs ["src" "resources"])
(def build-dir "target/")
(def class-dir (str build-dir "classes/"))
(def basis (b/create-basis {:project "deps.edn"}))

(def jar-file (format "%s-%s.jar" project-name version))


(a/set-config! {:project-name           project-name
                :lib                    lib
                :version                version
                :src-dirs               src-dirs
                :build-dir              build-dir
                :cljs-build-dir         cljs-build-dir
                :shadow-cljs-command    ["yarn" "shadow-cljs"]
                :shadow-cljs-build-name shadow-cljs-build-name
                :shadow-cljs-build-mode shadow-cljs-build-mode
                :class-dir              class-dir
                :basis                  basis
                :jar-file               jar-file})


;; misc

(def help
  "List all available tasks with summaries of their docstrings."
  a/help)

(def clean
  "Clean all build results."
  a/clean)


;; building

(def compile
  "Compile ClojureScript sources."
  a/compile-cljs-shadow)

(def jar
  "Pack the sources in a jar file."
  a/library-jar)


;; installing

(def install
  "Install sources jar into local Maven repository."
  a/install-library)
