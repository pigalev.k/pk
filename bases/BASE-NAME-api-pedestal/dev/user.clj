(ns user
  "Managing the service's system and reloading changed sources during
  development. Also allows to inspect the running system."
  (:require
   [clojure.tools.namespace.repl :refer [set-refresh-dirs]]
   [datahike.core :as d]
   [integrant.core :as ig]
   [integrant.repl :refer [clear go halt set-prep! reset reset-all]]
   [integrant.repl.state :as rs]
   [kaocha.repl :as k]
   [pk.bases.BASE-NAME-api.core :as c]
   [portal.api :as pa]
   [taoensso.timbre :as timbre]))


;; portal

(comment

  ;; open a new inspector
  (def p (pa/open))
  ;; add portal as a tap> target
  (add-tap #'pa/submit)
  ;; start tapping out values
  (tap> :hello)
  ;; bring selected value back into repl
  (prn @p)
  ;; remove portal from tap> targetset
  (remove-tap #'pa/submit)
  ;; close the inspector when done
  (pa/close)

)


;; integrant repl

(defn config
  "Read and return service config."
  []
  (c/config (c/config-resource-name) (c/profile)))

(defn init-ig-repl
  "Initialize Integrant REPL.
  Configures `ig/repl` functions, loads all needed namespaces and watches for
  changed files."
  []
  (set-refresh-dirs "src")
  (set-prep! (fn []
               (let [conf (config)]
                 (c/init-logging!
                  {:min-level         [["org.eclipse.jetty.*" :info]
                                       ["*" :debug]]
                   :color-stacktrace? true})
                 (ig/load-namespaces conf)
                 (ig/prep conf)))))

(init-ig-repl)

(comment

  ;; prepare and init the system in one step
  ;; (also `prep` and `init` can be run separately)
  (go)

  ;; the system configuration is stored in `integrant.repl.state/config`
  rs/config
  rs/system

  ;; stop the system
  (halt)

  ;; stop the system and clear the system configuration
  (clear)

  ;; reload changed source files and restart the system
  (reset)

  ;; reload all source files and restart the system
  (reset-all)


  (keys rs/system)

  (def conn (:pk.bases.BASE-NAME-api.database/datahike rs/system))
  (d/q '[:find (pull ?e [*])
         :where [?e :ENTITY/id #uuid "2c2791d1-392b-4a6f-aaec-4475ab433c3b"]]
       @conn)

  ;; to fix the alias redefinition error, so C-c C-k works again

  (ns-unalias *ns* 'c)

  timbre/*config*

)


;; running tests

(comment

  ;; run a suite

  (k/run :unit)

  ;; run all tests

  (k/run-all)

)
