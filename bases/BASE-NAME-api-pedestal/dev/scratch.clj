(ns scratch
  "Experimentation playground for development.
  To avoid cluttering source namespaces, dump all the temporary stuff here,
  preferably in the form of Rich comments. More scratches can be added, too.")
