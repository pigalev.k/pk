(ns pk.bases.BASE-NAME-api.spec-test
  (:require
   [clojure.spec.alpha :as s]
   [clojure.test :refer [deftest testing are]]
   [pk.bases.BASE-NAME-api.spec]))


;; TODO: add generative tests?

(deftest spec-coercions-test
  (testing "types are coerced correctly"
    (are [input conformed] (= conformed
                              (s/conform :coercion/string->integer input))
      "0"  0
      "1"  1
      "-1" -1
      1    ::s/invalid
      "a"  ::s/invalid)
    (are [input conformed] (= conformed
                              (s/conform :coercion/string->keyword input))
      "a"     :a
      "abc/d" :abc/d)
    (are [input conformed] (= conformed
                              (s/conform :coercion/string->uuid input))
      "a35bee00-8ed4-48c7-8002-ecfe2bf089fe"
      #uuid "a35bee00-8ed4-48c7-8002-ecfe2bf089fe"
      1   ::s/invalid
      "a" ::s/invalid)
    (are [input conformed] (= conformed
                              (s/conform :coercion/string->inst input))
      "2023-03-31T10:48:40.179-00:00" #inst "2023-03-31T10:48:40.179-00:00"
      "abc"                           ::s/invalid)))
