(ns pk.bases.BASE-NAME-api.core-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [pk.bases.BASE-NAME-api.core :as core]))


(deftest config--test
  (testing "service configuration"
    (let [config-resource-name "resources/test-config.edn"
          profile :dev
          config (core/config config-resource-name profile)]
      (is (map? config) "is a map")
      (is (= 123 (:config/key config)) "have a correct key and value"))))
