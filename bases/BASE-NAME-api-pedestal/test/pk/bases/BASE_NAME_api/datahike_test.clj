(ns pk.bases.BASE-NAME-api.datahike-test
  (:require
   [clojure.test :refer [deftest testing are]]
   [pk.bases.BASE-NAME-api.domain.ENTITY.store.datahike :as dstore]))

(deftest namespace-keys-test
  (testing "namespace-keys"
    (are [input result] (= (dstore/namespace-keys input "user") result)
      {:id 1}                            {:user/id 1}
      {"id" 1}                           {:user/id 1}
      {:some/id 1}                       {:user/id 1}
      {"some/id" 1}                      {:user/some/id 1}
      {:id      1
       :name    "Joe"
       :friends [{:id 2 :name "Molly"}]} {:user/id   1
                                          :user/name "Joe"
                                          :user/friends
                                          [{:id 2 :name "Molly"}]})))

(deftest unnamespace-keys-test
  (testing "unnamespace-keys"
    (are [input result] (= (dstore/unnamespace-keys input) result)
      {:user/id 1}             {:id 1}
      {:user/id   1
       :user/name "Joe"
       :user/friends
       [{:user/id   2
         :user/name "Molly"
         :person/age 32}]} {:id      1
                            :name    "Joe"
                            :friends [{:id 2 :name "Molly" :age 32}]})))
