(ns pk.bases.BASE-NAME-api.spec
  (:require
   [clojure.spec.alpha :as s]
   [clojure.string :as strng]
   [pk.bases.BASE-NAME-api.domain.ENTITY.store.datahike :as dstore])
  (:import
   (java.time Instant)
   (java.util Date UUID)))


;; helpers

(defn explain-errors
  "Formats (more or less) readable error messages for spec data
  validation error `explain-data`, as provided by
  `clojure.spec.alpha/explain-data`."
  [explain-data]
  (let [problems                     (::s/problems explain-data)
        problem-attrs-and-specs      (map (juxt :in :via :pred) problems)
        last-problem-attrs-and-specs (map (fn [[in via pred]]
                                            (vector (peek in)
                                                    (peek via)
                                                    (pr-str pred)))
                                          problem-attrs-and-specs)
        format-error-fn (fn [[attr spec pred]]
                          (if attr
                            (format
                             "attribute %s failed spec %s, predicate %s"
                             attr spec pred)
                            (format
                             "data failed spec: %s, predicate %s"
                             spec pred)))
        formatted-errors (map format-error-fn
                              last-problem-attrs-and-specs)]
    (strng/join "; " formatted-errors)))


;; specs, conformers and predicates

(s/def :type/string string?)
(s/def :type/keyword keyword?)
(s/def :type/integer int?)
(s/def :type/uuid uuid?)
(s/def :type/inst inst?)

(s/def :type/string.nonempty (s/and :type/string (complement empty?)))
(s/def :type/integer.positive (s/and :type/integer #(>= % 0)))


(defn string->integer
  "Conforms `string` to integer."
  [string]
  (try (Integer/parseInt string)
       (catch Exception _ ::s/invalid)))

(defn string->keyword
  "Conforms `string` to keyword."
  [string]
  (try (keyword string)
       (catch Exception _ ::s/invalid)))

(defn string->symbol
  "Conforms `string` to symbol."
  [string]
  (try (symbol string)
       (catch Exception _ ::s/invalid)))

(defn string->uuid
  "Conforms `string` to UUID."
  [string]
  (try (UUID/fromString string)
       (catch Exception _ ::s/invalid)))

(defn string->inst
  "Conforms `string` to instant in time (i.e., `java.util.Date`)."
  [string]
  (try (Date/from (Instant/parse string))
       (catch Exception _ ::s/invalid)))


(s/def :coercion/string->uuid
  (s/and :type/string (s/conformer string->uuid identity) :type/uuid))
(s/def :coercion/string->inst
  (s/and :type/string (s/conformer string->inst identity) :type/inst))
(s/def :coercion/string->integer
  (s/and :type/string (s/conformer string->integer identity) :type/integer))
(s/def :coercion/string->integer.positive
  (s/and :type/string :coercion/string->integer :type/integer.positive))
(s/def :coercion/string->keyword
  (s/and :type/string (s/conformer string->keyword identity) :type/keyword))
(s/def :coercion/string.nonempty->keyword
  (s/and :type/string.nonempty
         (s/conformer string->keyword identity) :type/keyword))


;; domain ENTITY data specs

(s/def :domain.ENTITY/id :type/uuid)
(s/def :domain.ENTITY/name :type/string)
(s/def :domain.ENTITY/created-at :type/inst)
(s/def :domain/ENTITY (s/nilable (s/keys :opt-un [:domain.ENTITY/id
                                                  :domain.ENTITY/name
                                                  :domain.ENTITY/created-at])))

(s/def :domain.ENTITY.string->/id :coercion/string->uuid)
(s/def :domain.ENTITY.string->/created-at :coercion/string->inst)
(s/def :domain/map->ENTITY
  (s/nilable (s/keys :opt-un [:domain.ENTITY.string->/id
                              :domain.ENTITY.string->/created-at])))

(s/def :domain/->ENTITY (s/and :domain/map->ENTITY :domain/ENTITY))


;; request and response data specs

;; parameters that come in JSON/EDN/... request body and are placed in a map
;; with keyword keys, values need both coercion and validation

(s/def :request.params/find-ENTITY-by-id
  (s/keys :req-un [:domain.ENTITY.string->/id]))
(s/def :request.params/create-ENTITY :domain/->ENTITY)
(s/def :request.params/replace-update-ENTITY :domain/->ENTITY)

;; parameters that come in query parameters (all strings) and are placed in a
;; map with keyword keys, values need both coercion and validation

(defn string->query-params-map
  "Conforms `string` to query parameters map."
  [string]
  (try
    (let [[predicate attribute & arguments]
          (remove empty? (strng/split string #":"))]
      {:predicate predicate
       :attribute attribute
       :arguments arguments})
    (catch Exception _ ::s/invalid)))

(defn string->sort-params-map
  "Conforms `string` to sort parameters map."
  [string]
  (try
    (let [[attribute order] (remove empty? (strng/split string #":"))]
      {:attribute attribute
       :order     order})
    (catch Exception _ ::s/invalid)))


(comment

  (string->query-params-map "like:foo:bar:baz:quux")
  (string->query-params-map ": eq:some-attr:some value: and another:")
  (string->query-params-map "eq:some-attr:some value: and another:")
  (string->sort-params-map "some-attr:asc")

)

;; query params

(s/def :request.params.find-ENTITIES/pattern vector?)
(s/def :request.params.find-ENTITIES.query/predicate
  (s/and :coercion/string->keyword
         (set (keys dstore/supported-query-predicates))))
(s/def :request.params.find-ENTITIES.query/attribute :type/string.nonempty)
(s/def :request.params.find-ENTITIES.query/argument  :type/string.nonempty)
(s/def :request.params.find-ENTITIES.query/arguments
  (s/* :request.params.find-ENTITIES.query/argument))

(s/def :request.params.find-ENTITIES/query-params-string :type/string.nonempty)
(s/def :request.params.find-ENTITIES/query-params-map
  (s/keys :req-un [:request.params.find-ENTITIES.query/predicate
                   :request.params.find-ENTITIES.query/attribute
                   :request.params.find-ENTITIES.query/arguments]))

(s/def :request.params.find-ENTITIES/query
  (s/and :request.params.find-ENTITIES/query-params-string
         (s/conformer string->query-params-map identity)
         :request.params.find-ENTITIES/query-params-map))

(comment

  (s/conform :request.params.find-ENTITIES/query "eq:foo")
  (s/conform :request.params.find-ENTITIES/query ":eq:foo:bar:baz:quux")

)

;; sort params

(s/def :request.params.find-ENTITIES.sort/attribute
  :coercion/string.nonempty->keyword)
(s/def :request.params.find-ENTITIES.sort/order
  (s/nilable (s/and :coercion/string.nonempty->keyword #{:asc :desc})))

(s/def :request.params.find-ENTITIES/sort-params-string :type/string.nonempty)
(s/def :request.params.find-ENTITIES/sort-params-map
  (s/keys :req-un [:request.params.find-ENTITIES.sort/attribute
                   :request.params.find-ENTITIES.sort/order]))

(s/def :request.params.find-ENTITIES/sort
  (s/and :request.params.find-ENTITIES/sort-params-string
         (s/conformer string->sort-params-map identity)
         :request.params.find-ENTITIES/sort-params-map))

;; pagination params

(s/def :request.params.find-ENTITIES/offset :coercion/string->integer.positive)
(s/def :request.params.find-ENTITIES/limit :coercion/string->integer.positive)

(s/def :request.params/find-ENTITIES
  (s/nilable (s/keys :opt-un [:request.params.find-ENTITIES/query
                              :request.params.find-ENTITIES/pattern
                              :request.params.find-ENTITIES/offset
                              :request.params.find-ENTITIES/limit
                              :request.params.find-ENTITIES/sort])))

(comment

  (s/conform :request.params.find-ENTITIES/query "eq:a:b:c")
  (s/explain-data :request.params.find-ENTITIES/query "eq:b")
  (s/explain-data :request.params.find-ENTITIES/query "eql:a:b:c")

  (s/conform :request.params/find-ENTITIES {})
  (s/conform :request.params/find-ENTITIES nil)
  (s/conform :request.params/find-ENTITIES {:query   "eq:name:Joe"
                                            :pattern "[*]"
                                            :limit   "2"
                                            :offset  "1"
                                            :order   "desc"})

)


;; responses are currently not validated, but the need can arise (really?)

(s/def :response.body/ENTITY (s/keys :opt-un [:ENTITY/id :ENTITY/name]))
(s/def :response.body/ENTITIES (s/coll-of :request/ENTITY :kind vector?))
(s/def :response.body/data (s/or :ENTITY :request/ENTITY
                                 :ENTITIES :request/ENTITIES))
(s/def :response/ok (s/keys :req-un [:response.body/data]))


;; system component specs

;; validated on system (re)start, no coercion (config reader can apply it's own
;; coercions if needed)

(s/def :type/network.port (s/int-in 1 65536))

;; stores (repositories) for the domain entities

(defn datahike-db-connection? [value]
  (and (instance? clojure.lang.Atom value)
       (instance? datahike.db.DB @value)))

(s/def :domain.ENTITY.store.datahike/schema
  (s/nilable (s/coll-of map? :kind vector?)))
(s/def :domain.ENTITY.store.datahike/initial-data
  (s/nilable (s/coll-of :domain/ENTITY :kind vector?)))
(s/def :domain.ENTITY.store.datahike/connection datahike-db-connection?)
;; used to validate sytem component
(s/def :domain.ENTITY.store/datahike
  (s/keys :req-un [:domain.ENTITY.store.datahike/connection]
          :opt-un [:domain.ENTITY.store.datahike/schema
                   :domain.ENTITY.store.datahike/initial-data]))


;; databases

(s/def :database.datahike.db-spec.store/backend #{:jdbc :mem})
(s/def :database.datahike.db-spec.store/dbname :type/string.nonempty)
(s/def :database.datahike.db-spec.store/dbtype :type/string.nonempty)
(s/def :database.datahike.db-spec.store/host :type/string.nonempty)
(s/def :database.datahike.db-spec.store/port :type/network.port)
(s/def :database.datahike.db-spec.store/user :type/string.nonempty)
(s/def :database.datahike.db-spec.store/password :type/string.nonempty)
(s/def :database.datahike.db-spec/store
  (s/keys :req-un [:database.datahike.db-spec.store/backend]
          :opt-un [:database.datahike.db-spec.store/dbname
                   :database.datahike.db-spec.store/dbtype
                   :database.datahike.db-spec.store/host
                   :database.datahike.db-spec.store/port
                   :database.datahike.db-spec.store/user
                   :database.datahike.db-spec.store/password]))
(s/def :database.datahike.db-spec/name :type/string.nonempty)
(s/def :database.datahike.db-spec/schema-flexibility? boolean?)
(s/def :database.datahike.db-spec/keep-history? boolean?)
(s/def :database.datahike.db-spec/attribute-refs? boolean?)
(s/def :database.datahike/db-spec
  (s/keys :req-un [:database.datahike.db-spec/store]
          :opt-un [:database.datahike.db-spec/name
                   :database.datahike.db-spec/schema-flexibility?
                   :database.datahike.db-spec/keep-history?
                   :database.datahike.db-spec/attribute-refs?]))
(s/def :database.datahike/delete-db boolean?)
;; used to validate sytem component
(s/def :database/datahike (s/keys :req-un [:database.datahike/db-spec
                                           :database.datahike/delete-db]))


;; web

;; routes

(s/def :web.routes/ENTITY-store :domain.ENTITY.store/datahike)
;; used to validate sytem component
(s/def :web/routes (s/keys :req-un [:web.routes/ENTITY-store]))

;; server

(s/def :web.server.allowed-origins/creds boolean?)
(s/def :web.server.allowed-origins/allowed-origins
  (s/nilable (s/coll-of :type/string :kind vector?)))
(s/def :web.server/type #{:jetty})
(s/def :web.server/host :type/string.nonempty)
(s/def :web.server/port :type/network.port)
(s/def :web.server/join? boolean?)
(s/def :web.server/allowed-origins
  (s/keys :req-un [:web.server.allowed-origins/creds
                   :web.server.allowed-origins/allowed-origins]))
(s/def :web.server/routes (s/coll-of map?))
;; used to validate sytem component
(s/def :web/server (s/keys :req-un [:web.server/type
                                    :web.server/host
                                    :web.server/port
                                    :web.server/join?
                                    :web.server/routes]
                           :opt-un [:web.server/allowed-origins]))
