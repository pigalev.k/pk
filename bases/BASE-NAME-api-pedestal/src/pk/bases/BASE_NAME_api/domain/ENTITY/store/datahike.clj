(ns pk.bases.BASE-NAME-api.domain.ENTITY.store.datahike
  "Datahike ENTITY store."
  (:refer-clojure :exclude [find update replace sort])
  (:require
   [clojure.core :as c]
   [clojure.walk :as w]
   [datahike.api :as d]
   [datahike-jdbc.core]
   [pk.components.ENTITY.api :as a]
   [taoensso.timbre :as timbre])
  (:import (java.util Date)
           (java.time Instant)))


(def entity-name
  "A namespace for ENTITY attributes in db."
  "ENTITY")

(defn namespace-keys
  "Adds namespace `ns` to all keys in `m`.
  Keys should be strings or keywords. If keyword keys are already
  namespaced, replaces their namespaces with `ns`."
  [m ns]
  (update-keys m #(keyword ns (name %))))

(defn unnamespace-keys
  "Recursively removes namespaces from all keys in `m`.
  Keys should be keywords."
  [m]
  (let [f (fn [[k v]] [(keyword (name k)) v])]
    (w/postwalk (fn [x] (if (map? x) (into {} (map f x)) x)) m)))


;; schema

;; transactions

(defn insert-ENTITY-tx-data
  "Returns transaction data to insert a new instance of ENTITY.
  Default creation date/time can be provided in options, or current
  instant will be used as default."
  [ENTITY & {:keys [created-at]
             :or   {created-at (Date.)}}]
  (let [ENTITY-id (a/id ENTITY)]
    [(-> {:created-at created-at}
         (merge ENTITY)
         (namespace-keys entity-name)
         (assoc :db/id (- (abs (hash ENTITY-id)))))]))

(comment

  (def entity1 {:id #uuid "2c2791d1-392b-4a6f-aaec-4475ab433c3b"})
  (def entity2 {:id   #uuid "2c2791d1-392b-4a6f-aaec-4475ab433c3b"
                :name "a name"})
  (def entity3 {:id         #uuid "2c2791d1-392b-4a6f-aaec-4475ab433c3b"
                :name       "a name"
                :created-at (Date/from
                             (Instant/parse "1999-01-01T10:30:38Z"))})

  ;; without explicit and default creation date/time set, current instant used
  (insert-ENTITY-tx-data entity1)
  (insert-ENTITY-tx-data entity2)
  ;; with explicit creation date/time
  (insert-ENTITY-tx-data entity3)
  ;; with default creation date/time, not explicitly set
  (insert-ENTITY-tx-data entity2
                         {:created-at (Date/from
                                       (Instant/parse "1969-01-01T10:30:38Z"))})
  ;; with default creation date/time, and also explicitly set
  (insert-ENTITY-tx-data entity3
                         {:created-at (Date/from
                                       (Instant/parse "1969-01-01T10:30:38Z"))})

  ;; TODO: extract to tests
)


#_
(defn insert-ENTITIES-tx-data
  "Returns transaction data to insert several ENTITIES at once."
  [ENTITIES & {:keys [created-at] :or {created-at (Date.)}}]
  (mapcat insert-ENTITY-tx-data ENTITIES (repeat {:created-at created-at})))

#_
(comment

  (def entity1 {:id #uuid "2c2791d1-392b-4a6f-aaec-4475ab433c3b"})
  (def entity2 {:id   #uuid "2c2791d1-392b-4a6f-aaec-4475ab433c3b"
                :name "a name"})
  (def entity3 {:id         #uuid "2c2791d1-392b-4a6f-aaec-4475ab433c3b"
                :name       "a name"
                :created-at (Date/from
                             (Instant/parse "1999-01-01T10:30:38Z"))})

  ;; without default creation date/time
  (->> (insert-ENTITIES-tx-data [entity1 entity2 entity3])
       (map :ENTITY/created-at))
  ;; with default creation date/time
  (->> (insert-ENTITIES-tx-data
        [entity1 entity2 entity3]
        {:created-at (Date/from
                      (Instant/parse "1969-01-01T10:30:38Z"))})
       (map :ENTITY/created-at))

)

(defn update-ENTITY-tx-data
  "Returns transaction data to update an ENTITY.
  Attributes of existing entity not present in `ENTITY-data` will be
  left intact. If ENTITY ID is also present in `ENTITY-data`, it is
  ignored. ENTITY ID is a domain unique identifier, not internal,
  database-specific one."
  [id ENTITY-data]
  [(merge (-> ENTITY-data
              (dissoc :id)
              (namespace-keys entity-name))
          {:db/id [:ENTITY/id id]})])

(defn delete-ENTITY-tx-data
  "Returns transaction data to delete an ENTITY by ID. ID is a domain
  unique identifier, not internal, database-specific one."
  [id]
  [[:db/retractEntity [:ENTITY/id id]]])

(defn replace-ENTITY-tx-data
  "Returns transaction data to replace an ENTITY.
  Attributes of existing entity not present in `ENTITY-data` will be
  removed. If ENTITY ID is also present in `ENTITY-data`, it is
  ignored. ENTITY ID is a domain unique identifier, not internal,
  database-specific one; internal ID (:db/id) is not preserved by this
  operation (old entity is retracted and new one is inserted)."
  [id ENTITY-data]
  (concat (delete-ENTITY-tx-data id)
          (insert-ENTITY-tx-data (assoc ENTITY-data :id id))))


;; queries

(defn like
  "Returns true if `s` matches `regex`."
  [s re-str]
  (boolean (re-find (re-pattern re-str) s)))

(def supported-query-predicates
  {:eq   `=
   :like `like})

(def ENTITY-default-pull-pattern
  "A default pull pattern for ENTITY --- all attributes."
  '(pull ?e [*]))

(defn find-ENTITIES-query
  "Returns a query to find ENTITIES.
  Uses extended map (arg-map) format; see documentation to `d/q`.

  Parameters:
  - `db`: a Datahike db value.
  - `params`: a map of search/pagination/sorting parameters.
    - `query`: a search query description, contains
      - `predicate`: a predicate that will be called to determine which entities
        to return. See `supported-query-predicates` for the full list.
      - `attribute`: an ENTITY attribute name, whose value will be the first
        argument to `predicate`.
      - `arguments`: additional arguments to predicate, if any.
    - `pattern`: datalog pull pattern; determines what attributes of the found
      entities will be returned (default is wildcard pattern, all attributes).
    - `offset`: a number of results that will be dropped from the start of
      results list.
    - `limit`: a maximum number of results to return."
  [db {:keys [query pattern offset limit]
       :or   {pattern ENTITY-default-pull-pattern
              offset  0 limit -1}}]
  (let [{:keys [predicate attribute arguments]}
        query
        predicate-symbol   (supported-query-predicates predicate)
        attribute-name     (and attribute (keyword entity-name attribute))
        attribute-variable (and attribute (symbol (str "?" attribute)))]
    (cond-> {:query {:find [[pattern '...]]
                     :where []}
             :offset offset
             :limit limit
             :args [db]}
      ;; query is empty or absent; find all entities (return ENTITIES that have
      ;; an ENTITY id)
      (not (seq query)) (update-in [:query :where]
                                   conj ['?e (keyword entity-name "id")])
      ;; attribute name is present; add clause that binds a variable to the
      ;; attribute value (return ENTITIES that have that attribute)
      attribute-name    (update-in [:query :where]
                                   conj ['?e attribute-name attribute-variable])
      ;; qualified predicate symbol is resolved; add predicate clause with
      ;; previously bound variable and additional arguments, if any (return
      ;; ENTITIES for which predicate call with the attribute value and
      ;; additional arguments returns true)
      predicate-symbol  (update-in [:query :where]
                                   conj `[(~predicate-symbol
                                           ~attribute-variable
                                           ~@(map #(symbol (str "?" %))
                                                  arguments))])
      ;; additional arguments are present
      (and predicate-symbol
           (seq arguments))   (->
                               ;; add argument names as input variable names
                               (assoc-in [:query :in]
                                         `[~'$
                                           ~@(map
                                              #(symbol (str "?" %)) arguments)])
                               ;; add argument values to the query arguments
                               (assoc-in [:args]
                                         `[~db ~@arguments])))))

(comment

  (def params {:query   {:attribute "some-attr"
                         :predicate :eq
                         :arguments ["a-value"]}
               #_#_:pattern []
               :offset      1
               :limit       1})
  (def params1 {:query   {:attribute "some-attr"
                          :predicate :nope
                          :arguments ["a-value"]}
                #_#_:pattern []
                :offset      1
                :limit       1})
  (def params2 {:query   {:attribute "some-attr"
                          :predicate :like}
                #_#_:pattern []})

  ;; attribute, valid predicate present
  (find-ENTITIES-query "db" params)
  ;; attribute present, no valid predicate (shouldn't happen anyway, spec
  ;; validation will not allow this)
  (find-ENTITIES-query "db" params1)
  ;; no additional arguments
  (find-ENTITIES-query "db" params2)
  ;; empty query params
  (find-ENTITIES-query "db" {})
  ;; no query params
  (find-ENTITIES-query "db" nil)

  ;; TODO:
  ;; - extract this to tests, add more cases
  ;; - search by non-string attributes (UUID, inst, etc)
  ;; - add multispec to coerce/validate arguments for specific predicates

)

(def find-ENTITY-by-id-query
  "A query to retrieve an ENTITY by ID."
  [:find ENTITY-default-pull-pattern '.
   :in '$ '?id
   :where
   '[?e :ENTITY/id ?id]])


;; the store

(defrecord DatalogENTITYStore [connection]
  a/ENTITYStore
  (insert [_ item]
    (d/transact connection (insert-ENTITY-tx-data item)))
  (update [_ id item]
    (d/transact connection (update-ENTITY-tx-data id item)))
  (replace [_ id item]
    (d/transact connection (replace-ENTITY-tx-data id item)))
  (delete [_ id]
    (d/transact connection (delete-ENTITY-tx-data id)))
  (find [_ {:keys [sort] :as params}]
    (let [datalog-query        (find-ENTITIES-query @connection params)
          {:keys [attribute order]
           :or   {order :asc}} sort]
      (timbre/debugf
       "finding ENTITIES, datalog query: %s" datalog-query)
      (cond->> (unnamespace-keys (d/q datalog-query))
        attribute (sort-by attribute (if (= :desc order)
                                       #(compare %2 %1)
                                       compare)))))
  (find-by-id [_ id]
    (unnamespace-keys (d/q find-ENTITY-by-id-query @connection id))))

(defn empty-db?
  "Checks if the current db value held by connection is empty (contains
  no schema or ENTITY data)."
  [connection]
  (empty? (d/schema @connection)))

(defn create-ENTITY-store
  "Create an ENTITY store backed by Datahike db.

  Parameters:
  - `connection`: a connection to db.
  - `schema`: schema that will be applied if it was not applied already
    (db is empty).
  - `initial-data`: initial data that will be transacted into db if
    there are no data yet (db is empty)."
  [connection schema initial-data]
  (when (empty-db? connection)
    (when schema
      (d/transact connection schema)
      (when initial-data
        (d/transact connection initial-data))))
  (->DatalogENTITYStore connection))
