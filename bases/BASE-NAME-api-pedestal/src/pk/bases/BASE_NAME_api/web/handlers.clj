(ns pk.bases.BASE-NAME-api.web.handlers
  (:require
   [pk.components.ENTITY.api :as a]
   [taoensso.timbre :as timbre]))


(defn format-body
  "Create a response body using a template for response data."
  [data]
  {:data data})

(defn response
  "Create a Ring response map."
  [status body & {:as headers}]
  {:status status :body body :headers headers})

(defn data-response
  "Create a Ring response map with templated body."
  [status body & {:as headers}]
  (response status (format-body body) headers))

(def ok (partial response 200))
(def no-content (partial response 204 nil))
(def bad-request (partial response 400))
(def not-found (partial response 404 nil))

(def data-ok (partial data-response 200))
(def data-created (partial data-response 201))


(defn create-ENTITY
  "Returns response with the newly created ENTITY."
  [{:keys [created-ENTITY] :as request}]
  (if created-ENTITY
    (do
      (timbre/infof "created new ENTITY: %s"
                    (:id created-ENTITY))
      (data-created created-ENTITY))
    (throw (ex-info "Cannot create ENTITY"
                    {:id (get-in request [:params :id])}))))

(defn get-ENTITIES
  "Returns response with ENTITIES found in the store."
  [{:keys [found-ENTITIES]}]
  (timbre/infof "retrieved ENTITIES: %s" (count found-ENTITIES))
  (data-ok found-ENTITIES))

(defn get-ENTITY
  "Returns response with an existing ENTITY."
  [{:keys [found-ENTITY] :as request}]
  (if found-ENTITY
    (do
      (timbre/infof "retrieved existing ENTITY: %s" (a/id found-ENTITY))
      (data-ok found-ENTITY))
    (do
      (timbre/warnf "ENTITY not found: %s" (get-in request [:params :id]))
      (not-found))))

(defn update-ENTITY
  "Returns response confirming ENTITY update."
  [{:keys [updated-ENTITY] :as request}]
  (if updated-ENTITY
    (do
      (timbre/infof "updated ENTITY: %s" (:id updated-ENTITY))
      (ok updated-ENTITY))
    (do
      (timbre/warnf "ENTITY not found: %s" (get-in request [:params :id]))
      (not-found))))

(defn replace-ENTITY
  "Returns response confirming ENTITY replace."
  [{:keys [replaced-ENTITY] :as request}]
  (if replaced-ENTITY
    (do
      (timbre/infof "replaced ENTITY: %s" (:id replaced-ENTITY))
      (ok replaced-ENTITY))
    (do
      (timbre/warnf "ENTITY not found: %s" (get-in request [:params :id]))
      (not-found))))

(defn delete-ENTITY
  "Returns response confirming ENTITY deletion."
  [{:keys [deleted-ENTITY] :as request}]
  (if deleted-ENTITY
    (do
      (timbre/infof "deleted ENTITY: %s" (:id deleted-ENTITY))
      (no-content))
    (do
      (timbre/warnf "ENTITY not found: %s" (get-in request [:params :id]))
      (not-found))))

(defn health
  "Returns a health-check response."
  [_]
  (ok "ok"))
