(ns pk.bases.BASE-NAME-api.web.routes
  "Request handling pipeline:

  - Interceptors that have '-cofx' postfix in the name (coeffect handlers)
    collect data necessary for handler to handle the request (coeffects), and
    store them in request. Side-effecting.

  - Handlers are functions that produce response map from request (enriched
    with coeffects). Pure.

  - Interceptors that have '-fx' postfix in the name (effect handlers) apply
    desired effects (modify the response, write to log etc). Side-effecting."
  (:require
   [io.pedestal.http.route :as route]
   [io.pedestal.interceptor :as pi]
   [io.pedestal.interceptor.helpers :as ih]
   [muuntaja.interceptor :as mi]
   [pk.bases.BASE-NAME-api.spec]
   [pk.bases.BASE-NAME-api.web.handlers :as h]
   [pk.bases.BASE-NAME-api.web.interceptors :as i]))


(defn routes
  "Returns a set of endpoints that should be served by the service."
  [{:keys [ENTITY-store]}]
  (let [;; interceptors
        find-ENTITY-by-id-cofx
        (i/find-ENTITY-by-id-cofx ENTITY-store)
        find-ENTITIES-cofx
        (i/find-ENTITIES-cofx ENTITY-store)
        create-ENTITY-fx
        (i/create-ENTITY-fx ENTITY-store)
        update-ENTITY-fx
        (i/update-ENTITY-fx ENTITY-store)
        replace-ENTITY-fx
        (i/replace-ENTITY-fx ENTITY-store)
        delete-ENTITY-fx
        (i/delete-ENTITY-fx ENTITY-store)
        conform-params-create-ENTITY-cofx
        (i/conform-params-cofx :body-params
                               :request.params/create-ENTITY)
        conform-params-replace-update-ENTITY-cofx
        (i/conform-params-cofx :body-params
                               :request.params/replace-update-ENTITY)
        conform-params-find-ENTITY-by-id-cofx
        (i/conform-params-cofx :path-params
                               :request.params/find-ENTITY-by-id)
        conform-params-find-ENTITIES-cofx
        (i/conform-params-cofx :query-params
                               :request.params/find-ENTITIES)
        ;; handlers
        get-ENTITY-handler
        (ih/handler ::get-ENTITY-handler h/get-ENTITY)
        get-ENTITIES-handler
        (ih/handler ::get-ENTITIES-handler h/get-ENTITIES)
        create-ENTITY-handler
        (ih/handler ::create-ENTITY-handler h/create-ENTITY)
        update-ENTITY-handler
        (ih/handler ::update-ENTITY-handler h/update-ENTITY)
        replace-ENTITY-handler
        (ih/handler ::replace-ENTITY-handler h/replace-ENTITY)
        delete-ENTITY-handler
        (ih/handler ::delete-ENTITY-handler h/delete-ENTITY)
        health-handler
        (ih/handler ::health-handler h/health)]
    (route/expand-routes
     [[:BASE-NAME-api
       ["/" ^:interceptors [(pi/interceptor (mi/format-interceptor))]
        ["/health" {:any [::health health-handler]}]
        ["/api/v1"
         ["/ENTITY" {:post [::create-ENTITY
                            ^:interceptors
                            [conform-params-create-ENTITY-cofx
                             create-ENTITY-fx]
                            create-ENTITY-handler]
                     :get  [::get-ENTITIES
                            ^:interceptors
                            [conform-params-find-ENTITIES-cofx
                             find-ENTITIES-cofx]
                            get-ENTITIES-handler]}
          ["/:id" ^:interceptors
           [conform-params-find-ENTITY-by-id-cofx
            find-ENTITY-by-id-cofx]
           {:get    [::get-ENTITY get-ENTITY-handler]
            :patch  [::update-ENTITY
                     ^:interceptors
                     [conform-params-replace-update-ENTITY-cofx
                      update-ENTITY-fx]
                     update-ENTITY-handler]
            :put    [::replace-ENTITY
                     ^:interceptors
                     [conform-params-replace-update-ENTITY-cofx
                      replace-ENTITY-fx]
                     replace-ENTITY-handler]
            :delete [::delete-ENTITY
                     ^:interceptors [delete-ENTITY-fx]
                     delete-ENTITY-handler]}]]]]]])))
