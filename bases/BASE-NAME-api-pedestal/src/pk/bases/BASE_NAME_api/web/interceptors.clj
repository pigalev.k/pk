(ns pk.bases.BASE-NAME-api.web.interceptors
  (:require
   [clojure.spec.alpha :as s]
   [io.pedestal.interceptor :as i]
   [io.pedestal.interceptor.chain :as ic]
   [pk.bases.BASE-NAME-api.spec :as specs]
   [pk.bases.BASE-NAME-api.web.handlers :as h]
   [pk.components.ENTITY.api :as a]
   [taoensso.timbre :as timbre]))


(defn terminate-with-bad-request
  "Terminates request handling in `context`.
  Removes all following interceptors from queue, logs the error using
  `explanation-data` and sets response to 400 Bad Request using
  `explanation`."
  [context explanation explanation-data]
  (timbre/errorf
   "bad request: invalid parameters --- '%s'" explanation-data)
  (assoc-in (ic/terminate context) [:response]
            (h/bad-request
             {:error
              {:message     "Invalid request parameters"
               :explanation explanation}})))


(defn conform-params-cofx
  "Conforms request parameters found in request map under `params-key`
  with `spec` and merges the result to `:params` key in request
  map. If parameters are invalid, terminates request handling and sets
  response to 400 Bad Request."
  [params-key spec]
  (i/interceptor
   {:name ::conform-params-cofx
    :enter
    (fn [context]
      (let [params           (get-in context [:request params-key])
            conformed-params (s/conform spec params)]
        (timbre/debugf
         "conforming request params --- %s %s to spec %s, ---> %s"
         params-key params spec conformed-params)
        (if (= ::s/invalid conformed-params)
          (let [explanation-data (s/explain-data spec params)
                explanation      (specs/explain-errors explanation-data)]
            (terminate-with-bad-request context explanation explanation-data))
          (update-in context [:request :params]
                     merge (s/unform spec conformed-params)))))}))

(defn find-ENTITIES-cofx
  "Returns an interceptor that searches ENTITIES in the store according
  to search/sort/pagination params under `:params` key in request map,
  and attaches them to the `:found-ENTITIES` key in the request map."
  [ENTITY-store]
  (i/interceptor
   {:name ::find-ENTITIES-cofx
    :enter
    (fn [context]
      (let [search-params (get-in context [:request :params])
            found-ENTITIES (a/find ENTITY-store search-params)]
        (assoc-in context [:request :found-ENTITIES] found-ENTITIES)))}))

(defn find-ENTITY-by-id-cofx
  "Returns an interceptor that looks for the ENTITY ID under the `:id`
  key in the `:params` of the request map, searches for existing
  ENTITY in the store by this ID and, if found, attaches it to the
  `:found-ENTITY` key in the request map."
  [ENTITY-store]
  (i/interceptor
   {:name ::find-ENTITY-by-id-cofx
    :enter
    (fn [context]
      (if-let [id (get-in context [:request :params :id])]
        (if-let [found-ENTITY (a/find-by-id ENTITY-store id)]
          (assoc-in context [:request :found-ENTITY] found-ENTITY)
          context)
        context))}))

(defn create-ENTITY-fx
  "Returns an interceptor that creates a new ENTITY from the ENTITY data
  in the request body, persists it to the store and attaches it to the
  `:created-ENTITY` key in the request map. If ENTITY ID is present in
  ENTITY data and entity with this ID already exists, terminates
  request handling and sets response to 400 Bad Request."
  [ENTITY-store]
  (i/interceptor
   {:name ::create-ENTITY-fx
    :enter
    (fn [context]
      (let [ENTITY-data (get-in context [:request :params])
            ENTITY-id   (a/id ENTITY-data)]
        (cond
          (and ENTITY-id
               (a/find-by-id ENTITY-store ENTITY-id))
          (let [explanation (format "ENTITY '%s' already exists" ENTITY-id)]
            (terminate-with-bad-request context explanation explanation))
          (or ENTITY-data {}) (let [new-ENTITY (a/create-ENTITY ENTITY-data)
                                    ENTITY-id  (a/id new-ENTITY)
                                    _
                                    (a/insert ENTITY-store new-ENTITY)
                                    persisted-new-ENTITY
                                    (a/find-by-id ENTITY-store ENTITY-id)]
                                (assoc-in context [:request :created-ENTITY]
                                          persisted-new-ENTITY))
          :else               context)))}))

(defn update-ENTITY-fx
  "Returns an interceptor that looks for an existing ENTITY under the
  `:found-ENTITY` key in the request map and, if found, updates it in
  the store with ENTITY-data found under the `:params` key of request
  map and attaches it to the `:updated-ENTITY` key in the request
  map."
  [ENTITY-store]
  (i/interceptor
   {:name ::update-ENTITY-fx
    :enter
    (fn [context]
      (if-let [found-ENTITY (get-in context [:request :found-ENTITY])]
        (let [ENTITY-id      (a/id found-ENTITY)
              ENTITY-data    (get-in context [:request :params])
              _              (a/update ENTITY-store ENTITY-id ENTITY-data)
              updated-ENTITY (a/find-by-id ENTITY-store ENTITY-id)]
          (assoc-in context [:request :updated-ENTITY] updated-ENTITY))
        context))}))

(defn replace-ENTITY-fx
  "Returns an interceptor that looks for an existing ENTITY under the
  `:found-ENTITY` key in the request map and, if found, replaces it in
  the store with ENTITY data found under the `:params` key of request
  map and attaches it to the `:replaced-ENTITY` key in the request
  map."
  [ENTITY-store]
  (i/interceptor
   {:name ::replace-ENTITY-fx
    :enter
    (fn [context]
      (if-let [found-ENTITY (get-in context [:request :found-ENTITY])]
        (let [ENTITY-id       (a/id found-ENTITY)
              ENTITY-data     (get-in context [:request :params])
              _               (a/replace ENTITY-store ENTITY-id ENTITY-data)
              replaced-ENTITY (a/find-by-id ENTITY-store ENTITY-id)]
          (assoc-in context [:request :replaced-ENTITY] replaced-ENTITY))
        context))}))

(defn delete-ENTITY-fx
  "Returns an interceptor that looks for an existing ENTITY under the
  `:found-ENTITY` key in the request map and, if found, deletes this
  ENTITY from the store and attaches it to the `:deleted-ENTITY` key
  in the request map."
  [ENTITY-store]
  (i/interceptor
   {:name ::delete-ENTITY-fx
    :enter
    (fn [context]
      (if-let [found-ENTITY (get-in context [:request :found-ENTITY])]
        (do
          (a/delete ENTITY-store (a/id found-ENTITY))
          (assoc-in context [:request :deleted-ENTITY] found-ENTITY))
        context))}))
