(ns pk.bases.BASE-NAME-api.system
  "Managing the lifecycle of the components from which the system is
  composed (initialize, halt, suspend, resume). Components and their
  options are described in a configuration file."
  (:require
   [datahike.api :as d]
   [integrant.core :as ig]
   [io.pedestal.http :as http]
   [io.pedestal.log]
   [pk.bases.BASE-NAME-api.domain.ENTITY.store.datahike :as dstore]
   [pk.bases.BASE-NAME-api.spec]
   [pk.bases.BASE-NAME-api.web.routes :as r]
   [taoensso.timbre :as timbre]))


;; databases

;; datahike database: a connection, that always points to the current db value,
;; is used as a datasource
(defmethod ig/init-key :pk.bases.BASE-NAME-api.database/datahike
  [_ {:keys [db-spec delete-db]}]
  (when delete-db
    (timbre/info "deleting existing datahike db")
    (d/delete-database db-spec))
  (when (not (d/database-exists? db-spec))
    (timbre/info "creating new datahike db")
    (d/create-database db-spec))
  (timbre/info "connecting to datahike db")
  (d/connect db-spec))

(defmethod ig/pre-init-spec :pk.bases.BASE-NAME-api.database/datahike
  [_]
  :database/datahike)

;; the connection should be released eventually
(defmethod ig/halt-key! :pk.bases.BASE-NAME-api.database/datahike
  [_ connection]
  (timbre/info "releasing datahike connection")
  (d/release connection))


;; stores (repositories) for the domain entities

;; datahike ENTITY store
(defmethod ig/init-key :pk.bases.BASE-NAME-api.domain.ENTITY.store/datahike
  [_ {:keys [connection schema initial-data]}]
  (timbre/info "creating datahike ENTITY datastore")
  (dstore/create-ENTITY-store connection schema initial-data))

(defmethod ig/pre-init-spec
  :pk.bases.BASE-NAME-api.domain.ENTITY.store/datahike
  [_]
  :domain.ENTITY.store/datahike)


;; web server

(defmethod ig/init-key :pk.bases.BASE-NAME-api.web/routes
  [_ options]
  (timbre/info "creating routes")
  (r/routes options))

(defmethod ig/pre-init-spec :pk.bases.BASE-NAME-api.web/routes
  [_]
  :web/routes)

(defmethod ig/init-key :pk.bases.BASE-NAME-api.web/server
  [_ options]
  (let [{:keys
         [host port type join? routes resource-path
          allowed-origins]} options
        service             {::http/type            type
                             ::http/host            host
                             ::http/port            port
                             ::http/join?           join?
                             ::http/routes          routes
                             ::http/resource-path   (or resource-path "/public")
                             ::http/allowed-origins (or allowed-origins [""])}]
    (timbre/infof "starting web server on %s:%s" host port)
    (-> service
        http/default-interceptors
        #_http/dev-interceptors
        http/create-server
        http/start)))

(defmethod ig/pre-init-spec :pk.bases.BASE-NAME-api.web/server
  [_]
  :web/server)

(defmethod ig/halt-key! :pk.bases.BASE-NAME-api.web/server
  [_ server]
  (timbre/info "stopping web server")
  (http/stop server))
