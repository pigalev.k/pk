(ns pk.bases.BASE-NAME-api.core
  "Service entry point.
  Defines configuration reading, signal handling, logging setup,
  starts the system."
  (:require
   [aero.core :as a]
   [clojure.java.io :as io]
   [clojure.string :as s]
   [integrant.core :as ig]
   [pk.bases.BASE-NAME-api.system]
   [signal.handler :as sig]
   [taoensso.timbre :as timbre])
  (:gen-class))


(set! *warn-on-reflection* true)


;; setting up logging

(defn init-logging!
  "Configures and initializes logging."
  [& {:keys [min-level color-stacktrace?]
      :or   {min-level         :debug
             color-stacktrace? true}}]
  (timbre/set-min-level! :info)
  (timbre/infof "configuring logging, minimum logging level %s"
                min-level)
  (cond-> (assoc timbre/default-config :min-level min-level)
    (not color-stacktrace?) (assoc :output-opts {:stacktrace-fonts {}})
    :finally                timbre/set-config!))


;; reading configuration file

;; recognize integrant custom tags in the configuration file
(defmethod a/reader 'ig/ref
  [{:keys [profile] :as opts} tag value]
  (ig/ref value))

;; recognize custom tag to read secrets from files in the configuration file
(defmethod a/reader 'secret-file
  [{:keys [profile] :as opts} tag value]
  (try
    (let [secret (s/trim (slurp value))]
      (timbre/infof "secret file '%s' loaded successfully" value)
      secret)
    (catch Exception e
      (timbre/errorf "can't load secret file '%s': %s)"
                     value (ex-message e)))))


;; creating the service configuration

(defn profile
  "Returns the runtime profile of the service.
  E.g., `:dev` (default), `:test` or `:prod`."
  []
  (keyword (or (System/getenv "PROFILE") :dev)))

(defn config-resource-name
  "Returns the configuration resource name."
  []
  (or (System/getenv "CONFIG") "config/base/config.edn"))

(defn config
  "Returns the service configuration read from configuration resource
   (a file on classpath)."
  [config-resource-name profile]
  (assert (not-empty config-resource-name)
          "config resource name should not be empty")
  (timbre/infof "reading config: %s" config-resource-name)
  (let [config (a/read-config (io/resource config-resource-name)
                              {:profile  profile
                               :resolver a/resource-resolver})]
    (assert (map? config) "config should be a map")
    (timbre/infof "config keys:\n\t%s" (s/join "\n\t" (keys config)))
    config))


;; system lifecycle: (re)starting and stopping

(defonce ^:private system (atom nil))

(defn start!
  "Start the system."
  []
  (let [a-profile         (profile)
        min-level         (if (= :dev a-profile) :debug :info)
        color-stacktrace? (if (= :dev a-profile) true false)]
    (init-logging! {:min-level         [["org.eclipse.jetty.*" :info]
                                        ["*" min-level]]
                    :color-stacktrace? color-stacktrace?})
    (let [a-config-resource-name (config-resource-name)
          a-config               (config a-config-resource-name a-profile)]
      (timbre/infof "starting the system, profile %s, config file %s"
                    a-profile a-config-resource-name)
      (reset! system (ig/init a-config)))))

(defn stop!
  "Stop the system."
  []
  (timbre/info "stopping the system")
  (ig/halt! @system)
  (reset! system nil))

(defn restart!
  "Restart the system."
  []
  (timbre/info "restarting the system")
  (stop!)
  (start!))


;; configuring signal handlers

(sig/with-handler :int
  (try
    (timbre/info "caught SIGINT, quitting")
    (stop!)
    (timbre/info "all components shut down")
    (finally (System/exit 0))))

(sig/with-handler :term
  (try
    (timbre/info "caught SIGTERM, quitting")
    (stop!)
    (timbre/info "all components shut down")
    (finally (System/exit 0))))

(sig/with-handler :hup
  (timbre/info "caught SIGHUP, reloading")
  (restart!)
  (timbre/info "system restarted"))


;; service entry point

(defn -main
  "Start the system described in the configuration."
  [& args]
  (timbre/info "starting the service")
  (start!))
