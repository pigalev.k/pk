(ns pk.bases.short-url-cli.core
  (:refer-clojure :exclude [alias])
  (:require
   [babashka.cli :as cli]
   [babashka.pods :as pods]
   [clojure.java.shell :as sh]
   [clojure.string :as st]
   [pk.components.alias.api :as a]
   [pk.components.alias.core :as ac]
   [pk.components.alias-store.relational.sql :as sql-fns]))

(pods/load-pod 'org.babashka/go-sqlite3 "0.1.0")
(require '[pod.babashka.go-sqlite3 :as sqlite])


(set! *warn-on-reflection* true)


;;;; configuration

(def default-config {:db "short-url.db"})


;;;; CLI

(def common-arguments-spec {:db {:alias   :d
                                 :desc    "path to SQLite database"
                                 :ref     "<path>"
                                 :default (:db default-config)}})

(def shorten-arguments-spec {:url {:require  true
                                   :validate ac/http-url?}})
(def unshorten-arguments-spec {:alias {:require true}})

(defn current-file-name
  "Returns the name of the script file currently being executed."
  []
  (let [sep     (System/getProperty "file.separator")
        pattern (re-pattern (format "[^%s]*?$" sep))]
    (or (re-find pattern *file*)
        "<unknown>")))

(defn current-git-revision
  "Returns the current git revision (short commit hash)."
  []
  (let [{:keys [exit out]} (sh/sh "git" "rev-parse" "--short" "HEAD")]
    (if [zero? exit]
      (st/trim out)
      "<unknown>")))

(defn init-db
  "Creates database and required tables, if they do not already exist.
  Returns `parsed-cmdline`."
  [{{:keys [db]} :opts :as parsed-cmdline}]
  (sqlite/execute! db (sql-fns/create-alias-table-sql :alias))
  parsed-cmdline)

(defn help
  "Returns formatted usage help message."
  [_]
  (str (format "Short URL CLI (rev. %s)\n" (current-git-revision))
       (format "Usage: %s <command> [options] [arg]\n\n"
               (current-file-name))
       "Commands:\n  shorten <url>\n  unshorten <alias>\n  all\n"
       "  help\n\n"
       "Options:\n"
       "  name     value  default      description\n"
       "  -----------------------------------------------\n"
       (cli/format-opts {:spec common-arguments-spec})))

(defn shorten
  "Returns existing or new alias for `url`."
  [{{:keys [db url]} :opts :as parsed-cmdline}]
  (let [existing-alias-resultset
        (sqlite/query db
                      (sql-fns/retrieve-alias-by-original-sql :alias url))]
    (if (empty? existing-alias-resultset)
      (do
        (sqlite/execute! db (sql-fns/insert-alias-sql :alias (a/alias url)))
        (first
         (sqlite/query db (sql-fns/retrieve-alias-by-original-sql :alias url))))
      (first existing-alias-resultset))))

(defn unshorten
  "Returns the URL associated with `alias`, or nil."
  [{{:keys [db alias]} :opts :as parsed-cmdline}]
  (first (sqlite/query db (sql-fns/retrieve-alias-by-id-sql :alias alias))))

(defn all
  "Returns the formatted list of all existing aliases."
  [{{:keys [db]} :opts :as parsed-cmdline}]
  (sqlite/query db (sql-fns/retrieve-all-aliases-sql :alias)))

(defn -main
  "Application entry point."
  [& args]
  (cli/dispatch
   [{:cmds       ["shorten"]
     :fn         (comp println shorten init-db)
     :spec       (merge common-arguments-spec shorten-arguments-spec)
     :exec-args  default-config
     :args->opts [:url]}
    {:cmds       ["unshorten"]
     :fn         (comp println unshorten init-db)
     :spec       (merge common-arguments-spec unshorten-arguments-spec)
     :exec-args  default-config
     :args->opts [:alias]}
    {:cmds      ["all"]
     :spec      common-arguments-spec
     :exec-args default-config
     :fn        (comp println all init-db)}
    {:cmds ["help"] :fn (comp println help)}
    {:cmds [] :fn (comp println help)}]
   args
   {:error-fn
    (fn [{:keys [spec type cause msg option] :as data}]
      (if (= :org.babashka/cli type)
        (println (format "Error: %s" msg))
        (throw (ex-info msg data)))
      (System/exit 1))})
  nil)


(comment

  (binding [*command-line-args* ["shorten" "https://ya.ru"]]
    (-main))
  (binding [*command-line-args* ["unshorten" "abcdef"]]
    (-main))
  (binding [*command-line-args* []]
    (-main))


  (a/alias "http://google.com" {:length 10})


  (sqlite/execute! (:db default-config) (sql-fns/create-alias-table-sql :alias))
  (let [al (a/alias "https://google.com")]
    (sqlite/execute! (:db default-config) (sql-fns/insert-alias-sql :alias al)))
  (sqlite/query (:db default-config) (sql-fns/retrieve-all-aliases-sql :alias))
  (sqlite/query (:db default-config)
                (sql-fns/retrieve-alias-by-id-sql :alias "pfvga"))

)
