# pk

A monorepo workspace structured (generally) along with concepts and guiding
principles introduced by [Polylith](https://polylith.gitbook.io/polylith).

This workspace is for personal projects in Clojure(Script).

## Workspace

A **workspace** is a place where all the building blocks and all their assets
--- code, configuration, documentation, other resources --- live, and where
all the work --- designing, implementing, testing, maintaining, deploying,
operating, documenting --- happens; from initial vision to working,
maintainable software that creates value.

## Building blocks

Source code and other assets are organized into several types of building
blocks: **components**, **bases** and **projects**.

### Component

Contains reusable functionality that serves some narrow purpose, like
representing a user entity or defining a set of functions to store the entity
in the relational database.

### Base

Combines functionality from components and external libraries and exposes a
public interface (REST, CLI, web, etc) that allows its users accomplish some
tasks, like authenticating users or managing
(creating/reading/updating/deleting) their personal notes.

### Project

Combines one or several bases with configuration to build some artifact(s) ---
jar files, scripts, native binaries, container images, etc --- that can be
deployed (alone or in cooperation with other artifacts) as web services,
mobile or desktop applications, etc., to allow their users achieve some goal
--- like capturing, editing, managing and sharing their personal notes on a
mobile device. Projects ideally don't contain their own source code, only
configuration (and possibly other assets).

## Guiding principles

- Each building block lives in it's own separate directory, contains its own
  assets and describes its own dependencies; the building block directories
  are grouped by block type.

- Building block types form a hierarchy, so dependency relations always point
  in one direction --- from more often changing, more specific and less
  reusable blocks to more stable, more generic and reusable ones. Components
  can depend on other components and external libraries; bases can depend on
  components and external libraries; projects usually depend on bases.

- Dependencies between blocks are expressed through their **interfaces**;
  depending on implementation details is discouraged. Interface is usually
  placed in a `pk.<block type>.<block name>.api` namespace. A block can have
  multiple different implementations of its interface for its consumers to
  choose from, e.g., persistence component (store) for some domain entity can
  have implementations backed by relational database, key-value store and
  in-memory data structure.

## Contents

### Projects

- `url-shortener-api`: a service that exposes a REST api to shorten URLs and
  get original URLs back
- `url-shortener-spa`: a ClojureScript/Reagent SPA for URL shortening that
  uses the `url-shortener-api` service as its backend
- `url-shortener-api-spa`: an SPA for URL shortening bundled together with a
  backend service
- `url-shortener-cli`: a command-line utility for URL shortening

### Bases

- `short-url-api`: provides an HTTP interface (REST API) for URL shortening
- `short-url-spa`: provides a web interface (SPA) for URL shortening
- `short-url-cli`: provides a command-line interface for URL shortening
- `devcards`: provides live documentation and visual testing for UI components

### Components

- `alias`: represents a relationship between some value and its short
  representation
- `alias-store`: persists and retrieves aliases using a datasource
- `datasource`: creates and releases datasources (relational, datalog,
  in-memory)
- `build`: helpers and tasks for the workspace-wide build system
