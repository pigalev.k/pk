(ns pk.components.alias-store.datahike
  "Datahike alias store."
  (:refer-clojure :exclude [short])
  (:require
   [datahike-jdbc.core]
   [datahike.api :as d]
   [pk.components.alias.api :as a]
   [pk.components.alias.core]
   [pk.components.alias-store.api :as dsa])
  (:import
   (java.util Date)))


;; schema

;; transactions

(defn insert-alias-tx-data
  "Returns transaction data to insert an alias."
  [alias & {:keys [created-at]
            :or   {created-at (Date.)}}]
  (let [short    (a/short alias)
        original (a/original alias)]
    [{:db/id            (- (abs (hash short)))
      :alias/short      short
      :alias/original   original
      :alias/created-at created-at}]))

(defn insert-aliases-tx-data
  "Returns transaction data to insert several aliases at once."
  [aliases & {:keys [created-at] :or {created-at (Date.)}}]
  (map insert-alias-tx-data aliases (repeat {:created-at created-at})))

(defn delete-alias-tx-data
  "Returns transaction data to delete an alias by the short string."
  [short]
  [[:db/retractEntity [:alias/short short]]])

(defn delete-aliases-tx-data
  "Returns transaction data to delete sevral aliases by their short
  strings."
  [shorts]
  (mapv #(vector :db/retractEntity [:alias/short %]) shorts))


;; queries

(def alias-pull-pattern
  '(pull ?a [[:alias/short :as :short]
             [:alias/original :as :original]
             [:alias/created-at :as :created-at]]))

(def retrieve-all-aliases-query
  "A query to retrieve all aliases."
  [:find [alias-pull-pattern '...]
   :where
   '[?a :alias/short]])

(def retrieve-alias-by-short-query
  "A query to retrieve an alias by the short string."
  [:find alias-pull-pattern '.
   :in '$ '?short
   :where
   '[?a :alias/short ?short]])

(def retrieve-alias-by-original-query
  "A query to retrieve an alias by the original datum."
  [:find alias-pull-pattern '.
   :in '$ '?original
   :where
   '[?a :alias/original ?original]])

(def all-aliases-count-query
  "A query to retrieve count of all existing aliases."
  '[:find (count-distinct ?a) .
    :where
    [?a :alias/short]])


;; the store

(defrecord DatahikeAliasStore [connection]
  dsa/AliasStore
  (insert [_ item]
    (d/transact connection (insert-alias-tx-data item)))
  (delete [_ id]
    (d/transact connection (delete-alias-tx-data id)))
  (retrieve-all [_]
    (d/q retrieve-all-aliases-query @connection))
  (retrieve-by-short [_ short]
    (d/q retrieve-alias-by-short-query @connection short))
  (retrieve-by-original [_ original]
    (d/q retrieve-alias-by-original-query @connection original))
  dsa/AliasStoreTransactional
  (insert-all [_ items]
    (throw (ex-info "Not implemented (yet)." {:items items})))
  (delete-all [_ ids]
    (let [{:keys [db-before db-after]}
          (d/transact connection (delete-aliases-tx-data ids))
          aliases-count-before (or (d/q all-aliases-count-query db-before) 0)
          aliases-count-after  (or (d/q all-aliases-count-query db-after) 0)]
      (- aliases-count-before aliases-count-after))))

(defn empty-db?
  "Checks if the current db value held by connection is empty (contains no schema
  or alias data)."
  [connection]
  (empty? (d/schema @connection)))

(defn alias-store*
  "Create an alias store backed by Datahike database.

  Parameters:
  - `connection`: a connection to db, an atom containing the latest db value.
  - `schema`: schema that will be applied if it was not applied already
    (db is empty).
  - `initial-data`: initial data that will be transacted into db if
    there are no data yet (db is empty)."
  [connection schema initial-data]
  (when (empty-db? connection)
    (when schema
      (d/transact connection schema)
      (when initial-data
        (d/transact connection initial-data))))
  (->DatahikeAliasStore connection))

(defmethod dsa/alias-store :datahike
  [{:keys [connection schema initial-data]}]
  (alias-store* connection schema initial-data))
