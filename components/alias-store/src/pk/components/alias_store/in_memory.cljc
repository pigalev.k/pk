(ns pk.components.alias-store.in-memory
  "In-memory alias store (a set of records)."
  (:refer-clojure :exclude [short])
  (:require
   [clojure.set :as s]
   [pk.components.alias.api :as a]
   [pk.components.alias.core]
   [pk.components.alias-store.api :as sapi]))


(defrecord InMemoryAliasStore [datasource]
  sapi/AliasStore
  (insert [_ item]
    (swap! datasource conj item))
  (delete [_ shrt]
    (when-let [delenda (s/select #(= shrt (a/short %)) @datasource)]
      (swap! datasource s/difference delenda)))
  (retrieve-by-short [_ shrt]
    (first (s/select #(= shrt (a/short %)) @datasource)))
  (retrieve-by-original [_ original]
    (first (s/select #(= original (a/original %)) @datasource)))
  (retrieve-all [_]
    @datasource))

(defn create-alias-store*
  "Create an in-memory alias store (a set of records)."
  [datasource]
  (->InMemoryAliasStore datasource))

(defmethod sapi/alias-store :in-memory
  [{:keys [datasource]}]
  (create-alias-store* datasource))


(comment

  ;; TODO: extract to tests

  (def datasource (atom #{}))
  (def mstore (sapi/alias-store {:type       :in-memory
                                 :datasource datasource}))

  (sapi/insert mstore (a/alias "hello"))
  (def first-alias (first (sapi/retrieve-all mstore)))
  (sapi/retrieve-by-short mstore (a/short first-alias))
  (sapi/delete mstore (a/short first-alias))
  (sapi/retrieve-by-short mstore (a/short first-alias))

)
