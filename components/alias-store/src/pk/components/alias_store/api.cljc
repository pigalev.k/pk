(ns pk.components.alias-store.api
  (:refer-clojure :exclude [find type]))


(defprotocol AliasStore
  "A store for aliases.
  Supports basic operations: retrieving single aliases/sets of
  aliases, inserting/updating/deleting single aliases."
  (insert [this item])
  (delete [this id])
  (retrieve-all [this])
  (retrieve-by-short [this short])
  (retrieve-by-original [this original]))

(defprotocol AliasStoreSearchable
  "A store for aliases.
  Supports searching aliases by arbitrary criteria, described by
  `params`."
  (find [this params]))

(defprotocol AliasStoreTransactional
  "A store for aliases.
  Supports atomic insert/deletion of several aliases (all individual
  operations succeed or entire transaction fails)."
  (insert-all [this items])
  (delete-all [this ids]))


(defmulti alias-store
  "Creates an alias store.
  See implementation's docs for the supported parameters."
  :type)

(defmethod alias-store :default
  [{:keys [type]}]
  (throw (ex-info "No alias store implementation found for the given type"
                  {:type type})))


(comment

  ;; TODO: extract to tests

  (alias-store {:type       :whoa
                :datasource {}})

)
