(ns pk.components.alias-store.relational
  "Relational alias store."
  (:require
   [honey.sql :as sql]
   [next.jdbc :as jdbc]
   [next.jdbc.date-time]
   [pk.components.alias-store.api :as asa]
   [pk.components.alias-store.relational.sql :as sql-fns]))


;; the store

(defrecord RelationalAliasStore [datasource table]
  asa/AliasStore
  (insert [_ item]
    (jdbc/execute-one! datasource (sql-fns/insert-alias-sql table item)))
  (delete [_ id]
    (jdbc/execute-one! datasource (sql-fns/delete-alias-sql table id)))
  (retrieve-all [_]
    (jdbc/execute! datasource (sql-fns/retrieve-all-aliases-sql table)))
  (retrieve-by-short [_ id]
    (jdbc/execute-one! datasource (sql-fns/retrieve-alias-by-id-sql table id)))
  (retrieve-by-original [_ original]
    (jdbc/execute-one! datasource
                       (sql-fns/retrieve-alias-by-original-sql table original))))

(defn ^:private table-exists?
  "Check if `table` already exists in the database represented by
  `datasource`."
  [datasource table]
  (let [table-exists-sql (sql/format
                          {:select :%count.*
                           :from   table})]
      (try
        (when (jdbc/execute! datasource table-exists-sql)
          true)
        (catch Exception _ false))))

;; TODO: add support for schema and initial-data parameters

(defn ^:private create-alias-table!
  "Create a table for aliases."
  [datasource table]
  (with-open [conn (jdbc/get-connection datasource)]
    (jdbc/execute-one! conn (sql-fns/create-alias-table-sql table))))

(defn ^:private drop-alias-table!
  "Drop the table for aliases."
  [datasource table]
  (with-open [conn (jdbc/get-connection datasource)]
    (jdbc/execute-one! conn (sql-fns/drop-alias-table-sql table))))

(defn alias-store*
  "Create an alias store backed by relational db.

  Parameters:
  - `datasource`: something `next.jdbc.protocols.Sourceable` (datasource or
    db-spec either in map or string form)
  - `table`: a name for the alias table
  - `drop-tables`: drop the alias table if it exists
  - `create-tables`: create the alias table, if it does not exist"
  [datasource table drop-tables create-tables]
  (when drop-tables
    (when (table-exists? datasource table)
      (drop-alias-table! datasource table)))
  (when create-tables
    (when-not (table-exists? datasource table)
      (create-alias-table! datasource table)))
  (->RelationalAliasStore datasource table))

(defmethod asa/alias-store :relational
  [{:keys [datasource table drop-tables create-tables]
    :or {table :alias
         drop-tables false
         create-tables true}}]
  (alias-store* datasource table drop-tables create-tables))
