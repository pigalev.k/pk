(ns pk.components.alias-store.relational.sql
  "Functions that produce SQL from data descriptions. Do not depend on
  `next.jdbc`, so can be used, e.g., from babashka scripts."
  (:require
   [honey.sql :as sql]
   [pk.components.alias.api :as a]
   [pk.components.alias.core])
  (:import (java.util Date)))


;; SQL statements

;; DDL

(defn create-alias-table-sql
  "Returns a SQL statement for creating a `table`."
  [table & {:keys [short-max-length original-max-length]
            :or   {short-max-length    16
                   original-max-length 2048}}]
  (sql/format
   {:create-table [table :if-not-exists]
    :with-columns
    [[:short [:varchar short-max-length] :primary-key]
     [:original [:varchar original-max-length] [:not nil]]
     [:created-at :timestamp]]}))

(defn drop-alias-table-sql
  "Returns a SQL statement for dropping the `table`."
  [table]
  (sql/format
   {:drop-table [:if-exists table]}))

;; DML

(defn insert-alias-sql
  "Returns a SQL statement to insert an alias."
  [table alias & {:keys [created-at] :or {created-at (Date.)}}]
  (let [short    (a/short alias)
        original (a/original alias)]
    (sql/format
     {:insert-into [table [:short :original :created-at]]
      :values      [[short original created-at]]})))

(defn insert-aliases-sql
  "Returns a SQL statement to insert several aliases at once."
  [table aliases & {:keys [created-at] :or {created-at (Date.)}}]
  (let [aliases-values
        (map #(vector (a/short %) (a/original %) created-at) aliases)]
    (sql/format
     {:insert-into [table [:short :original :created-at]]
      :values aliases-values})))

(defn delete-alias-sql
  "Returns a SQL statement to delete an alias by id."
  [table id]
  (sql/format
   {:delete-from table
    :where       [:= :short id]}))

;; DQL

(defn retrieve-all-aliases-sql
  "Returns a SQL statement to retrieve all aliases."
  [table]
  (sql/format {:select :* :from table}))

(defn retrieve-alias-by-id-sql
  "Returns a SQL statement to retrieve an alias by id."
  [table id]
  (sql/format {:select :*
               :from   table
               :where  [:= :short id]}))

(defn retrieve-alias-by-original-sql
  "Returns a SQL statement to retrieve an alias by original datum."
  [table original]
  (sql/format {:select :*
               :from   table
               :where  [:= :original original]}))
