(ns pk.components.alias-store.api-test
  (:require
   [clojure.test :refer [testing deftest is]]
   [pk.components.alias-store.api :as sapi]))


;; TODO: write some tests

(deftest alias-store-api-test
  (testing "alias store constructor"
    (is (thrown? Exception (sapi/alias-store {:type :not-implemented}))
        "throws on unknown store type")))
