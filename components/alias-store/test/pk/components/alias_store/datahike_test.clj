(ns pk.components.alias-store.datahike-test
  (:require
   [clojure.test :refer [testing deftest is use-fixtures]]
   [pk.components.alias.api :as a]
   [pk.components.alias-store.api :as asa]
   [pk.components.alias-store.datahike]
   [pk.components.datasource.api :as dsa]
   [pk.components.datasource.datahike]))


(def schema [{:db/doc         "A short representation of the aliased object."
              :db/ident       :alias/short
              :db/unique      :db.unique/identity
              :db/valueType   :db.type/string
              :db/cardinality :db.cardinality/one}

             {:db/doc         "An original object that was aliased."
              :db/ident       :alias/original
              :db/valueType   :db.type/string
              :db/cardinality :db.cardinality/one}

             {:db/doc         "Date and time of alias creation."
              :db/ident       :alias/created-at
              :db/valueType   :db.type/instant
              :db/cardinality :db.cardinality/one}])

(def initial-data [{:db/id            -100
                    :alias/short      "abcd"
                    :alias/original   "https://example.com"
                    :alias/created-at #inst "2023-03-01T10:39:48.817-00:00"}])

(defonce ^:dynamic *store* nil)

(def ds-options {:type       :datahike
                 :db-spec    {:store              {:id      "hello-db"
                                                   :backend :mem}
                              :name               "hello-db"
                              :schema-flexibility :write
                              :keep-history?      true}
                 :db-options {:delete-db true}})

(defn alias-store
  "A fixture that instantiates Datahike datasource, creates alias store
  and binds it to `*store*` before each test."
  [t]
  (let [ds    (dsa/datasource ds-options)
        store (asa/alias-store {:type         :datahike
                                :connection   ds
                                :schema       schema
                                :initial-data initial-data})]
    (binding [*store* store]
      (t))))

(use-fixtures :each alias-store)


(deftest alias-store-datahike-test
  (testing "Datahike alias store"
    (is (instance? datahike.db.TxReport
                   (asa/insert *store* (a/alias "hello")))
        "can insert aliases")
    (is (= 2 (count (asa/retrieve-all *store*)))
        "can retrieve all inserted aliases")
    (is (map? (asa/retrieve-by-original *store* "hello"))
        "can retrieve aliases by original")
    (let [example-alias-id "abcd"]
      (is (map? (asa/retrieve-by-short *store* example-alias-id))
          "can retrieve aliases by id (short string)")
      (is (instance? datahike.db.TxReport
                     (asa/delete *store* example-alias-id))
          "can delete inserted aliases")
      (is (nil? (asa/retrieve-by-short *store* example-alias-id))
          "does not contain deleted aliases"))))
