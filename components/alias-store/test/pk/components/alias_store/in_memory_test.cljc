(ns pk.components.alias-store.in-memory-test
  (:require
   [clojure.test :refer [testing deftest is use-fixtures]]
   [pk.components.alias.api :as a]
   [pk.components.alias-store.api :as asa]
   [pk.components.alias-store.in-memory]
   [pk.components.datasource.api :as dsa]
   [pk.components.datasource.in-memory]))


(defonce ^:dynamic *store* nil)

(def ds-options {:type :in-memory-set})

(defn alias-store
  "A fixture that instantiates in-memory datasource, creates alias store
  and binds it to `*store*` before each test."
  [t]
  (let [ds    (dsa/datasource ds-options)
        store (asa/alias-store {:type       :in-memory
                                :datasource ds})]
    (binding [*store* store]
      (t))))

(use-fixtures :each alias-store)


(deftest alias-store-in-memory-test
  (testing "in-memory set alias store"
    (is (= 1 (count (asa/insert *store* (a/alias "hello"))))
        "can insert aliases")
    (is (= 1 (count (asa/retrieve-all *store*)))
        "can retrieve all inserted aliases")
    (is (= "hello" (a/original (asa/retrieve-by-original *store* "hello")))
        "can retrieve aliases by original")
    (let [example-alias-id (a/short (asa/retrieve-by-original *store* "hello"))]
      (is (= "hello" (a/original
                      (asa/retrieve-by-short *store* example-alias-id)))
          "can retrieve aliases by id (short string)")
      (is (= 0 (count (asa/delete *store* example-alias-id)))
          "can delete inserted aliases")
      (is (nil? (asa/retrieve-by-short *store* example-alias-id))
          "does not contain deleted aliases"))))
