# alias-store

A persistence component for aliases. It have several implementations, backed by:

- an in-memory data structure (a set)
- a Datahike database
- a relational database (using JDBC)

## Use

Require `api` namespace and namespace(s) of required implementation(s), e.g.

```clojure
(ns pk.bases.hello-api
  (:require
    [next.jdbc :as jdbc]
    [pk.components.alias.api :as a]
    [pk.components.alias-store.api :as asa]
    [pk.components.alias-store.in-memory]
    [pk.components.alias-store.relational]
    [pk.components.datasource.api :as dsa]
    [pk.components.datasource.relational]))
```

Create a store instance, specifying desired implementation by the `:type` key;
other store options are implementation-specific.

```clojure
;; create a datasource directly or using a datasource component
(def jdbc-datasource (jdbc/get-datasource {...}))
(def jdbc-datasource (dsa/datasource {:type :relational-pooled
                                      :db-spec      {...}
                                      :db-options   {...}
                                      :pool-options {...}}))

(def rstore (asa/alias-store {:type       :relational
                              :datasource jdbc-datasource}))
```

Use the store to save and retrieve aliases

```clojure
;; insert an alias

(asa/insert rstore (a/alias "http://example.com?user=Joe"))

;; retrieve all aliases

(def first-alias (first (asa/retrieve-all rstore)))

;; retrieve an alias by short string (unique identifier)

(asa/retrieve-by-short rstore (a/short first-alias))

;; delete an alias

(asa/delete rstore (a/short first-alias))
```

## Implementations

### Configuration

- `:in-memory`:
  - `:datasource`: an in-memory datasource

- `:datahike`:
  - `:connection`: a connection to Datahike database (an atom containing the
    latest database value)
  - `:schema`: optional; data describing entities and attributes to store
  - `:initial-data`: optional; data to transact after creating the store

- `:relational`:
  - `:datasource`: a `next.jdbc` datasource
  - `:table-name`: optional; a name for the table for storing aliases (default
    is `:alias`)
  - `:drop-tables`: optional; drop alias tables after creating the store
    (default is false)
  - `:create-tables`: optional; create necessary tables after creating the store
    (default is true)

### Dependencies

Implementation dependencies are not included in component dependencies to avoid
polluting classpath with unneeded code and bloating the size of project
uberjars, so you must specify each used implementation's dependencies manually:

- `in-memory`: no dependencies
- `datahike`: `io.replikativ/datahike`, `io.replikativ/datahike-jdbc` for JDBC
  backend
- `relational`: `com.github.seancorfield/next.jdbc`,
  `com.github.seancorfield/honeysql`, necessary JDBC drivers
  (e.g. `com.h2database/h2`)

Versions used in development and testing are specified under the `:dev` alias in
`deps.edn`.

## Develop

Start a REPL

```
clj -A:dev
```

or use your favourite editor to start one.

## Build

List available build tasks and their usage summaries

```
clojure -T:build
```

See also documentation of projects/bases that use this component.

## Test

Run all tests

```
clojure -M:dev:test
```
See Kaocha documentation for additional information.
