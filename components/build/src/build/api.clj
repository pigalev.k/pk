(ns build.api
  (:require
   [build.helpers :as h]
   [clojure.string :as s]
   [clojure.tools.build.api :as b]))


(set! *warn-on-reflection* true)

;; project's build  configuration, empty by default

(def ^:dynamic *config*
  "Project's build configuration, empty by default.
  Use `set-config!` to override the default for all tasks."
  {})

(defn set-config!
  "Set `*config*` for all tasks."
  [config-map]
  (alter-var-root #'*config* (constantly config-map)))

(defmacro with-config
  "Execute forms with a bound build config map."
  [config-map & forms]
  `(binding [*config* ~config-map] ~@forms))


;; simple tasks

;; misc

(defn help
  "List all available tasks with summaries of their docstrings."
  [args]
  (let [{task-sym 'help
         :keys    [project-name tasks-ns]
         :or      {tasks-ns "build"}} (merge *config* args)]
    (if task-sym
      (let [task-name (name task-sym)
            task      (resolve (symbol tasks-ns task-name))
            task-help (:doc (meta task) "<not documented>")]
        (if task
          (do
            (printf "A build script for '%s'.\n\nTask docstring summary:\n\n"
                    project-name)
            (printf "  %s: %s\n " task-name task-help)
            (println "\nRun 'help' command without parameters"
                     "to see all available tasks."))
          (do
            (printf "Unknown task: %s\n\n" task-name)
            (help nil))))
      (let [vars              (ns-interns (find-ns (symbol tasks-ns)))
            fns               (filter #(fn? @(second %)) vars)
            private?          #(contains? (meta (second %)) :private)
            tasks             (remove private? fns)
            first-sentence    #".*\.(\s|$)"
            task-description  #(vector
                                (first %)
                                (s/trim
                                 (first
                                  (re-find first-sentence
                                           (:doc (meta (second %))
                                                 "<no doc>")))))
            task-descriptions (map task-description tasks)]
        (printf "A build script for '%s'.\n\nAvailable tasks:\n\n" project-name)
        (doseq [[task-name task-help] (sort task-descriptions)]
          (println (format "  %s:  %s" task-name task-help)))
        (println (str "\nTasks accept parameters as key-value pairs, e.g. `:port 8888`\n"
                      "or `:dry-run true`."))
        (println (str
                  "\nAll tasks support `:dry-run` parameter - report planned actions,\n"
                  "commands and environment, but do not execute."))
        (println
         "\nRun 'help' command with a task name to see it's full docstring.")))))

(defn clean
  "Clean all build results.
  Removes `:build-dir` and `:cljs-build-dir`."
  [args]
  (let [{:keys [build-dir cljs-build-dir
                dry-run]} (merge *config* args)]
    (println "Cleaning build results...")
    (when build-dir
      (printf "cleaning '%s'...\n" build-dir))
    (when cljs-build-dir
      (printf "cleaning '%s'...\n" cljs-build-dir))
    (flush)
    (if dry-run
      (println "dry run: doing nothing")
      (do
        (when build-dir
          (b/delete {:path build-dir}))
        (when cljs-build-dir
          (b/delete {:path cljs-build-dir}))))))


;; preparing for compilation/packing

(defn create-pom
  "Creates POM (Project Object Model) file."
  [args]
  (let [{:keys [basis src-dirs class-dir lib version
                dry-run]} (merge *config* args)]
    (printf "Creating POM file in '%s'...\n" class-dir)
    (flush)
    (if dry-run
      (println "dry run: doing nothing")
      (b/write-pom {:basis     basis
                    :src-dirs  src-dirs
                    :class-dir class-dir
                    :lib       lib
                    :version   version}))))

(defn copy-sources
  "Prepare source files for compilation.
  Copies source files from `:src-dirs` to `:class-dir`."
  [args]
  (let [{:keys [src-dirs class-dir
                dry-run]} (merge *config* args)]
    (printf "Copying sources from '%s' to '%s'...\n" src-dirs class-dir)
    (flush)
    (if dry-run
      (println "dry run: doing nothing")
      (b/copy-dir {:src-dirs   src-dirs
                   :target-dir class-dir}))))


;; compiling the sources

(defn compile-clj
  "Compile Clojure sources.
  Using dependencies from `:basis` and source files in `:class-dir`,
  compiles the sources starting at `:entrypoint`."
  [args]
  (let [{:keys [basis class-dir entrypoint
                dry-run]} (merge *config* args)]
    (printf "Compiling Clojure sources in '%s'...\n" class-dir)
    (flush)
    (if dry-run
      (println "dry run: doing nothing")
      (b/compile-clj {:basis      basis
                      :class-dir  class-dir
                      :ns-compile [entrypoint]}))))

(defn compile-cljs-shadow
  "Compile ClojureScript sources with `shadow-cljs`.
  Using `:shadow-cljs-command` (vector of strings) and
  `:shadow-cljs-build-mode` (`:compile`, `:release`; default
  `:release`) compiles the `:shadow-cljs-build-name` build."
  [args]
  (let [{:keys [shadow-cljs-command shadow-cljs-build-name shadow-cljs-build-mode
                dry-run]
         :or   {dry-run false}} (merge *config* args)]
    (let [command-and-args (into shadow-cljs-command
                                 [(name shadow-cljs-build-mode)
                                  (name shadow-cljs-build-name)])]
      (println "Compiling ClojureScript sources with shadow-cljs...")
      (flush)
      (if dry-run
        (printf "dry run: command %s\n" command-and-args)
        (b/process {:command-args command-and-args})))))


;; packing

(defn jar
  "Pack sources into a jar file.
  Packs the source files in `:class-dir` into the `:jar-file` in the
  `:build-dir` directory. For libraries/dependable artifacts."
  [args]
  (let [{:keys [class-dir build-dir jar-file
                dry-run]} (merge *config* args)]
    (printf "Packing sources from '%s' into jar '%s%s'...\n"
            class-dir build-dir jar-file)
    (flush)
    (if dry-run
      (println "dry run: doing nothing")
      (b/jar {:class-dir class-dir
              :jar-file  (str build-dir jar-file)}))))

(defn uberjar
  "Pack compiled sources and their dependencies into an uberjar file.
  Packs the compiled sources in `:class-dir` and their dependencies
  from the `:basis` into the `:uberjar-file` in the `:build-dir`
  directory, using `:entrypoint` as an entry point for the uberjar
  execution. For executables/deployable artifacts."
  [args]
  (let [{:keys [class-dir basis entrypoint build-dir uberjar-file
                dry-run]}
        (merge *config* args)]
    (printf "Packing compiled sources from '%s' and their deps into uberjar\n"
             class-dir)
    (printf "'%s%s'...\n" build-dir uberjar-file)
    (flush)
    (if dry-run
      (println "dry run: doing nothing")
      (b/uber {:class-dir class-dir
               :basis     basis
               :main      entrypoint
               :uber-file (str build-dir uberjar-file)}))))

(defn image
  "Build a container image.
  Image will
  - have the name `:image-name`
  - contain an `:uberjar-file` of `:version` as it's entry point
  - use `:container-runtime-command` to start a build (think `docker`)
  - use `:image-build-file` as file with build instructions (think `Dockerfile`)
  - use `:image-build-context-path` as a root directory for the build"
  [args]
  (let [{:keys [uberjar-file version
                container-runtime-command
                image-name image-build-file image-build-context-path
                dry-run]} (merge *config* args)]
    (let [command-and-args [container-runtime-command "build"
                            "--build-arg" (format "UBERJAR=%s" uberjar-file)
                            "--build-arg" (format "VERSION=%s" version)
                            "--tag" image-name
                            "-f" image-build-file
                            image-build-context-path]]
      (printf "Building image '%s'...\n" image-name)
      (flush)
      (if dry-run
        (printf "dry run: command %s\n" command-and-args)
        (b/process {:command-args command-and-args})))))


;; installing

(defn install
  "Install POM and source jar to local Maven repository.
  Installs jar file `:jar-file` and POM from `:class-dir` for library
  `:lib` with version `:version` into the local Maven repository
  defined in `:basis`."
  [args]
  (let [{:keys [lib version basis class-dir build-dir jar-file
                dry-run]} (merge *config* args)]
    (printf "Installing library '%s' to local Maven repository, jar file '%s'\n"
            lib jar-file)
    (if dry-run
      (println "dry run: doing nothing")
      (b/install {:lib       lib
                  :version   version
                  :basis     basis
                  :class-dir class-dir
                  :jar-file  (str build-dir jar-file)}))))


;; running

(defn run-uberjar
  "Run an uberjar (`C-c` to stop).
  Runs the `:uberjar-file` from `:build-dir`, using `:config-file` as
  a configuration file name.

  Other parameters:
  - `:profile`: interpreted by the service at runtime according to its
    configuration (default is `:test`)
  - `:port`: determines the host machine port that will be used by
    service (default is `8888`)"
  [args]
  (let [{:keys [build-dir uberjar-file config-file
                profile port dry-run]
         :or   {profile :test
                port    8888}} (merge *config* args)
        uberjar-path           (str build-dir uberjar-file)
        command-and-args       ["java" "-jar" uberjar-path]
        env                    {"PROFILE" (name profile)
                                "CONFIG"  config-file
                                "PORT"    (str port)}]
    (printf "Running uberjar '%s...\n" uberjar-path)
    (flush)
    (if dry-run
      (do
        (printf "dry run: command %s\n" command-and-args)
        (printf "dry run: env %s\n" env))
      (b/process {:command-args command-and-args
                  :env          env}))))

(defn run-container
  "Run a container (`C-c` to stop).
  Runs the container with name `:project-name` from image
  `:image-name`, using `:config-file` as a configuration file name.

  Other parameters:
  - `:profile`: interpreted by the service at runtime according to its
    configuration (default is `:test`)
  - `:port`: determines the host machine port that will be forwarded to the
    service (default is `8888`)"
  [args]
  (let [{:keys [project-name image-name config-file
                container-runtime-command
                profile port dry-run]
         :or   {profile :test
                port    8888}} (merge *config* args)]
    (let [command-and-args [container-runtime-command
                            "run" "--rm"
                            "-e" (format "PROFILE=%s" (name profile))
                            "-e" (format "CONFIG=%s" config-file)
                            "-p" (format "%s:8888" port)
                            "--name" project-name
                            image-name]]
      (printf "Running a container '%s' from image '%s'...\n"
              project-name image-name)
      (flush)
      (if dry-run
        (printf "dry run: command %s\n" command-and-args)
        (b/process {:command-args command-and-args})))))

(defn run-compose
  "Run a compose.
  Runs the containers described in a compose file `:file` (default is
  `deploy/compose.yml`) using `:compose-command`, specifying
  `:compose-project` as a compose project.

  If some images do not exist, they will be downloaded or built first.

  Parameters `:uberjar-file` and `:version` are used when the
  project's image is built, `:image` and `:config-file` - when a
  container is started from the image.

  Other parameters:
  - `:rebuild`: rebuild all the project's locally-built images, even if
    they already exist (default `false`)
  - `:recreate`: remove and recreate all project's containers (default `true`)
  - `:profile`: interpreted by the service at runtime according to its
    configuration (default is `:test`)
  - `:port`: determines the host machine port that will be forwarded to the
    service (if configured in `:file`, default is `8888`)"
  [args]
  (let [{:keys [compose-command compose-project file
                uberjar-file version
                image-name config-file
                rebuild recreate
                profile port
                dry-run]
         :or   {profile  :test
                port     8888
                file     "deploy/compose.yml"
                rebuild  false
                recreate true}} (merge *config* args)
        command-and-args        (->> [compose-command
                                      "-p" compose-project
                                      (when file ["-f" (str file)])
                                      "up" "-d"
                                      (when rebuild "--build")
                                      (when recreate "--force-recreate")]
                                     (keep identity)
                                     flatten
                                     vec)
        env                     {"IMAGE"   image-name
                                 "VERSION" version
                                 "UBERJAR" uberjar-file
                                 "PROFILE" (name profile)
                                 "CONFIG"  config-file
                                 "PORT"    (str port)}]
    (printf "Running the service '%s' and its dependencies...\n" image-name)
    (flush)
    (if dry-run
      (do
        (printf "dry run: command %s\n" command-and-args)
        (printf "dry run: env %s\n" env))
      (b/process {:command-args command-and-args
                  :env          env}))))

(defn stop-compose
  "Stop a compose.
  Stops the containers described in a compose file `:file` (default is
  `deploy/compose.yml`) using `:compose-command`, specifying
  `:compose-project` as a compose project.

  Other parameters:
  - `:destroy`: also remove all project's containers and
    networks, but not volumes (default `true`).
  - `:destroy-volumes`: destroy volumes too (default `false`)."
  [args]
  (let [{:keys [compose-command compose-project file
                image-name
                destroy destroy-volumes  dry-run]
         :or   {destroy         true
                destroy-volumes false
                file            "deploy/compose.yml"}} (merge *config* args)
        command-and-args
        (->> [compose-command
              "-p" compose-project
              (when file ["-f" (str file)])
              (if destroy "down" "stop")
              (when destroy-volumes "-v")]
             (keep identity)
             flatten
             vec)]
    (printf "Stopping the service '%s' and its dependencies...\n" image-name)
    (flush)
    (if dry-run
      (printf "dry run: command %s\n" command-and-args)
      (b/process {:command-args command-and-args}))))


;; composite tasks

(defn library-jar
  "Pack project/base/component sources into a library jar file.
  Cleans previous build results, creates POM and copies sources before
  packing."
  [args]
  (println "Building library jar...")
  (flush)
  (clean args)
  (create-pom args)
  (copy-sources args)
  (jar args))

(defn service-uberjar
  "Pack compiled Clojure sources into an uberjar file.
  Cleans previous build results, copies and compiles the sources
  before packing."
  [args]
  (println "Building service uberjar...")
  (flush)
  (clean args)
  (copy-sources args)
  (compile-clj args)
  (uberjar args))

(defn service-app-uberjar
  "Pack the compiled Clojure and ClojureScript sources into an uberjar file.
  Cleans previous build results, creates POM, copies and compiles the
  sources before packing."
  [args]
  (println "Building service + app uberjar...")
  (flush)
  (clean args)
  (compile-cljs-shadow args)
  (copy-sources args)
  (compile-clj args)
  (uberjar args))

(defn install-library
  "Install POM and source library jar to local Maven repository.
  If the jar file does not exist, it will be built first."
  [args]
  (let [{:keys [build-dir jar-file]} (merge *config* args)
        jar-path                     (str build-dir jar-file)
        jar-exists?                  (h/path-exists? jar-path)]
    (when-not jar-exists?
      (printf "Library jar '%s' does not exist, building...\n" jar-path)
      (flush)
      (library-jar args))
    (install args)))

(defn run-service-uberjar
  "Run a service uberjar (`C-c` to stop).
  If the uberjar for the current version does not exist, build it
  first."
  [args]
  (let [{:keys [build-dir uberjar-file]} (merge *config* args)
        uberjar-path                     (str build-dir uberjar-file)
        uberjar-exists?                  (h/path-exists? uberjar-path)]
    (when-not uberjar-exists?
      (printf "Uberjar '%s' does not exist, building...\n" uberjar-path)
      (flush)
      (service-uberjar args))
    (run-uberjar args)))

(defn run-service-app-uberjar
  "Run a service + app uberjar (`C-c` to stop).
  If the uberjar for the current version does not exist, build it
  first."
  [args]
  (let [{:keys [build-dir uberjar-file]} (merge *config* args)
        uberjar-path                     (str build-dir uberjar-file)
        uberjar-exists?                  (h/path-exists? uberjar-path)]
    (when-not uberjar-exists?
      (printf "Uberjar '%s' does not exist, building...\n" uberjar-path)
      (flush)
      (service-app-uberjar args))
    (run-uberjar args)))

(defn run-service-container
  "Run the image in a standalone container (`C-c` to stop).
  If an image for the current version does not exist, build it first.

  Parameters:
  - `:profile`: interpreted by the service at runtime according to its
    configuration (default is `:test`).
  - `:port`: determines the host machine port that will be forwarded to the
    service (default is `8888`).
  - `:dry-run`: if `true`, only print the constructed command and args, but
    do not execute."
  [args]
  (let [{:keys [image-name container-runtime-command]} (merge *config* args)]
    (when-not (h/image-exists? image-name container-runtime-command)
      (printf "Image '%s' does not exist, building...\n" image-name)
      (flush)
      (image args))
    (run-container args)))
