(ns build.helpers
  (:require
   [clojure.java.io :as io]
   [clojure.tools.build.api :as b]))


(defn path-exists?
  "Check if a given path already exists under the project root."
  [file-path]
  (.exists (io/file (b/resolve-path file-path))))

(comment

  b/*project-root*
  (path-exists? "target/")

)

(defn image-exists?
  "Check if an image with given name already exists.
  Uses `container-runtime-command` to access the container runtime."
  [image-name container-runtime-command]
  (= 0 (:exit
        (b/process {:command-args [container-runtime-command
                                   "image" "exists" image-name]}))))
