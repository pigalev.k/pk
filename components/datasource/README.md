# datasource

A component that manages (creates and releases) datasources --- objects that
can be used to interact with sources of data, such as databases, message
queues, etc.

The component have implementations for
- in-memory data structures (set, vector)
- Datahike database
- relational database (using JDBC and optionally HikariCP for connection
  pooling)

## Use

Require `api` namespace and namespace(s) of desired implementation(s), e.g.

```clojure
(ns pk.bases.hello-api
  (:require
    [pk.components.datasource.api :as dsa]
    [pk.components.datasource.in-memory]
    [pk.components.datasource.datahike]
    [pk.components.datasource.relational]))
```

Create a datasource, specifying desired implementation with `:type` key; other
datasource options are implementation-specific.

```clojure
(def pooled-relational-datasource
  (dsa/datasource {:type         :relational-pooled
                   :db-spec      {:dbname "hello-db"
                                  :dbtype "h2:mem"}
                   :db-options   {}
                   :pool-options {:poolName        "hello-pool"
                                  :minimumIdle     1
                                  :maximumPoolSize 3}}))

(def in-memory-set-datasource
  (dsa/datasource {:type   :in-memory-set
                   :sorted true}))
```

Use the datasource to issue commands/queries

```clojure
(jdbc/execute! pooled-relational-datasource
  ["CREATE TABLE hello (name VARCHAR(10));"])
(jdbc/execute! pooled-relational-datasource
  ["INSERT INTO hello (name) VALUES (?);" "Joe"])
(jdbc/execute! pooled-relational-datasource
  ["SELECT * FROM hello;"])

(swap! in-memory-set-datasource conj "hello" "there")
(clojure.set/select #{"hello"} @in-memory-set-datasource)
```

Release the datasource, if necessary

```clojure
(dsa/release! pooled-relational-datasource)
```

## Implementations

### Configuration

Implementation-specific options:

- `:in-memory-set`:
  - `:sorted`: if true, use sorted set

- `:relational`:
  - `:db-spec`: a database specification accepted by `next.jdbc/get-datasource`
  - `:db-options`: default options for other `next.jdbc` functions, such as
    `plan` or `execute!`; see
    https://cljdoc.org/d/com.github.seancorfield/next.jdbc/1.3.874/doc/all-the-options
- `:relational-pooled`: same as `:relational`, and
  - `:pool-options`: connection pool configuration options as camelCase
  keywords; see the connection pool library documentation and
  https://cljdoc.org/d/com.github.seancorfield/next.jdbc/1.3.874/api/next.jdbc.connection#-%3Epool
   - `:pool-library`: a connection pool library to use, one of `:hikari-cp`, `:c3p0`

- `:datahike`:
  - `:db-spec`: a database specification accepted by `datahike.api` database
    manipulation functions like `create-database` and `connect`; see
    https://github.com/replikativ/datahike/blob/master/doc/config.md
  - `:db-options`:
    - `:delete-db`: delete the database described by db-spec if it exists before
      creating a new one; default is false

### Dependencies

Implementation dependencies are not included in component dependencies to avoid
polluting classpath with unneeded code and bloating the size of project
uberjars, so you must specify each used implementation's dependencies explicitly
in a consuming component/base `deps.edn`:

- `in-memory`: no dependencies
- `datahike`: `io.replikativ/datahike`, `io.replikativ/datahike-jdbc` for JDBC
  backend
- `relational`: `com.github.seancorfield/next.jdbc`,
  `com.github.seancorfield/honeysql`, necessary JDBC drivers
  (e.g. `com.h2database/h2`), a connection pool library, if needed (e.g.,
  `hikari-cp/hikari-cp`)

Implementation dependency versions used in development and testing are specified
under the `:dev` alias in the component's `deps.edn`.

## Develop

Start a REPL

```
clj -A:dev
```

or use your favourite editor to start one.

## Build

List available build tasks and their usage summaries

```
clojure -T:build
```

See also documentation of projects/bases that use this component.

## Test

Run all tests

```
clojure -M:dev:test
```
See Kaocha documentation for additional information.
