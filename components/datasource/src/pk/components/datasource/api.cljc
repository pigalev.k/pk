(ns pk.components.datasource.api)


(defmulti datasource
  "Creates a datasource.
  See implementation's docs for the supported parameters."
  :type)

(defmulti release!
  "Releases the datasource."
  type)


(defmethod datasource :default
  [{:keys [type]}]
  (throw (ex-info "No database implementation found for the given type"
                  {:type type})))

(defmethod release! :default [o]
  (throw (ex-info "Don't know how to release datasource of the given type"
                  {:value o :type (type o)})))
