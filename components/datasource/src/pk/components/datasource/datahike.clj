(ns pk.components.datasource.datahike
  "Managing Datahike datasources."
  (:require
   [datahike-jdbc.core]
   [datahike.api :as d]
   [pk.components.datasource.api :as dba])
  (:import (clojure.lang Atom)
           (java.util Date)))


(defmethod dba/datasource :datahike
  [{:keys [db-spec db-options]}]
  (let [{:keys [delete-db]} db-options]
    (when delete-db
      (d/delete-database db-spec))
    (when (not (d/database-exists? db-spec))
      (d/create-database db-spec))
    (d/connect db-spec)))

(defmethod dba/release! Atom
  [datasource]
  (when (instance? datahike.db.DB @datasource)
    (d/release datasource)))
