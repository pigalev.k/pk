(ns pk.components.datasource.relational
  "Manages relational datasources."
  (:require
   [next.jdbc :as jdbc]
   [next.jdbc.connection :as connection]
   [next.jdbc.date-time]
   [next.jdbc.result-set :as rs]
   [pk.components.datasource.api :as dba]))


(def default-db-options
  "Default options for the `next.jdbc` data manipulation an query functions.
  See
  `https://cljdoc.org/d/com.github.seancorfield/next.jdbc/1.3.874/doc/all-the-options`
  for the full list."
  {:builder-fn rs/as-unqualified-kebab-maps})

(defn load-class
  "Load class by `class-name`.
  Returns the loaded class. If class cannot be loaded, returns nil."
  [class-name]
  (try
    (.. ClassLoader
        getSystemClassLoader
        (loadClass class-name))
    (catch Exception _ nil)))

(def pool-library-class
  "Maps pool library names to pool implementation class names."
  {:hikari-cp (load-class "com.zaxxer.hikari.HikariDataSource")
   :c3p0      (load-class "com.mchange.v2.c3p0.PooledDataSource")})


(defmethod dba/datasource :relational
  [{:keys [db-spec db-options]}]
  (-> (jdbc/get-datasource db-spec)
      (jdbc/with-options (merge default-db-options db-options))))

(defmethod dba/datasource :relational-pooled
  [{:keys [db-spec db-options pool-library pool-options]
    :or {pool-library :hikari-cp}}]
  (-> (connection/->pool (pool-library-class pool-library)
                         (merge db-spec pool-options))
      (jdbc/with-options (merge default-db-options db-options))))


;; once `next.jdbc` datasource is created, it produces and closes connections
;; automatically, no need to close the datasource itself; closing it will have
;; no effect. If the datasource is pooled, pool should be closed properly.

(defmethod dba/release! next.jdbc.default_options.DefaultOptions
  [{:keys [connectable]}]
  (let [pool-library-classes (keep identity (vals pool-library-class))]
    (doseq [c pool-library-classes]
      (when (instance? c connectable)
        (.close connectable)))))
