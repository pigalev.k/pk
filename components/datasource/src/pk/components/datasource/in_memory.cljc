(ns pk.components.datasource.in-memory
  "Managing in-memory datasources.
  In-memory datasource is a data structure (set, vector, etc) in an
  atom."
  (:require
   [pk.components.datasource.api :as dba]))


(defmethod dba/datasource :in-memory-set
  [{:keys [sorted]}]
  (cond
    sorted (atom (sorted-set))
    :else  (atom #{})))

(defmethod dba/datasource :in-memory-vector
  [_]
  (atom []))

;; no need to release an in-memory datasource, just forget a reference to it and
;; it will be garbage-collected
