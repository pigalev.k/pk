(ns pk.components.datasource.relational-test
  (:require
   [clojure.test :as test :refer [testing deftest is use-fixtures]]
   [next.jdbc :as jdbc]
   [pk.components.datasource.api :as dsa]
   [pk.components.datasource.relational]))


(defonce ^:dynamic *datasources* nil)

(defn datasources
  "A fixture that instantiates in-memory Datahike datasource and binds
  it to `*datasource*` once before tests."
  [t]
  (let [relational-ds        (dsa/datasource {:type       :relational
                                              :db-spec    {:dbname "hello-db"
                                                           :dbtype "h2:mem"}
                                              :db-options {}})
        pooled-relational-ds (dsa/datasource
                              {:type         :relational-pooled
                               :db-spec      {:dbname "hello-db-pooled"
                                              :dbtype "h2:mem"}
                               :db-options   {}
                               :pool-options {:poolName        "hello-pool"
                                              :minimumIdle     1
                                              :maximumPoolSize 3}})]
    (binding [*datasources* {:relational        relational-ds
                             :pooled-relational pooled-relational-ds}]
      (t))))

(use-fixtures :once datasources)


(def drop-table ["DROP TABLE IF EXISTS hello;"])
(def create-table ["CREATE TABLE hello (name VARCHAR(10));"])
(def insert ["INSERT INTO hello (name) VALUES (?), (?);" "Joe" "Molly"])
(def select ["SELECT * FROM hello;"])


(deftest relational-test
  (testing "relational datasource"
    (let [ds (:relational *datasources*)]
      (is (do
            (jdbc/execute! ds drop-table)
            (jdbc/execute! ds create-table)
            (jdbc/execute! ds insert))
          "can execute DDL/DML statements")
      (is (= 2 (count (jdbc/execute! ds select)))
          "can execute queries")
      (is (= nil (dsa/release! ds))
          "can be released"))))

(deftest pooled-relational-test
  (testing "pooled relational datasource"
    (let [ds (:pooled-relational *datasources*)]
      (is (do
            (jdbc/execute! ds drop-table)
            (jdbc/execute! ds create-table)
            (jdbc/execute! ds insert))
          "can execute DDL/DML statements")
      (is (= 2 (count (jdbc/execute! ds select)))
          "can execute queries")
      (is (= nil (dsa/release! ds))
          "can be released"))))
