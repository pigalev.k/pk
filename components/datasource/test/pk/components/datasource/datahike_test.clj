(ns pk.components.datasource.datahike-test
  (:require
   [clojure.test :as test :refer [testing deftest is use-fixtures]]
   [pk.components.datasource.api :as dsa]
   [pk.components.datasource.datahike]
   [datahike.api :as d])
  (:import (java.util Date)))


(defonce ^:dynamic *datasource* nil)

(defn datasource
  "A fixture that instantiates in-memory Datahike datasource and binds
  it to `*datasource*` before each test."
  [t]
  (let [ds (dsa/datasource {:type       :datahike
                            :db-spec    {:store              {:id      "hello-db"
                                                              :backend :mem}
                                         :name               "hello-db"
                                         :schema-flexibility :read
                                         :keep-history?      true}
                            :db-options {:delete-db true}})]
    (binding [*datasource* ds]
      (t))))

(use-fixtures :each datasource)


(def tx-data
  [{:db/id           -1
    :user/name       "Joe"
    :user/email      "joe@example.com"
    :user/created-at (Date.)}
   {:db/id           -2
    :user/name       "Molly"
    :user/email      "molly@example.com"
    :user/created-at (Date.)}])

(def query
  '[:find (pull ?e [*])
    :where
    [?e :user/name]])


(deftest datahike-test
  (testing "Datahike datasource"
    (is (instance? datahike.db.TxReport (d/transact *datasource* tx-data))
        "can transact")
    (is (= 2 (count (d/q query @*datasource*)))
        "can be queried")
    (is (= nil (dsa/release! *datasource*))
        "can be released")))
