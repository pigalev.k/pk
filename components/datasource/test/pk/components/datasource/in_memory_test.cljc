(ns pk.components.datasource.in-memory-test
  (:require
   [clojure.set :as s]
   [clojure.test :as test :refer [testing deftest is use-fixtures]]
   [pk.components.datasource.api :as dsa]
   [pk.components.datasource.in-memory])
  (:import (clojure.lang PersistentHashSet PersistentTreeSet PersistentVector)))


(defonce ^:dynamic *datasources* nil)

(defn datasources
  "A fixture that instantiates in-memory datasources and binds a map of
  them to `*datasources*`."
  [t]
  (let [set-ds        (dsa/datasource {:type :in-memory-set})
        sorted-set-ds (dsa/datasource {:type :in-memory-set :sorted true})
        vector-ds     (dsa/datasource {:type :in-memory-vector})]
    (binding [*datasources* {:set        set-ds
                             :sorted-set sorted-set-ds
                             :vector     vector-ds}]
      (t))))

(use-fixtures :once datasources)


(deftest in-memory-set-test
  (testing "in-memory datasource"
    (testing "backed by set"
      (let [set-ds       (:set *datasources*)
            set-ds-value @set-ds]
        (is (= PersistentHashSet (type set-ds-value))
            "contains a hash set")
        (let [_            (swap! set-ds conj "hello" "there" "cully")
              _            (swap! set-ds conj "hello")
              set-ds-value @set-ds]
          (is (= 3 (count set-ds-value))
              "inserts values correctly, does not allow duplicates"))
        (let [_            (swap! set-ds s/difference #{"cully"})
              set-ds-value @set-ds]
          (is (= 2 (count set-ds-value))
              "removes values correctly"))
        (is (= nil (dsa/release! set-ds))
            "can be released")))
    (testing "backed by sorted set"
      (let [sorted-set-ds       (:sorted-set *datasources*)
            sorted-set-ds-value @sorted-set-ds]
        (is (= PersistentTreeSet (type sorted-set-ds-value))
            "contains a tree set")
        (let [_
              (swap! sorted-set-ds conj "hello" "there" "cully")
              _
              (swap! sorted-set-ds conj "hello" "there" "cully")
              sorted-set-ds-value @sorted-set-ds]
          (is (= 3 (count sorted-set-ds-value))
              "inserts values correctly, does not allow duplicates")
          (is (= "cully" (first sorted-set-ds-value))
              "sorts its values in ascending order"))
        (let [_                   (swap! sorted-set-ds s/difference #{"cully"})
              sorted-set-ds-value @sorted-set-ds]
          (is (= 2 (count sorted-set-ds-value))
              "removes values correctly"))
        (is (= nil (dsa/release! sorted-set-ds))
            "can be released")))))

(deftest in-memory-vector-test
  (testing "in-memory datasource"
    (testing "backed by vector"
      (let [vector-ds       (:vector *datasources*)
            vector-ds-value @vector-ds]
        (is (= PersistentVector (type vector-ds-value))
            "contains a vector")
        (let [_               (swap! vector-ds conj "hello" "there" "cully")
              _               (swap! vector-ds conj "hello" "there" "cully")
              vector-ds-value @vector-ds]
          (is (= 6 (count vector-ds-value))
              "inserts values correctly, allow duplicates")
          (is (= "hello" (first vector-ds-value))
              "stores values in insertion order"))
        (let [_               (swap! vector-ds #(remove #{"cully"} %))
              vector-ds-value @vector-ds]
          (is (= 4 (count vector-ds-value))
              "removes values correctly"))
        (is (= nil (dsa/release! vector-ds))
            "can be released")))))
