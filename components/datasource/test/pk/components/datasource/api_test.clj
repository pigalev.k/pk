(ns pk.components.datasource.api-test
  (:require
   [clojure.test :as test :refer [testing deftest is]]
   [pk.components.datasource.api :as dsa]))


(deftest datasource-api-test
  (testing "datasource"
    (is (thrown? Exception (dsa/datasource {:type :absent-implementation}))
        "throws if datasource type is unsupported"))
  (testing "release!"
    (is (thrown? Exception (dsa/release! :not-a-datasource))
        "throws if datasource type is unsupported")))
