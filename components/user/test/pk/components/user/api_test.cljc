(ns pk.components.user.api-test
  (:require
   [clojure.test :as test :refer [testing deftest is]]
   [pk.components.user.api :as u])
  (:import (java.util UUID)))


(deftest user-test
  (testing "user"
    (let [random-id (UUID/randomUUID)
          an-user (u/->User random-id)]
      (is (uuid? (u/id an-user))
          "have a correct id"))))
