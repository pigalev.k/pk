(ns pk.components.user.api
  "Create and manipulate users."
  (:refer-clojure :exclude [replace update]))


;; protocols

(defprotocol PUser
  "An actor that can interact with programs (e.g., person or other program)."
  (id [this]))

(defprotocol UserStore
  "A store for users."
  (insert [this item])
  (replace [this item])
  (update [this item])
  (delete [this id])
  (find-all [this])
  (find-by-id [this id])
  (find-by-attribute [this attr pred]))


;; types

(defrecord User [id]
  PUser
  (id [_] id))
