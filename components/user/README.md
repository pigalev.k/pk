# user

`User` is an entity that represents an actor that can interact with programs
that use it (e.g., person or another program).

## Test

Run all tests

```
clojure -M:dev:test
```

See Kaocha documentation for additional information.
