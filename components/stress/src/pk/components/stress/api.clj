(ns pk.components.stress.api
  (:require
   [babashka.http-client :as http]
   [com.climate.claypoole :as cp]
   [pk.components.stress.core :as core]))


(defn run
  "Run a stress test of HTTP service.
  Sends parallel requests to `url`, using a thread pool, and returns a seq of
  responses (in order of completion)."
  [url {:keys [requests threads method]}]
  (let [request-options {:method method
                         :throw  false}]
    (->> (core/requests url request-options)
         (take requests)
         (cp/upmap threads http/request))))
