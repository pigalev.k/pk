(ns pk.components.stress.core
  "Stress tests for HTTP services."
  (:require
   [babashka.http-client :as http]))


(defn http-url?
  "Returns true if `string` is a valid HTTP(S) url.
  Only checks a protocol prefix."
  [string]
  (boolean (re-find #"^https?://" string)))

(defn http-method?
  "Returns true if keyword `kw` represents a name of HTTP method."
  [kw]
  (boolean (#{:get :post :put :patch :delete :options :head} kw)))


(defn requests
  "Infinite lazy seq of identical configurations for HTTP requests to `url`."
  [url request-options]
  (repeat (assoc request-options :uri url)))
