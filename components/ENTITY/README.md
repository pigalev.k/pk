# ENTITY

`ENTITY` is an entity that represents CONCEPT.

## Develop

Start a REPL

```
clj -A:dev
```

or use your favourite editor to start one.

## Build

List available build tasks and their usage summaries

```
clojure -T:build
```

## Test

Run all tests

```
clojure -M:test
```
See Kaocha documentation for additional information.

## Build

See documentation of projects that use this component.
