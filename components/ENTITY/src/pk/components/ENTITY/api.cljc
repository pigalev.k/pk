(ns pk.components.ENTITY.api
  "Manage ENTITIES."
  (:refer-clojure :exclude [find replace update]))


;; protocols

(defprotocol PENTITY
  "Represents CONCEPT."
  (id [this]))

(defprotocol ENTITYStore
  "A store for ENTITIES."
  (insert [this item])
  (replace [this id item])
  (update [this id item])
  (delete [this id])
  (find [this params])
  (find-by-id [this id]))


;; types

(defrecord ENTITY [id]
  PENTITY
  (id [_] id))

(defn create-ENTITY
  "Create new ENTITY from `ENTITY-data` map.
  If there is `id` attribute in `ENTITY-data`, its value will be used
  as the ENTITY ID. If `id-fn` is provided, it will be applied to
  `id-fn-args` to generate the ENTITY ID. If both are provided, `id`
  is preferred. If none are provided, a random UUID is generated."
  [ENTITY-data & {:keys [id-fn id-fn-args]}]
  (cond
    (id ENTITY-data) (map->ENTITY ENTITY-data)
    :else (let [ENTITY-id (if id-fn (apply id-fn id-fn-args) (random-uuid))]
            (map->ENTITY (merge {:id ENTITY-id} ENTITY-data)))))


;; extending some existing types to support PENTITY protocol

#?(:clj
   (extend-protocol PENTITY
     clojure.lang.APersistentMap
     (id [this] (get this :id))
     nil
     (id [_] nil)))

#?(:cljs
   (extend-protocol PENTITY
     cljs.core.PersistentArrayMap
     (id [this] (get this :id))
     cljs.core.PersistentHashMap
     (id [this] (get this :id))
     cljs.core.PersistentTreeMap
     (id [this] (get this :id))
     default
     (id [this] (.-id this))))


(comment

     (id (create-ENTITY {:name "Joe"}))
     (id (create-ENTITY {:id 12 :name "Molly"}))
     (id (create-ENTITY {:id 12 :name "Molly"}
                        {:id-fn inc
                         :id-args [11]}))
     (id {:id 12 :name "Molly"})

     #?(:cljs (id (create-ENTITY {:name "Joe"})))
     #?(:cljs (id {:id 12 :name "Molly"}))
     #?(:cljs (id #js {:id 12 :name "Molly"}))

)
