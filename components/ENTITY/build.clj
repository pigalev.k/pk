(ns build
  "Component tasks --- building, installing, etc."
  (:require
   [build.api :as a]
   [clojure.tools.build.api :as b]))


(def project-name "ENTITY")
(def lib (symbol (format "pk/%s" project-name)))
(def version (format "0.1.%s" (b/git-count-revs nil)))

(def src-dirs ["src" "resources"])
(def build-dir "target/")
(def class-dir (str build-dir "classes/"))
(def basis (b/create-basis {:project "deps.edn"}))

(def jar-file (format "%s-%s.jar" project-name version))


(a/set-config! {:project-name              project-name
                :lib                       lib
                :version                   version
                :src-dirs                  src-dirs
                :build-dir                 build-dir
                :class-dir                 class-dir
                :basis                     basis
                :jar-file                  jar-file})


;; misc

(def help
  "List all available tasks with summaries of their docstrings."
  a/help)

(def clean
  "Clean all build results."
  a/clean)


;; building

(def jar
  "Pack the sources in a jar file."
  a/library-jar)


;; installing

(def install
  "Install sources jar into local Maven repository."
  a/install-library)
