(ns scratch
  "Experimentation playground."
  (:require
   [pk.components.alias.core :as c]
   [pk.components.alias.impl]
   [pk.components.alias.protocols :as p]))


(comment

  (def a0 (p/alias "http://some.site"))
  (def a1 (p/alias "http://some.other.site"))
  (def a2 (p/alias "http://another.site" {:length 10}))

  (let [prefix "https://sh.rt/"]
    (for [s [a0 a1 a2]]
         (format "%s%s" prefix (:short s))))


  (p/alias 1)
  (p/alias {:id 1 :name "asd"})


)
