(ns pk.components.alias.api-test
  (:require [clojure.test :as test :refer [deftest testing is]]
            [pk.components.alias.core :as c]
            [pk.components.alias.api :as a]))


(deftest alias--from-string-test
  (testing "alias created from string"
    (let [original-string "http://some.site"
          an-alias (a/alias original-string {:length 7})]
      (is (string? (a/short an-alias))
          "have a string short representation")
      (is (string? (a/original an-alias))
          "have a string original representation")
      (is (= 7 (count (a/short an-alias))) "have short of correct length")
      (is (every? (set c/lowercase-latin-chars) (a/short an-alias))
          "have short composed of correct characters")
      (is (= original-string (a/original an-alias)) "have correct original"))))
