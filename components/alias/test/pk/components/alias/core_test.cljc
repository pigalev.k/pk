(ns pk.components.alias.core-test
  (:require
   [clojure.test :as test :refer [deftest testing is]]
   [pk.components.alias.core :as c]))


(def valid-url
  "https://www.ex.co/usrs/1/i?ds[]=name+a%20&foo=bar,baz,quux&sort=id:asc#id-1")

(def invalid-url "nope")


(deftest rand-string--produces-strings-of-required-length-test
  (testing "rand-string produces strings with the length"
    (is (= 1 (count (c/rand-string))) "1 by default"))
    (is (= 10 (count (c/rand-string 10))) "specified by parameter"))

(deftest http-url?-test
  (testing "http-url?"
    (is (c/http-url? valid-url)
        "accepts valid urls")
    (is (not (c/http-url? invalid-url))
        "refuses invalid urls")))
