(ns pk.components.alias.api
  "Create aliases for objects."
  (:refer-clojure :exclude [alias short]))


;; alias

(defprotocol PAlias
  "A relationship between an object and its short representation."
  (short [this])
  (original [this]))

(defrecord Alias [original short]
  PAlias
  (short [_] short)
  (original [_] original))

(defprotocol IntoAlias
  "Something that may be given an alias (short representation)."
  (alias [this] [this opts]))
