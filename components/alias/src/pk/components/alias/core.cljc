(ns pk.components.alias.core
  (:refer-clojure :exclude [alias short])
  (:require
   [clojure.spec.alpha :as s]
   [pk.components.alias.api :as a])
  #?(:clj (:import (java.lang String))))


;; domain functions and data

(def lowercase-latin-chars
  "Sequence of lowercase latin characters."
  (map char (range 97 123)))

(defn rand-string
  "Construct random string of given length from given chars.
   Length defaults to 1, chars default to lowercase alphanumeric characters."
  ([]
   (rand-string 1 lowercase-latin-chars))
  ([length]
   (rand-string length lowercase-latin-chars))
  ([length character-set]
   (apply str (take length (repeatedly #(rand-nth character-set))))))

(def http-url-pattern
  "Regex for valid HTTP(S) URLs.
  Only checks for protocol prefix and acceptable characters."
  #"^https?://[-a-zA-Z0-9.,_\+&@#/%?=~|!:;\[\]]*$")

(defn http-url?
  "Returns true if `s` is a valid HTTP(S) URL, according to
  `http-url-pattern`."
  [s]
  (boolean (re-matches http-url-pattern s)))


;; specs

(s/def ::short (s/and string? #(seq %)))
(s/def ::original (s/and string?
                         http-url?))
(s/def ::created-at inst?)
(s/def ::alias (s/keys :req-un [::short]
                       :opt-un [::original ::created-at]))

(s/def ::length (s/and int? #(> % 3)))
(s/def ::options (s/keys :opt-un [::length]))


;; implementations of alias protocols for some basic types

(extend-type #?(:clj String
                :cljs string)
  a/IntoAlias
  (alias
    ([this]
     (a/alias this {}))
    ([this {:keys [length character-set]
            :or {length 5
                 character-set lowercase-latin-chars}}]
     (let [new-short-string (rand-string length character-set)
           new-alias (a/->Alias this new-short-string)]
       new-alias))))

(extend-type #?(:clj Object
                :cljs default)
  a/IntoAlias
  (alias
    ([this]
     (a/alias this {}))
    ([this opts]
     (throw
      (ex-info "Don't know how to alias"
               {:value this :type (type this) :opts opts})))))


#?(:clj
   (extend-protocol a/PAlias
     clojure.lang.PersistentArrayMap
     (short [this] (get this :short))
     (original [this] (get this :original))))

#?(:cljs
   (extend-protocol a/PAlias
     cljs.core.PersistentArrayMap
     (short [this] (get this :short))
     (original [this] (get this :original))
     default
     (short [this] (.-short ^js/Object this))
     (original [this] (.-original ^js/Object this))))
