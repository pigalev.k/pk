# alias

`Alias` is an entity representing a relationship between some value and it's
short representation, e.g. short string for a longer string, URL or contents of
a file.

## Use

Require an `api` namespace and, if needed, namespaces with implementations of
alias-related protocols.

```clojure
(ns pk.bases.hello-alias-api
  (:require
    [pk.components.alias.api :as a]
    [pk.components.alias.core])))
```

Default implementation (`core`) allows to
- create aliases from strings representing HTTP URLs using randomly generated
  short strings
- use maps/JS objects as aliases (access alias attributes)

Create an alias

```clojure
(def an-alias (a/alias "http://example.com"))
```

Access alias attributes

```clojure
(a/short an-alias)
(a/original an-alias)

(a/short {:short "abcd"})
(a/original #js {:original "http://example.com"})
```

## Develop

Start a REPL

```
clj -A:dev
```

or use your favourite editor to start one.

## Build

List available build tasks and their usage summaries

```
clojure -T:build
```

## Test

Run all tests

```
clojure -M:dev:test
```

See Kaocha documentation for additional information.
