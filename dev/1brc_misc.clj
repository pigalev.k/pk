(ns dev.1brc-misc
  (:require
   [clojure.string :as s]
   [clojure.core.reducers :as r]
   [clojure.core.async :refer [go go-loop chan] :as a]
   [clojure.java.io :as io]
   [dev.1brc :as brc :refer [update-total-stats scsv-split-regex combine-stats
                             read-file moving-average stats parallel-stats
                             transducing-stats read-scsv-file line->measurement]]
   [taoensso.tufte :as tufte :refer [defnp p profiled profile format-pstats]])
  (:import (java.io ByteArrayOutputStream)))


;; various experiments, expect a mess here


;; using reducers

(defn folding-next-measurement
  ([] {})
  ([acc [key val]]
   (let [num-val (parse-double val)]
     (update acc key update-total-stats num-val))))

(defn parallel-folding-stats [measurements]
  (r/fold combine-stats folding-next-measurement measurements))

;; using manual looping and transients

(defn update-stats [stats line]
  (let [[key sval]                                    (s/split line scsv-split-regex)
        val                                           (parse-double sval)
        key-stats
        (transient (get stats key {}))
        {:keys [count min max average]
         :or   {count 0
                min val
                max val
                average val}} key-stats
        new-key-stats
        (assoc! key-stats
                :count (inc count)
                :min (min min val)
                :max (max max val)
                :average (moving-average average count val))]
    (assoc! stats key (persistent! new-key-stats))))


(defn transient-stats [lines]
  (loop [stats (transient {}) lines lines]
    (if (empty? lines)
      (persistent! stats)
      (recur (update-stats stats (first lines)) (rest lines)))))

(comment

  (time (def st (transient-stats (read-file "dev/resources/measurements1M.txt"))))
  (count (keys st))

  )


(comment

  ;; a symbolic link to a potentially huge file (~12G); estimated time circa
  ;; 100 minutes
  (time (stats (read-scsv-file "dev/resources/measurements.txt")))

  (time (dorun (read-scsv-file "dev/resources/measurements.txt")))

  (time (stats (read-file "dev/resources/measurements1K.txt")))
  (time (parallel-stats (read-file "dev/resources/measurements1K.txt")))

  ;; sequence is not foldable, so no good
  (time (stats (read-file "dev/resources/measurements1M.txt")))
  (time (parallel-stats (read-file "dev/resources/measurements1M.txt")))


  (time (transducing-stats (read-file "dev/resources/measurements1M.txt")))

  )

(comment

  (def filename "dev/resources/measurements.txt")
  (def lines (read-file filename))
  (def measurements (read-scsv-file filename))

  (defn cumulative-average [xs]
    (reduce-kv moving-average 0 xs))

  (cumulative-average [1 2 3 4 5 6.0])

  ;; java baseline 3:32 (~212 sec)
  (defn ignore [& args] nil)
  (time (reduce ignore (read-file "dev/resources/measurements.txt"))) ;; ~113 sec
  (time (dorun (read-file "dev/resources/measurements.txt"))) ;; ~98 sec

  )

;; manual looping, transients, arrays

(defn new-stats [stats line]
  (let [[key sval] (p :splitting-line (s/split line scsv-split-regex))
        val        (p :parsing-double (parse-double sval))
        key-stats  (p :getting-key-stats (get stats key (double-array [0.0 val val 0.0])))
        count      (p :getting-count (aget ^doubles key-stats 0))
        min-val    (p :getting-min (aget ^doubles key-stats 1))
        max-val    (p :getting-max (aget ^doubles key-stats 2))
        average    (p :getting-average (aget ^doubles key-stats 3))]
    (p :setting-count (aset-double ^doubles key-stats 0 (inc count)))
    (p :setting-min (aset-double key-stats 1 (min min-val val)))
    (p :setting-max (aset-double key-stats 2 (max max-val val)))
    (p :setting-average (aset-double key-stats 3 (moving-average average count val)))
    (p :setting-key-stats (assoc! stats key key-stats))))

(defn process [lines]
  (loop [stats (transient {}) lines lines]
    (if (empty? lines)
      (p :making-stats-persistent (persistent! stats))
      (recur (new-stats stats (first lines)) (rest lines)))))

(comment

  ;; set up profiler
  (tufte/add-basic-println-handler! {})
  ;; print the profiling information
  (profiled {} (process (read-file "dev/resources/measurements1M.txt")))

  ;; generate profiling information to inspect later
  (def res (profiled {} (process (read-file "dev/resources/measurements1M.txt"))))
  (def st @(second res))
  (println (format-pstats st))


  (time (process (read-file "dev/resources/measurements1M.txt")))


)


;; reading binary vs text

(defn read-binary-file [filename bufsize]
  (with-open [in (p :creating-input-stream (io/input-stream (io/file filename)))]
    (let [buf (p :creating-byte-array (byte-array bufsize))]
      (loop [chunks (transient [])]
        (let [n (p :read-bytes (.read in buf))]
          (if (= n bufsize)
            (recur (conj! chunks buf))
            (let [persistent-chunks (p :persisting-chunks (persistent! chunks))
                  chunks-count      (count persistent-chunks)]
              (p :printing-report
                 (println "done reading file in" chunks-count
                          "chunks of" bufsize "bytes")))))))))

(defn byte-chunk-seq [^java.io.InputStream in bufsize]
  (let [buf (byte-array bufsize)
        n   (.read in buf)]
    (if (= n bufsize)
      (cons buf (lazy-seq (byte-chunk-seq in bufsize)))
      (byte-array n buf))))

(defn delimited-byte-chunks-seq [^java.io.InputStream in
                                 ^ByteArrayOutputStream buf
                                 field-separator
                                 line-separator]
  (let [field (loop []
                (let [b (p :reading-a-byte (.read in))]
                  (cond (= (p :char-to-byte
                              (byte field-separator)) b) (let [buf-contents
                                                               (p :converting-name
                                                                  (.toString buf))]
                                                           (.reset buf)
                                                           buf-contents)
                        (= (p :char-to-byte
                              (byte line-separator)) b)  (let [buf-contents
                                                               (p :converting-value
                                                                  (parse-double
                                                                   (.toString buf)))]
                                                           (.reset buf)
                                                           buf-contents)
                        (= -1 b)                         :end-of-stream
                        :else                            (do (p :writing-a-byte
                                                                (.write buf b)) (recur)))))]
    (when (not= :end-of-stream field)
      (p :consing-field
         (cons field
               (lazy-seq
                (delimited-byte-chunks-seq in buf field-separator line-separator)))))))

(comment

  (profile {} (read-binary-file "dev/resources/measurements1M.txt" 1000))
  (profile {} (read-binary-file "dev/resources/measurements.txt" 1000))

  (time
   (def bcs (byte-chunk-seq (io/input-stream
                             (io/file "dev/resources/measurements1M.txt")) 1024)))
  (String. (first bcs))

  (time (count (byte-chunk-seq (io/input-stream
                                (io/file "dev/resources/measurements.txt")) 1024)))

  (take 4 (delimited-byte-chunks-seq (io/input-stream
                                      (io/file "dev/resources/measurements1M.txt"))
                                     (ByteArrayOutputStream.)
                                     \; \newline))

  ;; 600ms
  (time (dorun (delimited-byte-chunks-seq (io/input-stream
                                           (io/file "dev/resources/measurements1M.txt"))
                                          (ByteArrayOutputStream.)
                                          \; \newline)))

  (def res (profile {}
             (delimited-byte-chunks-seq (io/input-stream
                                         (io/file "dev/resources/measurements1M.txt"))
                                        (ByteArrayOutputStream.)
                                        \; \newline)))

  ;; 6x faster still (~100ms)
  (time (dorun (line-seq (io/reader "dev/resources/measurements1M.txt"))))

  ;; 516 sec
  (time (dorun (delimited-byte-chunks-seq (io/input-stream
                                           (io/file "dev/resources/measurements.txt"))
                                          (ByteArrayOutputStream.)
                                          \; \newline)))
)


;; observations

;; - laziness is not required

;; - even though lines are read sequentially, reducing the sequence of
;;   measurements to stats map can probably be done in parallel (offloading
;;   reduction to worker(s) in another thread(s))

;; - `core.async` channels can be used to communicate between the reader and
;;   the worker(s)



(defn update-key-stats [{:keys [count minimum maximum average]} value]
  {:count   (inc count)
   :minimum (min minimum value)
   :maximum (max maximum value)
   :average (moving-average average count value)})


(defn async-stats [filename]
  (with-open [rdr (io/reader filename)]
    ;; create the communication channel and start two processes that use this
    ;; channel to communicate
    (let [;; a channel that accepts text lines and transforms them to
          ;; measurement vectors
          measurements-chan (a/chan 1 line->measurement)
          ;; a producer process that takes lines from the file and puts them
          ;; to the measurements channel
          producer-chan (go-loop []
                          (when-let [line (.readLine rdr)]
                            (a/>! measurements-chan line)
                            (recur)))
          ;; a consumer process that takes measurements from the measurements
          ;; channel and reduces them to stats map, until the channel is
          ;; closed
          result-chan (go (a/<! (a/reduce update-stats {} measurements-chan)))]
      ;; wait for the producer to complete (process the entire file)
      (a/<!! producer-chan)
      ;; signal to the consumer process that there are no more values
      (a/close! measurements-chan)
      ;; return the result of the reduction
      (a/<!! result-chan))))

(defn format-stats [stats]
  (into (sorted-map) stats))

(comment

  ;; ~4-6 sec, no cigar
  (time (format-stats (async-stats "dev/resources/measurements1M.txt")))

)

;; using transducers + `core.async`

(defn measurements [filename]
  (sequence line->measurement (read-file filename)))

(defn async-transducing-stats [filename]
  (let [ch     (chan)
        res-ch (go (a/<! (a/reduce update-total-stats {} ch)))]
    (dorun
     (for [measurement (measurements filename)]
       (a/>!! ch measurement)))
    (a/close! ch)
    (a/<!! res-ch)))

(comment

  (time (dorun (measurements "dev/resources/measurements1M.txt")))

  ;; 13 secs
  (time (def s (async-transducing-stats "dev/resources/measurements1M.txt")))

)

;; skipping lines with Scanner


(defn scanning
  "Reads the file line by line."
  [filename]
  (let [scanner (java.util.Scanner. (io/input-stream filename))
        line-pattern #".*?\n"]
    (loop [lines-count 0]
      (if (.hasNextLine scanner)
        (do
          (.nextLine scanner)
          (recur (inc lines-count)))
        lines-count))))


(comment

  (time (scanning "dev/resources/measurements1M.txt"))
  (time (scanning "dev/resources/measurements.txt"))

)
