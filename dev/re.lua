-- Returns true if message contains the substring with last byte removed
function contains(message, substring)
   return string.find(message, substring:sub(1, -2), 1, true)
end


message = "Вы меряете взглядом Лепрогнома-работника."
q3nik1 = "Лепрогнома-работника,"

if contains(message, q3nik1) then
    print("ТипаБорто")
end
