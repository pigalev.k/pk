(ns fns)

(defn slow-ascending-fn
  "y = floor(1/2 * x)"
  [x]
  (int (* 1/2 x)))

(defn fast-ascending-fn
  "y = x^2"
  [x]
  (* x x))

(def combined-fn
  "y = (floor(1/2 * x))^2"
  (comp fast-ascending-fn slow-ascending-fn))

(def naturals (iterate inc 0))
(def slow (map slow-ascending-fn naturals))
(def fast (map fast-ascending-fn naturals))
(def combined (map combined-fn naturals))

(comment

  (take 10 naturals)
  (take 10 slow)
  (take 10 fast)
  (take 10 combined)

)
