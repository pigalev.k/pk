(ns recur)


(defn factorial-natural
  "Returns factorial of a natural number `n`.
  May blow the call stack for large `n`s. Can cause integer overflow
  for large `n`s."
  [n]
  (cond
    (= 1 n) 1
    :else   (* n (factorial-natural (dec n)))))

(comment

  (factorial-natural 5)
  (try
    (factorial-natural 100)
    (catch ArithmeticException e "it throws"))
  (try
    (factorial-natural 100000N)
    (catch StackOverflowError e "it throws again"))

)


(defn factorial-recur
  "Returns factorial of a natural number `n`.
  Does not blow the call stack for large `n`s. Uses an auxiliary
  `loop` with extra parameters to store state and maintain tail
  position of the recursive call. Still can cause integer overflow for
  large `n`s (use BigInt)."
  [n]
  (loop [counter n result 1]
    (cond
      (= 1 counter) result
      :else         (recur (dec counter) (* counter result)))))

(comment

  (factorial-recur 5)
  (try
    (factorial-recur 100)
    (catch ArithmeticException e "it throws"))
  (factorial-recur 100000N)

)


;; generating a vector


(defn my-list [n]
  (if (zero? n) [] (conj (my-list (dec n)) n)))

(defn my-list* [end]
  (loop [counter 0 result []]
    (if (= counter end)
      result
      (recur (inc counter) (conj result counter)))))

(defn my-list** [end]
  (for [i (range end)] i))

(defn dot [xs ys]
  (reduce + (map * xs ys)))
;; => #'recur/dot

(type (defn dot [xs ys]
        (reduce + (map * xs ys))))
;; => clojure.lang.Var

(comment

  (my-list 8)
  ;; => [1 2 3 4 5 6 7 8]

  (my-list* 8)
  ;; => [0 1 2 3 4 5 6 7]

  (my-list** 8)
  ;; => (0 1 2 3 4 5 6 7)

)

(defn integers [x]
  (cons x (lazy-seq (integers (inc x)))))

(def naturals (integers 1))

(take 10 naturals)
;; => (1 2 3 4 5 6 7 8 9 10)
