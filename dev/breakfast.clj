(ns breakfast
  (:require
   [clojure.spec.alpha :as s]
   [clojure.string :as string]))


;; Breakfast Formal Grammar (BFG)

;; https://craftinginterpreters.com/representing-code.html


(s/def ::bread #{"toast" "bisquits" "English muffin"})
(s/def ::protein
  (s/or :sausage #{"sausage"}
        :cooked-eggs (s/cat :cooked #{"scrambled" "poached" "fried"}
                            :eggs #{"eggs"})
        :crispy-bacon (s/cat :really+ (s/+ #{"really"})
                             :crispy-bacon #{"crispy bacon"})))

(s/def ::breakfast
  (s/or :protein-with-breakfast
        (s/cat :protein ::protein
               :with-breakfast (s/? (s/cat :with #{"with"}
                                           :breakfast ::breakfast
                                           :side #{"on the side"})))
        :bread ::bread))


(comment

  ;; Would you like some recursive breakfast?

  (->> (s/exercise ::breakfast 1)
       ffirst
       flatten
       (string/join " ")
       string/capitalize
       (#(str % ".")))


  ;; Is it edible?

  (s/conform ::breakfast "English muffin")
  (s/conform ::breakfast '(("really" "crispy bacon")
                           "with" "English muffin" "on the side"))
  (s/conform ::breakfast '(("really" "crispy bacon")
                           "with"
                           (("really" "crispy bacon")
                            "with" "bisquits" "on the side")
                           "on the side"))

)
