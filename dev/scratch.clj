(ns scratch
  (:require
   [clojure.test :as tst]
   [clojure.test.check :as tc]
   [clojure.test.check.generators :as gen]
   [clojure.test.check.properties :as prop]
   [clojure.test.check.clojure-test :as tcct]))




;; define properties to check them manually

(def even-numbers
  "Product of two numbers (even and odd) is always even."
  (prop/for-all [even (gen/such-that
                       even? gen/large-integer 100)
                 odd (gen/such-that
                      odd? gen/large-integer 100)]
                (even? (*' even odd))))

(tc/quick-check 1000 even-numbers)


;; define property-based tests to run with clojure.test

(tcct/defspec even-numbers-test
  1000
  even-numbers)

(tst/run-tests)


;; experiments

(comment

  (gen/sample gen/large-integer)

)
