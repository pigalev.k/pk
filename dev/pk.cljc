(ns dev.pk
  "Experiments with code, libraries etc."
  (:require
   [clojure.core :refer [format]]))

(comment
  (defn hayo
    "Make a greeting."
    ([] (hayo "Polylith"))
    ([a-name]
     (format "HAYO %s!" a-name)))

  (hayo)

  (def lowercase-latin-chars
    "Sequence of lowercase latin characters."
    (map char (range 97 123)))

  (defn rand-string
    "Construct random string of given length from given chars.
     Length defaults to 1, chars default to lowercase alphanumeric characters."
    ([]
     (rand-string 1 lowercase-latin-chars))
    ([length]
     (rand-string length lowercase-latin-chars))
    ([length chars]
     (apply str (take length (repeatedly #(rand-nth chars))))))

  (rand-string 5)


)


;; getting protocols/interfaces/classes that a type participates in

(comment

  (defprotocol PFoo
    (foo [this] [this that] "doc"))

  (defrecord Foo [name]
    PFoo
    (foo [_] (println name) name))

  (def j (Foo. "joe"))

  (foo j)

  (bean Foo)
  (ancestors Foo)
  (extenders PFoo)

  (defn interfaces [o]
    (for [interface (.getInterfaces (class o))]
      (.getCanonicalName ^Class interface)))

  (interfaces Foo)
  (interfaces j)


  PFoo
  (let [f (-> PFoo
                :sigs
                :foo)]
    (cons (:name f) (:arglists f)))


)
