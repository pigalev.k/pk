(ns time
  (:require [java-time.api :as jt]))


(let [start (jt/local-date 2022 2 1)
      now   (jt/local-date)]
  (jt/time-between start now :days))
