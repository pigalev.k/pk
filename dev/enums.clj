(ns enums)

;; collection of enums

(def enums (atom {}))

;; functions that operate on the collection and on enums

(defn add-enum [name & items]
  (swap! enums assoc name (vec items)))

(defn find-index [pred vec]
  (reduce-kv
    (fn [_ k v]
      (if (pred v)
        (reduced k)))
    nil
    vec))

(defn enum-value-index [enum-name enum-value]
  (if-let [enum (get @enums enum-name)]
    ;; (.indexOf enum enum-value)
    (find-index #{enum-value} enum)
    -1))

;; convenience macros

(defmacro defenum [name & items]
  `(apply add-enum '~name '~items))

(defmacro one []
  (list 'enum-value-index ''my-enum ''one))

;; usage

;; (re)defining enums and retrieving values

(defenum my-enum one two three)
(println (one))

(defenum my-enum two three one)
(println (one))

;; no such enum
(println (enum-value-index 'no-such-enum 'one))
;; no such name
(println (enum-value-index 'my-enum 'no-such-name))

@enums

(comment

  (defmacro env []
    (mapv #(type (.init %)) (vals &env)))

  (let [a (+ 1 2)
        b "foo"]
    (env))

  (defmacro form [arg]
    [arg (type &form)])

  (form (+ 1 2 3))

)
