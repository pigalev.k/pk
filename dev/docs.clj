(ns docs
  "Display any Clojure source file or Markdown document as HTML page.
  Paths are relative to the project root."
  (:require
   [nextjournal.clerk :as clerk]))


(comment

  ;; start Clerk's built-in webserver on the default port 7777, opening the
  ;; browser when done
  (clerk/serve! {:browse? true})

  ;; either call `clerk/show!` explicitly
  (clerk/show! "docs/styleguides.md")
  (clerk/show! "dev/docs.clj")
  (clerk/show! "projects/hello-datahike/src/hello_datahike/datalog.clj")

  ;; or let Clerk watch the given `:paths` for changes
  (clerk/serve! {:watch-paths ["doc" "development/src"]})

  ;; stop Clerk's webserver and file watcher
  (clerk/halt!)

)
