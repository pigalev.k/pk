(ns ya)

(defn yablokon [digits target]
  (let [[head & tail] digits]
    (cond
      (empty? tail) '()
      (= head target) head)))
