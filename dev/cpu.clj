(def code
  '(do
     (require '[babashka.process :refer [shell]])
     (require '[clojure.string :refer [split split-lines]])
     (let [stats (-> (shell {:out :string :extra-env {"LANG" "C"}} "mpstat" "1" "1") :out)
           idle (-> stats
                    split-lines
                   (nth 3)
                   (split #"\s+")
                   last
                   parse-double)]
       (->> (- 100.0 idle)
            (printf "CPU: %.1f%%")))))

(eval code)
(str code)
