(ns date-seq
  (:require
   [java-time.api :as jt]))


(defn yearly-within
  "Returns a sequence of yearly local dates starting at `date` that fit
  within the period delimited by the `start` and `end` dates."
  [date start end]
  (when (jt/not-after? date end)
    (let [dates (jt/iterate jt/plus date (jt/years 1))]
      (->> dates
           (drop-while (fn [b-day] (jt/before? b-day start)))
           (take-while (fn [b-day] (jt/not-after? b-day end)))))))

(comment

  (def five-birthdays (let [birthday (jt/local-date 1984 2 29)
                            start (jt/local-date 2000 1 1)
                            end (jt/local-date 2004 12 1)]
                        (yearly-within birthday start end)))

  (count five-birthdays) ;; => 5
  (map jt/format five-birthdays)
  ;; => ("2000-02-28" "2001-02-28" "2002-02-28" "2003-02-28" "2004-02-28")

  (def no-birthdays (let [birthday (jt/local-date 2005 2 28)
                          start (jt/local-date 2000 1 1)
                          end (jt/local-date 2004 12 1)]
                      (yearly-within birthday start end)))

  (count no-birthdays) ;; => 0
  (map jt/format no-birthdays) ;; => ()

)
