(ns find-successive-chars
  (:require
   [clojure.java.io :as io]
   [clojure.string :as s]))

(defn successive-chars-regex
  "Returns a regex that matches a string containing given (unicode) chars in order."
  [chars]
  (let [unicode-escaped-chars    (map #(format "\\u%04x" (int %)) chars)
        exclude-chars-subpattern (format "[^%s]*" (s/join unicode-escaped-chars))]
    (->> unicode-escaped-chars
         (reduce (fn [acc val]
                   (str acc val exclude-chars-subpattern)) exclude-chars-subpattern)
         re-pattern)))

(defn successive-chars?
  "Returns true if the string contains given (unicode) chars in order."
  [chars string]
  (boolean (re-matches (successive-chars-regex chars) string)))

(defn word-seq
  "Returns a sequence of words in the string."
  [string {:keys [unicode]}]
  (re-seq (if unicode #"[\p{L}\p{N}]+" #"\w+") string))

(defn find-words-1
  "Returns a sequence of words in `file` that contain `chars` in order."
  [file chars]
  (->> (line-seq (io/reader file))
       (keep #(word-seq % {:unicode true}))
       flatten
       (filter #(successive-chars? chars %))))


;; a new approach: create a seq of words from the file and filter it by
;; applying several regexes to each word

(defn re-some-matches
  "Returns true if any of `regexes` matches `string`."
  [regexes string]
  (boolean (some #(re-matches % string) regexes)))

(defn file-word-seq
  "Returns a sequence of words in `file`, matched by `word-regex`."
  [file word-regex]
  (->> (line-seq (io/reader file))
       (keep #(re-seq word-regex %))
       flatten))

(defn find-words
  "Returns a sequence of words in `file` that match at least one of
  `regexes`."
  ([regexes file]
   (find-words regexes file #"[\p{L}\p{N}]+"))
  ([regexes file word-regex]
   (->> (file-word-seq file word-regex)
        (filter #(re-some-matches regexes %)))))


(comment

  (find-words [(successive-chars-regex [\е \ч \н \о])
                   (successive-chars-regex  [\а \е \и \о \у])]
                  "/tmp/Звёздная пехота.txt")

  (find-words [(successive-chars-regex "аиеая")] "/tmp/Звёздная пехота.txt")
  (find-words [(successive-chars-regex "дсцплнрнм")] "/tmp/Звёздная пехота.txt")

  (def successive-russian-vowels-regexes
    "Match words with 5 (Russian) vowels in alphabetical order."
    ;; add more character sequences --- by hand or generate
    (let [vowel-variants ["аеиоу" "аеуюя" ]]
      (map successive-chars-regex vowel-variants)))

  (find-words successive-russian-vowels-regexes "/tmp/Звёздная пехота.txt")


  (re-some-matches [#"\d+" #"\w+" #"a"] "foo")
  (re-some-matches [#"\d+" #"\w+" #"a"] "ёфй")
  (re-some-matches [#"\d+" #"\w+" #"a" #"\p{L}+"] "ёфй")

  (count (file-word-seq "/tmp/Звёздная пехота.txt"))
  (take 5 (file-word-seq "/tmp/Звёздная пехота.txt"))
  (filter #(re-matches (successive-chars-regex "оеа") %)
          (file-word-seq "/tmp/Звёздная пехота.txt"))


  (word-seq " foo    bar, baz! 123 456a" {:unicode true})
  (word-seq " foo    bar, baz! 123 456a" {:unicode false})
  (word-seq " Привет, мир!   123 456a 789ё" {:unicode true})
  (word-seq " Привет, мир!   123 456a 789ё" {:unicode false})


  (def sst (line-seq (io/reader "/tmp/Звёздная пехота.txt")))
  (word-seq (first sst) {:unicode true})


  (def c [\е \о])
  (re-matches #"[\u0430]" "а")
  (re-matches (successive-chars-regex [\а]) "а")
  (re-matches (successive-chars-regex c) "вечно")

  (successive-chars? c "вечно")
  (successive-chars? c "вечн")

  (->> sst
       (keep word-seq)
       flatten
       (filter #(successive-chars? [\а \е \о \у] %)))

  (find-words-1 "/tmp/Звёздная пехота.txt" [\е \ч \н \о])
  (find-words-1 "/tmp/Звёздная пехота.txt" [\е \ч \о])
  (find-words-1 "/tmp/Звёздная пехота.txt" [\а \о \у \и \е])
  (find-words-1 "/tmp/Звёздная пехота.txt" [\а \е \и \о \у])
  (find-words-1 "/tmp/Звёздная пехота.txt" "аеиоа")

  (find-words-1 "find_successive_chars.clj" [\a \t \t])

)
