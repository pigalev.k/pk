(ns score
  (:require
   [clojure.string :as string]))


(defn char-score
  "Returns score for character `c`."
  [c]
  (case (string/lower-case c)
    ("a" "e" "i" "o" "u" "l" "n" "r" "s" "t") 1
    ("d" "g")                                 2
    ("b" "c" "m" "p")                         3
    ("f" "h" "v" "w" "y")                     4
    ("k")                                     5
    ("j" "x")                                 8
    ("q" "z")                                 10
    0))

(defn string-score
  "Returns total score for string `s`."
  [s]
  (reduce + (map char-score s)))


(comment

  (char-score \a)
  (string-score "abc")

)
