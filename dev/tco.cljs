(ns tco)

(defn foo [x]
  (if (zero? x)
    (println "done")
    (do
      (println x)
      (recur (dec x)))))

(foo 3)
