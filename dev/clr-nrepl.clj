#_{:clj-kondo/ignore [:namespace-name-mismatch]}
(ns clr-nrepl
  (:require [clojure.tools.nrepl :as nrepl]))

(comment

  (def server (nrepl/start-server!))
  (nrepl/stop-server! server)

)
