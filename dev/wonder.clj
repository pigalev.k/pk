(ns wonder)


;; Test for wondrous numbers from "Aria with Diverse Variations" in GEB

;; We begin with a number, and if it is odd, we triple it, and add 1. If it is
;; even, we take half of it. Then we repeat the process. Call a number which
;; eventually reaches 1 this way a wondrous number, and a number which doesn't,
;; an unwondrous number.

(defn wondrous?
  "Returns true if `n` is wondrous. But what if it is not?"
  [n]
  (cond
    (= 1 n)   true
    (even? n) (recur (/ n 2))
    :else     (recur (inc (* n 3)))))

(defn wondrous-steps
  "If `n` is wondrous, returns a vector of numbers obtained at each step
  in the proof."
  [n]
  (loop [n n steps []]
      (cond
        (= 1 n)   steps
        (even? n) (let [next-n (/ n 2)]
                    (recur next-n (conj steps next-n)))
        :else     (let [next-n (inc (* n 3))]
                    (recur next-n (conj steps next-n))))))


(comment

  (wondrous? 15)
  (wondrous-steps 15)
  (count (wondrous-steps 15))
  (apply max (wondrous-steps 15))

  (wondrous? 27)
  (wondrous-steps 27)
  (count (wondrous-steps 27))
  (apply max (wondrous-steps 27))

  (every? wondrous? (range 2 1000))

)
