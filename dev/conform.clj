(ns conform
  (:require
   [clojure.spec.alpha :as s]))


;; check that a string:
;; - begins and ends with a letter
;; - does not contain 3 digits successively

;; examples:
;; - a12b1с: ok
;; - a123b, 1аа2в: not ok


(defn letter? [c]
  (Character/isLetter c))

(defn digit? [c]
  (Character/isDigit c))


(s/def :char/letter letter?)
(s/def :char/digit digit?)
(s/def :char/not-digit (complement digit?))

(s/def :seq/not-digit (s/* :char/not-digit))
(s/def :seq/not-3-digits
  (s/& (s/* :char/digit) #(not= 3 (count %))))
(s/def :seq/starts-with-a-letter
  (s/and (s/conformer first)
         :char/letter))
(s/def :seq/not-3-successive-digits
  (s/cat :not-3-digits :seq/not-3-digits))

(s/def :string/starts-with-a-letter-and-not-3-successive-digits
  (s/and string?
         (s/conformer seq)
         (s/cat :seq/starts-with-a-letter (s/+ :seq/not-3-successive-digits))))

(comment

  (s/valid? :string/starts-with-a-letter "a12b1c")
  (s/valid? :string/starts-with-a-letter "a123b")
  (s/valid? :string/starts-with-a-letter "1аа2в")

  (s/explain :string/not-3-successive-digits "a123b")
  (s/conform :string/not-3-successive-digits "1аа2в")

  (s/conform :string/starts-with-a-letter-and-not-3-successive-digits
             "a12b1с")

)
