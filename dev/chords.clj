(ns chords)



(defmulti format-step
  "Formats a chord step to a string."
  (fn [[step-name]] step-name))

(defmethod format-step :tonic [[step-name tone]]
  [step-name (name tone)])

(defmethod format-step :third [[step-name step-kind]]
  [step-name (case step-kind
               :minor "m"
               nil "5"
               "")])

(defmethod format-step :fifth [[step-name step-kind]]
  [step-name (case step-kind
               :diminished "dim"
               nil "5-"
               "")])

(defmethod format-step :seventh [[step-name step-kind]]
  [step-name (case step-kind
               :minor "7"
               :major "maj7"
               "")])

(defmethod format-step :default [[step-name]]
  [step-name ""])

(defn format-chord
  "Formats `chord` to a string."
  [chord]
  (let [{:keys [tonic third fifth seventh]
         :as chord-formatted-steps} (into {} (map format-step chord))]
    (str tonic third fifth seventh)))


(comment

  (format-step [:tonic :D])
  (format-step [:seventh :major])

  (def Am7 {:tonic   :A
            :third   :minor
            :fifth   :perfect
            :seventh :minor})

  (def Ebmaj7 {:tonic   :Eb
               :third   :major
               :fifth   :perfect
               :seventh :major})

  (def Bdim {:tonic :B
             :third :minor
             :fifth :diminished})

  (format-chord Am7)
  (format-chord Ebmaj7)
  (format-chord Bdim)
  (format-chord {:tonic :B
                 :third nil
                 :fifth :perfect})

)
