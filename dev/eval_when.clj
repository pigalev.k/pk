(ns eval-when
  (:require
   [clojure.walk :refer [macroexpand-all]]
   [foo :refer [deffoo sum]]))

(deffoo (atom 0))

(swap! foo inc)
(println @foo)

;; using user-defined functions at macro expansion (compile) time

(defn id [x] x)
(defn- private-id [x] x)

;; calling at expansion time
(defmacro double-id [x] [(id x) (private-id x)])
;; expanding to calls
(defmacro double-sum [x y] `(* 2 (sum ~x ~y)))
;; defining vars at compile time
(defmacro double-def [val]
  (println "defining quux at compile time")
  (def quux val)
  (println "quux is" quux))
;; reading vars at compile time from another macro
(defmacro double-print []
  (println "quux is indeed" quux))

(double-id (+ 1 2 3))
(double-sum 7 8)
(double-def 1)
(double-print)

(macroexpand-all '(double-id (+ 1 2 3)))
;; => [(+ 1 2 3) (+ 1 2 3)]

(macroexpand-all '(double-sum 7 8))
;; => (clojure.core/* 2 (foo/sum 7 8))

(macroexpand-all '(double-def quux))
;; => nil
