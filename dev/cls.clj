(ns cls)

(defn A
  "Returns an instance of class A."
  [x y]
  (let [ff (fn []
             (printf "internal method, x: %s, y: %s\n" x y))
        f (fn []
            (println "external method invokes internal one:")
            (ff))]
    {:f f}))

(comment

  (def a (A 2 5))
  (def f (:f a))

  (f)

)
