(ns doublesort)

(defn doublesort
  "Sorts negative elements of `xs`, leaving nonnegative ones alone in
  their places. Optional `comparator` can be provided."
  ([xs]
   (doublesort xs nil))
  ([xs comparator]
   (let [sorted-negs (sort (or comparator compare) (filter neg? xs))]
     (loop [xs xs sorted-negs sorted-negs doublesorted-xs []]
       (cond
         (empty? xs)       doublesorted-xs
         (neg? (first xs)) (recur (rest xs)
                                  (rest sorted-negs)
                                  (conj doublesorted-xs (first sorted-negs)))
         :else             (recur (rest xs)
                                  sorted-negs
                                  (conj doublesorted-xs (first xs))))))))


(comment

  (def xs [1 10 20 -1 -4  5 -2  8])

  ;; ascending
  (doublesort xs)
  ;; => [1 10 20 -4 -2 5 -1 8]

  ;; descending
  (doublesort xs (fn [a b] (compare b a)))
  ;; => [1 10 20 -1 -2 5 -4 8]

)
