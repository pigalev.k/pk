(ns brace-expansion
  (:refer-clojure :exclude [read]))

(defn tokenize
  "Tokenizes `string` for brace expansion."
  [string]
  (re-seq #"[^{},]+|[{},]" string))

(defn read
  "Reads `tokens` and returns a vector of brace expansion results."
  [tokens]
  (loop [tokens tokens results []]
    (let [[token & more] tokens]
      (cond
        (nil? token) results
        (= "{" token) ))))

(defn expand
  "Expands braces as bash does."
  [string])

(comment

  (def simple "/path/to/{foo,bar}")
  (def foo "/path/to/{foo,ba{r,z}}.txt")

  (tokenize foo)

  (read (tokenize simple))

  (re-seq #"\{.*?\}" foo)


)
