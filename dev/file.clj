(ns file
  (:require [clojure.string :as s]
            [clojure.java.io :as io]
            :verbose)
  (:import (java.io File)))

;; a file filter implementation that keeps only .clj files
(let [ff (reify java.io.FilenameFilter
           (accept [this dir filename]
             (s/ends-with? filename ".clj")))
      dir (File. "dev")]
  (into [] (.listFiles dir ff)))

(let [dir  (io/file "dev")
      clj? #(s/ends-with? (.getName ^File %) ".clj")]
  (->> (file-seq dir)
       (filter clj?)
       count))
