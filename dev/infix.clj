#!/usr/bin/env bb

(ns infix
  (:require
   [infix.macros :as i]))


(i/infix 3 + 5 * 8)
(println (i/infix (3 + 5) * 8))
