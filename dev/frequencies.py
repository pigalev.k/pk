from functools import reduce

def inc(acc, val):
    if val in acc:
        acc[val] += 1
    else:
        acc[val] = 1
    return acc

def frequencies(xs):
    return reduce(inc, xs, {})

if __name__ == "__main__":
    numbers = [1, 1, 1, 2, 3, 3, 4]
    print(frequencies(numbers))
