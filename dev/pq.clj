(ns pq
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as string]))


;; Exploring the pq formal system from GEB


(def symbols #{\p \q \-})

(defn contains-allowed-symbols-only?
  "Returns true if string `s` contains only allowed symbols (contained
  in `symbols`)."
  [s]
  (every? symbols s))

(defn has-axiom-form?
  "Returns true if `s` is formally an axiom. The axiom decision
  procedure."
  [s]
  (let [[_ pre mid post] (re-find #"(-*)p(-+)q(-+)" s)]
    (and pre
         mid
         post
         (= (+ (count pre) (count mid))
            (count post)))))

(has-axiom-form? "-p--q---")
(has-axiom-form? "-p--q--")
(has-axiom-form? "--")
(has-axiom-form? "pq")


(def to-lower-case
  "Conforms a string to lower case."
  (spec/conformer string/lower-case))

(spec/def ::axiom
  (spec/and string?
            to-lower-case
            contains-allowed-symbols-only?
            has-axiom-form?))

(spec/valid? ::axiom "-p--q---")
(spec/conform ::axiom "-p--q---")

(spec/valid? ::axiom "-p--q--")
(spec/conform ::axiom "-p--q--")
(spec/explain-data ::axiom "-p--q--")


(defn append-hyphens-rule
  "Returns a string `s` with hyphens appended to middle and posterior
  parts of it. A production rule."
  [s]
  (let [[_ pre mid post] (re-find #"(-*)p(-+)q(-+)" s)]
    (and pre
         mid
         post
         (str pre "p" (str mid "-") "q" (str post "-")))))

(append-hyphens-rule "-p--q---")
(spec/valid? ::axiom (append-hyphens-rule "-p--q---"))
