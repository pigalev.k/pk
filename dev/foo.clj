(ns foo)

(defn sum [x y] (+ x y))

(defmacro deffoo [val] `(def ~'foo ~val))
