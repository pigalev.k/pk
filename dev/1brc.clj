(ns dev.1brc
  (:require [clojure.java.io :as io]
            [clojure.string :as s]
            [taoensso.tufte :as tufte :refer [defnp p profiled profile format-pstats]])
  (:import (java.io File BufferedReader FileInputStream)
           (java.nio.channels FileChannel)))

(tufte/add-basic-println-handler! {})

(set! *warn-on-reflection* true)


;; loosely following rules (and using data) of 1 Billion Records Challenge
;; - https://www.morling.dev/blog/one-billion-row-challenge/
;; - https://github.com/gunnarmorling/1brc?tab=readme-ov-file#results
;; - https://github.com/gunnarmorling/1brc/discussions/categories/show-and-tell


;; naïve implementation

(def scsv-split-regex
  "A regex to split the semicolon-separated line to fields."
  #";")

(defn read-file
  "Returns a lazy sequence of lines in the file."
  [filename]
  (line-seq (io/reader filename)))

(defn read-scsv-file
  "Returns a lazy sequence of measurements read from the file. A
  measurement is a vector of a station name and a floating-point
  measured value."
  [filename]
  (->> (read-file filename)
       (map #(s/split % scsv-split-regex))
       (map #(update % 1 parse-double))))

(defn moving-average
  "Returns cumulative moving average given previous average, the count
  of values used to compute it, and the new value. See
  https://en.wikipedia.org/wiki/Moving_average#Cumulative_moving_average"
  [average count value]
  (/ (+ value (* average count)) (inc count)))

(defn update-station-stats
  "Returns the map containing stats of some station updated with the
  given measured value."
  [{:keys [count minimum maximum average]} value]
  {:count   (inc count)
   :minimum (min minimum value)
   :maximum (max maximum value)
   :average (moving-average average count value)})

(defn update-total-stats
  "Returns the map of station stats updated with the result of updating
  the stats of the given station with the given measured value."
  ([] {})
  ([acc] acc)
  ([acc [key value]]
   (update acc
           key
           (fnil update-station-stats
                 {:count 0 :minimum value :maximum value :average value})
           value)))

(defn stats
  "Returns a map of station stats keyed by station names, obtained by
  reducing the sequence of measurements."
  [measurements]
  (reduce update-total-stats {} measurements))

(defn format-stats
  "Returns the map of station stats, sorted lexicographically by key."
  [stats]
  (into (sorted-map) stats))

(comment

  ;; the baseline

  ;; 1.2 sec
  (time (format-stats (stats (read-scsv-file "dev/resources/measurements1M.txt"))))

  ;; 1300 sec (~20 min)
  (time (format-stats (stats (read-scsv-file "dev/resources/measurements.txt"))))


  (profile {} (format-stats (stats (read-scsv-file "dev/resources/measurements1M.txt"))))

)


;; using transducers

(def line->measurement
  "Transforms a reducing function to coerce its input values from
  strings to measurements (vectors of a station name and a
  floating-point value)."
  (comp
   (map #(s/split % scsv-split-regex))
   (map #(update % 1 parse-double))))

(defn transducing-stats
  "Returns the map of station stats keyed by station names, obtained by
  transducing (reducing with transformation of the reducing function)
  the sequence of lines."
  [lines]
  (transduce line->measurement update-total-stats lines))

(comment

  (take 3 (sequence line->measurement (read-file "dev/resources/measurements1M.txt")))

  ;; better than the baseline by 15-20%

  ;; ~1 sec
  (time (format-stats (transducing-stats (read-file "dev/resources/measurements1M.txt"))))

  ;; ~1000 sec (~17 min)
  (time (format-stats (transducing-stats (read-file "dev/resources/measurements.txt"))))

  (profile {}
    (format-stats
     (transducing-stats (read-file "dev/resources/measurements1M.txt"))))


  ;; read entire file in memory

  ;; ~0.3 sec
  (time (def lines (doall (read-file "dev/resources/measurements1M.txt"))))
  (first lines)
  ;; ~1 sec
  (time (format-stats (transducing-stats lines)))

  ;; -J-Xmx55G and still cannot read entire file to memory; OOM kills java
  (time (def all-lines (doall (read-file "dev/resources/measurements.txt"))))
  (first all-lines)
  (time (format-stats (transducing-stats all-lines)))

)


;; using transients and arrays of primitives

;; profiler shows that updating stats maps (station and total) consumes most
;; of the time

;; - representing total stats as a transient map and updating it in-place
;; - representing station stats as an array of doubles and updating it in-place
;; - using type hints to avoid slow reflection calls

(defn update-station-stats!
  "Returns an array of doubles containing stats of some station updated
  with the given measured value."
  [^doubles [count minimum maximum average :as station-stats] ^double value]
  (doto ^doubles station-stats
    (aset 0 ^double (unchecked-inc count))
    (aset 1 ^double (min minimum value))
    (aset 2 ^double (max maximum value))
    (aset 3 ^double (moving-average average count value))))

(defn update-total-stats!
  "Returns the (transient) map of station stats updated with the result
  of updating the stats of the given station with the given measured
  value."
  ([] (transient {}))
  ([acc] acc)
  ([acc [key value]]
   (assoc! acc
           key
           ((fnil update-station-stats!
                  (double-array [0.0 value value value]))
            (acc key)
            value))))

(defn transducing-stats!
  "Returns the map of station stats keyed by station names, obtained by
  transducing (reducing with transformation of the reducing function)
  the sequence of lines."
  [lines]
  (persistent! (transduce line->measurement update-total-stats! lines)))

(defn format-stats-1
  "Returns the map of station stats (converted to maps from arrays of doubles),
  sorted lexicographically by key."
  [stats]
  (into (sorted-map) (map (fn [[key [count minimum maximum average]]]
                            [key {:count   count
                                  :minimum minimum
                                  :maximum maximum
                                  :average average}]) stats)))

(comment

  ;; the same as the baseline (or slightly worse), worse than simple
  ;; transducing variant by ~30%

  ;; ~1.3 sec
  (time (format-stats-1
         (transducing-stats! (read-file "dev/resources/measurements1M.txt"))))

  (profile {}
    (format-stats-1
     (transducing-stats! (read-file "dev/resources/measurements1M.txt"))))

)


;; using mutable Java hashmaps

(def default-hashmap-load-factor
  "Load factor value used by default to create Java hashmaps."
  0.75)

(def station-stats-map-capacity
  "Initial capacity of a hashmap that holds station stats (always 4 keys).
  Should be high enough to avoid rehashing."
  (int (Math/ceil (/ 4 default-hashmap-load-factor))))

(def total-stats-map-capacity
  "Initial capacity of a hashmap that holds total stats (number of keys
  is the number of stations --- 413). Should be high enough to avoid
  rehashing."
  (int (Math/ceil (/ 413 default-hashmap-load-factor))))

(defn update-station-stats-mutable!
  "Returns the mutable Java hashmap containing stats of some station
  updated with the given measured value."
  [{:keys [count minimum maximum average] :as station-stats} value]
  (doto ^java.util.HashMap station-stats
    (.put :count (inc count))
    (.put :minimum (min minimum value))
    (.put :maximum (max maximum value))
    (.put :average (moving-average average count value))))

(defn update-total-stats-mutable!
  "Returns the mutable Java hashmap containing total stats updated with
  the result of updating the stats of the given station with the given
  measured value."
  ([] (java.util.HashMap. ^int total-stats-map-capacity))
  ([acc] acc)
  ([acc [key value]]
   (doto ^java.util.HashMap acc
     (.put key
           ((fnil update-station-stats-mutable!
                  (doto (java.util.HashMap. ^int station-stats-map-capacity)
                    (.put :count 0)
                    (.put :minimum value)
                    (.put :maximum value)
                    (.put :average value)))
            (.get ^java.util.HashMap acc key)
            value)))))

(defn transducing-stats-mutable!
  "Returns the map of station stats keyed by station names, obtained by
  transducing (reducing with transformation of the reducing function)
  the sequence of lines."
  [lines]
  (transduce line->measurement update-total-stats-mutable! lines))

(comment

  ;; the same as the simple transducing variant with Clojure's immutable maps

  ;; ~1 sec
  (time (format-stats
         (transducing-stats-mutable! (read-file "dev/resources/measurements1M.txt"))))

  ;; roughly the same
  (time (format-stats
         (transducing-stats-mutable! (take (int 1e6)
                                           (read-file "dev/resources/measurements.txt")))))

  (profile {}
    (format-stats-1
     (transducing-stats-mutable! (read-file "dev/resources/measurements1M.txt"))))

)


;; using manual looping

(defn looping-stats
  "Returns the map of station stats keyed by station names.
  Reads a semicolon-separated file with measurement records eagerly
  line by line, parses each line into a measurement (station name and
  measured value) and updates the corresponding station's stats using
  the value."
  [filename]
  (let [rdr (io/reader filename)]
    (loop [stats {}]
      (if-let [line (.readLine ^java.io.BufferedReader rdr)]
        (let [[key string-value]    (s/split line scsv-split-regex)
              value                 (parse-double string-value)
              updated-station-stats ((fnil update-station-stats {:count   0
                                                                 :minimum value
                                                                 :maximum value
                                                                 :average value})
                                     (stats key)
                                     value)]
          (recur (assoc stats key updated-station-stats)))
        stats))))

(comment

  ;; ~35% better than baseline (and ~12% better than the transducing variant)

  ;; ~0.85 sec
  (time (format-stats (looping-stats "dev/resources/measurements1M.txt")))

  ;; ~850 sec (~14 min)
  (time (format-stats (looping-stats "dev/resources/measurements.txt")))

)


;; using manual looping and mutable hashmaps

(defn looping-stats-mutable!
  "Returns the map of station stats keyed by station names.
  Reads a semicolon-separated file with measurement records eagerly
  line by line, parses each line into a measurement (station name and
  measured value) and updates the corresponding station's stats using
  the value. Uses mutable hashmaps to store total and station stats."
  [filename]
  (let [rdr (io/reader filename)]
    (loop [stats (java.util.HashMap.)]
      (if-let [line (.readLine ^java.io.BufferedReader rdr)]
        (let [[key string-value] (s/split line scsv-split-regex)
              value              (parse-double string-value)
              {:keys [count minimum maximum average] :as key-stats}
              (or (.get ^java.util.HashMap stats key)
                  (doto (java.util.HashMap.)
                    (.put :count 0)
                    (.put :maximum value)
                    (.put :minimum value)
                    (.put :average value)))
              updated-key-stats
              (doto ^java.util.HashMap key-stats
                (.put :count (inc count))
                (.put :minimum (min minimum value))
                (.put :maximum (max maximum value))
                (.put :average (moving-average average count value)))]
          (recur (doto stats (.put key updated-key-stats))))
        stats))))

(comment

  ;; ~50% better than baseline (and ~30% better than the transducing variant)

  ;; ~0.7 sec
  (time (format-stats (looping-stats-mutable! "dev/resources/measurements1M.txt")))

  ;; ~750 sec (~12 min)
  (time (format-stats (looping-stats-mutable! "dev/resources/measurements.txt")))

)


;; using multiple threads to read the file (divided to logical segments) and
;; computing stats for each segment, then combining the results

(def ^:dynamic *end-of-file*
  "Integer value returned by a file input stream when end of file is reached."  -1)

(def ^:dynamic *end-of-line*
  "Integer value that corresponds to the end of line character." 10)

(defn file-segment
  "Returns a logical segment of `file`, defined by start and end
  positions in bytes. The segment will begin at `start` and end after
  newline nearest to `start` + `initial-length` position (inclusive),
  i.e., segment always ends between lines (or at the end of file);
  `start` defaults to 0; `initial-length` defaults to the length of
  the entire file."
  ([^File file]
   (file-segment file 0))
  ([^File file start]
   (file-segment file start (.length file)))
  ([^File file start initial-length]
   (when (pos? initial-length)
     (with-open [file-input-stream (FileInputStream. file)]
       (let [file-channel         (.getChannel file-input-stream)
             end-of-line-or-file? #{*end-of-file* *end-of-line*}
             at-initial-end       (+ start initial-length)
             before-initial-end   (dec at-initial-end)]
         {:file  file
          :start start
          :end   (do
                   (.position file-channel ^int before-initial-end)
                   (let [initial-end-byte (.read file-input-stream)]
                     (if (end-of-line-or-file? initial-end-byte)
                       (min (.position file-channel) (.length file))
                       (do
                         (.position file-channel ^int at-initial-end)
                         (while (not (end-of-line-or-file? (.read file-input-stream))))
                         (min (.position file-channel) (.length file))))))})))))

(defn file-segment-seq
  "Returns a lazy sequence of at most `n` successive logical segments of
  `file`. (As segments may be expanded to align with line boundaries,
  there may be fewer of them, especially if segments are small and
  lines are long.)"
  ([^java.io.File file n]
   (file-segment-seq file 0 n))
  ([^java.io.File file start n]
   (when (> n 0)
     (let [remaining-file-length  (- (.length file) start)
           initial-segment-length (int (/ remaining-file-length n))]
       (when-let [segment (file-segment file start initial-segment-length)]
         (cons segment (lazy-seq (file-segment-seq file (:end segment) (dec n)))))))))

(defn file-segment-line-seq
  "Returns a lazy sequence of lines from the `segment`. Reading begins
  at `start` and ends after `end`. (The line that contains the end
  position will be the last read. Segment constructor guarantees that
  segment will always end at a line boundary, but manually created
  segments may not.)"
  ([{:keys [file start end]}]
   (let [file-input-stream (FileInputStream. ^File file)
         _                 (.position (.getChannel file-input-stream) ^int start)
         reader            (io/reader file-input-stream)]
     (file-segment-line-seq reader 0 (- end start))))
  ([^BufferedReader reader bytes-read bytes-max]
   (when (not (>= bytes-read bytes-max))
     (when-let [line (.readLine reader)]
       (let [line-length-bytes (inc (count (.getBytes line)))]
         (cons line (lazy-seq (file-segment-line-seq reader
                                                     (+ bytes-read line-length-bytes)
                                                     bytes-max))))))))

(defn merge-station-stats
  "Returns two station stats maps merged."
  [{cnt1 :count min1 :minimum max1 :maximum avg1 :average}
   {cnt2 :count min2 :minimum max2 :maximum avg2 :average}]
  {:count   (+ cnt1 cnt2)
   :minimum (min min1 min2)
   :maximum (max max1 max2)
   :average (/ (+ (* cnt1 avg1) (* cnt2 avg2)) (+ cnt1 cnt2))})

(defn parallel-stats
  "Returns the map of station stats keyed by station names. Reads the
  measurements data from `filename`, that is a text file containing
  semicolon-separated values. Uses multiple threads to read the
  file (divided to `n` logical segments) and computes stats for each
  segment, then combinesthe results. Default number of segments is the
  number of available cores + 2, same as the number of threads used by
  `pmap`."
  ([filename]
   (parallel-stats filename (+ 2 (.. Runtime getRuntime availableProcessors))))
  ([filename n]
   (let [file (io/file filename)]
     (reduce #(merge-with merge-station-stats %1 %2)
             (pmap (fn [segment]
                     (transducing-stats (file-segment-line-seq segment)))
                   (file-segment-seq file n))))))

(comment


  ;; 80% better than baseline (4.5 min vs 22 min)

  ;; 0.3 sec
  (time (format-stats (parallel-stats "dev/resources/measurements1M.txt")))

  ;; 263 sec (~4.5 min)
  (time (format-stats (parallel-stats "dev/resources/measurements.txt")))

)

(comment

  (def f1d (io/file "dev/resources/measurements1D.txt"))
  (def f1m (io/file "dev/resources/measurements1M.txt"))
  (def f1b (io/file "dev/resources/measurements.txt"))

  (def l1m (.length f1m))
  (def l1b (.length f1b))

  (def cores 8.0)
  (def initial-segment-length-1m (int (/ l1m cores)))

  (def fis1m (java.io.FileInputStream. f1m))
  (def fch1m (.getChannel fis1m))
  (.position fch1m)
  (.position fch1m 12)
  (.position fch1m 0)

  (= (.read fis1m) 10)

  (def rdr1m (io/reader fis1m))
  (.readLine rdr1m)



  (def text-file (io/file "dev/1brc.test.txt"))

  (file-segment text-file)
  (file-segment text-file 0 3)
  (file-segment text-file 0 4)
  (file-segment text-file 0 5)

  (file-segment-line-seq (file-segment text-file 0 3))
  (file-segment-line-seq (file-segment text-file 0 4))
  (file-segment-line-seq (file-segment text-file 0 5))

  (file-segment-seq text-file 2)

  (map file-segment-line-seq (file-segment-seq text-file 1))
  (map file-segment-line-seq (file-segment-seq text-file 2))
  (map file-segment-line-seq (file-segment-seq text-file 3))
  (map file-segment-line-seq (file-segment-seq text-file 4))
  (map file-segment-line-seq (file-segment-seq text-file 5))
  (map file-segment-line-seq (file-segment-seq text-file 6))
  (map file-segment-line-seq (file-segment-seq text-file 7))
  (map file-segment-line-seq (file-segment-seq text-file 8))


  (def f0 (io/file "dev/resources/measurements1D.txt"))
  (file-segment f0 0 10)
  (file-segment-line-seq (file-segment f0))

  (sequence line->measurement
            (file-segment-line-seq (file-segment f0)))

  (def f1 (io/file "dev/resources/measurements1M.txt"))
  (file-segment f1 0 10)
  (file-segment-seq f1 8)

  (count (file-segment-line-seq (file-segment f1)))

  (count (sequence line->measurement
                   (file-segment-line-seq (file-segment f1))))

  (time (count (file-segment-line-seq (file-segment f1 0 13))))
  (time (count (file-segment-line-seq (file-segment f1 0 130))))
  (time (count (file-segment-line-seq (file-segment f1 0))))

)


;; using core aggregation functions

(defn aggregate
  [[station measurements]]
  (let [values (map second measurements)
        n      (count values)]
    [station {:count   (count values)
              :min     (reduce min values)
              :max     (reduce max values)
              :average (/ (reduce + values) n)}]))

(comment

  (time (->> (read-file "dev/resources/measurements1M.txt")
             (sequence line->measurement)
             (group-by first)
             (map aggregate)
             (into (sorted-map))))

  (time (->> (file-segment-line-seq
              (file-segment
               (io/file "dev/resources/measurements.txt") 0 1e7))
             (sequence line->measurement)
             (group-by first)
             (map aggregate)
             (into (sorted-map))))

)
