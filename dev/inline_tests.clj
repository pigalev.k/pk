(ns inline-tests
  (:require [clojure.test :as t]))

(defn foo
  "Returns sum of `a` and `b`."
  {:test (fn []
           (t/is (= 12 (foo 6 6)))
           (t/is (= 3 (foo 1 2))))}
  [a b]
  (+ a b))

(t/run-tests)
