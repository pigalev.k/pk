(ns miu
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as string]))


;; Exploring the MIU formal system from GEB


;; Starting from axioms, produce (derive, infer) theorems by applying rules to
;; axioms and already produced theorems --- the process of
;; production (derivation, inference).

;; If there is a test to distinguish theorems from non-theorems, it is called a
;; decision procedure for the given formal system. Rules of inference too can be
;; used to distinguish theorems, but only implicitly, by enumeration, that is
;; not guaranteed to terminate (by extension), while decision procedure supposed
;; to do so explicitly (by intension) and to be guaranteed to terminate.

;; It is a requirement for a formal system that it's set of axioms always has
;; decision procedure, but set of theorems may not.


;; TODO: use the common terminology when naming data and functions.


;; symbols

(def symbols #{\m \i \u})

(spec/def ::theorem
  (spec/and (spec/conformer string/lower-case)
            #(every? symbols %)))

(assert (spec/valid? ::theorem "MIU"))
(assert (not (spec/valid? ::theorem "MIUH")))


;; rules

;; predicates

(defn ends-with-i?
  "Returns true if `s` ends with letter 'I'."
  [s]
  (= (last (string/lower-case s)) \i))

(assert (ends-with-i? "mi"))
(assert (not (ends-with-i? "mu")))


(defn starts-with-m?
  "Returns true if `s` starts with letter 'M'."
  [s]
  (= (first (string/lower-case s)) \m))

(assert (starts-with-m? "mu"))
(assert (not (starts-with-m? "hello")))


(defn contains-triple-i?
  "Returns true if `s` contains three consecutive letters 'I'."
  [s]
  (re-find #"i{3}" (string/lower-case s)))

(assert (contains-triple-i? "aiiif"))
(assert (not (contains-triple-i? "hiio")))


(defn contains-double-u?
  "Returns true if `s` contains two consecutive letters 'U'."
  [s]
  (re-find #"u{2}" (string/lower-case s)))

(assert (contains-double-u? "quux"))
(assert (not (contains-double-u? "sure")))

;; effects

(defn append-u
  "Returns `s` with letter 'U' appended to the end."
  [s]
  (str s "U"))

(assert (= "REU" (append-u "RE")))


(defn duplicate-rest-after-m
  "Returns `s` with the rest of letters after first letter 'M' duplicated."
  [s]
  (string/upper-case
   (string/replace-first (string/lower-case s) #"m(.*)" "m$1$1")))

(assert (= "MIUUIUU" (duplicate-rest-after-m "miuu")))
(assert (= "MMM" (duplicate-rest-after-m "mm")))


(defn replace-triple-i-with-u
  "Returns `s` with the first occurrence of three consecutive letters
  'I' replaced with letter 'U'."
  [s]
  (string/upper-case
   (string/replace-first (string/lower-case s) #"i{3}" "u")))

(assert (= "MU" (replace-triple-i-with-u "miii")))
(assert (= "MIUU" (replace-triple-i-with-u "miuiii")))


(defn drop-double-u
  "Returns `s` with the first occurrence of two consecutive letters 'U'
  dropped."
  [s]
  (string/upper-case
   (string/replace-first (string/lower-case s) #"u{2}" "")))

(assert (= "M" (drop-double-u "muu")))
(assert (= "MUIM" (drop-double-u "muimuu")))


;; the engine

(def effects
  {::append-u                append-u
   ::duplicate-rest-after-m  duplicate-rest-after-m
   ::replace-triple-i-with-u replace-triple-i-with-u
   ::drop-double-u           drop-double-u})

(defn applicable-effects
  "Returns all effects applicable to string `s`."
  [s]
  (cond-> []
    (ends-with-i? s)       (conj ::append-u)
    (starts-with-m? s)     (conj ::duplicate-rest-after-m)
    (contains-triple-i? s) (conj ::replace-triple-i-with-u)
    (contains-double-u? s) (conj ::drop-double-u)))

(applicable-effects "mi")
(applicable-effects "m")
(applicable-effects "miii")
(applicable-effects "muum")


(defn apply-effects
  "Returns string `s` with all effects in `fx` applied to it in turn."
  [s fx]
  (cond
    (empty? fx) s
    :else       (recur ((effects (first fx)) s) (rest fx))))


(apply-effects "mi" (applicable-effects "mi"))
(apply-effects "mi" [])
(apply-effects "mi" [::duplicate-rest-after-m
                     ::duplicate-rest-after-m
                     ::duplicate-rest-after-m
                     #_::replace-triple-i-with-u])


(defn run
  "Do `n` steps of inference, applying all available rules to `s` at each step."
  [n s]
  (loop [i n result [s]]
    (cond
      (zero? i) result
      :else     (recur (dec i)
                       (conj result
                             (apply-effects (last result)
                                            (applicable-effects
                                             (last result))))))))

(run 4 "mi")
