#!/usr/bin/env bb

(require '[babashka.http-client :as http]
         '[babashka.pods :as pods]
         '[clojure.java.io :as io]
         '[clojure.string :as s])

(pods/load-pod 'retrogradeorbit/bootleg "0.1.9")

(require '[pod.retrogradeorbit.bootleg.utils :refer [convert-to]]
         '[pod.retrogradeorbit.hickory.select :as sel])



(def response
  (http/get "https://www.linux.org.ru/notifications?filter=reaction"
            {:headers {"Cookie" "<redacted>"}}))


(def html-string (-> response
                     :body
                     s/trim))
(def hickory-data (-> html-string
                      (convert-to :hickory)))
(def hiccup-data (-> html-string
                     (convert-to :hiccup)))

(def reactions (->> hickory-data
                    (sel/select (sel/class "reaction"))
                    (map (comp s/trim
                               second
                               #(s/split % #"\s+")
                               first :content))
                    frequencies))

(comment

  (spit "dev/notifications.edn" response)
  (spit "dev/notifications.html" html-string)

)
