(def animals '(("Sam" :bear)  ("Billy" :rabbit) ("Bobby" :lion)))

(def second-animal-species (comp rest first rest))

(comment

  (println "Species of the second animal is" (second-animal-species animals))

)
