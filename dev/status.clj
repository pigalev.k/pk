(ns status
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as s]
   [clojure.java.io :as io])
  (:import (java.io IOException FileInputStream)))


(spec/def ::pid (spec/or :num (spec/and number? pos?)
                         :str #{"self"}))

(spec/def ::ks (spec/nilable (spec/coll-of string? :kind vector?)))
(spec/def ::transforms (spec/map-of string? fn?))


(defn str->num-with-unit
  "Converts a string value with a number and unit of measure to a pair
  of integer and unit string."
  [attr-string]
  (-> (s/split attr-string #"\s")
      (update 0 parse-long)))

(defn num-with-unit->num-of-bytes
  "Converts a number-unit pair to a number of bytes."
  [num-unit-pair]
  (let [[n unit] num-unit-pair]
    (case (s/lower-case unit)
      "kb" (* n 1024)
      "mb" (* n 1024 1024)
      "gb" (* n 1024 1024 1024)
      n)))

(def transforms
  "Associates status attribute names with transforming functions for
  their values."
  {"Name" s/upper-case
   "VmSwap" str->num-with-unit
   "VmSize" (comp num-with-unit->num-of-bytes str->num-with-unit)})


(defn kib->mib
  "Converts kibibytes to mebibytes (powers of 2).
  Rounds the result to the nearest integer."
  [x] (Math/round (/ x 1024)))

(defn read-status
  "Reads a process status file from /proc.
  The `pid` can be a numeric process ID or string 'self'. Returns a
  seq of lines. Not lazy. If an IO exception occurs, returns nil."
  [pid]
  (try
    (with-open [reader (-> (format "/proc/%s/status" pid)
                           FileInputStream.
                           (io/reader))]
      (doall (line-seq reader)))
    (catch IOException e
      (println (ex-message e)))))

(defn line->pair
  "Transforms the `line` to a pair of strings.
  Extracts a field name and its value, trims both, and places them
  into a vector."
  [line]
  (-> line (s/split #":\t" 2) (->> (mapv s/trim))))

(defn transform-vals
  "Transforms values of the `m` according to `transforms` map."
  [m transforms]
  (let [applicable-transforms (select-keys transforms (keys m))]
    (merge-with (fn [acc val]
                  (println acc val)
                  (if acc (val acc) acc)) m applicable-transforms)))

(defn status-map
  "Transforms process status from seq of lines (strings) to a map.
  Associates process status attribute names to their values (all
  strings). If `ks` is not nil, status map will contain only keys
  present in `ks`, else all keys. If `transforms` is not nil, status
  map values will be transformed according to it."
  [status-lines & [ks transforms]]
  (let [status-map (into {} (map line->pair status-lines))]
    (cond-> status-map
      ks         (select-keys ks)
      transforms (transform-vals transforms))))

(defn status
  "Returns a process status map."
  [pid & [ks transforms]]
  (assert (spec/valid? ::pid pid))
  (assert (spec/valid? ::ks ks))
  (assert (spec/valid? ::transforms transforms))
  (some-> (read-status pid)
          (status-map ks transforms)))

(comment

  (def ks ["Name" "State" "PPid" "VmSize" "VmRSS" "VmSwap"])

  (status-map "1" ks)


  (status 1)
  (status 1 ks transforms)

  (status "self" ["VmSwap" "foo"] transforms)

  (let [line "VmSwap:\t40987 kB" kib->mib (fn [x] (Math/round (/ x 1024.0)))]
    (-> line (s/split #"\s") second parse-long kib->mib))

)
