# url-shortener-api-spa webapp using datahike over jdbc as db (replicated):
# + postgres
# + traefik reverse proxy
# + whoami service (replicated)

# note: Datahike currently do not have a dedicated transactor like Datomic does
# (each replica writes to postgres without coordinating writes in any way with
# other replicas), so there will be data consistency issues in replicated setup;
# use for testing purposes only.

# note: currently traefik does not work with podman out of the box; it requires
# starting the podman socket unit from the user and providing his socket to
# traefik in a volume (see below):
# $ systemctl --user enable --now podman.socket

volumes:
  db-data:

networks:
  pk:

secrets:
  # will be mounted as `/run/secrets/db-password` file
  # in containers that use the secret
  db-password:
    file: ../.db-password

services:
  whoami:
    image: docker.io/traefik/whoami
    deploy:
      replicas: 2
    restart: always
    labels:
      # explicitly tell Traefik to expose this container
      - "traefik.enable=true"
      # the domain/path/... the service will respond to
      # - "traefik.http.routers.whoami.rule=Host(`whoami.example.com`)"
      - "traefik.http.routers.whoami.rule=Path(`/whoami`)"
      # allow request only from the predefined entry point named "web"
      - "traefik.http.routers.whoami.entrypoints=web"
      # apply request rate limiting (rps)
      - "traefik.http.middlewares.test-ratelimit.ratelimit.average=1"
    networks:
      - pk

  url-shortener-api-spa:
    image: ${IMAGE}
    restart: on-failure:5
    deploy:
      replicas: 3
    labels:
      # explicitly tell Traefik to expose this container
      - "traefik.enable=true"
      # the domain/path/... the service will respond to
      - "traefik.http.routers.url-shortener-api-spa.rule=PathPrefix(`/url-shortener`)"
      - "traefik.http.routers.url-shortener-api-spa.middlewares=api-spa-stripprefix"
      - "traefik.http.middlewares.api-spa-stripprefix.stripprefix.prefixes=/url-shortener"
      # other routing methods are available, e.g. host-based
      # - "traefik.http.routers.url-shortener-api-spa.rule=Host(`api.example.com`)"
      # allow request only from the predefined entry point named "web"
      - "traefik.http.routers.url-shortener-api-spa.entrypoints=web"
    build:
      context: ../../../
      dockerfile: ./projects/url-shortener-api-spa/Containerfile
      args:
        VERSION: ${VERSION}
        UBERJAR: ${UBERJAR}
    depends_on:
      - postgres
    secrets:
      - db-password
    expose:
      - 8888
    environment:
      PROFILE: ${PROFILE}
      CONFIG: ${CONFIG}
    networks:
      - pk

  postgres:
    image: docker.io/postgres:15.1
    restart: always
    secrets:
      - db-password
    expose:
      - 5432
    ports:
      - 5432:5432
    volumes:
      - db-data:/var/lib/postgresql/data
      - ./postgres/url-shortener-api-spa.init.sh:/docker-entrypoint-initdb.d/url-shortener-api-spa.init.sh
    environment:
      PROFILE:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD_FILE: /run/secrets/db-password
    networks:
      - pk

  ingress:
    image: docker.io/traefik:v2.9
    # enables the web UI and tells Traefik to listen to docker (podman)
    command:
      # - "--log.level=DEBUG"
      # enable docker configuration provider
      - "--providers.docker"
      # do not expose containers unless explicitly told so
      - "--providers.docker.exposedbydefault=false"
      # traefik will listen on port 8080 by default for API requests
      - "--api.insecure=true"
      # traefik will listen to incoming requests on the port 8000 (HTTP)
      - "--entrypoints.web.address=:8000"
    ports:
      # the HTTP port
      - "8000:8000"
      # the Web UI (enabled by --api.insecure=true)
      - "8080:8080"
    volumes:
      # so that Traefik can listen to the podman events
      # note: check that the (host) user ID is correct
      - /run/user/1000/podman/podman.sock:/var/run/docker.sock:ro
    networks:
      - pk
