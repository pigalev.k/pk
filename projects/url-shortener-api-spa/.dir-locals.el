;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . (;; configure default Cider REPL
         (cider-clojure-cli-aliases . ":test")
         (cider-clojure-cli-parameters . "")
         ;; configure an org-roam directory for the workspace task management
         ;; (use org-roam commands when visiting a file from the workspace)
         (eval . (setq-local
                  org-roam-directory
                  (expand-file-name "docs/tasks"
                                    (locate-dominating-file
                                     "." ".dir-locals.el"))))
         (eval . (setq-local
                  org-roam-db-location
                  (expand-file-name "org-roam.db"
                                    org-roam-directory))))))
