# url-shortener-api-spa

A ClojureScript/Reagent SPA for URL shortening along with a Clojure/Pedestal
REST API service.

## Getting Started

1. Install `node`
2. Install npm dependencies

```
# or npm i
yarn
```

## Develop

### SPA and API

The entry point for the SPA is in the `short-url-spa` base, the entry point for
the API service is in the `short-url-api` base; see their documentation on
development workflows.

### Markup and styles

SPA markup and styles are available to edit in the project's `resources/public`
directory.

### Build script (`build.clj`)

Add the `:build` alias to REPL options (e.g., `C-u M-x cider-jack-in` in Emacs,
default keys `C-u C-c M-j`).

```
clj -M:build:nrepl
```

## Build & run

See available tasks

```
clojure -T:build
```

Build the app image and start it with its dependencies in containers

```
clojure -T:build run-compose
```

then visit http://localhost:8000/url-shortener/

## Test

Unit tests

```
clojure -M:test
```

Manual integration tests

See `docs/api.org`.

## Data

For example, when PostgreSQL or Datahike over PostgreSQL is used for
persistence: while PostgreSQL container is running (with no clients)

### Backup

```
podman exec -i <postgres container name> /bin/bash -c "PGPASSWORD=<password> \\
    pg_dump --username postgres <database name>" \\
    > ~/<database name>--$(date +%Y-%m-%d--%H:%M:%S).sql
```

### Restore

```
podman exec -i <postgres container name> /bin/bash -c "PGPASSWORD=<password> \\
    psql --username postgres <database name>" < ~/dump.sql
```

## ClojureScript build report

```
npx shadow-cljs run shadow.cljs.build-report app report.html
```
