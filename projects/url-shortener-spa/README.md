# url-shortener-spa

A Clojurescript/Reagent frontend project for the `url-shortener-api`
service. Uses `short-url-spa` as its base.

## Getting Started

1. Install `node`
2. Install npm dependencies

```
# or npm i
yarn
```

## Develop

The entry point for this SPA is in the `short-url-spa` base, please see its
documentation on development workflow.

### Markup and styles

Application markup and styles are available to edit in the project's `public`
directory.

### Build script (`build.clj`)

Add the `:build` alias to REPL options (e.g., `C-u M-x cider-jack-in` in Emacs,
default keys `C-u C-c M-j`).

```
clj -M:build:nrepl
```

## Test

### Unit

TODO

### End-to-end

TODO

### Manual

Serve a build

```
clojure -M:serve :port 4444 :dir "target"
```

## Release

ClojureScript source paths and dependencies are managed through `deps.edn`, npm
dependencies are specified in `package.json`, build configuration is in the
`shadow-cljs.edn`. See these files to learn more.

Build once

```
clojure -M:shadow-cljs compile app
```

Build for production

```
clojure -M:shadow-cljs release app
```

Clean all the build results

```
clojure -T:build clean
```

Build a Nginx container image to serve the build

```
clojure -T:build image
```

Use `help` command to see all available tasks.

## Run

Run the service in a standalone container (`C-c` to stop)

```
clojure -T:build run-container [:port 4444]
```

Use `help` command to see all available tasks.
