(ns build
  "Project tasks --- building, running, etc."
  (:require
   [build.api :as a]
   [clojure.java.io :as io]
   [clojure.string :as s]
   [clojure.tools.build.api :as b]))


(def lib 'pk/url-shortener-spa)
(def version (format "0.1.%s" (b/git-count-revs nil)))
(def build-dir "target/")

(def image-name (format "%s:%s" lib version))
(def image-build-file "Containerfile")
(def image-build-context-path "../../")
(def image-build-command "podman")

(def container-runtime-command "podman")
(def container-compose-command "podman-compose")
(def container-compose-project "pk")

;; TODO: review build config, add build tasks
