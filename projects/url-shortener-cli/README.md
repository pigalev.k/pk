# url-shortener-cli

Command-line utility for URL shortening.

A babashka uberscript that stores alias data in a local SQLite database.

## Getting Started

### Build

View build tasks for the project

```
bb tasks
```

Invoke tasks

```
# e.g., 'bb run clean'; or just 'bb clean'
bb run <task>
```

### Use

```
target/suctl help
target/suctl shorten --db my.db https://google.com
target/suctl unshorten abcdef --db my.db
target/suctl all
```
