;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; configure default Cider REPL
((nil . ((cider-clojure-cli-aliases . ":dev:test")
         (cider-clojure-cli-parameters . ""))))
