(ns build
  "Project tasks --- building, running, etc."
  (:require
   [build.api :as a]
   [clojure.string :as s]
   [clojure.tools.build.api :as b]))


(def project-name "PROJECT-NAME")
(def lib (symbol (format "pk-templates/%s-api" project-name)))
(def version (format "0.1.%s" (b/git-count-revs nil)))

(def entrypoint 'pk.bases.BASE-NAME-api.core)
(def src-dirs ["src" "resources"])
(def build-dir "target/")
(def class-dir (str build-dir "classes/"))
(def basis (b/create-basis {:project "deps.edn"}))
(def config-file (format "%s/config.edn" project-name))

(def jar-file (format "%s-%s.jar" (name lib) version))
(def uberjar-file (format "%s-%s-standalone.jar" (name lib) version))

(def container-runtime-command "podman")
(def image-name (format "%s:%s" (s/lower-case (name lib)) version))
(def image-build-file "Containerfile")
(def image-build-context-path "../../")

(def compose-command "podman-compose")
(def compose-project "pk-templates")


(a/set-config! {:project-name              project-name
                :lib                       lib
                :entrypoint                entrypoint
                :version                   version
                :src-dirs                  src-dirs
                :build-dir                 build-dir
                :class-dir                 class-dir
                :basis                     basis
                :config-file               config-file
                :jar-file                  jar-file
                :uberjar-file              uberjar-file
                :container-runtime-command container-runtime-command
                :image-name                image-name
                :image-build-file          image-build-file
                :image-build-context-path  image-build-context-path
                :compose-command           compose-command
                :compose-project           compose-project})


;; misc

(def help
  "List all available tasks with summaries of their docstrings."
  a/help)

(def clean
  "Clean all build results."
  a/clean)


;; building

(def uberjar
  "Build the project and pack the results in an uberjar file."
  a/service-uberjar)

(def image
  "Build a container image with the project's uberjar."
  a/image)


;; running

(def run
  "Run the service uberjar (`C-c` to stop).
  If an uberjar for the current version does not exist, build it
  first.

  Other parameters:
  - `:profile`: interpreted by the service at runtime according to its
    configuration (default is `:test`)
  - `:port`: determines the host machine port that will be used by
    service (default is `8888`)"
  a/run-service-uberjar)

(def run-container
  "Run the service in a standalone container (`C-c` to stop).
  If an image for the current version does not exist, build it first.

  Other parameters:
  - `:profile`: interpreted by the service at runtime according to its
    configuration (default is `:test`)
  - `:port`: determines the host machine port that will be forwarded to the
    service (default is `8888`)"
  a/run-container)

(def run-compose
  "Run the service and its runtime dependencies in containers.

  Uses `podman-compose` by default. If some images do not exist, they will be
  downloaded or built.

  Parameters:
  - `:file`: configuration of all container workloads (default is
    `deploy/compose.yml`)
  - `:rebuild`: rebuild all the project's locally-built images, even if
    they already exist (default `false`)
  - `:recreate`: remove and recreate all project's containers if the  parameter
    is true,  (default `true`)
  - `:profile`: interpreted by the service at runtime according to its
    configuration (default is `:test`)
  - `:port`: determines the host machine port that will be forwarded to the
    service (if configured in `:file`, default is `8888`)"
  a/run-compose)

(def stop-compose
  "Stop the service and its runtime dependencies.

  Parameters:
  - `:file`: configuration of all container workloads (default is
    `deploy/compose.yml`)
  - `:destroy`: also remove all project's containers and
    networks, but not volumes (default `true`)
  - `:destroy-volumes`: destroy volumes too (default `false`)"
  a/stop-compose)
