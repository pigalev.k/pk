#!/bin/bash

set -e

psql --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<- EOSQL
  CREATE DATABASE project_name_api__datahike__${PROFILE};
EOSQL
