# url-shortener-api

Service that exposes a REST api to shorten URLs and get original URLs back.

## Develop

### API

The entry point for this service is in the `short-url-api` base, please see its
documentation on development workflow.

### Build script (`build.clj`)

Add the `:build` alias to REPL options (e.g., `C-u M-x cider-jack-in` in Emacs,
default keys `C-u C-c M-j`).

```
clj -M:build:nrepl
```

## Build

```
clojure -T:build <command>
```

where commands are

+ `clean`: clean all the build results
+ `jar`: pack the project sources in a jar file (for libraries)
+ `uberjar`: build the project and pack the results in an uberjar file
+ `image`: build a container image with the project's uberjar

Use `help` command to see all available tasks.

## Run

```
clojure -T:build <command>
```

where commands are

+ `run`: run the service uberjar (`C-c` to stop)
+ `run-container`: run the service in a standalone container (`C-c` to stop)
+ `run-compose`: run the service and its runtime dependencies in containers
+ `stop-compose`: stop the service and its runtime dependencies

Use `help` command to see all available tasks.

## Test

Manual integration tests

See `docs/api.org`.

## Data

For example, when PostgreSQL or Datahike over PostgreSQL is used for
persistence: while PostgreSQL container is running (with no clients)

### Backup

```
podman exec -i <postgres container name> /bin/bash -c "PGPASSWORD=<password> \\
    pg_dump --username postgres <database name>" \\
    > ~/<database name>--$(date +%Y-%m-%d--%H:%M:%S).sql
```

### Restore

```
podman exec -i <postgres container name> /bin/bash -c "PGPASSWORD=<password> \\
    psql --username postgres <database name>" < ~/dump.sql
```
