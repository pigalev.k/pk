#!/bin/bash

set -e

psql --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<- EOSQL
	CREATE DATABASE short_url_api__${PROFILE};
EOSQL
