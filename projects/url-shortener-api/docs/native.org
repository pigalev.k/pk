* Compiling the service to native image with GraalVM

  Currently not quite working.

  1. Build uberjar.

  2. Run uberjar with GraalVM tracing agent to generate reachability metadata
     #+begin_src sh
       <graal-java> -agentlib:native-image-agent=config-output-dir=target/reachability-metadata \
                  -jar target/<uberjar file name>
     #+end_src

  3. Review the generated metadata (JSON files) in
     'target/reachability-metadata' directory, place those that are needed in
     'resources/META-INF/native-image/pk/url-shortener-api' directory.

  4. Compile uberjar to native image
     #+begin_src sh
       native-image -jar target/<uberjar file name> \
                    target/<native image file name>
     #+end_src


  Issues:
  - Image building fails due to problems with reflection & buildtime class
    initialization.
