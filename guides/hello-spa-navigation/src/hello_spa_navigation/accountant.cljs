(ns hello-spa-navigation.accountant
  (:require
   [accountant.core :as accountant]
   [secretary.core :as secretary :refer-macros [defroute]]))


;; Navigation between "pages" of SPA.

;; https://github.com/venantius/accountant

(enable-console-print!)
(println "SPA navigation: Accountant guide")

(comment

  ;; Accountant is a ClojureScript library to make navigation in single-page
  ;; applications simple.

  ;; by default, clicking a link in a ClojureScript application that isn't a
  ;; simple URL fragment will trigger a full page reload. This defeats the
  ;; purpose of using such elegant frameworks as Om, Reagent, et al.

  ;; with Accountant, links that correspond to defined routes will trigger
  ;; dispatches to those routes and update the browser's path, but won't reload
  ;; the page.

  ;; Accountant also lets you navigate the app to a new URL directly, rather
  ;; than through <a> tags.

  ;; be aware that Accountant relies on the browser's HTML5 history, so older
  ;; browsers will be left behind.

  (accountant/configure-navigation!
   {:nav-handler   (fn [path]
                     (secretary/dispatch! path)
                     (println "Path: " path))
    :path-exists?  (fn [path]
                     (secretary/locate-route path))
    :reload-same-path? false})

  ;; the `:nav-handler` value is a fn of one argument, the path we're about to
  ;; navigate to. You'll want to make whatever side-effect you need to render
  ;; the page here. If you're using secretary, it'd look something like:

  ;; (fn [path]
  ;;   (secretary/dispatch! path))

  (defroute "/users/:id" {:as params}
    (js/console.log (str "User: " (:id params))))

  (secretary/dispatch! "/users/gf3")


  ;; if you're using bidi + just rendering via react, that might look like:

  ;; (fn [path]
  ;;   (om/update! app [:path] path))

  ;; the `:path-exists?` value is a fn of one argument, the path that we're
  ;; about to navigate to. The fn should return truthy if the path is handled by
  ;; your SPA, because accountant will call `event.preventDefault()` to prevent
  ;; the browser from doing a full page request.

  ;; using secretary, `:path-exists?` should have a value like:

  ;; (fn [path]
  ;;   (secretary/locate-route path))

  ;; using bidi, it would look like:

  ;; (fn [path]
  ;;   (boolean (bidi/match-route app-routes path)))

  ;; by default, clicking a link to the currently active route will not trigger
  ;; the navigation handler. You can disable this behavior and always trigger
  ;; the navigation handler by setting `reload-same-path?` to true during
  ;; configuration.

  ;; you can also use Accountant to set the current path in the browser, e.g.

  (accountant/navigate! "/users/1")

  ;; if you want to dispatch the current path, just add the following:

  (accountant/dispatch-current!)

  ;; note that both `navigate!` and `dispatch-current!` can only be used after
  ;; calling `configure-navigation!`.

  ;; to cleanup the resources allocated by `configure-navigation!`, use
  ;; `unconfigure-navigation!`. This is useful in cases where you create a
  ;; component that manages configuring navigation, and would like to be able to
  ;; easily start/stop it.

  (accountant/unconfigure-navigation!)


  ;; caveat: UI frameworks

  ;; sometimes links may be used nested within UI components, especially when
  ;; using third-party wrappers, like react-bootstrap etc. These links may have
  ;; an empty href attribute or a value like `#`. Two things might happen:
  ;; Either, if a route is defined for the root path (i.e. '/' or '/#'),
  ;; accountant will suppress the browser navigation and dispatch via secretary
  ;; or the browser will reload the page.

  ;; to prevent this, Accountant looks for an attribute `data-trigger` on every
  ;; link. The presence of this attribute signals that this link is a means to
  ;; trigger a callback, not a navigation. If `data-trigger` is defined on a
  ;; link it gets completely ignored, just like a button.

  ;; example: when using a DropdownButton with MenuItems each item will contain
  ;; an <a href="#"...> element. Since this element can't be replaced, we can at
  ;; least add arbitrary attributes to it:

  ;; (let [dropdown-button (reagent/adapt-react-class
  ;;                        js/ReactBootstrap.DropdownButton)
  ;;       menuitem (reagent/adapt-react-class js/ReactBootstrap.MenuItem)]
  ;;   [dropdown-button {:id "foo" :title "..." :onSelect (fn [idx]...)}
  ;;    [menuitem {:id "1"
  ;;               :data-trigger true
  ;;               :eventKey "1"}]

)
