(ns hello-spa-navigation.secretary
  (:require
   [goog.events :as events]
   [secretary.core :as secretary :refer-macros [defroute]])
  (:import [goog History]
           [goog.history EventType]))


;; Routing and dispatch

;; https://github.com/clj-commons/secretary

(enable-console-print!)
(println "SPA routing and dispatch: Secretary guide")

(comment

  ;; basic routing and dispatch

  ;; Secretary is built around two main goals: creating route matchers and
  ;; dispatching actions. Route matchers match and extract parameters from URI
  ;; fragments and actions are functions which accept those parameters.

  ;; `defroute` is Secretary's primary macro for defining a link between a route
  ;; matcher and an action. The signature of this macro is [`name?` `route`
  ;; `destruct` & `body`]. We will skip the `name?` part of the signature for
  ;; now and return to it when we discuss named routes. To get clearer picture
  ;; of this let's define a route for users with an id.

  (defroute "/users/:id" {:as params}
    (js/console.log (str "User: " (:id params))))

  ;; in this example "/users/:id" is `route`, the route matcher, {:as params} is
  ;; `destruct`, the destructured parameters extracted from the route matcher
  ;; result, and the remaining (js/console.log ...) portion is `body`, the route
  ;; action.

  ;; before going in to more detail let's try to dispatch our route.

  (secretary/dispatch! "/users/gf3")

  ;; with any luck, when we refresh the page and view the console, we should see
  ;; that "User: gf3" has been logged somewhere.


  ;; route matchers

  ;; by default the route matcher may either be a string or regular
  ;; expression. String route matchers have special syntax which may be familiar
  ;; to you if you've worked with Sinatra or Ruby on Rails. When
  ;; `secretary/dispatch!` is called with a URI it attempts to find a route
  ;; match and it's corresponding action. If the match is successful, parameters
  ;; will be extracted from the URI. For string route matchers these will be
  ;; contained in a map; for regular expressions a vector.

  ;; in the example above, the route matcher "/users/:id" successfully matched
  ;; against "/users/gf3" and extracted {:id "gf3} as parameters. You can refer
  ;; to the table below for more examples of route matchers and the parameters
  ;; they return when matched successfully.

  ;;   Route matcher      URI             Parameters
  ;; - "/:x/:y"           "/foo/bar"      {:x "foo" :y "bar"}
  ;; - "/:x/:x"           "/foo/bar"      {:x ["foo" "bar"]}
  ;; - "/files/*.:format" "/files/x.zip"  {:* "x" :format "zip"}
  ;; - "*"                "/any/thing"    {:* "/any/thing"}
  ;; - "/*/*"             "/n/e/thing"    {:* ["n" "e/thing"]}
  ;; - "/*x/*y"           "/n/e/thing"    {:x "n" :y "e/thing"}
  ;; - #"/[a-z]+/\d+"     "/foo/123"      ["/foo/123"]
  ;; - #"/([a-z]+)/(\d+)" "/foo/123"      ["foo" "123"]


  ;; parameter destructuring

  ;; now that we understand what happens during dispatch we can look at the
  ;; `destruct` argument of `defroute`. This part is literally sugar around
  ;; `let`. Basically whenever one of our route matches is successful and
  ;; extracts parameters this is where we destructure them. Under the hood, for
  ;; example with our users route, this looks something like the following.

  (let [{:as params} {:id "gf3"}]
    params)

  ;; Given this, it should be fairly easy to see that we could have have written

  (defroute "/users/:id" {id :id}
    (js/console.log (str "User: " id)))

  (secretary/dispatch! "/users/gf3")

  ;; and seen the same result. With string route matchers we can go even further
  ;; and write

  (defroute "/users/:id" [id]
    (js/console.log (str "User: " id)))

  (secretary/dispatch! "/users/gf3")

  ;; which is essentially the same as saying {:keys [id]}.

  ;; for regular expression route matchers we can only use vectors for
  ;; destructuring since they only ever return vectors.

  (defroute #"/users/(\d+)" [id]
    (js/console.log (str "User: " id)))

  (secretary/dispatch! "/users/gf3")


  ;; query parameters

  ;; if a URI contains a query string it will automatically be extracted to
  ;; `:query-params` for string route matchers and to the last element for
  ;; regular expression matchers.

  (defroute "/users/:id" {:keys [id query-params]}
    (js/console.log (str "User: " id))
    (js/console.log (pr-str query-params)))

  (secretary/dispatch! "/users/10?action=delete")

  (defroute #"/users/(\d+)" [id query-params]
    (js/console.log (str "User: " id))
    (js/console.log (pr-str query-params)))

  (secretary/dispatch! "/users/10?action=delete")


  ;; named routes

  ;; while route matching and dispatch is by itself useful, it is often
  ;; necessary to have functions which take a map of parameters and return a
  ;; URI. By passing an optional name to `defroute` Secretary will define this
  ;; function for you.

  (defroute users-path "/users" []
    (js/console.log "Users path"))

  (defroute user-path "/users/:id" [id]
    (js/console.log (str "User " id "'s path")))

  (secretary/dispatch! "/users/10?action=delete")
  (users-path)
  (user-path {:id 1})

  ;; this also works with `:query-params`.

  (user-path {:id 1 :query-params {:action "delete"}})

  ;; if the browser you're targeting does not support HTML5 history you can call

  (secretary/set-config! :prefix "#")

  ;; to prefix generated URIs with a "#".

  (user-path {:id 1})

  ;; beware that using `:prefix` that way will make resulting URIs no longer
  ;; compliant with standard URI syntax –-- the fragment must be the last part of
  ;; the URI after the query. Indeed, the syntax of a URI is defined as:

  ;; scheme:[//[user:password@]host[:port]][/]path[?query][#fragment]

  ;; Secretary adds a `#` after the path so it makes the fragment hide the
  ;; query. For instance, the following URL is comprehended in different ways by
  ;; secretary and the spec:

  ;; https://www.example.com/path/of/app#path/inside/app?query=params&as=defined&by=secretary

  ;; - the fragment is "path/inside/app?query=params&as=defined&by=secretary"
  ;;   for standard libraries, but is "path/inside/app" according to Secretary
  ;; - the query is "" for standard libraries, but is
  ;;   "query=params&as=defined&by=secretary" according to Secretary


  ;; available protocols

  ;; you can extend Secretary's protocols to your own data types and records if
  ;; you need special functionality.

  ;; - `IRenderRoute`
  ;; - `IRouteMatches`

  ;; `IRenderRoute`

  ;; most of the time the defaults will be good enough but on occasion you may
  ;; need custom route rendering. To do this implement `IRenderRoute` for your
  ;; type or record.

  (defrecord User [id]
    secretary/IRenderRoute
    (render-route [_]
      (str "/users/" id))
    (render-route [this params]
      (str (secretary/render-route this) "?"
           (secretary/encode-query-params params))))

  (secretary/render-route (User. 1))
  (secretary/render-route (User. 1) {:action :delete})


  ;; `IRouteMatches`

  ;; it is seldom you will ever need to create your own route matching
  ;; implementation as the built in String and RegExp routes matchers should be
  ;; fine for most applications. Still, if you have a suitable use case then
  ;; this protocol is available. If your intention is to is to use it with
  ;; `defroute` your implementation must return a map or vector.

  ;; example with `goog.History`

  (def application
    (js/document.getElementById "app"))

  (defn set-html! [el content]
    (aset el "innerHTML" content))

  (secretary/set-config! :prefix "#")

  ;; /#/
  (defroute home-path "/" []
    (set-html! application "<h1>OMG! YOU'RE HOME!</h1>"))

  ;; /#/users
  (defroute users-path "/users" []
    (set-html! application "<h1>USERS!</h1>"))

  ;; /#/users/:id
  (defroute user-path "/users/:id" [id]
    (let [message (str "<h1>HELLO USER <small>" id "</small>!</h1>")]
      (set-html! application message)))

  ;; /#/777
  (defroute jackpot-path "/777" []
    (set-html! application "<h1>YOU HIT THE JACKPOT!</h1>"))

  ;; Catch all
  (defroute "*" []
    (set-html! application "<h1>LOL! YOU LOST!</h1>"))

  ;; quick and dirty history configuration

  (doto (History.)
    (events/listen EventType.NAVIGATE #(secretary/dispatch! (.-token %)))
    (.setEnabled true))

)
