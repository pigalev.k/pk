(ns hello-spa-navigation.clerk
  (:require
   [accountant.core :as accountant]
   [clerk.core :as clerk]
   [goog.dom :as gdom]
   [goog.string :as gstr]
   [reagent.core :as r]
   [reagent.dom :as rdom]
   [secretary.core :as secretary :refer-macros [defroute]]))


;; Navigation between, and within, "pages" of SPA.

;; https://github.com/PEZ/clerk

(enable-console-print!)
(println "SPA navigation: Clerk guide")


;; basics

(comment

  ;; the problem

  ;; today's web browsers handle all this automatic scroll positioning perfectly
  ;; for regular sites. But the S in SPA really means that everything happens on
  ;; the same page, even if it looks to the user as if navigating between pages
  ;; happens. A new page is just the result of rendering new content, so scroll
  ;; position is not adjusted properly (remains the same). In addition to this:

  ;; - the browser's default scroll restoration for history navigation can't
  ;;   be trusted within an SPA. It sometimes looks like it works, but then
  ;;   comes with big time surprises at other times.
  ;; - in-page navigation to anchor targets doesn't happen at all unless we add
  ;;   code for it.


  ;; solution

  ;; clerk takes care of the scroll positioning when:

  ;; - navigating to a new page, e.g. if the user clicks a link to another page.
  ;;   Scroll is set to the top of the page in these cases.
  ;; - navigation to target anchors within the page. Scroll is smoothly adjusted
  ;;   to the top of the target element. This only works if the routing library
  ;;   you are using supports hash targets.
  ;; - navigating back/forth using the web browser history navigation. Scroll
  ;;   position is restored to whatever it was when the user left it.

  ;; Clerk does not deal with anything else beside the above. Use it together
  ;; wih your routing and HTML5 history libararies of choice.


  ;; initialize

  ;; initialize Clerk as early as possible when your app is starting:

  (clerk/initialize!)


  ;; after navigation dispatch

  ;; after any routing/navigation dispatch of your app you need to tell Clerk
  ;; about the new path.

  (clerk/navigate-page! "/hello")


  ;; after render

  ;; then just one more thing. To avoid flicker, Clerk defers scroll adjustment
  ;; until after the page is rendered. You need to tell Clerk when rendering is
  ;; done:

  (clerk/after-render!)

  ;; depending on your project, the after render notification will need to be
  ;; injected in different ways. Here are examples for two common ClojureScript
  ;; React frameworks, Rum and Reagent:

  ;; Rum

  ;; rum has an utility callback `:after-render` that can be used in a mixin for
  ;; this purpose, like so:

  ;; (defc page < rum/reactive
  ;;   {:after-render
  ;;    (fn [state]
  ;;      (after-render!)
  ;;      state)})

  ;; Reagent

  ;; for Reagent, you can use the `reagent.core/after-render` function, which
  ;; calls any function you provide to it when rendering is done:

  (r/after-render clerk/after-render!)

  ;; you can also hook it in to the component life cycle, `:component-did-mount`
  ;; and `:component-did-update`, if that suits your project and taste better.


  ;; putting it all together

  (def lorem "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
  eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis eleifend quam
  adipiscing vitae proin sagittis nisl. Facilisi etiam dignissim diam quis. Nibh
  ipsum consequat nisl vel pretium lectus quam id leo. Aliquam eleifend mi in
  nulla posuere. Platea dictumst vestibulum rhoncus est. Nisl condimentum id
  venenatis a condimentum vitae sapien pellentesque. Risus at ultrices mi tempus
  imperdiet nulla malesuada. Felis donec et odio pellentesque diam volutpat
  commodo sed egestas. Nisl nunc mi ipsum faucibus vitae aliquet nec. Aliquam
  etiam erat velit scelerisque in. Habitant morbi tristique senectus et
  netus. Suspendisse interdum consectetur libero id faucibus. Ipsum faucibus
  vitae aliquet nec ullamcorper sit amet risus. Non diam phasellus vestibulum
  lorem sed risus.")

  (defn lorem-ipsum
    "Creates `n` paragraphs of `text` with anchor links."
    [n text]
    (let [nbsp (gstr/unescapeEntities "&nbsp;")]
      [:div#lorem-top.lorem-ipsum
       [:h2 "In-page navigation demo"]
       (for [i (range n)]
         [:a {:href (str "#lorem-" i) :key i} (str "lorem-" i nbsp)])
       [:a {:href "#lorem-bottom"} "bottom"]
       [:br][:hr]
       (for [i (range n)]
         [:<> {:key i}
          [:a {:href (str "#lorem-" i)
               :id   (str "lorem-" i)}[:b "Lorem " i nbsp]]
          [:a {:href "#lorem-top"} "top" nbsp]
          [:a {:href "#lorem-bottom"} "bottom" nbsp]
          [:p text]])
       [:hr]
       (for [i (range n)]
         [:a {:href (str "#lorem-" i) :key i} (str "lorem-" i nbsp)])
       [:a#lorem-bottom {:href "#lorem-top"} "top"]]))

  (defn mount-root
    "Mount reagent component `c` into DOM and render it."
    [c]
    (rdom/render c (gdom/getElement "app")))

  ;; the Leiningen Reagent template's `init!` function will look like so with
  ;; all clerky stuff added:

  (defn init! []
    (clerk/initialize!)
    (accountant/configure-navigation!
     {:nav-handler
      (fn [path]
        (println "Navigating to " path)
        (r/after-render clerk/after-render!)
        (secretary/dispatch! path)
        (clerk/navigate-page! path))
      :path-exists?
      (fn [path]
        (secretary/locate-route path))})
    (accountant/dispatch-current!)
    (mount-root [lorem-ipsum 10 lorem]))

  (init!)

  ;; for Rum it will look very similar, except that you need to use a mixin for
  ;; your page component's `:after-render` callback instead of using the
  ;; `reagent.core/after-render`.


  ;; it is not always this simple

  ;; registering `clerk/after-render!` on the "page component" or on navigation
  ;; dispatch is not sufficient for some applications. Some pages get loaded and
  ;; rendered in phases and for some apps it can take quite a while before they
  ;; have all the data they need to render the final page. (And lots of other
  ;; cases.) Finding the right entry point to inject the Clerk
  ;; functions/commands will sometimes be a challenge.


  ;; caveats

  ;; - IMPORTANT: if you are using some kind of analytics (like Google
  ;;   Analytics) for stats on site usage for your SPA, take care with any
  ;;   history change events resulting in "virtual" page hits. Clerk uses the
  ;;   browser's history state to store the current scroll position for the
  ;;   page. Specifically, the default History Change Trigger of Google Tag
  ;;   Manager can't be used as is. You risk spamming your stats with "page
  ;;   views" that really are just the user scrolling.

  ;; - Clerk depends on HTML 5 history and does not handle routing that rely on
  ;;   prefixing route paths with '#'. If you still need to target browsers that
  ;;   do not have HTML 5 history: no Clerk for you.

)
