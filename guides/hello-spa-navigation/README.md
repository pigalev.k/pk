# hello-spa-navigation

Exploring Clerk and Accountant --- libraries for navigation in ClojureScript
SPAs.

- https://github.com/PEZ/clerk
- https://github.com/venantius/accountant

## Getting started

Start a cljs REPL in terminal

```
clj -M -m cjls.main
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
