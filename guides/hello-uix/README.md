# hello-uix

Exploring UIx --- an idiomatic ClojureScript interface to modern React.js.

- https://github.com/roman01la/uix

## Getting started

Start a cljs REPL in terminal

```
clj -M -m cjls.main
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
