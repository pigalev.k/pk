(ns hello-sieppari.core
  (:require
   [clojure.core.async :as async]
   #?(:clj [manifold.deferred :as d])
   [promesa.core :as p]
   [sieppari.async.core-async]
   #?(:clj [sieppari.async.manifold])
   [sieppari.core :as s]))


;; Sieppari

;; https://cljdoc.org/d/metosin/sieppari/0.0.0-alpha13/doc/readme
;; node REPL works fine

;; intro

(comment

  ;; what it does

  ;; interceptors, like in Pedestal, but with minimal implementation and optimal
  ;; performance.

  ;; the core Sieppari depends on Clojure and nothing else.

  ;; if you are new to interceptors, check the Pedestal Interceptors
  ;; documentation. Sieppari's `sieppari.core/execute` follows a
  ;; `:request`/`:response` pattern. For Pedestal-like behavior, use
  ;; `sieppari.core/execute-context`.


  ;; sync

  ;; interceptor: in `:enter` update value in request

  (def inc-x-interceptor
    {:enter (fn [ctx] (update-in ctx [:request :x] inc))})

  ;; interceptor: in `:leave` update value in response

  (def double-y-interceptor
    {:leave (fn [ctx] (update-in ctx [:response :y] * 2))})

  ;; handler: take `:x` from request, apply `inc`, and return an map with `:y`

  (defn handler [request]
    {:y (inc (:x request))})

  #?(:clj (s/execute
           [inc-x-interceptor double-y-interceptor handler]
           {:x 40}))

  #?(:cljs (s/execute
            [inc-x-interceptor double-y-interceptor handler]
            {:x 40}
            ;; cljs is always async
            (partial println "SUCCESS:")
            (partial println "FAILURE:")))


  ;; async

  ;; any step in the execution pipeline (`:enter`, `:leave`, `:error`) can
  ;; return either a context map (synchronous execution) or an instance of
  ;; `AsyncContext`, indicating asynchronous execution.

  ;; by default, clojure deferrables, `java.util.concurrent.CompletionStage` and
  ;; `js/promise` satisfy the `AsyncContext` protocol.

  ;; using `s/execute` with async steps will block

  ;; async interceptor: in `:leave` double value of `[:response :y]`:

  (def multiply-y-interceptor
    {:leave (fn [ctx]
              #?(:clj (future
                        (Thread/sleep 1000)
                        (update-in ctx [:response :y] * 2))
                 :cljs (async/go
                         (async/<! (async/timeout 1000))
                         (update-in ctx [:response :y] * 2))))})

  #?(:clj (s/execute
           [inc-x-interceptor multiply-y-interceptor handler]
           {:x 40}))

  ;; using non-blocking version of `s/execute`

  (s/execute
   [inc-x-interceptor multiply-y-interceptor handler]
   {:x 40}
   (partial println "SUCCESS:")
   (partial println "FAILURE:"))

  ;; blocking on async computation

  #?(:clj (let [respond (promise)
                raise (promise)]
            (s/execute
             [inc-x-interceptor multiply-y-interceptor handler]
             {:x 40}
             respond
             raise) ; returns nil immediately
            (deref respond 2000 :timeout)))

  ;; any step can return a `java.util.concurrent.CompletionStage` or
  ;; `js/promise`, Sieppari works out of the box with libraries like Promesa

  (def chain
    [{:enter #(update-in % [:request :x] inc)}
     {:leave #(p/promise (update-in % [:response :x] / 10))}
     {:enter #(p/delay 1000 %)}
     identity])

  ;; blocking

  #?(:clj (s/execute chain {:x 40}))

  ;; non-blocking

  ;; note: result will be Ratio in clj and float in cljs
  (s/execute
   chain
   {:x 40}
   (partial println "SUCCESS:")
   (partial println "SUCCESS:"))


  ;; external async libraries

  ;; to add a support for one of the supported external async libraries, just
  ;; add a dependency to them and require the respective Sieppari
  ;; namespace. Currently supported async libraries are
  ;; - `core.async`: `sieppari.async.core-async`, clj & cljs
  ;;  - Manifold: `sieppari.async.manifold`, clj

  ;; to extend Sieppari async support to other libraries, just extend the
  ;; `AsyncContext` protocol.


  ;; core.async

  (defn multiply-x-interceptor--core-async [n]
    {:enter (fn [ctx]
              (async/go (update-in ctx [:request :x] * n)))})

  #?(:clj (s/execute
           [inc-x-interceptor (multiply-x-interceptor--core-async 10) handler]
           {:x 40}))

  (s/execute
   [inc-x-interceptor (multiply-x-interceptor--core-async 10) handler]
   {:x 40}
   (partial println "SUCCESS:")
   (partial println "SUCCESS:"))


  ;; Manifold

  #?(:clj

     (defn minus-x-interceptor--manifold [n]
            {:enter (fn [ctx]
                      (d/success-deferred (update-in ctx [:request :x] - n)))})

     (s/execute
      [inc-x-interceptor (minus-x-interceptor--manifold 10) handler]
      {:x 40}))

)
