# hello-sieppari

Exploring Sieppari --- small, fast, and complete interceptor library for
Clojure/Script with built-in support for common async libraries.

- https://github.com/metosin/sieppari

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
