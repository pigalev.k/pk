(ns hello-software-flexibility.regex)


(def chars-needing-quoting #{\. \[ \\ \^ \$ \*})

(def r:dot ".")
(def r:bol "^")
(def r:eol "$")

(defn r:seq
  [& exprs]
  (str "(" (apply str exprs) ")"))

(defn r:quote
  [string]
  (apply r:seq
   (map (fn [c]
          (if (contains? chars-needing-quoting c)
            (list \\ c)
            (list c)))
        string)))




(comment

  (r:seq r:bol r:dot)
  (doall (r:quote ".asd*"))

)
