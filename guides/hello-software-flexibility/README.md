# hello-software-flexibility

Exploring concepts from Software Design for Flexibility book.

- https://mitpress.mit.edu/9780262045490/software-design-for-flexibility/

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
