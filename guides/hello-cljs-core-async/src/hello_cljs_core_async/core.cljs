(ns hello-cljs-core-async.core
  "Basics of `cljs.core.async`."
  (:require
   [cljs.core.async
    :refer-macros [go go-loop alt!]
    :refer [chan put! take! >! <! buffer dropping-buffer
            sliding-buffer timeout close! alts!]]))


;; cljs.core.async 101
;; https://github.com/loganpowell/cljs-guides/blob/master/src/guides/core-async-basics.md

;; main differences with clj:
;; - no blocking


;; `put!`, `take!`

(defn take-logger [order]
  (prn (str "order taken: " order)))

(defn put-logger [boolean]
  (prn (str "order put? " boolean)))

(comment

  (def bufferless-chan (chan))

  ;; put
  (put! bufferless-chan "Futo Maki" put-logger)
  (put! bufferless-chan "Vegan Spider" put-logger)

  ;; take
  (take! bufferless-chan take-logger)

  ;; when item is successfully put or taken, callback called on following side
  ;; first (who made a last move): if there was pending take before put, put's
  ;; callback will be called first and take's second
)


;; pending puts overflow

(defn put!-order [channel order]
  (put! channel order put-logger))

(defn take!-order [channel]
  (take! channel take-logger))

(defn bot-orders [channel order]
  (dotimes [x 1030]
    (put!-order channel order)))

(defn put!-n-order [channel order n]
  (put! channel (str "#: " n " order: " order) put-logger))

(defn IHOS-orders [channel order]
  (dotimes [x 2100] ; lots-o'-orders
    (put!-n-order channel order x)))

(comment

  ;; max pending put/take queue length is 1024
  (def bufferless-chan (chan))
  (bot-orders bufferless-chan "Sushi!")

  ;; 10 orders are accepted by channel immediately
  (def fixed-chan (chan 10))
  (bot-orders fixed-chan "Sushi!")

  ;; one order is taken from buffer and another is immediately accepted into
  ;; buffer from the queue of pending puts
  (take!-order fixed-chan)


  (def fixed-chan (chan 10))
  ;; will overflow
  (IHOS-orders fixed-chan "Nigiri!")

  ;; sliding buffer (always accept and drop oldest on overflow)
  (def slide-chan (chan (sliding-buffer 10)))
  ;; all accepted (most dropped out)
  (IHOS-orders slide-chan "Sashimi")
  ;; 10 newest remain
  (take!-order slide-chan)

  ;; dropping buffer (always accept and drop incoming on overflow)
  (def drop-chan (chan (dropping-buffer 10)))
  (IHOS-orders drop-chan "Tofu Katsu")
  ;; 10 oldest remain
  (take!-order drop-chan)

)


;; backpressure with upstream `>!` in `go` and async downstream `take!`

(defn backpressured-orders [channel order]
  (go
    (dotimes [x 2100] ; increase number of bot orders
      (put-logger (>! channel (str "#: " x " order: " order))))))

(defn burst-take! [channel]
  (dotimes [x 50] ; take 50 orders at a time
    (take!-order channel)))

(comment

  (def burst-chan (chan 50))
  ;; channel accepts 50 order puts, then producer is parked
  (backpressured-orders burst-chan "Umami Tamago")

  ;; 50 orders are taken from channel buffer, another 50 can be put
  (burst-take! burst-chan)

)


;; backpressure with upstream `>!` in `go` and parking downstream `<!` in `go`

(defn burst-<! [channel]
  (go
    (dotimes [x 50]
      (take-logger (<! channel)))))

(comment

  (def burst-chan (chan 50))
  (backpressured-orders burst-chan "Umami Tamago")

  (burst-<! burst-chan)

  ;; same as before
)


;; backpressure with async upstream `put!` and and parking downstream `<!` in
;; `go`: no avail

;; to implement backpressure, the parking syntax `(go ...(>! ...))` must be
;; implemented upstream (from the producer/putting end) of the channel.

(comment

  (def burst-chan (chan 50))
  (IHOS-orders burst-chan "Miso Soup!")
  (burst-<! burst-chan)

  ;; no good, orders get dropped
)


;; shutdown processing with `close!`

(defn max-order [channel order]
  (go
    (dotimes [x 12]
      (put-logger (>! channel (str "#: " x " order: " order))))
    (close! channel)))

(defn take!-til-closed [channel]
  (dotimes [x 5]
    (take!-order channel)))

(comment

  (def capacity (chan 5))
  (max-order capacity "Wildcard")

  ;; after supplying 12 orders (on the 3rd take batch) channel got closed and
  ;; last 3 takes got nil
  (take!-til-closed capacity)

  (put! capacity "Overflow" put-logger)
)


;; control structure: `alts!`

(defn timeout-chan [channel]
  (let [closer (timeout 3000)]
    (go (while true (<! (timeout 250)) (>! channel "Main Bar")))
    (go (while true (<! (timeout 500)) (>! channel "Online Order")))
    (go (while true (<! (timeout 750)) (>! channel "Roulette Room")))
    (go-loop [_ []]
      (let [[val ch] (alts! [channel closer])] ; <- `alts!`
        (cond
          (= ch closer)
          (do
            (close! channel)
            (.log js/console
                  (str "No more orders. Domo Arigatogozaimashita.")))
          :else
          (recur (.log js/console (str "Order up: " (<! channel)))))))))

(comment

  (def capacity (chan 5))

  ;; accepts order from that of 3 producers that is ready until timeout is
  ;; reached and channel is closed
  (timeout-chan capacity)

  (take! capacity take-logger)

  ;; prints to terminal where the JS runtime is running

  ;; We just dtarted three. independent. asynchronous. processes. In three lines
  ;; of code
)
