(ns hello-cljs-core-async.unraveled
  "More thorough guide through `cljs.core.async`.
  See http://funcool.github.io/clojurescript-unraveled/#csp-with-core-async."
  (:require
   [cljs.core.async
    :refer-macros [go go-loop alt!]
    :refer [chan timeout onto-chan! to-chan!
            buffer dropping-buffer sliding-buffer
            put! take! offer! poll! >! <! alts! close!
            pipe pipeline pipeline-async split merge into
            mult tap untap untap-all
            pub sub unsub unsub-all
            mix admix unmix unmix-all toggle solo-mode]
    :as async]))


(enable-console-print!)


;;;; basics

;; channels, puts and takes

(comment

  ;; puts and takes are asynchronous; they can succeed (return) immediately but
  ;; be completed at a later time.

  (def ch (chan))
  (take! ch #(println "Got a value:" %))
  ;; New or pending takes will be "completed" with the `nil` sentinel value if
  ;; channel is closed; therefore nils cannot be put on the channel.
  (put! ch 42 #(println "Put succeeded:" %))
  ;; `put!` operations return true to signal that the put operation could
  ;; succeed, even though the value hasn’t yet been taken. Channels can be
  ;; closed, which will cause the put operations to not succeed.
  (close! ch)
  (put! ch 42)

)


;; buffers

(comment

  ;; there is a hard limit of 1024 pending puts or takes.

  ;; put operations can be buffered (will immediately succeed when value is
  ;; placed into buffer), with various policies on buffer overflow.

  ;; fixed size
  (def ch (chan 32))
  (def ch (chan (buffer 32)))
  (put! ch 42 #(println "Put succeeded:" %))
  (take! ch #(println "Got a value:" %))
  (close! ch)

  ;; dropping newest
  (def ch (chan (dropping-buffer 2)))
  (put! ch 42 #(println "Put succeeded:" %))
  (take! ch #(println "Got a value:" %))
  (close! ch)

  ;; sliding out oldest
  (def ch (chan (sliding-buffer 2)))
  (put! ch 42 #(println "Put succeeded:" %))
  (take! ch #(println "Got a value:" %))
  (close! ch)

)


;; transducers

(comment

  ;; putting values in a channel can be thought as a transducible process; we
  ;; can create channels and hand them a transducer, giving us the ability to
  ;; transform the input values before being put in the channel.

  ;; if we want to use a transducer with a channel we must supply a buffer since
  ;; the reducing function that will be modified by the transducer will be the
  ;; buffer’s add function. A buffer's 'add' function is a reducing function
  ;; since it takes a buffer and an input and returns a buffer with such input
  ;; incorporated.

  (def ch (chan 1 (map inc)))
  (put! ch 41)
  (take! ch #(println "Got" %))
  (close! ch)

  ;; when the reducing function returns a reduced value, channels recognize it
  ;; as a termination condition, which for channels is being closed; so channels
  ;; will be closed when a reduced value is encountered

  ;; `take` stateful transducer allows maximum 2 puts into the channel
  (def ch (chan 1 (take 2)))
  (take! ch #(println "Got" %))
  (put! ch 41)

)


;; handling exceptions

(comment

  ;; if adding a value to a buffer throws an exception, the operation will fail
  ;; and the exception will be logged to the console. However, channel
  ;; constructors accept a third argument: a function for handling exceptions.

  ;; when creating a channel with an exception handler, it will be called with
  ;; the exception whenever an exception occurs. If the handler returns `nil`
  ;; the operation will fail silently and if it returns another value the add
  ;; operation will be retried with such value.

  (defn exception-xform
    [rfn]
    (fn [acc input]
      (throw (js/Error. "I fail!"))))

  (defn handle-exception
    [ex]
    (println "Exception message:" (.-message ex))
    42)

  (def ch (chan 1 exception-xform handle-exception))
  (put! ch 0)
  (take! ch #(println "Got:" %))

)


;; offer and poll

(comment

  ;; `offer!` and `poll!` are synchronous putting and taking operations; they
  ;; will (or will not) succeed and be completed simultaneously and immediately.

  ;; `offer!` puts a value in a channel if it’s possible to do so
  ;; immediately. It returns `true` if the channel received the value and `nil`
  ;; otherwise. Note that, unlike with `put!`, `offer!` cannot distinguish
  ;; between closed and open channels.

  (def ch (chan 1))
  (offer! ch 42)
  (offer! ch 43)

  ;; `poll!` takes a value from a channel if it’s possible to do so
  ;; immediately. Returns the value if succesful and `nil` otherwise. Unlike
  ;; `take!`, `poll!` cannot distinguish between closed and open channels.

  (def ch (chan 1))
  (poll! ch)
  (offer! ch 42)
  (poll! ch)

)


;;;; processes

;; go

(comment

  ;; processes are pieces of logic that run independently and use channels for
  ;; communication and coordination. Puts and takes inside a process will stop
  ;; the process until the operation completes. Stopping a process doesn’t block
  ;; the only thread we have in the environments where ClojureScript
  ;; runs. Instead, it will be resumed at a later time when the operation is
  ;; waiting for being performed.

  ;; processes are launched using the `go` macro and puts and takes use the `<!`
  ;; and `>!` placeholders. The `go` macro rewrites your code to use callbacks
  ;; but inside go everything looks like synchronous code, which makes
  ;; understanding it straightforward.

  (def ch (chan))

  (go
    (println [:a] "Gonna take from channel")
    (println [:a] "Got" (<! ch)))

  (go
    (println [:b] "Gonna put on channel")
    (>! ch 42)
    (println [:b] "Just put 42"))

  ;;  we are launching a process with `go` that takes a value from `ch` and
  ;;  prints it to the console. Since the value isn’t immediately available it
  ;;  will park until it can resume. After that we launch another process that
  ;;  puts a value on the channel.

  ;; since there is a pending take, the put operation succeeds and the value is
  ;; delivered to the first process, then both processes terminate.

  ;; both go blocks run independently and, even though they are executed
  ;; asynchronously, they look like synchronous code. The above go blocks are
  ;; fairly simple but being able to write concurrent processes that coordinate
  ;; via channels is a very powerful tool for implementing complex asynchronous
  ;; workflows. Channels also offer a great decoupling of producers and
  ;; consumers.

  (close! ch)

  ;; processes can wait for an arbitrary amount of time too; there is a
  ;; `timeout` function that return a channel that will be closed after the
  ;; given amount of miliseconds. Combining a timeout channel with a take
  ;; operation inside a go block gives us the ability to sleep:

  (defn seconds
    []
    (.getSeconds (js/Date.)))

  (go
    (println [:a] "Gonna take a nap:" (seconds))
    (<! (timeout 1000))
    (println [:a] "I slept one second, bye!" (seconds)))

  ;; the process does nothing for one second when it blocks in the take
  ;; operation of the timeout channel. The program continues and after one
  ;; second the process resumes and terminates.

)


;; choice

(comment

  ;; apart from putting and taking one value at a time inside a `go` block we
  ;; can also make a non-deterministic choice on multiple channel operations
  ;; using `alts!`. `alts!` is given a series of channel put or take
  ;; operations (note that we can also try to put and take in a channel at the
  ;; same time) and only performs one as soon as is ready; if multiple
  ;; operations can be performed when calling `alts!` it will do a pseudo random
  ;; choice by default.

  ;; for example, we can try an operation on a channel and cancel it after a
  ;; certain amount of time combining the timeout function and `alts!` (to
  ;; perform conditional read):

  (def ch (chan))

  (go
    (println [:a] "Gonna take a nap")
    (<! (timeout 1000))
    (println [:a] "I slept one second, trying to put a value on channel")
    (>! ch 42)
    (println [:a] "I'm done!"))

  (go
    (println [:b] "Gonna try taking from channel")
    (let [cancel (timeout 300)
          [value ch] (alts! [ch cancel])]
    (if (= ch cancel)
      (println [:b] "Too slow, take from channel cancelled")
      (println [:b] "Got" value))))

  ;; let’s look at how to express a conditional write operation:

  (def a-ch (chan))
  (def another-ch (chan))

  (go
    (println [:a] "Take a value from `a-ch`")
    (println [:a] "Got" (<! a-ch))
    (println [:a] "I'm done!"))

  (go
    (println [:b] "Take a value from `another-ch`")
    (println [:a] "Got" (<! another-ch))
    (println [:b] "I'm done!"))

  (go
    (println [:c] "Gonna try putting in both channels simultaneously")
    (let [[value ch] (alts! [[a-ch 42]
                             [another-ch 99]])]
      (if (= ch a-ch)
        (println [:c] "Put a value in `a-ch`")
        (println [:c] "Put a value in `another-ch`"))))

)


;; priority

(comment

  ;; `alts!` default is to make a non-deterministic choice whenever several
  ;; operations are ready to be performed. We can instead give priority to the
  ;; operations passing the `:priority` option to `alts!`. Whenever `:priority`
  ;; is `true`, if more than one operation is ready, they will be tried in
  ;; order (from left to right in `alts!` channel list argument).

  (def a-ch (chan))
  (def another-ch (chan))

  (go
    (println [:a] "Put a value on `a-ch`")
    (>! a-ch 42)
    (println [:a] "I'm done!"))

  (go
    (println [:b] "Put a value on `another-ch`")
    (>! another-ch 99)
    (println [:b] "I'm done!"))

  (go
    (println [:c] "Gonna try taking from both channels with priority")
    (let [[value ch] (alts! [a-ch another-ch] :priority true)]
      (if (= ch a-ch)
        (println [:c] "Got" value "from `a-ch`")
        (println [:c] "Got" value "from `another-ch`"))))

)


;; defaults

(comment

  ;; `alts!` can return immediately if no operation is ready and we provide a
  ;; default value. We can conditionally do a choice on the operations if and
  ;; only if any of them is ready, returning a default value if it's not.

  (def a-ch (chan))
  (def another-ch (chan))

  (go
    (println [:a] "Gonna try taking from any of the channels without blocking")
    (let [[value ch] (alts! [a-ch another-ch] :default :not-ready)]
      (if (and (= value :not-ready)
               (= ch :default))
        (println [:a] "No operation is ready, aborting")
        (println [:a] "Got" value))))

  (put! a-ch 42)

)


;;;; combinators

;; `pipe`

(comment

  ;; `pipe` takes an input and output channels and pipes all the values put on
  ;; the input channel to the output one. The output channel is closed whenever
  ;; the source is closed unless we provide a `false` third argument:

  (def in (chan))
  (def out (chan))
  (pipe in out)

  (go-loop [value (<! out)]
    (if (nil? value)
      (println [:a] "I'm done!")
      (do
        (println [:a] "Got" value)
        (println [:a] "Waiting for a value")
        (recur (<! out)))))

  (put! in 0)
  (put! in 1)
  (close! in)

  ;; we used the `go-loop` macro for reading values recursively until the `out`
  ;; channel is closed. Notice that when we close the `in` channel the `out`
  ;; channel is closed too, making the `go-loop` terminate.

)


;; `pipeline-async`

(comment

  ;; `pipeline-async` takes a number for controlling parallelism, an output
  ;; channel, an asynchronous function and an input channel. The asynchronous
  ;; function has two arguments: the value put in the input channel and a
  ;; channel where it should put the result of its asynchronous operation,
  ;; closing the result channel after finishing. The number controls the number
  ;; of concurrent go blocks that will be used for calling the asynchronous
  ;; function with the inputs.

  ;; The output channel will receive outputs in an order relative to the input
  ;; channel, regardless the time each asynchronous function call takes to
  ;; complete. It has an optional last parameter that controls whether the output
  ;; channel will be closed when the input channel is closed, which defaults to
  ;; true.

  (def in (chan))
  (def out (chan))
  (def parallelism 3)

  (defn wait-and-put [value ch]
    (let [wait (rand-int 1000)]
      (js/setTimeout (fn []
                       (println "Waiting" wait "miliseconds for value" value)
                       (put! ch wait)
                       (close! ch))
                     wait)))

  (pipeline-async parallelism out wait-and-put in)

  (go-loop [value (<! out)]
    (if (nil? value)
      (println [:a] "I'm done!")
      (do
        (println [:a] "Got" value)
        (println [:a] "Waiting for a value")
        (recur (<! out)))))

  (put! in 1)
  (put! in 2)
  (put! in 3)
  (close! in)

)


;; `pipeline`

(comment

  ;; `pipeline` is similar to `pipeline-async` but instead of taking and
  ;; asynchronous function it takes a transducer instead. The transducer will be
  ;; applied independently to each input.

  (def in (chan))
  (def out (chan))
  (def parallelism 3)

  (pipeline parallelism out (map inc) in)

  (go-loop [value (<! out)]
    (if (nil? value)
      (println [:a] "I'm done!")
      (do
        (println [:a] "Got" value)
        (println [:a] "Waiting for a value")
        (recur (<! out)))))

  (put! in 1)
  (put! in 2)
  (put! in 3)
  (close! in)

)


;; `split`

(comment

  ;; `split` takes a predicate and a channel and returns a vector with two
  ;; channels, the first of which will receive the values for which the
  ;; predicate is true, the second will receive those for which the predicate is
  ;; false. We can optionally pass a buffer or number for the channels with the
  ;; third (true channel) and fourth (false channel) arguments.

  (def in (chan))
  (def chans (split even? in))
  (def even-ch (first chans))
  (def odd-ch (second chans))

  (go-loop [value (<! even-ch)]
    (if (nil? value)
      (println [:evens] "I'm done!")
      (do
        (println [:evens] "Got" value)
        (println [:evens] "Waiting for a value")
        (recur (<! even-ch)))))

  (go-loop [value (<! odd-ch)]
    (if (nil? value)
      (println [:odds] "I'm done!")
      (do
        (println [:odds] "Got" value)
        (println [:odds] "Waiting for a value")
        (recur (<! odd-ch)))))

  (put! in 0)
  (put! in 1)
  (put! in 2)
  (put! in 3)
  (close! in)

)


;; `map`

(comment

  ;; `async/map` takes a function and a collection of source channels, and
  ;; returns a channel which contains the values produced by applying f to the
  ;; set of first items taken from each source channel, followed by applying f
  ;; to the set of second items from each channel, until any one of the channels
  ;; is closed, at which point the output channel will be closed. The returned
  ;; channel will be unbuffered by default, or a buf-or-n can be supplied.

  (def c1 (chan))
  (def c2 (chan))

  (def out (async/map + [c1 c2]))

  (go-loop [result (<! out)]
    (if (nil? result)
      (println "Mapping is done.")
      (do
        (println "Result is" result)
        (recur (<! out)))))

  (do
    (put! c1 1)
    (put! c2 2))

  (do
    (put! c1 12)
    (put! c2 13))

  ;; to automatically close the out channel, all source channels must yield
  ;; either a value or sentinel nil (a take from all of them must be completed);
  ;; then if there any nils present in results, mapping is stopped and out
  ;; channel is closed.

  ;; autoclosing the `out`, variant 1
  (do
    (close! c1)
    (put! c1 24)
    (put! c2 23))

  ;; autoclosing the `out`, variant 2
  (do
    (close! c1)
    (close! c2))

)


;; `reduce`

(comment

  ;; `async/reduce` takes a reducing function, initial value and an input
  ;; channel. It returns a channel with the result of reducing over all the
  ;; values put on the input channel before closing it using the given initial
  ;; value as the seed.

  (def in (chan))

  (go
    (println "Result" (<! (async/reduce + (+) in))))

  (do (put! in 0)
      (put! in 1)
      (put! in 2)
      (put! in 3)
      (close! in))

)


;; `transduce`

(comment

  ;; `async/transduce` async/reduces a channel with a transformation (xform f).
  ;; Returns a channel containing the result. ch must close before transduce
  ;; produces a result.

  (def in (chan))
  (def xform (map inc))

  (go
    (println "Result" (<! (async/transduce xform + (+) in))))

  (do (put! in 0)
      (put! in 1)
      (put! in 2)
      (put! in 3)
      (close! in))

)


;; `onto-chan!`

(comment

  ;; `onto-chan!` takes a channel and a collection and puts the contents of the
  ;; collection into the channel. It closes the channel after finishing although
  ;; it accepts a third argument for specifying if it should close it or
  ;; not. Let’s rewrite the reduce example using onto-chan:

  (def in (chan))

  (go
    (println "Result" (<! (async/reduce + (+) in))))

  (onto-chan! in [0 1 2 3])

)


;; `to-chan!`

(comment

  ;; `to-chan!` takes a collection and returns a channel where it will put every
  ;; value in the collection, closing the channel afterwards.

  (def ch (to-chan! (range 3)))

  (go-loop [value (<! ch)]
    (if (nil? value)
      (println [:a] "I'm done!")
      (do
        (println [:a] "Got" value)
        (println [:a] "Waiting for a value")
        (recur (<! ch)))))

)


;; `into`

(comment

  ;; `async/into` returns a channel containing the single (collection) result of
  ;; the items taken from the channel conjoined to the supplied collection. ch
  ;; must close before into produces a result.

  (def in (chan))
  (def out (into [1] in))

  (go-loop [result (<! out)]
    (if (nil? result)
      (println "Done.")
      (do
        (println "Result:" result)
        (recur (<! out)))))

  (do
    (put! in 1)
    (put! in 12)
    (close! in))

)

;; `merge`

(comment

  ;; `merge` takes a collection of input channels and returns a channel where it
  ;; will put every value that is put on the input channels. The returned
  ;; channel will be closed when all the input channels have been closed. The
  ;; returned channel will be unbuffered by default but a number or buffer can
  ;; be provided as the last argument.


  (def in1 (chan))
  (def in2 (chan))
  (def in3 (chan))

  (def out (merge [in1 in2 in3]))

  (go-loop [value (<! out)]
    (if (nil? value)
      (println [:a] "I'm done!")
      (do
        (println [:a] "Got" value)
        (println [:a] "Waiting for a value")
        (recur (<! out)))))

  (put! in1 1)
  (close! in1)
  (put! in2 2)
  (close! in2)
  (put! in3 3)
  (close! in3)

)


;;;; higher-level abstractions

;; `mult`

(comment

  ; whenever we have a channel whose values have to be broadcasted to many
  ; others, we can use `mult` for creating a multiple of the supplied
  ; channel. Once we have a mult, we can attach channels to it using `tap` and
  ; dettach them using `untap`. Mults also support removing all tapped channels
  ; at once with `untap-all`.

  ;; every value put in the source channel of a mult is broadcasted to all the
  ;; tapped channels, and all of them must accept it before the next item is
  ;; broadcasted. For preventing slow takers from blocking the mult’s values we
  ;; must use buffering on the tapped channels judiciously.

  ;; closed tapped channels are removed automatically from the mult. When
  ;; putting a value in the source channels when there are still no taps such
  ;; value will be dropped.

  ;; source channel and mult
  (def in (chan))
  (def m-in (mult in))

  ;; sink channels
  (def a-ch (chan))
  (def another-ch (chan))

  ;; taker for `a-ch`
  (go-loop [value (<! a-ch)]
    (if (nil? value)
      (println [:a] "I'm done!")
      (do
        (println [:a] "Got" value)
        (recur (<! a-ch)))))

  ;; taker for `another-ch`, which sleeps for 3 seconds between takes
  (go-loop [value (<! another-ch)]
    (if (nil? value)
      (println [:b] "I'm done!")
      (do
        (println [:b] "Got" value)
        (println [:b] "Resting 3 seconds")
        (<! (timeout 3000))
        (recur (<! another-ch)))))

  ;; tap the two channels to the mult
  (tap m-in a-ch)
  (tap m-in another-ch)

  ;; see how the values are delivered to `a-ch` and `another-ch`
  (put! in 1)
  (put! in 2)

)


;; pub-sub

(comment

  ;; after learning about mults you could imagine how to implement a pub-sub
  ;; abstraction on top of `mult`, `tap` and `untap` but since it’s a widely
  ;; used communication mechanism, `core.async` already implements this
  ;; functionality.

  ;; instead of creating a mult from a source channel, we create a publication
  ;; with `pub` giving it a channel and a function that will be used for
  ;; extracting the topic of the messages.

  ;; we can subscribe to a publication with `sub`, giving it the publication we
  ;; want to subscribe to, the topic we are interested in and a channel to put
  ;; the messages that have the given topic. Note that we can subscribe a
  ;; channel to multiple topics.

  ;; `unsub` can be given a publication, topic and channel for unsubscribing
  ;; such channel from the topic. `unsub-all` can be given a publication and a
  ;; topic to unsubscribe every channel from the given topic.

  ;; source channel and publication
  (def in (chan))
  (def publication (pub in :action))

  ;; sink channels
  (def a-ch (chan))
  (def another-ch (chan))

  ;; channel with `:increment` action
  (sub publication :increment a-ch)

  (go-loop [value (<! a-ch)]
    (if (nil? value)
      (println [:a] "I'm done!")
      (do
        (println [:a] "Increment:" (inc (:value value)))
        (recur (<! a-ch)))))

  ;; channel with `:double` action
  (sub publication :double another-ch)

  (go-loop [value (<! another-ch)]
    (if (nil? value)
      (println [:b] "I'm done!")
      (do
        (println [:b] "Double:" (* 2 (:value value)))
        (recur (<! another-ch)))))

  ;; see how values are delivered to `a-ch` and `another-ch` depending on their
  ;; action
  (put! in {:action :increment :value 98})
  (put! in {:action :double :value 21})

)


;; mixer

(comment

  ;; as we learned in the section about `core.async` combinators, we can use the
  ;; `merge` function for combining multiple channels into one. When merging
  ;; multiple channels, every value put in the input channels will end up in the
  ;; merged channel. However, we may want more finer-grained control over which
  ;; values put in the input channels end up in the output channel, that’s where
  ;; mixers come in handy.

  ;; `core.async` gives us the `mixer` abstraction, which we can use to combine
  ;; multiple input channels into an output channel. The interesting part of the
  ;; mixer is that we can mute, pause and listen exclusively to certain input
  ;; channels.

  ;; we can create a mixer given an output channel with `mix`. Once we have a
  ;; mixer, we can add input channels into the mix using `admix`, remove it
  ;; using `unmix` or remove every input channel with `unmix-all`.

  ;; for controlling the state of the input channel we use the `toggle` function
  ;; giving it the mixer and a map from channels to their states. Note that we
  ;; can add channels to the mix using `toggle`, since the map will be merged
  ;; with the current state of the mix. The state of a channel is a map which
  ;; can have the keys `:mute`, `:pause` and `:solo` mapped to a boolean.

  ;; let’s see what muting, pausing and soloing channels means:

  ;; - a muted input channel means that, while still taking values from it, they
  ;;   won’t be forwarded to the output channel. Thus, while a channel is muted,
  ;;   all the values put in it will be discarded.

  ;; - a paused input channel means that no values will be taken from it. This
  ;;   means that values put in the channel won’t be forwarded to the output
  ;;   channel nor discarded.

  ;; - when soloing one or more channels the output channel will only receive
  ;;   the values put in soloed channels. By default non-soloed channels are
  ;;   muted but we can use `solo-mode` to decide between muting or pausing
  ;;   non-soloed channels.

  ;; that was a lot of information, so let’s see an example to improve our
  ;; understanding. First of all, we'll set up a mixer with an out channel and
  ;; add three input channels to the mix. After that, we'll be printing all the
  ;; values received on the out channel to illustrate the control over input
  ;; channels:

  ;; Output channel and mixer
  (def out (chan))
  (def mixer (mix out))

  ;; Input channels
  (def in-1 (chan))
  (def in-2 (chan))
  (def in-3 (chan))

  (admix mixer in-1)
  (admix mixer in-2)
  (admix mixer in-3)

  ;; let's listen to the `out` channel and print what we get from it
  (go-loop [value (<! out)]
    (if (nil? value)
      (println [:a] "I'm done")
      (do
        (println [:a] "Got" value)
        (recur (<! out)))))

  ;; by default, every value put in the input channels will be put in the out
  ;; channel:

  (do
    (put! in-1 1)
    (put! in-2 2)
    (put! in-3 3))

  ;; let’s pause the `in-2` channel, put a value in every input channel and
  ;; resume `in-2`:

  (toggle mixer {in-2 {:pause true}})

  (do
    (put! in-1 1)
    (put! in-2 2)
    (put! in-3 3))

  (toggle mixer {in-2 {:pause false}})

  ;; as you can see in the example above, the values put in the paused channels
  ;; aren’t discarded. For discarding values put in an input channel we have to
  ;; mute it, let’s see an example:

  (toggle mixer {in-2 {:mute true}})

  (do
    (put! in-1 1)
    (put! in-2 2)  ; `out` will never get this value since it's discarded
    (put! in-3 3))

  (toggle mixer {in-2 {:mute false}})

  ;; we put a value 2 in the `in-2` channel and, since the channel was muted at
  ;; the time, the value is discarded and never put into `out`. Let’s look at
  ;; the third state a channel can be inside a mixer: solo.

  ;; as we mentioned before, soloing channels of a mixer implies muting the rest
  ;; of them by default:

  (toggle mixer {in-1 {:solo true}
                 in-2 {:solo true}})

  (do
    (put! in-1 1)
    (put! in-2 2)
    (put! in-3 3)) ; `out` will never get this value since it's discarded

  (toggle mixer {in-1 {:solo false}
                 in-2 {:solo false}})

  ;; however, we can set the mode the non-soloed channels will be in while there
  ;; are soloed channels. Let’s set the default non-solo mode to pause instead
  ;; of the default mute:

  (solo-mode mixer :pause)

  (toggle mixer {in-1 {:solo true}
                 in-2 {:solo true}})

  (do
    (put! in-1 1)
    (put! in-2 2)
    (put! in-3 3))

  (toggle mixer {in-1 {:solo false}
                 in-2 {:solo false}})

)
