# hello-cljs-core-async

Using `core.async` in ClojureScript.

## Getting Started

Start cljs REPL in terminal

```
clj -M -m cjls.main
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
