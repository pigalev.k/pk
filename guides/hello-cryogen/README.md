# hello-cryogen

Exploring cryogen --- a simple static site generator written in Clojure.

- https://github.com/cryogen-project/cryogen

Note: local sources may use outdated version of Cryogen.

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
