# hello-muuntaja

Exploring Muuntaja --- Clojure library for fast HTTP format negotiation,
encoding and decoding.

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
