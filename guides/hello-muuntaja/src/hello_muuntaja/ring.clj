(ns hello-muuntaja.ring
  (:require
   [cognitect.transit :as transit]
   [muuntaja.core :as m]
   [muuntaja.middleware :as mw]))


;; usage with Ring

;; https://cljdoc.org/d/metosin/muuntaja/0.6.8/doc/with-ring

(comment

  ;; simplest thing that works

  ;; ring application that can read and write JSON, EDN and Transit:

  (defn handler [_]
    {:status 200
     :body {:ping "pong"}})

  ;; with defaults

  (def app (mw/wrap-format handler))

  (def request {:headers {"accept" "application/json"}})

  (->> request app)
  (->> request app :body slurp)


  ;; with Muuntaja instance

  ;; like previous, but with custom Transit options and a standalone Muuntaja:

  ;; custom Record

  (defrecord Ping [])

  ;; custom transit handlers

  (def write-handlers
    {Ping (transit/write-handler (constantly "Ping") (constantly {}))})

  (def read-handlers
    {"Ping" (transit/read-handler map->Ping)})

  ;; a configured Muuntaja

  (def muuntaja
    (m/create
     (update-in
      m/default-options
      [:formats "application/transit+json"]
      merge
      {:encoder-opts {:handlers write-handlers}
       :decoder-opts {:handlers read-handlers}})))

  (defn endpoint [_]
    {:status 200
     :body {:ping (->Ping)}})

  (def app (-> endpoint (mw/wrap-format muuntaja)))

  (def request {:headers {"accept" "application/transit+json"}})

  (->> request app)
  (->> request app :body slurp)
  (->> request app :body (m/decode muuntaja "application/transit+json"))


  ;; middleware chain

  ;; Muuntaja doesn't catch formatting exceptions itself, but throws them
  ;; instead. If you want to format those also, you need to split the
  ;; `wrap-format` into parts.

  ;; this:

  (-> app (mw/wrap-format muuntaja))

  ;; can be written as:

  (-> app
      ;; format the request
      (mw/wrap-format-request muuntaja)
      ;; format the response
      (mw/wrap-format-response muuntaja)
      ;; negotiate the request & response formats
      (mw/wrap-format-negotiate muuntaja))

  ;; now you can add your own exception-handling middleware between the
  ;; `wrap-format-request` and `wrap-format-response`. It can catch the
  ;; formatting exceptions and it's results are written with the response
  ;; formatter.

  ;; here's a "complete" stack:

  '(-> app
       ;; support for `:params`
       (mw/wrap-params)
       ;; format the request
       (mw/wrap-format-request muuntaja)
       ;; catch exceptions
       (mw/wrap-exceptions my-exception-handler)
       ;; format the response
       (mw/wrap-format-response muuntaja)
       ;; negotiate the request & response formats
       (mw/wrap-format-negotiate muuntaja))

  ;; see example of real-life use from compojure-api:
  ;; https://github.com/metosin/compojure-api/blob/master/src/compojure/api/middleware.clj. It
  ;; also reads the `:produces` and `:consumes` from Muuntaja instance and
  ;; passed them to Swagger docs. `:params`-support is needed to allow compojure
  ;; destucturing syntax.

)
