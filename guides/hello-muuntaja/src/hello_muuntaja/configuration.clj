(ns hello-muuntaja.configuration
  (:require
   [cognitect.transit :as t]
   [muuntaja.core :as muuntaja])
  (:import (java.util Date)))


;; configuration

;; https://cljdoc.org/d/metosin/muuntaja/0.6.8/doc/configuration

(comment

  ;; Muuntaja is data-driven, allowing mostly everything to be defined via
  ;; options. Muuntaja is created with `muuntaja.core/create` function. It takes
  ;; either an existing Muuntaja instance, options-map or nothing as a
  ;; parameter.


  ;; examples

  ;; with defaults
  (def m (muuntaja/create))

  ;; with a muuntaja as prototype (no-op)
  (muuntaja/create m)

  ;; with default options (same features, different instance)
  (muuntaja/create muuntaja/default-options)


  ;; default options

  '{:http {:extract-content-type extract-content-type-ring
           :extract-accept-charset extract-accept-charset-ring
           :extract-accept extract-accept-ring
           :decode-request-body? (constantly true)
           :encode-response-body? encode-collections}

    :allow-empty-input? true
    :return :input-stream

    :default-charset "utf-8"
    :charsets available-charsets

    :default-format "application/json"
    :formats
    {"application/json" json-format/json-format
     "application/edn" edn-format/edn-format
     "application/transit+json" transit-format/transit-json-format
     "application/transit+msgpack" transit-format/transit-msgpack-format}}


  ;; custom options

  ;; as options are just data, normal Clojure functions can be used to modify
  ;; them. Options are sanity checked at creation time. There are also the
  ;; following helpers in the `muuntaja.core`:

  ;; - `select-formats`: takes a sequence of formats selecting them and setting
  ;;   the first one as default format.
  ;; - `transform-formats`: takes an 2-arity function [format, format-options],
  ;;   which returns new format-options for that format. Returning nil removes
  ;;   that format. Called for all registered formats.


  ;; examples


  ;; modifying JSON encoding opts

  ;; easiest way is to add a `:encoder-opts` key to the format with the options
  ;; map as the value.

  (def m
    (muuntaja/create
     (assoc-in
      muuntaja/default-options
      [:formats "application/json" :encoder-opts]
      {:encode-key-fn #(.toUpperCase (name %))})))

  (slurp (muuntaja/encode m "application/json" {:kikka 42}))

  ;; check `jsonista` object-mapper docs for available options.


  ;; setting Transit writers and readers

  (def m
    (muuntaja/create
     (update-in
      muuntaja/default-options
      [:formats "application/transit+json"]
      merge {:decoder-opts
             {:handlers
              {"date" (t/read-handler (fn [[y m d]]
                                        (Date. y m d)))}}
             :encoder-opts
             {:handlers
              {Date (t/write-handler "date" (fn [d]
                                              [(.getYear d)
                                               (.getMonth d)
                                               (.getDay d)]))}}})))

  (def transit-with-dates
    (slurp (muuntaja/encode m
                            "application/transit+json"
                            {:kikka 42 :merkkipaalu (Date.)})))

  (muuntaja/decode m "application/transit+json" transit-with-dates)


  ;; `:decoder-opts` is passed to reader and `:encoder-opts` to writer.


  ;; using only selected formats

  ;; supporting only "application/edn" and "application/transit+json" formats.

  (def m
    (muuntaja/create
     (-> muuntaja/default-options
         (update
          :formats
          select-keys
          ["application/edn"
           "application/transit+json"]))))

  ;; ups. That didn't work. The `:default-format` is now illegal. This works:

  (def m
    (muuntaja/create
     (-> muuntaja/default-options
         (update
          :formats
          select-keys
          ["application/edn"
           "application/transit+json"])
         (assoc :default-format "application/edn"))))

  ;; same with `select-formats`:

  (def m
    (muuntaja/create
     (muuntaja/select-formats
      muuntaja/default-options
      ["application/edn"
       "application/json"])))

  (:default-format (muuntaja/options m))


  ;; disabling decoding

  ;; we have to remove `:decoder` key from all formats.

  (def m
    (muuntaja/create
     (update
      muuntaja/default-options
      :formats
      #(into
        (empty %)
        (map (fn [[k v]]
               [k (dissoc v :decoder)]) %)))))

  (muuntaja/encoder m "application/json")
  (muuntaja/decoder m "application/json")

  ;; same with `transform-formats`:

  (def m
    (muuntaja/create
     (muuntaja/transform-formats
      muuntaja/default-options
      #(dissoc %2 :encoder))))


  ;; using streaming JSON and Transit encoders

  ;; to be used with Ring 1.6.0+, with Pedestal & other streaming libs. We have
  ;; to change the return types of the given formats:

  (def m (muuntaja/create
          (-> muuntaja/default-options
              (assoc-in [:formats "application/json" :return]
                        :output-stream)
              (assoc-in [:formats "application/transit+json" :return]
                        :output-stream))))

  (muuntaja/encode m "application/json" {:kikka 2})

  (slurp (muuntaja/encode m "application/json" {:kikka 2}))


  ;; loose matching on Content-Type

  ;; if, for some reason, you want use the RegExps to match the
  ;; Content-Type (like `ring-json`, `ring-transit` & `ring-middleware-format`
  ;; do, you need just to add a `:matches` key to format with a regexp as a
  ;; value. The following procudes loose matching like in
  ;; ring-middleware-format:

  (def m
    (muuntaja/create
     (-> muuntaja/default-options
         (assoc-in [:formats "application/json" :matches]
                   #"^application/(.+\+)?json$")
         (assoc-in [:formats "application/edn" :matches]
                   #"^application/(vnd.+)?(x-)?(clojure|edn)$")
         #_(assoc-in [:formats "application/msgpack" :matches]
                   #"^application/(vnd.+)?(x-)?msgpack$")
         #_(assoc-in [:formats "application/x-yaml" :matches]
                   #"^(application|text)/(vnd.+)?(x-)?yaml$")
         (assoc-in [:formats "application/transit+json" :matches]
                   #"^application/(vnd.+)?(x-)?transit\+json$")
         (assoc-in [:formats "application/transit+msgpack" :matches]
                   #"^application/(vnd.+)?(x-)?transit\+msgpack$"))))

  (muuntaja/negotiate-request-response
   m
   {:body "{\"foo\": 123}"
    :headers {"accept" "application/vnd.foobar+json; charset=utf-8"}})


  ;; creating a new format

  ;; see Creating new formats.


  ;; implicit configuration

  ;; currently, both cheshire & clojure-msgpack allow new type encoders to be
  ;; defined via Clojure protocol extensions. Importing a namespace could bring
  ;; new type mappings or override existings without a warning, potentially
  ;; breaking things. Ordering of the imports should not matter!

  ;; TODO
  ;; - make all options explicit. See:
  ;;   https://github.com/dakrone/cheshire/issues/7

)
