(ns hello-muuntaja.new-formats
  (:require
   [clojure.string :as str]
   [muuntaja.core :as muuntaja]
   [muuntaja.format.json :as json-format]
   [muuntaja.format.transit :as transit-format]))


;; creating new formats

;; https://cljdoc.org/d/metosin/muuntaja/0.6.8/doc/creating-new-formats

(comment

  ;; formats are presented as Clojure maps, registered into options under
  ;; `:formats` with format name as a key. Format maps can have the following
  ;; optional keys:
  ;; - `:decoder`: a function (or a function generator) to parse an `InputStream`
  ;;   into Clojure data structure. If the key is missing or value is nil, no
  ;;   decoding will be done.
  ;; - `:encoder`: a function (or a function generator) to encode Clojure data
  ;;   structures into an `InputStream` or to
  ;;   `muuntaja.protocols/IntoInputStream`. If the key is missing or value is
  ;;   nil, no encoding will be done.
  ;; - `:opts`: extra options maps for both the decoder and encoder function
  ;;   generator.
  ;; - `:decoder-opts`: extra options maps for the decoder function generator.
  ;; - `:encoder-opts`: extra options maps for the encoder function generator.
  ;; - `:matches`: a regexp for additional matching of the Content-Type in
  ;;   request negotiation. Added for legacy support, e.g.
  ;;   #"^application/(.+\+)?json$". Results of the regexp are memoized against
  ;;   the given Content-Type for near constant-time performance.


  ;; function generators

  ;; to allow easier customization of the formats on the client side, function
  ;; generators can be used instead of plain functions. Function generators have
  ;; a Duct/Reagent-style vector presentations with the generator function &
  ;; optionally the default opts for it.

  {:decoder [json-format/decoder]}
  ;; => (json-format/decoder {})

  {:decoder [json-format/decoder {:decode-key-fn true}]}
  ;; => (json-format/decoder {:decode-key-fn true})

  ;; clients can override format options by providing `:decoder-opts` or
  ;; `:encoder-opts`. These get merged over the default opts.

  {:decoder [json-format/decoder {:decode-key-fn true}]
   :decoder-opts {:bigdecimals? true}}
   ;; => (json-format/decoder
   ;;     {:decode-key-fn true, :bigdecimals? true})


  ;; example of a new format

  (def streaming-upper-case-json-format
    {:decoder [json-format/decoder
               {:keywords? false, :decode-key-fn str/upper-case}]
     :encoder [json-format/encoder]
     :return :output-stream})

  (def m
    (muuntaja/create
     (assoc-in
      muuntaja/default-options
      [:formats "application/upper-json"]
      streaming-upper-case-json-format)))

  (->> {:kikka 42}
       (muuntaja/encode m "application/json")
       (muuntaja/decode m "application/upper-json"))

)
