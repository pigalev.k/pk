(ns hello-muuntaja.core
  (:require
   [camel-snake-kebab.core :as csk]
   [muuntaja.core :as m]
   [muuntaja.middleware :as middleware]))


;; Muuntaja

;; https://cljdoc.org/d/metosin/muuntaja/0.6.8/doc/readme


;; intro

(comment

  ;; standalone

  ;; use default Muuntaja instance to encode & decode JSON:

  (->> {:kikka 42}
       (m/encode "application/json"))

  (->> {:kikka 42}
       (m/encode "application/json")
       slurp)

  (->> {:kikka 42}
       (m/encode "application/json")
       (m/decode "application/json"))

  ;; Ring

  ;; automatic decoding of request body and response body encoding based on
  ;; "Content-Type", "Accept" and "Accept-Charset" headers:

  (defn echo [request]
    {:status 200
     :body (:body-params request)})

  (def app (middleware/wrap-format echo))

  (def request
    {:headers
     {"content-type" "application/edn"
      "accept" "application/transit+json"}
     :body "{:kikka 42}"})

  (app request)

  ;; automatic decoding of response body based on "Content-Type" header:

  (-> request app m/decode-response-body)

  ;; there is a more detailed Ring guide too (see With Ring).


  ;; interceptors

  ;; Muuntaja support Sieppari-style interceptors too. See
  ;; `muuntaja.interceptor` for details.

  ;; interceptors can be used with Pedestal too, all but the
  ;; exception-interceptor which conforms to the simplified exception handling
  ;; model of Sieppari.

  ;; configuration

  ;; explicit Muuntaja instance with custom EDN decoder options:

  (def m
    (m/create
     (assoc-in
      m/default-options
      [:formats "application/edn" :decoder-opts]
      {:readers {'INC inc}})))

  (->> "{:value #INC 41}"
       (m/decode m "application/edn"))

  ;; explicit Muuntaja instance with custom date formatter:

  (def m
    (m/create
     (assoc-in
      m/default-options
      [:formats "application/json" :encoder-opts]
      {:date-format "yyyy-MM-dd"})))

  (->> {:value (java.util.Date.)}
       (m/encode m "application/json")
       slurp)

  ;; explicit Muuntaja instance with camelCase `:encode-key-fn`:

  (def m
    (m/create
     (assoc-in
      m/default-options
      [:formats "application/json" :encoder-opts]
      {:encode-key-fn csk/->camelCase})))

  (->> {:some-property "some-value"}
       (m/encode m "application/json")
       slurp)

  ;; returning a function to encode transit-json:

  (def encode-transit-json
    (m/encoder m "application/transit+json"))

  (slurp (encode-transit-json {:kikka 42}))


  ;; encoding format

  ;; by default, `encode` writes value into a `java.io.ByteArrayInputStream`.
  ;; This can be changed with a `:return` option, accepting the following values:
  ;; - `:input-stream`: encodes into `java.io.ByteArrayInputStream` (default)
  ;; - `:bytes`: encodes into `byte[]`. Faster than Stream, enables NIO for
  ;;   servers supporting it
  ;; - `:output-stream`: encodes lazily into `java.io.OutputStream` via a
  ;;   callback function

  ;; all return types satisfy the following protocols & interfaces:

  ;; - `ring.protocols.StreamableResponseBody`, Ring 1.6.0+ will stream these
  ;;   for you
  ;; - `clojure.io.IOFactory`, so you can slurp the response


  ;; `:input-stream`

  (def m (m/create (assoc m/default-options :return :input-stream)))

  (->> {:kikka 42}
       (m/encode m "application/json"))


  ;; `:bytes`

  (def m (m/create (assoc m/default-options :return :bytes)))

  (->> {:kikka 42}
       (m/encode m "application/json"))


  ;; `:output-stream`

  (def m (m/create (assoc m/default-options :return :output-stream)))

  (->> {:kikka 42}
       (m/encode m "application/json"))


  ;; format-based return

  (def m (m/create
          (assoc-in m/default-options
                    [:formats "application/edn" :return] :output-stream)))

  (->> {:kikka 42}
       (m/encode m "application/json"))

  (->> {:kikka 42}
       (m/encode m "application/edn"))


  ;; HTTP format negotiation

  ;; HTTP format negotiation is done using request headers for both
  ;; request ("Content-Type", including the charset) and response ("Accept" and
  ;; "Accept-Charset"). With the default options, a full match on the
  ;; "Content-Type" is required, e.g. "application/json". Adding a `:matches`
  ;; regexp for formats enables more loose matching. See Configuration for more
  ;; info.

  ;; results of the negotiation are published into request & response under
  ;; namespaced keys for introspection. These keys can also be set manually,
  ;; overriding the content negotiation process.


  ;; exceptions

  ;; when something bad happens, a typed exception is thrown. You should handle
  ;; it elsewhere. Thrown exceptions have an `ex-data` with the following
  ;; `:type` value (plus extra info to enable generating descriptive errors to
  ;; clients):

  ;; - `:muuntaja/decode`, input can't be decoded with the negotiated format
  ;;   & charset.
  ;; - `:muuntaja/request-charset-negotiation`, request charset is illegal.
  ;; - `:muuntaja/response-charset-negotiation`, could not negotiate a charset
  ;;   for the response.
  ;; - `:muuntaja/response-format-negotiation`, could not negotiate a format for
  ;;   the response.


  ;; server spec

  ;; request

  ;; - `:muuntaja/request`, client-negotiated request format and charset as
  ;;   `FormatAndCharset` record. Will be used in the response pipeline.
  ;; - `:muuntaja/response`, client-negotiated response format and charset
  ;;   as `FormatAndCharset` record. Will be used in the response pipeline.
  ;; - `:body-params` contains the decoded body.

  ;; response

  ;; - `:muuntaja/encode`, if set to truthy value, the response body will be
  ;;   encoded regardles of the type (primitives!)
  ;; - `:muuntaja/content-type`, handlers can use this to override the
  ;;   negotiated content-type for response encoding, e.g. setting it to
  ;;   "application/edn" will cause the response to be formatted in EDN.


  ;; options

  ;; default options

  '{:http {:extract-content-type extract-content-type-ring
           :extract-accept-charset extract-accept-charset-ring
           :extract-accept extract-accept-ring
           :decode-request-body? (constantly true)
           :encode-response-body? encode-collections}

    :allow-empty-input? true
    :return :input-stream

    :default-charset "utf-8"
    :charsets available-charsets

    :default-format "application/json"
    :formats
    {"application/json" json-format/json-format
     "application/edn" edn-format/edn-format
     "application/transit+json" transit-format/transit-json-format
     "application/transit+msgpack" transit-format/transit-msgpack-format}}

)
