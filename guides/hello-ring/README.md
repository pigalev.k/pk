# hello-ring

Exploring Ring --- Clojure HTTP server abstraction.

- https://github.com/ring-clojure/ring

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.

## Starting Ring Hello Server

```
clojure -M:start
```

## Building and running Ring Hello Server uberjar

Build

```
clojure -X:uberjar
```

Run

```
java -jar target/hello-ring.jar
```

Use

See `resources/public/api.org`.
