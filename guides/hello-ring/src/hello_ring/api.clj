(ns hello-ring.api
  (:require
   [clojure.string :as string]
   [ring.handler.dump :as dump]
   [ring.middleware.json :as mjson]
   [ring.middleware.params :as p]
   [ring.mock.request :as mock]
   [ring.util.io :as rio]
   [ring.util.mime-type :as mime]
   [ring.util.parsing :as parsing]
   [ring.util.request :as req]
   [ring.util.response :as resp]
   [ring.util.time :as rut])
  (:import
   (java.net URL)
   (java.util Date)))


;; Ring API (1.9.6).

;; - specification: https://github.com/ring-clojure/ring/blob/master/SPEC
;; - API docs:
;;   - https://ring-clojure.github.io/ring/index.html"


;; `ring.util.request`

  (defn make-request
    "Create a request map."
    []
    {:server-port 3000
     :server-name "localhost"
     :remote-addr "192.168.0.123"
     :uri "messages"
     :query-string "id=123&sort=asc"
     :scheme :https
     :request-method :post
     :protocol "HTTP/1.1"
     :ssl-client-cert nil
     :headers {"content-type" "application/json; charset=utf-8"
               "content-length" "1234"
               "location" "https://localhost:3000/message/123"}
     :body (rio/string-input-stream "{\"user\": \"Joe\"}")})

(comment

  (def request (make-request))
  (req/body-string request)
  (req/character-encoding request)
  (req/content-length request)
  (req/content-type request)
  (req/in-context? request "/prefix")
  (req/path-info request)
  (req/request-url request)
  (req/set-context request "mess")
  (req/urlencoded-form? request)

)


;; `ring.util.response`

(comment

  (resp/bad-request "nope")
  (resp/charset {} "utf-9")
  (resp/content-type {} "application/json")
  (resp/created "http://localhost:3000/messages/123" {})
  (resp/file-response "deps.edn")
  (resp/find-header
   (resp/created "http://localhost:3000/messages/123" {}) "location")
  (resp/get-charset (resp/charset {} "utf-9"))
  (resp/get-header
   (resp/created "http://localhost:3000/messages/123" {}) "location")
  (resp/header {} "ololo" "trololo")
  (resp/not-found "ololo")
  resp/redirect-status-codes
  (resp/redirect "https://localhost:3000/mess" :see-other)
  (resp/resource-data (URL. "file://deps.edn"))
  (resp/resource-response "about.html")
  (resp/response "ololo")
  (resp/response? (resp/response "yar"))
  (resp/response? {})
  (resp/set-cookie {} "x-id" "19")
  (resp/status {} 400)
  (resp/update-header
   (resp/created "http://localhost:3000/messages/123" {})
   "location" string/upper-case)
  (resp/url-response (URL. "file://deps.edn"))

)


;; `ring.handler.dump`

(comment

  (def request (make-request))
  (dump/handle-dump request)

)


;; `ring.util.parsing`

(comment

  (def request (make-request))
  (parsing/find-content-type-charset (resp/get-header request "content-type"))

)


;; `ring.util.mime-type`

(comment

  mime/default-mime-types
  (mime/ext-mime-type ".pdf")

)


;; `ring.util.io`

(comment

  (def sis (rio/string-input-stream "lorem ipsum"))
  (.read sis)
  (rio/close! sis)

)


;; `ring.util.time`

(comment

  (rut/format-date (Date.))
  (rut/parse-date (rut/format-date (Date.)))

)


;; `ring.middleware.json`

(comment

  (def request (make-request))
  (mjson/json-body-request request {})
  (-> {:uri "hello"}
      (mock/query-string {:name "Molly"})
      (mock/json-body {:user "Joe"})
      (mjson/json-params-request {})
      (p/params-request {}))
  (mjson/json-response {:body {:user "Joe"}} {})

)
