(ns hello-ring.concepts
  (:require
   [ring.middleware.keyword-params :as kp]
   [ring.middleware.params :as p]))


;; Concepts

;; https://github.com/ring-clojure/ring/wiki/Concepts


;; A web application developed for Ring consists of four components:

;; - Handler
;; - Request
;; - Response
;; - Middleware


;; handlers

(comment

  ;; handlers are functions that define your web application. Synchronous
  ;; handlers take one argument, a map representing a HTTP request, and return a
  ;; map representing the HTTP response.

  (defn what-is-my-ip [request]
    {:status 200
     :headers {"Content-Type" "text/plain"}
     :body "Hello, World!"})

  (what-is-my-ip {:uri "/hello"})

  ;; this function returns a map that Ring can translate into a HTTP
  ;; response. The response returns a plain text file that contains the IP
  ;; address that was used to access the web application.

  ;; handlers may also be asynchronous. Handlers of this type take three
  ;; arguments: the request map, a response callback and an exception callback.

  (defn what-is-my-ip [request respond raise]
    (respond {:status 200
              :headers {"Content-Type" "text/plain"}
              :body (:remote-addr request)}))

  (what-is-my-ip {:uri "/hello" :remote-addr "127.0.0.1"}
                 println println)

  ;; because these two types of handler have different arities, we can combine
  ;; them into a handler that can be used synchronously or asynchronously:

  (defn what-is-my-ip
    ([request]
     {:status 200
      :headers {"Content-Type" "text/plain"}
      :body (:remote-addr request)})
    ([request respond raise]
     (respond (what-is-my-ip request))))

  (what-is-my-ip {:uri "/hello" :remote-addr "127.0.0.1"})
  (what-is-my-ip {:uri "/hello" :remote-addr "127.0.0.1"}
                 println println)


  ;; all official Ring middleware supports both types of handler, but for most
  ;; purposes synchronous handlers are sufficient.

  ;; the handler function can be converted into a web application through a
  ;; variety of different methods which will be covered in the next section.

)


;; requests

(comment

  ;; HTTP requests are represented by Clojure maps. There are a number of
  ;; standard keys that will always exist, but requests can (and often do)
  ;; contain custom keys added by middleware.

  ;; the standard keys are:

  ;; - `:server-port`: the port on which the request is being handled.
  ;; - `:server-name`: the resolved server name, or the server IP address.
  ;; - `:remote-addr`: the IP address of the client or the last proxy that
  ;;   sent the request.
  ;; - `:uri`: The request URI (the full path after the domain name).
  ;; - `:query-string`: the query string, if present.
  ;; - `:scheme`: the transport protocol, either :http or :https.
  ;; - `:request-method`: the HTTP request method, which is one of `:get`,
  ;;   `:head`, `:options`, `:put`, `:post`, or `:delete`.
  ;; - `:headers`: a Clojure map of lowercase header name strings to
  ;;   corresponding header value strings.
  ;; - `:body`: an InputStream for the request body, if present.

  ;; previous versions of Ring also had the following keys. These are now
  ;; DEPRECATED.

  ;; - `:content-type`: the MIME type of the request body, if known.
  ;; - `:content-length`: the number of bytes in the request body, if known.
  ;; - `:character-encoding`: the name of the character encoding used in the\
  ;;   request body, if known.

  (def request
    {:server-port    3000
     :server-name    "example.com"
     :remote-addr    "127.0.0.1"
     :uri            "/hello"
     :query-string   "?name=Joe"
     :scheme         :https
     :request-method :get
     :headers        {"accept" "application/json"}
     :body           nil})

)


;; responses

(comment

  ;; the response map is created by the handler, and contains three keys:

  ;; - `:status`: the HTTP status code, such as 200, 302, 404 etc.
  ;; - `:headers`: a Clojure map of HTTP header names to header values. These
  ;;   values may either be strings, in which case one name/value header will
  ;;   be sent in the HTTP response, or a collection of strings, in which case
  ;;   a name/value header will be sent for each value.
  ;; - `:body`: a representation of the response body, if a response body is
  ;;   appropriate for the response's status code.

  ;; the body can be one of four types:

  ;; - `String`: the body is sent directly to the client.
  ;; - `ISeq`: each element of the seq is sent to the client as a string.
  ;; - `File`: the contents of the referenced file is sent to the client.
  ;; - `InputStream`: the contents of the stream is sent to the client. When the
  ;;   stream is exhausted, the stream is closed.

  (def response
    {:status 200
     :headers {"content-type" "application/json"}
     :body "Hello, World!"})

)


;; middleware

(comment

  ;; middleware are higher-level functions that add functionality to
  ;; handlers. The first argument of a middleware function should be a handler,
  ;; and its return value should be a new handler function that will call the
  ;; original handler.

  (defn wrap-content-type [handler content-type]
    (fn [request]
      (let [response (handler request)]
        (assoc-in response [:headers "Content-Type"] content-type))))


  (defn handler [request] {:status 200 :body "Hello, World!"})

  (def app (-> handler (wrap-content-type "application/edn")))

  (app {:uri "/hello"})

  ;; this middleware function adds a "Content-Type" header to every response
  ;; generated by the handler. It will only work with synchronous handlers, but
  ;; we can extend it to support both synchronous and asynchronous handlers:

  (defn content-type-response [response content-type]
    (assoc-in response [:headers "Content-Type"] content-type))

  (defn wrap-content-type [handler content-type]
    (fn
      ([request]
       (-> (handler request) (content-type-response content-type)))
      ([request respond raise]
       (handler request
                #(respond (content-type-response % content-type)) raise))))


  (defn sync-handler [request]
    {:status 200 :body "Hello, World!"})
  (defn async-handler [request respond raise]
    (respond {:status 200 :body "Hello, World!"}))

  (def sync-app (-> sync-handler (wrap-content-type "application/edn")))

  (sync-app {:uri "/hello"})

  (def async-app (-> async-handler (wrap-content-type "application/edn")))

  (async-app {:uri "/hello"} println println)

  ;; notice that we factored out the common code that changes the response into
  ;; its own function. By convention, if wrap-foo is our middleware function,
  ;; then foo-request and foo-response are helper functions that operate on the
  ;; request and response.

  ;; once this middleware is written, it can be applied to a handler:

  (def app
    (wrap-content-type handler "text/html"))

  (app {:uri "/hello"})

  ;; this defines a new handler, app that consists of the handler handler with
  ;; the `wrap-content-type` middleware applied.

  ;; the threading macro `->` can be used to chain middleware together:

  (defn handler [request] {:status 200 :body request})

  (def app
    (-> handler
        (wrap-content-type "text/html")
        (kp/wrap-keyword-params)
        (p/wrap-params)))

  (app {:uri "/hello" :query-string "name=Joe"})

  ;; middleware is used often in Ring, and is used to provide much of its
  ;; functionality beyond handling raw HTTP requests. Parameters, sessions, and
  ;; file uploading are all handled by middleware in the Ring standard library.

  ;; Note: middleware runs from bottom to top, and if any middleware creates a
  ;; response, that will short-circuit and the middleware above will not be
  ;; called.

)
