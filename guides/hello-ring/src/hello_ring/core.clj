(ns hello-ring.core
  (:require
   [org.httpkit.server :as httpkit]
   [ring.adapter.jetty :as jetty])
  (:gen-class))


;; Getting started

;; https://github.com/ring-clojure/ring/wiki/Getting-Started


(defn handler
  "A Ring handler. Receives a request map and returns a response map."
  [request]
  {:status 200
   :headers {"Content-Type" "text/plain"}
   :body "Hello World"})


;; handle different servers with multimethods:
;; - can have only one server at a time (in this implementation)

(defonce server (atom nil))

(defmulti mm-start!
  "Start web-server with supplied handler and options.
  Use multimethods to handle different servers."
  (fn [_ & {:keys [type]}] type))

(defmethod mm-start! :jetty
  [handler & {:keys [port join?] :or {port 3000 join? false}}]
  (reset! server {:type   :jetty
                  :server (jetty/run-jetty handler {:port  port
                                                    :join? join?})}))

(defmethod mm-start! :httpkit
  [handler & {:keys [port] :or {port 3000}}]
  (reset! server {:type   :httpkit
                  :server (httpkit/run-server handler {:port port})}))

(defmethod mm-start! :default
  [_ & {:keys [type]}]
  (throw (ex-info "Cannot start: unknown server type" {:type type})))


(defmulti mm-stop!
  "Stop the web-server."
  (fn [server-atom] (:type @server-atom)))

(defmethod mm-stop! :jetty
  [server-atom]
  (when-let [{:keys [server]} @server-atom]
    (.stop server)
    (reset! server-atom nil)))

(defmethod mm-stop! :httpkit
  [server-atom]
  (when-let [{:keys [server]} @server-atom]
    (server :timeout 100)
    (reset! server-atom nil)))

(defmethod mm-stop! :default
  [server-atom]
  (when-let [{:keys [type]} @server-atom]
    (throw (ex-info "Cannot stop: unknown server type" {:type type}))))


;; handle different servers with protocols & records:
;; - can easily have several servers at once
;; - no varargs in protocol methods

(defprotocol WebServerInstance
  "An instance of web-server that can be started and stopped with given handler
  and options."
  (start! [this handler] [this handler options])
  (stop! [this]))

(defrecord JettyInstance [state]
  WebServerInstance
  (start! [this handler]
    (start! this handler {:port 3000 :join? false}))
  (start! [_ handler {:keys [port join?]}]
    (reset! state (jetty/run-jetty handler {:port port :join? join?})))
  (stop! [_]
    (when-let [server @state]
      (.stop server)
      (reset! state nil))))

(defrecord HttpkitInstance [state]
  WebServerInstance
  (start! [this handler]
    (start! this handler {:port 3000}))
  (start! [_ handler {:keys [port]}]
    (reset! state (httpkit/run-server handler {:port port})))
  (stop! [_]
    (when-let [server @state]
      (server :timeout 100)
      (reset! state nil))))

(defn create-jetty
  "Create a Jetty webserver instance."
  []
  (->JettyInstance (atom nil)))

(defn create-httpkit
  "Create an Httpkit webserver instance."
  []
  (->HttpkitInstance (atom nil)))


(defn -main
  "Application entry point."
  [& args]
  (mm-start! handler :type :jetty :join? true))


(comment

  ;; starting and stopping the server

  (mm-start! handler :type :jetty)
  (mm-stop! server)

  (mm-start! handler :type :httpkit)
  (mm-stop! server)

  (mm-start! handler :type :nope)
  (mm-stop! server)

  (mm-start! handler)
  (mm-stop! server)

  (def jetty-server (create-jetty))
  (start! jetty-server handler {:port 3333})
  (stop! jetty-server)

  (def httpkit-server (create-httpkit))
  (start! httpkit-server handler {:port 4444})
  (stop! httpkit-server)

)
