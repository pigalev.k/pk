(ns hello-ring.guides
  (:require
   [clojure.java.io :as io]
   [clojure.string :as string]
   [fipp.edn :as fipp]
   [hello-ring.core :as core]
   [java-time.api :as time]
   [ring.logger :as logger]
   [ring.middleware.content-type :as ct]
   [ring.middleware.cookies :as c]
   [ring.middleware.defaults :as defaults]
   [ring.middleware.file :as f]
   [ring.middleware.json :as json]
   [ring.middleware.keyword-params :as kp]
   [ring.middleware.not-modified :as nm]
   [ring.middleware.multipart-params :as mp]
   [ring.middleware.params :as p]
   [ring.middleware.reload :as mr]
   [ring.middleware.resource :as r]
   [ring.middleware.session :as s]
   [ring.middleware.session.cookie :as sc]
   [ring.middleware.session.store :as ss]
   [ring.middleware.stacktrace :as ms]
   [ring.mock.request :as mock]
   [ring.util.io :as rio]
   [ring.util.response :as resp]
   [ring.util.time :as rtime]
   [taoensso.timbre :as timbre])
  (:import (java.util Date UUID)))


;; Guides & HOWTOs

;; https://github.com/ring-clojure/ring/wiki


;; creating responses

(comment

  ;; you can create Ring response maps manually (see Concepts), but the
  ;; `ring.util.response` namespace contains a number of useful functions to make
  ;; this task easier.

  ;; the `response` function creates a basic "200 OK" response:

  (resp/response "Hello World")

  ;; you can then use functions like `content-type` to alter the base response
  ;; to add in additional headers and other components:

  (resp/content-type (resp/response "Hello World") "text/plain")

  ;; special functions also exist to create redirects:

  (resp/redirect "http://example.com")

  ;; and to return static files or resources:

  (resp/file-response "index.html" {:root "resources/public"})

  (resp/resource-response "index.html" {:root "public"})

)


;; serving static resources

(comment

  ;; web applications often need to serve static content, such as images or
  ;; stylesheets. Ring provides two middleware functions to do this.


  ;; one is `wrap-file`. This serves static content from a directory on the
  ;; local filesystem:

  (defn handler [_] (resp/response "Hello, World!"))

  (def app (f/wrap-file handler "resources/public"))

  (app {:request-method :get :uri "/hello"})
  (app {:request-method :get :uri "/index.html"})

  ;; this will wrap a handler such that the directory at the given root-path is
  ;; checked for a static file with which to respond to the request, proxying
  ;; the request to the wrapped handler if such a file does not exist.


  ;; the other is `wrap-resource`. This serves static content from the JVM
  ;; classpath:

  (defn handler [_] (resp/response "Hello, World!"))

  (def app
    (r/wrap-resource handler "public"))

  (app {:request-method :get :uri "/hello"})
  (app {:request-method :get :uri "/index.html"})

  ;; if you're using a Clojure build tool like Leiningen then the
  ;; non-source-file resources for a project are kept in the resources
  ;; directory. Files in this directory are automatically included in jar or war
  ;; files.

  ;; so in the above example, files placed in the `resources/public` directory
  ;; will be served up as static files.


  ;; often you'll want to combine `wrap-file` or `wrap-resource` with other
  ;; middleware, usually `wrap-content-type` and `wrap-not-modified`:

  (defn handler [_] (resp/response "Hello, World!"))

  (def app
    (-> handler
        (f/wrap-file "resources/public")
        (ct/wrap-content-type {:mime-types {"org" "text/x-org"}})
        (nm/wrap-not-modified)))

  ;; default content-type is "application/octet-stream"
  (app {:request-method :get :uri "/hello"})

  ;; content-type is inferred from file extension in `:uri`
  (app {:request-method :get :uri "/index.html"})
  (app {:request-method :get :uri "/style.css"})
  (app {:request-method :get :uri "/api.org"})

  ;; not modified (using Ring date formatter)
  (def now (rtime/format-date (java.util.Date.)))
  (app {:request-method :get
        :headers {"if-modified-since" now}
        :uri "/index.html"})

  ;; modified (using external date formatter)
  (def past (time/format :rfc-1123-date-time
                         (time/zoned-date-time
                          1999 06 19 0 0 0 0
                          (time/zone-id "GMT"))))
  (app {:request-method :get
        :headers {"if-modified-since" past}
        :uri "/index.html"})

  ;; the `wrap-content-type` middleware chooses a content-type based on the file
  ;; extension. For instance, a file called `hello.txt` would get a content-type
  ;; of `text/plain`. You can also add custom mime-types by using the
  ;; `:mime-types` option.

  ;; the `wrap-not-modified` middleware checks the "Last-Modified" header on the
  ;; response against the "If-Modified-Since" header on the request. This saves
  ;; bandwidth by ensuring clients don't need to download resources they've
  ;; already cached.

  ;; note that this extra middleware needs to wrap around (i.e. come after)
  ;; the `wrap-resource` or `wrap-file` functions.

)


;; accepting parameters

(comment

  ;; URL-encoded parameters are the primary way browsers pass values to web
  ;; applications. They are sent when a user submits a form, and are usually
  ;; used for things like pagination.

  ;; because Ring is a low level interface, it does not support parameters
  ;; unless you apply the correct middleware:

  (defn handler [request]
    (resp/response (select-keys request [:query-params :form-params :params])))

  (def app
    (-> handler
        p/wrap-params))

  ;; the `wrap-params` middleware provides support for URL-encoded parameters in
  ;; the query string, or from the HTTP request body.

  ;; when applied to a handler, the parameter middleware adds three new keys to
  ;; the request map:

  ;; - `:query-params`: a map of parameters from the query string
  ;; - `:form-params`: a map of parameters from submitted form data
  ;; - `:params`: a merged map of all parameters

  ;; usually you'll only want to use the `:params` key, but the other keys are
  ;; there in case you need to distinguish between a parameter passed via the
  ;; query string, and one passed via a POSTed HTML form.

  ;; the parameters keys are strings, and the values are either strings, if
  ;; there is only one value associated with the parameter name, or vectors, if
  ;; there is more than one name/value pair with the same name.


  ;; one value per parameter name
  (app {:request-method :get :uri "/hello" :query-string "name=Joe"})
  (app {:request-method :post
        :uri            "/hello"
        :headers        {"content-type" "application/x-www-form-urlencoded"}
        ;; create a stream manually
        :body           (io/input-stream (.getBytes "name=Joe"))})

  ;; several values per parameter name, maybe empty
  (app {:request-method :get
        :uri            "/hello"
        :query-string   "name=Joe&name=Molly&name&empty"})
  (app {:request-method :post
        :uri            "/hello"
        :headers        {"content-type" "application/x-www-form-urlencoded"}
        ;; use Ring helper to create a stream
        :body           (rio/string-input-stream
                         "name=Joe&name=Molly&name&empty")})

  ;; it does not provide support for file uploads, which is handled by the
  ;; `wrap-multipart-params` middleware. See the section on File Uploads for
  ;; more information about multipart forms.

  ;; the wrap-params function accepts an optional map of options. There's
  ;; currently only one recognized key:

  ;; `:encoding`: the character encoding of the parameters. Defaults to the
  ;; request character encoding, or UTF-8 if no request character encoding is
  ;; set.


  ;; `wrap-keyword-params` middleware allows to convert parameter keys (in
  ;; `:params` only) to keywords for convenience. It should go after
  ;; `wrap-params` middleware at runtime (applied before it at wrapping time).

  (defn handler [request]
    (resp/response (select-keys request [:query-params :form-params :params])))

  (def app
    (-> handler
        (kp/wrap-keyword-params {:parse-namespaces? true})
        p/wrap-params))

  ;; parameter names as simple keywords
  (app {:request-method :get :uri "/hello" :query-string "name=Joe"})
  (app {:request-method :post
        :uri            "/hello"
        :headers        {"content-type" "application/x-www-form-urlencoded"}
        :body           (io/input-stream (.getBytes "name=Joe"))})

  ;; parameter names as namespaced keywords
  (app {:request-method :get :uri "/hello" :query-string "user/name=Joe"})
  (app {:request-method :post
        :uri            "/hello"
        :headers        {"content-type" "application/x-www-form-urlencoded"}
        :body           (io/input-stream (.getBytes "user/name=Joe"))})

)


;; cookies

(comment

  ;; to add cookie support to your Ring handler, you'll need to wrap it in the
  ;; `wrap-cookies` middleware.

  ;; to set cookies on a client, add a `:cookies` key to the response map. To
  ;; read cookies sent by the client, get them from `:cookies` key in the
  ;; request map.

  (defn handler [request]
    (-> (resp/response (select-keys request [:cookies]))
        (assoc :cookies {"session-id" {:value     "session-id-hash"
                                       :domain    "example.com"
                                       :path      "/hello"
                                       :secure    true
                                       :http-only true
                                       :max-age   100
                                       :expires   (rtime/format-date (Date.))
                                       :same-site :strict}
                         "icu"        {:value "well"}})))

  (def app
    (-> handler
        c/wrap-cookies))

  ;; setting cookies in response headers
  (app {:uri "/hello"})

  ;; also reading cookies from request headers
  (app {:uri     "/hello"
        :headers {"cookie" "foo=bar;frob=quux"}})

  ;; as well as setting the value of the cookie, you can also set additional
  ;; attributes:

  ;; - `:domain`: restrict the cookie to a specific domain
  ;; - `:path`: restrict the cookie to a specific path
  ;; - `:secure`: restrict the cookie to HTTPS URLs if true
  ;; - `:http-only`: restrict the cookie to HTTP if true (not accessible via
  ;;   e.g. JavaScript)
  ;; - `:max-age`: the number of seconds until the cookie expires
  ;; - `:expires`: a specific date and time the cookie expires
  ;; - `:same-site`: specify `:strict`, `:lax`, or `:none` to determine whether
  ;;   cookies should be sent with cross-site requests

)


;; sessions

(comment

  ;; sessions in Ring work a little differently than you might expect, because
  ;; Ring attempts to be functional when possible.


  ;; to store set cookies and send them with requests, like browser do

  (defonce cookie-jar (atom nil))

  (defn wrap-local-cookie-jar
    "Get the first \"Set-Cookie\" header in the response and put it in the cookie
  jar (an atom), then add this cookie to every subsequent request. Should be
  outermost in the middleware stack (first to see the request and last to see
  the response)."
    [handler cookie-jar]
    (fn [request]
      (let [request (if (seq @cookie-jar)
                      (assoc-in request [:headers "cookie"]
                                (string/join ";" @cookie-jar))
                      request)
            response (handler request)
            set-cookies (get-in response [:headers "Set-Cookie"])]
        (when set-cookies
          (reset! cookie-jar set-cookies))
        response)))


  ;; session data is passed via the request map on the `:session` key. The
  ;; following example prints out the current username from the session.

  (defn handler
    [{:keys [session]}]
    (let [username (:username session)
          response (resp/response
                    (str "Hello, " (:username session "anonymous")))]
      ;; add random username to session if not already present, so we can check
      ;; that session is working (persists the username)
      (if-not username
        (assoc response :session
               (assoc session :username (str "user #" (rand-int 10))))
        response)))

  (def app (-> handler
               s/wrap-session
               (wrap-local-cookie-jar cookie-jar)))

  (app {:uri "/hello"})


  ;; to change the session data, you can add a `:session` key to the response
  ;; that contains the updated session data. The next example counts the number
  ;; of times the current session has accessed the page.

  (defn handler
    [{session :session}]
    (let [count (:count session 0)
          session (assoc session :count (inc count))]
      (-> (resp/response (str "You accessed this page " count " times."))
          (assoc :session session))))

  (def app (-> handler
               s/wrap-session
               (wrap-local-cookie-jar cookie-jar)))

  (app {:uri "/hello"})


  ;; to delete the session entirely, set the `:session` key on the response to
  ;; nil:

  (defn handler
    [request]
    (-> (resp/response "Session deleted.")
        (assoc :session nil)))

  (def app (-> handler
               s/wrap-session
               (wrap-local-cookie-jar cookie-jar)))

  (app {:uri "/hello"})


  ;; if you simply want to recreate the session, due to a privilege escalation
  ;; for example, add the `:recreate` key to the session metadata. This will
  ;; cause the session identifier that is sent to the browser to change.

  (defn handler [request]
    (-> (resp/response "Session identifier recreated")
        (assoc :session (vary-meta (:session request) assoc :recreate true))))

  (def app (-> handler
               s/wrap-session
               (wrap-local-cookie-jar cookie-jar)))

  (app {:uri "/hello"})


  ;; often you'll want to control how long the session cookie exists on the
  ;; user's browser. You can alter the session cookie's attributes using the
  ;; `:cookie-attrs` option:

  (def app
    (-> handler
        (s/wrap-session {:cookie-attrs {:max-age 3600}})))

  (app {:uri "/hello"})

  ;; in this case, the cookie's maximum lifespan is set to 3600 seconds, or 1
  ;; hour.


  ;; you can also use this to ensure that session cookies for sites secured with
  ;; HTTPS are not leaked through HTTP:

  (def app
    (-> handler
        (s/wrap-session  {:cookie-attrs {:secure true}})))

  (app {:uri "/hello"})


  ;; session stores

  ;; session data is saved in session stores. There are two stores included in
  ;; Ring:

  ;; - `ring.middleware.session.memory/memory-store`: stores sessions in memory
  ;; - `ring.middleware.session.cookie/cookie-store`: stores sessions encrypted
  ;;   in a cookie

  ;; by default, Ring stores session data in memory, but this can be overridden
  ;; with the `:store` option:

  (def app
    (s/wrap-session handler
                    {:store (sc/cookie-store {:key "a 16-byte secret"})}))

  ;; you can write your own session store by implementing the
  ;; `ring.middleware.session.store/SessionStore` protocol:

  (defn read-data [k])
  (defn generate-new-random-key [])
  (defn save-data [k v])
  (defn delete-data [k])

  (deftype CustomStore []
    ss/SessionStore
    (read-session [_ key]
      (read-data key))
    (write-session [_ key data]
      (let [key (or key (generate-new-random-key))]
        (save-data key data)
        key))
    (delete-session [_ key]
      (delete-data key)
      nil))

  ;; note that when writing the session, the key will be nil if this is a new
  ;; session. The session store should expect this, and generate a new random
  ;; key. It is very important that this key cannot be guessed, otherwise
  ;; malicious users could access other people's session data.

)


;; uploading a file

(comment

  ;; uploading a file to a website requires multipart form handling, which Ring
  ;; provides with its `wrap-multipart-params` middleware.

  ;; the options for this middleware function are:

  ;; - `:encoding`: the character encoding of the parameters. Acts the same as
  ;;   the same option in `wrap-params`.
  ;; - `:store`: a function to use to store the uploaded files. There are two
  ;;   stores included with Ring.

  (defn handler
    [request]
    (resp/response
     (with-out-str (fipp/pprint (select-keys request [:multipart-params])))))

  (def app
    (-> handler
        p/wrap-params
        mp/wrap-multipart-params))

  ;; (re)start a server with the app

  (do
    (core/mm-stop! core/server)
    (core/mm-start! app :type :httpkit))

  ;; upload files (from the project root)

  ;; curl http://localhost:3000 -F file=@deps.edn -F file=@README.md

  ;; uploaded file(s) can be found under the `:multipart-params` (and `:params`)
  ;; key in the request as a map or vector of maps under a `file` key, where
  ;; `:tempfile` is a `java.io.File` object that contains the uploaded data. You
  ;; can use this to perform any further processing you want.

  ;; by default, uploads are stored in temporary files that are deleted an hour
  ;; after being uploaded. This is handled by
  ;; `ring.middleware.multipart-params.temp-file/temp-file-store` function.

  ;; building on the above example, if you wanted to save the tempfile created
  ;; by the Ring server to a different and/or permanent location on your server,
  ;; you can use bean to get the path information for the tempfile, and a
  ;; combination of io/copy and io/file to read the file and save it to that new
  ;; location:

  (defn save-files [tempfile-or-files target-path]
    (try
      (let [tempfiles       (if (sequential? tempfile-or-files)
                              tempfile-or-files
                              [tempfile-or-files])
            tempfiles-names (map #(:filename %) tempfiles)
            tempfiles-paths (map #(:path (bean (:tempfile %))) tempfiles)]
        (dorun
         (map
          (fn [filename temp-path]
            (let [new-file-parent (io/file target-path)
                  new-file-path   (str target-path filename)]
              (when-not (.exists new-file-parent)
                (io/make-parents new-file-path))
              (io/copy (io/file temp-path) (io/file new-file-path))))
          tempfiles-names tempfiles-paths))
        (str "Uploaded files are available at: "
             "http://localhost:3000/uploads,\n"
             "file names: "(string/join ", " tempfiles-names)))
      (catch Exception e
        (fipp/pprint  e)
        (str "Cannot upload file(s): " (.getMessage e)))))

  (defn handler
    [request]
    (fipp/pprint (:params request))
    (let [tempfile-or-files (get-in request [:params "file"])]
      (if tempfile-or-files
        (resp/response (save-files tempfile-or-files "resources/uploads/"))
        (resp/not-found "File not found in uploads."))))

  (def app
    (-> handler
        (f/wrap-file "resources")
        p/wrap-params
        mp/wrap-multipart-params
        ms/wrap-stacktrace-log))

  ;; (re)start a server with the app

  (do
    (core/mm-stop! core/server )
    (core/mm-start! app :type :jetty))

  (do
    (core/mm-stop! core/server)
    (core/mm-start! app :type :httpkit))

  ;; upload files

  ;; curl http://localhost:3000 -F file=@deps.edn -F file=@README.md

  ;; download them back

  ;; curl http://localhost:3000/uploads/deps.edn

)


;; reloading

(comment

  ;; reloading namespaces when a source file is edited can be useful for
  ;; development.

)

;; should be outside comment form for reloading to work

(defn handler [_] (resp/response "Hello, World!"))

(def reloadable-app
  (mr/wrap-reload #'handler))

(reloadable-app {:request-method :get :uri "/hello"})


;; component lifecycle management

(comment

  ;; the reloaded workflow was described first here:
  ;; http://thinkrelevance.com/blog/2013/06/04/clojure-workflow-reloaded

  ;; the reloaded workflow uses components that can be started or stopped,
  ;; arranged together into a system. During development, the system is started
  ;; in a running REPL. After source files are changed, the system is stopped,
  ;; the source files reloaded, and the system started once more. This can be
  ;; put into a single command or even attached to a shortcut.

  ;; a component could include stuff like
  ;; - reading configuration
  ;; - database connection
  ;; - running a Jetty server

  ;; a short list of libraries offering support for this approach:
  ;; - https://github.com/stuartsierra/component
  ;; - https://github.com/weavejester/integrant
  ;; - https://github.com/tolitius/mount

)


;; defaults

(comment

  ;; knowing what middleware to add to a Ring application, and in what order,
  ;; can be difficult and prone to error.

  ;; this library attempts to automate the process, by providing sensible and
  ;; secure default configurations of Ring middleware for both websites and HTTP
  ;; APIs.


  ;; there are four configurations included with the middleware:

  ;; - `api-defaults`: add support for urlencoded parameters
  ;; - `site-defaults`: add support for parameters, cookies, sessions, static
  ;;   resources, file uploads, and a bunch of browser-specific security headers
  ;; - `secure-api-defaults`: additionally force SSL (redirects HTTP->HTTPS,
  ;;   headers etc.)
  ;; - `secure-site-defaults`: additionally force SSL (redirects HTTP->HTTPS,
  ;;   headers etc.)

  (def req
    (-> (mock/request :post "https://example.com/hello")
        (mock/query-string {:name "Joe"})
        (mock/body {:also [:molly "charlie"]})
        (mock/header :x-hostname "tower")
        (mock/header :x-csrf-token "12345")
        (mock/content-type "application/x-my-api+json")
        (mock/content-length 19)
        (mock/cookie :my-api-session-id (UUID/randomUUID))))

  (def http-req
    (-> (mock/request :post "http://example.com/hello")
        (mock/query-string {:name "Joe"})
        (mock/body {:also [:molly "charlie"]})
        (mock/header :x-hostname "tower")
        (mock/header :x-csrf-token "12345")
        (mock/content-type "application/x-my-api+json")
        (mock/content-length 19)
        (mock/cookie :my-api-session-id (UUID/randomUUID))))

  (defn handler
    [request]
    (resp/response {:request request}))


  (def api
    (-> handler
        (defaults/wrap-defaults defaults/api-defaults)))

  (fipp/pprint (api req))


  (def site
    (-> handler
        (defaults/wrap-defaults defaults/site-defaults)))

  (fipp/pprint (site req))
  (fipp/pprint (site http-req))


  (def secure-api
    (-> handler
        (defaults/wrap-defaults defaults/secure-api-defaults)))

  (fipp/pprint (secure-api req))
  (fipp/pprint (secure-api http-req))


  (def secure-site
    (-> handler
        (defaults/wrap-defaults defaults/secure-site-defaults)))

  (fipp/pprint (secure-site req))
  (fipp/pprint (secure-site http-req))


  ;; proxies

  ;; if your app is sitting behind a load balancer or reverse proxy, as is often
  ;; the case in cloud-based deployments, you'll want to set `:proxy` to true:

  (assoc defaults/secure-site-defaults :proxy true)

  ;; this is particularly important when your site is secured with SSL, as the
  ;; SSL redirect middleware will get caught in a redirect loop if it can't
  ;; determine the correct URL scheme of the request.


  ;; customizing

  ;; the default configurations are just maps of options, and can be customized
  ;; to suit your needs. For example, if you wanted the normal site defaults,
  ;; but without session support, you could use:

  (assoc defaults/site-defaults :session false)

  ;; the following configuration keys are supported:

  ;; - `:cookies`: set to true to parse cookies from the request.
  ;; - `:params`: a map of options that describes how to parse parameters from
  ;;   the request.
  ;; - `:keywordize`: set to true to turn the parameter keys into keywords.
  ;; - `:multipart`: set to true to parse urlencoded parameters in the query
  ;;   string and the request body, or supply a map of options to pass to the
  ;;   standard Ring `multipart-params` middleware.
  ;; - `:nested`: set to true to allow nested parameters via the standard Ring
  ;;   nested-params middleware
  ;; - `:urlencoded`: set to true to parse urlencoded parameters in the query
  ;;   string and the request body.
  ;; - `:proxy`: set to true if the application is running behind a reverse
  ;;   proxy or load balancer.
  ;; - `:responses`: a map of options to augment the responses from your
  ;;   application.
  ;; - `:absolute-redirects`: any redirects to relative URLs will be turned
  ;;   into redirects to absolute URLs, to better conform to the HTTP spec.
  ;; - `:content-types`: adds the standard Ring `content-type` middleware.
  ;; - `:default-charset`: adds a default charset to any text `content-type`
  ;;   lacking a charset.
  ;; - `:not-modified-responses`: adds the standard Ring `not-modified`
  ;;   middleware.
  ;; - `:security`: options for security related behaviors and headers.
  ;; - `:anti-forgery`: set to true to add CSRF protection via the
  ;;   `ring-anti-forgery` library.
  ;; - `:content-type-options`: prevents attacks based around media-type
  ;;   confusion. See: `wrap-content-type-options`.
  ;; - `:frame-options`: prevents your site from being placed in frames or
  ;;   iframes. See: `wrap-frame-options`.
  ;; - `:hsts`: if true, enable HTTP Strict Transport Security. See: `wrap-hsts`.
  ;; - `:ssl-redirect`: if true, redirect all HTTP requests to the equivalent
  ;;   HTTPS URL. A map with an `:ssl-port` option may be set instead, if the
  ;;   HTTPS server is on a non-standard port. See: `wrap-ssl-redirect`.
  ;; - `:xss-protection` (deprecated): enable the X-XSS-Protection header. This
  ;;   is no longer considered best practice and should be avoided. See:
  ;;   `wrap-xss-protection`.
  ;; - `:session`: a map of options for configuring session handling via the
  ;;   Ring session middleware.
  ;; - `:flash`: if set to true, the Ring flash middleware is added.
  ;; - `:store`: the Ring session store to use for storing sessions.
  ;; - `:static`: a map of options to configure how to find static content.
  ;; - `:files`: a string or collection of strings containing paths to
  ;;   directories to serve files from. Usually the `:resources` option below
  ;;   is more useful.
  ;; - `:resources`: a string or collection of strings containing classpath
  ;;   prefixes. This will serve any resources in locations starting with the
  ;;   supplied prefix.

)


;; logging

(comment

  ;; basics

  (defn handler [_] (resp/response "Hello, World!"))

  (def app
    (-> handler
        (logger/wrap-with-logger)))

  (app (mock/request :get "https://example.com/hello"))
  (app (mock/request :post "https://example.com/hello"))

  (do
    (core/mm-stop! core/server)
    (core/mm-start! app :type :jetty))

  ;; what gets logged

  ;; - an `:info` level message when a request begins.
  ;; - a `:debug` level message with the request parameters (redacted).
  ;; - an `:info` level message when a response is returned without server
  ;;   errors (i.e. its HTTP status is < 500), otherwise an `:error` level
  ;;   message is logged.
  ;; - an `:error` level message with a stack trace when an exception is thrown
  ;;   during response generation.

  ;; all messages will be usually timestamped by your logging infrastructure.


  ;; logging backends

  ;; other logging backends can be plugged by passing the `log-fn` option. This
  ;; is how you could use `timbre` instead of `c.t.logging`:

  (defn handler [_] (resp/response "Hello, World!"))

  (def logger-options {:log-fn (fn [{:keys [level throwable message]}]
                                 (timbre/log level throwable message))})

  (def app
    (-> handler
        (logger/wrap-log-response logger-options)))

  (app (mock/request :get "https://example.com/hello"))
  (app (mock/request :post "https://example.com/hello"))

  ;; curl http://localhost:3000/hello

  ;; some Timbre settings

  ;; set the minimum logging level

  ;; a Timbre logging call will be disabled (noop) when the call's
  ;; level (e.g. (info ...) is less than the active minimum
  ;; level (e.g. `:warn`).

  ;; levels: `:trace` < `:debug` < `:info` < `:warn` < `:error` < `:fatal`
  ;; < `:report`

  ;; set the minimum level for all namespaces or the current namespace

  (timbre/set-min-level! :warn)
  (timbre/set-min-level! :info)

  (timbre/set-ns-min-level! :report)
  (timbre/set-ns-min-level! :info)


  ;; advanced usage

  ;; explicitly log start, finish, and parameters of requests

  ;; `ring.logger` comes with more fine-grained middleware apart from
  ;; `wrap-with-logger`:

  ;; - `wrap-log-request-start`: logs the start of the request
  ;; - `wrap-log-response`: logs the response and request timings
  ;; - `wrap-log-request-params`: logs the request parameters, using redaction
  ;;   to hide sensitive values (passwords, tokens, etc)


  ;; to log just the start and finish of requests (no parameters):

  (defn handler [_] (resp/response "Hello, World!"))

  (def logger-options {:log-fn (fn [{:keys [level throwable message]}]
                                 (timbre/log level throwable message))})

  (def app
    (-> handler
        (logger/wrap-log-response logger-options)
        ;; more middleware to parse params, cookies, etc.
        kp/wrap-keyword-params
        p/wrap-params
        (logger/wrap-log-request-start logger-options)))

  (app (mock/request :get "https://example.com/hello"))
  (app (mock/request :post "https://example.com/hello"))

  ;; to measure request latency, `wrap-log-response` will use the
  ;; `ring.logger/start-ms` key added by `wrap-log-request-start` if both
  ;; middlewares are being used, or will call `System/currentTimeMillis` to
  ;; obtain the value by itself.

  ;; to explicitly log start, finish, and parameters of requests:

  (defn handler [_] (resp/response "Hello, World!"))

  (def logger-options {:log-fn       (fn [{:keys [level throwable message]}]
                                       (timbre/log level throwable message))
                       :transform-fn #(assoc % :level :info)})

  (def app
    (-> handler
        (logger/wrap-log-response logger-options)
        ;; more middleware to parse params, cookies, etc.

        ;; the following line must come before `wrap-params`
        (logger/wrap-log-request-params logger-options)
        kp/wrap-keyword-params    ; optional
        json/wrap-json-params     ; required for JSON body params
        p/wrap-params             ; required for query and form body params
        (logger/wrap-log-request-start logger-options)))

  (app (mock/request :get "https://example.com/hello?name=Joe"))
  (app (-> (mock/request :post "https://example.com/hello?name=Molly")
           (mock/json-body {:also :barnacles :token "12345"})))

  ;; note that in the above example, the `:transform-fn` is optional. You can
  ;; either bump your log level to DEBUG or use `:transform-fn` to adjust the
  ;; log level of params logging before it hits the `log-fn`. The latter is
  ;; often preferable, since DEBUG tends to be very noisy.

)
