# hello-spec-tools

Exploring `spec-tools` --- Clojure(Script) tools for `clojure.spec`.

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
