# hello-shadow-cljs

Basics of building ClojureScript codebases with `shadow-cljs`.

- https://github.com/thheller/shadow-cljs

## Getting Started

Install npm dependencies

```
yarn
```

Run the `app` build and watch sources for changes

```
npx shadow-cljs watch app
```
