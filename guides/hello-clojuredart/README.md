# hello-clojuredart

Exploring ClojureDart --- Clojure dialect for Dart and Flutter.

- https://github.com/Tensegritics/ClojureDart

## Getting started

No REPL yet.

Compile, watch and run

```
clj -M:cljd flutter [-d device-id]
```

Run `flutter devices` to see available device ids (id is a string in the
second field)

## Update

Initialize the example project in the current directory

```
clj -M:cljd init
```

Update to the latest ClojureDart

```
clj -M:cljd upgrade
```
