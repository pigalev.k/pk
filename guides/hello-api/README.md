# hello-api

Exploring tools for building various APIs in Clojure (REST, RPC, etc).

- `Farseer`: a set of modules for handling JSON RPC in Clojure
  https://github.com/igrishaev/farseer
-

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
