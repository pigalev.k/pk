(ns hello-api.json-rpc.farseer
  (:require
   [cheshire.core :as json]
   [clojure.spec.alpha :as s]
   [farseer.handler :refer [make-handler]]
   [farseer.http :as http]
   [farseer.jetty :as jetty]))


;; domain/infra functions (usually in a separate ns/component)

(defn get-by-id-stub
  "Get the user by id (stub)."
  [db user-id]
  ;; in reality should be smth like (jdbc/get-by-id db :users user-id)
  {:user/id 12, :user/name "Ololosha" :user/sources [db]})


;; RPC methods and their input and output data specifications

(defn print-context
  "Logs the handler context to console."
  [context _]
  (println context))


(defn sum
  "Returns the sum of numbers."
  [_ xs]
  (apply + xs))

(s/def ::sum.in
  (s/* number?))

(s/def ::sum.out
  number?)


(defn get-user-by-id
  [{:keys [db-from-static-context
           db-from-dynamic-context]} ;; context
   {:keys [user-id]}]  ;; params
  (merge-with (fn [left right] (if (coll? left) ((partial apply conj left) right) right))
              (get-by-id-stub db-from-static-context user-id)
              (get-by-id-stub db-from-dynamic-context user-id)))

(s/def :user/id pos-int?)

(s/def :user/user-by-id.in
  (s/keys :req-un [:user/id]))

(s/def :user/user-by-id.out
  (s/nilable map?))


;; handlers' configuration (methods usage and specs)

(def config
  {:rpc/handlers
   {:core/print-context
    {:handler/function #'print-context}
    :math/sum
    {:handler/function #'sum
     :handler/spec-in  ::sum.in
     :handler/spec-out ::sum.out}
    :user/get-by-id
    {:handler/function #'get-user-by-id}}})

(def context {:version "1.0.0"
              :db-from-static-context "static db connection"})


;; an RPC handler (RPC request map, context -> RPC response map)

(def handler
  "An RPC handler."
  (make-handler config context))


;; testing the handler

(def test-print-context
  {:id      1
   :method  :core/print-context
   :jsonrpc "2.0"})

(def test-sum-1
  {:id      1
   :method  :math/sum
   :params  [1 2]
   :jsonrpc "2.0"})
(def test-sum-2
  {:id      1
   :method  :math/sum
   :params  [1 2 3 4 5]
   :jsonrpc "2.0"})
(def test-missing-method
  {:id      1
   :method  :system/rmrf
   :params  [1 2]
   :jsonrpc "2.0"})
(def test-sum-spec
  {:id      1
   :method  :math/sum
   :params  [nil 2]
   :jsonrpc "2.0"})
(def test-user-by-id
  {:id      1
   :method  :user/get-by-id
   :params {:id 12}
   :jsonrpc "2.0"})
(def test-notification
  {:method  :core/print-context
   :jsonrpc "2.0"})

(comment

  (handler test-print-context)
  (handler test-sum-1)
  (handler test-sum-2)
  (handler test-missing-method)
  (handler test-sum-spec)
  (handler test-user-by-id
           {:db-from-dynamic-context "dynamic db connection"})
  (handler test-notification)
  (handler [test-sum-1
            test-sum-2
            test-user-by-id
            test-missing-method
            test-notification])

)


;; Ring HTTP handler (app) (HTTP request map -> HTTP response map)

(def app
  (http/make-app config {:db-from-dynamic-context "dynamic db connection"}))

;; test the app

(def rpc-request
  {:id      1
   :jsonrpc "2.0"
   :method  :math/sum
   :params  [1 2]})

(def http-request
  {:request-method :post
   :uri            "/"
   :headers        {"content-type" "application/json"}
   :body           (-> rpc-request json/generate-string .getBytes)})

(def http-request-batch
  {:request-method :post
   :uri            "/"
   :headers        {"content-type" "application/json"}
   :body           (-> [rpc-request rpc-request]
                       json/generate-string
                       .getBytes)})

(comment

  (app http-request)
  (app http-request-batch)

)


;; infrastructure

(defonce server (atom nil))


;; for development (not blocking current thread, i.e., the REPL)

(defn start-dev
  "Start a web-server in another thread."
  []
  (reset! server
          (jetty/start-server config context)))

(defn stop-dev []
  (jetty/stop-server @server))

(defn restart []
  (stop-dev)
  (start-dev))


;; for production (blocking current thread)

(defn start
  "Start a web-server in the current thread."
  []
  (jetty/start-server (assoc config :jetty/join? true)))
