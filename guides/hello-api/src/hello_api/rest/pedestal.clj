(ns hello-api.rest.pedestal
  (:require
   [clojure.data.json :as json]
   [clojure.string :as s]
   [io.pedestal.http :as http]
   [io.pedestal.http.route :as route]
   [io.pedestal.http.content-negotiation :as conneg]))


;; data

(def unmentionables
  #{"yhwh" "voldemort" "mxyzptlk" "rumplestiltskin" "曹操"})


;; helpers

(defn response [status body & {:as headers}]
  {:status status :body body :headers headers})

(def ok
  "Return a 200 OK HTTP response with given body and headers."
  (partial response 200))
(def created
  "Return a 201 Created HTTP response with given body and headers."
  (partial response 201))
(def accepted
  "Return a 201 Created HTTP response with given body and headers."
  (partial response 202))
(def bad-request
  "Return a 400 Bad Request HTTP response with given body and headers."
  (partial response 400))
(def not-found
  "Return a 404 Not Found HTTP response with given body and headers."
  (partial response 404))

(defn greeting-for
  "Return a greeting for a given name. Greets world if name is nil,
  returns nil if name is empty string."
  [nm]
  (cond
    (nil? nm)                        "Hello, world!"
    (= 0 (count nm))                 nil
    (unmentionables
     (s/lower-case nm)) nil
    :else                            (str "Hello, " nm "!")))


;; handlers and interceptors

(def supported-content-types
  ["text/html" "application/edn" "application/json" "text/plain"])

(def content-negotiation-interceptor
  (conneg/negotiate-content supported-content-types))

(def coerce-body
  "Coerce the response body into accepted format."
  {:name ::coerce-body
   :leave
   (fn [context]
     (let [accepted         (get-in context [:request :accept :field]
                                    "text/plain")
           response         (get context :response)
           body             (get response :body)
           coerced-body     (case accepted
                              "text/html"        body
                              "text/plain"       body
                              "application/edn"  (pr-str body)
                              "application/json" (json/write-str body))
           updated-response (assoc response
                                   :headers {"Content-Type" accepted}
                                   :body    coerced-body)]
       (assoc context :response updated-response)))})

;; a simple function handler (returned value becomes a response)
(defn respond-hello
  "A greeting route handler. Returns a greeting, personalized if name was given."
  [request]
  (println (str "params: " (:params request)))
  (let [nm (get-in request [:params :name])
        body (greeting-for nm)]
    (if body
      (ok body "Content-Type" "text/html")
      (bad-request "if given, name should not be empty or unmentionable"))))

;; an interceptor handler (handlers add response to context map, request then
;; considered handled)
(def echo
  "An echo route handler. Returns the request to the sender."
  {:name ::echo
   :enter (fn [context]
            (let [request (:request context)
                  response (ok request)]
              (assoc context :response response)))})


;; routes

(def routes
  (route/expand-routes
   #{["/greet"
      :get
      [coerce-body content-negotiation-interceptor respond-hello]
      :route-name ::greet]
     ["/echo"  :get echo :route-name ::echo]}))


;; infrastructure (starting/stopping/restarting a service, etc)

(defonce server (atom nil))

(def service-map
  {::http/routes routes
   ::http/type   :jetty
   ::http/port   8890})


;; for development (not blocking current thread, i.e., the REPL)

(defn start-dev
  "Start a web-server in another thread."
  []
  (reset! server
          (http/start (http/create-server
                       (assoc service-map
                              ::http/join? false)))))

(defn stop-dev []
  (http/stop @server))

(defn restart []
  (stop-dev)
  (start-dev))


;; for production (blocking current thread)

(defn start
  "Start a web-server in the current thread."
  []
  (http/start (http/create-server service-map)))
