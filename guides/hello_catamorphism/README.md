# hello-catamorphism

Exploring the concept of catamorphism and it's practical uses.

- https://en.wikipedia.org/wiki/Catamorphism

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
