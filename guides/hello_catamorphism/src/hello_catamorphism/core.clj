(ns hello-catamorphism.core
  (:require
   [clojure.string :as string]
   [clojure.set :as set]
   [clojure.walk :as walk]))

;; Source: https://deque.blog/2017/01/26/catamorph-your-dsl-clojure/

;; DSL: representation example
;; [:add 1 2
;;  [:mul 0 "x" "y"]
;;  [:mul 1 "y" 2]
;;  [:add 0 "x"]]

;; constructors
(defn cst [n] n)
(defn sym [s] s)
(defn add [& args] (into [:add] args))
(defn mul [& args] (into [:mul] args))

;; extractors
(defn rator [e] (first e))
(defn rands [e] (rest e))

;; predicates
(defn cst? [n] (number? n))
(defn sym? [v] (string? v))
(defn op?  [e] (vector? e))
(defn add? [e] (and (op? e) (= (rator e) :add)))
(defn mul? [e] (and (op? e) (= (rator e) :mul)))

;; simple interpreter (pretty-printer)
(defn print-expr [expr]
  (walk/postwalk
   (fn algebra [e]
     (cond
       (cst? e) (str e)
       (sym? e) e
       (add? e) (str "(+ " (string/join " " (rest e)) ")")
       (mul? e) (str "(* " (string/join " " (rest e)) ")")
       :else e))
   expr))

;; expression example
(def expr
  (add
   (cst 1) (cst 2)
   (mul (cst 0) (sym "x") (sym "y"))
   (mul (cst 1) (sym "y") (cst 2))
   (add (cst 0) (sym "x"))))

;; interpret the expression example
(print-expr expr)
;; "(+ 1 2 (* 0 x y) (* 1 y 2) (+ 0 x))"

;; evaluator interpreter
(defn evaluate [env e]
  (walk/postwalk
   (fn [e]
     (cond
       (sym? e) (get env e)
       (add? e) (reduce + (rest e))
       (mul? e) (reduce * (rest e))
       :else e))
   e))

;; evaluate the expression example
(def env {"x" 1 "y" 12})
(evaluate env expr)
;; 28

;; expression symbols enumerator interpreter
(defn dependencies [e]
  (walk/postwalk
   (fn [e]
     (cond
       (cst? e) #{}
       (sym? e) #{e}
       (op? e) (apply set/union (rands e))
       :else e))
   e))

;; enumerate symbols of expression example
(dependencies expr)
;; #{"x" "y"}


;; optimizations
(defn make-op
  [rator rands]
  (if (< 1 (count rands))
    (into [rator] rands)
    (first rands)))

(defn optimize-op
  [[rator & rands] binary-op neutral]
  (let [{csts true vars false} (group-by cst? rands)
        sum-cst (reduce binary-op neutral csts)]
    (cond
      (empty? vars) (cst sum-cst)
      (= neutral sum-cst) (make-op rator vars)
      :else (make-op rator (into [(cst sum-cst)] vars)))))

(defn optimize-add [e]
  (if (add? e)
    (optimize-op e + 0)
    e))

(defn optimize-mul [e]
  (if (mul? e)
    (if (some #{(cst 0)} e)
      (cst 0)
      (optimize-op e * 1))
    e))

(defn optimize [e]
  (walk/postwalk
    (comp optimize-mul optimize-add)
    e))

;; optimize the expression example
(print-expr expr)
;; "(+ 1 2 (* 0 x y) (* 1 y 2) (+ 0 x))"
(print-expr (optimize expr))
;; "(+ 3 (* 2 y) x)"


;; partial application
(defn replace-var
  [env x]
  (if (string? x) (get env x x) x))

(defn partial-eval
  [env e]
  (walk/postwalk
    (comp optimize-mul optimize-add #(replace-var env %))
    e))

;; partially evaluate the expression example
(print-expr expr)
;; "(+ 1 2 (* 0 x y) (* 1 y 2) (+ 0 x))"
(print-expr (partial-eval {"y" 0} expr))
;; "(+ 3 x)"



;; postwalk examples

(walk/postwalk
 #(do (print (str % " ")) %)
 [:add 1 [:mul "x" 2]])

