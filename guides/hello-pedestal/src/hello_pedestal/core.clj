(ns hello-pedestal.core
  (:require
   [clojure.data.json :as json]
   [clojure.edn :as edn]
   [clojure.string :refer [lower-case]]
   [io.pedestal.http :as http]
   [io.pedestal.http.content-negotiation :as conneg]
   [io.pedestal.http.route :as route]
   [io.pedestal.test :as test]
   [mount.core :as mount :refer [defstate]]))


(comment

  (defonce database (atom {}))

  ;; data

  (def unmentionables #{"yhwh" "voldemort" "mxyzptlk" "rumplestiltskin" "曹操"})
  (def supported-types ["text/html" "application/edn"
                        "application/json" "text/plain"])


  ;; helpers

  (defn response [status body & {:as headers}]
    {:status status :body body :headers headers})

  (def ok (partial response 200))
  (def created (partial response 201))
  (def accepted (partial response 202))
  (def bad-request (partial response 400))
  (def not-found (partial response 404))

  (defn greeting-for [name]
    (cond
      (nil? name) nil
      (unmentionables (lower-case name)) nil
      (empty? name) "Hello, anonymous!\n"
      :else (str "Hello, " name "!\n")))

  (defn make-list [name]
    {:name name
     :items {}})

  (defn make-list-item [name]
    {:name name
     :done? false})

  (defn find-list-by-id [dbval db-id]
    (get dbval db-id))

  (defn find-list-item-by-ids [dbval list-id item-id]
    (get-in dbval [list-id :items item-id] nil))

  (defn list-item-add
    [dbval list-id item-id new-item]
    (if (contains? dbval list-id)
      (assoc-in dbval [list-id :items item-id] new-item)
      dbval))


  ;; interceptors

  (def content-neg-intc (conneg/negotiate-content supported-types))

  (def coerce-body
    {:name ::coerce-body
     :leave
     (fn [context]
       (let [accepted (get-in context [:request :accept :field] "text/plain")
             response (get context :response)
             body (get response :body)
             coerced-body (case accepted
                            "text/html" body
                            "text/plain" body
                            "application/edn" (pr-str body)
                            "application/json" (json/write-str body))
             updated-response (assoc response
                                     :headers {"Content-Type" accepted}
                                     :body coerced-body)]
         (assoc context :response updated-response)))})

  (def db-interceptor
    {:name :database-interceptor
     :enter
     (fn [context]
       (update context :request assoc :database @database))
     :leave
     (fn [context]
       (if-let [[op & args] (:tx-data context)]
         (do
           (apply swap! database op args)
           (assoc-in context [:request :database] @database))
         context))})

  (def list-create
    {:name :list-create
     :enter
     (fn [context]
       (let [nm       (get-in context [:request :query-params :name]
                              "Unnamed List")
             new-list (make-list nm)
             db-id    (str (gensym "l"))
             url      (route/url-for :list-view :params {:list-id db-id})]
         (assoc context
                :response (created new-list "Location" url)
                :tx-data [assoc db-id new-list])))})

  (def list-view
    {:name :list-view
     :enter
     (fn [context]
       (if-let [db-id (get-in context [:request :path-params :list-id])]
         (if-let [the-list (find-list-by-id
                            (get-in context [:request :database]) db-id)]
           (assoc context :result the-list)
           context)
         context))})

  (def entity-render
    {:name :entity-render
     :leave
     (fn [context]
       (if-let [item (:result context)]
         (assoc context :response (ok item))
         context))})

  (def list-item-view
    {:name :list-item-view
     :leave
     (fn [context]
       (if-let [list-id (get-in context [:request :path-params :list-id])]
         (if-let [item-id (get-in context [:request :path-params :item-id])]
           (if-let [item (find-list-item-by-ids
                          (get-in context [:request :database]) list-id item-id)]
             (assoc context :result item)
             context)
           context)
         context))})

  (def list-item-create
    {:name :list-item-create
     :enter
     (fn [context]
       (if-let [list-id (get-in context [:request :path-params :list-id])]
         (let [nm       (get-in context [:request :query-params :name]
                                "Unnamed Item")
               new-item (make-list-item nm)
               item-id  (str (gensym "i"))]
           (-> context
               (assoc :tx-data  [list-item-add list-id item-id new-item])
               (assoc-in [:request :path-params :item-id] item-id)))
         context))})


  ;; handlers

  (defn respond-hello [request]
    #_{:status 200 :body "Hello world!"}
    #_{:status 200 :body request}
    #_(let [name (get-in request [:query-params :name] "anonymous")]
        {:status 200 :body (str "Hello, " name "\n")})
    (let [name (get-in request [:query-params :name])
          greeting (greeting-for name)]
      (if greeting
        (ok greeting)
        (not-found))))

  (def echo
    {:name ::echo
     :enter (fn [context]
              (let [request (:request context)
                    response (ok request)]
                (assoc context :response response)))})

  (defn lol [request]
    (ok "Ololo!"))


  ;; routes

  (def routes
    (route/expand-routes
     #{["/greet" :get [coerce-body content-neg-intc respond-hello]
        :route-name :greet]
       ["/echo" :get echo]
       ["/lol" :get lol :route-name :lol]
       ;; api
       ["/todo"                    :post   [db-interceptor list-create]]
       ["/todo"                    :get    echo :route-name :list-query-form]
       ["/todo/:list-id"           :get    [entity-render db-interceptor
                                            list-view]]
       ["/todo/:list-id"           :post   [entity-render list-item-view
                                            db-interceptor list-item-create]]
       ["/todo/:list-id/:item-id"  :get    [entity-render list-item-view
                                            db-interceptor]]
       ["/todo/:list-id/:item-id"  :put    echo :route-name :list-item-update]
       ["/todo/:list-id/:item-id"  :delete echo :route-name :list-item-delete]}))


  ;; app state management

  (defstate ^{:on-reload :noop} http-server
    :start (http/start
            (http/create-server
             {::http/routes routes
              ::http/type :jetty
              ::http/port 8890
              ::http/join? false}))
    :stop (http/stop http-server))

  (defn start []
    (mount/start))

  (defn stop []
    (mount/stop))

  (defn restart []
    (stop)
    (start))


  ;; some manual tests

  (defn test-request [verb url]
    (io.pedestal.test/response-for (::http/service-fn http-server) verb url))

  ;; routing

  (route/try-routing-for routes :prefix-tree "/greet" :get)

  ;; server

  (test/response-for (:io.pedestal.http/service-fn http-server) :get "/todo")

  (dissoc (test/response-for
           (:io.pedestal.http/service-fn http-server) :get "/no-such-route")
          :body)

  (dissoc (test/response-for
           (:io.pedestal.http/service-fn http-server) :delete "/todo") :body)

  (dissoc (test/response-for
           (:io.pedestal.http/service-fn http-server) :post "/todo") :body)
  (as-> (test/response-for
         (:io.pedestal.http/service-fn http-server) :get "/todo/ololo/12")
      response
      (:body response)
      (edn/read-string {:default str} response)
      (:path-params response))

  (test-request :post "/todo?name=SomeList")

  (test-request :get "/todo/l24391")

  (test-request :post "/todo/l24391")

)
