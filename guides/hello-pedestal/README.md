# hello-pedestal

Exploring Pedestal --- a set of libraries to build services and applications
that runs in the back end and can serve up HTML pages or handle API requests.

- http://pedestal.io/index

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
