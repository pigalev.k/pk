# hello-causality

Exploring concepts of causality.

- https://en.wikipedia.org/wiki/Causality

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
