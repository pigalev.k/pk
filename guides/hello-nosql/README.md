# hello-<topic>

Exploring various noSQL databases.

- Redis: the open source, in-memory data store used by millions of developers as
  a database, cache, streaming engine, and message broker https://redis.io/

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.

## Dependencies

Redis

```
podman-compose -f redis.yml up -d
```
