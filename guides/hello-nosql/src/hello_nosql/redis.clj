(ns hello-nosql.redis
  (:require
   [clojure.repl :as repl]
   [clojure.string :as string]
   [taoensso.carmine :as c]
   [taoensso.carmine.locks :as locks]
   [taoensso.carmine.message-queue :as car-mq]
   [taoensso.carmine.tundra :as t]
   [taoensso.carmine.tundra.s3 :as s3]))


;; Quickstart

;; https://github.com/ptaoussanis/carmine

(comment

  ;; features

  ;; - fast, simple all-Clojure library.
  ;; - fully documented API with support for the latest Redis commands.
  ;; - production-ready connection pooling.
  ;; - auto de/serialization of Clojure data types via Nippy.
  ;; - fast, simple message queue API.
  ;; - fast, simple distributed lock API.
  ;; - support for Lua scripting, Pub/Sub, etc.
  ;; - support for custom reply parsing.
  ;; - includes Ring session-store.

  ;; an major upcoming (v4) release will include support for RESP3, Redis
  ;; Sentinel, Redis Cluster, and more.


  ;; quickstart

  ;; you'll usually want to define a single connection pool, and one connection
  ;; spec for each of your Redis servers.

  ;; create a new stateful pool

  (defonce my-conn-pool (c/connection-pool {}))
  (def my-conn-spec-1 {:uri "redis://localhost:6379"})

  (def opts
    {:pool my-conn-pool
     :spec my-conn-spec-1})

  ;; this my-wcar-opts can then be provided to Carmine's `c/wcar` ("with
  ;; Carmine") API:

  (c/wcar opts (c/ping))

  ;; `c/wcar` is the main entry-point to Carmine's API, see its docstring for
  ;; more info on pool and spec opts, etc.

  ;; you can create a wcar partial for convenience:

  (defmacro wcar* [& body] `(c/wcar ~opts ~@body))

  (wcar* (c/ping))

  ;; seems not to work: Can't embed object in code, maybe print-dup not defined:
  ;; GenericKeyedObjectPool


  ;; command pipelines

  ;; calling multiple Redis commands in a single wcar body uses efficient Redis
  ;; pipelining under the hood, and returns a pipeline reply (vector) for easy
  ;; destructuring:

  (c/wcar opts
   (c/ping)
   (c/set "foo" "bar")
   (c/get "foo"))

  ;; if the number of commands you'll be calling might vary, it's possible to
  ;; request that Carmine always return a destructurable pipeline-style reply:

  (c/wcar opts :as-pipeline (c/ping))

  ;; if the server replies with an error, an exception is thrown:

  (c/wcar opts (c/spop "foo"))

  ;; but what if we're pipelining?

  (c/wcar opts
   (c/set  "foo" "bar")
   (c/spop "foo")
   (c/get  "foo"))


  ;; serialization

  ;; the only value type known to Redis internally is the byte string. But
  ;; Carmine uses Nippy under the hood and understands all of Clojure's rich
  ;; datatypes, letting you use them with Redis painlessly:

  (c/wcar opts
   (c/set "clj-key"
          {:bigint (bigint 31415926535897932384626433832795)
           :vec    (vec (range 5))
           :set    #{true false :a :b :c :d}
           :bytes  (byte-array 5)})
   (c/get "clj-key"))

  ;; types are handled as follows:
  ;; Clojure type     Redis type
  ;; Strings          Redis strings
  ;; Keywords         Redis strings
  ;; Simple numbers   Redis strings
  ;; Everything else  Auto de/serialized with Nippy

  ;; you can force automatic de/serialization for an argument of any type by
  ;; wrapping it with car/freeze.


  ;; command coverage and documentation

  ;; Carmine uses the official Redis command spec to generate its own command
  ;; API and documentation:

  (repl/doc c/sort)

  ;; each Carmine release will use the latest command spec. But if a new Redis
  ;; command is available that hasn't yet made it to Carmine, you can always use
  ;; the `c/redis-call` function to issue arbitrary commands.

  ;; this is also useful for Redis module commands, etc.


  ;; Lua scripting

  ;; redis offers powerful Lua scripting capabilities.

  ;; as an example, let's write our own version of the set command:

  (defn my-set
    [key value]
    (c/lua "return redis.call('set', _:my-key, 'lua '.. _:my-val)"
           ;; named key variables and their values
           {:my-key key}
           ;; named non-key variables and their values
           {:my-val value}))

  (c/wcar opts
   (my-set  "foo" "bar")
   (c/get "foo"))

  ;; script primitives are also provided: `eval`, `eval-sha`, `eval*`,
  ;; `eval-sha*`.


  ;; helpers

  ;; the Lua command above is a good example of a Carmine helper.

  ;; Carmine will never surprise you by interfering with the standard Redis
  ;; command API. But there are times when it might want to offer you a helping
  ;; hand (if you want it). Compare:

  (c/wcar opts
          (c/zunionstore
           "dest-key" 3 "zset1" "zset2" "zset3"  "WEIGHTS" 2 3 5))

  (c/wcar opts
          (c/zunionstore* "dest-key"  ["zset1" "zset2" "zset3"] "WEIGHTS" 2 3 5))

  ;; both of these calls are equivalent but the latter counted the keys for
  ;; us. `c/zunionstore*` is another helper: a slightly more convenient version
  ;; of a standard command, suffixed with a * to indicate that it's
  ;; non-standard.

  ;; helpers currently include: `c/atomic`, `c/eval*`, `c/evalsha*`, `c/info*`,
  ;; `c/lua`, `c/sort*`, `c/zinterstore*`, and `c/zunionstore*`. See their
  ;; docstrings for more info.


  ;; commands are (just) functions

  ;; in Carmine, Redis commands are real functions. Which means you can use them
  ;; like real functions:

  (c/wcar opts (doall (repeatedly 5 c/ping)))

  (let [first-names ["Salvatore"  "Rich"]
        surnames    ["Sanfilippo" "Hickey"]]
    (c/wcar opts (mapv #(c/set %1 %2) first-names surnames)
           (mapv c/get first-names)))

  (c/wcar opts (mapv #(c/set (str "key-" %) (rand-int 10)) (range 3))
          (mapv #(c/get (str "key-" %)) (range 3)))

  ;; and since real functions can compose, so can Carmine's. By nesting `c/wcar`
  ;; calls, you can fully control how composition and pipelining interact:

  (let [hash-key "awesome-people"]
    (c/wcar opts
     (c/hmset hash-key "Rich" "Hickey" "Salvatore" "Sanfilippo")
     (mapv (partial c/hget hash-key)
           ;; execute with own connection & pipeline then return result for
           ;; composition:
           (c/wcar opts (c/hkeys hash-key)))))


  ; listeners & pub/sub

  ;; Carmine has a flexible Listener API to support persistent-connection
  ;; features like monitoring and Redis's fantastic Pub/Sub facility:

  (def listener
    (c/with-new-pubsub-listener (:spec opts)
      {"foobar" (fn f1 [msg] (println "Channel match: " msg))
       "foo*"   (fn f2 [msg] (println "Pattern match: " msg))}
      (c/subscribe  "foobar" "foobaz")
      (c/psubscribe "foo*")))

  ;; note the map of message handlers. `f1` will trigger when a message is
  ;; published to channel "foobar". `f2` will trigger when a message is
  ;; published to "foobar", "foobaz", "foo Abraham Lincoln", etc.

  ;; publish messages:

  (c/wcar opts (c/publish "foobar" "Hello to foobar!"))

  ;; which will trigger:

  ;; (f1 '("message" "foobar" "Hello to foobar!"))

  ;; and also

  ;; (f2 '("pmessage" "foo*" "foobar" "Hello to foobar!"))

  ;; you can adjust subscriptions and/or handlers:

  (c/with-open-listener listener
    ;; unsubscribe from every channel (leave patterns alone)
    (c/unsubscribe)
    (c/psubscribe "an-extra-channel"))

  (swap! (:state listener) assoc "*extra*" (fn [x] (println "EXTRA: " x)))

  ;; remember to close the listener when you're done with it:

  (c/close-listener listener)

  ;; note that subscriptions are connection-local: you can have three different
  ;; listeners each listening for different messages, using different
  ;; handlers. This is great stuff.

  ;; reply parsing

  ;; want a little more control over how server replies are parsed? You have all
  ;; the control you need:

  (c/wcar opts
   (c/ping)
   (c/with-parser string/lower-case (c/ping) (c/ping))
   (c/ping))

  ;; binary data

  ;; Carmine's serializer has no problem handling arbitrary `byte[]` data. But
  ;; the serializer involves overhead that may not always be desirable. So for
  ;; maximum flexibility Carmine gives you automatic, zero-overhead read and
  ;; write facilities for raw binary data:

  (c/wcar opts
   (c/set "bin-key" (byte-array 50))
   (c/get "bin-key"))


  ;; message queue

  ;; Redis makes a great message queue server:

  (def my-worker
    (car-mq/worker opts "my-queue"
                   {:handler (fn [{:keys [message attempt]}]
                               (println "Received" message)
                               {:status :success})}))

  (c/wcar opts (car-mq/enqueue "my-queue" "my message!"))

  (car-mq/stop my-worker)

  ;; guarantees:

  ;; - messages are persistent (durable) as per Redis config
  ;; - each message will be handled once and only once
  ;; - handling is fault-tolerant: a message cannot be lost due to handler crash
  ;; - message de-duplication can be requested on an ad hoc (per message) basis.
  ;;   In these cases, the same message cannot ever be entered into the queue
  ;;   more than once simultaneously or within a (per message) specifiable
  ;;   post-handling backoff period.


  ;; distributed locks

  (locks/with-lock
    ;; connection details
    opts
    ;; lock name/identifier
    "my-lock"
    ;; time to hold lock
    1000
    ;; time to wait (block) for lock acquisition
    500
    (println "This was printed under lock!"))

  ;; again: simple, distributed, fault-tolerant, and fast.


  ;; Tundra (beta)

  ;; Redis is a beautifully designed datastore that makes some explicit
  ;; engineering tradeoffs. Probably the most important: your data must fit in
  ;; memory. Tundra helps relax this limitation: only your hot data need fit in
  ;; memory. How does it work?

  ;; - use Tundra's `dirty` command any time you modify/create evictable keys
  ;; - use `worker` to create a threaded worker that'll automatically replicate
  ;;   dirty keys to your secondary datastore
  ;; - when a dirty key hasn't been used in a specified TTL, it will be
  ;;   automatically evicted from Redis (eviction is optional if you just want
  ;;   to use Tundra as a backup/just-in-case mechanism)
  ;; - use `ensure-ks` any time you want to use evictable keys - this'll extend
  ;;   their TTL or fetch them from your datastore as necessary

  ;; that's it: two Redis commands, and a worker! Tundra uses Redis' own
  ;; dump/restore mechanism for replication, and Carmine's own message queue to
  ;; coordinate the replication worker.

  ;; Tundra can be very easily extended to any K/V-capable
  ;; datastore. Implementations are provided out-the-box for: Disk, Amazon S3
  ;; and DynamoDB.

  (def my-tundra-store
    (t/tundra-store
     ;; a datastore that implements the necessary (easily-extendable) protocol:
     (s3/s3-datastore {:access-key "" :secret-key ""}
                      "my-bucket/my-folder")))

  ;; now we have access to the Tundra API:

  ;; create a replication worker

  (t/worker my-tundra-store {} {})

  ;; queue for replication

  (t/dirty my-tundra-store "foo:bar1" "foo:bar2" ...)

  ;; fetch from replica when necessary

  (t/ensure-ks my-tundra-store "foo:bar1" "foo:bar2" ...)

  ;; note that this API makes it convenient to use several different datastores
  ;; simultaneously (perhaps for different purposes with different latency
  ;; requirements).

)
