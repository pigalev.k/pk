(ns hello-kubernetes-api.core
  (:require [kubernetes.api.v1 :as k8s]
            [clojure.core.async :refer [<!! <!]]))


;; First, set up a (micro)k8s cluster and run a kubernetes proxy with kubectl
;; proxy --port=8080


;; Make a context for querying k8s
(def ctx (k8s/make-context "http://localhost:8080"))


;; List all nodes
(<!! (k8s/list-namespaced-node ctx))
;; {:kind "NodeList",
;;  :apiVersion "v1",
;;  :metadata {:selfLink "/api/v1/nodes", :resourceVersion "29092"},
;;  :items
;;  [{:metadata
;;    {:name "quicksilver",
;;     :selfLink "/api/v1/nodes/quicksilver",
;;     :uid "67aefa3c-12e6-40dd-8b9d-0c6769cf3907",
;;     :resourceVersion "29023",
;;     :creationTimestamp "2019-12-10T13:09:58Z",
;;     :labels
;;     {:beta.kubernetes.io/arch "amd64",
;;      :beta.kubernetes.io/os "linux",
;;      :kubernetes.io/arch "amd64",
;;      :kubernetes.io/hostname "quicksilver",
;;      :kubernetes.io/os "linux",
;;      :microk8s.io/cluster "true"},
;;     :annotations
;;     {:node.alpha.kubernetes.io/ttl "0",
;;      :volumes.kubernetes.io/controller-managed-attach-detach "true"}},
;;    :spec {},
;;    :status
;;    {:capacity
;;     {:cpu "4",
;;      :ephemeral-storage "95596964Ki",
;;      :hugepages-1Gi "0",
;;      :hugepages-2Mi "0",
;;      :memory "7576296Ki",
;;      :pods "110"},
;;     :allocatable
;;     {:cpu "4",
;;      :ephemeral-storage "94548388Ki",
;;      :hugepages-1Gi "0",
;;      :hugepages-2Mi "0",
;;      :memory "7473896Ki",
;;      :pods "110"},
;;     :conditions
;;     [{:type "MemoryPressure",
;;       :status "False",
;;       :lastHeartbeatTime "2019-12-10T19:41:32Z",
;;       :lastTransitionTime "2019-12-10T13:09:49Z",
;;       :reason "KubeletHasSufficientMemory",
;;       :message "kubelet has sufficient memory available"}
;;      {:type "DiskPressure",
;;       :status "False",
;;       :lastHeartbeatTime "2019-12-10T19:41:32Z",
;;       :lastTransitionTime "2019-12-10T13:09:49Z",
;;       :reason "KubeletHasNoDiskPressure",
;;       :message "kubelet has no disk pressure"}
;;      {:type "PIDPressure",
;;       :status "False",
;;       :lastHeartbeatTime "2019-12-10T19:41:32Z",
;;       :lastTransitionTime "2019-12-10T13:09:49Z",
;;       :reason "KubeletHasSufficientPID",
;;       :message "kubelet has sufficient PID available"}
;;      {:type "Ready",
;;       :status "True",
;;       :lastHeartbeatTime "2019-12-10T19:41:32Z",
;;       :lastTransitionTime "2019-12-10T13:27:20Z",
;;       :reason "KubeletReady",
;;       :message "kubelet is posting ready status. AppArmor enabled"}],
;;     :addresses
;;     [{:type "InternalIP", :address "192.168.0.101"}
;;      {:type "Hostname", :address "quicksilver"}],
;;     :daemonEndpoints {:kubeletEndpoint {:Port 10250}},
;;     :nodeInfo
;;     {:containerRuntimeVersion "containerd://1.2.5",
;;      :machineID "312edcfbabd3435784b57c254a85580f",
;;      :architecture "amd64",
;;      :kubeProxyVersion "v1.16.3",
;;      :kernelVersion "5.3.0-24-generic",
;;      :kubeletVersion "v1.16.3",
;;      :systemUUID "3be8dfce-9255-0342-b253-9b8488ce10d8",
;;      :operatingSystem "linux",
;;      :bootID "7f67d40c-1aa9-4914-88ff-ee03fae3337b",
;;      :osImage "Ubuntu 19.10"},
;;     :images
;;     [{:names
;;       ["gcr.io/google-samples/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af"
;;        "gcr.io/google-samples/kubernetes-bootcamp:v1"],
;;       :sizeBytes 83642968}
;;      {:names
;;       ["docker.io/cdkbot/registry-amd64@sha256:9dc5ad9e0b3ed59afa4febe8d663fd545628c60886a2c9fcd0e186e5cf6343ef"
;;        "docker.io/cdkbot/registry-amd64:2.6"],
;;       :sizeBytes 54673787}
;;      {:names
;;       ["k8s.gcr.io/heapster-grafana-amd64@sha256:4a472eb4df03f4f557d80e7c6b903d9c8fe31493108b99fbd6da6540b5448d70"
;;        "k8s.gcr.io/heapster-grafana-amd64:v4.4.3"],
;;       :sizeBytes 51566280}
;;      {:names
;;       ["docker.io/kubernetesui/dashboard@sha256:ae756074fa3d1b72c39aa98cfc6246c6923e7da3beaf350d80b91167be868871"
;;        "docker.io/kubernetesui/dashboard:v2.0.0-beta5"],
;;       :sizeBytes 37866311}
;;      {:names
;;       ["k8s.gcr.io/heapster-amd64@sha256:59fb34ffd902282d06fcc940a906df9787edf78651743f4c8c4abf8b3468c0e9"
;;        "k8s.gcr.io/heapster-amd64:v1.5.2"],
;;       :sizeBytes 20088732}
;;      {:names
;;       ["docker.io/kubernetesui/metrics-scraper@sha256:2026f9f7558d0f25cc6bab74ea201b4e9d5668fbc378ef64e13fddaea570efc0"
;;        "docker.io/kubernetesui/metrics-scraper:v1.0.2"],
;;       :sizeBytes 16120983}
;;      {:names
;;       ["docker.io/coredns/coredns@sha256:e83beb5e43f8513fa735e77ffc5859640baea30a882a11cc75c4c3244a737d3c"
;;        "docker.io/coredns/coredns:1.5.0"],
;;       :sizeBytes 13341835}
;;      {:names
;;       ["docker.io/cdkbot/hostpath-provisioner-amd64@sha256:339f78eabc68ffb1656d584e41f121cb4d2b667565428c8dde836caf5b8a0228"
;;        "docker.io/cdkbot/hostpath-provisioner-amd64:1.0.0"],
;;       :sizeBytes 9745308}
;;      {:names
;;       ["docker.io/cdkbot/addon-resizer-amd64@sha256:a5ff31fb60d32e02780441fc81bc91dd549097d6afeef3c6decb6005289795af"
;;        "docker.io/cdkbot/addon-resizer-amd64:1.8.1"],
;;       :sizeBytes 8343051}
;;      {:names
;;       ["k8s.gcr.io/heapster-influxdb-amd64@sha256:f433e331c1865ad87bc5387589965528b78cd6b1b2f61697e589584d690c1edd"
;;        "k8s.gcr.io/heapster-influxdb-amd64:v1.3.3"],
;;       :sizeBytes 4748077}
;;      {:names
;;       ["k8s.gcr.io/pause@sha256:f78411e19d84a252e53bff71a4407a5686c46983a2c2eeed83929b888179acea"
;;        "k8s.gcr.io/pause:3.1"],
;;       :sizeBytes 317164}]}}]}


;; Pass an optional parameter
(<!! (k8s/list-namespaced-node ctx {:namespace "default"}))
;; {:kind "NodeList",
;;  :apiVersion "v1",
;;  :metadata {:selfLink "/api/v1/nodes", :resourceVersion "29337"},
;;  :items
;;  [{:metadata
;;    {:name "quicksilver",
;;     :selfLink "/api/v1/nodes/quicksilver",
;;     :uid "67aefa3c-12e6-40dd-8b9d-0c6769cf3907",
;;     :resourceVersion "29316",
;;     :creationTimestamp "2019-12-10T13:09:58Z",
;;     :labels
;;     {:beta.kubernetes.io/arch "amd64",
;;      :beta.kubernetes.io/os "linux",
;;      :kubernetes.io/arch "amd64",
;;      :kubernetes.io/hostname "quicksilver",
;;      :kubernetes.io/os "linux",
;;      :microk8s.io/cluster "true"},
;;     :annotations
;;     {:node.alpha.kubernetes.io/ttl "0",
;;      :volumes.kubernetes.io/controller-managed-attach-detach "true"}},
;;    :spec {},
;;    :status
;;    {:capacity
;;     {:cpu "4",
;;      :ephemeral-storage "95596964Ki",
;;      :hugepages-1Gi "0",
;;      :hugepages-2Mi "0",
;;      :memory "7576296Ki",
;;      :pods "110"},
;;     :allocatable
;;     {:cpu "4",
;;      :ephemeral-storage "94548388Ki",
;;      :hugepages-1Gi "0",
;;      :hugepages-2Mi "0",
;;      :memory "7473896Ki",
;;      :pods "110"},
;;     :conditions
;;     [{:type "MemoryPressure",
;;       :status "False",
;;       :lastHeartbeatTime "2019-12-10T19:45:34Z",
;;       :lastTransitionTime "2019-12-10T13:09:49Z",
;;       :reason "KubeletHasSufficientMemory",
;;       :message "kubelet has sufficient memory available"}
;;      {:type "DiskPressure",
;;       :status "False",
;;       :lastHeartbeatTime "2019-12-10T19:45:34Z",
;;       :lastTransitionTime "2019-12-10T13:09:49Z",
;;       :reason "KubeletHasNoDiskPressure",
;;       :message "kubelet has no disk pressure"}
;;      {:type "PIDPressure",
;;       :status "False",
;;       :lastHeartbeatTime "2019-12-10T19:45:34Z",
;;       :lastTransitionTime "2019-12-10T13:09:49Z",
;;       :reason "KubeletHasSufficientPID",
;;       :message "kubelet has sufficient PID available"}
;;      {:type "Ready",
;;       :status "True",
;;       :lastHeartbeatTime "2019-12-10T19:45:34Z",
;;       :lastTransitionTime "2019-12-10T13:27:20Z",
;;       :reason "KubeletReady",
;;       :message "kubelet is posting ready status. AppArmor enabled"}],
;;     :addresses
;;     [{:type "InternalIP", :address "192.168.0.101"}
;;      {:type "Hostname", :address "quicksilver"}],
;;     :daemonEndpoints {:kubeletEndpoint {:Port 10250}},
;;     :nodeInfo
;;     {:containerRuntimeVersion "containerd://1.2.5",
;;      :machineID "312edcfbabd3435784b57c254a85580f",
;;      :architecture "amd64",
;;      :kubeProxyVersion "v1.16.3",
;;      :kernelVersion "5.3.0-24-generic",
;;      :kubeletVersion "v1.16.3",
;;      :systemUUID "3be8dfce-9255-0342-b253-9b8488ce10d8",
;;      :operatingSystem "linux",
;;      :bootID "7f67d40c-1aa9-4914-88ff-ee03fae3337b",
;;      :osImage "Ubuntu 19.10"},
;;     :images
;;     [{:names
;;       ["gcr.io/google-samples/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af"
;;        "gcr.io/google-samples/kubernetes-bootcamp:v1"],
;;       :sizeBytes 83642968}
;;      {:names
;;       ["docker.io/cdkbot/registry-amd64@sha256:9dc5ad9e0b3ed59afa4febe8d663fd545628c60886a2c9fcd0e186e5cf6343ef"
;;        "docker.io/cdkbot/registry-amd64:2.6"],
;;       :sizeBytes 54673787}
;;      {:names
;;       ["k8s.gcr.io/heapster-grafana-amd64@sha256:4a472eb4df03f4f557d80e7c6b903d9c8fe31493108b99fbd6da6540b5448d70"
;;        "k8s.gcr.io/heapster-grafana-amd64:v4.4.3"],
;;       :sizeBytes 51566280}
;;      {:names
;;       ["docker.io/kubernetesui/dashboard@sha256:ae756074fa3d1b72c39aa98cfc6246c6923e7da3beaf350d80b91167be868871"
;;        "docker.io/kubernetesui/dashboard:v2.0.0-beta5"],
;;       :sizeBytes 37866311}
;;      {:names
;;       ["k8s.gcr.io/heapster-amd64@sha256:59fb34ffd902282d06fcc940a906df9787edf78651743f4c8c4abf8b3468c0e9"
;;        "k8s.gcr.io/heapster-amd64:v1.5.2"],
;;       :sizeBytes 20088732}
;;      {:names
;;       ["docker.io/kubernetesui/metrics-scraper@sha256:2026f9f7558d0f25cc6bab74ea201b4e9d5668fbc378ef64e13fddaea570efc0"
;;        "docker.io/kubernetesui/metrics-scraper:v1.0.2"],
;;       :sizeBytes 16120983}
;;      {:names
;;       ["docker.io/coredns/coredns@sha256:e83beb5e43f8513fa735e77ffc5859640baea30a882a11cc75c4c3244a737d3c"
;;        "docker.io/coredns/coredns:1.5.0"],
;;       :sizeBytes 13341835}
;;      {:names
;;       ["docker.io/cdkbot/hostpath-provisioner-amd64@sha256:339f78eabc68ffb1656d584e41f121cb4d2b667565428c8dde836caf5b8a0228"
;;        "docker.io/cdkbot/hostpath-provisioner-amd64:1.0.0"],
;;       :sizeBytes 9745308}
;;      {:names
;;       ["docker.io/cdkbot/addon-resizer-amd64@sha256:a5ff31fb60d32e02780441fc81bc91dd549097d6afeef3c6decb6005289795af"
;;        "docker.io/cdkbot/addon-resizer-amd64:1.8.1"],
;;       :sizeBytes 8343051}
;;      {:names
;;       ["k8s.gcr.io/heapster-influxdb-amd64@sha256:f433e331c1865ad87bc5387589965528b78cd6b1b2f61697e589584d690c1edd"
;;        "k8s.gcr.io/heapster-influxdb-amd64:v1.3.3"],
;;       :sizeBytes 4748077}
;;      {:names
;;       ["k8s.gcr.io/pause@sha256:f78411e19d84a252e53bff71a4407a5686c46983a2c2eeed83929b888179acea"
;;        "k8s.gcr.io/pause:3.1"],
;;       :sizeBytes 317164}]}}]}


;; Create a pod
(<!! (k8s/create-namespaced-pod ctx
                                {:kind "Pod" :metadata {:name "test"}
                                 :spec {:containers [{:name "nginx" :image "nginx"}]}}
                                {:namespace "default"}))
;; {:kind "Pod",
;;  :apiVersion "v1",
;;  :metadata
;;  {:name "test",
;;   :namespace "default",
;;   :selfLink "/api/v1/namespaces/default/pods/test",
;;   :uid "7cc162a3-27cc-48f8-8554-8238f597da75",
;;   :resourceVersion "29440",
;;   :creationTimestamp "2019-12-10T19:47:15Z"},
;;  :spec
;;  {:serviceAccount "default",
;;   :securityContext {},
;;   :containers
;;   [{:name "nginx",
;;     :image "nginx",
;;     :resources {},
;;     :volumeMounts
;;     [{:name "default-token-svm7k",
;;       :readOnly true,
;;       :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;     :terminationMessagePath "/dev/termination-log",
;;     :terminationMessagePolicy "File",
;;     :imagePullPolicy "Always"}],
;;   :volumes
;;   [{:name "default-token-svm7k",
;;     :secret {:secretName "default-token-svm7k", :defaultMode 420}}],
;;   :schedulerName "default-scheduler",
;;   :priority 0,
;;   :restartPolicy "Always",
;;   :dnsPolicy "ClusterFirst",
;;   :tolerations
;;   [{:key "node.kubernetes.io/not-ready",
;;     :operator "Exists",
;;     :effect "NoExecute",
;;     :tolerationSeconds 300}
;;    {:key "node.kubernetes.io/unreachable",
;;     :operator "Exists",
;;     :effect "NoExecute",
;;     :tolerationSeconds 300}],
;;   :serviceAccountName "default",
;;   :terminationGracePeriodSeconds 30,
;;   :enableServiceLinks true},
;;  :status {:phase "Pending", :qosClass "BestEffort"}}


;; List of all pods
(<!! (k8s/list-pod ctx))
;; {:kind "PodList",
;;  :apiVersion "v1",
;;  :metadata {:selfLink "/api/v1/pods", :resourceVersion "29913"},
;;  :items
;;  [{:metadata
;;    {:labels {:app "registry", :pod-template-hash "d7d7c8bc9"},
;;     :ownerReferences
;;     [{:apiVersion "apps/v1",
;;       :kind "ReplicaSet",
;;       :name "registry-d7d7c8bc9",
;;       :uid "a8d90317-4f81-4cf9-beaa-c2f15c37fa13",
;;       :controller true,
;;       :blockOwnerDeletion true}],
;;     :creationTimestamp "2019-12-10T13:10:42Z",
;;     :uid "be89bf14-08ac-44c7-90b7-7b684c1e4fad",
;;     :name "registry-d7d7c8bc9-cr4tw",
;;     :resourceVersion "21561",
;;     :selfLink
;;     "/api/v1/namespaces/container-registry/pods/registry-d7d7c8bc9-cr4tw",
;;     :namespace "container-registry",
;;     :generateName "registry-d7d7c8bc9-"},
;;    :spec
;;    {:serviceAccount "default",
;;     :securityContext {},
;;     :containers
;;     [{:terminationMessagePolicy "File",
;;       :volumeMounts
;;       [{:name "registry-data", :mountPath "/var/lib/registry"}
;;        {:name "default-token-mwqpl",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :name "registry",
;;       :env
;;       [{:name "REGISTRY_HTTP_ADDR", :value ":5000"}
;;        {:name "REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY",
;;         :value "/var/lib/registry"}
;;        {:name "REGISTRY_STORAGE_DELETE_ENABLED", :value "yes"}],
;;       :ports [{:name "registry", :containerPort 5000, :protocol "TCP"}],
;;       :terminationMessagePath "/dev/termination-log",
;;       :imagePullPolicy "IfNotPresent",
;;       :image "cdkbot/registry-amd64:2.6",
;;       :resources {}}],
;;     :volumes
;;     [{:name "registry-data",
;;       :persistentVolumeClaim {:claimName "registry-claim"}}
;;      {:name "default-token-mwqpl",
;;       :secret {:secretName "default-token-mwqpl", :defaultMode 420}}],
;;     :schedulerName "default-scheduler",
;;     :priority 0,
;;     :nodeName "quicksilver",
;;     :restartPolicy "Always",
;;     :dnsPolicy "ClusterFirst",
;;     :tolerations
;;     [{:key "node.kubernetes.io/not-ready",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}
;;      {:key "node.kubernetes.io/unreachable",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}],
;;     :serviceAccountName "default",
;;     :terminationGracePeriodSeconds 30,
;;     :enableServiceLinks true},
;;    :status
;;    {:phase "Running",
;;     :conditions
;;     [{:type "Initialized",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:11:21Z"}
;;      {:type "Ready",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "ContainersReady",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "PodScheduled",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:11:21Z"}],
;;     :hostIP "192.168.0.101",
;;     :podIP "10.1.84.16",
;;     :podIPs [{:ip "10.1.84.16"}],
;;     :startTime "2019-12-10T13:11:21Z",
;;     :containerStatuses
;;     [{:started true,
;;       :ready true,
;;       :name "registry",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:54Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:11:42Z",
;;         :finishedAt "2019-12-10T17:57:43Z",
;;         :containerID
;;         "containerd://5746e718f6125a898faad5a5ea554e088a65d187754965e2556d7d01a8fcbf61"}},
;;       :containerID
;;       "containerd://1f21be25daefe40d1f2c487b4f9067de259215cb27a2e435188646fd8c796c1d",
;;       :imageID
;;       "docker.io/cdkbot/registry-amd64@sha256:9dc5ad9e0b3ed59afa4febe8d663fd545628c60886a2c9fcd0e186e5cf6343ef",
;;       :image "docker.io/cdkbot/registry-amd64:2.6"}],
;;     :qosClass "BestEffort"}}
;;   {:metadata
;;    {:labels {:app "kubernetes-bootcamp", :pod-template-hash "69fbc6f4cf"},
;;     :ownerReferences
;;     [{:apiVersion "apps/v1",
;;       :kind "ReplicaSet",
;;       :name "kubernetes-bootcamp-69fbc6f4cf",
;;       :uid "6f35a805-63e2-4959-93ef-cae2cd36c411",
;;       :controller true,
;;       :blockOwnerDeletion true}],
;;     :creationTimestamp "2019-12-10T13:28:47Z",
;;     :uid "80b08051-bcaa-4e6a-8fdb-1d9d345b6815",
;;     :name "kubernetes-bootcamp-69fbc6f4cf-wzknc",
;;     :resourceVersion "21557",
;;     :selfLink
;;     "/api/v1/namespaces/default/pods/kubernetes-bootcamp-69fbc6f4cf-wzknc",
;;     :namespace "default",
;;     :generateName "kubernetes-bootcamp-69fbc6f4cf-"},
;;    :spec
;;    {:serviceAccount "default",
;;     :securityContext {},
;;     :containers
;;     [{:name "kubernetes-bootcamp",
;;       :image "gcr.io/google-samples/kubernetes-bootcamp:v1",
;;       :resources {},
;;       :volumeMounts
;;       [{:name "default-token-svm7k",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :terminationMessagePath "/dev/termination-log",
;;       :terminationMessagePolicy "File",
;;       :imagePullPolicy "IfNotPresent"}],
;;     :volumes
;;     [{:name "default-token-svm7k",
;;       :secret {:secretName "default-token-svm7k", :defaultMode 420}}],
;;     :schedulerName "default-scheduler",
;;     :priority 0,
;;     :nodeName "quicksilver",
;;     :restartPolicy "Always",
;;     :dnsPolicy "ClusterFirst",
;;     :tolerations
;;     [{:key "node.kubernetes.io/not-ready",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}
;;      {:key "node.kubernetes.io/unreachable",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}],
;;     :serviceAccountName "default",
;;     :terminationGracePeriodSeconds 30,
;;     :enableServiceLinks true},
;;    :status
;;    {:phase "Running",
;;     :conditions
;;     [{:type "Initialized",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:28:47Z"}
;;      {:type "Ready",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "ContainersReady",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "PodScheduled",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:28:47Z"}],
;;     :hostIP "192.168.0.101",
;;     :podIP "10.1.84.17",
;;     :podIPs [{:ip "10.1.84.17"}],
;;     :startTime "2019-12-10T13:28:47Z",
;;     :containerStatuses
;;     [{:started true,
;;       :ready true,
;;       :name "kubernetes-bootcamp",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:56Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:29:15Z",
;;         :finishedAt "2019-12-10T17:57:44Z",
;;         :containerID
;;         "containerd://f1ad48f52b60bdb45c02594fd9d8ffe033cc0f82ca4df8303dbeb5d513fb6dd9"}},
;;       :containerID
;;       "containerd://9838b2e68429b310951bc56504dcbabc4f366d139ff8fd12a7e1bc1c573bc369",
;;       :imageID
;;       "gcr.io/google-samples/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af",
;;       :image "gcr.io/google-samples/kubernetes-bootcamp:v1"}],
;;     :qosClass "BestEffort"}}
;;   {:metadata
;;    {:name "test",
;;     :namespace "default",
;;     :selfLink "/api/v1/namespaces/default/pods/test",
;;     :uid "7cc162a3-27cc-48f8-8554-8238f597da75",
;;     :resourceVersion "29469",
;;     :creationTimestamp "2019-12-10T19:47:15Z"},
;;    :spec
;;    {:serviceAccount "default",
;;     :securityContext {},
;;     :containers
;;     [{:name "nginx",
;;       :image "nginx",
;;       :resources {},
;;       :volumeMounts
;;       [{:name "default-token-svm7k",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :terminationMessagePath "/dev/termination-log",
;;       :terminationMessagePolicy "File",
;;       :imagePullPolicy "Always"}],
;;     :volumes
;;     [{:name "default-token-svm7k",
;;       :secret {:secretName "default-token-svm7k", :defaultMode 420}}],
;;     :schedulerName "default-scheduler",
;;     :priority 0,
;;     :nodeName "quicksilver",
;;     :restartPolicy "Always",
;;     :dnsPolicy "ClusterFirst",
;;     :tolerations
;;     [{:key "node.kubernetes.io/not-ready",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}
;;      {:key "node.kubernetes.io/unreachable",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}],
;;     :serviceAccountName "default",
;;     :terminationGracePeriodSeconds 30,
;;     :enableServiceLinks true},
;;    :status
;;    {:phase "Running",
;;     :conditions
;;     [{:type "Initialized",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T19:47:15Z"}
;;      {:type "Ready",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T19:47:34Z"}
;;      {:type "ContainersReady",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T19:47:34Z"}
;;      {:type "PodScheduled",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T19:47:15Z"}],
;;     :hostIP "192.168.0.101",
;;     :podIP "10.1.84.21",
;;     :podIPs [{:ip "10.1.84.21"}],
;;     :startTime "2019-12-10T19:47:15Z",
;;     :containerStatuses
;;     [{:started true,
;;       :ready true,
;;       :name "nginx",
;;       :restartCount 0,
;;       :state {:running {:startedAt "2019-12-10T19:47:34Z"}},
;;       :lastState {},
;;       :containerID
;;       "containerd://48e21af0b31385e7f6f48ab8bc78cf4d8472596ae1ee9bc6fa9bdc6df0d35af4",
;;       :imageID
;;       "docker.io/library/nginx@sha256:50cf965a6e08ec5784009d0fccb380fc479826b6e0e65684d9879170a9df8566",
;;       :image "docker.io/library/nginx:latest"}],
;;     :qosClass "BestEffort"}}
;;   {:metadata
;;    {:labels {:k8s-app "kube-dns", :pod-template-hash "9b8997588"},
;;     :ownerReferences
;;     [{:apiVersion "apps/v1",
;;       :kind "ReplicaSet",
;;       :name "coredns-9b8997588",
;;       :uid "de3a44ad-0b49-499e-a1ca-379a099da88f",
;;       :controller true,
;;       :blockOwnerDeletion true}],
;;     :creationTimestamp "2019-12-10T13:10:19Z",
;;     :uid "1d4551e2-1def-4b0e-9ee8-2ad87d744d57",
;;     :name "coredns-9b8997588-8qlw6",
;;     :resourceVersion "21567",
;;     :selfLink "/api/v1/namespaces/kube-system/pods/coredns-9b8997588-8qlw6",
;;     :annotations #:scheduler.alpha.kubernetes.io{:critical-pod ""},
;;     :namespace "kube-system",
;;     :generateName "coredns-9b8997588-"},
;;    :spec
;;    {:serviceAccount "coredns",
;;     :securityContext {},
;;     :priorityClassName "system-cluster-critical",
;;     :containers
;;     [{:terminationMessagePolicy "File",
;;       :args ["-conf" "/etc/coredns/Corefile"],
;;       :securityContext
;;       {:capabilities {:add ["NET_BIND_SERVICE"], :drop ["all"]},
;;        :readOnlyRootFilesystem true,
;;        :allowPrivilegeEscalation false},
;;       :volumeMounts
;;       [{:name "config-volume", :readOnly true, :mountPath "/etc/coredns"}
;;        {:name "coredns-token-x98r6",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :readinessProbe
;;       {:httpGet {:path "/ready", :port 8181, :scheme "HTTP"},
;;        :timeoutSeconds 1,
;;        :periodSeconds 10,
;;        :successThreshold 1,
;;        :failureThreshold 3},
;;       :name "coredns",
;;       :ports
;;       [{:name "dns", :containerPort 53, :protocol "UDP"}
;;        {:name "dns-tcp", :containerPort 53, :protocol "TCP"}
;;        {:name "metrics", :containerPort 9153, :protocol "TCP"}],
;;       :livenessProbe
;;       {:httpGet {:path "/health", :port 8080, :scheme "HTTP"},
;;        :initialDelaySeconds 60,
;;        :timeoutSeconds 5,
;;        :periodSeconds 10,
;;        :successThreshold 1,
;;        :failureThreshold 5},
;;       :terminationMessagePath "/dev/termination-log",
;;       :imagePullPolicy "IfNotPresent",
;;       :image "coredns/coredns:1.5.0",
;;       :resources
;;       {:limits {:memory "170Mi"}, :requests {:cpu "100m", :memory "70Mi"}}}],
;;     :volumes
;;     [{:name "config-volume",
;;       :configMap
;;       {:name "coredns",
;;        :items [{:key "Corefile", :path "Corefile"}],
;;        :defaultMode 420}}
;;      {:name "coredns-token-x98r6",
;;       :secret {:secretName "coredns-token-x98r6", :defaultMode 420}}],
;;     :schedulerName "default-scheduler",
;;     :priority 2000000000,
;;     :nodeName "quicksilver",
;;     :restartPolicy "Always",
;;     :dnsPolicy "Default",
;;     :tolerations
;;     [{:key "CriticalAddonsOnly", :operator "Exists"}
;;      {:key "node.kubernetes.io/not-ready",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}
;;      {:key "node.kubernetes.io/unreachable",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}],
;;     :serviceAccountName "coredns",
;;     :terminationGracePeriodSeconds 30,
;;     :enableServiceLinks true},
;;    :status
;;    {:phase "Running",
;;     :conditions
;;     [{:type "Initialized",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:19Z"}
;;      {:type "Ready",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:59:06Z"}
;;      {:type "ContainersReady",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:59:06Z"}
;;      {:type "PodScheduled",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:19Z"}],
;;     :hostIP "192.168.0.101",
;;     :podIP "10.1.84.20",
;;     :podIPs [{:ip "10.1.84.20"}],
;;     :startTime "2019-12-10T13:10:19Z",
;;     :containerStatuses
;;     [{:started true,
;;       :ready true,
;;       :name "coredns",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:57Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:10:36Z",
;;         :finishedAt "2019-12-10T17:57:44Z",
;;         :containerID
;;         "containerd://c05157679bb81db5712c22241280be76279bf50000d5fd08dd71d27bd4e737dd"}},
;;       :containerID
;;       "containerd://71ec07872f076d4e1657559944c8f59cfc49957423145909ba099a806cb660c7",
;;       :imageID
;;       "docker.io/coredns/coredns@sha256:e83beb5e43f8513fa735e77ffc5859640baea30a882a11cc75c4c3244a737d3c",
;;       :image "docker.io/coredns/coredns:1.5.0"}],
;;     :qosClass "Burstable"}}
;;   {:metadata
;;    {:labels
;;     {:k8s-app "dashboard-metrics-scraper", :pod-template-hash "687667bb6c"},
;;     :ownerReferences
;;     [{:apiVersion "apps/v1",
;;       :kind "ReplicaSet",
;;       :name "dashboard-metrics-scraper-687667bb6c",
;;       :uid "88ebfe77-817d-44ec-8035-aec377d52551",
;;       :controller true,
;;       :blockOwnerDeletion true}],
;;     :creationTimestamp "2019-12-10T13:10:27Z",
;;     :uid "16e4849a-63dc-4ac9-89d7-69941678b245",
;;     :name "dashboard-metrics-scraper-687667bb6c-l6mbk",
;;     :resourceVersion "21558",
;;     :selfLink
;;     "/api/v1/namespaces/kube-system/pods/dashboard-metrics-scraper-687667bb6c-l6mbk",
;;     :namespace "kube-system",
;;     :generateName "dashboard-metrics-scraper-687667bb6c-"},
;;    :spec
;;    {:serviceAccount "kubernetes-dashboard",
;;     :securityContext {},
;;     :containers
;;     [{:terminationMessagePolicy "File",
;;       :volumeMounts
;;       [{:name "tmp-volume", :mountPath "/tmp"}
;;        {:name "kubernetes-dashboard-token-nrbwn",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :name "dashboard-metrics-scraper",
;;       :ports [{:containerPort 8000, :protocol "TCP"}],
;;       :livenessProbe
;;       {:httpGet {:path "/", :port 8000, :scheme "HTTP"},
;;        :initialDelaySeconds 30,
;;        :timeoutSeconds 30,
;;        :periodSeconds 10,
;;        :successThreshold 1,
;;        :failureThreshold 3},
;;       :terminationMessagePath "/dev/termination-log",
;;       :imagePullPolicy "IfNotPresent",
;;       :image "kubernetesui/metrics-scraper:v1.0.2",
;;       :resources {}}],
;;     :volumes
;;     [{:name "tmp-volume", :emptyDir {}}
;;      {:name "kubernetes-dashboard-token-nrbwn",
;;       :secret
;;       {:secretName "kubernetes-dashboard-token-nrbwn", :defaultMode 420}}],
;;     :schedulerName "default-scheduler",
;;     :priority 0,
;;     :nodeName "quicksilver",
;;     :restartPolicy "Always",
;;     :dnsPolicy "ClusterFirst",
;;     :tolerations
;;     [{:key "node-role.kubernetes.io/master", :effect "NoSchedule"}
;;      {:key "node.kubernetes.io/not-ready",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}
;;      {:key "node.kubernetes.io/unreachable",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}],
;;     :serviceAccountName "kubernetes-dashboard",
;;     :terminationGracePeriodSeconds 30,
;;     :enableServiceLinks true},
;;    :status
;;    {:phase "Running",
;;     :conditions
;;     [{:type "Initialized",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:29Z"}
;;      {:type "Ready",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "ContainersReady",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "PodScheduled",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:27Z"}],
;;     :hostIP "192.168.0.101",
;;     :podIP "10.1.84.19",
;;     :podIPs [{:ip "10.1.84.19"}],
;;     :startTime "2019-12-10T13:10:29Z",
;;     :containerStatuses
;;     [{:started true,
;;       :ready true,
;;       :name "dashboard-metrics-scraper",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:57Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:10:45Z",
;;         :finishedAt "2019-12-10T17:57:44Z",
;;         :containerID
;;         "containerd://c26d9fc89898bc60d20c68f5526cb1fdce4576837f3b6fa71bac6d92f5d88a42"}},
;;       :containerID
;;       "containerd://b9a38cd28ec6672c1547e293a2f8221f71d54dddc3575a73af845454d65597c6",
;;       :imageID
;;       "docker.io/kubernetesui/metrics-scraper@sha256:2026f9f7558d0f25cc6bab74ea201b4e9d5668fbc378ef64e13fddaea570efc0",
;;       :image "docker.io/kubernetesui/metrics-scraper:v1.0.2"}],
;;     :qosClass "BestEffort"}}
;;   {:metadata
;;    {:labels
;;     {:k8s-app "heapster", :pod-template-hash "5c58f64f8b", :version "v1.5.2"},
;;     :ownerReferences
;;     [{:apiVersion "apps/v1",
;;       :kind "ReplicaSet",
;;       :name "heapster-v1.5.2-5c58f64f8b",
;;       :uid "b0a8dcb7-2fda-41ea-8d4d-c48668bfda4c",
;;       :controller true,
;;       :blockOwnerDeletion true}],
;;     :creationTimestamp "2019-12-10T13:10:27Z",
;;     :uid "d59cca8a-a89c-4d7e-a7aa-60335e81b152",
;;     :name "heapster-v1.5.2-5c58f64f8b-pt7g8",
;;     :resourceVersion "21562",
;;     :selfLink
;;     "/api/v1/namespaces/kube-system/pods/heapster-v1.5.2-5c58f64f8b-pt7g8",
;;     :annotations #:scheduler.alpha.kubernetes.io{:critical-pod ""},
;;     :namespace "kube-system",
;;     :generateName "heapster-v1.5.2-5c58f64f8b-"},
;;    :spec
;;    {:serviceAccount "heapster",
;;     :securityContext {},
;;     :priorityClassName "system-cluster-critical",
;;     :containers
;;     [{:terminationMessagePolicy "File",
;;       :volumeMounts
;;       [{:name "heapster-token-kjm9z",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :name "heapster",
;;       :command
;;       ["/heapster"
;;        "--source=kubernetes.summary_api:''"
;;        "--sink=influxdb:http://monitoring-influxdb:8086"],
;;       :livenessProbe
;;       {:httpGet {:path "/healthz", :port 8082, :scheme "HTTP"},
;;        :initialDelaySeconds 180,
;;        :timeoutSeconds 5,
;;        :periodSeconds 10,
;;        :successThreshold 1,
;;        :failureThreshold 3},
;;       :terminationMessagePath "/dev/termination-log",
;;       :imagePullPolicy "IfNotPresent",
;;       :image "k8s.gcr.io/heapster-amd64:v1.5.2",
;;       :resources {}}
;;      {:name "eventer",
;;       :image "k8s.gcr.io/heapster-amd64:v1.5.2",
;;       :command
;;       ["/eventer"
;;        "--source=kubernetes:''"
;;        "--sink=influxdb:http://monitoring-influxdb:8086"],
;;       :resources {},
;;       :volumeMounts
;;       [{:name "heapster-token-kjm9z",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :terminationMessagePath "/dev/termination-log",
;;       :terminationMessagePolicy "File",
;;       :imagePullPolicy "IfNotPresent"}
;;      {:terminationMessagePolicy "File",
;;       :volumeMounts
;;       [{:name "heapster-config-volume", :mountPath "/etc/config"}
;;        {:name "heapster-token-kjm9z",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :name "heapster-nanny",
;;       :command
;;       ["/pod_nanny"
;;        "--config-dir=/etc/config"
;;        "--cpu=80m"
;;        "--extra-cpu=0.5m"
;;        "--memory=140Mi"
;;        "--extra-memory=4Mi"
;;        "--threshold=5"
;;        "--deployment=heapster-v1.5.2"
;;        "--container=heapster"
;;        "--poll-period=300000"
;;        "--estimator=exponential"],
;;       :env
;;       [{:name "MY_POD_NAME",
;;         :valueFrom {:fieldRef {:apiVersion "v1", :fieldPath "metadata.name"}}}
;;        {:name "MY_POD_NAMESPACE",
;;         :valueFrom
;;         {:fieldRef {:apiVersion "v1", :fieldPath "metadata.namespace"}}}],
;;       :terminationMessagePath "/dev/termination-log",
;;       :imagePullPolicy "IfNotPresent",
;;       :image "cdkbot/addon-resizer-amd64:1.8.1",
;;       :resources
;;       {:limits {:cpu "50m", :memory "92360Ki"},
;;        :requests {:cpu "50m", :memory "92360Ki"}}}
;;      {:terminationMessagePolicy "File",
;;       :volumeMounts
;;       [{:name "eventer-config-volume", :mountPath "/etc/config"}
;;        {:name "heapster-token-kjm9z",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :name "eventer-nanny",
;;       :command
;;       ["/pod_nanny"
;;        "--config-dir=/etc/config"
;;        "--cpu=100m"
;;        "--extra-cpu=0m"
;;        "--memory=190Mi"
;;        "--extra-memory=500Ki"
;;        "--threshold=5"
;;        "--deployment=heapster-v1.5.2"
;;        "--container=eventer"
;;        "--poll-period=300000"
;;        "--estimator=exponential"],
;;       :env
;;       [{:name "MY_POD_NAME",
;;         :valueFrom {:fieldRef {:apiVersion "v1", :fieldPath "metadata.name"}}}
;;        {:name "MY_POD_NAMESPACE",
;;         :valueFrom
;;         {:fieldRef {:apiVersion "v1", :fieldPath "metadata.namespace"}}}],
;;       :terminationMessagePath "/dev/termination-log",
;;       :imagePullPolicy "IfNotPresent",
;;       :image "cdkbot/addon-resizer-amd64:1.8.1",
;;       :resources
;;       {:limits {:cpu "50m", :memory "92360Ki"},
;;        :requests {:cpu "50m", :memory "92360Ki"}}}],
;;     :volumes
;;     [{:name "heapster-config-volume",
;;       :configMap {:name "heapster-config", :defaultMode 420}}
;;      {:name "eventer-config-volume",
;;       :configMap {:name "eventer-config", :defaultMode 420}}
;;      {:name "heapster-token-kjm9z",
;;       :secret {:secretName "heapster-token-kjm9z", :defaultMode 420}}],
;;     :schedulerName "default-scheduler",
;;     :priority 2000000000,
;;     :nodeName "quicksilver",
;;     :restartPolicy "Always",
;;     :dnsPolicy "ClusterFirst",
;;     :tolerations
;;     [{:key "CriticalAddonsOnly", :operator "Exists"}
;;      {:key "node.kubernetes.io/not-ready",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}
;;      {:key "node.kubernetes.io/unreachable",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}],
;;     :serviceAccountName "heapster",
;;     :terminationGracePeriodSeconds 30,
;;     :enableServiceLinks true},
;;    :status
;;    {:phase "Running",
;;     :conditions
;;     [{:type "Initialized",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:29Z"}
;;      {:type "Ready",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:59:01Z"}
;;      {:type "ContainersReady",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:59:01Z"}
;;      {:type "PodScheduled",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:27Z"}],
;;     :hostIP "192.168.0.101",
;;     :podIP "10.1.84.18",
;;     :podIPs [{:ip "10.1.84.18"}],
;;     :startTime "2019-12-10T13:10:29Z",
;;     :containerStatuses
;;     [{:started true,
;;       :ready true,
;;       :name "eventer",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:58Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:11:00Z",
;;         :finishedAt "2019-12-10T17:57:43Z",
;;         :containerID
;;         "containerd://64201fa1aafd799d629494c05beeccd87891ec700f7193fba1b55c89d545aab6"}},
;;       :containerID
;;       "containerd://5dd806bbfef020855fda100bca3991e25f02b7e978159f8b9763a627ce46915d",
;;       :imageID
;;       "k8s.gcr.io/heapster-amd64@sha256:59fb34ffd902282d06fcc940a906df9787edf78651743f4c8c4abf8b3468c0e9",
;;       :image "k8s.gcr.io/heapster-amd64:v1.5.2"}
;;      {:started true,
;;       :ready true,
;;       :name "eventer-nanny",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:59Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:11:25Z",
;;         :finishedAt "2019-12-10T17:57:43Z",
;;         :containerID
;;         "containerd://738c4e065d74f8f5c1cc69a64d9098142b6b98801a332aa2576801250aa061cc"}},
;;       :containerID
;;       "containerd://e443753e1af203bc1d7054ad7a4768e007573ddce8c5e4889a43522a68ece68d",
;;       :imageID
;;       "docker.io/cdkbot/addon-resizer-amd64@sha256:a5ff31fb60d32e02780441fc81bc91dd549097d6afeef3c6decb6005289795af",
;;       :image "docker.io/cdkbot/addon-resizer-amd64:1.8.1"}
;;      {:started true,
;;       :ready true,
;;       :name "heapster",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:56Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:11:00Z",
;;         :finishedAt "2019-12-10T17:57:43Z",
;;         :containerID
;;         "containerd://64c951bfd7fda621d201cd9aa95909c1843894399c421c51703da9740dd5e3c8"}},
;;       :containerID
;;       "containerd://f070c1ccd1f7b940671a4233dad2904f3f7fc1ef649b852dbc473002aa4eeb72",
;;       :imageID
;;       "k8s.gcr.io/heapster-amd64@sha256:59fb34ffd902282d06fcc940a906df9787edf78651743f4c8c4abf8b3468c0e9",
;;       :image "k8s.gcr.io/heapster-amd64:v1.5.2"}
;;      {:started true,
;;       :ready true,
;;       :name "heapster-nanny",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:59Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:11:24Z",
;;         :finishedAt "2019-12-10T17:57:44Z",
;;         :containerID
;;         "containerd://a71a900e7fe889b0a42c9eec140b08ebb8de6a22f7521368eb44cff73e90b3f4"}},
;;       :containerID
;;       "containerd://6faedee188590ce94b6cd7476b1221cba99afac330d821c5557146a94bae0f26",
;;       :imageID
;;       "docker.io/cdkbot/addon-resizer-amd64@sha256:a5ff31fb60d32e02780441fc81bc91dd549097d6afeef3c6decb6005289795af",
;;       :image "docker.io/cdkbot/addon-resizer-amd64:1.8.1"}],
;;     :qosClass "Burstable"}}
;;   {:metadata
;;    {:labels {:k8s-app "hostpath-provisioner", :pod-template-hash "7b9cb5cdb4"},
;;     :ownerReferences
;;     [{:apiVersion "apps/v1",
;;       :kind "ReplicaSet",
;;       :name "hostpath-provisioner-7b9cb5cdb4",
;;       :uid "5f707e23-6bd6-48ab-81e1-d205e151a68d",
;;       :controller true,
;;       :blockOwnerDeletion true}],
;;     :creationTimestamp "2019-12-10T13:10:42Z",
;;     :uid "a78d6be9-505e-48c9-90f9-8584eab31241",
;;     :name "hostpath-provisioner-7b9cb5cdb4-mvvk4",
;;     :resourceVersion "21556",
;;     :selfLink
;;     "/api/v1/namespaces/kube-system/pods/hostpath-provisioner-7b9cb5cdb4-mvvk4",
;;     :namespace "kube-system",
;;     :generateName "hostpath-provisioner-7b9cb5cdb4-"},
;;    :spec
;;    {:serviceAccount "microk8s-hostpath",
;;     :securityContext {},
;;     :containers
;;     [{:name "hostpath-provisioner",
;;       :image "cdkbot/hostpath-provisioner-amd64:1.0.0",
;;       :env
;;       [{:name "NODE_NAME",
;;         :valueFrom {:fieldRef {:apiVersion "v1", :fieldPath "spec.nodeName"}}}
;;        {:name "PV_DIR", :value "/var/snap/microk8s/common/default-storage"}],
;;       :resources {},
;;       :volumeMounts
;;       [{:name "pv-volume",
;;         :mountPath "/var/snap/microk8s/common/default-storage"}
;;        {:name "microk8s-hostpath-token-c8nzj",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :terminationMessagePath "/dev/termination-log",
;;       :terminationMessagePolicy "File",
;;       :imagePullPolicy "IfNotPresent"}],
;;     :volumes
;;     [{:name "pv-volume",
;;       :hostPath {:path "/var/snap/microk8s/common/default-storage", :type ""}}
;;      {:name "microk8s-hostpath-token-c8nzj",
;;       :secret
;;       {:secretName "microk8s-hostpath-token-c8nzj", :defaultMode 420}}],
;;     :schedulerName "default-scheduler",
;;     :priority 0,
;;     :nodeName "quicksilver",
;;     :restartPolicy "Always",
;;     :dnsPolicy "ClusterFirst",
;;     :tolerations
;;     [{:key "node.kubernetes.io/not-ready",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}
;;      {:key "node.kubernetes.io/unreachable",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}],
;;     :serviceAccountName "microk8s-hostpath",
;;     :terminationGracePeriodSeconds 30,
;;     :enableServiceLinks true},
;;    :status
;;    {:phase "Running",
;;     :conditions
;;     [{:type "Initialized",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:42Z"}
;;      {:type "Ready",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "ContainersReady",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "PodScheduled",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:42Z"}],
;;     :hostIP "192.168.0.101",
;;     :podIP "10.1.84.14",
;;     :podIPs [{:ip "10.1.84.14"}],
;;     :startTime "2019-12-10T13:10:42Z",
;;     :containerStatuses
;;     [{:started true,
;;       :ready true,
;;       :name "hostpath-provisioner",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:55Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:11:19Z",
;;         :finishedAt "2019-12-10T17:57:44Z",
;;         :containerID
;;         "containerd://9fa1c3d9683de22dbede82596e2298671c004c05b04896bfc9b7f50a745a77b1"}},
;;       :containerID
;;       "containerd://4e271f6cb925b92a842c29ab98137a6ed49f27a8947ceeedec459f07346af5a1",
;;       :imageID
;;       "docker.io/cdkbot/hostpath-provisioner-amd64@sha256:339f78eabc68ffb1656d584e41f121cb4d2b667565428c8dde836caf5b8a0228",
;;       :image "docker.io/cdkbot/hostpath-provisioner-amd64:1.0.0"}],
;;     :qosClass "BestEffort"}}
;;   {:metadata
;;    {:labels {:k8s-app "kubernetes-dashboard", :pod-template-hash "5c848cc544"},
;;     :ownerReferences
;;     [{:apiVersion "apps/v1",
;;       :kind "ReplicaSet",
;;       :name "kubernetes-dashboard-5c848cc544",
;;       :uid "2acb09d7-eb7f-4de3-8a47-f0997147dacd",
;;       :controller true,
;;       :blockOwnerDeletion true}],
;;     :creationTimestamp "2019-12-10T13:10:27Z",
;;     :uid "508f0c16-e01b-4915-aabc-6021bb8cef90",
;;     :name "kubernetes-dashboard-5c848cc544-v28jx",
;;     :resourceVersion "21560",
;;     :selfLink
;;     "/api/v1/namespaces/kube-system/pods/kubernetes-dashboard-5c848cc544-v28jx",
;;     :namespace "kube-system",
;;     :generateName "kubernetes-dashboard-5c848cc544-"},
;;    :spec
;;    {:serviceAccount "kubernetes-dashboard",
;;     :securityContext {},
;;     :containers
;;     [{:terminationMessagePolicy "File",
;;       :args ["--auto-generate-certificates" "--namespace=kube-system"],
;;       :volumeMounts
;;       [{:name "kubernetes-dashboard-certs", :mountPath "/certs"}
;;        {:name "tmp-volume", :mountPath "/tmp"}
;;        {:name "kubernetes-dashboard-token-nrbwn",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :name "kubernetes-dashboard",
;;       :ports [{:containerPort 8443, :protocol "TCP"}],
;;       :livenessProbe
;;       {:httpGet {:path "/", :port 8443, :scheme "HTTPS"},
;;        :initialDelaySeconds 30,
;;        :timeoutSeconds 30,
;;        :periodSeconds 10,
;;        :successThreshold 1,
;;        :failureThreshold 3},
;;       :terminationMessagePath "/dev/termination-log",
;;       :imagePullPolicy "Always",
;;       :image "kubernetesui/dashboard:v2.0.0-beta5",
;;       :resources {}}],
;;     :volumes
;;     [{:name "kubernetes-dashboard-certs",
;;       :secret {:secretName "kubernetes-dashboard-certs", :defaultMode 420}}
;;      {:name "tmp-volume", :emptyDir {}}
;;      {:name "kubernetes-dashboard-token-nrbwn",
;;       :secret
;;       {:secretName "kubernetes-dashboard-token-nrbwn", :defaultMode 420}}],
;;     :schedulerName "default-scheduler",
;;     :priority 0,
;;     :nodeName "quicksilver",
;;     :restartPolicy "Always",
;;     :dnsPolicy "ClusterFirst",
;;     :tolerations
;;     [{:key "node-role.kubernetes.io/master", :effect "NoSchedule"}
;;      {:key "node.kubernetes.io/not-ready",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}
;;      {:key "node.kubernetes.io/unreachable",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}],
;;     :serviceAccountName "kubernetes-dashboard",
;;     :terminationGracePeriodSeconds 30,
;;     :enableServiceLinks true},
;;    :status
;;    {:phase "Running",
;;     :conditions
;;     [{:type "Initialized",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:29Z"}
;;      {:type "Ready",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "ContainersReady",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "PodScheduled",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:27Z"}],
;;     :hostIP "192.168.0.101",
;;     :podIP "10.1.84.15",
;;     :podIPs [{:ip "10.1.84.15"}],
;;     :startTime "2019-12-10T13:10:29Z",
;;     :containerStatuses
;;     [{:started true,
;;       :ready true,
;;       :name "kubernetes-dashboard",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:57Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:10:54Z",
;;         :finishedAt "2019-12-10T17:57:43Z",
;;         :containerID
;;         "containerd://8aa73c247fdeeffa2826b17e17093ccd49c581f70e5c2a440b4f7c8271b558df"}},
;;       :containerID
;;       "containerd://015379dfa5770ae90e5e9fea5afcabde1dd5a7ca15475d027090ed6c72797fc9",
;;       :imageID
;;       "docker.io/kubernetesui/dashboard@sha256:ae756074fa3d1b72c39aa98cfc6246c6923e7da3beaf350d80b91167be868871",
;;       :image "docker.io/kubernetesui/dashboard:v2.0.0-beta5"}],
;;     :qosClass "BestEffort"}}
;;   {:metadata
;;    {:labels
;;     {:k8s-app "influxGrafana", :pod-template-hash "6d599df6bf", :version "v4"},
;;     :ownerReferences
;;     [{:apiVersion "apps/v1",
;;       :kind "ReplicaSet",
;;       :name "monitoring-influxdb-grafana-v4-6d599df6bf",
;;       :uid "4afc26fb-0f4d-4875-bae4-9676fc8c7b6c",
;;       :controller true,
;;       :blockOwnerDeletion true}],
;;     :creationTimestamp "2019-12-10T13:10:27Z",
;;     :uid "db23c571-60fc-4ff0-a0b3-542de843817c",
;;     :name "monitoring-influxdb-grafana-v4-6d599df6bf-8w7fj",
;;     :resourceVersion "21555",
;;     :selfLink
;;     "/api/v1/namespaces/kube-system/pods/monitoring-influxdb-grafana-v4-6d599df6bf-8w7fj",
;;     :annotations #:scheduler.alpha.kubernetes.io{:critical-pod ""},
;;     :namespace "kube-system",
;;     :generateName "monitoring-influxdb-grafana-v4-6d599df6bf-"},
;;    :spec
;;    {:serviceAccount "default",
;;     :securityContext {},
;;     :priorityClassName "system-cluster-critical",
;;     :containers
;;     [{:name "influxdb",
;;       :image "k8s.gcr.io/heapster-influxdb-amd64:v1.3.3",
;;       :ports
;;       [{:name "http", :containerPort 8083, :protocol "TCP"}
;;        {:name "api", :containerPort 8086, :protocol "TCP"}],
;;       :resources
;;       {:limits {:cpu "100m", :memory "500Mi"},
;;        :requests {:cpu "100m", :memory "500Mi"}},
;;       :volumeMounts
;;       [{:name "influxdb-persistent-storage", :mountPath "/data"}
;;        {:name "default-token-fvtjp",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :terminationMessagePath "/dev/termination-log",
;;       :terminationMessagePolicy "File",
;;       :imagePullPolicy "IfNotPresent"}
;;      {:terminationMessagePolicy "File",
;;       :volumeMounts
;;       [{:name "grafana-persistent-storage", :mountPath "/var"}
;;        {:name "default-token-fvtjp",
;;         :readOnly true,
;;         :mountPath "/var/run/secrets/kubernetes.io/serviceaccount"}],
;;       :name "grafana",
;;       :env
;;       [{:name "INFLUXDB_SERVICE_URL", :value "http://monitoring-influxdb:8086"}
;;        {:name "GF_AUTH_BASIC_ENABLED", :value "false"}
;;        {:name "GF_AUTH_ANONYMOUS_ENABLED", :value "true"}
;;        {:name "GF_AUTH_ANONYMOUS_ORG_ROLE", :value "Admin"}
;;        {:name "GF_SERVER_ROOT_URL",
;;         :value
;;         "/api/v1/namespaces/kube-system/services/monitoring-grafana/proxy/"}],
;;       :ports [{:name "ui", :containerPort 3000, :protocol "TCP"}],
;;       :terminationMessagePath "/dev/termination-log",
;;       :imagePullPolicy "IfNotPresent",
;;       :image "k8s.gcr.io/heapster-grafana-amd64:v4.4.3",
;;       :resources
;;       {:limits {:cpu "100m", :memory "100Mi"},
;;        :requests {:cpu "100m", :memory "100Mi"}}}],
;;     :volumes
;;     [{:name "influxdb-persistent-storage", :emptyDir {}}
;;      {:name "grafana-persistent-storage", :emptyDir {}}
;;      {:name "default-token-fvtjp",
;;       :secret {:secretName "default-token-fvtjp", :defaultMode 420}}],
;;     :schedulerName "default-scheduler",
;;     :priority 2000000000,
;;     :nodeName "quicksilver",
;;     :restartPolicy "Always",
;;     :dnsPolicy "ClusterFirst",
;;     :tolerations
;;     [{:key "node-role.kubernetes.io/master", :effect "NoSchedule"}
;;      {:key "CriticalAddonsOnly", :operator "Exists"}
;;      {:key "node.kubernetes.io/not-ready",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}
;;      {:key "node.kubernetes.io/unreachable",
;;       :operator "Exists",
;;       :effect "NoExecute",
;;       :tolerationSeconds 300}],
;;     :serviceAccountName "default",
;;     :terminationGracePeriodSeconds 30,
;;     :enableServiceLinks true},
;;    :status
;;    {:phase "Running",
;;     :conditions
;;     [{:type "Initialized",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:29Z"}
;;      {:type "Ready",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "ContainersReady",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T17:58:59Z"}
;;      {:type "PodScheduled",
;;       :status "True",
;;       :lastProbeTime nil,
;;       :lastTransitionTime "2019-12-10T13:10:27Z"}],
;;     :hostIP "192.168.0.101",
;;     :podIP "10.1.84.13",
;;     :podIPs [{:ip "10.1.84.13"}],
;;     :startTime "2019-12-10T13:10:29Z",
;;     :containerStatuses
;;     [{:started true,
;;       :ready true,
;;       :name "grafana",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:56Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:11:15Z",
;;         :finishedAt "2019-12-10T17:57:43Z",
;;         :containerID
;;         "containerd://3d374420ebcecd793aa0cda3b41ec4f25781eb968976b6215fed623d72393284"}},
;;       :containerID
;;       "containerd://8490fccf5983f6c2cbae8862e5202e4586f3799dade3a420e5de3048dd52cd38",
;;       :imageID
;;       "k8s.gcr.io/heapster-grafana-amd64@sha256:4a472eb4df03f4f557d80e7c6b903d9c8fe31493108b99fbd6da6540b5448d70",
;;       :image "k8s.gcr.io/heapster-grafana-amd64:v4.4.3"}
;;      {:started true,
;;       :ready true,
;;       :name "influxdb",
;;       :restartCount 1,
;;       :state {:running {:startedAt "2019-12-10T17:58:54Z"}},
;;       :lastState
;;       {:terminated
;;        {:exitCode 255,
;;         :reason "Unknown",
;;         :startedAt "2019-12-10T13:10:39Z",
;;         :finishedAt "2019-12-10T17:57:43Z",
;;         :containerID
;;         "containerd://8fd0454a5cd7f5b33702ae8eec80c25ec226b7130a5aaf6673431a733e7490c8"}},
;;       :containerID
;;       "containerd://96403258b8d0bcb1877c86826e44fb9756be9446b04179bdf1882979b1b5e9a5",
;;       :imageID
;;       "k8s.gcr.io/heapster-influxdb-amd64@sha256:f433e331c1865ad87bc5387589965528b78cd6b1b2f61697e589584d690c1edd",
;;       :image "k8s.gcr.io/heapster-influxdb-amd64:v1.3.3"}],
;;     :qosClass "Guaranteed"}}]}


;; Use a label selector when listing pods
(<!! (k8s/list-pod ctx {:label-selector "kube-system=true"}))
;; {:kind "PodList",
;;  :apiVersion "v1",
;;  :metadata {:selfLink "/api/v1/pods", :resourceVersion "29544"},
;;  :items []}

