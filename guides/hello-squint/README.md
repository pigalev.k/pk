# hello-squint

Exploring Squint and Cherry --- Clojurescript to JS (ES) compilers.

- Light-weight ClojureScript dialect https://github.com/squint-cljs/squint
- Experimental ClojureScript to ES6 module compiler
  https://github.com/squint-cljs/cherry

## Getting started

```
npx squint run src/hello.cljs
```

```
npx cherry run src/hello.cljs
```
