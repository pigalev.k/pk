import * as cherry_core from 'cherry-cljs/cljs.core.js';
import * as fs from 'fs';
import { fileURLToPath } from 'url';
cherry_core.println.call(null, fs.existsSync.call(null, fileURLToPath.call(null, import.meta.url)));
var foo = (function (p__1) {
const map__21 = p__1;
const map__22 = cherry_core.__destructure_map.call(null, map__21);
const a3 = cherry_core.get.call(null, map__22, cherry_core.keyword("a"));
const b4 = cherry_core.get.call(null, map__22, cherry_core.keyword("b"));
const c5 = cherry_core.get.call(null, map__22, cherry_core.keyword("c"));
return (a3) + (b4) + (c5);
});
cherry_core.println.call(null, foo.call(null, cherry_core.array_map(cherry_core.keyword("a"), 1, cherry_core.keyword("b"), 2, cherry_core.keyword("c"), 3)));

export { foo }
