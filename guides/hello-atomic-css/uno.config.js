import { defineConfig, presetWind, presetIcons } from 'unocss';

export default defineConfig({
  cli: {
    entry: {
      patterns: process.env.NODE_ENV == 'production' ? ["./public/uno/js/main.js"]
        : ["./src/**/uno/**/*.cljs", "./public/uno/js/cljs-runtime/*.js"],
      outFile: "./public/uno/css/main.css"
    }
  },
  presets: [
    presetWind(),
    presetIcons(
      {
        scale: 1.0,
        extraProperties: {
          'display': 'inline-block',
          'vertical-align': 'middle',
        },
      }
    ),
  ],
});
