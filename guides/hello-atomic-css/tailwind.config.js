const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors');

module.exports = {
  // for the production build, look for Tailwind classes in shadow-cljs output
  // file; for the development build, look in cljs-runtime dir, that contains
  // files that are actually compiled; postcss watch should be a whole lot
  // faster
  content: process.env.NODE_ENV == 'production' ? ["./public/tailwind/js/main.js"]
    : ["./src/**/tailwind/**/*.cljs", "./public/tailwind/js/cljs-runtime/*.js"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Fira Sans", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        primary: {
          DEFAULT: '#678fca',
          '50': '#f3f6fb',
          '100': '#e3ebf6',
          '200': '#cedeef',
          '300': '#acc7e4',
          '400': '#85abd5',
          '500': '#678fca',
          '600': '#5e7fc0',
          '700': '#4965ac',
          '800': '#40548d',
          '900': '#384770',
          '950': '#252e46',
        },
        secondary: {
          DEFAULT: '#7ab131',
          '50': '#f5fbea',
          '100': '#e8f5d2',
          '200': '#d3ebab',
          '300': '#b5dd79',
          '400': '#96ca4b',
          '500': '#7ab131',
          '600': '#5e8c24',
          '700': '#496c1f',
          '800': '#3b561e',
          '900': '#344a1d',
          '950': '#19280b',
        },
        error: colors.red,
        warning: colors.yellow,
        success: colors.green,
        info: colors.blue,
      }
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
