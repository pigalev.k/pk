# hello-atomic-css

Exploring usage of Atomic CSS frameworks with ClojureScript.

- Tailwind CSS
  - An utility-first CSS framework https://tailwindcss.com/
  - Unstyled UI components https://headlessui.com/
  - ClojureScript interface to React https://github.com/reagent-project/reagent
  - Example project: shadow-cljs + reagent + tailwindcss + headlessui
    https://github.com/jacekschae/shadow-cljs-tailwindcss

- UnoCSS
  - Instant On-demand Atomic CSS Engine https://unocss.dev/

## Getting started

Start a cljs REPL in terminal

```
clj -M -m cjls.main
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.

## Prepare

1. Install `node`
2. Install npm dependencies

```
yarn
```

## Configure

- `shadow-cljs` builds the application JS code from cljs sources; it is
  configured in `shadow-cljs.edn` file; its server UI will be available at
  http://localhost:9630; development HTTP servers will be available at
  http://localhost:8020 (Tailwind), http://localhost:8021 (Uno)

Tailwind:

- `tailwindcss` searches for Tailwind CSS classes used in application sources
  and puts their definitions in the resulting CSS file that will be used by
  the Tailwind application; it is configured in `tailwind.config.js` file
- `postcss` creates a style building pipeline (running tailwindcss and
  possibly other steps like minification), it is configured in
  `postcss.config.js` file

Uno:

- `unocss` searches for Uno CSS classes used in application sources and puts
  their definitions in the resulting CSS file that will be used by the Uno
  application; it is configured in `uno.config.js` file

## Build and Run

Start `shadow-cljs` application build together with `postcss` styles build,
both watching for changes in respective source files

```
yarn dev
```

Build application and styles once, for release build

```
yarn release
```

Serve the release build (default port is 4444)

```
clj -X:serve
```

See `scripts` section in `package.json` and `:aliases` section in `deps.edn`
for all available commands.
