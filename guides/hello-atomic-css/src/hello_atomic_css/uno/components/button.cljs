(ns hello-atomic-css.uno.components.button)

(defn button
  "A button component."
  [{:keys [on-click]} text]
  [:button
   {:class [:flex :items-center
            :bg-amber-500 :text-white :px-4 :py-2 :rounded-md :border-0 :shadow
            :uppercase :font-bold
            :cursor-pointer
            :hover:bg-amber-600 :active:bg-amber-700 :transition]
    :on-click (or on-click #(println (str "button pressed: " text)))}
   [:div {:class [:i-mdi:alert-circle-outline :mr-2]}]
   [:span {:class []}text]])
