(ns hello-atomic-css.uno.app
  (:require
   [hello-atomic-css.uno.views.home :refer [home]]
   [reagent.dom :as dom]))


(defn app
  "The main application's view."
  []
  [home])


(defn ^:dev/after-load start
  "Mounts the application into the DOM. Should be invoked at the
  application start and after code reloading finishes."
  []
  (dom/render [app]
    (.getElementById js/document "app")))

(defn init
  "The application entry point."
  []
  ;; init is called once when the page loads; this is called in the index.html
  ;; and must be exported so it is available even in `:advanced` release
  ;; builds
  (js/console.log "ClojureScript + UnoCSS application initialized")
  (start))
