(ns hello-atomic-css.uno.views.home
  (:require [hello-atomic-css.uno.components.button :refer [button]]))

(defn home
  "Application's home page."
  []
  [:<>
   [:h1 "Hello UnoCSS!"]
   [button {} "Click me"]])
