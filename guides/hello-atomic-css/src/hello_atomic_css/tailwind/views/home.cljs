(ns hello-atomic-css.tailwind.views.home
  (:require
   [hello-atomic-css.tailwind.components.button :refer [button]]
   [hello-atomic-css.tailwind.components.content :refer [content]]
   [hello-atomic-css.tailwind.components.footer :refer [footer]]
   [hello-atomic-css.tailwind.components.header :refer [header]]
   [hello-atomic-css.tailwind.components.headless-ui :refer [disclosure]]
   [hello-atomic-css.tailwind.components.link :refer [link]]
   [hello-atomic-css.tailwind.components.page :refer [page]]))


(defn home
  "The application's home page."
  []
  [page
   [:<>
    [header {} [:div.space-x-4
                [link {:text-color :text-success-500} "https://google.com" "Google"]
                [link {:text-color :text-warning-500} "https://ya.ru" "Yandex"]
                [link {:text-color :text-error-500} "https://bing.com" "Bing"]
                [link {:text-color :text-info-500} "https://duckduckgo.com/"
                 "DuckDuckGo"]]]
    [content [:<>
              ;; font
              [:p.p-4.rounded-md.ring-2
               [:span.font-mono.text-xs.font-thin.tracking-widest
                {:class ["text-black/50"]}
                "Some"]
               [:span.font-sans.text-sm.italic.tracking-normal " example "]
               [:span.font-serif.text-lg.antialiased.font-bold.tracking-tighter
                {:class ["text-black/25"]}
                "content."]]
              [:p.w-32.p-4.rounded-md.ring-2.hover:aspect-square "Square on hover"]
              ;; lists
              [:ul.list-inside.list-disc.text-left
               [:li "5 cups chopped Porcini mushrooms"]
               [:li "1/2 cup of olive oil"]
               [:li "3lb of celery"]]
              ;; columns
              [:div.columns-3.gap-16
               [:p.w-full.bg-slate-200.border.rounded "column one"]
               [:p.w-full.bg-slate-200.border.rounded "column two"]
               [:p.w-full.bg-slate-200.border.rounded "column three"]]
              ;; tables
              [:div.table.w-full
               [:div.table-header-group
                [:div.table-row
                 [:div.table-cell.text-left "Song"]
                 [:div.table-cell.text-left "Artist"]
                 [:div.table-cell.text-left "Year"]]]
               [:div.table-row-group
                [:div.table-row
                 [:div.table-cell.text-left "The sliding Mr. Bones"]
                 [:div.table-cell.text-left "Malcolm Lockyer"]
                 [:div.table-cell.text-left "1961"]]
                [:div.table-row
                 [:div.table-cell.text-left "Witchy Woman"]
                 [:div.table-cell.text-left "The Eagles"]
                 [:div.table-cell.text-left "1972"]]]]
              ;; positioning
              [:div.relative.h-32.w-32.bg-gradient-to-br.from-primary-700.to-secondary-300.border.rounded.hover:rotate-6.transition.duration-300.origin-top-right
               [:div.absolute.h-14.w-14.-right-4.-top-4.bg-red-700.border.rounded-full.blur-sm.hover:scale-75.transition.duration-300.transform-gpu]]
              ;; flex
              [:div.flex.flex-row-reverse.gap-x-2
               (for [i (range 2)]
                 ^{:key i} [:div {:class ["basis-1/4" :border-b-4]} (str i)])
               [:div {:class ["basis-1/2" :border-4 :rounded]} "2"]]
              ;; grids
              [:div.grid.grid-cols-4.gap-4
               (for [i (range 10)]
                 ^{:key i} [:div.bg-slate-300.border.rounded i])
               [:div.col-span-2.bg-slate-300.border.rounded "10-11"]
               [:div.col-span-3.bg-slate-300.border.rounded "12-13-14"]
               [:div.col-start-2.col-end-4.bg-slate-300.border.rounded "15-16"]
               [:div.col-start-2.col-span-3.bg-slate-300.border.rounded
                [:div "17-18-19"]]]
              [:div.grid.grid-rows-4.grid-flow-col.gap-4
               (for [i (range 10)]
                 ^{:key i} [:div.bg-slate-200.border.rounded i])
               [:div.row-span-2.bg-slate-200.border.rounded "10-11"]
               [:div.row-span-3.bg-slate-200.border.rounded "12-13-14"]
               [:div.row-start-2.row-end-4.bg-slate-200.border.rounded "15-16"]
               [:div.row-start-2.row-span-3.bg-slate-200.border.rounded.flex.items-center.justify-center
                [:div "17-18-19"]]]
              [:div.grid.grid-cols-4.grid-flow-row.gap-4
               {:class ["auto-rows-[50px]"]}
               (for [i (range 10)]
                 ^{:key i} [:div.bg-slate-300.border.rounded i])]

              [button {} "Click me"]
              [disclosure "Explain" "Some explanation."]]]
    [footer {} "A footer."]]])
