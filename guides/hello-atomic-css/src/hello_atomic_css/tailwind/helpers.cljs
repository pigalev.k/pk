(ns hello-atomic-css.tailwind.helpers)

(defn merge-vectors
  "Merges vectors of keywords in `kws` into one vector."
  [& kws]
  (vec (apply concat kws)))
