(ns hello-atomic-css.tailwind.components.content
  (:require
   [hello-atomic-css.tailwind.components.card :refer [card]]))


(defn content
  "A content area component."
  [content]
  [card {:space-y :space-y-4} content])
