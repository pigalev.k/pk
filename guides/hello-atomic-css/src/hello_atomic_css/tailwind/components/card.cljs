(ns hello-atomic-css.tailwind.components.card)

(defn card
  "A card component."
  [{:keys [bg-color text-color space-x space-y]
    :or   {bg-color   :bg-slate-50
           text-color :text-black}} content]
  [:div {:class [:container
                 ;; full width, rounded corners, shadow
                 :w-full :rounded-sm :shadow
                 ;; light grey background, black text
                 bg-color text-color
                 ;; spacing between children
                 space-x space-y
                 ;; padding, centered text
                 :p-4 :text-center]}
   content])
