(ns hello-atomic-css.tailwind.components.link)

(defn link
  "A link component."
  [{:keys [text-color target]
    :or   {text-color :text-blue-500
           target     "_blank"}} url text]
  [:a {:class  [text-color :hover:underline :underline-offset-4
                :hover:decoration-dotted :hover:decoration-4]
       :href   url
       :target target}
   text])
