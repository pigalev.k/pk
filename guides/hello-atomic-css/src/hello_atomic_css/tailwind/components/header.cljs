(ns hello-atomic-css.tailwind.components.header
  (:require
   [hello-atomic-css.tailwind.components.card :refer [card]]))

(defn header
  "A header component."
  [_ content]
  [card {} content])
