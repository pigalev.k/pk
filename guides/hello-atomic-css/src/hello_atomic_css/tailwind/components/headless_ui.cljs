(ns hello-atomic-css.tailwind.components.headless-ui
  (:require [headlessui-reagent.core :as ui]))

(defn disclosure
  "An example disclosure component."
  [title content]
  [ui/disclosure
   [ui/disclosure-button {:class [:w-full :px-4 :py-2 :text-sm :font-medium
                                  :text-purple-900 :bg-purple-100 :rounded-lg]}
    title]
   [ui/disclosure-panel {:class [:px-4 :pt-4 :pb-2 :text-sm :text-gray-500]}
    [:p.p-4.bg-red-50.border-4.rounded-full.text-lg content]]])
