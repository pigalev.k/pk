(ns hello-atomic-css.tailwind.components.button
  (:require
   [hello-atomic-css.tailwind.helpers :refer [merge-vectors]]))

(defn button
  "A button component."
  [{:keys [class]} text on-click]
  [:button
   {:class (merge-vectors [:bg-primary :text-white :px-4 :py-2 :rounded-md :shadow-md
                           :uppercase :font-bold
                           :hover:bg-primary-600 :transition :duration-200
                           :active:bg-primary-700 :active:shadow-0]
                          class)
    :on-click (or on-click #(println (str "button pressed: " text)))}
   text])
