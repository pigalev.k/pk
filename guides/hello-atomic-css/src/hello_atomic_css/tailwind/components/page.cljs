(ns hello-atomic-css.tailwind.components.page)

(defn page
  "A page component."
  [content]
  [:div {:class "container lg mx-auto p-4 space-y-4"}
   content])
