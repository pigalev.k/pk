(ns hello-atomic-css.tailwind.components.footer
  (:require
   [hello-atomic-css.tailwind.components.card :refer [card]]))


(defn footer
  "A footer component."
  []
  [card {} "A footer."])
