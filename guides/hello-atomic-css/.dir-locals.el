;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; configure default Cider REPL
((nil . (
         ;; (cider-preferred-build-tool . shadow-cljs)
         ;; (cider-shadow-default-options . "tailwind-app")
         (cider-default-cljs-repl . shadow)
         (cider-clojure-cli-aliases . ":dev:cljs")
         ;; the new way to pass options to Clojure CLI
         (cider-clojure-cli-parameters . ""))))
