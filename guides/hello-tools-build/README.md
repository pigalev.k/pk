# hello-tools-build

Exploring tools.build --- a library of functions for building artifacts in
Clojure projects.

- https://clojure.org/guides/tools_build

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.

## Invoking a `build` tool

Tools are described as aliases in the `deps.edn`, functions invoked should
reside in the namespace specified in the corresponding alias.

```
clojure -T:build <function>
```
