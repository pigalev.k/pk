(ns build
  (:require
   [clojure.tools.build.api :as b]))


(def lib 'hello/hello-tools-build)
(def version (format "1.2.%s" (b/git-count-revs nil)))
(def class-dir "target/classes")
(def basis (b/create-basis {:project "deps.edn"}))
(def jar-file (format "target/%s-%s.jar" (name lib) version))
(def uber-file (format "target/%s-%s-standalone.jar" (name lib) version))


(defn info
  "See info."
  [{:keys [env] :as params}]
  (println (format "Params map: %s" params))
  (when env
    (println (format "Environment: %s" env)))
  params)

(defn clean
  "Remove the build directory."
  [_]
  (b/delete {:path "target"})
  _)

(defn jar
  "Build a jar file."
  [_]
  (b/write-pom {:class-dir class-dir
                :lib lib
                :version version
                :basis basis
                :src-dirs ["src"]})
  (b/copy-dir {:src-dirs ["src/main" "resources"]
               :target-dir class-dir})
  (b/jar {:class-dir class-dir
          :jar-file jar-file})
  _)

(defn uberjar
  "Build an uberjar."
  [_]
  (clean nil)
  (b/copy-dir {:src-dirs ["src/main" "resources"]
               :target-dir class-dir})
  (b/compile-clj {:basis basis
                  :src-dirs ["src/main"]
                  :class-dir class-dir})
  (b/uber {:class-dir class-dir
           :uber-file uber-file
           :basis basis})
  _)


(comment

  (info {:env :dev :foo :bar})

)
