(ns hello-tools-build.core)

(defn dot
  "A dot product of two vectors."
  [xs ys]
  (reduce + (map * xs ys)))
