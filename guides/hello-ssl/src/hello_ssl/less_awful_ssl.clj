(ns hello-ssl.less-awful-ssl
  (:require
   [less.awful.ssl :as ssl]
   [org.httpkit.client :as http])
  (:import
   (java.net InetAddress Socket ServerSocket SocketException)))


;; basics

;; https://github.com/aphyr/less-awful-ssl

(comment

  ;; purpose and goals

  ;; working with Java's crypto libraries requires deep knowledge of a complex
  ;; API, language-specific key+certificate storage, and knowing how to avoid
  ;; long-standing bugs in the Java trust algorithms. This library tries to make
  ;; it less complicated to build simple SSL-enabled applications: given a CA
  ;; certificate and a signed key and cert, it can give you an SSLContext
  ;; suitable for creating TCPSockets directly, or handing off to Netty.


  ;; example

  ;; in this example we'll be using OpenSSL's stock CA configuration and the
  ;; OpenSSL perl script to create a CA's directory structure. I'm assuming you
  ;; want your CA signing key encrypted, but the client and server keys
  ;; unencrypted (since they'll be deployed to processes which run without human
  ;; interaction).

  ;; create the CA directory hierarchy and keypair
  ;; http://kremvax.net/howto/ssl-openssl-ca.html
  ;; cp /usr/lib/ssl/misc/CA.pl ca
  ;; ./ca -newca

  ;; generate a server key
  ;; openssl genrsa -out server.key 4096

  ;; convert key to pcks8 because Java can't read OpenSSL's format
  ;; openssl pkcs8 -topk8 -nocrypt -in server.key -out server.pkcs8

  ;; generate a cert request
  ;; openssl req -new -key server.key -out newreq.pem

  ;; sign request with CA
  ;; ./ca -sign

  ;; rename signed cert and clean up unused files
  ;; mv newcert.pem server.crt
  ;; rm newreq.pem server.key

  ;; and generate a client key+cert as well
  ;; openssl genrsa -out client.key 4096
  ;; openssl pkcs8 -topk8 -nocrypt -in client.key -out client.pkcs8
  ;; openssl req -new -key client.key -out newreq.pem
  ;; ./ca -sign
  ;; mv newcert.pem client.crt
  ;; rm newreq.pem client.key

  ;; now fire up a repl and test that your client and server keys can work
  ;; together. `ssl/test-ssl` takes a client key and cert, a server key and
  ;; cert, and a trusted CA certificate, and verifies that a client can talk to
  ;; the server over a TLS socket:

  (def d (partial str "/path/to/keys/"))
  (apply ssl/test-ssl
         (map d ["client.pkcs8"
                 "client.crt"
                 "server.pkcs8"
                 "server.crt"
                 "demoCA/cacert.pem"]))


  ;; note that this doesn't work if you substitute some other certificate for
  ;; the trust chain, rather than the CA's:

  (apply ssl/test-ssl
         (map d ["client.pkcs8"
                 "client.crt"
                 "server.pkcs8"
                 "server.crt"
                 "server.crt"]))

  ;; in your app, you'll want to distribute a particular PKCS secret key, the
  ;; corresponding signed certificate, and the CA certificate (to verify the
  ;; peer's identity). Then you can build an `SSLContext`:

  (ssl/ssl-context "client.pkcs8" "client.crt" "ca.crt")

  ;; and given an SSL context, you can use it to construct a server or a client
  ;; TCP socket. See `ssl/test-ssl` for an example:

  (with-open [listener (-> (ssl/ssl-context "server.pkcs8" "server.crt" "ca.crt")
                           (ServerSocket. 1234))
              conn     (.accept listener)] '...)


  ;; example with a PKCS12 client certificate and httpkit

  ;; assume you've got a client key/certificate pair for example.com as a PKCS12
  ;; file client.p12, secured with password. Also, you've got the Certificate
  ;; Authority that was used to sign the client certificate as ca-cert.crt.

  ;; then you could do (your project needs http-kit):


  (def password (char-array "secret"))

  (def req (http/request {:sslengine (ssl/ssl-context->engine
                                      (ssl/ssl-p12-context "client.p12"
                                                           password
                                                           "ca-cert.crt"))
                          :url       "https://example.com/needs-client-cert"
                          :as        :stream}))

)

;; TODO: follow through
