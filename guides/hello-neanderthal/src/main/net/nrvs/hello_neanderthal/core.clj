(ns net.nrvs.hello-neanderthal.core
  (:require [uncomplicate.neanderthal.core :as nc]
            [uncomplicate.neanderthal.native :as nn]))


;; Getting started
;; https://neanderthal.uncomplicate.org/articles/getting_started.html

;; create two double-precision vectors and compute their dot product:

(def x (nn/dv 1 2 3))
(def y (nn/dv 10 20 30))
(nc/dot x y)

;; multiply matrices

(def a (nn/dge 3 2 [1 2 3 4 5 6]))
(def b (nn/dge 2 3 [10 20 30 40 50 60]))
(nc/mm a b)
