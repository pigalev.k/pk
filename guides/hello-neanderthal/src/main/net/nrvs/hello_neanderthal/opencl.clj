(ns net.nrvs.hello-neanderthal.opencl
  (:require [midje.sweet :refer [facts => truthy]]
            [criterium.core :refer [quick-bench with-progress-reporting]]
            [uncomplicate.commons.core :refer [with-release]]
            [uncomplicate.clojurecl.core
             :refer [with-default-1 finish!]]
            [uncomplicate.neanderthal
             [core :refer [asum dot axpy! mv! mm! transfer! copy]]
             [native :refer [fv fge]]
             [opencl :refer [with-default-engine clv clge]]]))

;; on GPU (OpenCL)
;; https://neanderthal.uncomplicate.org/articles/tutorial_opencl.html

;; with OpenCL 1.2 (Nvidia, pre-Vega AMD GPUs) use `command-queue-1` and
;; `with-default-1` instead of `command-queue` and `with-default`.

;; also https://stackoverflow.com/questions/61274605/opencl-codegen-phase-failed-compilation

(with-default-1
  (with-default-engine
    (facts "We'll write our GPU code here, but for now here is only the plain
    CPU stuff you recognize from the plain Neanderthal tutorial."
           (asum (fv 1 -2 3)) => 6.0)))


;; Do the computations

(with-default-1
  (with-default-engine
    (with-release [gpu-x (transfer! (fv 1 -2 3) (clv 3))]
      (facts
       "Create a vector on the device, write into it the data from the host
vector and compute the sum of absolute values."
       (asum gpu-x))) => 6.0))

;; Measure the performance

(with-default-1
  (with-default-engine
    (with-release [host-x (fv 1 -2 3)
                   gpu-x (clv 1 -2 3)]
      (facts
       "Compare the speed of computing small vectors on CPU and GPU"
       (asum host-x) => 6.0
       (println "CPU:")
       (with-progress-reporting (quick-bench (asum host-x)))
       (asum gpu-x) => 6.0
       (println "GPU:")
       (with-progress-reporting (quick-bench (do (asum gpu-x) (finish!))))))))
