# hello-clerk

Exploring Clerk --- moldable live programming for Clojure.

- https://github.com/nextjournal/clerk

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
