(ns hello-clerk.core
  (:require
   [nextjournal.clerk :as clerk]))


;; basics

(comment

  ;; start Clerk's built-in webserver on the default port 7777, opening the
  ;; browser when done
  (clerk/serve! {:browse? true})

  ;; either call `clerk/show!` explicitly
  (clerk/show! "src/hello_clerk/core.clj")

  ;; or let Clerk watch the given `:paths` for changes
  (clerk/serve! {:watch-paths ["src"]})

  ;; stop Clerk's webserver and file watcher
  (clerk/halt!)

)
