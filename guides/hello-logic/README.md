# hello-logic

Exploring logic programming facilities of Clojure(Script).

Sources:
- "Reasoned Schemer" by Daniel P. Friedman, William E. Byrd, Oleg Kiselyov, and
  Jason Hemann, second edition, MIT Press, 2018.

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
