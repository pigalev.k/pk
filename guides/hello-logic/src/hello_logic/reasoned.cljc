(ns hello-logic.reasoned
  (:require
   [clojure.core.logic :refer [run run* == s# u# fresh conde
                               firsto resto conso emptyo membero appendo
                               llist lcons lcons?]]
   [hello-logic.relations :refer [pair? pairo listo lol? lolo loso
                                  singletono proper-membero]]))


;; a question-answer guide to logic programming, originally written in Scheme,
;; adapted to Clojure

;; each question-answer pair is called a *frame* and have an ordinal
;; number (unique within the chapter).


;; Source: The Reasoned Schemer, Second Edition by Daniel P. Friedman, William
;; E. Byrd, Oleg Kiselyov, and Jason Hemann

;; also see
;; https://github.com/clojure/core.logic/wiki/Differences-from-The-Reasoned-Schemer
;; https://github.com/michelemendel/the_reasoned_schemer


;; concepts

;; functional programming: a program is a mathematical *function*, like
;; `square`, that multiplies it's argument by itself.


;; relational programming: a program is a *relation*, which is a generalization
;; of mathematical function, like `squareo` that generalizes `square` by
;; relating pairs of integers: `squareo(4, 16)` relates 4 with 16, and so
;; forth.

;; a relation supplied with arguments is a *goal*. A goal can succeed, fail or
;; have no value.


;; examples:

;; the goal squareo(3, x) succeeds by associating 9 with the variable `x`.

;; the goal squareo (y, 9) succeeds twice, by separately associating -3 and then
;; 3 with `y`.

;; if we have written our `squareo` relation properly, the goal squareo(z, 5)
;; fails, and we conclude that there is no integer whose square is 5; otherwise,
;; the goal has no value, and we cannot draw any conclusions about `z`.

;; using two variables lets us create a goal squareo(w, v) that succeeds an
;; unbounded number of times, enumerating all pairs of integers such that the
;; second integer is the square of the first. Used together, the goals
;; squareo(x, y) and squareo(-3, x) succeed --- regardless of the ordering of
;; the goals --- associating 9 with `x` and 81 with `y`.


;; 1. Playthings

(comment

  ;; 1. welcome back.

  ;; it is good to be here, again.


  ;; 2. have you finished The Little Schemer?

  ;; false


  ;; 3. that's okay. Do you know about "Cons the Magnificent?"

  ;; true


  ;; 4. do you know what recursion is?

  ;; Absolutely.


  ;; 5. what is a goal?

  ;; it is something that either succeeds, fails, or has no value.


  ;; 6. `s#` is a goal that succeeds. What is `u#`?

  ;; is it a goal that fails?


  ;; 7. exactly. What is the value of

  (run* [q] u#)

  ;; (), since `u#` fails, and because if g is a goal that fails, then the
  ;; expression (run* [q] g) produces the empty list.


  ;; 8. what is

  (== 'pea 'pod)

  ;; is it also a goal?


  ;; 9. yes. Does the goal

  (== 'pea 'pod)

  ;; succeed of fail?

  ;; it fails, because pea is not the same as pod.


  ;; 10. correct. What is the value of

  (run* [q]
    (== 'pea 'pod))

  ;; (), since the goal (== 'pea 'pod) fails.


  ;; 11. what is the value of

  (run* [q]
    (== q 'pea))

  ;; (pea). The goal (== q 'pea) succeeds, associating pea with the fresh
  ;; variable q.


  ;; 12. is the value of

  (run* [q]
    (== 'pea q))

  ;; the same as the value of

  (run* [q]
    (== q 'pea))

  ;; yes, they both have the value (pea), because the order of arguments to ==
  ;; does not matter.


  ;;;;;;;;;;;;;;;;;;;;;;;
  ;; The First Law of ==
  ;;;;;;;;;;;;;;;;;;;;;;;

  ;; (== v w) can be replaced with (== w v).


  ;; 13. we use the phrase 'what value is associated with' to mean the same
  ;; thing as the phrase 'what is the value of', but with the outer parentheses
  ;; removed from the resulting value. This lets us avoid one pair of matching
  ;; parentheses when describing the value of a `run*` expression.

  ;; that's important to remember!


  ;; 14. what value is associated with q in

  (run* [q]
    (== 'pea q))

  ;; pea. The value of the `run*` expression is (pea), and so the value
  ;; associated with q is pea.


  ;; 15. does the variable q remain fresh in

  (run* [q]
    (== 'pea q))

  ;; no. In this expression q does not remain fresh because the value pea is
  ;; associated with q. We must mind our peas and qs.


  ;; 16. does the variable q remain fresh in

  (run* [q]
    s#)

  ;; yes.


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; Every variable is initially fresh. A variable is no longer fresh if it
  ;; becomes associated with a non-variable value or if it becomes associated
  ;; with a variable that, itself, is no longer fresh.
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


  ;; 17. what is the value of

  (run* [q]
    s#)

  ;; (_0). In the value of a `run*` expression, each fresh variable is *reified*
  ;; by appearing as the underscore symbol followed by a numeric subscript.


  ;; 18. in the value (_0), what variable is reified as _0?

  ;; the fresh variable q.


  ;; 19. what is the value of

  (run* [q]
    (== 'pea 'pea))

  ;; (_0). Although the `run*` expression produces a nonempty list, q remains
  ;; fresh.


  ;; 20. what is the value of

  (run* [q]
    (== q q))

  ;; (_0). Although the `run*` expression producesa a nonempty list, the
  ;; successful goal (== q q) does not associate any value with the variable q.


  ;; 21. we can introduce a new fresh variable with `fresh`. What value is
  ;; associated with q in

  (run* [q]
    (fresh [x]
      (== 'pea q)))

  ;; pea. Introducing an unused variable does not change the value associated
  ;; with any other variable.


  ;; 22. is x the only variable that begins fresh in

  (run* [q]
    (fresh [x]
      (== 'pea q)))

  ;; no, since q also starts out fresh. All variables introduced by `fresh` or
  ;; `run*` begin fresh.


  ;; 23. is x the only variable that remains fresh in

  (run* [q]
    (fresh [x]
      (== 'pea q)))

  ;; yes, since pea is associated with q.


  ;; 24. suppose that we instead use x in the `==` expression. What value is
  ;; associated with q in

  (run* [q]
    (fresh [x]
      (== 'pea x)))

  ;; _0, since q remains fresh.


  ;; 25. suppose that we use both x and q. What value is associated with q in

  (run* [q]
    (fresh [x]
      (== (cons x '()) q)))

  ;; (_0). The value of (cons x '()) is associated with q, although x remains
  ;; fresh.


  ;; 26. what value is associated with q in

  (run* [q]
    (fresh [x]
      (== `(~x) q)))

  ;; (_0), since `(~x) is a shorthand for (cons x '()).


  ;; 27. is this a bit subtle?

  ;; indeed.


  ;; 28. tildas (~), as in the frame 26, can only precede variables. Thus, what
  ;; is not a variable behaves as if it were quoted.

  ;; in that case, reading off the values of backtick (`) expressions should not
  ;; be too difficult.


  ;; 29. two different fresh variables can be made the same by *fusing* them.

  ;; how can we fuse two different fresh variables?


  ;; 30. we fuse two different fresh variables using `==`. In the expression

  (run* [q]
    (fresh [x]
      (== x q)))

  ;; x and q are different fresh variables, so they are fused when the goal (==
  ;; x q) succeeds.

  ;; okay.


  ;; 31. what value is associated with q in

  (run* [q]
    (fresh [x]
      (== x q))),

  ;; _0. x and q are fused, but remain fresh. Fused variables get the same
  ;; association if a value (including another variable) is associated later
  ;; with either variable.


  ;; 32. what value is associated with q in

  (run* [q]
    (== '(((pea)) pod) '(((pea)) pod)))

  ;; _0.


  ;; 33. what value is associated with q in

  (run* [q]
    (== `(((pea)) pod) `(((pea)) ~q)))

  ;; `hello-logic.reasoned/pod` (instead of just pod), as Clojure automatically
  ;; namespace-qualifies symbols in backtick-quoted forms. Also because of this
  ;; example was modified to use backticks in both arguments to `==` for the
  ;; code to work as expected. `list` can be used to reproduce the original
  ;; example exactly, like this

  (run* [q]
    (== (list (list (list 'pea)) 'pod) (list (list (list 'pea)) q)))

  ;; (pod)


  ;; 34. what value is associated with q in

  (run* [q]
    (== `(((~q)) pod) `(((pea)) pod)))

  ;; `hello-logic.reasoned/pea` (instead of just pea), as in the previous frame.


  ;; 35. what value is associated with q in

  (run* [q]
    (fresh [x]
      (== `(((~q)) pod) `(((~x)) pod))))

  ;; _0, since q remains fresh, even though x is fused with q.


  ;; 36. what value is associated with q in

  (run* [q]
    (fresh [x]
      (== `(((~q)) ~x) `(((~x)) pod))))

  ;; `hello-logic.reasoned/pod` (instead of just pod), as in frames 33-34,
  ;; because `hello-logic.reasoned/pod` is associated with x, and because x is
  ;; fused with q.


  ;; 37. what value is associated with q in

  (run* [q]
    (fresh [x]
      (== `(~x ~x) q)))

  ;; (_0 _0). In the value of a `run*` expression, every instance of the same
  ;; fresh variable is replaced by the same reified variable.


  ;; 38. what value is associated with q in

  (run* [q]
    (fresh [x]
      (fresh [y]
        (== `(~q ~y) `((~x ~y) ~x)))))

  ;; (_0 _0), because the value of `(~x ~y) is associated with q, and because y
  ;; is fused with x, making y the same as x.


  ;; 39. when are two variables different?

  ;; two variables are different if they have not been fused. Every variable
  ;; introduced by `fresh` (or `run*`) is initially different from every other
  ;; variable.


  ;; 40. are q and x different variables in

  (run* [q]
    (fresh [x]
      (== 'pea q)))

  ;; yes, they are different.


  ;; 41. what value is associated with q in

  (run* [q]
    (fresh [x]
      (fresh [y]
        (== `(~x ~y) q))))

  ;; (_0 _1). In the value of a `run*` expression, each different fresh variable
  ;; is reified with an underscore followed by a distinct numeric subscript.


  ;; 42. what value is associated with s in

  (run* [s]
    (fresh [t]
      (fresh [u]
        (== `(~t ~u) s))))

  ;; (_0 _1). This expression and the previous expression differ only in the
  ;; names of their lexical variables. Such expressions have the same values.


  ;; 43. what value is associated with q in

  (run* [q]
    (fresh [x]
      (fresh [y]
        (== `(~x ~y ~x) q))))

  ;; (_0 _1 _0). x and y remain fresh, and since they are different variables,
  ;; they are reified differently. Reified variables are indexed by the order
  ;; they appear in the value produced by a `run*` expression.


  ;; 44. does

  (== '(pea) 'pea)

  ;; succeed?

  ;; no, since (pea) is not the same as pea.


  ;; 45. does

  (== `(~x) x)

  ;; succeed if (pea pod) is associated with x?

  ;; no, since ((pea pod)) is not the same as (pea pod).


  ;; 46. is there any value of x for which

  (== `(~x) x)

  ;; succeeds?

  ;; no. But what if x were fresh?


  ;; 47. even then, (== `(~x) x) could not succeed. No matter what value is
  ;; associated with x, x cannot be equal to a list in which x occurs.

  ;; what does it mean for x to *occur*?


  ;; 48. a variable x occurs in a variable y when x (or any variable fused with
  ;; x) appears in the value associated with y.

  ;; when do we say a variable occurs in a list?


  ;; 49. a variable x occurs in a list l when x (or any variable fused with x)
  ;; is an element of l, or when x occurs in an element of l. Does x occur in

  `(pea (~x) pod)

  ;; yes, because x is in the value of `(~x), the second element of the list.


  ;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Second Law of `==`
  ;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; if x is fresh, then (== v x) succeeds and associates v with x, unless x
  ;; occurs in v.


  ;; 50. what is the value of

  (run* [q]
    s# s#)

  ;; (_0), because multiple goals in the `run*` expression form implicit
  ;; conjunction goal in `core.logic`, i.e., it succeeds if all goals
  ;; succeed. Note: this differs from the original code, where explicit goal
  ;; `conj2` used to define a two-argument conjunction; so it is in the
  ;; following frames.


  ;; 51. what value is associated with q in

  (run* [q]
    s#
    (== 'corn q))

  ;; corn, because corn is associated with q when (== 'corn q) succeeds.


  ;; 52. what is the value of

  (run* [q]
    u#
    (== 'corn q))

  ;; (), because the implicit conjunction goal fails if one of its goals (u#
  ;; here) fail.


  ;; 53. yes. The conjunction goal also fails if any other of its goals
  ;; fail. What is the value of

  (run* [q]
    (== 'corn q)
    (== 'meal q))

  ;; (). In order for the conjunction goal to succeed, all its goals must
  ;; succeed. The first goal succeeds, associating corn with q. The second goal
  ;; cannot then associate meal with q, since q is no longer fresh.


  ;; 54. what is the value of

  (run* [q]
    (== 'corn q)
    (== 'corn q))

  ;; (corn). The first goal succeeds, associating corn with q. The second goal
  ;; succeeds because although q is no longer fresh, the value associated with q
  ;; is corn.


  ;; 55. what the value of

  (run* [q]
    (conde [u#] [u#]))

  ;; (), because the `conde` form is a disjunction goal, that succeeds if at
  ;; least one its goals (to be precise, the goal clauses, that can contain
  ;; multiple goals that form the conjunction) succeed, and here both of its
  ;; goals fail. Note: this differs from the original code, where explicit goal
  ;; `disj2` used to define a two-argument disjunction; so it is in the
  ;; following frames.


  ;; 56. what is the value of

  (run* [q]
    (conde [(== 'olive q)]
           [u#]))

  ;; (olive), because `conde` succeeds if any of its goals succeed, and here the
  ;; first one succeeds.


  ;; 57. what is the value of

  (run* [q]
    (conde [u#] [(== 'oil q)]))

  ;; (oil), because `conde` succeeds if any of its goals succeed, and here the
  ;; second one succeeds.


  ;; 58. what is the value of

  (run* [q]
    (conde [(== 'olive q)]
           [(== 'oil q)]))

  ;; (olive oil), a seq of two values. Both clauses contribute values. (==
  ;; 'olive q) succeeds, and olive is the first value associated with q. (==
  ;; 'oil q) also succeeds, and oil is the second value associated with q.


  ;; 59. what is the value of

  (run* [q]
    (fresh [x]
      (fresh [y]
        (conde [(== `(~x ~y) q)]
               [(== `(~y ~x) q)]))))

  ;; ((_0 _1) (_0 _1)), because both clauses of `conde` contribute values. In
  ;; the first value,x is reified as _0 and y is reified as _1. In the second
  ;; value, y is reified as _0 and x is reified as _1.


  ;; 60. correct! The variables x and y are not fused in the previous `run*`
  ;; expression, however. Each value produced by a `run*` expression is reified
  ;; independently of any other values. This means that the numbering of reified
  ;; variables begins again, from 0, within each reified value.


  ;; 61. do we consider

  (run* [x]
    (conde [(== 'olive x)]
           [(== 'oil x)]))

  ;; and

  (run* [x]
    (conde [(== 'oil x)]
           [(== 'olive x)]))

  ;; to be the same?

  ;; yes, because the first `run*` expression produces (olive oil), the second
  ;; `run*` expression produces (oil olive), and because the order of the values
  ;; does not matter.


  ;; 62. what is the value of

  (run* [x]
    (conde [(== 'olive x) u#]
           [(== 'oil x)]))

  ;; (oil).


  ;; 63. what is the value of

  (run* [x]
    (conde [(== 'olive x) s#]
           [(== 'oil x)]))

  ;; (olive oil).


  ;; 64. what is the value of

  (run* [x]
    (conde [(== 'oil x)]
           [(== 'olive x) s#]))

  ;; (oil olive).


  ;; 65. what is the value of

  (run* [x]
    (conde [(== 'virgin x) u#]
           [(conde [(== 'olive x)]
                   [(conde [s#]
                           [(== 'oil x)])])]))

  ;; (olive _0 oil). The clause [(== 'virgin x) u#] fails. Therefore, the body
  ;; of the `run*` behaves the same as the second `conde`.


  ;; 66. in the previous frame's expression, whose value is (olive _0 oil), how
  ;; do we end up with _0?

  ;; through the s# in the innermost `conde`, which succeeds without associatinf
  ;; a value with x.


  ;; 67. what is the value of this `run*` expression?

  (run* [r]
    (fresh [x]
      (fresh [y]
        (== 'split x)
        (== 'pea y)
        (== `(~x ~y) r))))

  ;; ((split pea)).


  ;; 68. is the value of this `run*` expression

  (run* [r]
    (fresh [x]
      (fresh [y]
        (== 'split x)
        (== 'pea y)
        (== `(~x ~y) r))))

  ;; the same as that of the previous frame?

  ;; yes. Can we make this expression shorter? Note: in Clojure expressions in
  ;; frames 67-68 are not only equivalent, but identical.


  ;; 69. is this

  (run* [r]
    (fresh [x]
      (fresh [y]
        (== 'split x)
        (== 'pea y)
        (== `(~x ~y) r))))

  ;; shorter?

  ;; very funny. Is there another way to simplify this `run*` expression?


  ;; 70. yes. If `fresh` were able to create any number of variables, how might
  ;; we rewrite the `run*` expression in the previous frame?

  ;; like this

  (run* [r]
    (fresh [x y]
      (== 'split x)
      (== 'pea y)
      (== `(~x ~y) r)))


  ;; 71. does the simplified expression in the previous frame still produce the
  ;; value ((split pea))?

  ;; yes. Can we keep simplifying this expression?


  ;; 72. sure. If `run*` were able to create any number of fresh variables, how
  ;; might we rewrite the expression from frame 70?

  ;; as this simpler expression,

  (run* [r x y]
    (== 'split x)
    (== 'pea y)
    (== `(~x ~y) r))


  ;; 73. does the expression in the previous frame still produce the
  ;; value ((split pea))?

  ;; no. The previous frame's `run*` expression produces ([(split pea) split
  ;; pea]), which is a list containing the values associated with r, x, and y,
  ;; respectively.


  ;; 74. how can we change the expression in frame 72 to get back the value from
  ;; frame 70, ((split pea))?

  ;; we can begin by removing r from the `run*` variable list.


  ;; 75. okay, so far. What else must we do, once we remove r from the `run*`
  ;; variable list?

  ;; we must remove (== `(~x ~y) r), which uses r. Here is the new `run*`
  ;; expression,

  (run* [x y]
    (== 'split x)
    (== 'pea y))


  ;; 76. what is the value of

  (run* [x y]
    (conde [(== 'split x)
            (== 'pea y)]
           [(== 'red x)
            (== 'bean y)]))

  ;; the list ([split pea] [red bean]).


  ;; 77. good guess! What is the value of

  (run* [r]
    (fresh [x y]
      (conde [(== 'split x)
              (== 'pea y)]
             [(== 'red x)
              (== 'bean y)])
      (== (list x y 'soup) r)))

  ;; the list ((split pea soup) (red bean soup)). Can we simplify this `run*`
  ;; expression?


  ;; 78. yes. `fresh` can take two goals, in which case it acts like a
  ;; conjunction goal. How might we rewrite the `run*` expression in the
  ;; previous frame?

  ;; like this,

  (run* [r]
    (fresh [x y]
      (conde [(== 'split x)
              (== 'pea y)]
             [(== 'red x)
              (== 'bean y)])
      (== (list x y 'soup) r)))

  ;; can `fresh` have more than two goals?


  ;; 79. yes. Original example asks to get rid of all explicit conjunction
  ;; goals, but in Clojure we didn't use explicit conjunction goals anyway, so
  ;; we cannot get rid of them.

  ;; `fresh` with multiple goals in Clojure also behaves as an implicit
  ;; conjunction goal, just like `run*`.


  ;; 80. This expression produces the value ((split pea soup) (red bean soup)),
  ;; just like the `run*` expression in frame 78.

  (run* [x y z]
    (conde [(== 'split x)
            (== 'pea y)]
           [(== 'red x)
            (== 'bean y)])
    (== 'soup z))

  ;; can this expression be simplified?

  ;; yes. We can allow `run*` to have more than one goal and act like a
  ;; conjunction goal, just as we did with `fresh`,

  (run* [x y z]
    (conde [(== 'split x)
            (== 'pea y)]
           [(== 'red x)
            (== 'bean y)])
    (== 'soup z))


  ;; 81. how can we simplify this `run*` expression from frame 75?

  (run* [x y]
    (== 'split x)
    (== 'pea y))

  ;; seems like in Clojure, it is already simple enough :)


  ;; 82. consider this very simple definition. It defines a relation. Note:
  ;; original example uses the `defrel` macro.

  (defn teacupo [t]
    (conde [(== 'tea t)]
           [(== 'cup t)]))

  ;; what is a relation?


  ;; 83. a relation is a kind of function that, when given arguments, produces a
  ;; goal. What is the value of

  (run* [x]
    (teacupo x))

  ;; (tea cup).


  ;; 84. what is the value of

  (run* [x y]
    (conde [(teacupo x)
            (== true y)]
           [(== false x)
            (== true y)]))

  ;; ([false true] [tea true] [cup true]). First (== false x) associates false
  ;; with x, then (teacupo x) associates tea with x, and finally (teacupo x)
  ;; associates cup with x.


  ;; 85. what is the value of

  (run* [x y]
    (teacupo x)
    (teacupo y))

  ;; ([tea tea] [tea cup] [cup tea] [cup cup]).


  ;; 86. what is the value of

  (run* [x y]
    (teacupo x)
    (teacupo x))

  ;; ([tea _0] [cup _0]). The first (teacupo x) associates tea with x and then
  ;; associates cup with x, while the second (teacupo x) already has the correct
  ;; associations for x, so it succeeds without associating anything. y remains
  ;; fresh.


  ;; 87. and what is the value of

  (run* [x y]
    (conde [(teacupo x)
            (teacupo x)]
           [(== false x)
            (teacupo y)]))

  ;; ([false tea] [false cup] [tea _0] [cup _0]).


  ;; 88. the `run*` expression in the previous frame has a pattern that appears
  ;; frequently: a disjunction containing conjunctions. This pattern appears so
  ;; often that we introduce a new form, `conde`.

  (run* [x y]
    (conde [(teacupo x)
            (teacupo x)]
           [(== false x)
            (teacupo y)]))

  ;; the original example asks to revise the `run*` expression from frame 76 to
  ;; use `conde`, but it uses `conde` already.


  ;; 89. the original example asks to revise the `run*` expression from frame 62
  ;; to use `conde`, but it uses `conde` already.


  ;; 90. what is the value of

  (run* [x y]
    (conde [(fresh [z]
              (== 'lentil z))]
           [(== x y)]))

  ;; ([_0 _0] [_0 _1]). In the first `conde` line x remains different from y,
  ;; and both are fresh. lentil is associated with z, which is not reified. In
  ;; the second `conde` line, both x and y remain fresh, but x is fused with y.


  ;; 91. we can extend the number of lines in a `conde`. What is the value of

  (run* [x y]
    (conde [(== 'split x)
            (== 'pea y)]
           [(== 'red x)
            (== 'bean y)]
           [(== 'green x)
            (== 'lentil y)]))

  ;; ([split pea] [red bean] [green lentil]). Does that mean that explicit
  ;; disjunction and conjunction goals are unnecessary?


  ;; 92. correct. We won't see them again until we go "Under the Hood" in
  ;; chapter 10. And with Clojure, maybe even then.

  ;; what does the "e" in `conde` stand for?


  ;; 93. it stands for "every", since every successful `conde` line contributes
  ;; one or more values.

  ;;;;;;;;;;;;;;;;;;;;;
  ;; The Law of `conde`
  ;;;;;;;;;;;;;;;;;;;;;

  ;; every successful `conde` line contributes one or more values.


  ;; now go make an almond butter and jam sandwich.

)


;; 2. Teaching Old Toys New Tricks

(comment

  ;; 1. what is the value of

  (first '(grape raisin pear))

  ;; grape.


  ;; 2. what is the value of

  (first '(a c o r n))

  ;; a.


  ;; 3. what value is associated with q in

  (run* [q]
    (firsto '(a c o r n) q))

  ;; a, because a is the `first` of (a c o r n).


  ;; 4. what value is associated with q in

  (run* [q]
    (firsto '(a c o r n) 'a))

  ;; _0, because a is the `first` of (a c o r n).


  ;; 5. what value is associated with r in

  (run* [r]
    (fresh [x y]
      (firsto (llist r y) x)
      (== 'pear x)))

  ;; pear. Since the `first` of (llist r y), which is the fresh variable r, is
  ;; fused with x. Then pear is associated with x, which in turn associates pear
  ;; with r.


  ;; 6. here is `firsto*` (`firsto` is already implemented in
  ;; `core.logic`). Note use of `lcons` instead of `cons`, as its arguments can
  ;; be any values, and Clojure's `cons` requires a collection or nil as the
  ;; second argument (cannot create pairs with improper tails, that somewhere
  ;; called "dotted").

  (defn firsto* [xs head]
    (fresh [tail]
      (== (lcons head tail) xs)))

  ;; what is unusual about this definition?

  ;; whereas `first` expects one argument, `firsto*` expects two.


  ;; 7. what is the value of

  (cons (first '(grape raisin pear))
        (first '((a) (b) (c))))

  ;; that is familiar: (grape a).


  ;; 8. what value is associated with r in

  ;; with self-implemented `firsto*`

  (run* [r]
    (fresh [x y]
      (firsto* '(grape raisin pear) x)
      (firsto* '((a) (b) (c)) y)
      (== (lcons x y) r)))

  ;; and with `firsto` provided by `core.logic`

  (run* [r]
    (fresh [x y]
      (firsto '(grape raisin pear) x)
      (firsto '((a) (b) (c)) y)
      (== (lcons x y) r)))

  ;; the same value: (grape a)


  ;; 9. why can we use `lcons` in the previous frame?

  ;; because variables introduced by `fresh` are values, and each argument to
  ;; `lcons` can be any value.


  ;; 10. what is the value of

  (rest '(grape raisin pear))

  ;; another familiar one: (raisin pear).


  ;; 11. what is the value of

  (first (rest (rest '(a c o r n))))

  ;; o.


  ;; 12. what value is associated with r in

  (run* [r]
    (fresh [v]
      (resto '(a c o r n) v)
      (fresh [w]
        (resto v w)
        (firsto w r))))

  ;; o. The process of transforming (first (rest (rest l))) into (resto l
  ;; v), (resto v w), and (firsto w r) is called *unnesting*. We introduct
  ;; `fresh` expressions as necessary as we unnest.


  ;; 13. define `resto*`.

  ;; it is almost the same as `firsto*`.

  (defn resto* [xs tail]
    (fresh [head]
      (== (lcons head tail) xs)))


  ;; 14. what is the value of

  (cons (rest '(grape raisin pear))
        (first '((a) (b) (c))))

  ;; also familiar: ((raisin pear) a).


  ;; 15. what value is associated with r in

  (run* [r]
    (fresh [x y]
      (resto '(grapr raisin pear) x)
      (firsto '((a) (b) (c)) y)
      (== (lcons x y) r)))

  ;; that's the same: ((raisin pear) a).


  ;; 16. what value is associated with q in

  (run* [q]
    (resto '(a c o r n) '(c o r n)))

  ;; _0, because (c o r n) is the `rest` of (a c o r n).


  ;; 17. what value is associated with x in

  (run* [x]
    (resto '(c o r n) (list x 'r 'n)))

  ;; o, because (o r n) is the `rest` of (c o r n), so o is associated with x.


  ;; 18. what value is associated with l in

  (run* [l]
    (fresh [x]
      (resto* l '(c o r n))
      (firsto* l x)
      (== 'a x)))

  ;; (a c o r n), because if the `rest` of l is (c o r n), then the list (list
  ;; head 'c 'o 'r 'n) is associated with l, where head is the variable
  ;; introduced in the definition of `resto*`. The `firsto*` of l, head, fuses
  ;; with x. When we associate a with x, we also associate a with head, so the
  ;; list (a c o r n) is associated with l.


  ;; 19. what value is associated with l in

  (run* [l]
    (conso '(a b c) '(d e) l))

  ;; ((a b c) d e), since `conso` associates the value of (lcons '(a b c) '(d
  ;; e)) with l.


  ;; 20. what value is associated with x in

  (run* [x]
    (conso x '(a b c) '(d a b c)))

  ;; d. Since (cons 'd '(a b c)) is (d a b c), `conso` associates d with x.


  ;; 21. what value is associated with r in

  (run* [r]
    (fresh [x y z]
      (== (list 'e 'a 'd x) r)
      (conso y (list 'a z 'c) r)))

  ;; (e a d c). We first associate (list 'e 'a 'd x) with r. We then perform the
  ;; `conso`, associating c with x, d with z, and e with y.


  ;; 22. what value is associated with x in

  (run* [x]
    (conso x (list 'a x 'c) (list 'd 'a x 'c)))

  ;; d, the value we can associate with x so that (lcons x (list 'a x 'c))
  ;; is (list 'd 'a x 'c).


  ;; 23. what value is associated with l in

  (run* [l]
    (fresh [x]
      (== (list 'd 'a x 'c) l)
      (conso x (list 'a x 'c) l)))

  ;; (d a d c). First we associate (list 'd 'a x 'c) with l. Then when we
  ;; `conso` x to (list 'a x 'c), we associate d with x.


  ;; 24. what value is associated with l in

  (run* [l]
    (fresh [x]
      (conso x (list 'a x 'c) l)
      (== (list 'd 'a x 'c) l)))

  ;; (d a d c), as in the previous frame. We `conso` x to (list 'a x 'c),
  ;; associating the list (list x 'a x 'c) with l. Then when we associate (list
  ;; 'd 'a x 'c) with l, we associate d with x.


  ;; 25. define `conso*` using `firsto` and `resto`.

  ;; here is a definition.

  (defn conso* [head tail xs]
    (firsto xs head)
    (resto xs tail))


  ;; 26. now, define the `conso**` relation using `==` instead of `firsto` and
  ;; `resto`.

  ;; here is the new `conso**`.

  (defn conso** [head tail xs]
    (== (lcons head tail) xs))


  ;; 27. here is a bonus question. What value is associated with l in

  (run* [l]
    (fresh [d t x y w]
      (conso w '(n u s) t)
      (resto l t)
      (firsto l x)
      (== 'b x)
      (resto l d)
      (firsto d y)
      (== 'o y)))

  ;; it's a five-element list, (b o n u s).


  ;; 28. what is the value of

  (empty? '(grape raisin pear))

  ;; false.


  ;; 29. what is the value of

  (empty? '())

  ;; true.


  ;; 30. what is the value of

  (run* [q]
    (emptyo '(grape raisin pear)))

  ;; ().


  ;; 31. what is the value of

  (run* [q]
    (emptyo '()))

  ;; (_0).


  ;; 32. what is the value of

  (run* [x]
    (emptyo x))

  ;; (()), since the only way (emptyo x) succeeds is if the empty list, (), is
  ;; associated with x.


  ;; 33. define `emptyo*` using `==`.

  ;; here is `emptyo*`.

  (defn emptyo* [x]
    (== '() x))


  ;; 34. is (llist 'split 'pea) a pair?

  ;; yes.


  ;; 35. is (llist 'split x) a pair?

  ;; yes.


  ;; 36. what is the value of

  ;; Clojure does not provide the `pair?` predicate, we should implement it
  ;; first

  (defn pair? [x]
    (or (lcons? x) (and (coll? x) (seq x))))

  (pair? (llist '(split) 'pea))

  ;; true


  ;; 37. what is the value of

  (pair? '())

  ;; nil


  ;; 38. is 'pair a pair?

  ;; no.


  ;; 39. is 'pear a pair?

  ;; no.


  ;; 40. is '(pear) a pair?

  ;; yes, it is the pair (pear . ()).


  ;; 41. what is the value of

  (first '(pear))

  ;; pear.


  ;; 42. what is the value of

  (rest '(pear))

  ;; ().


  ;; 43. how can we build these pairs?

  ;; use Cons the Magnificent. Note: in Clojure, `lcons` should be used to
  ;; create improper (dotted) pairs.


  ;; 44. what is the value of

  (lcons '(split) 'pea)

  ;; ((split) . pea).


  ;; 45. what value is associated with r in

  (run* [r]
    (fresh [x y]
      (== (lcons x (lcons y 'salad)) r)))

  ;; (_0 _1 . salad).


  ;; 46. here is `pairo*`.

  (defn pairo* [p]
    (fresh [head tail]
      (conso head tail p)))

  ;; is `pairo*` recursive?

  ;; no, it is not.


  ;; 47. what is the value of

  (run* [q]
    (pairo* (lcons q q)))

  ;; (_0). (lcons q q) creates a pair of the same fresh variable. But we are not
  ;; interested in the pair, only q.


  ;; 48. what is the value of

  (run* [q]
    (pairo* '()))

  ;; ().


  ;; 49. what is the value of

  (run* [q]
    (pairo* 'pair))

  ;; ().


  ;; 50. what value is associated with x in

  (run* [x]
    (pairo* x))

  ;; (_0 . _1).


  ;; 51. what value is associated with r in

  (run* [r]
    (pairo* (lcons r '())))

  ;; _0.


  ;; 52. is '(tofu) a *singleton*?

  ;; yes, because it is a list of a single value, tofu.


  ;; 53. is '((tofu)) a singleton?

  ;; yes, because it is a list of a single value, (tofu).


  ;; 54. is 'tofu a singleton?

  ;; no, because it is not a list of a single value.


  ;; 55. is (e tofu) a singleton?

  ;; no, because it is not a list of a single value.


  ;; 56. is '() a singleton?

  ;; no, because it is not a list of a single value.


  ;; 57. is '(e . tofu) a singleton?

  ;; no, because it is not a list of a single value.


  ;; 58. consider the definition of `singleton?`.

  (defn singleton? [l]
    (cond
      (pair? l) (empty? (rest l))
      :else     false))

  ;; what is the value of

  (singleton? '((a) (a b) c))

  ;; false.


  ;; 59. `singleton?` determines if its argument is a *proper list* of length
  ;; one.

  ;; what is a proper list?


  ;; 60. a list is *proper* if it is the empty list or if it is a pair whose
  ;; `rest` is proper. What is the value of

  (singleton? '())

  ;; false.


  ;; 61. what is the value of

  (singleton? (lcons 'pea '()))

  ;; true, because (pea) is a proper list of length one.


  ;; 62. what is the value of

  (singleton? '(sauerkraut))

  ;; true.


  ;; 63. to translate `singleton?` into `singletono*`, we must replace "else".
  ;; with true in the last `cond` line. Note: in Clojure any truthy expression
  ;; will do, as currently does a keyword, no redefinition is necessary.


  ;; 64. here is the translation of `singleton?`.

  (defn singletono* [l]
    (conde [(pairo l)
            (fresh [d]
              (resto l d)
              (emptyo d))]
           [s# u#]))

  ;; is `singletono*` a correct definiton?

  ;; it looks correct. How do we translate a function into a relation?


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Translation (Initial)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; To translate a function into a relation, unnest each expression in each
  ;; `cond` line, and replace each `cond` with `conde`. To unnest a true,
  ;; replace it with s#. To unnest a false, replace it with u#.


  ;; 65. where does

  (fresh [d]
    (resto l d)
    (emptyo d))

  ;; come from?

  ;; it is an unnesting of (empty? (rest l)). First we determine the `rest` of l
  ;; and associate it with the fresh variable d, and then we translate `empty?`
  ;; to `emptyo`.


  ;; 66. any `cond` line that has a top-level u# as a goal cannot contribute
  ;; values. Simplify `singletono*`.

  ;; here it is.

  (defn singletono** [l]
    (conde [(pairo l)
            (fresh [d]
              (resto l d)
              (emptyo d))]))


  ;;;;;;;;;;;;;;;;
  ;; The Law of u#
  ;;;;;;;;;;;;;;;;

  ;; any `conde` line that has u# as a top-level goal cannot contribute values.


  ;; 67. do we need (pairo* l) in the definition of `singletono**`?

  ;; no. This `conde` line also uses (resto l d). If d is fresh, then (pairo l)
  ;; succeeds exactly when (resto l d) succeeds. So here (pairo l) is
  ;; unnecessary.


  ;; 68. after we remove (pairo l), the `conde` has only one goal in its only
  ;; line. We can also replace the whole `conde` with just this goal. What is
  ;; our newly simplified definition of `singletono***`?

  ;; it's even shorter!

  (defn singletono*** [l]
    (fresh [d]
      (resto l d)
      (emptyo d)))


  ;; task: define both `firsto**` and `resto**` using `conso`.

  (defn firsto** [xs head]
    (fresh [tail]
      (conso head tail xs)))

  (defn resto** [xs tail]
    (fresh [head]
      (conso head tail xs)))

  ;; test it

  (run* [q]
    (firsto** '(a c o r n) q))

  (run* [q]
    (resto** '(a c o r n) q))

)


;; 3. Seeing Old Friends in New Ways

(comment

  ;; 1. Original example asks us to consider the definition of `list?`, but it
  ;; is unnecessary, as `seq?` is more appropriate in Clojure. What is the value
  ;; of

  (seq? '((a) (a b) c))

  ;; true.


  ;; 2. what is the value of

  (list? '())

  ;; true.


  ;; 3. what is the value of

  (list? 's)

  ;; false.


  ;; 4. what is the value of

  (list? (llist 'd 'a 't 'e 's))

  ;; false, because (d a t e . s) is not a proper list.


  ;; 5. translate `seq?` to `listo*`.

  ;; this is almost the same as `singletono*`.

  (defn listo* [l]
    (conde [(emptyo l) s#]
           [(pairo l)
            (fresh [d]
              (resto l d)
              (listo* d))]
           [s# u#]))


  ;; 6. where does

  (fresh [d]
    (resto l d)
    (listo d))

  ;; come from?

  ;; it is an unnesting of (seq? (rest l)). First we determine the `rest` of l
  ;; and associate it with the fresh variable d, and then we use d as the
  ;; argument in the recursion.


  ;; 7. here is a simplified version of `listo*`. What simplifications have we
  ;; made?

  (defn listo** [l]
    (conde [(emptyo l) s#]
           [(fresh [d]
              (resto l d)
              (listo d))]))

  ;; we have removed the final `conde` line, because The Law of u# says `conde`
  ;; lines that have u# as a top-level goal cannot contribute values. We also
  ;; have removed (pairo l), as in frame 2:68. Can we simplify `listo` further?


  ;; 8. yes, since any top-level s# can be removed from a `conde` line.

  ;; here is our simplified version.

  (defn listo*** [l]
    (conde [(emptyo l)]
           [(fresh [d]
              (resto l d)
              (listo d))]))


  ;;;;;;;;;;;;;;;;
  ;; The Law of s#
  ;;;;;;;;;;;;;;;;

  ;; any top-level s# can be removed from a `conde` line.


  ;; 9. what is the value of

  (run* [x]
    (listo (list 'a 'b x 'd)))

  ;; (_0), since x remains fresh.


  ;; 10. why is (_0) the value of

  (run* [x]
    (listo (list 'a 'b x 'd)))

  ;; for this use of `listo` to succeed, it is not necessary to determine the
  ;; value of x. Therefore x remains fresh, which shows that this use of `listo`
  ;; succeeds for any value associated with x.


  ;; 11. how is (_0) the value of

  (run* [x]
    (listo (list 'a 'b x 'd)))

  ;; `listo` gets the `rest` of each pair, and then uses recursion on that
  ;; `rest`. When `listo` reaches the end of (list 'a 'b x 'd), it succeeds
  ;; because (emptyo '()) succeeds, thus leaving x fresh.


  ;; 12. what is the value of

  #_(run* [x]
      (listo (llist 'a 'b 'c x)))

  ;; this expression has *no value*. Aren't there an unbounded number of
  ;; possible values that could be associated with x?


  ;; 13. yes, that's why it has no value. We can use `run 1` to get a list of
  ;; only the first value. Describe `run`'s behavior.

  ;; along with the arguments `run*` expects, `run` also expects a positive
  ;; number n. If the `run` expression has a value, its value is a list of at
  ;; most n elements.


  ;; 14. what is the value of

  (run 1 [x]
    (listo (llist 'a 'b 'c x)))

  ;; (()).


  ;; 15. what value is associated with x in

  (run 1 [x]
    (listo (llist 'a 'b 'c x)))

  ;; ().


  ;; 16. why is () the value associated with x in

  (run 1 [x]
    (listo (llist 'a 'b 'c x)))

  ;; because (a b c . x) is a proper list when x is the empty list.


  ;; 17. how is () the value associated with x in

  (run 1 [x]
    (listo (llist 'a 'b 'c x)))

  ;; when `listo` reaches the end of (a b c . x), (emptyo x) succeeds and
  ;; associates x with the empty list.


  ;; 18. what is the value of

  (run 5 [x]
    (listo (llist 'a 'b 'c x)))

  ;; (() (_0) (_0 _1) (_0 _1 _2) (_0 _1 _2 _3))


  ;; 19. why are the nonempty values lists of (_n)?

  ;; each _n corresponds to a fresh variable that has been introduced in the
  ;; goal of the second `conde` line of `listo`.


  ;; 20. we need one more example to understand `run`. In frame 1:91 we use
  ;; `run*` to produce all three values. How many values would be produced with
  ;; `run 7` instead of `run*`?

  ;; the same three values, ([split pea] [red bean] [green lentil]). Does that
  ;; mean if `run*` produces a list, then `run n` either produces the same list,
  ;; or a prefix of that list?


  ;; 21. yes. Here is `lol?`, where `lol?` stands for "list-of-lists?".

  (defn lol? [l]
    (cond
      (empty? l)       true
      (seq? (first l)) (lol? (rest l))
      :else            false))

  ;; describe what `lol?` does.

  ;; as long as each top-level value in the list l is a proper list, `lol?`
  ;; produces true. Otherwise, `lol?` produces false.


  ;; 22. here is the translation of `lol?`

  (defn lolo* [l]
    (conde [(emptyo l) s#]
           [(fresh [a]
              (firsto l a)
              (listo a))
            (fresh [d]
              (resto l d)
              (lolo* d))]
           [s# u#]))

  ;; simplify `lolo` using The Law of u# and The Law of s#.

  ;; here it is.

  (defn lolo** [l]
    (conde [(emptyo l)]
           [(fresh [a]
              (firsto l a)
              (listo a))
            (fresh [d]
              (resto l d)
              (lolo** d))]))


  ;; 23. what value is associated with q in

  (run* [q]
    (fresh [x y]
      (lolo (list (list 'a 'b) (list x 'c) (list 'd y)))))

  ;; _0, since the argument of `lolo` is a list of lists.


  ;; 24. what is the value of

  (run 1 [l]
    (lolo l))

  ;; (()). Since l is fresh, (emptyo l) succeeds and associates () with l.


  ;; 25. what value is associated with q in

  (run 1 [q]
    (fresh [x]
      (lolo (llist (list 'a 'b) x))))

  ;; _0, because `emptyo` of a fresh variable always succeeds and associates ()
  ;; with the fresh variable x.


  ;; 26. what is the value of

  (run 1 [x]
    (lolo (list (list 'a 'b) (list 'c 'd) x)))

  ;; (()), since replacing x with the empty list in the argument to `lolo`
  ;; transforms it to ((a b) (c d)), which is the same as ((a b) (c d)).


  ;; 27. what is the value of

  (run 5 [x]
    (lolo (llist (list 'a 'b) (list 'c 'd) x)))

  ;; (() (()) ((_0)) (() ()) ((_0 _1))).


  ;; 28. what do we get when we replace x in ((a b) (c d) . x) by the fourth
  ;; list in the previous frame?

  ;; ((a b) (c d) . (() ())), which is the same as ((a b) (c d) () ()).


  ;; 29. what is the value of

  (run 5 [x]
    (lolo x))

  ;; (() (()) ((_0)) (() ()) ((_0 _1))).


  ;; 30. is ((g) (tofu)) a list of singletons?

  ;; yes, since both (g) and (tofu) are singletons.


  ;; 31. is ((g) (e tofu)) a list of  singletons?

  ;; no, since (e tofu) is not a singleton.


  ;; 32. recall our definition of `singletono***` from frame 2:68.

  (defn singletono*** [l]
    (fresh [d]
      (resto l d)
      (emptyo d)))

  ;; redefine `singletono***` without using `resto` or `emptyo`.

  ;; here it is.

  (defn singletono**** [l]
    (fresh [a]
      (== (list a) l)))


  ;; 33. define `loso*`, where `los` stands for "list of singletons".

  ;; is this correct?

  (defn loso* [l]
    (conde [(emptyo l)]
           [(fresh [a]
              (firsto l a)
              (singletono a))
            (fresh [d]
              (resto l d)
              (loso* d))]))


  ;; 34. let's try it out. What value is associated with z in

  (run 1 [z]
    (loso* (llist (list 'g) z)))

  ;; ().


  ;; 35. why is () the value associated with z in

  (run 1 [z]
    (loso* (llist (list 'g) z)))

  ;; because ((g) . z) is a list of singletons when z is the empty list.


  ;; 36. what do we get when we replace z in ((g) . z) by ()?

  ;; ((g) . ()), which is the same as ((g)).


  ;; 37. how is () the value associated with z in

  (run 1 [z]
    (loso* (llist (list 'g) z)))

  ;; the variable l from the definition of `loso*` starts out as the list ((g)
  ;; . z). Since this list is not empty, (emptyo l) fails and we determine the
  ;; values contributed from the second `conde` line. In the second `conde`
  ;; line, d is fused with z, the `rest` of ((g) . z). The variable d is then
  ;; passed in the recursion. Since the variables d and z are fresh, (emptyo l)
  ;; succeeds and associates () with d and z.


  ;; 38. what is the value of

  (run 5 [z]
    (loso* (llist (list 'g) z)))

  ;; (() ((_0)) ((_0) (_1)) ((_0) (_1) (_2)) ((_0) (_1) (_2) (_3))).


  ;; 39. why are the nonempty values (_n)?

  ;; each _n corresponds to a fresh variable a that has been introduced in the
  ;; first goal of the second `conde` line of `loso*`.


  ;; 40. what do we get when we replace z in ((g) . z) by the fourth list in
  ;; frame 38?

  ;; ((g) . ((_0) (_1) (_2))), which is the same as ((g) (_0) (_1) (_2)).


  ;; 41. what is the value of

  (run 4 [r]
    (fresh [w x y z]
      (loso (llist (list 'g) (llist 'e w) (llist x y) z))
      (== (llist w (llist x y) z) r)))

  ;; ((() (_0)) (() (_0) (_1)) (() (_0) (_1) (_2)) (() (_0) (_1) (_2) (_3))).


  ;; 42. what do we get when we replace w, x, y, and z in (llist (list
  ;; 'g) (llist 'e w) (llist x y) z) using the third list in the previous frame?

  ;; ((g) (e) (_0) . ((_1) (_2))), which is the same
  ;; as ((g) (e) (_0) (_1) (_2)).


  ;; 43. what is the value of

  (run 3 [out]
    (fresh [w x y z]
      (== (llist (list 'g) (llist 'e w) (llist x y) z) out)
      (loso out)))

  ;; (((g) (e) (_0)) ((g) (e) (_0) (_1)) ((g) (e) (_0) (_1) (_2))).


  ;; 44. remember `member?*`.

  (defn member?* [x xs]
    (cond
      (empty? xs) false
      (= (first xs) x) true
      :else (member?* x (rest xs))))

  ;; true.


  ;; 45. try to translate `member?*`.

  ;; is this `membero*` correct?

  (defn membero* [x xs]
    (conde [(emptyo xs) u#]
           [(fresh [head]
              (firsto xs head)
              (== head x))
            s#]
           [s# (fresh [tail]
                 (resto xs tail)
                 (membero* x tail))]))


  ;; 46. yes, because `=` unnests to `==`. Simplify `membero*` using The Law of
  ;; u# and The Law of s#.

  ;; this is a simpler definition.

  (defn membero** [x xs]
    (conde [(fresh [head]
              (firsto xs head)
              (== head x))]
           [(fresh [tail]
              (resto xs tail)
              (membero** x tail))]))


  ;; 47. is this a simplification of `membero**`?

  (defn membero*** [x xs]
    (conde [(firsto xs x)]
           [(fresh [tail]
              (resto xs tail)
              (membero*** x tail))]))

  ;; yes, since in the previous frame (== head x) fuses a with
  ;; x. Therefore (firsto xs head) is the same as (firsto xs x).


  ;; 48. what value is associated with q in

  (run* [q]
    (membero* 'olive '(virgin olive oil)))

  ;; _0, because the use of `membero` succeeds, but this is still uninteresting;
  ;; the only variable q is not used in the body of the `run*` expression.


  ;; 49. what values is associated with y in

  (run 1 [y]
    (membero* y '(hummus with pita)))

  ;; hummus, because the first `conde` line in `membero` associates the value
  ;; of (first xs), which is hummus, with the fresh variable y.


  ;; 50. what value is associated with y in

  (run 1 [y]
    (membero* y '(with pita)))

  ;; with, because the first `conde` line associates the value of (first xs),
  ;; which is with, with the fresh variable y.


  ;; 51. what value is associated with y in

  (run 1 [y]
    (membero* y '(pita)))

  ;; pita, because the first `conde` line associates the value of (first xs),
  ;; which is pita, with the fresh variable y.


  ;; 52. what is the value of

  (run* [y]
    (membero* y '()))

  ;; (), because neither `conde` line succeeds.


  ;; 53. what is the value of

  (run* [y]
    (membero y '(hummus with pita)))

  ;; (hummus with pita). We already know the value of each recursion of
  ;; `membero`, provided y is fresh.


  ;; 54. so is the value of

  (run* [y]
    (membero y l))

  ;; always the value of l.

  ;; yes, when l is a proper list.


  ;; 55. what is the value of

  (run* [y]
    (membero y l))

  ;; where l is (pear grape . peaches)? Let's see

  (let [l (llist 'pear 'grape 'peaches)]
    (run* [y]
      (membero y l)))

  ;; (pear grape). y is not the same as l in this case, since l is not a proper
  ;; list.


  ;; 56. what value is associated with x in

  (run* [x]
    (membero 'e (list 'pasta x 'fagioli)))

  ;; e. The list contains three values with a variable in the middle. `membero`
  ;; determines that e is associated with x.


  ;; 57. why is e the value associated with x in

  (run* [x]
    (membero 'e (list 'pasta x 'fagioli)))

  ;; because e is the only value that can be associated with x so that (membero
  ;; 'e (list 'pasta x 'fagioli)) succeeds.


  ;; 58. what have we just done?

  ;; we filled in a blank in the list so that `membero` succeeds.


  ;; 59. what value is associated with x in

  (run 1 [x]
    (membero 'e (list 'pasta 'e x 'fagioli)))

  ;; _0, because the recursion reaches e, and succeeds, before it gets to x.


  ;; 60. what value is associated with x in

  (run 1 [x]
    (membero 'e (list 'pasta x 'e 'fagioli)))

  ;; e, because the recursion reaches the variable x, and succeeds, before it
  ;; gets to e.


  ;; 61. what is the value of

  (run* [x y]
    (membero 'e (list 'pasta x 'fagioli y)))

  ;; ([e _0] [_0 e])


  ;; 62. what does each value in the list mean?

  ;; there are two values in the list. We know from frame 60 that for the first
  ;; value when e is associated with x, `membero` succeeds, leaving y
  ;; fresh. Then we determine the second value. Here, e is associated with y,
  ;; while leaving x fresh.


  ;; 63. what is the value of

  (run* [q]
    (fresh [x y]
      (== (list 'pasta x 'fagioli y) q)
      (membero 'e q)))

  ;; ((pasta e fagioli _0) (pasta _0 fagioli e)).


  ;; 64. what is the value of

  (run 1 [l]
    (membero 'tofu l))

  ;; ((tofu . _0)).


  ;; 65. which lists are represented by (tofu . _0)?

  ;; every list whose `first` is tofu.


  ;; 66. what is the value of

  #_(run* [l]
      (membero 'tofu l))

  ;; it has no value, because `run*` never finishes building the list.


  ;; 67. what is the value of

  (run 5 [l]
    (membero* 'tofu l))

  ;; ((tofu . _0) (_0 tofu . _1) (_0 _1 tofu . _2) (_0 _1 _2 tofu . _3) (_0 _1
  ;; _2 _3 tofu . _4)). tofu is in every list. But can we require each list
  ;; containing tofu to be a proper list, instead of having a dot before each
  ;; list's final reified variable?


  ;; 68. perhaps. This final reified variable appears in each value just after
  ;; we find tofu. In `membero*`, which `conde` line associates tofu with the
  ;; `first` of a pair?

  ;; the first line, [(firsto xs x)].


  ;; 69. what does `membero*`'s first `conde` line say about the `rest` of xs?

  ;; nothing. This is why the final `rest`s remain fresh in frame 67.


  ;; 70. if the `rest` of l is (), is l a proper list?

  ;; yes.


  ;; 71. if th `rest` of l is (beet), is l a proper list?

  ;; yes.


  ;; 72. suppose l is a proper list. What values could be l's `rest`?

  ;; any proper list.


  ;; 73. here is `proper-membero*`.

  (defn proper-membero* [x xs]
    (conde [(firsto xs x)
            (fresh [tail]
              (resto xs tail)
              (listo tail))]
           [(fresh [tail]
              (resto xs tail)
              (proper-membero* x tail))]))

  ;; do `proper-membero*` and `membero*` differ?

  ;; yes. The `rest` of xs in the first `conde` line of `proper-membero*` must
  ;; be a proper list.


  ;; 74. now what is the value of

  (run 12 [l]
    (proper-membero 'tofu l))

  ;; ((tofu) (tofu _0) (_0 tofu) (tofu _0 _1) (tofu _0 _1 _2) (_0 tofu _1) (tofu
  ;; _0 _1 _2 _3) (tofu _0 _1 _2 _3 _4) (_0 _1 tofu) (_0 tofu _1 _2) (tofu _0 _1
  ;; _2 _3 _4 _5) (tofu _0 _1 _2 _3 _4 _5 _6))


  ;; 75. is ther a function `proper-member?*` we can transform and simplify into
  ;; `proper-membero*`?

  ;; yes. And here it is.

  (defn proper-member?* [x xs]
    (cond
      (empty? xs) false
      (= (first xs) x) (seq? (rest xs))
      :else (proper-member?* x (rest xs))))


  ;; now go make a cashew butter and marmalade sandwich and eat it!

)


;; 4. Double Your Fun

(comment

  ;; 1. here is `append*`.

  (defn append* [xs ys]
    (cond
      (empty? xs) ys
      :else (cons (first xs)
                  (append* (rest xs) ys))))

  ;; what is the value of

  (append* '(a b c) '(d e))

  ;; (a b c d e).


  ;; 2. what is the value of

  (append* '(a b c) '())

  ;; (a b c).


  ;; 3. what is the value of

  (append* '() '(d e))

  ;; (d e).


  ;; 4. what is the value of

  #_(append* 'a '(d e)),

  ;; it has no meaning, because a is not a proper list.


  ;; 5. what is the value of

  (append* '(d e) 'a)

  ;; it has no meaning, again? (in Clojure, that's right.)


  ;; 6. no. The value is (d e . a).

  ;; how is that possible?


  ;; 7. look closely at the definition of `append*`.

  ;; there are no `cond`-line questions asked about t. Ouch.


  ;; 8. here is the translation from `append*` and its simplification to
  ;; `appendo*`. (`core.logic` contains `appendo`.)

  (defn appendo* [xs ys out]
    (conde [(emptyo xs) (== ys out)]
           [(fresh [res]
              (fresh [xs-tail]
                (resto xs xs-tail)
                (appendo* xs-tail ys res))
              (fresh [xs-head]
                (firsto xs xs-head)
                (conso xs-head res out)))]))

  ;; how does `appendo*` differ from `listo`, `lolo`, and `membero`?

  ;; the `list?`, `lol?`, and `member?` definitions from the previous chapter
  ;; have only booleans as their values. `append*`, on the other hand, has more
  ;; interesting values. Are there consequences of this difference?


  ;; 9. yes, we introduce an additional argument, which here we call out, that
  ;; holds the value that would have been produced by `append*`'s value.

  ;; that's like `firsto`, `resto`, and `conso`, which also take an additional
  ;; argument.


  ;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Translation (Final)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; to translate a function into a relation, unnest each expression in each
  ;; `cond` line, and replace each `cond` with `conde`. To unnest a true,
  ;; replace it with s#. To unnest a false, replace it with u#. If the value of
  ;; at least one `cond` line can be a non-boolean, add an argument, say out, to
  ;; hold what would have been the function's value. When unnesting a line whose
  ;; value is not a boolean, ensure that either some value is associated with
  ;; out, or that out is the last argument to a recursion.


  ;; 10. why are there three `fresh`es in the `appendo*` definition?

  ;; because xs-tail is only mentioned in `resto` and `appendo*` clauses,
  ;; xs-head is only mentioned in `firsto` and `conso` clauses. But res is
  ;; mentioned in both inner `fresh`es.


  ;; 11. rewrite

  (fresh [res]
    (fresh [xs-tail]
      (resto xs xs-tail)
      (appendo* xs-tail ys res))
    (fresh [xs-head]
      (firsto xs xs-head)
      (conso xs-head res out)))

  ;; using only one `fresh`.

  (fresh [xs-head xs-tail res]
    (resto xs xs-tail)
    (appendo* xs-tail ys res)
    (firsto xs xs-head)
    (conso xs-head res out))


  ;; 12. how might we use `conso` in place of the `firsto` and `resto`?

  (fresh [xs-head xs-tail res]
    (conso xs-head xs-tail xs)
    (appendo* xs-tail ys res)
    (conso xs-head res out))


  ;; 13. redefine `appendo*` using these simplifications.

  ;; here it is.

  (defn appendo** [xs ys out]
    (conde [(emptyo xs) (== ys out)]
           [(fresh [xs-head xs-tail res]
              (conso xs-head xs-tail xs)
              (appendo** xs-tail ys res)
              (conso xs-head res out))]))


  ;; 14. can we similarly simplify our definitions of `loso*` as in frame 3:33,
  ;; `lolo*` as in frame 3:22, and `proper-membero*` as in frame 3:73?

  ;; yes.


  ;; 15. in our simplified definition of `appendo**`, how does the first `conso`
  ;; differ from the second one?

  ;; the first `conso`, (conso xs-head xs-tail xs), appears to associate values
  ;; with the variables xs-head and xs-tail. In other words, it appears to take
  ;; apart a `cons` pair, whereas (conso xs-head res out) appears to build a
  ;; `cons` pair.


  ;; 16. but, appearances can be deceiving?

  ;; indeeed they can.


  ;; 17. what is the value of

  (run 6 [x]
    (fresh [y z]
      (appendo x y z)))

  ;; (() (_0) (_0 _1) (_0 _1 _2) (_0 _1 _2 _3) (_0 _1 _2 _3 _4)).


  ;; 18. what is the value of

  (run 6 [y]
    (fresh [x y z]
      (appendo x y z)))

  ;; (_0 _0 _0 _0 _0 _0).


  ;; 19. since x is fresh, we know the first value comes from (emptyo l), which
  ;; succeeds, associating () with xs, and then ys, which is also fresh, is
  ;; fused with out. But, how do we get the second through the sixth values?

  ;; a new fresh variable res is passed into each recursion to
  ;; `appendo`. After (emptyo xs) succeeds, ys is fused with res, which is
  ;; fresh, since res is passed an argument (binding out) in the recursion.


  ;; 20. what is the value of

  (run 6 [z]
    (fresh [x y]
      (appendo x y z)))

  ;; (_0 (_0 . _1) (_0 _1 . _2) (_0 _1 _2 . _3) (_0 _1 _2 _3 . _4) (_0 _1 _2 _3
  ;; _4 . _5))


  ;; 21. now let's look at the first six values of x, y, and z at the same
  ;; time. What is the value of

  (run 6 [x y z]
    (appendo x y z))

  ;; ([() _0 _0] [(_0) _1 (_0 . _1)] [(_0 _1) _2 (_0 _1 . _2)] [(_0 _1 _2)
  ;; _3 (_0 _1 _2 . _3)] [(_0 _1 _2 _3) _4 (_0 _1 _2 _3 . _4)] [(_0 _1 _2 _3 _4)
  ;; _5 (_0 _1 _2 _3 _4 . _5)])


  ;; 22. what value is associated with x in

  (run* [x]
    (appendo '(cake)
             '(tastes yummy)
             x))

  ;; (cake tastes yummy).


  ;; 23. what value is associated with x in

  (run* [x]
    (fresh [y]
      (appendo (list 'cake '& 'ice y)
               '(tastes yummy)
               x)))

  ;; (cake & ice _0 tastes yummy).



)
