(ns hello-logic.relations
  (:require
   [clojure.core.logic :refer [== s# u# fresh conde
                               firsto resto conso emptyo
                               llist lcons lcons?]]))


;; some predicates and relations used in Reasoned Schemer walkthrough that are
;; not provided by `core.logic`.


(defn pair? [x]
  (or (lcons? x) (and (coll? x) (seq x))))

(defn pairo [p]
  (fresh [head tail]
    (conso head tail p)))


(defn singleton? [l]
  (cond
    (pair? l) (empty? (rest l))
    :else     false))

(defn singletono [l]
  (fresh [d]
    (resto l d)
    (emptyo d)))



(defn listo [l]
  (conde [(emptyo l) s#]
         [(fresh [d]
            (resto l d)
            (listo d))]))


(defn lol? [l]
  (cond
    (empty? l)       true
    (seq? (first l)) (lol? (rest l))
    :else            false))

(defn lolo [l]
  (conde [(emptyo l)]
         [(fresh [a]
            (firsto l a)
            (listo a))
          (fresh [d]
            (resto l d)
            (lolo d))]))

(defn loso [l]
  (conde [(emptyo l)]
         [(fresh [a]
            (firsto l a)
            (singletono a))
          (fresh [d]
            (resto l d)
            (loso d))]))

(defn member? [x xs]
  (cond
    (empty? xs) false
    (= (first xs) x) true
    :else (member? x (rest xs))))

(defn proper-member? [x xs]
  (cond
    (empty? xs) false
    (= (first xs) x) (seq? (rest xs))
    :else (proper-member? x (rest xs))))

(defn proper-membero [x xs]
  (conde [(firsto xs x)
          (fresh [tail]
            (resto xs tail)
            (listo tail))]
         [(fresh [tail]
            (resto xs tail)
            (proper-membero x tail))]))
