(ns hello-logic.unify
  (:require [clojure.core.unify :refer [unifier]]))


;; basics

;; https://github.com/clojure/core.unify

(comment

  ;; `core.unify` provides a la carte unification facilities that are not deeply
  ;; tied into the operation of a logic engine. While `core.logic` does provide
  ;; a similar simple unifier interface with support for specifying fine-grained
  ;; constraints, if you have no need for a logic programming system,
  ;; `core.unify` may be a better fit.


  ;; basic usage

  ;; symbols prefixed with '?' are variables

  (unifier '((?a * ?x ** 2) + (?b * ?x) + ?c)
           '(?z + (4 * 5) + 3))

)
