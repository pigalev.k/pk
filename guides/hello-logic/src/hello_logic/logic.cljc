(ns hello-logic.logic
  (:require
   [clojure.core.logic :as l]
   [clojure.core.logic.fd :as fd]))


;; basics

;; https://github.com/clojure/core.logic
;; https://github.com/clojure/core.logic/wiki
;; https://rawgit.com/dedeibel/clojure-core-logic-cheatsheets/master/out/cheatsheet-use-title-attribute-no-cdocs-summary.html

(comment

  ;; a logic program consists of a logic expression(s) and a solution engine or
  ;; solver. A logic expression is a set of logic variables and constraints upon
  ;; those variables. The logic expression is input to the logic engine which
  ;; returns all assignments to the logic variables that satisfy the constraints
  ;; in the expression. Generally speaking, you write the logic expression, and
  ;; the solver is provided (`core.logic` is the solver).

  ;; logic expression

  ;; a logic expression comprises a set of logic variables and a set of
  ;; constraints upon the values that those logic variables can assume.

  ;; logic variable

  ;; a logic variable, or lvar, is a variable that contains an ambiguous
  ;; value. That is, the variable can take several values, but only one at a
  ;; time.

  ;; constraint

  ;; constraints are expressions that limit the values the lvars may assume.


  ;; basic usage

  ;; take the set of logic-expressions, run them through the solver and return
  ;; all the values of logic-variable that satisfy the logic-expression.

  (l/run* [logic-variable]
    (l/== logic-variable true))

  (l/run* [q]
    (l/membero q [1 2 3])
    (l/membero q [2 3 4]))


  ;; pattern matching

  ;; some parts of `core.logic` support pattern matching. Patterns also provide
  ;; syntactic sugar to make it easy to describe collections in
  ;; `core.logic`. There are 2 reserved symbols that have special meanings in
  ;; patterns: "_" and ".".

  ;; lvar

  ;; patterns treat symbols specially. A symbol with a local binding (i.e.,
  ;; function arguments, or declared by something like `let` or `fresh`) will be
  ;; handled as normal. In contrast a symbol that is undefined locally is
  ;; implicitly treated as a newly named logical variable (as lvar).

  ;; the symbol `_` is even more special, and completely ignores local
  ;; bindings. Each appearance of `_` in the pattern will be converted to an
  ;; lvar --- but a new one each time. Thus the pattern [! ! !] (a new, implicit
  ;; lvar repeated 3 times) can unify with [1 1 1] but not [1 2 3]. The
  ;; pattern [_ _ _] can unify with either since it is 3 distinct lvar.

  ;; lcons

  ;; lcons (logical-cons) syntax may be used in any part of a pattern describing
  ;; a collection. That collection pattern contains a `.` symbol, e.g. [h1 h2 h3
  ;; . t2]. Each symbol in the pattern describes the respective element in the
  ;; collection. The exception to this is the last symbol, which describes the
  ;; rest of the collection after the other variables are associated.

  ;; symbols `a`/`b`/`c`/`d` have unified with the first 4 elements of the
  ;; collection, and e has unified with the rest. If the collection was small
  ;; enough, the last symbol may unify with an empty list.

  (l/run* [q a b c d e]
    (l/== q (range 10))
    (l/matche [q]
              [[[. a b c d e]]]))


  ;; examples

  ;; not a local binding, doesn't influence the following examples
  (def a 10)

  ;; lcons & lvar

  (l/run* [q]
    (l/matche [q]
              ([[a . b]])))
  ;; => ((_0 . _1)) ; `a` and `b` are both new, implicit lvar since they are not
  ;; bound locally and there is no value to unify them with

  ;; local binding

  (let [a 8]
    (l/run* [q]
      (l/matche [q]
                ([ [a . b] ]))))
  ;; => ((8 . _0)) ; `a` is bound locally so the value is used instead of
  ;; creating a new lvar


  ;; `defne` to create new goals with patterns
  (l/defne head-is-1 [q]
    ([(1 . _)]))

  (l/defne tail-is-7-8-9 [q]
    ([[_ . [7 8 9]]]))

  (l/run 2 [q]
    (tail-is-7-8-9 q)
    (head-is-1 q))
  ;; => (1 7 8 9)


  ;; more on logic variables

  ;; logic variables are similar to locals in Clojure. You can declare their
  ;; value, they are lexically scoped, and not assignable. Most of the
  ;; similarities end there.

  ;; for example, sometimes a logic variable may never actually take on a
  ;; specific value(s). A special value for lvars is `_N`, where N is some
  ;; number. This means "anything", i.e., that the lvar can take on any value
  ;; and still satisfy the constraints. Sometimes the variable is said to
  ;; be "nonground." We'll refer to "nonground" variables as
  ;; being "fresh". Furthermore, if you have two variables that can be anything
  ;; they will have values like _0 and _1, meaning that each can be anything and
  ;; they can be distinct from another. If they were both _0, it would mean
  ;; they can be anything but must be equal to another.

  ;; logic variables are introduced into a program in two ways.

  ;; the first is the main query variable introduced in

  ;; (l/run* [query-variable] ...)

  ;; whose values will be the return value of the (`run*` ...) expression. There
  ;; can only be one such main query variable.

  ;; The other method of introducing logic variables is using the `fresh`
  ;; operator, much like `let` in normal Clojure.

  ;; (l/fresh [a b c] & logic-expressions)

  ;; this introduces three logic variables `a`, `b` and `c`, that may be used
  ;; within the `logic-expressions`.


  ;; more on constraints

  ;; logic expressions constrain the values of logic variables. All expressions
  ;; directly beneath `run*` are combined in a logical AND (sometimes referred
  ;; to as conjunction). So in

  ;; (run* [q]
  ;;   (constraint-1)
  ;;   (constraint-2)
  ;;   (constraint-3))

  ;; each expression constrains `q` in some way, and `run*` will return the
  ;; values of `q` that satisfies all three constraints in the expression.

  ;; goals

  ;; `core.logic` is based upon miniKanren. miniKanren dictates that we define
  ;; our constraints in a particular way. Each is a goal that either succeeds or
  ;; fails.

  ;; the logic engine explores the possible values of all the lvars and returns
  ;; the value of `q` in each case where the logic expression, as a whole,
  ;; succeeds. In the example above all the goals are conjunction, hence the
  ;; expression succeeds if and only if all the goals succeed.


  ;; operators

  ;; miniKanren and hence `core.logic` are based upon three core operators:
  ;; `fresh`, `==` and `conde`.

  ;; `fresh`

  ;; we have already seen `fresh`, which introduces new lvars, in the fresh
  ;; state, into the logic program.

  ;; `unify`, or `==`

  ;; the most fundamental operator.

  ;; (== lvar1 lvar2)

  ;; serves to constrain each lvar to have the same set of possible values. It
  ;; is a goal that succeeds if lvar1 can be made equal to lvar2, otherwise it
  ;; fails.

  ;; unification of a single lvar with a literal

  ;; in the simplest case you can use Clojure literals for lvar1 or lvar2. For
  ;; example the expression:

  (l/run* [q]
    (l/== q 1))

  ;; succeeds if `q` can be made to have the value of 1. As `run*` introduces
  ;; `q` in the fresh state, this is possible. Hence the `unify` goal succeeds,
  ;; hence `run*` returns the list of successful values of `q`, that
  ;; is (1). This means that the set of possible values that `q` can assume,
  ;; under this constraint, is only the integer 1. The mental shortcut is to view
  ;; `unify` as constraining `q` to be equal to 1.

  ;; you can unify more complex datatypes too:

  (l/run* [q]
   (l/== q {:a 1 :b 2}))

  (l/run* [q]
   (l/== {:a q :b 2} {:a 1 :b 2}))

  ;; finally, the order of the arguments to `==` is irrelevant,

  (l/run* [q]
   (l/== 1 q))

  ;; is also (1).

  ;; gotcha: now although when printed an lvar looks like a list, this is not
  ;; the literal syntax for an lvar (the reader will not read it); there is not
  ;; in fact a literal syntax for an lvar. So the program

  (l/run* [q]
    (l/== q '(1 2 3)))

  ;; evaluates to ((1 2 3)), not (1 2 3). That is, `q` can only take the value
  ;; of the list (1 2 3), not either 1 or 2 or 3, which is what you might have
  ;; expected.

  ;; as stated above, `run*` finds the solutions that satisfy all the
  ;; constraints. This is because `run*` composes the goals in logical
  ;; conjunction. Thus,

  (l/run* [q]
    (l/== q 1)
    (l/== q 2))

  ;; returns (): there are no values of `q` that satisfy all the
  ;; constraints. That is, it's impossible for `q` to equal 1 AND for `q` to
  ;; equal 2 at the same time.

  ;; unification of two lvars

  ;; when you unify two lvars, the operation constrains each lvar to have the
  ;; same set of possible values. A mental shortcut is to consider unification
  ;; to be the intersection of the two sets lvar values.

  ;; to demonstrate this we have to cheat and go out of order a bit. As
  ;; mentioned there is no literal syntax for an lvar, so to constrain lvar1 to
  ;; have value (1, 2, 3), we have to introduce a goal, `membero`:

  ;; (l/membero x 1)

  ;; for now, and this is true but not the full story, think of it as
  ;; constraining `x` to be an element of the list `l`. So the program

  (l/run* [q]
    (l/membero q [1 2 3]))

  ;; evaluates to (1, 2, 3), meaning that `q` can be either 1 or 2 or 3, as we
  ;; wanted. Now we have our full demonstration:

  (l/run* [q]
    (l/membero q [1 2 3])
    (l/membero q [3 4 5]))

  ;; first we use `run*` and ask it to return the value of the lvar `q` under a
  ;; set of constraints. In the first we constrain `q` to have the value (1, 2,
  ;; 3), that is, `q` can be either 1 or 2 or 3. Then we constrain the same
  ;; lvar, `q`, to have the value (3, 4, 5), that is, `q` can be 3 or 4 or 5. In
  ;; order to satisfy both constraints `q` can only be 3, hence `run*`
  ;; returns (3). Another way to write the same thing, with unification is

  (l/run* [q]
    (l/fresh [a]
      (l/membero a [1 2 3])
      (l/membero q [3 4 5])
      (l/== a q)))

  ;; here we introduce an additional lvar, `a`, with `fresh` and constrain it to
  ;; being either 1 or 2 or 3. Then we constrain `q` to being either 3 or 4 or
  ;; 5. Finally we unify `a` and `q` leaving both with the value of their
  ;; intersection: (3).


  ;; `core.logic` is declarative

  ;; now some magic: the order of constraints does not matter as far as the
  ;; value of the (`run*` ...) expression is concerned. So the programs:

  (l/run* [q]
    (l/fresh [a]
      (l/membero q [1 2 3])
      (l/membero a [3 4 5])
      (l/== q a)))

  (l/run* [q]
    (l/fresh [a]
      (l/membero a [3 4 5])
      (l/== q a)
      (l/membero q [1 2 3])))

  (l/run* [q]
    (l/fresh [a]
      (l/== q a)
      (l/membero a [3 4 5])
      (l/membero q [1 2 3])))

  ;; all evaluate to (3).

  ;; `conde`

  ;; `conde` behaves a lot like `cond`, and is logical disjunction (OR). Its
  ;; syntax is

  ;; (l/conde &clauses)

  ;; where each clause is a series of at least one goal composed in
  ;; conjunction. `conde` succeeds for each clause that succeeds,
  ;; independently. You can read a `conde` goal

  ;; (run* [q]
  ;;   (conde
  ;;    [goal1 goal2 ...]
  ;;    ...))

  ;; a bit like this:

  ;; (run* [q]
  ;;   (OR
  ;;    [goal1 AND goal2 AND ...]
  ;;    ...))

  ;; some further examples may help:

  (l/run* [q]
    (l/conde
     [l/succeed]))

  ;; returns (_0). conde succeeds because `succeed` succeeds, however `q` is not
  ;; involved and hence can take on any value.

  ;; there can be any number of goals in the clause:

  (l/run* [q]
    (l/conde
     [l/succeed l/succeed l/succeed l/succeed]))

  ;; also returns (_0) and each term in the clause for `conde` succeeds, and
  ;; they are combined in logical conjunction, hence succeeding. However,

  (l/run* [q]
    (l/conde
     [l/succeed l/succeed l/fail l/succeed]))

  ;; returns (), as `conde` failed because of the `fail` in the clause, which
  ;; due to the conjunction causes the entire clause to fail. Thus there are no
  ;; values of `q` that can cause the expression to succeed.

  ;; furthermore, `conde` succeeds or fails for each clause independently

  (l/run* [q]
    (l/conde
     [l/succeed]
     [l/succeed]))

  ;; returns (_0 _0). There are two values here because `conde` succeeds twice,
  ;; once for each clause.

  (l/run* [q]
    (l/conde
     [l/succeed]
     [l/fail]))

  ;; returns (_0) because `conde` succeeds only for the first clause.

  ;; the above examples show the logical structure, but let's see some output.

  (l/run* [q]
    (l/conde
     [l/succeed (l/== q 1)]))

  ;; returns (1). This is because `succeed` and (`l/==` q 1) only both succeed
  ;; if `q` can be made equal to 1. Having two elements in the conde clause is
  ;; the usual structure as it reminds us of cond, but don't be misled:

  (l/run* [q]
    (l/conde
      [(l/== q 2) (l/== q 1)]))

  ;; returns (). If you expect `conde` to produce the same results as `cond`,
  ;; your thought process might incorrectly lead you to assume that this will
  ;; return (1). While following from a seemingly logical set of premises, it is
  ;; essential to understand that this is patently wrong --- each goal in a
  ;; `conde` clause is composed in conjunction. Since it is not possible for `q`
  ;; to be 2 and 1 at the same time, the clause fails, and hence `conde`
  ;; fails. Without a possibility of success, `q` returns the value ().

  (l/run* [q]
    (l/conde
     [(l/== q 1)]
     [(l/== q 2)]))

  ;; returns (1 2). The difference here is that each unification is in a
  ;; different clause of conde, and each can succeeds independently, producing a
  ;; value for `q`.


  ;; more goals

  ;; we've seen the interaction of the three operators: `fresh`, `conde` and
  ;; `==`, and these are the primitives of logic programming. `core.logic`
  ;; provides some higher level goals based on these primitives.

  ;; `conso` (the magnificent)

  ;; the most basic of these goals is `conso`; understand this and the rest will
  ;; follow. `conso` is, unsurprisingly, the logic programming equivalent of
  ;; `cons`. Recall

  ;; (cons x r)

  ;; returns `s` where `s` is a seq with the element `x` as its head and the seq
  ;; `r` as its rest:

  (cons 0 [1 2 3])

  ;; returns (0 1 2 3). `conso` is very similar but with the 'resulting' seq
  ;; passed as an argument

  ;; (conso x r s)

  ;; it is a function that succeeds only if `s` is a list with head `x` and rest
  ;; `r`. Hence, within a logic expression it constrains `x`, `r` and `s` in
  ;; this way. Again we can use either lvars or literals for any of `x` `r`
  ;; `s`. So:

  (l/run* [q]
    (l/conso 1 [2 3] q))

  ;; returns ((1 2 3)); that is, `q` is the lvar that can only take on the value
  ;; of the list (1 2 3). We have asked `run*` to find the value of `q`, being a
  ;; list with head 1 and rest (2 3), and it finds (1 2 3).

  (l/run* [q]
    (l/conso 1 q [1 2 3]))

  ;; returns ((2 3)); `q` is constrained to be that list which, when 1 is added
  ;; as the head results in the list (1 2 3).

  (l/run* [q]
    (l/conso q [2 3] [1 2 3]))

  ;; returns (1); `q` is the element that when added to the head of the
  ;; vector (2 3) results in vector (1 2 3). Even more interesting:

  (l/run* [q]
    (l/conso 1 [2 q] [1 2 3]))

  ;; returns (3); `q` is that element of the vector (2 element) that when 1 is
  ;; added as the head becomes the vector (1 2 3).

  ;; in summary: (conso f r s) succeeds if (first s) is f and (rest s) is r.

  ;; `resto`

  ;; `resto` is the complement of `conso`;

  ;; (resto l r)

  ;; constrains whatever logic variables are present such that `r` is (rest
  ;; l). So

  (l/run* [q]
    (l/resto [1 2 3 4] q))

  ;; returns ((2 3 4)). The same reorderings and substitutions that we showed
  ;; for `conso` apply here too.

  ;; in summary: (resto l r) succeeds if (rest l) is `r`.

  ;; `membero`

  ;; we've already had a sneak peak at `membero`:

  ;; (membero x l)

  ;; constrains whatever logic variables are present such that `x` is an element
  ;; of `l`.

  (l/run* [q]
    (l/membero q [1 2 3]))

  ;; returns (1, 2, 3); that is, either 1 or 2 or 3.

  (l/run* [q]
    (l/membero 7 [1 3 8 q]))

  ;; returns (7), the only value that `q` can take such that 7 is an element
  ;; of (1 3 8 q).

  ;; in summary: (membero x l) succeeds if `x` is any of the members of l.


  ;; summary

  ;; logic programming is a set of logic variables and the set of constraints
  ;; upon them, which are input to a solution engine which returns the complete
  ;; set of consistent solutions to that set of constraints.

  ;; the key concepts are the logic variable and the goal. A logic variable is a
  ;; variable that can assume a number of distinct values one at a time. A goal
  ;; is a function that returns succeed or fail. Goals are composed into a logic
  ;; expression. `run*` invokes the logic engine over a logic expression and
  ;; returns the complete set of values of the query logic variable that allow
  ;; the logic expression to succeed.

)


;; https://blog.taylorwood.io/2018/05/10/clojure-logic.html

(comment

  ;; denomination sums (making change)

  ;; given a set of denominations e.g. $1, $5, $10, how many ways are there to
  ;; reach a given amount?

  ;; first we define (recursive) goal `productsumo` that will initially take a
  ;; sequence of "fresh" logic variables, a set of denominations, and a sum that
  ;; we want to reach. We first bind head and tail vars for both `vars` and
  ;; `dens`, then we use arithmetic functions from the finite domain namespace
  ;; of `core.logic` to constrain new fresh vars product and `run-sum` such that
  ;; multiplying a denomination by some number and adding it to a running sum
  ;; eventually reaches our desired `sum` (or not).

  (defn productsumo [vars dens sum]
    (l/fresh [vhead vtail dhead dtail product run-sum]
      (l/conde
       [(l/emptyo vars) (l/== sum 0)]
       [(l/conso vhead vtail vars)
        (l/conso dhead dtail dens)
        (fd/* vhead dhead product)
        (fd/+ product run-sum sum)
        (productsumo vtail dtail run-sum)])))

  ;; that's the only custom goal we need to solve the problem. Now we can wrap
  ;; this in a function:

  (defn change [amount denoms]
    (let [dens (sort > denoms)
          vars (repeatedly (count dens) l/lvar)]
      (l/run* [q]
        ;; we want a map from denominations to their quantities
        (l/== q (zipmap dens vars))
        ;; prune problem space...
        ;; every var/quantity must be 0 <= n <= amount
        (l/everyg #(fd/in % (fd/interval 0 amount)) vars)
        ;; the real work
        (productsumo vars dens amount))))

  ;; and now let's find all the ways we can make 14 out of denominations 1, 2,
  ;; 5, and 10:

  (change 14 #{1 2 5 10})

)
