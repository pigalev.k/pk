# hello-statistics

Exploring Clojure support for statistical computations.

- A library of statistical distribution sampling and transducing functions
  https://github.com/MastodonC/kixi.stats

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
