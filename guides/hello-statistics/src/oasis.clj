(ns oasis)

;;;; probabilistic simulation

;; there is an oasis with one big water well; people arrive at the well with
;; bottles, jerry cans, and other types of containers, to pump water. The
;; supply of water is large, but the pump capacity is limited. The pump is
;; about to be replaced, and while it is clear that a larger pump capacity
;; will result in shorter waiting times, more powerful pumps are also more
;; expensive. Therefore, to prepare a decision that balances costs and
;; beneﬁts, we wish to investigate the relationship between pump capacity and
;; system performance.


;;;; creating a stochastic model

;; some general characteristics are known, such as how many people arrive per
;; day and how much water they take on average, but the individual arrival
;; times and amounts are unpredictable. We introduce random variables to
;; describe them: T_i is interarrival time, and S_i is the service time of
;; customer i. (Arrivals at T_1, T_1 + T_2, ...; service times are S_1, S_2,
;; ...). The pump capacity v is not a random variable (it is a model parameter
;; or decision variable, whose "best" value we wish to determine).

;; If customer i requires R_i liters of water, his service time is S_i = R_i/v

;; to complete the model description, we need to specify the distribution of
;; the random variables T_i and R_i:

;; - every T_i has an Exp(0.5) distribution (minutes);
;; - every R_i has a U(2,5) distribution (liters).

;; the particular choice of distributions would have to be supported by
;; evidence that they are suited for the system at hand; there, for many
;; arrival type processes, the exponential distribution is reasonable for the
;; interarrival times, and the uniform distribution is arbitrarily assumed for
;; the water amount

;; to evaluate system's performance, we want to extract from the model the
;; waiting times of the customers and how busy it is at the pump


(defn rand-seq
  "Returns a sequence of uniformly distributed random numbers in the
  range [0,1)."
  []
  (repeatedly rand))

(defn water-amount-seq
  "Returns a sequence of values of a uniformly distributed continuous
  random variable in the range [2,5)."
  []
  (map #(+ 2 (* 3 %)) (rand-seq)))

(comment

  (def rs (take 5 (rand-seq)))
  (def wrs (take 5 (water-amount-seq)))

)
