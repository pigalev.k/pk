(ns hello-statistics.kixi
  (:require
   [kixi.stats.core :refer [mean median standard-deviation iqr summary
                            correlation correlation-matrix histogram
                            post-complete]]
   [kixi.stats.distribution :refer [draw sample sample-summary
                                    uniform binomial quantile bernoulli]]
   [kixi.stats.test :refer [simple-z-test]]
   [redux.core :refer [fuse]]))


;;;; transducing functions --- statistical reducing functions that can be used
;;;; with `transduce`

(comment

  ;; estimate the sample standard deviation of :x

  (->> [{:x 2} {:x 4} {:x 4} {:x 4} {:x 5} {:x 5} {:x 5} {:x 7} {:x 9}]
       (transduce (map :x) standard-deviation))

  ;; estimate linear correlation coefficient between :x and :y

  (->>  [{:x 1 :y 3} {:x 2 :y 2} {:x 3 :y 1}]
        (transduce identity (correlation :x :y)))

  ;; construct correlation matrix for values :x, :y and :z

  (->> [{:x 1 :y 3 :z 2} {:x 2 :y 2 :z 4} {:x 3 :y 1 :z 6}]
       (transduce identity (correlation-matrix {:x :x :y :y :z :z})))

  ;; calculate mean and standard deviation at the same time

  (->> [2 4 4 4 5 5 5 7 9]
       (transduce identity (fuse {:mean mean :sd standard-deviation})))

  ;; calculate the median only of numbers greater than 5

  (def gt5? (filter #(> % 5)))

  (transduce gt5? median (range 10))

  ;; count both all numbers and those greater than 5

  (transduce identity (fuse {:n count :gt5 (gt5? count)}) (range 10))

  ;; calculate the median, iqr and 5-number summary

  (->> (range 100)
       (transduce identity (fuse {:median median
                                  :iqr iqr
                                  :summary summary})))

  ;; calculate the 2.5 and 97.5 quantile from an empirical distribution

  (def distribution
    (->> (range 100)
         (transduce identity histogram)))

  {:lower (quantile distribution 0.025)
   :upper (quantile distribution 0.975)}

  ;; or

  (->> (range 100)
       (transduce identity (post-complete histogram
                                          (fn [hist]
                                            {:lower (quantile hist 0.025)
                                             :upper (quantile hist 0.975)}))))

)


;;;; distribution sampling

(comment

  (draw (binomial {:n 100 :p 0.5}))

  (sample 10 (binomial {:n 100 :p 0.5}))

  ;; for discrete distributions, samples can be summarized

  (sample-summary 1000 (bernoulli {:p 0.3}))

  ;; deterministic sampling with seed

  (draw (uniform {:a 0 :b 1}) {:seed 42})

)


;;;; statistical tests

(comment

  ;; a z-test between a known population mean & standard deviation and a
  ;; sampled mean with a given sample size

  ;; not sure what happens here; it does not behave like described in readme

  (simple-z-test {:mu 100 :sd 12} {:mean 96 :n 55})

)
