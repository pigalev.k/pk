(ns hello-clojure.decompile
  (:require
   [clj-java-decompiler.core :refer [decompile disassemble]]))


(comment

  (defn hello [] (println "Hello, decompiler!"))
  (decompile hello)
  (disassemble hello)

  (decompile (case "foo"
               "foo" 1
               "bar" 2
               3))

)
