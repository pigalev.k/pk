(ns hello-clojure.deftype.bookdeftype)

(defn bar [] "bar")
(defprotocol P (foo [p]))
(deftype Foo [] :load-ns true
         P (foo [this] (bar)))
