(ns hello-clojure.state
  (:import (java.util.concurrent Executors)))


;; shared state is implemented as reference types; a reference should contain
;; an immutable data structure

;; Language: Concurrency and Parallelism
;; https://clojure-doc.org/articles/language/concurrency_and_parallelism


;;;; vars

;; a var is a piece of global shared state that can be (optionally)
;; dynamically rebound on a per-thread basis (isolated from other threads)

(comment

  (def ^:dynamic *v* [1 2 3])

  (var *v*)
  #'*v*
  (meta (var *v*))
  (bean (var *v*))

  (reduce + *v*)
  (binding [*v* [4 5]]
    (reduce + *v*))

  ;; binding conveyance

  (binding [*v* [4 5]]
    @(future (reduce + *v*)))


  (def n 12)

  ;; the same
  n
  @#'n

  (alter-var-root #'n inc)

)


;;;; agents

;; an agent is a piece of shared state with independent, asynchronous changes

(comment

  (def agent-initial-state {:choices []})
  (def a (agent agent-initial-state))

  @a

  ;; an action is a function that takes the agent state (and maybe additional
  ;; args) and returns the next state; actions are dispatched by `send` and
  ;; the likes

  ;; actions are run asyncronously in the dedicated thread pool one by one;
  ;; actions dispatched from one source (thread) are executed in the dispatch
  ;; order (possibly interleaved by actions from other sources)

  ;; registered validators and watchers are called on the return value of an
  ;; action

  (set-validator! a (fn [new-state] (every? int? (:choices new-state))))
  (add-watch a :print (fn [key ref old-state new-state]
                        (println
                         (format
                          "%s: agent '%s' changed it's value from '%s' to '%s'"
                          key ref old-state new-state))))

  ;; actions dispatched during the action execution are delayed until the
  ;; state change

  (defn add-choice
    "Returns `state` map with the random integer from 0 to `n` (exclusive)
  added to the vector under the `:choices` key."
    [state n]
    (update state :choices conj (rand-int n)))

  ;; for CPU-bound actions (fixed-size thread pool)
  (send a add-choice 10)
  ;; if action is potentially blocking on IO (thread pool that grows on
  ;; demand)
  (send-off a add-choice 10)
  ;; selecting an executor to provide action threads
  (send-via (Executors/newSingleThreadExecutor) a add-choice 10)

  ;; await for the dispatched actions to complete (may block indefinitely)
  (await a)

  ;; exception thrown during action is cached, and the exception will be
  ;; rethrown on dispatches until the error is cleared (i.e., agent is
  ;; restarted)

  (defn blow-up
    "Throws an exception."
    [_]
    (throw (ex-info "Take a nap." {:duration :indeterminate})))

  (send a blow-up)
  ;; see the error
  (ex-message (agent-error a))
  (restart-agent a agent-initial-state)

)


;;;; atoms

;; an atom is a piece of shared state with independent, synchronous changes

(comment

  (def at (atom {}))
  @at
  (swap! at assoc :id 1)
  (reset! at 0)

)


;;;; refs

;; a ref is a piece of shared state with dependent (on other refs),
;; synchronous, transactional changes

(comment

  (def r1 (ref 0))
  (def r2 (ref 0))
  [@r1 @r2]

  ;; when order of transactions matter
  (dosync
   (alter r1 inc)
   (alter r2 dec)
   [@r1 @r2])

  ;; when order of transactions doesn't matter; doesn't create transaction
  ;; conflicts, never retried
  (dosync
   (commute r1 inc)
   (commute r2 dec)
   [@r1 @r2])

  (io! "I/O is not allowed here"
       (print :ok))
  (dosync
   (io! "I/O is not allowed here"
        (print :ok)))

)
