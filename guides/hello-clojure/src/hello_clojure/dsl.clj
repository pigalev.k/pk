(ns hello-clojure.dsl
  (:require [clojure.string :as s]))


;; https://clojure-doc.org/articles/cookbooks/growing_a_dsl_with_clojure/


(def ^:dynamic *lang* ::sh)


;; data abstraction: list form

(defn- op
  "Returns the operator name of the list `form`."
  [form]
  (name (first form)))

(defn args
  "Returns the arguments of the list `form`."
  [form]
  (rest form))


;; the DSL

(defmulti emit
  "Emits the `form` transformed to `*lang*`."
  (fn [form]
    [*lang* (cond
              (list? form) :list
              (symbol? form) :symbol
              (string? form) :string
              (number? form) :number
              :else (throw (ex-info "Unknown form type" {:form form})))]))

(defmethod emit [::sh :list]
  [form]
  (case (op form)
    "println" (s/join " " (cons "echo" (map emit (args form))))
    nil))

(defmethod emit [::sh :symbol]
  [form]
  (str "$" form))

(defmethod emit [::sh :string]
  [form] form)

(defmethod emit [::sh :number]
  [form] form)

(defmethod emit :default
  [form]
  (throw (ex-info "Unknown language" {:lang *lang*})))

(defmacro script
  "Returns `form` translated to the text in language `*lang*`."
  [form]
  `(emit '~form))

(defmacro with-lang
  "Evaluates `body` with `*lang*` bound to `lang`."
  [lang & body]
  `(binding [*lang* ~lang]
     (do ~@body)))

(comment

  (emit '(println a))
  (script (println a))
  (with-lang ::sh
    (script (println a b "c")))

  ;; errors

  (emit #"\d+")
  (script #"\d+")
  (binding [*lang* ::python]
    (emit '(println a)))

)
