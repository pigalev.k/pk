(ns hello-clojure.laziness
  (:require
   [clojure.java.io :as io]))


(comment

  ;; laziness allows

  ;; defer computations; core sequence functions usually produce lazy sequences

  (def result (map (fn [i] (println ".") (inc i)) '[0 1 2 3]))
  result

  ;; create and use infinite sequences

  (def fib-seq
    (lazy-cat [1 1] (map + (rest fib-seq) fib-seq)))

  (take 10 fib-seq)

)


;; write as if you have infinite memory

(defn prefix-lines-in-file
  "Returns a lazy seq of lines from `file` with `prefix` added."
  [file prefix]
  (->> (line-seq (io/reader file))
       (map #(str prefix %))))

(comment

  (take 5 (prefix-lines-in-file "/etc/hosts" ">>> "))

)


;; produce and use infinite sequences

(comment

  (->> (iterate (fn [[a b]] [b (+ a b)]) [0 1])
       (map first)
       (drop 30)
       (take 10))

)


;; but beware that actual computation may happen way after the creation of a
;; lazy seq, do not hold onto the head and avoid side-effects
