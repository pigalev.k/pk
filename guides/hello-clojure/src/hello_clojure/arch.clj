(ns hello-clojure.arch
  "Experiments with application architectures.
  See https://yogthos.net/posts/2022-12-18-StructuringClojureApplications.html")


;; protocols for system components

(defprotocol Notify
  (send-invite [this email])
  (notify-user [this email message]))

(defprotocol DataStore
  (persist [this state])
  (query [this email])
  (add-funds [this email amount])
  (load-state [this workflow-id])
  (send-transfer [this from to amount]))


;; event (action) handler

(defmulti handle-action (fn [resources {:keys [action]}] action))

(defmethod handle-action :transfer [{:keys [store]} {:keys [from to amount] :as state}]
  (let [from-info (query store (:email from))
        to-info   (query store (:email to))
        available-funds (:funds from-info)
        state     (-> state
                      (update :from merge from-info)
                      (update :to merge to-info))]
    (cond
      (nil? to-info)
      (assoc state :action :invite)
      (>= available-funds amount)
      (do
        (send-transfer store (:email from) (:email to) amount)
        (assoc state :action :done))
      (< available-funds amount)
      (assoc state :action :notify-missing-funds))))

(defmethod handle-action :notify-missing-funds [{:keys [store notify]} {:keys [from] :as state}]
  (notify-user notify (:email from) "insufficient funds")
  (persist store (assoc state :action :transfer))
  {:action :await})

(defmethod handle-action :invite [{:keys [store notify]} {:keys [to] :as state}]
  (send-invite notify to)
  (persist store (assoc state :action :transfer))
  {:action :await})


;; data types (may be real or mocks) for the system components

(defrecord MockNotify []
  Notify
  (send-invite [_ email]
    (println "sending invite to" email))
  (notify-user [_ email message]
    (println "notifying" email message)))

(defrecord AtomDataStore [store]
  DataStore
  (persist [_ {:keys [id] :as state}]
    (swap! store assoc-in [:workflows id] state))
  (query [_  email]
    (get-in @store [:users email]))
  (add-funds [_ email amount]
    (println "adding funds to" email ":" amount)
    (swap! store
           update-in [:users "bob@foo.bar" :funds] (partial + amount)))
  (load-state [_ workflow-id]
    (println "hi")
    (get-in @store [:workflows workflow-id]))
  (send-transfer [_ from to amount]
    (println "transferring from" from "to" to ":" amount)
    (swap! store
           #(-> %
                (update-in [:users from :funds] - amount)
                (update-in [:users to :funds] + amount)))))


;; event dispatcher (start an action/workflow)

(defn run-workflow
  [{:keys [store] :as resources} workflow-id]
  (loop [state (load-state store workflow-id)]
    (condp = (-> state :action)
      :done state
      :await :workflow-suspended
      (let [state (handle-action resources state)]
        (recur state)))))


;; combine the components into a system, try to use it

(comment

  ;; instantiate a system from several components (data store, email
  ;; notification service)

  (def system
    {:store (->AtomDataStore
             (atom {:workflows {"33a19b1f-c7d1-45d8-9864-0ea17e01a26d"
                                {:id "33a19b1f-c7d1-45d8-9864-0ea17e01a26d"
                                 :from   {:email "bob@foo.bar"}
                                 :to     {:email "alice@bar.baz"}
                                 :amount 200
                                 :action :transfer}}
                    :users {"bob@foo.bar" {:funds 100}
                            "alice@bar.baz" {:funds 10}}}))
     :notify (->MockNotify)})


  ;; dispatch events (here, run workflows)

  (run-workflow system "33a19b1f-c7d1-45d8-9864-0ea17e01a26d")
  ;; insufficient funds
  (add-funds (:store system) "bob@foo.bar" 100)
  (run-workflow system "33a19b1f-c7d1-45d8-9864-0ea17e01a26d")
  ;; ok

   @(:store (:store system))
)
