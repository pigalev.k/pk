(ns hello-clojure.sci.basics
  (:require [sci.core :as sci]))


;; basics

;; https://github.com/babashka/SCI

(def a 12)

(defn square [x] (* x x))

(comment

  ;; SCI can be used if you want to evaluate code from user input, or use
  ;; Clojure for a DSL inside your project, but `eval` isn't safe or simply
  ;; doesn't work


  ;;;; main API function

  (sci/eval-string "(inc 1)")
  (sci/eval-string "(inc x)" {:namespaces {'user {'x 2}}})

  ;; most functions of `clojure.core` are accessible by default; more
  ;; functions can be enabled by using `:namespaces` and `:classes`

  (sci/eval-string "(println \"hello\")"
                   {:namespaces {'clojure.core {'println println}}})

  ;; additional namespaces and classes can be provided to be required in a SCI
  ;; program

  (def opts {:namespaces {'foo.bar {'println println}}
             :classes    {'java.util.UUID java.util.UUID}})
  (sci/eval-string "(require '[foo.bar :as lib])
                    (lib/println \"hello\")
                    (java.util.UUID/randomUUID)" opts)

  ;; a list of allowed symbols can be provided

  (sci/eval-string "(inc 1)" {:allow '[inc]})
  (sci/eval-string "(dec 1)" {:allow '[inc]})

  ;; providing a list of disallowed symbols has the opposite effect

  (sci/eval-string "(inc 1)" {:deny '[inc]})


  ;;;; macros

  ;; providing a macro as a binding can be done by providing a normal function
  ;; with some metadata and two additional arguments

  (def do-twice ^:sci/macro (fn [_&form _&env x] (list 'do x x)))
  (sci/eval-string "(do-twice (f))"
                   {:bindings {'do-twice do-twice 'f #(println "hello")}})

  ;; or you can refer to the macro from the Clojure environment via the
  ;; var (this only works on JVM)

  (defmacro do-twice [x] (list 'do x x))
  (sci/eval-string "(do-twice (f))"
                   {:namespaces {'user
                                 {'do-twice #'do-twice 'f #(println "hello")}}})


  ;;;; vars

  ;; SCI programs do not have access to Clojure vars, unless you explicitly
  ;; provide that access; SCI has its own var type, whose instances are
  ;; created with `def` and `defn` just like in normal Clojure

  (sci/eval-string "(def x 1) (defn foo [x] x) (println (foo x))"
                   {:namespaces {'clojure.core {'println println}}})

  ;; SCI vars also can be created from Clojure

  (def x (sci/new-var 'x 10))
  (sci/eval-string "(inc x)" {:namespaces {'user {'x x}}})

  ;; dynamic vars (with thread-local bindings) also can be created from SCI or
  ;; Clojure

  (sci/eval-string "(def ^:dynamic *x* 1) (binding [*x* 10] (println *x*))"
                   {:namespaces {'clojure.core {'println println}}})

  (def x1 (sci/new-var 'x 10 {:dynamic true}))
  (sci/eval-string "(binding [*x* 12] (inc *x*))"
                   {:namespaces {'user {'*x* x1}}})

  (def x2 (sci/new-dynamic-var 'x 10))
  (sci/eval-string "(binding [*x* 12] (inc *x*))"
                   {:namespaces {'user {'*x* x2}}})

  ;; `binding` in SCI will not work on Clojure's dynamic vars, but they can be
  ;; bound in Clojure functions called from SCI

  (def ^:dynamic *x* 1)
  (defn with-x [x-val f] (binding [*x* x-val] (f)))
  (defn get-x [] *x*)
  (def userns (sci/create-ns 'user))
  (sci/eval-string "(with-x 42 #(get-x))"
                   {:namespaces {'user {'with-x (sci/copy-var with-x userns)
                                        'get-x  (sci/copy-var get-x userns)}}})

  (def ^:dynamic *x* 1)
  (def userns (sci/create-ns 'user))
  ;; ^:dynamic is copied too
  (def sci-x (sci/copy-var *x* userns))
  ;; bind host var to SCI dynamic var value
  (defn get-x [] (binding [*x* @sci-x] *x*))
  (sci/eval-string "(binding [*x* 42] (get-x))"
                   {:namespaces {'user {'get-x (sci/copy-var get-x userns)
                                        ;; expose SCI dyn var
                                        '*x*   sci-x}}})


  ;;;; standard streams

  ;; dynamic vars `*in*`, `*out*`, `*err` in Clojure, `sci/in`, `sci/out`,
  ;; `sci/err` in SCI

  (def sw (java.io.StringWriter.))
  (sci/binding [sci/out sw] (sci/eval-string "(println \"hello\")"))
  (str sw)

  (sci/with-out-str (sci/eval-string "(println \"hello\")"))

  ;; enabling reading/printing with stdin/stdout from SCI

  (sci/binding [sci/out *out*
                sci/in *in*]
    (sci/eval-string "(print \"Type your name!\n> \")")
    (sci/eval-string "(flush)")
    (let [name (sci/eval-string "(read-line)")]
      (sci/eval-string "(printf \"Hello %s!\" name) (flush)"
                       {:bindings {'name name}})))

  ;; when adding a Clojure function to SCI that interacts with `*out*` (or
  ;; `*in*` or `*err*`), you can hook it up to SCI's context

  (defn foo [] (println "yello!"))
  ;; without binding `*out*` to sci's `out`, the Clojure function will use its
  ;; default `*out*`
  (sci/eval-string "(with-out-str (foo))" {:namespaces {'user {'foo foo}}})
  ;; let's hook foo up to SCI's context
  (defn wrapped-foo [] (binding [*out* @sci/out] (foo)))
  (sci/eval-string "(with-out-str (foo))" {:bindings {'foo wrapped-foo}})

  ;; to always enable printing in your SCI environment you can set `sci/out` and
  ;; `sci/err` to `*out*` and `*err*` respectively, globally

  (sci/alter-var-root sci/out (constantly *out*))
  (sci/alter-var-root sci/err (constantly *err*))


  ;;;; namespaces

  ;; create ns and copy some vars to it

  (def fns (sci/create-ns 'foo nil))

  (def foobar-ns {'A      (sci/copy-var a fns {:name 'A})
                  'square (sci/copy-var square fns)})

  (def ctx (sci/init {:namespaces {'user foobar-ns}}))

  (sci/binding [sci/out *out*]
    (sci/eval-string* ctx "(prn (square A))"))

  ;; copy an entire namespace

  (let [ens    (sci/create-ns 'edamame.core)
        sci-ns (sci/copy-ns edamame.core ens {:exclude [iobj?]})
        ctx    (sci/init {:namespaces {'edamame.core sci-ns}})]
    (prn (sci/eval-string*
          ctx
          "(require '[edamame.core :as e]) (e/parse-string \"1\")")))

)
