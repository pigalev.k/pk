(ns hello-clojure.core
  (:require
   [clojure.core.async :refer [>!! <!! >! <! alts!! chan go]]
   [clojure.core.reducers :as r]
   [clojure.core.server :as server]
   [clojure.data :refer [diff]]
   [clojure.edn :as edn]
   [clojure.inspector :as i]
   [clojure.java.browse :refer [browse-url *open-url-script*]]
   [clojure.java.io :as io]
   [clojure.java.javadoc :as browse]
   [clojure.java.shell :refer [sh] :as shell]
   [clojure.main :as main]
   [clojure.pprint :refer [cl-format pprint pp] :as pretty]
   [clojure.reflect :as reflect]
   [clojure.repl :refer [dir-fn dir source] :as repl]
   [clojure.set :refer [map-invert] :as st]
   [clojure.string :as s]
   [clojure.test :refer :all]
   [clojure.walk :as w]
   [clojure.xml :as xml]
   [clojure.zip :as zip]
   [criterium.core :refer [bench quick-bench quick-benchmark
                           with-progress-reporting]]
   [clojure.template :refer [apply-template do-template]]
   #_[keirin.core :as k])
  (:import
   (clojure.core.protocols IKVReduce)
   (clojure.lang IDeref PersistentQueue)
   ;; (java.awt Point)
   (java.io BufferedWriter ByteArrayInputStream ByteArrayOutputStream File
            FileReader FileWriter InputStream StringWriter)
   (java.net ConnectException InetAddress ServerSocket Socket SocketException
             URL)
   (java.nio.file FileSystems Path)
   (java.sql ResultSet ResultSetMetaData)
   (java.time Duration Instant)
   (java.util ArrayList Calendar Calendar$Builder Collections Date HashMap
              LinkedHashMap Scanner)
   (java.util.concurrent ConcurrentHashMap ConcurrentLinkedQueue
                         LinkedBlockingQueue Executors)
   (java.util.regex Pattern)
   (java.util.zip ZipInputStream)
   (javax.xml.parsers SAXParserFactory)))


;; From: Clojure: The Essential Reference, MEAP v28 (2021)


;; creating and manipulating functions

(comment

  ;; `defn`, `defn-`, `fn`, `fn*`

  (set! *warn-on-reflection* true)

  (def s (macroexpand
          '(defn hello [person]
             (str "hello " person))))

  (defn ^{:t1 1} foo
    "docstring"
    {:t2 2} (^Integer [^Integer a b] (+ a b))
    {:t3 3})

  (foo 1 2)
  (meta #'foo)


  (definline to-string [^Object o]
    (.toString o))

  (def f (fn af [x] (inc x)))

  (f 1)


  ;; `fnil`

  ((fnil inc 0) nil)
  ((fnil / 0 2) 23 nil)


  ;; `comp`

  (def ^{:arglists '([x y])
         :doc      "Sum and inc."}
    bar
    (comp inc +))

  (bar 2 4)

  ((reduce comp [println inc +]) 2 3)


  ;; `complement`

  ((complement nil?) nil)


  ;; `constantly`

  ((constantly 1) 2 3 4)

  (def a 1)
  (with-redefs [a   2
                foo (constantly 3)]
    (+ (foo 3 4) 2))


  ;; `identity`

  (filter identity [0 1 2 false 3 4 nil 5])

  (def cashiers (ref [1 2 3 4 5]))

  (defn next-available []
    (some identity @cashiers))

  (defn make-available! [n]
    (alter cashiers assoc (dec n) n) n)

  (defn make-unavailable! [n]
    (alter cashiers assoc (dec n) false) n)

  (defn book-lane []
    (dosync
     (if-let [lane (next-available)]
       (make-unavailable! lane)
       (throw (Exception. "All cashiers busy!")))))

  (book-lane)
  (book-lane)
  (next-available)
  (dosync (make-available! 2))
  @cashiers

  (partition-by identity "hello")


  ;; `juxt`

  ((juxt inc dec) 1)

  ((juxt first second last) (range 10))

  (def w ["wd5" "aba" "yp" "csu" "nwd7"])
  (sort-by (juxt count str) w)
  (group-by (juxt count type) w)
  (compare [1 2] [1 1])


  ;; `memfn`

  (map (memfn toUpperCase) ["keep" "calm" "and" "drink" "tea"])

  (def instants
    (repeatedly (fn []
                  (Thread/sleep (long (rand-int 100)))
                  (Instant/now))))

  (doall (take 2 instants))
  (do 1 2)

  (->> instants
       (take 2)
       (apply #(Duration/between %1 %2))
       ((memfn ^Duration toMillis)))

  (repeatedly 3 #(rand-int 100))


  ;; `partial`

  ((partial + 1) 2 3)


  ;; `every-pred` and `some-fn`

  ((every-pred string? empty?) "")
  ((every-pred number? zero?) 1)
  ((some-fn string? number?) 1)
  ((some-fn string? boolean?) 1)

  (let [add3 (partial + 3)]
    (-> 1
        inc
        add3
        ((partial +) 2)))

  (letfn [(add3 [x] (+ x 3))]
    (add3 1))


  ;; `cond->` and `cond->>`

  (let [a "ololo"]
    (cond-> a
      (number? a) inc
      (string? a) second))


  ;; `some->` and `some->>`

  (some-> {:a 1 :c 2}
          :c
          inc)

  (some-> {:a 1 :b 2}
          :c
          inc)

  (or (some-> (System/getenv "PORT")
              Integer.)
      4444)

  (or (some-> (System/getenv "USER")
              first)
      \a)


  ;; `as->`

  (as-> 1 i
    (+ 1 i)
    (* 4 i))

  (as-> {:a 1 :b 2 :c 3} x
    (assoc x :d 4)
    (vals x)
    (filter even? x)
    (apply + x))


  ;; `apply`

  (apply + [1 2 3])

  (defn rand-b [n]
    (->> #(rand-int 2)
         (repeatedly n)
         (apply str)))
  (rand-b 10)

  (defn event-stream []
    (interleave (repeatedly (fn [] (System/nanoTime))) (range)))
  (apply hash-map (take 10 (event-stream)))


  ;; `memoize`

  (defn- f* [a b]
    (println (format "Cache miss for [%s %s]" a b))
    (+ a b))

  (def f (memoize f*))

  (f 1 2)
  (f 1 3)

  (lazy-seq [1 2 3])


  ;; `trampoline`

  (defn- invoke
    [f-key & args]
    (apply (resolve (symbol (name f-key))) args))

  (defn green [[light & lights]]
    #(case light
       :red false
       nil  true
       (invoke light lights)))

  (defn red [[light & lights]]
    #(case light
       :amber false
       nil    true
       (invoke light lights)))

  (defn amber [[light & lights]]
    #(case light
       :green false
       nil    true
       (invoke light lights)))

  (defn flashing-red [[light & lights]]
    #(if (nil? light) true (invoke light lights)))

  (defn flashing-amber [[light & lights]]
    #(if (nil? light) true (invoke light lights)))

  (defn traffic-light [lights]
    (trampoline flashing-amber lights))

  (traffic-light [:red :amber :red])
  (traffic-light [:red :green :amber :red])

)


;; basic constructs

(comment

  ;; `let` and `let*`

  (let [a 1 b 2]
    [a b])

  (defn rule [moves]
    (let [[p1 p2] moves]
      (cond
        (= p1 p2)                            "tie game"
        (every? #{"rock" "paper"} moves)     "paper wins over rock"
        (every? #{"scissors" "rock"} moves)  "rock wins over scissors"
        (every? #{"paper" "scissors"} moves) "scissor wins over paper"
        :else                                "computer can't win that!")))

  (defn game-loop []
    (println "Rock, paper or scissors?")
    (let [human (read-line)
          ai    (rand-nth ["rock" "paper" "scissors"])
          res   (rule [human ai])]
      (if (= "exit" human)
        "Game over"
        (do
          (println (format "Computer played %s: %s" ai res))
          (recur)))))

  (game-loop)


  ;; `if-let`, `when-let`, `if-some` and `when-some`

  (if-let [n false]
    :ok
    :not-ok)
  (when-let [n false]
    :ok)
  (if-some [n false]
    :ok
    :not-ok)
  (when-some [n nil]
    :ok)

  (defn loc [resource]
    (when-let [file (clojure.java.io/resource resource)]
      (with-open [reader (clojure.java.io/reader file)]
        (count (line-seq reader)))))

  (loc "clojure/core.clj")
  (loc "clojure/core.cljs")


  (defn- master [items in]
    (go
      (doseq [item items]
        (>! in item))
      (close! in)))

  (defn- worker [out]
    (let [in (chan)]
      (go-loop []
        (if-some [item (<! in)]
          (do
            (>! out (str "*" item "*"))
            (recur))
          (close! out)))
      in))

  (defn process [items]
    (let [out (chan)]
      (master items (worker out))
      (loop [res []]
        (if-some [item (<!! out)]
          (recur (conj res item))
          res))))

  (process [1 2 3])


  ;; `not`, `and`, `or`, `bit-and` and `bit-or`


  (Long/toBinaryString (bit-and 2r11001001 2r11000110))
  (Long/toBinaryString (bit-or 2r11001001 2r11000110))
  (Long/toBinaryString (bit-xor 2r11001001 2r11000110))
  (Long/toBinaryString (bit-xor 2r11001001 2r11001001))
  (Long/toBinaryString (bit-clear 2r11001001 0))
  (Long/toBinaryString (bit-set 2r11001001 2))
  (Long/toBinaryString (bit-flip 2r11001001 0))
  (bit-test 2r11001001 0)
  (cl-format nil "~2,8,'0r" 2r11001001)


  ;; `if`, `if-not`, `when` and `when-not`

  (loop [i 0]
    (when-not (>= i 10)
      (println i)
      (recur (inc i))))

  (defn fib [a b cnt]
    (if (zero? cnt)
      b
      (recur (+ a b) a (dec cnt))))

  (fib 1 0 9)
  (map
   (partial fib 1 0)
   (range 10))


  ;; `cond`, `condp`, `case`

  (condp < 1
    1   :ok
    2   :not-ok
    0.5 :>> identity
    :default)

  (defn fizz-buzz [n]
    (condp #(zero? (mod %2 %1)) n
      15 "fizzbuzz"
      3  "fizz"
      5  "buzz"
      n))

  (map fizz-buzz (range 1 20))

  (let [n 1]
    (case n
      (0 1 2) "O"
      3       "l"
      4       "A"))


  ;; `loop`, `loop*` and `recur`

  (let [i 0]
    (#(if (> % 10)
        ::done
        (do
          (println %)
          (recur (inc %)))) i))

  (defn recursive-root [x]
    (loop [guess 1.]
      (if (> (Math/abs (- (* guess guess) x)) 1e-8)
        (recur (/ (+ (/ x guess) guess) 2.))
        guess)))

  (with-progress-reporting
    (bench (recursive-root 2.0)))
  (quick-bench (recursive-root 2.0))


  (defn repl
    "A simple REPL. Enter ::quit to exit the REPL."
    []
    (letfn [(print+ [x] (println x) (flush) x)]
      (loop [result nil]
          (when-not (= result :quit)
            (let [result
                  (try
                    (-> *in*
                        read
                        eval
                        print+)
                    (catch Exception e
                      (print+ (str "error: " (ex-message e)))))]
              (recur result))))))

  (repl)


  ;; `range`

  (range 3 5 0.5)
  (range (dec Long/MAX_VALUE) (+' Long/MAX_VALUE 3))
  (range 1 0 -1/10)

  (map range (range 10))

  (->> (reverse (range 10))
       (map range (range 10))
       (remove empty?))

  (defn palindrome? [xs]
    (let [cnt     (count xs)
          indices (range (quot cnt 2) -1 -1)]
      (every? #(= (nth xs %) (nth xs (- cnt % 1))) indices)))

  (palindrome? [1 2 3 4])
  (palindrome? [1 2 3 4 3 2 1])

  (defn string-palindrome? [s]
    (let [chars (some->> s
                         s/lower-case
                         (remove (comp s/blank? str)))]
      (palindrome? chars)))

  (string-palindrome? "Was it a car or a cat I saw")
  (string-palindrome? "Аргентина манит негра")
  (string-palindrome? "Quick brown fox")

  (transduce (comp
              (map inc)
              (map double)) + [1 2 3])


  ;; `for`

  (for [x      [1 2 3]
        y      [\a \b \c]
        z      [:x :y :z]
        :let   [nz (name z)
                s (str x y nz)]
        :while (not= x 3)
        :when  (not= z :z)]
    s)


  ;; `while`

  (while (> 0.5 (rand))
    (println "loop"))

  (defn forever []
    (while true
      (Thread/sleep 5000)
      (println "App running. Waiting for input...")))

  (defn status-thread []
    (let [t (Thread. forever)]
      (.start t)
      t))

  (def t (status-thread))
  (.stop t)


  ;; `dotimes`

  (dorun (map #(println "Count is:" %) (range 1 4)))
  (doall (map inc (range 1 4)))
  (doseq [i     (range 4)
          :when (odd? i)]
    (println i))
  (dotimes [n 5]
    (println n)
    (println n))
  (run! println (range 1 4))

  (time (dotimes [_ 1000000]
          (apply max (range 100))))

  (let [res (transient [])]
    (dotimes [i 10]
      (assoc! res i i))
    (persistent! res))


  (let [xform (comp (map inc) (filter even?))
        achan (clojure.core.async/chan 1 xform)]
    ;; achan will increment and filter everything put on it
    ;; (do-something-with achan)
    (go (>! achan 1))
    (println (<!! achan))
    ;; use the exact same transducer pipeline for sequence reduction
    (reduce (xform conj) [] (range 10)))



  ;; `first`, `second` and `last`

  (drop 2 v)
  (rand-nth v)
  (nthrest v 2)
  (peek v)
  (pop v)
  (subvec v 3)
  (first "abcd")

  (map-indexed + v)

  (sequential? v)

  (random-sample 0.5 v)
  (mapv inc v)
  (pmap inc v)


  (repeat 3 1)
  (take 5 (cycle (range 3)))

  (defn walk-all
  "Returns a lazy-seq of all first elements in colls, then all second
  elements and so on."
    [colls]
    (lazy-seq
     (let [ss (map seq colls)]
       (when (every? identity ss)
         (cons (map first ss) (walk-all (map next ss)))))))

  (def colls [[1 2 3 4] [2 3 nil] [4 5 6]])
  (walk-all colls)
  (walk-all [v v v])
  (cons 1 nil)

  (keep (fnil inc 0) (second colls))


  (reduce + v)
  (reductions + v)
  (frequencies (take 12 (cycle (range 5))))

  ;; OOM? Not so, but 12GB is consumed; (System/gc) helps
  (let [xs (range 1e8)] (last xs) (reduce + xs))
  (take 10 (reduce merge '() (range 1e8)))

)


;; creating and inspecting macros

(comment

  ;; `defmacro` (`quote`, `gensym`, `defilnine`, `destructure`,
  ;; `clojure.template/apply-template`, `clojure.template/do-template`)

  (defmacro simple [a] (str a))
  (simple 3)

  (gensym)
  (gensym "lol")
  `(2 3 ~(+ 1 3))
  `(2 3 ~@(cons (+ 1 3) '(5)))
  `(let [x 1] x)
  `(let [x# 1] x#)
  `~'foo

  (defmacro just-print-me [& args]
    (println &form))
  (just-print-me foo :bar 123)

  (defmacro with-print-locals [& body]
    (let [locals (vec (keys &env))]
      `(do (println ~locals)
           ~@body)))
  (let [a 1, b 2]
    (with-print-locals (+ 2 3)))

  (destructure '[[x] [1 2]])
  (destructure '[[x y] [1 2]])
  (destructure '[{:keys [id name] :as m} {:id 1 :name "Ololosha"}])

  (apply-template '[x y] '(+ x y x) [1 2])
  (apply-template '[x] '(let [x x] x) [1])
  (apply-template '[x] '(P(x) ∧ (∃ x Q(x))) '[y])


  ;; `macroexpand`

  (macroexpand '(do-template [x y] (+ y x) 2 4 3 5))

)


;; operations on numbers

(comment

  ;; `+`, `-`, `*`, `/`, `inc`, `dec`, `quot`, `rem`, `mod`, `+'`, `-'`, `*'`,
  ;; `inc'`, `dec'`, `unchecked-add` and other unchecked operators,
  ;; `unchecked-add-int` and other unchecked-int operators

  (/ 12)
  (/ 10 3)

  (inc 1.1)
  (inc Long/MAX_VALUE)
  (inc' Long/MAX_VALUE)

  (unchecked-add Long/MAX_VALUE 1)
  (unchecked-add-int Integer/MAX_VALUE 1)
  (unchecked-negate Long/MAX_VALUE)
  (type (unchecked-add-int 1 2))


  (quot 14 12)
  (rem 14 12)
  (mod 14 12)
  (unchecked-remainder-int 14 12)

  (quot 2 12)
  (mod 2 12)

  (def alpha
    ["a" "b" "c" "d"
     "e" "f" "g" "h"
     "i" "j" "k" "l"
     "m" "n" "o" "p"
     "q" "r" "s" "t"
     "u" "v" "w" "x"
     "y" "z"])

  (defn ++ [c]
    (-> (.indexOf alpha c)
        inc
        (mod (count alpha))
        alpha))

  (++ "a")
  (++ "z")


  ;; `max`, `min`, `max-key`, `min-key`

  (max 1 2 3 ##-Inf)
  (max (/ -1.0 0) (/ 1.0 0))
  (max (/ 0.0 0) 1)

  (def vo [{:id 1} {:id 2} {:id 44}])
  (apply max-key :id vo)
  (apply min-key :id vo)


  ;; `rand` and `rand-int`

  (rand)
  (rand 10.0)
  (rand-int 2)
  (rand-int -10)

  (shuffle vo)
  (random-sample 0.1 vo)


  ;; `with-precision`

  (bigint 1)
  (bigdec 2.9)
  (/ 22. 7)
  (/ 22M 7)
  (with-precision 4 (/ 22M 7))
  (= 123.4M 123.4M)
  (== 0.1 (float 0.1))

  (hash {:id 1})
  (.hashCode {:id 1})
  (hash "hello")
  (= '(1 2 3) [1 2 3])
  (= (hash '(1 2 3))
     (hash [1 2 3]))

)


;; comparison and equality

(comment

  ;; `=` (equal) and `not=` (not equal)

  (defn generate [& [{:keys [cheat reels]
                      :or {cheat 0 reels 3}}]]
    (->> (repeatedly rand)
         (map #(int (* % 100)))
         (filter pos?)
         (map #(mod (- 100 cheat) %))
         (take reels)))

  (defn play [& [opts]]
    (let [res (generate opts)]
      {:win (apply = res)
       :result res}))

  (play)
  (play {:cheat 100})
  (play {:cheat 10})
  (play {:reels 10})

  (distinct [1 2 3 4 1 0 -1 1 3])


  ;; `==` (double equal)

  (== (double 0.1234567) 0.1234567M)
  (== 1)
  (= 1)
  (identical? 127 127)
  (identical? 128 128)
  (class 127)
  (class nil)
  (= nil nil)


  ;; `<` , `>` , `<=` , `>=`

  (< 0 (byte 1) 2 2.1 3N 4M 21/2)

  (sort > (range 10))

  (drop-while
   (partial > 90)
   (shuffle (range 100)))


  ;; `compare`

  (compare 1 1)
  (compare 0 1)
  (compare 1 0)
  (compare nil ##-Inf)
  (compare nil nil)
  (compare 1/2 1/3)
  (compare "a" "z")
  (compare "abc" "abcde")
  (compare :user/a :user1/a)
  ((comparator <) 1 2)


  ;; `identical?`

  (identical? 'user/a 'user/a)
  (identical? :user/a :user/a)


  ;; `hash`

  (hash-ordered-coll [1])
  (hash-unordered-coll #{1})
  (hash-unordered-coll {:id 1})
  (mix-collection-hash 1 1)


  ;; `clojure.data/diff`

  (diff 1 2)
  (diff [1 2 3 5] [1 2 4 7])
  (diff {:id 1 :name "Ololosha"} {:id 2 :name "Ololosha"})
  (diff "abc" "bcd")
)


;; reducers and transducers

(comment

  ;; `fold`

  (def map-inc (r/map inc))
  (def filter-odd (r/filter odd?))
  (def compose-all (comp map-inc filter-odd))
  (def apply-to-input (compose-all (range 10)))
  (reduce + apply-to-input)

  (type apply-to-input)

  (reduce conj (r/map inc (range 10)))

  (r/fold 1e6 + + (into [] (range 1e7)))

  (defn pi [n]
    "Pi Leibniz formula approx."
    (->> (range)
         (filter odd?)
         (take n)
         (map / (cycle [1 -1]))
         (reduce +)
         (* 4.0)))

  (defn large-map [i j]
    (into {}
          (map vector (range i) (repeat j))))

  (defn combinef [init]
    (fn
      ([] init)
      ([m _] m)))

  (defn reducef [^java.util.Map m k]
    (doto m
      (.put k (pi (.get m k)))))

  (def a-large-map (ConcurrentHashMap. (large-map 100000 100)))

  (extend-protocol
      clojure.core.protocols/IKVReduce
    java.util.concurrent.ConcurrentHashMap
    (kv-reduce [m f _]
      (reduce (fn [amap [k v]] (f amap k)) m m)))

  (dorun
   (r/fold
    (combinef a-large-map)
    reducef
    a-large-map))

  (time
   (dorun
    (r/fold
     (combinef a-large-map)
     reducef
     a-large-map)))

  (.get a-large-map 8190)

  (defn foldmap [m n combinef reducef]
    (#'r/foldvec
     (into [] (keys m))
     n
     combinef
     reducef))

  (extend-protocol r/CollFold
    java.util.concurrent.ConcurrentHashMap
    (coll-fold
      [m n combinef reducef]
      (foldmap m n combinef reducef)))

  (def a-large-map (ConcurrentHashMap. (large-map 100000 100)))

  (time
   (dorun
    (r/fold
     (combinef a-large-map)
     reducef
     a-large-map)))

  (def a-large-map (large-map 100000 100))


  ;; `monoid`, `reducer` and `folder`

  (time
   (dorun
    (r/fold
     (r/monoid merge (constantly {}))
     (fn [m k v] (assoc m k (pi v)))
     a-large-map)))


  (defn divisible-by-10 [current-reducing-fn]
    (fn [acc el]
      (if (zero? (mod el 10))
        (current-reducing-fn acc el)
        acc)))

  (into []
        (r/reducer
         (range 100)
         divisible-by-10))

  (r/fold
   (r/monoid merge (constantly {}))
   (fn [m k v] (assoc m k (+ 3 v)))
   (r/folder
    (zipmap (range 100) (range 100))
    (fn [rf] (fn [m k v] (if (zero? (mod k 10)) (rf m k v) m)))))


  (defn reducer-dedupe [coll]
    (r/reducer coll
               (fn [rf]
                 (let [prev (volatile! ::none)]
                   (fn [acc el]
                     (let [v @prev]
                       (vreset! prev el)
                       (if (= v el)
                         acc
                         (rf acc el))))))))

  (->> (range 10)
       (r/map range)
       (r/mapcat conj)
       (r/filter odd?)
       reducer-dedupe
       (into []))


  (r/fold
   (r/monoid str (constantly "Concatenate "))
   ["th" "is " "str" "ing"])

  (r/fold (r/monoid + (constantly 0)) (range 10))
  (r/fold (r/monoid * (constantly 1)) (r/map inc (range 10)))
  (r/fold (r/monoid merge (constantly {}))
          (fn [m k v] (assoc m k (str v)))
          (zipmap (range 10) (range 10)))


  (transduce (comp (filter odd?) (map inc)) + (range 10))
  (transduce (map inc) + [1 2 3])
  (r/fold + (sequence (map inc) [1 2 3]))
  (r/fold + (eduction (map inc) [1 2 3]))
  (transduce (map inc) conj [] [1 2 3])
  (into [] (eduction (filter odd?) (map inc) [1 2 3]))
  (transduce (map inc) (completing -) 0 (range 10))


  (def values (LinkedBlockingQueue. 1))

  (defn value-seq []
    (lazy-seq
     (cons (.take values) (value-seq))))

  (defn moving-average [[cnt sum avg] x]
    (let [new-cnt (inc cnt)
          new-sum (+ sum (unreduced x))
          new-avg (/ new-sum (double new-cnt))
          res [new-cnt new-sum new-avg]]
      (println res)
      (if (reduced? x)
        (reduced res)
        res)))

  (defn start []
    (let [out *out*]
      (.start (Thread.
               #(binding [*out* out]
                  (println "Done:"
                           (reduce
                            moving-average
                            [0 0 0]
                            (value-seq))))))))

  (start)
  (.offer values 10)
  (.offer values 10)
  (.offer values 50)
  (.offer values (reduced 20))


  ;; `foldcat`, `cat` and `append!`

  (def input (r/map inc (into [] (range 1000))))
  (take 5 (r/fold r/cat r/append! input))
  (take 5 (r/foldcat input))

)


;; collections

(comment

  ;; `into`

  (into (vector-of :int) (comp (map inc) (filter even?)) (range 10))
  (into nil [1 2 3])
  (reduce conj '() [1 2 3])
  (conj nil 1)
  (conj [] 12 13 14)
  (into nil (map inc) [1 2 3])
  (into {} [{:a "1"} {:b "2"} {:b 3}])
  (type (nth (into-array Byte/TYPE [1 2 3]) 1))
  (transient [1 2 3])


  ;; `count`

  (count [1 2 3])
  (count {:id 1})


  ;; `nth`

  (nth nil -1)
  (nth [1 2 3] 1)
  (nth (subvec [1 2 3] 2) 0)
  (nth (vector-of :int 1 2 3) 0)

  (bit-and (hash {}) 0xFFFF)
  (partition 2 (range 12))


  ;; `empty`

  (empty (range 10))
  (let [q (empty clojure.lang.PersistentQueue/EMPTY)]
    (seq (conj q 1 2 3)))
  (empty "abc")
  (empty nil)


  (defn walk [data pred f]
    (letfn [(walk-c [d] (map
                         (fn [k] (walk k pred f)) d))
            (walk-m [d] (reduce-kv
                         (fn [m k v] (assoc m k (walk v pred f))) {} d))]
      (cond
        (map? data) (walk-m data)
        (coll? data) (into (empty data) (walk-c data))
        :else (if (pred data) (f data) data))))

  (def coll {:a [1 "a" [] {:c "z"} '(1 2)] :av 1N})
  (walk coll (every-pred number? odd?) inc)


  ;; `every?`, `not-every?`, `some` and `not-any?`

  (every? (every-pred number? pos?) [1 2 3 4])
  (not-every? neg? [-1 -2 0 -3])
  (some neg? [-1 -2 0 -3])
  (some #{0} [-1 -2 0 -3])
  (some (fn [[k v :as e]] (when (#{:id} k) e))
        {:id 1 :name "Ololosha"})
  (some {:id 1 :name "Ololosha"} [:id])
  (not-any? neg? [1 2 0 3])


  ;; `empty?` and `not-empty`

  (empty? [])
  (not-empty [1])


  ;; `conj`

  (def q (atom clojure.lang.PersistentQueue/EMPTY))
  (swap! q conj 1)
  (swap! q conj 2)
  (swap! q conj 3)

  (peek @q)
  (seq @q)
  (-> @q
      pop
      seq)
  (-> (clojure.lang.PersistentQueue/EMPTY)
      peek)


  (conj (range 3) 99)
  (conj (into [] (range 3)) 99)
  (conj {:a 1} [:b 2])
  (conj (vector-of :byte 1 2 3) 4)

  (as-> (sorted-set) s
    (conj s 1 3 2 4 -1)
    (conj s 111 -111))

  (cons 1 '(2 3))


  ;; `get`

  (defn select-matching [m k]
    (let [regex (re-pattern (str ".*" k ".*"))]
      (->> (keys m)
           (filter #(re-find regex (.toLowerCase %)))
           (reduce #(assoc %1 (keyword %2) (get m %2)) {}))))

  (defn search [k]
    (merge (select-matching (System/getProperties) k)
           (select-matching (System/getenv) k)))

  (search "user")

  (.get {:a 1 :b 2} :b)
  (.getOrDefault {:a 1 :b 2} :c "Not found")

  (find {:a 1 :b 2} :b)


  ;; `contains`

  (contains? [1 2 3] 0)
  (contains? #{1 2 3} 2)
  (contains? {:id 1} :id)
  (contains? "hello" 4)

  (.intValue (long (Math/pow 2 32)))


  ;; `rand-nth`, `shuffle`, `random-sample`

  (rand-nth (range 10))
  (rand-nth "abcdefghijklmnopqrstuvwxyz")
  (rand-nth '(1 2 3))
  (rand-nth [])
  (rand-nth nil)


  (defn round-robin [f hosts]
    (let [hosts (shuffle hosts)
          idx (atom 0)]
      (fn []
        (f (nth hosts @idx))
        (reset! idx (mod (inc @idx) (count hosts))))))

  (defn request [host & [path]]
    (println "calling" (format "http://%s/%s" host (or path "index.html"))))

  (def hosts ["10.100.89.42" "10.100.86.57" "10.100.23.12"])
  (def get-host (round-robin request hosts))
  (get-host)


  (random-sample 0.5 (range 10))

  (defn up-to-n-flip [n]
    (comp (take n) (random-sample 0.5)))

  (defn n-flip [n]
    (comp (random-sample 0.5) (take n)))

  (def head-tail-stream
    (interleave (repeat "head") (repeat "tail")))

  (defn flip [n]
    (into [] (n-flip n) head-tail-stream))

  (defn flip-up-to [n]
    (into [] (up-to-n-flip n) head-tail-stream))

  (flip 10)
  (flip-up-to 10)

  (eduction (n-flip 3) head-tail-stream)


  ;; `frequencies`

  (sort-by last > (frequencies [:a :b :b :c :c :d]))
  (frequencies ['() [] (clojure.lang.PersistentQueue/EMPTY)])
  (frequencies [(byte 1) (short 1) (int 1) (long 1) 1N])
  (group-by #(count (name %)) [:a :b :c :as :ad :af :asdf :asde ::a])
  (partition-by #(count (name %)) [:a :b :c :as :ad :af :asdf :asde ::a])


  ;; `sort` and `sort-by`

  (sort [18 43 3 0 9])
  (sort > [18 43 3 0 9])
  (sort < [18 43 3 0 9])

  (sort-by :age [{:age 65} {:age 13} {:age 8}])
  (sort-by :age > [{:age 65} {:age 13} {:age 8}])
  (sort-by str [:f "s" \c 'u])

  (let [a (to-array [3 2 1])] (sort a) (seq a))


  ;; `group-by`

  (def dict (slurp "/usr/share/dict/words"))
  (def anagrams (->> dict
                     (re-seq #"\S+")
                     (group-by sort)
                     (sort-by (comp count second) >)
                     (map second)))

  (nth anagrams 12)
  (count anagrams)


  ;; `replace`

  (replace {:a "a" :b "b"} [:a 1 2 :b 3 4])
  (replace [:a :b :c] (range 10))
  (map #({:a "a" :b "b"} % %) [:a 1 2 :b 3 4])

  (def h {:id 1
          :name "Ololosha"
          :assets [{:id 2
                    :name "asd"
                    :parts [1 2 3 {:id 3 :name "none"}]}]})

  (w/postwalk-demo h)
  (w/postwalk-demo [1 2 3 {:id 1 :name "asd"}])

  (w/prewalk-demo h)
  (w/prewalk-demo [1 2 3 {:id 1 :name "asd"}])


  ;; `reverse`

  (reverse [9 0 8 6 7 5 1 2 4 3])

  (defn dna-complementary-strand [strand]
    (->> strand
         reverse
         (replace {\A \T \T \A \C \G \G \C})
         (apply str)))

  (defn dna-complement [strand]
    [strand (dna-complementary-strand strand)])

  (dna-complementary-strand "ATCGCT")
  (dna-complement "ATCG")

  (rseq (into [] "ATCGCT"))

  (def t [:a :b :c [:d :e [:f 3]]])

  (tree-seq vector? seq t)


  ;; `walk`, `prewalk`, `postwalk`, `prewalk-demo` and `postwalk-demo`

  (def data
    {:type "workflow"
     :action '(do (println "flowchart") :done)
     :nodes [{:type "flowchart"
              :action '(do (println "flowchart") :done)
              :nodes [{:type "workflow"
                       :action nil
                       :nodes false}]}
             {:type "routine"
              :action '(do (println "routine") :done)
              :nodes [{:type "delimiter"
                       :action '(println "delimiter")
                       :nodes "2011/01/01"}]}
             {:type "pipeline"
              :action '(do (println "pipeline") :done)
              :nodes [{:type "workflow"
                       :action '(Thread/sleep 10000)
                       :nodes 90.11}]}
             {:type "delimiter"
              :action '(do (println "pipeline") :done)
              :nodes [{:type "workflow"
                       :nodes 90.11}]}]})

  (defn- step [node]
    (if (= "pipeline" (:type node))
      (dissoc node :nodes)
      (if-let [result (eval (:action node))]
        (assoc node :result result)
        node)))

  (time (w/prewalk step data))
  (time (w/postwalk step data))


  (def data
    [[1 2]
     [3 :a [5 [6 7 :b [] 9] 10 [11 :c]]]
     [:d 14]])


  ;; `prewalk-replace` and `postwalk-replace`

  (= (w/prewalk-replace {:a "A" :b "B" :c "C" :d "D"} data)
     (w/postwalk-replace {:a "A" :b "B" :c "C" :d "D"} data))

  (def ^:const greek
    '[α β γ δ ε ζ η θ ι κ λ μ ν ξ ο π ρ σ τ υ φ χ ψ ω])

  (= (w/prewalk-replace greek data)
     (w/postwalk-replace greek data))

  (def formula
    '(and (and a1 a2)
          (or (and a16 a3) (or a5 a8)
              (and (and a11 a9) (or a4 a8)))
          (and (or a5 a13) (and a4 a6)
               (and (or a9 (and a10 a11))
                    (and a12 a15)
                    (or (and a1 a4) a14
                        (and a15 a16))))))

  (def ands
    '{(and true true) true
      (and true false) false
      (and false true) false (and false false) false})

  (def ors
    '{(or true true) true (or true false) true
      (or false true) true (or false false) false})

  (def var-map
    '{a1 false a2 true a3 false a4 false
      a5 true a6 true a7 false a8 true
      a9 false a10 false a11 true a12 false
      a13 true a14 true a15 true a16 false})

  (def transformed-formula
    (w/postwalk-replace (merge var-map ands ors) formula))

  (eval transformed-formula)


  ;; `clojure.zip`

  (def vzip
    (zip/vector-zip
     [(subvec [1 2 2] 0 2)
      [3 4 [5 10 (vector-of :int 11 12)]]
      [13 14]]))

  (def szip
    (zip/seq-zip
     (list
      (range 2)
      (take 2 (cycle [1 2 3]))
      '(3 4 (5 10))
      (cons 1 '(0 2 3)))))

  (def xzip
    (zip/xml-zip
     (->
      "<b class=\"top\">
        <a>3764882</a>
        <c>80.12389</c>
        <f>
          <f1>77488</f1>
          <f2>1921.89</f2>
        </f>
       </b>"
      .getBytes io/input-stream xml/parse)))

  (pprint (meta vzip))
  (pprint vzip)
  (pprint szip)
  (pprint xzip)

  (pprint (zip/down vzip))
  (pprint (zip/rightmost (zip/down vzip)))

  (-> vzip zip/down zip/down zip/down)
  (-> vzip zip/down zip/rightmost (zip/replace :replaced) zip/up zip/node)
  (-> vzip zip/down zip/rightmost (zip/edit conj 15) zip/up zip/node)
  (-> vzip zip/down zip/rightmost zip/remove zip/root)
  (-> vzip zip/down zip/rightmost zip/remove zip/node)
  (-> vzip zip/down zip/rightmost (zip/insert-left 'INS) zip/up zip/node)
  (-> vzip zip/down zip/rightmost (zip/insert-right 'INS) zip/up zip/node)
  (-> vzip zip/down zip/rightmost (zip/insert-child 'INS) zip/up zip/node)
  (-> vzip zip/down zip/rightmost (zip/append-child 'INS) zip/up zip/node)
  (-> vzip zip/down zip/rightmost zip/down (zip/insert-child 'INS))
  (-> vzip zip/down zip/rightmost zip/down (zip/append-child 'INS))

  (defn remove-child [loc]
    (zip/replace loc
                 (zip/make-node loc (zip/node loc) (rest (zip/children loc)))))
  (-> vzip zip/down zip/rightmost remove-child zip/up zip/node)
  (-> vzip zip/down zip/rightmost remove-child remove-child zip/up zip/node)
  (-> vzip zip/next zip/node)
  (-> vzip zip/next zip/next zip/node)
  (-> vzip zip/next zip/next zip/next zip/node)
  (-> vzip zip/next zip/next zip/next zip/next zip/node)
  (->> vzip
       (iterate zip/next)
       (take-while (complement zip/end?))
       (map zip/node))


  (-> xzip zip/down zip/node)
  (-> xzip zip/down zip/children)
  (-> xzip zip/down zip/lefts)
  (-> xzip zip/down zip/rights)

  (zip/node szip)
  (-> szip
      zip/down zip/right zip/right
      zip/down zip/rightmost
      zip/down
      zip/path)


  (def document
    {:tag :balance
     :meta {:class "bold"}
     :node
     [{:tag :accountId
       :meta nil
       :node [3764882]}
      {:tag :lastAccess
       :meta nil
       :node ["2011/01/01"]}
      {:tag :currentBalance
       :meta {:class "red"}
       :node [{:tag :checking
               :meta nil
               :node [90.11]}]}]})

  (defn custom-zip [root]
    (zip/zipper
     #(some-> % :node first map?)
     (comp seq :node)
     (fn [node children]
       (assoc node :node (vec children)))
     root))

  (def czip (custom-zip document))
  (-> czip zip/down zip/rightmost zip/down zip/node)
)


;; `sequences`

(comment

  ;; `seq`, `rseq`

  (tree-seq vector? identity [1 2 [3 4]])
  (->> (file-seq (io/file "."))
       (map (memfn getName))
       count)

  (seq nil)
  (sequence nil)
  (sequence (map *)
            (range 10)
            (range 10)
            (range 10))

  (def v [1 2 3])
  (reversible? v)
  (rseq v)

  (defn complement-dna [nucl]
    ({\a \t \t \a \c \g \g \c} nucl))

  (defn is-palindrome? [dna]
    (= (map complement-dna dna) (rseq dna)))

  (defn find-palindromes [dna]
    (let [dna-length (count dna)]
      (for [i (range dna-length)
            j (range (inc i) dna-length)
            :let [word (subvec dna i (inc j))]
            :when (is-palindrome? word)]
        [i j (apply str word)])))

  (find-palindromes (vec "acgt"))


  ;; `subseq`, `rsubseq`

  (subseq (apply sorted-set (range 10)) > 2 < 8)
  (rsubseq (apply sorted-map (range 10)) <= 5)

  (defn smallest> [coll x] (first (subseq coll > x)))
  (defn smallest>= [coll x] (first (subseq coll >= x)))
  (defn greatest< [coll x] (first (rsubseq coll < x)))
  (defn greatest<= [coll x] (first (rsubseq coll <= x)))
  (def coll (sorted-map "a" 5 "f" 23 "z" 12 "g" 1 "b" 0))

  (smallest> coll "f")
  (smallest>= coll "f")
  (greatest< coll "f")
  (greatest<= coll "f")

  (def dict
    (into (sorted-set)
          (s/split (slurp "/usr/share/dict/words") #"\s+")))

  (defn complete [w dict]
    (take 4 (subseq dict >= w)))

  (complete "cloj" dict)
  (map #(complete % dict) ["c" "cl" "clo" "clos" "closu"])

  (java.util.Collections/binarySearch [0 0 1 2 3] 2)


  (def items (shuffle (range 1e5)))

  (quick-bench (let [x 5000 xs (sort items)]
                 (first (drop-while #(>= x %) xs))))

  (quick-bench (let [x 5000 ss (into (sorted-set) items)]
                 (first (subseq ss > x))))


  ;; `seque`

  (seque (range 10))

  (defn fast-producer [n]
    (->> (into () (range n))
         (map #(do (println "produce" %) %))))

  (defn slow-consumer [xs]
    (keep
     #(do
        (println "consume" %)
        (Thread/sleep 2000))
     xs))

  (slow-consumer (fast-producer 5))
  (slow-consumer (seque (fast-producer 5)))

  (defn slow-producer [n]
    (->> (into () (range n))
         (map
          #(do
             (println "produce" %)
             (Thread/sleep 2000) %))))

  (defn fast-consumer [xs]
    (map #(do (println "consume" %) %) xs))

  (fast-consumer (slow-producer 5))
  (fast-consumer (seque (slow-producer 5)))

  (defn by-type [ext]
    (fn [^String fname]
      (.endsWith fname ext)))

  (defn lazy-scan []
    (->> (java.io.File. "/")
         file-seq
         (map (memfn getPath))
         (filter (by-type ".txt"))
         (seque 50)))

  (defn go! []
    (loop [results (partition 5 (lazy-scan))]
      (println (with-out-str (clojure.pprint/write (first results))))
      (println "more?")
      (when (= "y" (read-line))
        (recur (rest results)))))

  (go!)

  (->> (java.io.File. "/")
         file-seq
         (map (memfn getPath))
         (drop 1000)
         (take 3))


  ;; `pmap`, `pcalls` and `pvalues`

  (pmap + (range 10) (range 10))

  (pcalls
   (constantly "Function")
   #(System/currentTimeMillis)
   #(println "side-effect"))

  (pvalues
   (+ 1 1)
   (Math/sqrt 2)
   (str "last" " " "item"))

  (time
   (dorun
    (map #(do (Thread/sleep 10) %) (range 1000))))
  (time
   (dorun
    (pmap #(do (Thread/sleep 10) %) (range 1000))))


  ;; `repeatedly`, `iterate`, `repeat` and `cycle`

  (take 5 (repeatedly rand))
  (take 5 (iterate inc 1))
  (take 5 (cycle (range 2)))
  (take 5 (repeat 5))


  (deref (future (Thread/sleep 1000))
         100
         :timeout)


  (def q (ConcurrentLinkedQueue. (range 20)))

  (def ^:const parallel 5)

  (defn task [job]
    (Thread/sleep (long (rand-int 2000)))
    (println "Work done on" job)
    (inc job))

  (def workers
    (repeatedly
     #(let [out *out*]
        (future
          (binding [*out* out]
            (when-let [item (.poll q)]
              (task item)))))))

  (defn run [workers]
    (println "-> starting" parallel "new workers")
    (let [done? #(> (reduce + (remove nil? %)) 30)
          futures (doall (take parallel workers))
          results (mapv deref futures)]
      (println (str "Results: " results))
      (cond
        (done? results) results
        (.isEmpty q) (println "Empty.")
        :else (recur (drop parallel workers)))))

  (run workers)
  (run (nthnext workers 15))
  (str q)


  (def fibo
    (iterate
     (fn [[x y]] [y (+' x y)])
     [0 1]))

  (take 10 fibo)
  (take 10 (map first fibo))
  (drop 4 (take 10 (map first fibo)))


  (defn calculate-pi [precision]
    (transduce
     (comp
      (map #(/ 4 %))
      (take-while #(> (Math/abs %) precision)))
     +
     (iterate #(* ((if (pos? %) + -) % 2) -1) 1.0)))

  (time (calculate-pi 1e-6))


  (take 10 (drop 3 (cycle [1 2 3 4])))


  (defn to-tally [n]
    (apply str (concat
                (repeat (quot n 5) "卌")
                (repeat (mod n 5) "|"))))

  (defn new-tally []
    (let [cnt (atom 0)]
      (fn []
        (to-tally (swap! cnt inc)))))

  (def t (new-tally))

  (repeatedly 5 t)


  ;; `lazy-seq`

  (lazy-seq [1])

  (defn trace [x] (println "evaluating" x) x)
  (def output (lazy-seq (list (trace 1) 2 3)))
  (first output)

  (defn repeat*
    [x]
    (lazy-seq (cons x (repeat* x))))

  (take 3 (repeat* 1))

  (defn repeatedly*
    [f]
    (lazy-seq (cons (f) (repeatedly f))))

  (take 3 (repeatedly* rand))

  (defn iterate*
    [f x]
    (lazy-seq (cons x (iterate* f (f x)))))

  (take 3 (iterate* inc 12))

  (defn lazy-map [f coll]
    (lazy-seq
     (when-first [x coll]
       (println "iteration")
       (cons (f x)
             (lazy-map f (rest coll))))))

  (def lazy-out (lazy-map str '(0 1 2)))
  (first lazy-out)

  (defn sieve [n]
    (letfn [(divisible-by? [n] #(zero? (rem % n)))
            (step [[x & xs]]
              (lazy-seq (cons x
                              (step (remove (divisible-by? x) xs)))))]
      (take n (step (nnext (range))))))

  (sieve 10)

  (let [[x & xs] [1 2 3 4]]
    xs)

  (concat [1 2 3] [4 5 6])
  (seq [1 2 3])


  (defn squares [x]
    (cons (* x x) (lazy-seq (squares (* x x)))))

  (def sq2 (squares 2))

  (take 5 sq2)
  (take 6 sq2)


  ;; `tree-seq`

  (pprint
   (tree-seq vector? identity [[1 2 [3 [[4 5] [] 6]]]]))

  (pprint
   (tree-seq map? :friends
             {:id 1
              :name "Ololosha"
              :friends [{:id 2 :name "Ashalola"}
                        {:id 3}
                        {:id 4}]}))

  (map (memfn getName)
       (take 5
             (remove (memfn ^File isDirectory)
              (tree-seq
               (memfn ^File isDirectory)
               (comp seq (memfn ^File listFiles))
               (File. "/")))))


  (def document
    {:tag :balance
     :meta {:class "bold"}
     :readonly true
     :node
     [{:tag :accountId
       :meta nil
       :node [3764882]}
      {:tag :lastAccess
       :meta nil
       :node ["2011/01/01"]}
      {:tag :currentBalance
       :meta {:class "red"}
       :node [{:tag :checking
               :meta nil
               :node [90.11]}]}]})

  (def branch?
    map?
    #_(complement (some-fn string? number?)))

  (def document-seq
    (tree-seq
     branch?
     :node
     document))

  (remove branch? document-seq)
  (keep :meta document-seq)
  (keep :readonly document-seq)


  ;; `file-seq`

  (take 10 (->> (file-seq (io/file "."))
               (filter (memfn ^File isDirectory))
               (map (memfn ^File getName))))


  ;; `xml-seq`

  (def balance
    "<balance>
       <accountId>3764882</accountId>
       <currentBalance>80.12389</currentBalance>
       <contract>
         <contractId>77488</contractId>
         <currentBalance>1921.89</currentBalance>
       </contract>
     </balance>")

  (def xml (-> ^String balance .getBytes io/input-stream xml/parse))
  (filter (comp string? first :content) (xml-seq xml))
  (filter string? (xml-seq xml))

  (def feeds
    [[:guardian "https://www.theguardian.com/world/rss"]
     [:wash-post "http://feeds.washingtonpost.com/rss/rss_blogpost"]
     [:nytimes "https://rss.nytimes.com/services/xml/rss/nyt/World.xml"]
     [:wsj "https://feeds.a.dj.com/rss/RSSWorldNews.xml"]
     [:reuters "http://feeds.reuters.com/reuters/UKTopNews"]])

  (defn search-news [q [feed url]]
    (let [content (comp first :content)]
      [feed
       (sequence
        (comp
         (filter (comp string? content))
         (filter (comp #{:title} :tag))
         (filter #(re-find q (content %)))
         (map content))
        (xml-seq (xml/parse url)))]))

  (pmap (partial search-news #"(?i)climate") feeds)


  ;; `re-seq`

  (re-seq #"\d+" "This sentence has 2 numbers and 6 words.")

  (def sb (doto (StringBuilder.)
            (.append "23")
            (.append "aa 42")))

  (re-seq #"\d+" sb)

  (map (memfn ^String toUpperCase) (re-seq #"\w" "hello"))

  (def signed-up
    "Jack 221 610-5007 (call after 9pm),
     Anna 221 433-4185,
     Charlie 661 471-3948,
     Hugo 661 653-4480 (busy on Sun),
     Jane 661 773-8656,
     Ron 555 515-0158")

  (let [people (re-seq #"(\w+) (\d{3}) \d{3}-\d{4}" signed-up)]
    {:names (map second people) :area (map last people)})

  (defn restream-seq [^Pattern re ^InputStream is]
    (let [s (Scanner. is "UTF-8")]
      ((fn step []
         (if-let [token (.findInLine s re)]
           (cons token (lazy-seq (step)))
           (when (.hasNextLine s) (.nextLine s) (step)))))))

  (defn pi-seq [is]
    (sequence
     (comp
      cat
      (map int)
      (map #(mod % 48)))
     (restream-seq #"\d{10}" is)))

  (def ^URL pi-digits (URL. "https://tinyurl.com/pi-digits"))

  (with-open [^InputStream is (.openStream pi-digits)]
    (doall (take 20 (pi-seq is))))


  ;; `line-seq`

  (with-open [r (io/reader "https://tinyurl.com/pi-digits")]
    (count (line-seq r)))

  (def alexa "http://s3.amazonaws.com/alexa-static/top-1m.csv.zip")

  (defn zip-reader [url]
    (-> (URL. url)
        .openConnection
        .getInputStream
        ZipInputStream.
        (doto .getNextEntry)
        io/reader))

  (defn domain [^String line]
    (some-> line (.split "\\.") last))

  (defn first-of-domain [ext]
    (with-open [^java.io.Reader r (zip-reader alexa)]
      (some #(when (= ext (domain %)) %) (line-seq r))))

  (first-of-domain "me")

  (defn top-10-domains-by-traffic [url]
    (with-open [^java.io.Reader r (zip-reader url)]
      (->> (line-seq r)
           (map domain)
           frequencies
           (sort-by last >)
           (take 10))))

  (top-10-domains-by-traffic alexa)


  ;; `resultset-seq`

  (defn rand-resultset [attrs]
    (reify
      ResultSet
      (getMetaData [_]
        (reify
          ResultSetMetaData
          (getColumnCount [_] (count attrs))
          (getColumnLabel [_ idx] (nth attrs (dec idx)))))
      (next [_] true)
      (close [_])
      (^Object getObject [_ ^int idx] (rand-int 1000))))

  (take 3 (resultset-seq (rand-resultset ["id" "count"])))


  ;; `iterator-seq` and `enumeration-seq`

  (def an-iterator (.iterator [1 2 3]))
  (def an-enumeration (Collections/enumeration [1 2 3]))
  (iterator-seq an-iterator)
  (enumeration-seq an-enumeration)

  (->> "Clojure is the best language"
       (.splitAsStream #"\s+")
       .iterator
       iterator-seq)


  ;; `concat` and `lazy-cat`

  (concat [1 2 3] () {:a 1} "hi" #{5})
  (concat [1] nil)
  (lazy-cat [1 2 3] (range 4))

  (defn identifier [x]
    (let [classname #(.getName ^Class %)
          split #(.split ^String % "\\.")
          typex (type x)]
      (apply str
             (interpose "-"
                        (concat
                         (split (classname typex))
                         (mapcat (comp split classname) (supers typex)))))))

  (identifier #"regex")
  (identifier [])

  (supers (class "hello"))

  (def fibs (lazy-cat [0 1] (map +' fibs (rest fibs))))
  (take 10 fibs)
  (last (take 100 fibs))


  ;; `list`, `cons` and `list*`

  (type (list 1 2 3 4 5))
  (type (list* 1 2 3 nil))

  (str (seq (take 10 (iterate list ()))))

  (def push conj)
  (def opening-brackets {\[ \] \( \) \{ \}})

  (defn check [form stack]
    (let [closing-brackets (map-invert brackets)
          reduction-step
          (fn [q x]
            (cond
              (opening-brackets x) (do (println q)
                                       (push q x))
              (closing-brackets x)
              (if (= (opening-brackets (peek q)) x)
                (do (println q)
                  (pop q))
                (throw
                 (ex-info
                  (str "Unmatched delimiter " x) {})))
              :else q))]
      (when (reduce reduction-step stack form)
        :ok)))

  (check "(let [a (inc 1]) (+ a 2))" ())
  (check "(let [a (inc 1)] (+ a 2))" ())

)


;; sequential processing

(comment

  ;; `rest`, `next`, `fnext`, `nnext`, `ffirst`, `nfirst` and `butlast`

  (defn lazy-expensive []
    (map #(do (println "thinking hard") %)
         (into () (range 10))))

  (defn lazy-loop-next [xs]
    (lazy-seq
     (when xs
       (cons
        (first xs)
        (lazy-loop-next (next xs))))))

  (first (lazy-loop-next (lazy-expensive)))
  (second (lazy-loop-next (lazy-expensive)))

  (defn lazy-loop-rest [xs]
    (lazy-seq
     (when-first [x xs]
       (cons x
             (lazy-loop-rest (rest xs))))))

  (first (lazy-loop-rest (lazy-expensive)))


  (defn into* [to & args]
    (into to
          (apply comp (butlast args))
          (last args)))

  (into* [] (range 10))
  (into* [] (map inc) (range 10))
  (into* [] (map inc) (filter odd?) (range 10))


  ;; `drop`, `drop-while`, `drop-last`, `take`, `take-while`, `take-last`,
  ;; `nthrest`, `nthnext`

  (take 10 (range 5))
  (take-while even? (range 5))
  (take-last 10 (range 5))
  (take-nth 2 (range 5))

  (drop 1 (range 5))
  (drop-while even? (range 5))
  (drop-last 1 (range 5))

  (def day-of-year (.get (Calendar/getInstance) (Calendar/DAY_OF_YEAR)))
  (drop day-of-year (range 1 366))

  (defn generate-file [id]
    (let [file (File/createTempFile (str "temp" id "-") ".tmp")]
      (println "Creating file" (.getName ^File file))
      (with-open [fw (io/writer file)]
        (binding [*out* fw]
          (pr id)
          file))))

  (defn fetch-clean [f]
    (let [content (slurp f)]
      (println "Deleting file" (.getName ^File f))
      (io/delete-file f)
      content))

  (defn service []
    (let [data (map #(generate-file %) (list 1 2 3 4 5))]
      (nthrest (map fetch-clean data) 2)))

  (def consumer (service))
  (first consumer)
  (fnext consumer)
  (last consumer)

  (pop [1 2 3])


  ;; `keep` and `keep-indexed`

  (keep-indexed #(nthnext (repeat %2 %1) %1) [1 3 8 3 4 5 6])

  (defn index-of [x coll]
    (keep-indexed #(when (= %2 x) %1) coll))

  (first (index-of 2 (list 3 9 1 0 2 3 2)))
  (first (index-of 11 (list 3 9 1 0 2 3 2)))


  ;; `mapcat`

  (map range [1 5 10])
  (mapcat range [1 5 10])
  (sequence (mapcat repeat) [1 2 3] ["!" "?" "$"])
  (sequence (map repeat) [1 2 3] ["!" "?" "$"])

  (def hex?
    (set (sequence
          (comp
           (mapcat range)
           (map char))
          [48 65]
          [58 71])))

  (every? hex? "CAFEBABE")


  (def libs {:async        [:analyzer.jvm]
             :analyzer.jvm [:memoize :analyzer :reader :asm]
             :memoize      [:cache]
             :cache        [:priority-map]
             :priority-map []
             :asm          []})

  (defn tsort [deps k]
    (loop [res () ks [k]]
      (if (empty? ks)
        res
        (recur (apply conj res ks)
               (mapcat deps ks)))))

  (tsort libs :async)


  ;; `interpose` and `interleave`

  (interpose :orange [:green :red :green :red])
  (interleave [:green :red :blue] [:yellow :magenta :cyan])

  (defn team [& names]
    (apply interleave (map repeat names)))

  (defn shifts [& teams]
    (apply interleave teams))

  (def a-team (team :john :rob :jessica))
  (def b-team (team :arthur :giles))
  (def c-team (team :paul :eva :donald :jake))

  (take 10 (shifts a-team b-team))

  (defn untangle [n xs]
    (letfn [(step [xs]
              (lazy-seq
               (cons
                (take-nth n xs)
                (step (rest xs)))))]
      (take n (step xs))))

  (untangle 2 (interleave (range 3) (repeat 3 ".")))


  ;; `partition`, `partition-all` and `partition-by`

  (partition 3 (range 10))
  (partition-all 3 (range 10))
  (take 5 (partition 0 [1]))

  (partition 3 2 (range 10))
  (partition 3 3 [:a :b :c] (range 10))

  (def records (map #(-> {:id % :data (str %)}) (range 1000)))

  (defn log [query] (str (subs query 0 70) "...\n"))

  (defn insert-query [records]
    (let [->value (fn [{:keys [id data]}] (format "(%s,%s)" id data))
          rows    (apply str (interpose "," (map ->value records)))]
      (log
       (str "INSERT INTO records (id, data) VALUES " rows
            " ON DUPLICATE KEY UPDATE"))))

  (println (pmap insert-query (partition-all 10 records)))

  (def temps [42 42 42 42 43 43 43 44 44 44 45 45 46 48 45 44 42 42 42 42 41
              41])

  (map count (partition-by identity temps))


  ;; `flatten`

  (flatten [[1 2 [2 3] '(:x :y [nil []])]])

  (defn core-fns [form]
    (->> (w/macroexpand-all form)
         flatten
         (map str)
         (map #(re-find #"clojure\.core/(.*)" %))
         (keep last)
         distinct))

  (core-fns
   '(for [[head & others] coll
          :while          #(< i %)
          :let            [a (mod i 2)]]
      (when (zero? a)
        (doseq [item others]
          (print item)))))


  ;; `distinct`, `dedupe` and `distinct?`

  (distinct [1 2 1 1 3 2 4 1])
  (distinct? 1 2 3 2 4 1)
  (dedupe [1 2 1 1 3 2 4 1])

  (def votes [
              {:id 14637 :vote 3 :secs 5}
              {:id 39212 :vote 4 :secs 9}
              {:id 39212 :vote 4 :secs 9}
              {:id 14637 :vote 2 :secs 43}
              {:id 39212 :vote 4 :secs 121}
              {:id 39212 :vote 4 :secs 121}
              {:id 45678 :vote 1 :secs 19}])

  (->> votes
       dedupe
       (group-by :id)
       (reduce-kv
        (fn [m user votes]
          (assoc m user (distinct (map :vote votes))))
        {}))

  (group-by :id votes)

  (def duplicates [8 1 2 1 1 7 3 3])
  (distinct duplicates)
  (dedupe (sort duplicates))
  (set duplicates)


  ;; `take-nth`

  (take-nth 3 [0 1 2 3 4 5 6 7 8 9])
  (take-nth 2.5 (range 10))
  (sequence (take-nth 2.5) (range 10))

  (defn xdrop-nth [n]
    (keep-indexed
     #(when-not (zero? (rem %1 n)) %2)))

  (sequence (xdrop-nth 3) (range 10))


  ;; `split-at` and `split-with`

  (split-at 8 (range 10))
  (split-at 11 (range 10))
  (split-with (complement zero?) [1 4 5 0 3 2 0 1 1 0])
  (split-with (complement #{\a \e \i \o \u}) "hello")


  ;; `when-first`

  (when-first [x (range 10)] (str x))
  (when-first [x ()] (print "never gets here"))


  ;; `chunk-cons`, `chunk-first`, `chunk-rest`, `chunk-next`, `chunk-buffer`,
  ;; `chunk-append` and `chunk`

  (def b (chunk-buffer 10))
  (chunk-append b 0)
  (chunk-append b 1)
  (chunk-append b 2)
  (def first-chunk (chunk b))
  (chunk-cons first-chunk ())

  (let [cf (chunk-first (range 10))
        cb (chunk-buffer (count cf))]
    (map #(chunk-append cb %) cf)
    (chunk-cons (chunk cb) ()))
)


;; maps

(comment

  ;; `hash-map`, `array-map`

  (hash-map)
  (hash-map "Jack N"
            "381-883-1312"
            "Book Shop" "381-144-1256"
            "Lee J."
            "411-742-0032"
            "Jack N"
            "534-131-9922")

  (apply hash-map (mapcat vector (range 4) (repeatedly rand)))

  (def long-url
    (str "https://notifications.google.com/u/0/_"
         "/NotificationsOgbUi/data/batchexecute?"
         "f.sid=4896754370137081598&hl=en&soc-app=208&"
         "soc-platform=1&soc-device=1&_reqid=53227&rt="))

  (defn split-pair [pair]
    (let [[k v] (s/split pair #"=")]
      (if v
        [k v]
        [k nil])))

  (defn params [url]
    (as-> url x
      (s/split x #"\?")
      (last x)
      (s/split x #"\&")
      (mapcat split-pair x)
      (apply hash-map x)))

  (params long-url)

  (let [pairs (into [] (range 2e6))]
    (quick-bench (apply hash-map pairs)))


  ;; `sorted-map` and `sorted-map-by`

  (sorted-map :c 3 :b 2 :a 1)
  (sorted-map-by #(< (:age %1) (:age %2))
                 {:age 35} ["J" "K"]
                 {:age 13} ["Q" "R"]
                 {:age 14} ["T" "V"])

  (defn timed [s]
    (let [t (System/nanoTime)]
      (println "key" s "created at" t)
      (with-meta s {:created-at t})))

  (def m (sorted-map (timed 'a) 1 (timed 'a) 2))
  (println m)
  (meta (ffirst m))


  ;; `create-struct`, `defstruct`, `struct-map`, `struct` and `accessor`

  (defstruct Foo :id :name)
  (def o (struct-map Foo :id 1 :name "Ololosha"))
  (struct Foo 1 "Ololosha")
  (def a (accessor Foo :id))
  (a o)


  ;; `zipmap`

  (zipmap [:a :b :c] [1 2 3])

  (def file-content
    "TITLE,FIRST,LAST,NUMBER,STREET,CITY,POST,JOINED
     Mrs,Mary,Black,20,Hillbank St,Kelso,TD5 7JW,01/05/2012 12:51
     Miss,Chris,Bowie,44,Hall Rd,Sheffield,S5 7PW,01/05/2012 17:02
     Mr,John,Burton,41,Warren Rd,Yarmouth,NR31 9AB,01/05/2012 17:08")

  (defn split [line]
    (s/split line #","))

  (defn transform [data]
    (let [lines   (line-seq data)
          headers (split (first lines))]
      (eduction
       (map split)
       (map (partial zipmap headers))
       (rest lines))))

  (with-open [data (io/reader (char-array file-content))]
    (doall (transform data)))


  ;; `keys` and `vals`

  (keys {:a 1 :b 2 :c 3 :d 4 :e 5 :f 6 :h 7 :i 8 :j 9})
  (vals {:a 1 :b 2 :c 3})


  ;; `find`, `key` and `val`

  (find (hash-map :a 1 :b 2) :a)
  (find (array-map :a 1 :b 2) :a)
  (find (sorted-map :a 1 :b 2) :a)
  (find (struct (create-struct :a :b) 1 2) :a)
  (find (HashMap. {:a 1 :b 2}) :a)
  (find [:a :b :c] 1)
  (find (subvec [:x :a :b :c] 1 3) 1)
  (find (vector-of :int 1 2 3) 1)

  (key (first {:a 1 :b 2}))
  (key (find {:a 1 :b 2} :a))
  ((juxt key val) (last (System/getenv)))


  ;; `select-keys` and `get-in`

  (select-keys {:a 1 :b 2 :c 3} [:a :c])
  (get-in {:a 1 :b {:c 3}} [:b :c])
  (select-keys [:a :b :c :d :e] [1 3])
  (get-in [:a :b :c [:d :e]] [3 1])

  (get-in '(0 1 2 3) [0])
  (select-keys {:a 1 :b 2} [])
  (get-in {:a 1 :b 2} [])

  (let [word "hello"]
    (select-keys (vec word) (filter even? (range (count word)))))

  (def products
    [{:product
      {:legal-fee-added    {:rate "2%" :period "monthly"}
       :company-name       "Together"
       :fee-attributes     [["Jan" 8] 99 50 13 38 62]
       :initial-rate       9.15
       :initial-term-label {:bank "provided" :form "Coverage"}
       :created-at         1504556932727}}
     {:product
      {:legal-fee-added    {:rate "4.2%" :period "yearly"}
       :company-name       "SGI"
       :fee-attributes     [["Mar" 8] 99 50 13 38 62]
       :initial-rate       2.15
       :initial-term-label {:bank "provided" :form "Coverage"}
       :created-at         1504556432722}}
     {:product
      {:legal-fee-added    {:rate "2.6%" :period "monthly"}
       :company-name       "Together"
       :fee-attributes     [["Jan" 8] 99 50 13 38 62]
       :initial-rate       5.5
       :initial-term-label {:bank "Chase" :form "Assisted"}
       :created-at         1504556332211}}])

  (defn lowest-rate [products]
    (get-in products [0 :product :legal-fee-added :rate]))

  (lowest-rate products)


  ;; `assoc`, `assoc-in` and `dissoc`

  (def m {:a "1" :b "2" :c "3"})
  (assoc m :b "changed")
  (dissoc m :a :c)

  (def m {:a "1" :b "2" :c {:x1 {:x2 "z1"}}})
  (assoc-in m [:c :x1 :x2] "z2")
  (assoc-in m [:c1 :x1 :x3] "z3")

  (def v [1 2 3 4 5 6 7])
  (assoc v 1 22)
  (assoc-in v [7 1 2] 1)


  ;; `update` and `update-in`

  (update {:a 1 :b 2} :b inc)
  (update {:a 1 :b 2} :c (fnil inc 273.15))
  (update-in {:a 1 :b {:c 2}} [:b :c] + 13)

  (update [:a :b :c] 3 (fnil keyword "d"))
  (update-in [:a :b :c [:a]] [3 0] (fnil #(keyword (s/upper-case (name %))) :w))
  (update [:a :b :c] 1 (fnil #(keyword (s/upper-case (name %))) :w))


  ;; `merge` and `merge-with`

  (merge {:a 1 :b 2 :c 1} {:c 3 :d 4})
  (merge-with + {:a 1} {:b 2 :a 10})
  (merge)

  (let [m1 {:id [11] :colors ["red" "blue"]}
        m2 {:id [10] :colors ["yellow"]}
        m3 {:id [31] :colors '("brown" "red")}]
    (merge-with into m1 m2 m3))

  (defprotocol IComplex
    (sum [c1 c2]))

  (defrecord Complex [re im]
    IComplex
    (sum [c1 c2] (merge-with + c1 c2)))

  (sum (Complex. 2 5) (Complex. 1 3))

  (let [m1 {:a [1 3] :b 2}
        m2 {:a 'a :b 'b}
        m3 {:a [:b :c] :b "b"}]
    (merge-with (fn [v1 v2]
                  (if (::multi (meta v1))
                    (conj v1 v2)
                    ^::multi [v1 v2]))
                m1 m2 m3))


  ;; `reduce-kv`

  (reduce
   (fn [m [k v]] (assoc m k (inc v)))
   {}
   {:a 1 :b 2 :c 3})

  (reduce-kv
   (fn [m k v] (assoc m k (inc v)))
   {}
   {:a 1 :b 2 :c 3})

  (def env
    {"TERM_PROGRAM" "iTerm.app"
     "SHELL"        "/bin/bash"
     "COMMAND_MODE" "Unix2003"})

  (defn transform [^String s]
    (some-> s
            .toLowerCase
            (.replace "_" "-")
            keyword))

  (reduce-kv
   (fn [m k v] (assoc m (transform k) v))
   {}
   env)

  (extend-protocol clojure.core.protocols/IKVReduce
    java.util.Map
    (kv-reduce [m f init]
      (let [iter (.. m entrySet iterator)]
        (loop [ret init]
          (if (.hasNext iter)
            (let [^java.util.Map$Entry kv (.next iter)
                  ret                     (f ret (.getKey kv) (.getValue kv))]
              (if (reduced? ret)
                @ret
                (recur ret)))
            ret)))))

  (reduce-kv
   (fn [m k v] (assoc m (transform k) v))
   {}
   (System/getenv))

  (reduce-kv
   (fn [m k v] (assoc m (transform k) v))
   {}
   (System/getProperties))

  (reduce-kv
   (fn [m k v]
     (if (> k 2)
       (reduced m)
       (assoc m k v)))
   {}
   [:a :b :c :d :e])

  (reduce-kv
   (fn [m k v]
     (if (= k :abort)
       (reduced m)
       (assoc m k v)))
   {}
   (LinkedHashMap. {:a 1 :abort true :c 3}))


  ;; `clojure.walk/keywordize-keys` and `clojure.walk/stringify-keys`

  (w/keywordize-keys {"a" 1 "b" 2})
  (w/stringify-keys {:a 1 :b 2})

  (def products
    [{"type"     "Fixed"
      "bookings" [{"upto" 999 "flat" 249.0}]
      "enabled"  false}
     {"type"     "Variable"
      "bookings" [{"upto" 200 "flat" 20.0}]
      "enabled"  true}])

  (w/keywordize-keys products)

  ;; requires a PersistentMap
  (w/keywordize-keys (System/getProperties))


  ;; `clojure.set/rename-keys`

  (st/rename-keys {:a 1 :b 2 :c 3} {:a "AA" :b "B1" :c "X"})
  (st/rename-keys {:a 1 :b 2 :c 3} {:c :a :a :b :b :c})
  (st/rename-keys (sorted-map :a 1 :b 2 :c 3) {:a :z})


  ;; `clojure.set/map-invert`

  (st/map-invert {:a 1 :b 2})
  (st/map-invert {:a 1 :b 1 :c 1 :d 1})
  (st/map-invert [1 2 3 4 4])

  (def scramble-key
    {\a \t \b \m \c     \o \d \l
     \e \z \f \i \g     \b \h \u
     \i \h \j \n \k     \s \l \r
     \m \a \n \q \o     \d \p \e
     \q \k \r \y \s     \f \t \c
     \u \p \v \w \w     \x \x \j
     \y \g \z \v \space \-})

  (defn scramble [text scramble-key]
    (apply str (map scramble-key text)))

  (defn unscramble [text scramble-key]
    (apply str (map (map-invert scramble-key) text)))

  (scramble "try to read this if you can" scramble-key)
  (unscramble "cyg-cd-yztl-cuhf-hi-gdp-otq" scramble-key)
)


;; vectors

(comment

  ;; `vector`, `vec`

  (vector :a :b :c)
  (apply vector 1 2 [3 4 5])
  (vec '(3 4 5))


  ;; `peek` and `pop`

  ([:a :b :c] 2)

  (get [:a :b :c] 2)
  (nth [:a :b :c] 2)
  (assoc [:a :b :c] 2 :d)
  (conj [:a :b :c] 3.1 :e)
  (pop [:a :b :c])
  (peek [:a :b :c])


  (k/bench (Thread/sleep 100))


  (vec '(:a 1 nil {}))

  (.isArray (class (make-array Integer 3)))
  (.isArray (class (int-array [1 2 3])))


  (def q (PersistentQueue/EMPTY))
  (def v [])
  (def l ())
  (peek (conj q "a" "b" "c"))
  (peek (conj v "a" "b" "c"))
  (peek (conj l "a" "b" "c"))

  (-> (PersistentQueue/EMPTY) (conj "a" "b" "c") pop vec)
  (pop (conj [] "a" "b" "c"))
  (pop (conj () "a" "b" "c"))


  ;; `vector-of`

  (vector-of :int)
  (vector-of :int 16/5 2.0 1M Double/NaN)
  ((vector-of :int 1 2 3) 2)
  (sort [(vector-of :int 7 8 9)
         (vector-of :int 0 1 2)])

  (vector-of :boolean true true false)

  (def max-iterations 99)

  (defn calc-mandelbrot [c-re c-im]
    (let [sq
          (fn [x] (* x x))
          iter (reduce (fn [[z-re z-im] i]
                         (if (or (= i 99) (> (+ (sq z-re) (sq z-im)) 4))
                           (reduced i)
                           [(+ c-re (sq z-re) (- (sq z-im)))
                            (+ c-im (* 2 z-re z-im))]))
                       [0 0] (range (inc max-iterations)))]
      (vector-of :double c-re c-im iter)))

  (def mandelbrot-set
    (for [im (range 1 -1 -0.05) re (range -2 0.5 0.0315)]
      (calc-mandelbrot re im)))

  (doseq [row (partition 80 mandelbrot-set)]
    (doseq [point row]
      (print (if (> max-iterations (get point 2)) "*" " ")))
    (println))


  ;; `mapv`, `filterv`

  (mapv inc [0 1 2 3])
  (filterv odd? (range 8))

  (defn calculate-pi [precision]
    (->> (iterate #(* ((if (pos? %) + -) % 2) -1) 1.0)
         (map #(/ 4 %))
         (take-while #(> (Math/abs %) precision))
         (reduce +)))

  (defn calculate-e [precision]
    (letfn [(factorial [n] (reduce * (range 1 (inc n))))]
      (->> (range)
           (map #(/ (+ (* 2.0 %) 2) (factorial (inc (* 2 %)))))
           (take-while #(> (Math/abs %) precision))
           (reduce +))))

  (defn get-results [channels]
    (let [[result channel] (alts!! channels)
          new-channels
          (filterv #(not= channel %) channels)]
      (if (empty? new-channels)
        [result]
        (conj (get-results new-channels) result))))

  (let [[pi-in pi-out e-in e-out] (repeatedly 4 chan)]
    (go (>! pi-out {:type :pi :num (calculate-pi (<! pi-in))}))
    (go (>! e-out {:type :e :num (calculate-e (<! e-in))}))
    (>!! pi-in 1e-4)
    (>!! e-in 1e-5)
    (get-results [e-out pi-out]))


  ;; `subvec`

  (subvec [1 2 3 4] 1 3)

  (defn norm [v]
    (loop [v v
           res 0.]
      (if (= 0 (count v))
        (Math/sqrt res)
        (recur (subvec v 1)
               (+ res (Math/pow (nth v 0) 2))))))

  (norm [-2 1])
)


;; sets

(comment

  ;; `set`, `hash-set`

  (set? #{})
  (hash-set :yellow :red :green :green)
  (hash-set (rand) (rand) (rand))
  (set [:yellow :red :green :green])

  (= #{3 2 1} (hash-set 1 2 3))
  (some #{:x :c} [:a :b :c :d :e])

  (def s #{:a :b :c})
  (def powerset-of-s
    #{#{} #{:a} #{:b} #{:c} #{:a :b} #{:a :c} #{:b :c} #{:a :b :c}})

  (defn powerset [s]
    (when-first [x s]
      (let [p (or (powerset (disj s x)) (hash-set #{}))]
        (st/union p (set (map conj p (repeat x)))))))

  (defn powerset* [items]
    (reduce
     (fn [s x]
       (st/union s (map #(conj % x) s)))
     (hash-set #{})
     items))

  (powerset #{1 2 3})
  (powerset* #{1 2 3})
  (powerset #{})
  (powerset* #{})


  ;; `sorted-set` and `sorted-set-by`

  (type (set (sorted-set 8 7 4 2 1 3)))
  (type (into #{} (sorted-set 8 7 4 2 1 3)))


  (sorted-set "t" "d" "j" "w" "y")
  (sorted-set-by #(compare %2 %1) "t" "d" "j" "w" "y")
  (sorted-set [1 "b" :x] [1 "a" :y])
  (sorted-set-by compare [1 "b" :x] [1 "a" :y])
  (sorted-set-by #(compare (last %2) (last %1)) [1 "b" :x] [1 "a" :y])

  (sorted-set-by
   (fn [a b]
     (println a b)
     (compare [(count b) a] [(count a) b]))
   [1 :a] [:b] [3 :c] [:v])


  ;; `disj`

  (disj #{1 4 6 8} 4 8)
  (disj (sorted-set-by > 1 4 6) 4)
  (disj #{1})

  (def ports (atom #{}))

  (defn serve [port]
    (if (= @ports (swap! ports conj port))
      "Port already serving requests."
      (future
        (with-open [server (ServerSocket. port)
                    socket (.accept server)
                    writer (io/writer socket)
                    reader (io/reader socket)]
          (as-> (.readLine reader) line
            (str line "\n")
            (.write writer line)
            (.flush writer)))
        (swap! ports disj port))))

  (serve 10002)


  ;; `union`, `difference` and `intersection`

  (st/union #{1 2 3} #{4 2 6})
  (st/union nil nil)
  (st/union)
  (st/difference #{1 2 3} #{4 2 6})
  (st/difference #{1 2 3} #{1 2 3})
  (st/difference nil nil)
  (st/intersection #{1 2 3} #{4 2 6})
  (st/intersection #{1 3} #{4 6})
  (st/intersection #{1 2 3} nil)
  (st/intersection nil nil)

  (defn symmetric-difference [s1 s2]
    (st/difference (st/union s1 s2) (st/intersection s1 s2)))

  (symmetric-difference (sorted-set 1 2 4) #{1 6 8})


  ;; `subset?` and `superset?`

  (st/subset? #{1 2} #{1 2 3})
  (st/superset? #{:a :b :c} #{:a :c})

  (st/superset? nil #{})
  (st/subset? #{0 3} [:a :b :c :d])


  ;; `select`, `index`, `rename`, `join` and `project`

  (def users
    #{{:user-id 1 :name "john"
       :age 22 :type "personal"}
      {:user-id 2 :name "jake"
       :age 28 :type "company"}
      {:user-id 3 :name "amanda" :age 63 :type "personal"}})

  (def accounts
    #{{:acc-id 1 :user-id 1 :amount 300.45 :type "saving"}
      {:acc-id 2 :user-id 2 :amount 1200.0 :type "saving"}
      {:acc-id 3 :user-id 1 :amount 850.1 :type "debit"}})

  (st/select #(> (:age %) 30) users)

  (st/project
   (st/join users accounts {:user-id :user-id})
   [:user-id :acc-id :name])

  (st/project
   (st/join users accounts {:user-id :user-id})
   [:user-id :acc-id :type])

  (st/project
   (st/join users (st/rename accounts {:type :atype}))
   [:user-id :acc-id :type :atype])

  (st/index users [:type])

)


;; concurrency

(comment

  ;; `future`

  (defn timer [seconds]
    (future
      (Thread/sleep (* 1000 seconds))
      (println "done" seconds "seconds.")
      ::ok))

  (def t1 (timer 10))
  (future? t1)
  (future-done? t1)
  (realized? t1)
  @t1

  (def an-hour (timer (* 60 60)))
  (future-cancelled? an-hour)
  (future-cancel an-hour)
  (future-cancelled? an-hour)
  (future-cancel an-hour)
  (deref an-hour)

  (defn fetch-async [url] ;
    (future (doall (xml-seq (xml/parse url)))))

  (let [guardian (fetch-async "https://www.theguardian.com/world/rss")
        rss (fetch-async "https://rss.com/blog/category/press-releases/feed/")]
    (count (concat (take 5 @rss)
                   (take 10 (drop 5 @guardian)))))


  ;; `promise` and `deliver`

  (def p (promise))
  (realized? p)
  (future (println "Thread 1 got access to" @p))
  (future (println "Thread 2 got access to" @p))
  (future (println "Thread 3 got access to" @p))
  (deref p 1000 ::failed)
  (deliver p :location)
  (p :location)
  (realized? p)
  @p

  (def msgs (atom []))

  (defn smoke [smoker ingr1 ingr2]
    (let [i1 (deref ingr1 100 "fail!")
          i2 (deref ingr2 100 "fail!")]
      (swap! msgs conj (str smoker " " i1 " " i2))))

  (defn pick-two [tobacco paper matches]
    (rest
     (shuffle
      [#(deliver tobacco :tobacco)
       #(deliver paper :paper)
       #(deliver matches :matches)])))

  (defn run [n]
    (dotimes [i n]
      (swap! msgs conj (str "Round " i))
      (let [tobacco (promise) paper (promise) matches (promise)]
        (future (smoke "tobacco holder" paper matches))
        (future (smoke "paper holder" tobacco matches))
        (future (smoke "matches holder" tobacco paper))
        (doseq [add (pick-two tobacco paper matches)] (add))
        (Thread/sleep 100)))
    @msgs)

  (pprint (run 3))


  ;; `delay`

  (def d (delay (do (println "evaluated") ::forced)))
  (delay? d)
  (realized? d)
  (deref d)
  (force d)
  (realized? d)
  @d

  (def d1 (delay (do (Thread/sleep 10000) ::awake)))
  @d1

  (force 1)

  (def coll [(delay (println :evaluated) :item0) :item1 :item2])
  (map force coll)

  (def d (delay (throw (ex-info "error" {:cause (rand)}))))
  (try @d (catch Exception e (ex-data e)))


  ;; `ref`

  (def account-1 (ref 1000))
  (def account-2 (ref 500))

  @account-1

  (defn transfer [amount]
    (dosync
     (when (pos? (- @account-1 amount))
       (alter account-1 - amount)
       (alter account-2 + amount))
     {:account-1 @account-1 :account-2 @account-2}))

  (transfer 300)

  (ref-history-count account-1)
  (ref-min-history account-1)
  (ref-max-history account-1)

  (def r (ref 0))
  (ref-history-count r)

  (future
    (dosync
     (println "T1 waiting 5 seconds")
     (Thread/sleep 5000)
     (println "T1 reading ref:" @r)))

  (future
    (dosync
     (println "T2 changing ref")
     (println "T2 new value of ref:" (alter r inc))))

  (ref-history-count r)


  ;; `atom`

  (def m (atom {:a 1 :b {:c 2}}))

  (swap! m update-in [:b :c] inc)

  (def configuration (atom {}))

  (defn initialize []
    (reset! configuration (System/getenv)))

  (initialize)
  (take 3 (keys @configuration))

  (defn swap-or-bail! [a f & [attempts]]
    (loop [i (or attempts 3)]
      (if (neg? i)
        (println "Could not update. Bailing out.")
        (let [old (deref a)
              success? (compare-and-set! a old (f old))]
          (when-not (or success? (neg? i))
            (println "Update failed. Retry" i)
            (recur (dec i)))))))

  (defn slow-inc [x]
    (Thread/sleep 5000)
    (inc x))

  (def a (atom 0))
  (def f (future (swap-or-bail! a slow-inc)))

  (reset! a 1)
  (reset! a 2)
  (reset! a 3)
  (reset! a 4)
  @a


  ;; `agent`

  (def a (agent 0))
  (send a inc)
  (deref a)
  @(send a inc)
  (deref a)

  (def a (agent 10000))
  (def b (agent 10000))

  (defn slow-update [x]
    (Thread/sleep x)
    (inc x))

  (send a slow-update)
  (send b slow-update)
  (time (await a b))

  (send a slow-update)
  (send-off a slow-update)
  (time (await-for 2000 a b))

  (def fj-pool (Executors/newWorkStealingPool))

  (defn send-fj [^clojure.lang.Agent a f & args]
    (apply send-via fj-pool a f args))

  (def a (agent 1))
  (send-fj a inc)
  (await a)
  @a

  (set-agent-send-executor! fj-pool)
  (set-agent-send-off-executor! fj-pool)
  (release-pending-sends)

  (def a (agent 2))
  (send-off a #(/ % 0))
  (send-off a inc)
  (-> (agent-error a)
      (.getMessage))
  (restart-agent a 2)
  @a

  (def a (agent 2))
  (defn handle-error [a e]
    (println "Error was" (.getMessage e))
    (println "The agent has value" @a)
    (restart-agent a 2))

  (set-error-handler! a handle-error)

  (send-off a #(/ % 0))
  (send-off a inc)
  @a

  (set-error-mode! a :continue)
  (send-off a #(/ % 0))
  @a

  (shutdown-agents)

  @#'a


  ;; `deref` and `realized?`

  (def s1 (map inc (range 100)))

  (realized? s1)
  (first s1)
  (realized? s1)


  ;; `set-validator!` and `get-validator`

  (def a (atom 1))

  (set-validator! a pos?)

  (defn- should-be-positive [x]
    (if (pos? x)
      x
      (throw
       (ex-info (format "%s should be positive" x)
                {:valid? (pos? x)
                 :value x
                 :error "Should be a positive number"
                 :action "State hasn't changed"}))))

  (set-validator! a should-be-positive)

  (swap! a dec)

  (try (swap! a dec)
       (catch Exception e (ex-data e)))

  (get-validator a)

  (set-validator! a nil)


  ;; `add-watch` and `remove-watch`

  (def account-1 (ref 1000 :validator (some-fn zero? pos?)))
  (def account-2 (ref 500 :validator (some-fn zero? pos?)))

  (defn transfer [amount a1 a2]
    (dosync
     (alter a1 - amount)
     (alter a2 + amount))
    {:account-1 @a1
     :account-2 @a2})

  (defn- to-monthly-statement [k r old new]
    (let [direction (if (< old new) "[OUT]" "[IN]")]
      (spit (str "/tmp/statement." k)
            (format "%s: %s$\n" direction (Math/abs (- old new)))
            :append true)))

  (add-watch account-1 "acc1" to-monthly-statement)
  (add-watch account-2 "acc2" to-monthly-statement)

  (transfer 300 account-1 account-2)
  (transfer 500 account-2 account-1)

  (println (slurp "/tmp/statement.acc1"))
  (println (slurp "/tmp/statement.acc2"))

  @account-1
  @account-2


  ;; `locking`, `monitor-enter` and `monitor-exit`

  (def lock (Object.))
  (def acc1 (volatile! 1000))
  (def acc2 (volatile! 300))

  (defn transfer [sum orig dest]
    (locking lock
      (let [balance (- @orig sum)]
        (when (pos? balance)
          (vreset! orig balance)
          (vreset! dest (+ @dest sum)))))
    [@orig @dest])

  (dotimes [_ 1500]
    (future (transfer 1 acc1 acc2)))

  [@acc1 @acc2]

)


;; types, classes, hierarchies and polymorphism

(comment

  ;; symbols and keywords

  (type #())
  (symbol "s")
  (keyword "k")

  first

  (def form (read-string "(a b :c)"))

  (map type form)

  (defmacro reading-symbols [& symbols]
    `(map type '~symbols))

  (reading-symbols a b)

  (def ax (symbol "a/x"))
  (def bx (symbol "b/x"))

  [(name ax) (name bx)]
  (= ax bx)
  [(namespace ax) (namespace bx)]

  (def ax (symbol "a" "x"))
  (def bx (keyword "b" "x"))
  [(namespace ax) (namespace bx)]

  (identical? (symbol "a") (symbol "a"))
  (identical? (keyword "a") (keyword "a"))

  (find-keyword "never-created")
  (find-keyword "doc")


  ;; type checking

  (cast String 1)

  (let [add-meta (with-meta [1 2 3] {:type 'MyVector})
        no-meta  [1 2 3]]
    [(type add-meta) (class add-meta) (type no-meta)])

  (defn make-type [obj t]
    (vary-meta obj assoc :type t))

  (def person (make-type {:name "John" :title "Mr"} :person))
  (type person)
  (def manning (make-type {:name "Manning" :owner "Marjan"} :business))

  (defn print-contact [contact]
    (condp = (type contact)
      :person   (println (:title contact) (:name contact))
      :business (println (:name contact) (str "(" (:owner contact) ")"))
      String    (println "Contact:" contact)
      (println "Unknown format.")))

  (print-contact person)
  (print-contact manning)
  (print-contact "Mr. Renzo")
  (print-contact nil)

  (defmulti print-contact type)

  (defmethod print-contact :person
    [contact]
    (println (:title contact) (:name contact)))

  (defmethod print-contact :business
    [contact]
    (println (:name contact) (str "(" (:owner contact) ")")))

  (defmethod print-contact String
    [contact]
    (println "Contact:" contact))

  (defmethod print-contact :default
    [contact]
    (println "Unknown format."))

  (print-contact person)
  (print-contact manning)
  (print-contact "Mr. Renzo")
  (print-contact nil)

  (in-ns 'my-package)
  (clojure.core/refer-clojure)
  (type (fn q? []))

  (instance? java.lang.Number (bigint 1))
  (instance? java.lang.Comparable 1)
  (supers (class 1))
  (ancestors (class 1))


  ;; `gen-class` and `gen-interface`

  (gen-interface
   :name "user.BookInterface"
   :extends [java.io.Serializable])

  (ancestors user.BookInterface)

  (.toString (reify user.BookInterface
     Object (toString [_] "A marker interface for books.")))

  (gen-class 'testgenclass)

  (binding [*compile-files* true]
    (gen-class 'testgenclass))

  (spit "src/hello_clojure/genclass/bookgenclass.clj"
        "
(ns hello-clojure.genclass.bookgenclass)
(gen-class :name hello-clojure.genclass.bookgenclass.GenClass
           :main true)
(defn -main [& args]
  (println \"Hello from Java\"))")

  (binding [*compile-path* "target"]
    (compile 'hello-clojure.genclass.bookgenclass))

  (import 'hello-clojure.genclass.bookgenclass.GenClass)
  (GenClass/main (make-array String 0))


;; `deftype` and `definterface`

  (deftype Point [x y])
  (def p (new Point 1 2))
  (def p (Point. 1 2))
  (def p (->Point 1 2))
  (.x p)
  (.-x p)
  (. p y)

  (defn- distance [x1 y1 x2 y2]
    (Math/sqrt
     (+ (Math/pow (- x1 x2) 2)
        (Math/pow (- y1 y2) 2))))

  (deftype Point [x y]
    Comparable
    (compareTo [p1 p2]
      (compare (distance (.x p1) (.y p1) 0 0)
               (distance (.x p2) (.y p2) 0 0)))
    Object
    (toString [this]
      (str "(" x "," y ")")))

  (sort [(->Point 5 2) (->Point 2 4) (->Point 3 1)])

  (definterface IPerson
    (getName [])
    (setName [s])
    (getAge [])
    (setAge [n]))

  (deftype Person [^:unsynchronized-mutable name
                   ^:unsynchronized-mutable age]
    IPerson
    (getName [this] name)
    (setName [this s] (set! name s))
    (getAge [this] age)
    (setAge [this n] (set! age n))
    Object
    (toString [this]
      (format "Person[name=%s, age=%s]" name age)))

  (def p (->Person "Natasha" "823"))
  (.setAge p 23)

  (spit "src/hello_clojure/deftype/bookdeftype.clj"
        "(ns hello-clojure.deftype.bookdeftype)
           (defn bar [] \"bar\")
           (defprotocol P (foo [p]))
           (deftype Foo [] :load-ns true
             P (foo [this] (bar)))")

  (binding [*compile-path* "development/out"]
    (compile 'hello.clojure.deftype.bookdeftype))

  (import 'hello.clojure.deftype.bookdeftype.Foo)
  (def p (Foo.))
  (.foo p)


  ;; `proxy`

  (def ^Runnable r
    (proxy [Runnable] []
      (run [] (println (rand)))))

  (.run r)

  (definterface Concatenable
    (^java.io.File concat [^java.io.File f]))

  (defn cfile [fname]
    (proxy [File Concatenable] [^String fname]
      (concat [^File f]
        (spit (.getPath f) (slurp this) :append true)
        f)))

  (def ^Concatenable etchosts (cfile "/etc/hosts"))
  (def ^Concatenable f2 (cfile "/tmp/temp2.txt"))

  (spit f2 "# need to create this file\n")
  (.concat etchosts f2)
  (slurp "/tmp/temp2.txt")

  (update-proxy
   etchosts
   {"concat"
    #(let [^File f1 %1 ^File f2 %2]
       (.createNewFile ^File f2)
       (spit (.getPath f2) (slurp f1) :append true)
       f2)})

  (-> etchosts
      (.concat (cfile "/tmp/temp3.txt"))
      (.concat (cfile "/tmp/hosts-copy.txt")))

  (slurp "/tmp/hosts-copy.txt")

  (def DocumentException (get-proxy-class Exception IDeref))
  (def SyntaxException (get-proxy-class Exception IDeref))
  (def FormattingException (get-proxy-class Exception IDeref))

  (defn bail
    ([ex s]
     (throw
      (-> ex
          (construct-proxy s)
          (init-proxy
           {"deref" (fn [this] (str "Cause: " s))}))))
    ([ex s ^Exception e]
     (throw
      (-> ex
          (construct-proxy s e)
          (init-proxy
           {"deref" (fn [this] (str "Root: " (.getMessage e)))})))))

  (defn verify-age [^String s]
    (try
      (Integer/valueOf s)
      (catch Exception e
        (bail SyntaxException "Age is not a number" e))))

  (try
    (let [age "AA"]
      (verify-age age))
    (catch Exception e @e))


  (def m (doto (HashMap.)
           (.put :a "a")
           (.put :b "b")
           (.put :c "c")))

  (defn stringify-key [m k v]
    (assoc m (str k) v))

  (reduce-kv stringify-key {} m)

  (reduce-kv stringify-key {}
             (reify IKVReduce
               (kv-reduce [this f init]
                 (reduce-kv f init (into {} m)))))

  ;; `defrecord`

  (defrecord Point [x y])

  (def p (Point. 1 2))
  (.x p)
  (:x p)

  (def p (map->Point {:x 1 :y 2}))
  (.x p)
  (:x p)

  (defn- euclidean-distance [x1 y1 x2 y2]
    (Math/sqrt
     (+ (Math/pow (- x1 x2) 2)
        (Math/pow (- y1 y2) 2))))

  (defrecord Point [x y]
    Object
    (toString [this]
      (format "%s[%s,%s]" (type this) x y))
    Comparable
    (compareTo [p1 p2]
      (compare (euclidean-distance (.x p1) (.y p1) 0 0)
               (euclidean-distance (.x p2) (.y p2) 0 0))))

  (sort [(->Point 5 2) (->Point 2 4) (->Point 3 1)])

  (type (->Point 1 2))
  (str (->Point 1 2))


  ;; protocols

  (defprotocol Fooable
    (foo [this])
    (bar [this]))

  (type Fooable)
  (pprint Fooable)
  (pprint (vec (.getDeclaredMethods hello_clojure.core.Fooable)))

  (extend java.lang.String
    Fooable
    {:foo #(.toUpperCase %)})

  (foo "arg")

  (foo (with-meta #{}
         {'foo (fn [this] "on numbers")}))


  ;; `extend`, `extend-type` and `extend-protocol`

  (defprotocol Reflect
    (declared-methods [this]))

  (extend java.lang.Object
    Reflect
    {:declared-methods
     (fn [this]
       (map
        (comp #(s/replace % #"clojure\.lang\." "cl.")
              #(s/replace % #"java\.lang\." "jl."))
        (.getDeclaredMethods (class this))))})

  (pprint (declared-methods (atom nil)))


  (defprotocol IFace
    (foo [this])
    (bar [this])
    (baz [this]))

  (def AFace
    {:foo (fn [this] (str "AFace::foo"))
     :bar (fn [this] (str "AFace::bar"))})

  (defrecord MyFace [])

  (extend MyFace
    IFace
    (assoc AFace :foo (fn [this] (str "MyFace::foo"))))

  (foo (->MyFace))
  (bar (->MyFace))
  (baz (->MyFace))

  (def my-face (->MyFace))
  (foo my-face)

  (extend MyFace
    IFace
    (assoc AFace :foo (fn [this] (str "new"))
           :baz (fn [this] (str "baz"))))

  (foo my-face)
  (baz my-face)


  (defprotocol INode (value [_]))
  (defprotocol IBranch (left [_]) (right [_]))
  (defprotocol ILeaf (compute [_]))

  (extend INode IBranch)
  (extend INode ILeaf)

  (defrecord Branch [id left right]
    INode (value [_] (str "Branch::" id))
    IBranch (left [_] left) (right [_] right))

  (defrecord Leaf [id]
    INode (value [_] (str "Leaf::" id))
    ILeaf (compute [_] (str "computed:" id)))

  (def tree
    (->Branch 1
              (->Branch :A (->Leaf 4) (->Leaf 5))
              (->Branch :B (->Leaf 6) (->Leaf 7))))

  (defn traverse
    ([tree]
     (traverse [] tree))
    ([acc tree]
     (let [acc (conj acc (value tree))]
       (if (satisfies? IBranch tree)
         (into
          (traverse acc (left tree))
          (traverse acc (right tree)))
         (conj acc (compute tree))))))

  (traverse tree)


  (extend-type MyFace
    IFace
    (bar [this] (str "MyFace::bar")))

  (bar my-face)


  (defprotocol Money
    (as-currency [n]))

  (extend-protocol Money
    Integer
    (as-currency [n] (format "%s$" n))
    clojure.lang.Ratio
    (as-currency [n]
      (format "%s$ and %sc" (numerator n) (denominator n))))

  (extenders Money)
  (extends? Money Integer)

  (as-currency (int 10))
  (as-currency (/ 1 20))


  ;; `derive` and `make-hierarchy`

  (defn custom-hierarchy [& derivations]
    (reduce (fn [h [child parent]] (derive h child parent))
            (make-hierarchy)
            derivations))

  (def h (custom-hierarchy
          [:clerk :person]
          ['owner 'person]
          [String :person]))

  (isa? h 'owner 'person)
  (isa? h :clerk :person)
  (isa? h String :person)


  (def h (custom-hierarchy
          [:unix :os]
          [:bsd :unix]
          [:mac :bsd]))

  (isa? h :mac :unix)
  (isa? h :bsd :os)
  (isa? h :windows :os)


  (def h (custom-hierarchy
          [:unix :os] [:windows :os] [:os2 :os]
          [:redhat :linux] [:debian :linux]
          [:linux :os] [:linux :unix] [:bsd :unix]
          [:mac :bsd]))

  (descendants h :unix)
  (ancestors h :mac)


  (def h (custom-hierarchy
          [:unix :os]
          [:windows :unix]
          [:mac :unix]))

  (isa? h :windows :unix)

  (def h (underive h :windows :unix))

  (isa? h :windows :unix)


  (def h (custom-hierarchy
          [:clerk :person]
          [:owner :person]
          [:unix :os]
          [:bsd :unix]
          [:mac :bsd]))

  (isa? h [:mac :owner] [:unix :person])


  (ancestors String)
  (descendants h :person)

  (isa? h :owner :owner)
  (isa? :owner :owner)


  ;; `defmulti` and `defmethod`

  (def total-payments
    {:op 'times
     :expr
     [[:loan 150000]
      {:op 'pow
       :expr
       [{:op 'plus
         :expr
         [[:incr 1]
          {:op   'divide
           :expr [[:rate 3.16]
                  [:decimals 100]
                  [:months 12]]}]}
        {:op   'times
         :expr [[:months 12] [:years 10]]}]}]})

  (def ops
    {'plus   +
     'times  *
     'divide /
     'pow    #(Math/pow %1 %2)})

  (defmulti calculate
    (fn [form] (:op form)))

  (defmethod calculate 'plus
    [{:keys [op expr]}]
    (apply (ops op) (map calculate expr)))

  (defmethod calculate 'times
    [{:keys [op expr]}]
    (apply (ops op) (map calculate expr)))

  (defmethod calculate 'divide
    [{:keys [op expr]}]
    (apply (ops op) (map calculate expr)))

  (defmethod calculate 'pow
    [{[x y] :expr}]
    (Math/pow (calculate x) (calculate y)))

  (defmethod calculate nil
    [[descr number]]
    number)

  (defmethod calculate :default [form]
    (throw (RuntimeException. (str "Don't know how to calculate" form))))

  (calculate total-payments)


  (def ops
    {'plus   [+ :varargs]
     'times  [* :varargs]
     'divide [/ :varargs]
     'pow    [#(Math/pow %1 %2) :twoargs]})

  (defn- add-ops [hierarchy ops]
    (reduce
     (fn [h [op [f kind]]] (derive h op kind))
     hierarchy
     ops))

  (def hierarchy
    (add-ops (make-hierarchy) ops))

  (defn resolve-op [ops op]
    (first (ops op)))

  (do
    ;; defmulti can't be simply redefined
    (def calculate nil)
    (defmulti calculate
      (fn [{op :op}]
        (if op
          (println (format "op: %s (is %s)"
                           op (first (ancestors hierarchy op))))
          (println "a value"))
        op)
      :hierarchy #'hierarchy))

  (defmethod calculate :varargs
    [{:keys [op expr]}]
    (apply (resolve-op ops op) (map calculate expr)))

  (defmethod calculate :onearg
    [{op :op [x] :expr}]
    ((resolve-op ops op) (calculate x)))

  (defmethod calculate :twoargs
    [{op :op [x y] :expr}]
    ((resolve-op ops op) (calculate x) (calculate y)))

  (defmethod calculate nil
    [[_ number]]
    number)

  (defmethod calculate :default
    [form]
    (throw
     (RuntimeException.
      (str "Don't know how to calculate " form))))

  (calculate total-payments)

  (descendants hierarchy :varargs)
  (descendants hierarchy :twoargs)
  (descendants hierarchy :onearg)

  (first (ancestors hierarchy 'plus))

  (defn sound-speed-by-temp [temp]
    {:op 'with-mapping
     :expr
     [{'inc  [inc :onearg]
       'sqrt [(fn [x] (Math/sqrt x)) :onearg]}
      {:op 'times
       :expr
       [[:mph 738.189]
        {:op 'sqrt
         :expr
         [{:op 'inc
           :expr
           [{:op   'divide
             :expr [[:celsius temp]
                    [:zero 273.15]]}]}]}]}]})

  (defmethod calculate :default
    [{op :op [ops forms] :expr :as form}]
    (if (= 'with-mapping op)
      (do
        (alter-var-root #'hierarchy add-ops ops)
        (alter-var-root #'ops into ops)
        (calculate forms))
      (throw (RuntimeException. (str "Don't know how to calculate " form)))))

  (calculate (sound-speed-by-temp -60))

  (- (calculate (sound-speed-by-temp -60))
     (calculate (sound-speed-by-temp 20)))


  (pprint (methods calculate))
  (get-method calculate :varargs)
  (remove-method calculate :onearg)


  (defmulti edges
    "Retrieves first and last from a collection" type)

  (defmethod edges java.lang.Iterable [x]
    ((juxt first last) (seq x)))

  (defmethod edges clojure.lang.IPersistentList [x]
    ((juxt first last) (seq x)))

  (edges (list 1 2 3))

  (prefer-method edges clojure.lang.IPersistentList java.lang.Iterable)

  (edges (list 1 2 3))

  (pprint (prefers edges))
  (pprint (prefers print-dup))


  (defmulti recursive type)

  (defmethod recursive Long
    recursive-impl [cnt]
    (if (> cnt 0)
      (do (println cnt)
          (recursive-impl (dec cnt)))
      cnt))

  (recursive 10)


  (defmulti throwing identity)

  (defmethod throwing :default
    throwing-impl [x]
    (throw (RuntimeException. (str "Problems with" x))))

  (throwing (symbol " this fn" ))

)


;; vars and namespaces

(comment

  ;; `def`, `declare`, `intern` and `defonce`

  (+ 1 1)
  ((var clojure.core/+) 1 1)
  ((deref (var +)) 1 1)

  (ns myns)

  (type (def mydef "thedef"))

  mydef

  (identical? (var mydef)
              ((ns-map 'hello-clojure.core) 'mydef))

  (meta (var mydef))

  (def unbound-var)
  unbound-var


  (declare state-one)

  (def state-zero
    #(if (= \0 (first %))
       (state-one (next %))
       (if (nil? %) true false)))

  (def state-one ;
    #(if (= \1 (first %))
       (state-zero (next %))
       (if (nil? %) true false)))

  (state-zero "0100100001")
  (state-zero "0101010101")


  *ns*
  (create-ns 'ext)
  (intern 'ext 'ext-var 1)
  ((ns-map 'ext) 'ext-var)
  (intern 'yet-to-exist 'a 1)


  (def definitions
    {'ns1 [['a1 1] ['b1 2]]
     'ns2 [['a2 2] ['b2 2]]})

  (defn defns [definitions]
    (for [[ns defs] definitions
          [name body] defs]
      (do
        (create-ns ns)
        (intern ns name body))))

  (defns definitions)


  (def redefine "1")
  (defonce dont-redefine "1")
  (def redefine "2")
  (defonce dont-redefine "2")
  redefine
  dont-redefine

  (def ^:const ssd "dds")


  ;; `var`, `find-var` and `resolve`

  (var a)
  (def a 1)
  (var a)

  (var test-var/a)
  (create-ns 'test-var)
  (intern 'test-var 'a 1)
  (var test-var/a)

  (= (var a) (var test-var/a))


  (var clojure.core/+)
  #'clojure.core/+
  (identical? (var clojure.core/+) #'clojure.core/+)


  (find-var 'user/test-find-var)
  (find-var 'test-find-var)
  (find-var 'hello-clojure.core/a)


  (resolve 'Exception)
  (resolve (symbol "[I"))


  (defn replace-var [name value]
    (let [protected #{'system}]
      (when (resolve protected name)
        (intern *ns* name value))))

  (def mydef 1)
  (def system :dont-change-me)
  (replace-var 'x 2)

  (replace-var 'mydef 2)
  mydef

  (replace-var 'system 2)
  system


  (dir-fn 'clojure.set)
  (dir clojure.set)


  (def ^:dynamic *dvar*)
  ((ns-map 'user) '*dvar*)
  (bound? #'*dvar*)
  (thread-bound? #'*dvar*)

  (binding [*dvar* 1]
    [(bound? #'*dvar*)
     (thread-bound? #'*dvar*)])

  (def avar)
  (bound? #'avar)
  (thread-bound? #'avar)
  (intern *ns* 'avar 1)
  (bound? #'avar)
  (thread-bound? #'avar)


  (def a 1)
  (def b 2)
  (def c 3)
  (bound? #'a #'b #'c)


  (def a-var {:a 1})
  (alter-var-root
   #'a-var
   update-in [:a] inc)

  (def a-var 1)
  (future
    (alter-var-root
     #'a-var
     (fn [old]
       (Thread/sleep 10000)
       (inc old))))
  ;; blocking call for 10 seconds
  a-var


  (definline timespi [x] `(* ~x 3.14))
  (alter-var-root #'timespi (fn [_] (constantly 1)))
  (timespi 10)

  (alter-meta! #'timespi dissoc :inline-arities :inline)
  (timespi 10)


  (defn fetch-title [url]
    (let [input (slurp url)]
      (last
       (re-find #"Title: (.*)\." input))))

  (def sample-article "Some Title: Salary increases announced.")

  (with-redefs [slurp (constantly sample-article)]
    (= "Salary increases announced" (fetch-title "url")))

  (with-redefs-fn {#'slurp (constantly sample-article)}
    #(= "Salary increases announced" (fetch-title "url")))


  ;; not thread-safe
  (defn x [] 5)
  (defn y [] 9)
  (dotimes [i 10]
    (future (with-redefs [x #(rand)] (* (x) (y))))
    (future (with-redefs [y #(rand)] (* (x) (y)))))
  [(x) (y)]


  ;; thread-safe
  (defn ^:dynamic x [] 5)
  (defn ^:dynamic y [] 9)
  (dotimes [i 10]
    (future (binding [x #(rand)] (* (x) (y))))
    (future (binding [y #(rand)] (* (x) (y)))))
  [(x) (y)]


  ;; `binding`

  (def ^:dynamic *trace*)
  (defmacro trace! [msg & body]
    `(do
       (when (thread-bound? #'*trace*)
         (set! *trace* (conj *trace* ~msg)))
       ~@body))

  (defn params [query]
    (let [pairs (clojure.string/split query #"&")]
      (trace! (format "Handling params %s" pairs)
              (->> pairs
                   (map #(clojure.string/split % #"="))
                   (map #(apply hash-map %))
                   (apply merge)))))

  (defn handle-request [{:strs [op arg1 arg2]}]
    (let [op (resolve (symbol op))
          x (Integer. arg1)
          y (Integer. arg2)]
      (trace!
       (format "Handling request %s %s %s" op x y)
       (op x y))))

  (binding [*trace* []]
    (let [query "op=+&arg1=1&arg2=2"
          res (handle-request (params query))]
      (pprint *trace*)
      res))


  (def ^:dynamic *debug*)

  (defn debug [msg]
    (when (and (thread-bound? #'*debug*) *debug*)
      (println "Debugging..." msg)))

  (binding [*debug* true]
    (.start (Thread. #(debug "from a thread."))))

  (binding [*debug* true]
    (.start (Thread. (bound-fn* #(debug "from a thread.")))))


  ;; `with-local-vars`

  (defn ++ [v]
    (var-set v (inc (var-get v))))

  (defn count-even [xs]
    (with-local-vars [a 0]
      (doseq [x xs]
        (when (zero? (rem x 2))
          (++ a)))
      @a))

  (count-even (range 10))


  ;; `ns`, `in-ns`, `create-ns` and `remove-ns`

  (def a 1)
  (ns-name (.ns #'a))


  ns1/v1
  (contains? (set (map ns-name (all-ns))) 'ns1)
  (create-ns 'ns1)
  (intern 'ns1 'v1 "now it's working")
  (contains? (ns-map 'ns1) 'v1)
  ns1/v1
  (contains? (set (map ns-name (all-ns))) 'ns1)

  (remove-ns 'ns1)
   ns1/v1

   (create-ns 'disappear)
   (intern 'disappear 'my-var 0)
   (refer 'disappear :only ['my-var])
   my-var

   (remove-ns 'disappear)
   (.ns #'my-var)

   (create-ns 'disappear)
   (intern 'disappear 'my-var 1)
   my-var
   @#'disappear/my-var


   ;; `alias`, `ns-aliases` and `ns-unalias`

   (create-ns 'com.web.tired-of-typing-this.myns)
   (ns-aliases 'com.web.tired-of-typing-this.myns)

   (intern 'com.web.tired-of-typing-this.myns 'myvar 0)
   com.web.tired-of-typing-this.myns/myvar

   (alias 'myns 'com.web.tired-of-typing-this.myns)
   (ns-aliases *ns*)
   myns/myvar

   (ns-aliases 'myns)

   ('s (ns-aliases *ns*))
   (keys (ns-aliases *ns*))

   (ns-unalias *ns* 'myns)
   (keys (ns-aliases *ns*))


   ;; `ns-map` and `ns-unmap`

   (pprint (ns-map (create-ns 'newns)))

   (distinct (map #(map type %) (ns-map 'myns)))

   (intern 'newns 'a 12)
   #'newns/a
   newns/a
   (ns-unmap 'newns 'a)
   #'newns/a
   newns/a


   ;; `ns-publics`, `ns-interns`, `ns-imports`

   (count (ns-publics *ns*))
   (count (ns-interns *ns*))
   (-> (diff (ns-publics *ns*)
             (ns-interns *ns*))
       (nth 1))
   (count (ns-imports *ns*))
   (count (ns-aliases *ns*))
   (count (ns-map *ns*))


   ;; `refer`, `refer-clojure`, `require`, `loaded-libs`, `use`, `import`

   (binding [*ns* (the-ns 'newns)]
     (refer 'clojure.data
            :only ['diff]
            :rename {'diff 'biff}))

   ('biff (ns-map 'newns))


   (binding [*ns* (the-ns 'newns)]
     (require '[clojure.data.xml :as x :refer [element?]]
              :verbose
              :reload))

   (contains? (set (map ns-name (all-ns))) 'clojure.data.xml)
   (ns-aliases 'newns)
   ('element? (ns-map 'newns))


   (binding [*ns* (the-ns 'newns)]
     (use '[clojure.java.io
            :only [reader file]
            :rename {reader r}]
          :verbose
          :reload-all))

   (ns-aliases 'newns)
   ('r (ns-map 'newns))


   (binding [*ns* (the-ns 'newns)]
     (import '[java.util ArrayList HashMap]))

   ('ArrayList (ns-imports 'newns))


   (pprint (loaded-libs))

   (def nss (set (map ns-name (all-ns))))
   (pprint
    (second
     (diff (loaded-libs) nss)))


   (ns-name (first (all-ns)))

   (find-ns 'clojure.edn)
   (find-ns 'newns)
   (find-ns 'no-ns)

   (the-ns 'notavail)
   (the-ns 'clojure.edn)
   (the-ns *ns*)

   (ns-name *ns*)

   (namespace :notcreateyet/a)
   (namespace ::a)
   (namespace 'alsosymbols/s)

   ;; `meta`, `with-meta`, `vary-meta`, `alter-meta!` and `reset-meta!`

   (pprint (meta #'+))
   (meta 1)

   (def v (with-meta [1 2 3] {:initial-count 3}))
   (meta (conj v 3 4 5))
   (meta (with-meta
           (with-meta  [1 2 3] {:a 1 :b 2}) {:a 2}))
   (meta (into [] v))
   (with-meta (atom 0) {:not "supported"})
   (meta (atom 0))


   (def v (with-meta [1 2 3]
            {:initial-count 3 :last-modified #inst "1985-04-12"}))
   (meta v)
   (def v (vary-meta (conj v 4)
                     assoc :last-modified #inst "1985-04-13"))
   (meta v)


   (def counter
     (atom 0 :meta {:last-modified #inst "1985-04-12"}))
   (meta counter)
   (alter-meta!
    (do (swap! counter inc) counter)
    assoc :last-modified (Date.))
   (meta counter)
   @counter

   (meta *ns*)
   (reset-meta! *ns* {:doc "A namespace"})
   (meta *ns*)

)


;; evaluation

(comment

  ;; `read`

  (instance? java.io.PushbackReader *in*)

  (def output (read))
  output
  (type output)


  (def example
    "#?(:clj (System/currentTimeMillis)
        :cljs (js/Console :log)
        :cljr (|Dictionary<Int32,String>|.)
        :default <anything you want>)")

  (defn reader-from [s]
    (-> (java.io.StringReader. s)
        (clojure.lang.LineNumberingPushbackReader.)))

  (read (reader-from example))
  (read {:read-cond :allow} (reader-from example))
  (read {:read-cond :preserve} (reader-from example))

  (def example
    "#?(:cljs :cljs :my :my :default <missing>)")

  (read {:read-cond :allow} (reader-from example))
  (read {:read-cond :allow :features #{:my}} (reader-from example))

  (read (reader-from ";; a comment"))
  (read {:eof nil} (reader-from ";; a comment"))
  (read (reader-from ";; a comment") false :eof)


  (read (reader-from "#=(+ 1 2)"))
  (read (reader-from "(java.lang.System/exit 0)"))
  ;; WARNING: the JVM will exit.
  (read (reader-from "#=(java.lang.System/exit 0)"))

  (binding [*read-eval* false]
    (read (reader-from "#=(java.lang.System/exit 0)")))

  (binding [*read-eval* :unknown]
    (read (reader-from "(+ 1 2)")))

  default-data-readers


  ;; `read-string`

  (read-string "(+ 1 2)")
  (read-string "#=(+ 1 2)")
  (read-string {:eof "nothing to read"} "")

  (binding [*data-readers* {'point identity}]
    (read-string "#point [1 2]"))

  (binding [*data-readers* {'uuid (constantly 'UUID)}]
    (read-string "#uuid \"374c8c4-fd89-4f1b-a11f-42e334ccf5ce\""))


  (reader-conditional '(:clj :code) false)
  (reader-conditional '(:clj [1 2 3]) true)

  (read-string {:read-cond :allow} "(list #?(:clj [1 2 3]))")
  (read-string {:read-cond :allow} "(list #?@(:clj [1 2 3]))")

  (def parse (read-string {:read-cond :preserve} "#?(:clj [1 2 3])"))
  (reader-conditional? parse)
  (:form parse)
  (:splicing? parse)


  ;; `eval`

  (eval [+ 1 2])
  (eval '(+ 1 2))
  (eval '(do (println "eval-ed") (+ 1 2)))

  (def rules
    "If the light is red, you should stop
     If the light is green, you can cross
     If the light is orange, it depends")

  (defmacro If [light & args]
    (let [[_ _ op v & action] args]
      `(when (= '~light '~v) '~action)))

  (defn parenthesize [s]
    (->> s
         s/split-lines
         (remove empty?)
         (map (comp
               #(str "(" % ")")
               s/trim))))

  (defn traffic-light [color rules]
    (->> rules
         parenthesize
         (map read-string)
         (map #(list* (first %) color (rest %)))
         (some eval)))

  (traffic-light 'red rules)
  (traffic-light 'green rules)
  (traffic-light 'orange rules)


  ;; `test` and `assert`

  (defn sqrt
    {:test
     #(when-not (== (sqrt 4) 2.)
        (throw (RuntimeException. "sqrt(4) should be 2")))}
    [x]
    (loop [guess 1.]
      (if (> (Math/abs (- (* guess guess) x)) 1e-8)
        (recur (/ (+ (/ x guess) guess) 2.))
        guess)))

  (test #'sqrt)
  (test #'+)

  (assert (= 1 (+ 3 3)) "It should be 6")

  (defn sqrt
    {:test #(assert (== (sqrt 4) 2.) "sqrt(4) should be 2")}
    [x]
    (loop [guess 1.]
      (if (> (Math/abs (- (* guess guess) x)) 1e-16)
        (recur (/ (+ (/ x guess) guess) 2.))
        guess)))

  (sqrt 4)
  (test #'sqrt)


  ;; `load`

  (load "/clojure/set")
  (clojure.set/union #{1 2 3} #{2 3 5})

  (binding [*ns* 'clojure.set
            clojure.core/*loading-verbosely* true]
    (load "zip"))


  ;; `load-file`

  (spit "/tmp/source.clj"
        "(ns ns1)
         (def a 1)
         (def b 2)
         (println \"a + b =\" (+ a b))
         (+ a b)")

  (load-file "/tmp/source.clj")


  ;; `load-string`

  (def code "(do (def a 1)\n(def b 2)\n(def c 3))")
  (meta (load-string code))
  (:line (meta #'c))

  (eval (read-string code))
  (:line (meta #'c))


  ;; `load-reader`

  (let [rdr (-> (java.io.StringReader. "(* 12 4)")
                (clojure.lang.LineNumberingPushbackReader.))]
    (load-reader rdr))


  ;; `compile`

  (spit "src/hello_clojure/source.clj"
        "
(ns hello-clojure.source)
(defn plus [x y] (+ x y))")

  (binding [*compile-path* "target"]
    (compile 'hello-clojure.source))


  ;; `clojure.edn/read` and `clojure.edn/read-string`

  (read-string "@#'+")
  (edn/read-string "@#'+")

  (edn/read-string
   "#point [1 2]")

  (edn/read-string
   {:readers {'point identity}}
   "#point [1 2]")

  (edn/read-string
   {:readers {'inst (constantly "override")}}
   "#inst \"2017-08-23T10:22:22.000-00:00\"")

  (edn/read-string
   {:default #(format "[Tag '%s', Value %s]" %1 %2)}
   "[\"There is no tag for \" #point [1 2] \"or\" #line [[1 2] [3 4]]]")



  (binding [*data-readers* {'point identity}]
    (edn/read-string "#point [1 2]"))


  (tagged-literal 'point [1 2])
  (:tag (tagged-literal 'point [1 2]))
  (:form (tagged-literal 'point [1 2]))
  (type (tagged-literal 'point [1 2]))
  (tagged-literal? (tagged-literal 'tag :form))

  (edn/read-string
   {:default tagged-literal}
   "[\"There is no tag for \" #point [1 2] \"or\" #line [[1 2] [3 4]]]")

  (binding [*default-data-reader-fn* tagged-literal]
    (read-string
     "[\"There is no tag for \" #point [1 2] \"or\" #line [[1 2] [3 4]]]"))

  (def date (edn/read-string "#inst \"2017-08-23T10:22:22.000-00:00\""))
  (= date (edn/read-string (pr-str date)))

  (edn/read-string {:readers {'point identity}} "#point [1 2]")

)


;; formatting and printing

(comment

  ;; `format`, `printf` and `cl-format`

  (format "%3d" 1)
  (format "%03d" 1)
  (format "%.2f" 10.3456)
  (format "%10s", "Clojure")
  (format "%-10s", "Clojure")
  (format "%-11.11s" "truncatefixedsize")
  (format "%-11.11s" "size")
  (format "%tT" (java.util.Calendar/getInstance))

  (cl-format nil "~:d" 1000000)
  (cl-format nil "~b" 10)
  (cl-format nil "Anno Domini ~@r" 2023)
  (cl-format nil "~r" 158)
  (cl-format nil "~:r and ~:r" 1 2)
  (cl-format nil "~r banana~:p" 1)
  (cl-format nil "~r banana~:p" 2)

  (def fmt "~#[nope~;~a~;~a and ~a~:;~a, ~a~]~#[~; and ~a~:;, ~a, etc~].")
  (apply cl-format nil fmt [1 2])
  (apply cl-format nil fmt [1 2 3])
  (apply cl-format nil fmt [1 2 3 4])

  (def fmt "I see ~[no~:;~:*~r~] fish~:*~[es~;~:;es~].")
  (cl-format nil fmt 0)
  (cl-format nil fmt 1)
  (cl-format nil fmt 100)

  (def paragraph
    ["This" "sentence" "is" "too" "long" "for" "a" "small" "screen"
     "and" "should" "appear" "in" "multiple" "lines" "no" "longer"
     "than" "20" "characters" "each" "."])
  (println (cl-format nil "~{~<~%~1,20:;~A~> ~}" paragraph))


  ;; `pr`, `prn`, `pr-str`, `prn-str`, `print`, `println`, `print-str`,
  ;; `println-str`: pr* for machines (reader), print* for humanses

  (pr "a" 'a \a)
  (print "a" 'a \a)

  (def java-map (HashMap. {:a "1" :b nil}))
  (prn java-map)
  (println java-map)

  (def data {:a [1 2 3]
             :b '(:a :b :c)
             :c {"a" 1 "b" 2}})
  (pr-str data)
  (prn-str data)
  (print-str data)
  (println-str data)

  (with-open [w (io/writer "/tmp/range.txt")]
    (binding [*out* w]
      (dorun
       (->> (range 100000)
            (partition 10)
            (map println)))))

  (first (line-seq (io/reader "/tmp/range.txt")))

  ;; `pprint`, `pp`, `write` and `print-table`

  (def data {:a ["red" "blue" "green"]
             :b '(:north :south :east :west)
             :c {"x-axis" 1 "y-axis" 2}})
  data
  (pp)
  (pprint data)

  (with-open [w (io/writer "/tmp/prettyrange.txt")]
    (pretty/write
     (for [x (range 10)] (range x)) :stream w))

  (def op-fn
    "(defn op [sel](condp = sel \"plus\" + \"minus\" - \"mult\"
     * \"div\" / \"rem\" rem \"quot\" quot))")

  (pprint (read-string op-fn))
  (pretty/with-pprint-dispatch
    pretty/code-dispatch
    (pprint (read-string op-fn)))

  (pretty/write 20/3 :base 2 :radix true)
  (pretty/write (range 100) :length 3)
  (pretty/write 'clojure.core/+ :suppress-namespaces true)
  (pretty/write (range 20) :right-margin 10)

  (pretty/print-table (list (zipmap (range 10) (range 100 110))))
  (pretty/print-table (repeat 4 (zipmap (range 10) (range 100 110))))


  ;; `print-method`, `print-dup` and `print-ctor`

  (deftype Point [x y])

  (pr (Point. 1 2))

  (defmethod print-method hello_clojure.core.Point
    [object writer]
    (let [class-name (.getName (class object))
          args (str (.x object) " " (.y object))]
      (.append writer (format "(%s. %s)" class-name args))))

  (def point (Point. 1 2))
  (def point-as-str (pr-str point))
  (def point-as-list (read-string point-as-str))
  (def back-to-point (eval point-as-list))
  [point-as-str :type (type point-as-str)]
  [point-as-list :type (type point-as-list)]
  [back-to-point :type (type back-to-point)]


  (binding [*print-dup* true]
    (pr-str {:a 1 :b 2}))

  (defmethod print-method hello_clojure.core.Point [object writer]
    (.append writer (format "[x=%s, y=%s]" (.x object) (.y object))))

  (pr-str (Point. 1 2))

  (defmethod print-dup hello_clojure.core.Point [object writer]
    (print-ctor
     object
     (fn print-args [object writer]
       (.append writer (str (.x object) " " (.y object))))
     writer))

  (binding [*print-dup* true] (pr-str (Point. 1 2)))

  (binding [*print-dup* true] (pr-str (map inc (range 3))))

  (binding [*print-dup* true]
    (read-string (pr-str (Point. 1 2))))


  ;; `slurp` and `spit`

  (spit "/tmp/test.txt" "Look, I can write a file!")
  (slurp "/tmp/test.txt")

  (def book (slurp "https://www.gutenberg.org/files/2600/2600-0.txt"))
  (reduce str (take 22 book))

  (slurp "/etc/hosts" :encoding "UTF-16")
  (spit "/tmp/test.txt" "Something." :append true)
  (slurp "/tmp/test.txt")


  (spit (FileWriter. "/tmp/xxx") "txt")
  (slurp (FileReader. "/tmp/xxx"))

  (spit (ByteArrayOutputStream.) (byte-array [1 2 3]))
  (slurp (ByteArrayInputStream. (byte-array [1 2 3])))

  (spit (File. "/tmp/xxx") "txt!")
  (slurp (File. "/tmp/xxx"))

  (spit (URL. "file:///tmp/xxx") "url")
  (slurp (URL. "file:///tmp/xxx"))
  (slurp (URL. "http://manning.com"))

  (def f1 (future
            (with-open [server (ServerSocket. 3000)
                        socket (.accept server)
                        reader (io/reader socket)
                        writer (io/writer socket)]
              (let [req (.readLine reader)
                    res (str req "\n")]
                (.write writer res)
                (.flush writer)
                res))))

  (spit (Socket. "localhost" 3000) "msg\n")
  @f1

  (def f2 (future
            (with-open [server (ServerSocket. 3000)
                        socket (.accept server)
                        reader (io/reader socket)
                        writer (io/writer socket)]
              (.write writer "HAYO!")
              (.flush writer)
              ::done)))

  (slurp (Socket. "localhost" 3000))
  @f2

)


;; strings and regular expressions

(comment

  ;; `str`

  (str "Should " "this " "be " "a " "single " "sentence?")
  (str :a 'b 1e8 (Object.) [1 2] {:a 1})
  (str (map inc (range 10)))
  (pr-str (map inc (range 10)))

  (apply str (range 10))
  (reduce str (interpose "," (range 10)))
  (apply str (interpose "," (range 10)))


  ;; `join`

  (s/join (list "Should " "this " "be " 1 \space 'sentence?))
  (s/join "," (range 10))


  ;; `replace`, `replace-first`, `re-quote-replacement`

  (def strng "Chat-room messages are up-to-date")
  (s/replace strng \- \space)
  (s/replace "Closure is a Lisp" "Closure" "Clojure")
  (s/replace "I'm a little unjure" \j \s)
  (s/replace "I'm a little unjure" "j" "s")
  (s/replace "I'm a little unjure" "j" \s)

  (def s "Why was 12 afraid of 14? Because 14 ate 18.")
  (s/replace s #"\d+" #(str (/ (Integer/valueOf %) 2)))

  (def s "Easter in 2038: 04/25/2038, Easter in 2285: 03/22/2285")
  (s/replace s #"(\d{2})/(\d{2})/(\d{4})" "$2/$1/$3")

  (def s "May 2018, June 2019")
  (s/replace s #"May|June" "10$")
  (s/replace s #"May|June" (s/re-quote-replacement "10$ in"))

  (def s "A drink here and a drink home.")
  (s/replace s #"drink|soda|water" "beer")
  (s/replace-first s #"drink|soda|water" "beer")


  ;; `subs`, `split` and `split-lines`

  (def s "The quick brown fox\njumps over the lazy dog")
  (subs s 20 30)
  (s/split s #"\s")
  (s/split-lines s)

  (def errors
    ["String index out of range: 34"
     "String index out of range: 48"
     "String index out of range: 3"])
  (map #(subs % 27) errors)

  (def errors
    ["String is out of bound: 34"
     "48 is not a valid index."
     "Position 3 is out of bound."])
  (map #(peek (s/split % #"\D+")) errors)


  (def ls (:out (sh "ls" "-Alh" "/etc/cups/ppd")))
  (def printers (s/split-lines ls))

  (last printers)

  (sequence
   (comp (map #(s/split % #"\s+"))
         (map last)
         (filter #(re-find #"ppd" %))
         (map #(s/replace % #".ppd.*" "")))
   printers)


  ;; `trim`, `triml`, `trimr`, `trim-newline`

  (def whitespace
    (map
     #(hash-map :int % :char (char %) :hex (format "%x" %))
     (filter (comp
              #(Character/isWhitespace %) char) (range 65536))))

  (count whitespace)

  (s/blank? "\t \n \u000b \f \r \u001c \u001d \u001e \u001f")
  (s/blank? "\u0020 \u1680 \u2000 \u2001 \u2002 \u2003")
  (s/blank? "\u2004 \u2005 \u2006 \u2008 \u2009")
  (s/blank? "\u200a \u2028 \u2029 \u205f \u3000")

  (s/trim "    *Look, no more spaces.*    ")
  (s/trim "\t1\t2n\n")

  (s/trimr "   *Spaces on the left are not removed with trimr.*   ")
  (s/triml "   *Spaces on the right are not removed with triml.*  ")

  (s/trim-newline "\n Only spaces and\n newline at the end.\n\r")


  ;; `escape`, `char-name-string`, `char-escape-string`

  (def link
    "Patterson, John: 'Once Upon a Time in the West'")

  (def link-escape
    {\, "_comma_"
     \space "__"
     \. "_dot_"
     \' "_quote_"
     \: "_colon_"
     \newline "_newline_"})

  (s/escape link link-escape)

  char-name-string
  char-escape-string

  (map #(char-name-string % %) "Hello all!\n")

  (def s "Type backslash-t '\t' followed by backslash-n '\n'")
  (println s)
  (println (s/escape s char-escape-string))


  ;; `lower-case`, `upper-case`, `capitalize`

  (-> some?
      source
      with-out-str
      s/upper-case
      println)

  (def primary-colors #{"red" "green" "blue"})
  (def book (slurp "https://www.gutenberg.org/files/2600/2600-0.txt"))

  (->> (s/split book #"\s+")
       (filter primary-colors)
       frequencies)

  (->> (s/split book #"\s+")
       (map s/lower-case)
       (filter primary-colors)
       frequencies)

  (def names
    ["john abercrombie"
     "Brad mehldau"
     "Cassandra Wilson"
     "andrew cormack"])
  (sequence
   (comp
    (mapcat #(s/split % #"\b"))
    (map s/capitalize)
    (partition-all 3)
    (map s/join))
   names)

  (map s/upper-case ['symbols :keywords 1e10 (Object.)])


  ;; `index-of`, `last-index-of`

  (s/index-of "Bonjure Clojure" \j)
  (s/last-index-of "Bonjure Clojure!" "ju")
  (s/index-of "Bonjure Clojure" "z")

  (s/index-of
   (doto (StringBuffer.)
     (.append "Bonjure")
     (.append \space)
     (.append "Clojure"))
   \j)


  ;; `blank?`, `ends-with?`, `starts-with?`, `includes?`

  (s/blank? " \t \n \f \r ")
  (s/blank? "\u000B \u001C \u001D \u001E \u001F")

  (s/starts-with? "Bonjure Clojure" "Bon")
  (s/starts-with? "Bonjure Clojure" "Clo")
  (s/starts-with? "" "")
  (s/starts-with? "Anything starts with nothing." "")
  (s/ends-with? "Bonjure Clojure" "ure")
  (s/ends-with? "Bonjure Clojure" "Bon")
  (s/ends-with? "" "")
  (s/ends-with? "Anything ends with nothing." "")

  (s/includes? "Bonjure Clojure" "e C")


  ;; `re-pattern`, `re-matcher`, `re-groups`, `re-seq`, `re-matches`, `re-find`

  (filter
   #(re-find #"-seq" (str (key %)))
   (ns-publics 'clojure.core))

  (def manning-contacts (slurp "https://www.manning.com/contact"))
  (count (set (map last (re-seq #"(http\S+\.com)" manning-contacts))))
)


;; mutation and side effects

(comment

  ;; `transient`, `persistent!`, `conj!`, `pop!`, `assoc!`, `dissoc!` and `disj!`

  (def v (transient []))
  (conj v 1)

  (def v (transient []))
  (def s (transient #{}))
  (def m (transient {}))
  ((conj! v 0) 0)
  ((conj! s 0) 0)
  ((assoc! m :a 0) :a)

  (def transient-map (transient {}))
  (def java-map (HashMap.))

  (dotimes [i 20]
    (assoc! transient-map i i)
    (.put java-map i i))

  (persistent! transient-map)
  (into {} java-map)

  (def transient-map (transient {}))
  (def m
    (reduce
     (fn [m k] (assoc! m k k))
     transient-map
     (range 20)))

  (persistent! m)


  ;; `doseq`, `dorun`, `run!`, `doall`, `do`

  (defn unchunked [n]
    (map #(do (print ".") %)
         (subvec (vec (range n)) 0 n)))

  (doseq [x (unchunked 10)
          :while (< x 5)] x)
  (dorun 5 (unchunked 10))
  (run! #(do (print "!") %) (unchunked 10))


  (defn get-lines [url] ;
    (with-open [r (io/reader url)]
      (doall (line-seq r))))

  (def lines (get-lines "https://gutenberg.org/files/50/50.txt"))
  (count lines)
  (nth lines 8)

  (do
    (println "hello") ;
    (+ 1 1))

  (if (some even? [1 2 3])
    (do
      (println "found some even number")
      (apply + [1 2 3]))
    (println "there was no even number."))


  ;; `volatile!`, `vreset!`, `vswap!` and `volatile?`

  (def v (volatile! 0))
  (volatile? v)
  (vswap! v inc)
  (vreset! v 0)


  (def ready (volatile! false))
  (def result (volatile! nil))

  (defn start-consumer []
    (future
      (while (not @ready)
        (Thread/yield))
      (println "Consumer getting result:" @result)))

  (defn start-producer []
    (future
      (vreset! result :done)
      (vreset! ready :done)))

  (start-consumer)
  (start-producer)


  ;; `set!`

  (def p (Point.))
  [(. p x) (. p y)]
  (set! (. p -x) 1)
  (set! (. p -y) 2)
  [(. p x) (. p y)]


  (def non-dynamic 1)
  (def ^:dynamic *dynamic* 1)
  (set! non-dynamic 2)
  (set! *dynamic* 2)

  (binding [*dynamic* 1]
    (set! *dynamic* 2))


  (fn [x] (.toString x))
  (set! *warn-on-reflection* true)
  (fn [x] (.toString x))


  (deftype Counter [^:unsynchronized-mutable cnt]
    clojure.lang.IFn
    (invoke [this] (set! cnt (inc cnt))))

  (def counter (->Counter 0))
  (counter)
  (counter)

)


;; Java interoperation

(comment

  ;; `.`, `..` and `doto`

  (. Thread sleep 1000)
  (. Math random)
  (. Math (random))
  (. Math -PI)
  (. Thread$State NEW)

  (Thread/sleep 1000)
  (Math/random)
  (Math/-PI)

  (. (java.awt.Point. 1 2) x)
  (.x (java.awt.Point. 1 2))
  (. (java.awt.Point. 1 2) (getX))
  (.-x (java.awt.Point. 1 2))

  (defmacro getter [object field] ;
    (let [getName# (symbol (str "get" field))]
      `(. ~object ~getName#)))

  (getter (java.awt.Point. 2 2) "X")

  (.. (Calendar$Builder.)
      (setCalendarType "iso8601")
      (setWeekDate 2019 4 (Calendar/SATURDAY))
      (build))

  (macroexpand
   '(.. (Calendar$Builder.)
        (setCalendarType "iso8601")
        (setWeekDate 2019 4 (Calendar/SATURDAY))
        (build)))

  (def l (ArrayList.))
  (doto ^ArrayList l
    (.add "fee")
    (.add "fi")
    (.add "fo")
    (.add "fum"))


  ;; `new`

  (def sb (new StringBuffer "init"))
  (.append ^StringBuffer sb " item")
  (str sb)

  (let [l (Long. 1)]
    (new BigDecimal l))

  (let [l 1]
    (new BigDecimal l))

  (StringBuffer. "init")


  ;; `try`, `catch`, `finally` and `throw`

  (throw (Throwable. ": there was a problem."))

  (try
    (println "Program running as expected")
    (throw (RuntimeException. "Got a problem."))
    (println "program never reaches this line")
    (catch Exception e
      (println "Could not run properly. Bailing out." e)
      "returning home"))

  (try
    (Socket. (InetAddress/getByName "localhost") 61817)
    (catch ConnectException ce
      (println "Could not connect. Retry." ce))
    (catch SocketException se
      (println "Communication error" se))
    (catch Exception e
      (println "Something weird happened." e)
      (throw e)))

  (defmacro with-reader [r file & body]
    `(let [~r (io/reader ~file)]
       (try
         ~@body
         (finally
           (.close ~r)))))

  (with-reader r "/etc/hosts"
    (last (line-seq r)))

  (try
    (/ 1 0)
    (catch Exception e "Returning from catch") ;
    (finally (println "Also executing finally")))


  ;; `ex-info` and `ex-data`

  (def ex
    (ex-info "Temperature drop!"
             {:time "10:29pm"
              :reason "Front door open."
              :mitigation #(println "close the door")}))
  (type ex)
  (throw ex)

  (try
    (/ 1 0)
    (catch Exception e
      (throw
       (ex-info "Don't do this."
                {:type "Math"
                 :recoverable? false} e))))

  (defn randomly-failing-operation []
    (throw
     (ex-info "Weak connection."
              {:type :connection
               :recoverable? (< 0.3 (rand))})))

  (defn main-program-loop []
    (try
      (println "Attempting operation...")
      (randomly-failing-operation)
      (catch Exception e
        (let [{:keys [type recoverable?]} (ex-data e)] ;
          (if (and (= :connection type) recoverable?)
            (main-program-loop)
            (ex-info "Not recoverable problem."
                     {:type :connection} e))))))

  (main-program-loop)

  (def error-data
    (try (throw (ex-info "inner" {:recoverable? false}))
         (catch Throwable t
           (try (throw (ex-info "outer" {:recoverable? false} t))
                (catch Throwable t
                  (Throwable->map t))))))

  (keys error-data)
  (:cause error-data)
  (:via error-data)


  ;; `bean`

  (def point (bean (Point. 2 4)))
  (keys point)
  [(:x point) (:y point)]


  ;; `reflect` and `type-reflect`

  (keys (reflect/reflect clojure.lang.APersistentMap))

  (:ancestors (reflect/reflect clojure.lang.APersistentMap :ancestors true))
  (count (:members
          (reflect/reflect clojure.lang.APersistentMap)))
  (count (:members
          (reflect/reflect clojure.lang.APersistentMap :ancestors true)))

  (deftype StubReflector []
    reflect/Reflector
    (do-reflect [this typeref]
      {:bases #{} :flags #{} :members #{}}))

  (reflect/reflect java.lang.Integer :reflector (StubReflector.))

  (keys (reflect/reflect ""))
  (keys (reflect/type-reflect String))


  ;; `make-array`

  (def a (make-array Boolean/TYPE 3))
  (vec a)
  (def b (make-array Boolean 3))
  (vec b)

  (def a (make-array Integer/TYPE 4 2))
  (mapv vec a)


  ;; `object-array` and other typed initializers

  (vec (object-array 3))
  (vec (char-array 3))
  (vec (double-array 3))

  (vec (float-array 3 1.0))
  (vec (int-array [1 2 3]))

  (vec (int-array [\a \b \c]))
  (vec (int-array [4294967296]))

  (vec (int-array 5 (range 10)))
  (vec (boolean-array 5 [true true]))


  ;; `to-array`, `into-array`, `to-array-2d`

  (type (to-array [1 2 3]))
  (type (into-array [1 2 3]))

  (def a (to-array-2d [[1 2] [3 4]]))
  (map type a)
  (mapv vec a)


  ;; `aget`, `aset`, `alength` and `aclone`

  (def a (into-array [:a :b :c]))
  (aget #^objects a 0)
  (aset #^objects a 0 :changed)
  (vec a)

  (def matrix (to-array-2d [[0 1 2] [3 4 5] [6 7 8]]))
  (aget matrix 1 1)
  (aset matrix 1 1 99)
  (mapv vec matrix)

  (defn transpose! [matrix]
    (dotimes [i (alength ^objects matrix)]
      (doseq [j (range (inc i) (alength ^objects matrix))]
        (let [copy (aget matrix i j)]
          (aset matrix i j (aget matrix j i))
          (aset matrix j i copy)))))

  (def matrix
    (into-array
     (map double-array
          [[1.0 2.0 3.0]
           [4.0 5.0 6.0]
           [7.0 8.0 9.0]])))

  (transpose! matrix)
  (mapv vec matrix)

  (defn transpose [matrix]
    (let [size (alength ^objects matrix)
          output (into-array (map aclone matrix))]
      (dotimes [i size] ;
        (dotimes [j size]
          (aset output j i (aget matrix i j))))
      output))

  (def matrix
    (into-array
     (map double-array
          [[1.0 2.0 3.0]
           [4.0 5.0 6.0]
           [7.0 8.0 9.0]])))

  (def transposed (transpose matrix))
  (mapv vec transposed)
  (alength ^objects transposed)


  ;; `amap` and `areduce`

  (def a1 (int-array (range 10)))
  (def a2 (amap ^ints a1 idx output 1))
  (vec a2)
  (vec a1)

  (def a1 (int-array (range 4)))

  (defn debug [idx output]
    (println "idx:" idx "output:" output) 9)

  (def a2 (amap ^ints a1 idx ^ints output (debug idx (vec ^ints output))))

  (defn ainc [a]
    (amap ^ints a idx _ (inc (aget ^ints a idx))))

  (vec (ainc (int-array (range 10))))

  (def a (int-array (range 10)))
  (areduce ^ints a idx acc 0 (+ acc (aget ^ints a idx)))


  ;; `set-int` and other types setters

  (aset ^ints a 0 9)
  (aset-int a 0 9)

  (def matrix
    (into-array
     (map int-array [[1 2 3] [4 5 6]])))

  (aset-int matrix 0 2 99)
  (mapv vec matrix)


  ;; `ints` and other typed array casting

  (defn asum-int [a1 a2]
    (let [a1 (ints a1) a2 (ints a2)
          a (aclone (if (> (alength a1) (alength a2)) a1 a2))]
      (amap a idx ret
            (aset a idx
                  (+ (aget a1 idx) (aget a2 idx))))))

  (vec (asum-int (int-array [1 2 3]) (int-array [4 5 6])))
)


;; the toolbox

(comment

  ;; `clojure.xml`

  (def document (xml/parse "https://www.theguardian.com/world/rss"))
  (keys document)
  (:attrs document)

  (def conforming
    "<?xml version='1.0' encoding='utf-8'?>
       <!DOCTYPE html SYSTEM 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
       <html xmlns='http://www.w3.org/1999/xhtml'>
       <article>Hello</article>
     </html>")

  (def xml
    (-> ^String conforming .getBytes io/input-stream xml/parse))


  (defn non-validating [s ch]
    (..
     (doto
         (SAXParserFactory/newInstance)
       (.setFeature
        "http://apache.org/xml/features/nonvalidating/load-external-dtd"
        false))
     (newSAXParser)
     (parse s ch)))

  (def xml
    (-> ^String conforming .getBytes io/input-stream (xml/parse non-validating)))

  (xml/emit xml)


  ;; `clojure.inspector`

  (i/inspect-tree {:a 1 :b 2 :c [1 2 3 {:d 4 :e 5 :f [6 7 8]}]})

  (def events [{:time "2017-05-04T13:08:57Z" :msg "msg1"}
               {:time "2017-05-04T13:09:52Z" :msg "msg2"}
               {:time "2017-05-04T13:11:03Z" :msg "msg3"}
               {:time "2017-05-04T23:13:10Z" :msg "msg4"}
               {:time "2017-05-04T23:13:23Z" :msg "msg5"}])

  (i/inspect-table events)


  ;; `clojure.repl`

  (def life 42)
  (repl/doc life)
  (alter-meta! #'life assoc :doc
               "Answer to the Ultimate Question of Life the Universe and Everything")
  (repl/doc life)
  (repl/doc repl/doc)
  (repl/doc clojure.repl)
  (repl/find-doc "xml")
  (repl/apropos "first")
  (repl/dir clojure.walk)
  (apply str (interpose "," (repl/dir-fn 'clojure.walk)))
  (repl/source unchecked-inc-int)
  (repl/source-fn 'not-empty)

  (/ 1 0)
  *e
  (repl/pst)

  (def ex (ex-info "Problem." {:status :surprise}))
  (repl/pst ex)
  (repl/pst ex 4)

  (def ex
    (ex-info "Problem." {:status :surprise}
             (try (/ 1 0)
                  (catch Exception e
                    (ex-info "What happened?" {:status :unkown} e)))))
  (repl/pst ex 3)

  (repl/pst (repl/root-cause ex) 3)


  (defn my-funct! [] (throw (ex-info "error" {})))
  (str my-funct!)
  (repl/demunge (str my-funct!))
  (repl/demunge "_BANG___")

  (def stack-trace (try (my-funct!) (catch Exception e (.getStackTrace e))))
  (nth stack-trace 2)
  (repl/stack-element-str (nth stack-trace 2))


  ;; `clojure.main`

  clojure.core.reducers/fold
  (main/load-script "@clojure/core/reducers.clj")
  clojure.core.reducers/fold

  (spit "/tmp/hello.exe"
        "(ns hello)
         (println \"Hello World!\")")

  (main/load-script "/tmp/hello.exe")

  (main/repl :init #(println "Welcome to a new REPL! Press ctrl+D to exit."))

  (def repl-options
    [:init
     #(require '[calculator :refer :all])
     :prompt #(printf "enter expression :> ")])

  (apply main/repl repl-options)


  (def repl-options
    [:prompt #(printf "enter expression :> ")
     :read
     (fn [request-prompt request-exit]
       (or ({:line-start request-prompt :stream-end request-exit}
            (main/skip-whitespace *in*))
           (re-find #"^(\d+)([\+\-\*\/])(\d+)$" (read-line))))
     :eval
     (fn [[_ x op y]]
       (({"+" + "-" - "*" * "/" /} op)
        (Integer. x) (Integer. y)))])

  (apply main/repl repl-options)


  ;; `clojure.java.browse`

  (browse-url "https://www.manning.com/books/clojure-the-essential-reference")

  (binding [*open-url-script* (atom "wget")]
    (browse-url "http://www.gutenberg.org/files/2600/2600-0.txt"))


  ;; `clojure.java.shell`

  (sh "ls" "/usr/share/dict")
  (sh "grep" "5" :in (apply str (interpose "\n" (range 50))))

  (def image-file "/usr/share/icons/Adwaita/512x512/devices/printer-network.png")
  (def cmd (sh "cat" image-file :out-enc :bytes))
  (count (:out cmd))

  (defn pipe [cmd1 & cmds]
    (reduce
     (fn [{out :out} cmd]
       (apply sh (conj cmd :in out)))
     (apply sh cmd1)
     cmds))

  (println
   (:out
    (pipe
     ["env"]
     ["grep" "-i" "gnome"])))

  (def env
    {"VAR1" "iTerm.app"
     "VAR2" "/bin/bash"
     "COMMAND_MODE" "Unix2003"})

  (println
   (:out
    (sh "env" :env env)))

  (println (:out (sh "pwd")))
  (println (:out (sh "pwd" :dir "/tmp")))

  (shell/with-sh-dir "/usr/share"
    (shell/with-sh-env {:debug "true"}
      [(sh "env") (sh "pwd")]))


  ;; `clojure.core.server`

  (def s (server/start-server {:name "repl1" :port 8787 :accept
          'clojure.core.server/repl}))

  (clojure.core.server/repl)


  (defn data-eval [form]
    (let [out-writer (StringWriter.)
          err-writer (StringWriter.)
          capture-streams (fn []
                            (.flush *out*)
                            (.flush *err*)
                            {:out (.toString out-writer)
                             :err (.toString err-writer)})]
      (binding [*out* (BufferedWriter. out-writer)
                *err* (BufferedWriter. err-writer)]
        (try
          (let [result (eval form)]
            (merge (capture-streams) {:result result}))
          (catch Throwable t
            (merge (capture-streams) {:exception (Throwable->map t)}))))))

  (defn data-repl [& kw-opts]
    (println kw-opts)
    (apply main/repl
           (conj kw-opts
                 :need-prompt (constantly false)
                 :prompt (constantly nil)
                 :eval data-eval)))

  (def s (server/start-server {:name "repl2" :port 8788 :accept
         'hello.clojure.core/data-repl}))

  (server/stop-server "repl2")
  (server/stop-servers)


  ;; `clojure.java.io`

  (with-open [r (io/reader "/usr/share/dict/words")]
    (count (line-seq r)))

  (def s "string->array->reader->bytes->string")
  (with-open [r (io/reader (char-array s))]
    (slurp r))

  (with-open [w (io/writer "/tmp/output.txt")]
    (spit w "Hello\nClojure!!"))
  (println (slurp "/tmp/output.txt"))

  (with-open [r (io/reader "/usr/share/dict/words")
              w (io/writer "/tmp/words")]
    (doseq [line (line-seq r)]
      (.append w (str (s/upper-case line) "\n"))))

  (with-open [r (io/reader "/usr/share/dict/words")
              w (io/writer "/tmp/words" :append true)]
    (doseq [line (line-seq r)]
      (.write w (str (s/upper-case line) "\n"))))

  (def cjio (io/resource "clojure/java/io.clj"))
  (first (line-seq (io/reader cjio)))

  (def path
    (.. FileSystems
        getDefault
        (getPath "/tmp" (into-array String ["words"]))
        toUri))

  (def u1 (io/as-url "file:///tmp/words"))
  (def u2 (io/as-url (io/file "/tmp/words")))
  (def u3 (io/as-url path))

  (= u1 u2 u3)

  (extend-protocol io/Coercions
    Path
    (as-file [path] (io/file (.toUri path)))
    (as-url [path] (io/as-url (.toUri path))))

  (def path
    (.. FileSystems
        getDefault
        (getPath "/usr" (into-array String ["share" "dict" "words"]))))

  (io/as-url path)
  (io/file path)

  (keys (:impls io/Coercions))
  (io/file "/a/valid/file/path")
  (io/file (io/file "/a/valid/file/path"))
  (io/file (io/as-url "file://a/valid/url"))
  (io/file (.toURI (io/as-url "file://a/valid/uri")))
  (io/file nil)

  (io/file "/root" (io/file "not/root") "filename.txt")
  (io/file "/root" (io/file "/not/relative") "filename.txt")

  (io/copy "/usr/share/dict/words" (io/file "/tmp/words2"))
  (.exists (io/file "/tmp/words2"))

  (defmethod @#'io/do-copy [String String] [in out opts]
    (apply io/copy (io/file in) (io/file out) opts))

  (io/copy "/tmp/words2" "/tmp/words3")
  (.exists (io/file "/tmp/words3"))

  (def segments ["/tmp" "a" "b" "file.txt"])

  (apply io/make-parents segments)
  (io/copy (io/file "/tmp/words") (apply io/file segments))
  (count (line-seq (io/reader (io/file "/tmp/words"))))
  (count (line-seq (io/reader (apply io/file segments))))

  (io/delete-file "/does/not/exist")
  (io/delete-file "/does/not/exist" :ignore)
  (io/delete-file "/tmp/a/b/file.txt" "This file should exist")

  (def folders ["root/a/1" "root/a/2" "root/b/1" "root/c/1" "root/c/1/2"])
  (map io/make-parents folders)
  (map io/as-relative-path (file-seq (io/file "root")))


  ;; `clojure.test`

  (defn sqrt [x]
    (when-not (neg? x)
      (loop [guess 1.]
        (if (> (Math/abs (- (* guess guess) x)) 1e-8)
          (recur (/ (+ (/ x guess) guess) 2.))
          guess))))

  (deftest sqrt-test (assert (= 2 (sqrt 4)) "Expecting 2"))
  (:test (meta #'sqrt-test))
  (test #'sqrt-test)

  (with-test
    (defn sum [a b] (+ a b))
    (println "test called"))

  (test #'sum)

  (deftest sqrt-test (is (= 2 (sqrt 4)) "Expecting 2"))
  (test-var #'sqrt-test)

  (deftest sqrt-test
    (testing "The basics of squaring a number"
      (is (= 3 (sqrt 9))))
    (testing "Known corner cases"
      (is (= 0 (sqrt 0)))
      (is (= Double/NaN (sqrt Double/NaN)))))

  (test-var #'sqrt-test)

  (deftest sqrt-test
    (testing "The basics of squaring a number"
      (are [x y] (= (sqrt x) y)
        9 3
        0 0
        Double/NaN Double/NaN)))

  (test-var #'sqrt-test)

  (deftest sqrt-test
    (is (thrown? IllegalArgumentException (sqrt -4)))
    (is (thrown-with-msg? IllegalArgumentException #"negative" (sqrt -4)))
    (is (instance? Double (sqrt nil))))

  (binding [*stack-trace-depth* 3]
    (test-var #'sqrt-test))

  (defmethod assert-expr 'roughly [msg form]
    `(let [op1# ~(nth form 1)
           op2# ~(nth form 2)
           tolerance# (if (= 4 ~(count form)) ~(last form) 2)
           decimals# (/ 1. (Math/pow 10 tolerance#))
           result# (< (Math/abs (- op1# op2#)) decimals#)]
       (do-report
        {:type (if result# :pass :fail)
         :message ~msg
         :expected (format "%s should be roughly %s with %s tolerance"
                           op1# op2# decimals#)
         :actual result#})
       result#))

  (deftest sqrt-test
    (is (roughly 2 (sqrt 4) 14))
    (is (roughly 2 (sqrt 4) 15)))

  (test-var #'sqrt-test)

  (deftest a (is (= 1 (+ 2 2))))
  (deftest b (is (= 2 (+ 2 2))))
  (deftest c (is (= 4 (+ 2 2))))

  (test-all-vars 'hello.clojure.core)


  (deftest fail-a (is (= 1 (+ 2 2))))
  (deftest fail-b (is (= 1 (+ 2 2))))
  (deftest fail-c (is (= 1 (+ 2 2))))

  (defn test-ns-hook [] (fail-a) (fail-c))

  (test-ns 'hello.clojure.core)

  (run-tests)

  (run-all-tests)
  (run-all-tests #".*new.*")


  (ns-unmap *ns* 'test-ns-hook)

  (defn setup [tests]
    (println "### before")
    (tests)
    (println "### after"))

  (use-fixtures :each setup)
  (deftest a-test (is (= 1 1)))
  (deftest b-test (is (= 1 1)))
  (run-tests)


  ;; `clojure.java.javadoc`

  (browse/javadoc "this is a string object")
  (browse/javadoc #"regex")
  (browse/javadoc #("this fn class is not documented"))

  (defn java-version
    "Used in `jdocs-template`."
    []
    (let [jsv (System/getProperty "java.specification.version")]
      (if-let [single-digit (last (re-find #"^\d\.(\d+).*" jsv))]
        single-digit jsv)))

  (def jdocs-template
    "Uses [[java-version]]."
    (format "https://docs.oracle.com/javase/%s/docs/api/" (java-version)))

  (def known-prefix
    ["java." "javax." "org.ietf.jgss." "org.omg."
     "org.w3c.dom." "org.xml.sax."])

  (doseq [prefix known-prefix]
    (browse/add-remote-javadoc prefix jdocs-template))

  (pprint @browse/*remote-javadocs*)

)
