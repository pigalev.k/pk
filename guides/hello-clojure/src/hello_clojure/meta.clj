(ns hello-clojure.meta)


;; not all objects can have metadata, and not all of those can be assigned new
;; metadata

(defn can-have-meta?
  "Returns true if `x` can have metadata."
  [x]
  (instance? clojure.lang.IMeta x))

(defn can-set-meta?
  "Returns true if `x` can be assigned new metadata."
  [x]
  (instance? clojure.lang.IObj x))

(def a 1)

(defrecord Foo [x y])

(comment

  (can-have-meta? 1)
  (can-have-meta? "foo")
  (can-have-meta? :foo)
  (can-have-meta? 'foo)
  (can-have-meta? #'a)
  (can-have-meta? Foo)

  (can-set-meta? 1)
  (can-set-meta? "foo")
  (can-set-meta? :foo)
  (can-set-meta? 'foo)
  (can-set-meta? #'a)
  (can-set-meta? Foo)

)
