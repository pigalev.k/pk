(ns hello-clojure.tap
  (:require [clojure.pprint :refer [pprint]]))


(comment

  (add-tap pprint)

  (tap> {:id 1 :name "Joe"})

  (remove-tap pprint)

)
