(ns hello-clojure.conditions-and-restarts
  (:require [conditions :refer [condition error fall-through manage restart
                                restarts]]
            [clojure.java.io :refer [reader]]
            [clojure.string :as str]
            [clojure.test :refer [deftest is]]))

;; https://github.com/pangloss/pure-conditioning

;; A Tutorial on Conditions and Restarts in Clojure
;; https://nextjournal.com/pangloss/a-tutorial-on-conditions-and-restarts-in-clojure


;;;; a CSV parser

;; signaling conditions

(defn validate-url
  "Returns true if `string` is a valid URL (starts with http:// or https://),
  signals an `:url-invalid` condition otherwise (that throws an error
  by default, if not handled)."
  [string]
  (when-not (re-matches #"^https?://.*" string)
    (condition :url-invalid (restarts {:url string})
               (error "URL invalid"))))

(defn validate-rating
  "Returns true if `string` is a valid rating (is an integer between 1
  and 5, inclusive), signals an `:invalid-rating` condition
  otherwise (that throws an error by default, if not handled)."
  [string]
  (let [rating (read-string string)]
    (when-not (and (int? rating) (<= 1 rating 5))
      (condition :invalid-rating (restarts {:rating rating})
                 (error "Rating is not an integer in range")))))

(defn validate-visitors
  "Returns true if `string` is a valid number of visitors (is a
  non-negative integer), signals an `:invalid-visitors` condition
  otherwise (that throws an error by default, if not handled)."
  [string]
  (let [visitors (read-string string)]
    (when-not (nat-int? visitors)
      (condition :invalid-visitors (restarts {:visitors visitors})
                 (error "Number of visitors invalid")))))

(defn validate-date
  "Returns true if `string` is a valid date in the 'yyyy-mm-dd' format,
  signals a condition `:invalid-date` otherwise (that throws an error
  by default, if not handled)."
  [string]
  (when-not (re-matches #"^\d{4}-\d{2}-\d{2}$" string)
    (condition :invalid-date (restarts {:date string})
               (error "Published date not in valid format"))))

(def validators
  "A map of field header names to field validator functions."
  {"url"      validate-url
   "rating"   validate-rating
   "visitors" validate-visitors
   "date"     validate-date})

;; parsing the CSV

(defn parse-csv-file
  "Returns a vector of vectors of fields from the CSV `file`."
  [file]
  (with-open [f (reader file)]
    (mapv #(str/split % #",") (line-seq f))))

;; validating fields and the whole CSV file

(defn validate-field
  "Returns the result of the validation of the `value` using validator
  associated with `header`, signals an `:invalid-header` condition
  otherwise (without the default handler)."
  [header value]
  (if-let [f (validators header)]
    (f value)
    (condition :invalid-header {:header header})))

(defn validate-csv
  "Returns the result of validation of the CSV `file`. Signals a
  `:wrong-field-count` condition."
  [file]
  (let [[headers & rows :as all-rows] (parse-csv-file file)]
    (map (fn [line-number row]
           (if (not= (count row) (count headers))
             (condition :wrong-field-count {:line-number line-number}
                        (error
                         "Number of fields doesn't equal number of headers"))
             (manage [any? (fall-through #(assoc % :line-number line-number))]
               (mapv validate-field headers row))))
         (range 2 (count all-rows))
         rows)))


(comment

  (def csv (parse-csv-file "resources/conditions-and-restarts/example.csv"))
  (validate-csv "resources/conditions-and-restarts/example.csv")
)
