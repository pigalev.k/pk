(ns hello-clojure.iteration
  (:require
   [clojure.pprint :as pprint]))


;; paginate data with `iteration`

;; https://www.juxt.pro/blog/new-clojure-iteration/

(comment

  ;; the problem: unbounded requests (for arbitrarily large quantities of
  ;; objects at once) are problematic from performance and efficiency
  ;; viewpoints. A solution is to enforce a maximum page size (of 1000 items for
  ;; example) and include a token that allows the client to ask the next page of
  ;; data if they desire to do so. This prevents overloading by segmenting what
  ;; could be a very long network request into many smaller ones, giving the
  ;; server an opportunity to throttle requests if required.

  ;; an "iteration" is effectively a stateful abstraction over a collection
  ;; of data. The state consists of a "cursor" or "head position" which
  ;; indicates the element that will be returned next. The cursor normally
  ;; starts at some initial position when the iteration is first
  ;; created. Depending on the iteration type, the cursor might only move
  ;; forward, or also be allowed to go backward, or it can access the collection
  ;; at some random position.

  ;; interface to control the iteration:
  ;; - fetch the element at the cursor location
  ;; - move the cursor to a new location
  ;; - verify if there is an item at the position pointed by the cursor.
  ;; any iteration API that provides this level of control can be used to create
  ;; a pagination layer.


  ;; while it is true that the problem can be easily solved, the point is that
  ;; the same abstraction has been duplicated in many projects and Clojure
  ;; finally received an authoritative implementation.

)


;; examples

;; common setup

(def results (vec (repeatedly 100 rand)))

(defn page-results
  "Get a subvector from next-marker to the length of page-size or till
  the end of the vector, if page-size is too big."
  [next-items next-marker page-size]
  (subvec
   next-items
   next-marker
   (min (+ next-marker page-size)
        (count next-items))))

(defn list-objects
  "Stub list-objects call to simulate S3 interaction."
  [& {:keys [bucket-name prefix next-marker]}]
  (let [next-items  results
        page-size   12
        total-count (count next-items)
        truncated?  (< (+ next-marker page-size) total-count)]
    (println "### requested page at index" next-marker)
    (when (< next-marker total-count)
      {:truncated?       truncated?
       :next-marker      (when truncated? (+ next-marker page-size))
       :object-summaries (page-results next-items next-marker page-size)})))


(comment

  ;; using paginated API directly

  (list-objects :next-marker 0)
  (list-objects :next-marker 12)

)


;; iterating paginated objects old style

;; a simple adapter for paginated API

(defn create-list-objects-paginated
  "Thin adapter layer to call the actual Amazonica list-object API.
  Bucket and prefix can be anything since they are unused in our local
  stub.  This is our entry point into the Amazonica S3 library for
  this example."
  [bucket prefix]
  (fn [token]
    (list-objects :bucket-name bucket
                  :prefix prefix
                  :next-marker token)))

(comment

  (def list-objects-paginated (create-list-objects-paginated :bucket :prefix))

  (-> (list-objects-paginated 20)
      :object-summaries
      pprint/pprint)

)

;; creating a sequence abstraction over paginated API (using the pagination
;; adapter)

(defn fetch-summaries
  "Retrieve object summaries for items in `bucket` at `prefix`."
  ([bucket prefix]
   (fetch-summaries bucket prefix 0))
  ([bucket prefix token]
   (lazy-seq
    (let [list-objects-paginated
          (create-list-objects-paginated :bucket :prefix)
          {:keys [object-summaries truncated? next-marker]}
          (list-objects-paginated token)]
      (if truncated?
        (cons object-summaries (fetch-summaries bucket prefix next-marker))
        (list object-summaries))))))

(comment

  (->> (fetch-summaries :bucket :prefix 20)
       (sequence cat)
       (take 12)
       clojure.pprint/pprint)

)


;; iterating paginated objects using the `pagination` function

;; the new `iteration` function distills the essence of `fetch-summaries` by
;; providing all the relevant ingredients. It takes the following 5 input
;; arguments, the first of which is the only mandatory one. The parameters
;; defaults implement the common situation of paginated results returned as a
;; plain sequence. In our example, we need to tweak such parameters to work
;; with the map and keys returned by the S3 Amazonica API.

;; - `step` is a function of the next marker token. This function should
;;   contain the logic for making a request to the paginated API, passing the
;;   given token. The step function is expected to return the items produced
;;   by the iteration, including a way to tell if there are more items and the
;;   next token; step could be side-effecting. In our
;;   case, (create-list-objects-paginated :bucket :prefix) returns the step
;;   function.

;; - `somef` is a function that is applied to the return of (`step` token) and
;;   returns true or false based on the fact that the request contains results
;;   or not, respectively. It defaults to `some?` from the standard
;;   library. In our case we are going to use Amazonica S3 `:object-summaries`
;;   key to answer the question.

;; - `vf` is a function that is applied to the return of (`step` token)
;;   returns the items from the current response page. It defaults to
;;   `identity`, so we are using `:object-summaries` again in our case because
;;   this is where the results are available.

;; - `kf` is a function that is applied to the return of (`step` token) and
;;   should return the next marker token if one is available. This also
;;   defaults to `identity`. In our case we can use `:next-marker`.

;; - `initk` is an initial value for the marker. It defaults to nil (0 in our
;;   case).

(defn fetch-summaries*
  "New `fetch-summaries` using `iteration`."
  ([bucket prefix]
   (fetch-summaries* bucket prefix 0))
  ([bucket prefix token]
   (iteration (create-list-objects-paginated bucket prefix)
              :kf :next-marker
              :vf :object-summaries
              :initk token
              :somef :object-summaries)))

(comment

  (->> (fetch-summaries* :bucket :prefix 20)
       (sequence cat)
       (take 12)
       pprint/pprint)

)


;; laziness

;; accessing the item in 10th position from the first page produces 2 additional
;; network calls for pages well ahead of what we currently need. This is an
;; effect of using the cat transducer with sequence which produces chunking that
;; we can't easily control.

(comment

  (let [results (take 10 (sequence cat (fetch-summaries :bucket :prefix)))]
    (last results))

  (let [results (take 10 (sequence cat (fetch-summaries* :bucket :prefix)))]
    (last results))

)

;; this is not a problem of iteration itself, but more about the need to
;; concatenate the results back for processing maintaining laziness in place. We
;; can use the following trick to solve the problem:

(defn lazy-concat
  "A concat version that is completely lazy and does not require to use
  `apply`."
  [colls]
  (lazy-seq
   (when-first [c colls]
     (lazy-cat c (lazy-concat (rest colls))))))

(comment

  ;; no extra calls now

  (let [results (take 10 (lazy-concat (fetch-summaries :bucket :prefix)))]
    (last results))

  (let [results (take 10 (lazy-concat (fetch-summaries* :bucket :prefix)))]
    (last results))

)


;; reducing the iteration

(comment

  ;; iteration is "reducible" and will perform well with `transduce` or `into`:

  (first (into [] (comp cat (map str)) (fetch-summaries* :bucket :prefix)))

  ;; we have an option to stop the reduction using `halt-when` assuming we know
  ;; at which item to stop. Assuming we don't know about the existence of
  ;; `take`, here's how to stop after pulling 30 items from the API and sum them
  ;; up:

  (transduce
   (comp cat                            ;; concat pagination
         (map-indexed vector)           ;; create pairs index-item
         (halt-when (comp #{30} first)  ;; stop when index=30
                    (fn [acc val] acc)) ;; return acc so far
         (map last))                    ;; just the items
   (completing conj #(reduce + %))      ;; sum up items on exit
   (fetch-summaries* :bucket :prefix))


  ;; when transformed to a seq, iteration can also be reduced

  (reduce + (take 30 (lazy-concat (fetch-summaries* :bucket :prefix))))

)
