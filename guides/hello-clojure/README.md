# hello-clojure

Exploring Clojure --- a practical Lisp running on JVM.

## Links

- Clojure's deadly sin: an article about laziness in Clojure
  https://clojure-goes-fast.com/blog/clojures-deadly-sin/
- Configurable Clojure/Script interpreter suitable for scripting and Clojure
  DSLs https://github.com/babashka/SCI

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
