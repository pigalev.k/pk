(ns mal.core
  (:refer-clojure :exclude [load read-line read print eval])
  (:require
   [clojure.core :as clj]))


(defn load [filename] ::load)

(defn read-line
  "Returns the next line from `*in*`, or nil if EOF has been reached."
  []
  (.readLine *in*))

(defn read [string] string)

(defn eval [form] form)

(defn print
  "Prints `value`."
  [value]
  (clj/print value))

(defn read-eval-print
  "Reads data from `string`, evals the data, prints and returns the
  resulting value."
  [string]
  (let [value (-> string read eval)]
    (print value)
    (print \newline)
    (flush)
    value))

(defn repl
  "Starts a REPL with `prompt`."
  [prompt]
  (loop []
    (print prompt)
    (flush)
    (let [line  (read-line)
          value (read-eval-print line)]
      (when-not (#{",q"} value)
        (recur)))))


(comment

  (read-eval-print 1)
  (repl "> ")

)
