(ns mal.reader)


;; state: Reader(tokens): position, next(), peek()


(def tokenizer-regex
  #"[\s,]*(~@|[\[\]{}()'`~^@]|\"(?:\\.|[^\\\"])*\"?|;.*|[^\s\[\]{}('\"`,;)]*)")

(defn tokenize
  "use tokenizer-regex"
  [string]
  (->> (re-seq tokenizer-regex string)
       (map second)
       (remove empty?)))

(defn read-atom
  "int, float, string (escaped), keyword, nil, true, false, symbol"
  [a] a)

(defn read-list
  "repeatedly read-form until end token (EOF is error)"
  [l] l)

(defn read-form
  "expand reader macros, read-list (vectors, maps too), or read-atom"
  [form] form)

(defn read-string
  "tokenize, error if no tokens, call read-form(Reader(tokens))"
  [s] s)

(defn read [string] string)


(comment

  (tokenize "(a 2 3)")
  (tokenize "[1 2 {:id 1 :name \"Joe\"}]")

)
