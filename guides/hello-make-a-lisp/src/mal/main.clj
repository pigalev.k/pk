(ns mal.main
  (:refer-clojure :exclude [load])
  (:require
   [mal.core :refer [repl load]])
  (:gen-class))


(def prompt "> ")

(defn exit
  "Exits with `code`."
  [code]
  (System/exit code))

(defn -main [& args]
  (let [args-count (count args)]
    (println args)
    (cond
      (> args-count 1) (do
                         (println "Usage: mal [script]")
                         (exit 64))
      (= args-count 1) (load (first args))
      :else            (repl prompt))))
