(ns mal.printer)


(defn pr-str
  "- map pr-str across collections
   - unescape strings if print-readably"
  [tree, print-readably])
