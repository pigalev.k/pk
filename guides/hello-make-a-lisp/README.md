# hello-make-a-lisp

Exploring creation of Lisp interpreters.

Sources:
- Make a Lisp
  - https://github.com/kanaka/mal
  - http://kanaka.github.io/mal/cheatsheet.html

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
