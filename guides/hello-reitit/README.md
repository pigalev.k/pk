# hello-reitit

Exploring Reitit --- a fast data-driven router for Clojure(Script).

- https://cljdoc.org/d/metosin/reitit/0.5.18/doc/introduction

## Getting started

Start a cljs REPL in terminal

```
clj -M -m cjls.main
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
