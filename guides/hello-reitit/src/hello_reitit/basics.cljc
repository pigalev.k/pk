(ns hello-reitit.basics
  (:require
   [clojure.spec.alpha :as s]
   [reitit.core :as r]
   [reitit.dev.pretty :as pretty]
   [reitit.exception :as exception]
   [reitit.spec :as rs]
   [spec-tools.spell :as spell])
  (:import (java.io File)))


;; basics

;; https://cljdoc.org/d/metosin/reitit/0.6.0/doc/basics


;; route syntax

(comment

  ;; routes are defined as vectors of String path and optional (non-sequential)
  ;; route argument child routes.

  ;; routes can be wrapped in vectors and lists and nil routes are ignored.

  ;; paths can have path-parameters (like `:id`) or catch-all-parameters (like
  ;; `*path`). Parameters can also be wrapped in brackets, enabling use of
  ;; qualified keywords (like `{user/id}`, `{*user/path}`). By default, both
  ;; syntaxes are supported, see configuring routers on how to change this.


  ;; route examples

  ;; simple route

  ["/ping"]

  ;; two routes

  [["/ping"]
   ["/pong"]]

  ;; routes with route arguments

  [["/ping" ::ping]
   ["/pong" {:name ::pong}]]

  ;; routes with path parameters

  [["/users/:user-id"]
   ["/api/:version/ping"]]

  [["/users/{user-id}"]
   ["/files/file-{number}.pdf"]]

  ;; route with catch-all parameter

  ["/public/*path"]

  ["/public/{*path}"]

  ;; nested routes

  ["/api"
   ["/admin" {:middleware [::admin]}
    ["" ::admin]
    ["/db" ::db]]
   ["/ping" ::ping]]

  ;; same routes flattened

  [["/api/admin" {:middleware [::admin], :name ::admin}]
   ["/api/admin/db" {:middleware [::admin], :name ::db}]
   ["/api/ping" {:name ::ping}]]


  ;; encoding

  ;; reitit does not apply any encoding to your paths. If you need that, you
  ;; must encode them yourself. E.g., `/foo bar` should be `/foo%20bar`.


  ;; wildcards

  ;; normal path-parameters (like `:id`) can start anywhere in the path string,
  ;; but have to end either to slash `/` (currently hardcoded) or to an end of
  ;; path string:

  [["/api/:version"]
   ["/files/file-:number"]
   ["/user/:user-id/orders"]]

  ;; bracket path-parameters can start and stop anywhere in the path-string, the
  ;; following character is used as a terminator.

  [["/api/{version}"]
   ["/files/{name}.{extension}"]
   ["/user/{user-id}/orders"]]

  ;; having multiple terminators after a bracket path parameters with identical
  ;; path prefix will cause a compile-time error at router creation:

  (r/router [["/files/file-{name}.pdf"]             ;; terminator \.
             ["/files/file-{name}-{version}.pdf"]]) ;; terminator \-


  ;; slash-free routing

  [["broker.{customer}.{device}.{*data}"]
   ["events.{target}.{type}"]]


  ;; generating routes

  ;; routes are just data, so it's easy to create them programmatically:

  (defn cqrs-routes [actions]
    ["/api" {:interceptors [::api ::db]}
     (for [[type interceptor] actions
           :let [path (str "/" (name interceptor))
                 method (case type
                          :query :get
                          :command :post)]]
       [path {method {:interceptors [interceptor]}}])])

  (cqrs-routes
   [[:query   'get-user]
    [:command 'add-user]
    [:command 'add-order]])


  ;; explicit path-parameter syntax

  ;; router options `:syntax` allows the path-parameter syntax to be explicitly
  ;; defined. It takes a keyword or set of keywords as a value. Valid values are
  ;; `:colon` and `:bracket`. Default value is `#{:colon :bracket}`.

  ;; with defaults:

  (-> (r/router
       ["http://localhost:8080/api/user/{id}" ::user-by-id])
      (r/match-by-path "http://localhost:8080/api/user/123"))

  ;; supporting only `:bracket` syntax:

  (-> (r/router
       ["http://localhost:8080/api/user/{id}" ::user-by-id]
       {:syntax :bracket})
      (r/match-by-path "http://localhost:8080/api/user/123"))

)


;; router

(comment

  ;; routes are just data and to do routing, we need a router instance
  ;; satisfying the `reitit.core/Router` protocol. Routers are created with
  ;; `reitit.core/router` function, taking the raw routes and optionally an
  ;; options map.

  ;; the `Router` protocol:

  (defprotocol Router
    (router-name [this])
    (routes [this])
    (options [this])
    (route-names [this])
    (match-by-path [this path])
    (match-by-name [this name] [this name params]))


  ;; creating a router

  (def router
    (r/router
     ["/api"
      ["/ping" ::ping]
      ["/user/:id" ::user]]))

  ;; name of the created router:

  (r/router-name router)

  ;; the flattened route tree:

  (r/routes router)

  ;; with a router instance, we can do path-based (direct) routing or
  ;; name-based (reverse) routing.


  ;; more details about the router

  ;; router options

  (r/options router)

  ;; route names

  (r/route-names router)


  ;; composing

  ;; as routes are defined as plain data, it's easy to merge multiple route
  ;; trees into a single router.

  (def user-routes
    [["/users" ::users]
     ["/users/:id" ::user]])

  (def admin-routes
    ["/admin"
     ["/ping" ::ping]
     ["/db" ::db]])

  (def router
    (r/router
     [admin-routes
      user-routes]))

  ;; merged route tree:

  (r/routes router)


  ;; behind the scenes

  ;; when router is created, the following steps are done:

  ;; - route tree is flattened
  ;; - route arguments are expanded (via `:expand` option)
  ;; - routes are coerced (via `:coerce` options)
  ;; - route tree is compiled (via `:compile` options)
  ;; - route conflicts are resolved (via `:conflicts` options)
  ;; - optionally, route data is validated (via `:validate` options)
  ;; - router implementation is automatically selected (or forced via `:router`
  ;;   options) and created

)


;; path-based routing

(comment

  ;; path-based routing is done using the `reitit.core/match-by-path`
  ;; function. It takes the router and path as arguments and returns one of the
  ;; following:

  ;; - `nil`, no match
  ;; - `PartialMatch`, path matched, missing path-parameters (only in
  ;;   reverse-routing)
  ;; - `Match`, an exact match

  ;; given a router:

  (def router
    (r/router
     ["/api"
      ["/ping" ::ping]
      ["/user/:id" ::user]]))

  ;; no match returns `nil`:

  (r/match-by-path router "/hello")

  ;; match provides the route information:

  (r/match-by-path router "/api/user/1")

)


;; name-based (reverse) routing

(comment

  ;; all routes which have `:name` route data defined can also be matched by
  ;; name.

  ;; given a router:

  (def router
    (r/router
     ["/api"
      ["/ping" ::ping]
      ["/user/:id" ::user]]))

  ;; listing all route names:

  (r/route-names router)

  ;; no match returns `nil`:

  (r/match-by-name router ::kikka)

  ;; matching a route:

  (r/match-by-name router ::ping)

  ;; if not all path-parameters are set, a `PartialMatch` is returned:

  (r/match-by-name router ::user)
  (r/partial-match? (r/match-by-name router ::user))

  ;; with provided path-parameters:

  (r/match-by-name router ::user {:id "1"})

  ;; path-parameters are automatically coerced into strings, with the help
  ;; of (currently internal) protocol `reitit.impl/IntoString`. It supports
  ;; strings, numbers, booleans, keywords and objects:

  (r/match-by-name router ::user {:id 1})

  ;; there is also an exception throwing version:

  (r/match-by-name! router ::user)

  ;; to turn a Match into a path, there is `reitit.core/match->path`:

  (-> router
      (r/match-by-name ::user {:id 1})
      (r/match->path))

  ;; it can take an optional map of query-parameters too:

  (-> router
      (r/match-by-name ::user {:id 1})
      (r/match->path {:iso "möly"}))

)


;; route data

(comment

  ;; route data is the key feature of reitit. Routes can have any map-like data
  ;; attached to them, to be interpreted by the client application, `Router` or
  ;; routing components like `Middleware` or `Interceptors`.

  [["/ping" {:name ::ping}]
   ["/pong" {:handler identity}]
   ["/users" {:get {:roles #{:admin}
                    :handler identity}}]]

  ;; besides map-like data, raw routes can have any non-sequential route
  ;; argument after the path. This argument is expanded by Router (via `:expand`
  ;; option) into route data at router creation time.

  ;; by default, keywords are expanded into `:name` and functions into
  ;; `:handler` keys.

  (def router
    (r/router
     [["/ping" ::ping]
      ["/pong" identity]
      ["/users" {:get {:roles #{:admin}
                       :handler identity}}]]))


  ;; using route data

  ;; expanded route data can be retrieved from a router with `routes` and is
  ;; returned with `match-by-path` and `match-by-name` in case of a route match.

  (r/routes router)

  (r/match-by-path router "/ping")

  (r/match-by-name router ::ping)


  ;; nested  route data

  ;; for nested route trees, route data is accumulated recursively from root
  ;; towards leafs using `meta-merge`. Default behavior for collections is
  ;; `:append`, but this can be overridden to `:prepend`, `:replace` or
  ;; `:displace` using the target meta-data.

  ;; an example router with nested data:

  (def router
    (r/router
     ["/api" {:interceptors [::api]}
      ["/ping" ::ping]
      ["/admin" {:roles #{:admin}}
       ["/users" ::users]
       ["/db" {:interceptors [::db]
               :roles ^:replace #{:db-admin}}]]]))

  ;; resolved route tree:

  (r/routes router)


  ;; route data fragments

  ;; just like fragments in React.js, we can create routing tree fragments by
  ;; using empty path "". This allows us to add route data without accumulating
  ;; to path.

  ;; given a route tree:

  [["/swagger.json" ::swagger]
   ["/api-docs" ::api-docs]
   ["/api/ping" ::ping]
   ["/api/pong" ::pong]]

  ;; adding `:no-doc` route data to exclude the first routes from generated
  ;; Swagger documentation:

  [["" {:no-doc true}
    ["/swagger.json" ::swagger]
    ["/api-docs" ::api-docs]]
   ["/api/ping" ::ping]
   ["/api/pong" ::pong]]

  ;; accumulated route data:

  (def router
    (r/router
     [["" {:no-doc true}
       ["/swagger.json" ::swagger]
       ["/api-docs" ::api-docs]]
      ["/api/ping" ::ping]
      ["/api/pong" ::pong]]))

  (r/routes router)


  ;; top-level route data

  ;; route data can be introduced also via Router option `:data`:

  (def router
    (r/router
     ["/api"
      {:middleware [::api]}
      ["/ping" ::ping]
      ["/pong" ::pong]]
     {:data {:middleware [::session]}}))

  ;; expanded routes:

  (r/routes router)


  ;; customizing expansion

  ;; by default, router `:expand` option has value `r/expand` function, backed
  ;; by a `r/Expand` protocol. Expansion can be customized either by swapping
  ;; the `:expand` implementation or by extending the protocol. `r/Expand`
  ;; implementations can be recursive.

  ;; naive example to add direct support for `java.io.File` route argument:

  (extend-type File
    r/Expand
    (expand [file options]
      (r/expand
       #(slurp file)
       options)))

  (r/router
   ["/" (java.io.File. "index.html")])

)


;; route data validation

(comment

  ;; route data can be anything, so it's easy to go wrong. Accidentally adding a
  ;; `:role` key instead of `:roles` might hinder the whole routing app without
  ;; any authorization in place.

  ;; to fail fast, we could use the custom `:coerce` and `:compile` hooks to
  ;; apply data validation and throw exceptions on first sighted problem.

  ;; but there is a better way. Router has a `:validation` hook to validate the
  ;; whole route tree after it's successfully compiled. It expects a 2-arity
  ;; function `routes opts => ()` that can side-effect in case of validation
  ;; errors.


  ;; `clojure.spec`

  ;; namespace `reitit.spec` contains specs for main parts of `reitit.core` and
  ;; a helper function validate that runs spec validation for all route data and
  ;; throws an exception if any errors are found.

  ;; a router with invalid route data:

  (r/router
   ["/api" {:handler "identity"}])

  (r/router
   ["/api" {:handler "identity"}]
   {:validate rs/validate})


  ;; pretty errors

  ;; turning on pretty errors will give much nicer error messages (not in Cider
  ;; though):

  (r/router
   ["/api" {:handler "identity"}]
   {:validate rs/validate
    :exception pretty/exception})


  ;; customizing spec validation

  ;; `rs/validate` reads the following router options:
  ;; - `:spec`: the spec to verify the route data (default `::rs/default-data`)
  ;; - `:reitit.spec/wrap`: function of `spec => spec` to wrap all route specs

  ;; NOTE: `clojure.spec` implicitly validates all values with fully-qualified
  ;; keys if specs exist with the same name.

  ;; invalid spec value:

  (s/def ::role #{:admin :manager})
  (s/def ::roles (s/coll-of ::role :into #{}))

  (r/router
   ["/api" {:handler identity
            ::roles #{:adminz}}]
   {:validate rs/validate})


  ;; closed specs

  ;; To fail-fast on non-defined and misspelled keys on route data, we can close
  ;; the specs using `:reitit.spec/wrap` options with value of
  ;; `spec-tools.spell/closed` that closed the top-level specs.

  ;; requiring a `:description` and validating using closed specs:

  (s/def ::description string?)

  (r/router
   ["/api" {:summary "kikka"}]
   {:validate rs/validate
    :spec (s/merge ::rs/default-data
                   (s/keys :req-un [::description]))
    ::rs/wrap spell/closed})


  ;; it catches also typing errors:

  (r/router
   ["/api" {:descriptionz "kikka"}]
   {:validate rs/validate
    :spec (s/merge ::rs/default-data
                   (s/keys :req-un [::description]))
    ::rs/wrap spell/closed})

)


;; route conflicts

(comment


  ;; we should fail fast if a router contains conflicting paths or route names.

  ;; when a `Router` is created via `reitit.core/router`, both path and route
  ;; name conflicts are checked automatically. By default, in case of conflict,
  ;; an `ex-info` is thrown with a descriptive message. In some (legacy api)
  ;; cases, path conflicts should be allowed and one can override the path
  ;; conflict resolution via `:conflicts` router option or via `:conflicting`
  ;; route data.


  ;; path conflicts

  ;; routes with path conflicts:

  (def routes
    [["/ping"]
     ["/:user-id/orders"]
     ["/bulk/:bulk-id"]
     ["/public/*path"]
     ["/:version/status"]])

  ;; creating router with defaults:

  (r/router routes)

  ;; to ignore the conflicts:

  (r/router
   routes
   {:conflicts nil})

  ;; to just log the conflicts:

  (r/router
   routes
   {:conflicts (fn [conflicts]
                 (println (exception/format-exception
                           :path-conflicts nil conflicts)))})

  ;; alternatively, you can ignore conflicting paths individually
  ;; via `:conflicting` in route data:

  (def routes
    [["/ping"]
     ["/:user-id/orders" {:conflicting true}]
     ["/bulk/:bulk-id" {:conflicting true}]
     ["/public/*path" {:conflicting true}]
     ["/:version/status" {:conflicting true}]])

  (r/router routes)


  ;; name conflicts

  ;; routes with name conflicts:

  (def routes
    [["/ping" ::ping]
     ["/admin" ::admin]
     ["/admin/ping" ::ping]])

  ;; creating router with defaults:

  (r/router routes)

  ;; there is no way to disable the name conflict resolution.

)


;; Error Messages

(comment

  ;; all exceptions thrown in router creation are caught, formatted and rethrown
  ;; by the `reitit.core/router` function. Exception formatting is done by the
  ;; exception formatter defined by the `:exception` router option.


  ;; default errors

  ;; the default exception formatting uses `reitit.exception/exception`. It
  ;; produces single-color, partly human-readable, error messages.

  (r/router
   [["/ping"]
    ["/:user-id/orders"]
    ["/bulk/:bulk-id"]
    ["/public/*path"]
    ["/:version/status"]])


  ;; pretty errors

  ;; [metosin/reitit-dev "0.6.0"]

  ;; for human-readable and developer-friendly exception messages, there is
  ;; `reitit.dev.pretty/exception` (in the `reitit-dev` module). It is inspired
  ;; by the lovely errors messages of ELM and ETA and uses fipp, expound and
  ;; spell-spec for most of heavy lifting.

  (r/router
   [["/ping"]
    ["/:user-id/orders"]
    ["/bulk/:bulk-id"]
    ["/public/*path"]
    ["/:version/status"]]
   {:exception pretty/exception})


  ;; extending

  ;; behind the scenes, both error formatters are backed by a multimethod, so
  ;; they are easy to extend.


  ;; runtime exceptions

  ;; see Exception Handling with Ring.

)
