(ns hello-reitit.intro
  (:require
   [reitit.core :as r]
   [reitit.ring :as ring]))


;; https://cljdoc.org/d/metosin/reitit/0.6.0/doc/introduction


;; intro

(comment

  ;; simple router

  (def router
    (r/router
     [["/api/ping" ::ping]
      ["/api/orders/:id" ::order-by-id]]))

  ;; routing

  (r/match-by-path router "/api/ipa")
  (r/match-by-path router "/api/ping")
  (r/match-by-path router "/api/orders/1")

  ;; reverse-routing

  (r/match-by-name router ::ipa)
  (r/match-by-name router ::ping)
  (r/match-by-name router ::order-by-id)
  (r/partial-match? (r/match-by-name router ::order-by-id))
  (r/match-by-name router ::order-by-id {:id 2})


  ;; ring-router

  ;; ring-router adds support for `:handler` functions, `:middleware` and
  ;; routing based on `:request-method`. It also supports pluggable parameter
  ;; coercion (`clojure.spec`), data-driven middleware, route and middleware
  ;; compilation, dynamic extensions and more.

  (defn handler [_]
    {:status 200, :body "ok"})

  (defn wrap [handler id]
    (fn [request]
      (update (handler request) :wrap (fnil conj '()) id)))

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [[wrap :api]]}
       ["/ping" {:get handler
                 :name ::ping}]
       ["/admin" {:middleware [[wrap :admin]]}
        ["/users" {:get handler
                   :post handler}]]])))

  ;; routing

  (app {:request-method :get, :uri "/api/admin/users"})
  (app {:request-method :put, :uri "/api/admin/users"})

  ;; reverse-routing

  (-> app (ring/get-router) (r/match-by-name ::ping))

)
