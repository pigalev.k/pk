(ns hello-reitit.http
  (:require
   [io.pedestal.http :as server]
   [reitit.http :as http]
   [reitit.pedestal :as pedestal]
   [reitit.ring :as ring]
   [reitit.interceptor :as interceptor]
   [reitit.http.interceptors.dev :as dev]
   [reitit.interceptor.sieppari :as sieppari]))


;; HTTP

;; https://cljdoc.org/d/metosin/reitit/0.6.0/doc/http


;; interceptors

(comment

  ;; reitit has also support for interceptors as an alternative to using
  ;; middleware. Basic interceptor handling is implemented in
  ;; `reitit.interceptor` package. There is no interceptor executor shipped, but
  ;; you can use libraries like Pedestal Interceptor or Sieppari to execute the
  ;; chains.


  ;; `reitit-http`

  ;; [metosin/reitit-http "0.6.0"]

  ;; a module for http-routing using interceptors instead of middleware. Builds
  ;; on top of the `reitit-ring` module having all the same features.

  ;; the differences:

  ;; - `:interceptors` key used in route data instead of `:middleware`
  ;; - `reitit.http/http-router` requires an extra option `:executor` of type
  ;;   `reitit.interceptor/Executor` to execute the interceptor chain
  ;; - optionally, a routing interceptor can be used - it enqueues the matched
  ;;   interceptors into the context. See `reitit.http/routing-interceptor` for
  ;;   details.


  ;; simple example

  (defn interceptor [number]
    {:enter (fn [ctx]
              (update-in ctx [:request :number] (fnil + 0) number))})

  (def app
    (http/ring-handler
     (http/router
      ["/api"
       {:interceptors [(interceptor 1)]}

       ["/number"
        {:interceptors [(interceptor 10)]
         :get {:interceptors [(interceptor 100)]
               :handler (fn [req]
                          {:status 200
                           :body (select-keys req [:number])})}}]])
     ;; the default handler
     (ring/create-default-handler)
     ;; executor
     {:executor sieppari/executor}))

  (app {:request-method :get, :uri "/"})
  (app {:request-method :get, :uri "/api/number"})


  ;; why interceptors?

  ;; - https://quanttype.net/posts/2018-08-03-why-interceptors.html
  ;; - https://www.reddit.com/r/Clojure/comments/9csmty/why_interceptors/

)


;; Pedestal

(comment

  ;; Pedestal is a backend web framework for Clojure. `reitit-pedestal`
  ;; provides an alternative routing engine for Pedestal.

  ;; [metosin/reitit-pedestal "0.6.0"]

  ;; why should one use reitit instead of the Pedestal default routing?

  ;; - one simple route syntax, with full route conflict resolution.
  ;; - supports first class route data with spec validation.
  ;; - fixes some known problems in routing.
  ;; - can handle trailing backslashes.
  ;; - one router for both backend and frontend.
  ;; - supports parameter coercion & Swagger.
  ;; - is even faster.

  ;; to use Pedestal with reitit, you should first read both the Pedestal docs
  ;; and the reitit interceptor guide.

  ;; example

  ;; a minimalistic example on how to to swap the default-router with a reitit
  ;; router.

  ;; [io.pedestal/pedestal.service "0.5.5"]
  ;; [io.pedestal/pedestal.jetty "0.5.5"]
  ;; [metosin/reitit-pedestal "0.6.0"]
  ;; [metosin/reitit "0.6.0"]


  (defn interceptor [number]
    {:enter (fn [ctx] (update-in ctx [:request :number] (fnil + 0) number))})

  (def routes
    ["/api"
     {:interceptors [(interceptor 1)]}
     ["/number"
      {:interceptors [(interceptor 10)]
       :get {:interceptors [(interceptor 100)]
             :handler (fn [req]
                        {:status 200
                         :body (select-keys req [:number])})}}]])

  (-> {::server/type :jetty
       ::server/port 3000
       ::server/join? false
       ;; no pedestal routes
       ::server/routes []}
      (server/default-interceptors)
      ;; swap the reitit router
      (pedestal/replace-last-interceptor
       (pedestal/routing-interceptor
        (http/router routes)))
      (server/dev-interceptors)
      (server/create-server)
      (server/start))


  ;; compatibility

  ;; there is no common interceptor spec for Clojure and all default reitit
  ;; interceptors (coercion, exceptions etc.) use the Sieppari interceptor
  ;; model. It is mostly compatible with the Pedestal Interceptor model, only
  ;; exception being that the `:error` handlers take just 1 arity (context)
  ;; compared to Pedestal's 2-arity (context and exception).

  ;; currently, out of the reitit default interceptors, there is only the
  ;; `reitit.http.interceptors.exception/exception-interceptor` which has
  ;; the `:error` defined.

  ;; you are most welcome to discuss about a common interceptor spec in
  ;; #interceptors on Clojurians Slack.


  ;; more examples

  ;; simple

  ;; simple example with sync & async interceptors:
  ;; https://github.com/metosin/reitit/tree/master/examples/pedestal

  ;; Swagger

  ;; more complete example with custom interceptors, default interceptors,
  ;; coercion and swagger-support enabled:
  ;; https://github.com/metosin/reitit/tree/master/examples/pedestal-swagger

)


;; Sieppari

(comment

  ;; [metosin/reitit-sieppari "0.6.0"]

  ;; Sieppari is a new and fast interceptor implementation for Clojure, with
  ;; pluggable async supporting `core.async`, Manifold and Promesa.

  ;; to use Sieppari with `reitit-http`, we need to attach a
  ;; `reitit.interceptor.sieppari/executor` to a `http-router` to compile and
  ;; execute the interceptor chains. Reitit and Sieppari share the same
  ;; interceptor model, so all reitit default interceptors work seamlessly
  ;; together.

  ;; we can use both synchronous ring and async-ring with Sieppari.


  ;; synchronous Ring

  (defn i [x]
    {:enter (fn [ctx] (println "enter " x) ctx)
     :leave (fn [ctx] (println "leave " x) ctx)})

  (defn handler [_]
    #?(:clj (future {:status 200, :body "pong"})))

  (def app
    (http/ring-handler
     (http/router
      ["/api"
       {:interceptors [(i :api)]}

       ["/ping"
        {:interceptors [(i :ping)]
         :get {:interceptors [(i :get)]
               :handler handler}}]])
     {:executor sieppari/executor}))

  (app {:request-method :get, :uri "/api/ping"})


  ;; async-ring

  #?(:clj (let [respond (promise)]
            (app {:request-method :get, :uri "/api/ping"} respond nil)
            (deref respond 1000 ::timeout)))


  ;; examples

  ;; simple

  ;; simple example, with both sync & async code:
  ;; https://github.com/metosin/reitit/tree/master/examples/http

  ;; with batteries

  ;; with default interceptors, coercion and swagger-support:
  ;; https://github.com/metosin/reitit/tree/master/examples/http-swagger

)


;; default interceptors

(comment

  ;; [metosin/reitit-interceptors "0.6.0"]

  ;; just like the ring default middleware, but for interceptors.


  ;; parameters handling

  ;; - `reitit.http.interceptors.parameters/parameters-interceptor`


  ;; exception handling

  ;; - `reitit.http.interceptors.exception/exception-interceptor`


  ;; content negotiation

  ;; - `reitit.http.interceptors.muuntaja/format-interceptor`
  ;; - `reitit.http.interceptors.muuntaja/format-negotiate-interceptor`
  ;; - `reitit.http.interceptors.muuntaja/format-request-interceptor`
  ;; - `reitit.http.interceptors.muuntaja/format-response-interceptor`


  ;; multipart request handling

  ;; - `reitit.http.interceptors.multipart/multipart-interceptor`


  ;; example app

  ;; see an example app with the default interceptors in action:
  ;; https://github.com/metosin/reitit/blob/master/examples/http-swagger/src/example/server.clj.

)


;; transforming the interceptor chain

(comment

  ;; there is an extra option in `http-router` (actually, in the underlying
  ;; `interceptor-router`): `:reitit.interceptor/transform` to transform the
  ;; interceptor chain per endpoint. Value should be a function or a vector of
  ;; functions that get a vector of compiled interceptors and should return a
  ;; new vector of interceptors.

  ;; NOTE: the last interceptor in the chain is usually the handler, compiled
  ;; into an Interceptor. Applying a transformation `clojure.core/reverse` would
  ;; put this interceptor into first in the chain, making the rest of the
  ;; interceptors effectively unreachable. There is a helper
  ;; `reitit.interceptor/transform-butlast` to transform all but the last
  ;; interceptor.


  ;; example application

  (defn interceptor [message]
    {:enter
     (fn [ctx] (update-in ctx [:request :message] (fnil conj []) message))})

  (defn handler [req]
    {:status 200
     :body (select-keys req [:message])})

  (def app
    (http/ring-handler
     (http/router
      ["/api" {:interceptors [(interceptor 1) (interceptor 2)]}
       ["/ping" {:get {:interceptors [(interceptor 3)]
                       :handler handler}}]])
     {:executor sieppari/executor}))

  (app {:request-method :get, :uri "/api/ping"})


  ;; reversing the interceptor chain

  (def app
    (http/ring-handler
     (http/router
      ["/api" {:interceptors [(interceptor 1) (interceptor 2)]}
       ["/ping" {:get {:interceptors [(interceptor 3)]
                       :handler handler}}]]
      {::interceptor/transform (interceptor/transform-butlast reverse)})
     {:executor sieppari/executor}))

  (app {:request-method :get, :uri "/api/ping"})


  ;; interleaving interceptors

  (def app
    (http/ring-handler
     (http/router
      ["/api" {:interceptors [(interceptor 1) (interceptor 2)]}
       ["/ping" {:get {:interceptors [(interceptor 3)]
                       :handler handler}}]]
      {::interceptor/transform #(interleave % (repeat (interceptor :debug)))})
     {:executor sieppari/executor}))

  (app {:request-method :get, :uri "/api/ping"})


  ;; printing context diffs

  ;; [metosin/reitit-interceptors "0.6.0"]

  ;; using `reitit.http.interceptors.dev/print-context-diffs` transformation,
  ;; the context diffs between each interceptor are printed out to the
  ;; console. To use it, add the following router option:

  ;; `:reitit.interceptor/transform`
  ;; `reitit.http.interceptors.dev/print-context-diffs`

  (def app
    (http/ring-handler
     (http/router
      ["/api" {:interceptors [(interceptor 1) (interceptor 2)]}
       ["/ping" {:get {:interceptors [(interceptor 3)]
                       :handler handler}}]]
      {::interceptor/transform dev/print-context-diffs})
     {:executor sieppari/executor}))

  (app {:request-method :get, :uri "/api/ping"})


  ;; sample applications (uncomment the option to see the diffs):

  ;; - Sieppari:
  ;; - https://github.com/metosin/reitit/blob/master/examples/http-swagger/src/example/server.clj
  ;; - Pedestal:
  ;; - https://github.com/metosin/reitit/blob/master/examples/pedestal-swagger/src/example/server.clj

)
