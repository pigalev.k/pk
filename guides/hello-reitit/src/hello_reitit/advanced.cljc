(ns hello-reitit.advanced
  (:require
   [clojure.spec.alpha :as s]
   [clojure.spec.test.alpha :as stest]
   [clojure.string :as str]
   [compojure.core :refer [context]]
   [expound.alpha :as expound]
   [reitit.core :as r]
   [reitit.ring :as ring]
   [reitit.spec :as spec])
  (:import (clojure.lang IDeref)))


;; Advanced

;; https://cljdoc.org/d/metosin/reitit/0.6.0/doc/advanced


;; configuring routers

(comment

  ;; routers can be configured via options. The following options are available
  ;; for the `reitit.core/router`:

  ;; - `:path`: base-path for routes
  ;; - `:routes`: initial resolved routes (default [])
  ;; - `:data`: initial route data (default {})
  ;; - `:spec`: `clojure.spec` definition for a route data, see `reitit.spec`
  ;;   on how to use this
  ;; - `:syntax`: path-parameter syntax as keyword or set of keywords
  ;;   (default `#{:bracket :colon}`)
  ;; - `:expand`: function of `arg opts => data` to expand route arg to route
  ;;   data (default reitit.core/expand)
  ;; - `:coerce`: function of `route opts => route` to coerce resolved route,
  ;;   can throw or return nil
  ;; - `:meta-merge`: function which follows the signature of
  ;;   `meta-merge.core/meta-merge`, useful for when you want to have more
  ;;   control over the meta merging
  ;; - `:compile`: function of `route opts => result` to compile a route handler
  ;; - `:validate`: function of `routes opts => ()` to validate route (data)
  ;;   via side-effects
  ;; - `:conflicts`: function of `{route #{route}} => ()` to handle conflicting
  ;;   routes
  ;; - `:exception`: function of `Exception => Exception` to handle creation
  ;;   time exceptions (default `reitit.exception/exception`)
  ;; - `:router`: function of `routes opts => router` to override the actual
  ;;   router implementation

)


;; composing routers

(comment

  ;; data-driven approach in reitit allows us to compose routes, route data,
  ;; route specs, middleware and interceptors chains. We can compose routers
  ;; too. This is needed to achieve dynamic routing like in Compojure.


  ;; immutability

  ;; once a router is created, the routing tree is immutable and cannot be
  ;; changed. To change the routing, we need to create a new router with changed
  ;; routes and/or options. For this, the Router protocol exposes it's resolved
  ;; routes via `r/routes` and options via `r/options`.


  ;; adding routes

  ;; let's create a router:

  (def router
    (r/router
     [["/foo" ::foo]
      ["/bar/:id" ::bar]]))

  ;; we can query the resolved routes and options:

  (r/routes router)
  (r/options router)

  ;; let's add a helper function to create a new router with extra routes:

  (defn add-routes [router routes]
    (r/router
     (into (r/routes router) routes)
     (r/options router)))

  ;; we can now create a new router with extra routes:

  (def router2
    (add-routes
     router
     [["/baz/:id/:subid" ::baz]]))

  (r/routes router2)

  ;; the original router was not changed:

  (r/routes router)

  ;; when a new router is created, all rules are applied, including the conflict
  ;; resolution:

  (add-routes
   router2
   [["/:this/should/:fail" ::fail]])


  ;; merging routers

  ;; let's create a helper function to merge routers:

  (defn merge-routers [& routers]
    (r/router
     (apply merge (map r/routes routers))
     (apply merge (map r/options routers))))

  ;; we can now merge multiple routers into one:

  (def router
    (merge-routers
     (r/router ["/route1" ::route1])
     (r/router ["/route2" ::route2])
     (r/router ["/route3" ::route3])))

  (r/routes router)


  ;; nesting routers

  ;; routers can be nested using the catch-all parameter.

  ;; here's a router with deeply nested routers under a `:router` key in the
  ;; route data:

  (def router
    (r/router
     [["/ping" :ping]
      ["/olipa/*" {:name :olipa
                   :router
                   (r/router
                    [["/olut" :olut]
                     ["/makkara" :makkara]
                     ["/kerran/*" {:name :kerran
                                   :router (r/router
                                            [["/avaruus" :avaruus]
                                             ["/ihminen" :ihminen]])}]])}]]))

  ;; matching by path:

  (r/match-by-path router "/olipa/kerran/iso/kala")

  ;; that didn't work as we wanted, as the nested routers don't have such a
  ;; route. The core routing doesn't understand anything the `:router` key, so
  ;; it only matched against the top-level router, which gave a match for the
  ;; catch-all path.

  ;; as the `Match` contains all the route data, we can create a new matching
  ;; function that understands the `:router` key. Below is a function that does
  ;; recursive matching using the subrouters. It returns either nil or a vector
  ;; of matches.

  (defn recursive-match-by-path [router path]
    (when-let [match (r/match-by-path router path)]
      (if-let [subrouter (-> match :data :router)]
        (let [subpath (subs path (str/last-index-of (:template match) "/"))]
          (when-let [submatch (recursive-match-by-path subrouter subpath)]
            (cons match submatch)))
        (list match))))

  ;; with invalid nested path we get now nil as expected:

  (recursive-match-by-path router "/olipa/kerran/iso/kala")

  ;; with valid path we get all the nested matches:

  (recursive-match-by-path router "/olipa/kerran/avaruus")

  ;; let's create a helper to get only the route names for matches:

  (defn name-path [router path]
    (some->> (recursive-match-by-path router path)
             (mapv (comp :name :data))))

  (name-path router "/olipa/kerran/avaruus")

  ;; so, we can nest routers, but why would we do that?


  ;; dynamic routing

  ;; in all the examples above, the routers were created ahead of time, making
  ;; the whole route tree effectively static. To have more dynamic routing, we
  ;; can use router references allowing the router to be swapped over time. We
  ;; can also create fully dynamic routers where the router is re-created for
  ;; each request. Let's walk through both cases.

  ;; first, we need to modify our matching function to support router
  ;; references:

  (defn- << [x]
    (if (instance? IDeref x)
      (deref x) x))

  (defn recursive-match-by-path [router path]
    (when-let [match (r/match-by-path (<< router) path)]
      (if-let [subrouter (-> match :data :router <<)]
        (let [subpath (subs path (str/last-index-of (:template match) "/"))]
          (when-let [submatch (recursive-match-by-path subrouter subpath)]
            (cons match submatch)))
        (list match))))

  ;; then, we need some routers.

  ;; first, a reference to a router that can be updated on background, for
  ;; example when a new entry in inserted into a database. We'll wrap the router
  ;; into a atom:

  (def beer-router
    (atom
     (r/router
      [["/lager" :lager]])))

  ;; second, a reference to router, which is re-created on each routing request:

  (def dynamic-router
    (reify IDeref
      (deref [_]
        (r/router
         ["/duo" (keyword (str "duo" (rand-int 100)))]))))

  ;; we can compose the routers into a system-level static root router:

  (def router
    (r/router
     [["/gin/napue" :napue]
      ["/ciders/*" :ciders]
      ["/beers/*" {:name :beers
                   :router beer-router}]
      ["/dynamic/*" {:name :dynamic
                     :router dynamic-router}]]))

  ;; matching root routes:

  (name-path router "/vodka/russian")
  (name-path router "/gin/napue")

  ;; matching (nested) beer routes:

  (name-path router "/beers/lager")
  (name-path router "/beers/saison")

  ;; no saison!? Let's add the route:

  (swap! beer-router add-routes [["/saison" :saison]])

  ;; there we have it:

  (name-path router "/beers/saison")

  ;; we can't add conflicting routes:

  (swap! beer-router add-routes [["/saison" :saison]])

  ;; the dynamic routes are re-created on every request:

  (name-path router "/dynamic/duo")
  (name-path router "/dynamic/duo")


  ;; performance

  ;; with nested routers, instead of having to do just one route match, matching
  ;; is recursive, which adds a small cost. All nested routers need to be of
  ;; type catch-all at top-level, which is order of magnitude slower than fully
  ;; static routes. Dynamic routes are the slowest ones, at least two orders of
  ;; magnitude slower, as the router needs to be recreated for each request.

  ;; a quick benchmark on the recursive lookups:
  ;;   path             time    type
  ;; - `/gin/napue`     40ns    static
  ;; - `/ciders/weston` 440ns   catch-all
  ;; - `/beers/saison`  600ns   catch-all + static
  ;; - `/dynamic/duo`   12000ns catch-all + dynamic

  ;; the non-recursive lookup for `/gin/napue` is around 23ns.

  ;; comparing the dynamic routing performance with Compojure:

  (def app
    (context "/dynamic" [] (constantly :duo)))

  (app {:uri "/dynamic/duo" :request-method :get})

  ;;   path           time    type
  ;; - `/dynamic/duo` 20000ns compojure

  ;; can we make the nester routing faster? Sure. We could use the Router
  ;; `:compile` hook to compile the nested routers for better performance. We
  ;; could also allow router creation rules to be disabled, to get the dynamic
  ;; routing much faster.


  ;; when to use nested routers?

  ;; nesting routers is not trivial and because of that, should be avoided. For
  ;; dynamic (request-time) route generation, it's the only choice. For other
  ;; cases, nested routes are most likely a better option.

  ;; let's re-create the previous example with normal route nesting/composition.

  ;; a helper to the root router:

  (defn create-router [beers]
    (r/router
     [["/gin/napue" :napue]
      ["/ciders/*" :ciders]
      ["/beers" (into [] (for [beer beers]
                           [(str "/" beer) (keyword "beer" beer)]))]
      ["/dynamic/*" {:name :dynamic
                     :router dynamic-router}]]))

  ;; new new root router reference and a helper to reset it:

  (def router
    (atom (create-router nil)))

  (defn reset-router! [beers]
    (reset! router (create-router beers)))

  ;; the routing tree:

  (r/routes @router)

  ;; let's reset the router with some beers:

  (reset-router! ["lager" "sahti" "bock"])

  ;; we can see that the beer routes are now embedded into the core router:

  (r/routes @router)

  ;; and the routing works:

  (name-path @router "/beers/sahti")

  ;; all the beer-routes now match in constant time.
  ;;   path           time type
  ;; - `/beers/sahti` 40ns static


  ;; TODO

  ;; - add an example how to do dynamic routing with `reitit-ring`
  ;; - maybe create a `recursive-router` into a separate ns with all Router
  ;;   functions implemented correctly? maybe not...
  ;; - add `reitit.core/merge-routes` to effectively merge routes with route data

)


;; different routers

(comment

  ;; reitit ships with several different implementations for the `Router`
  ;; protocol, originally based on the Pedestal implementation. `router`
  ;; function selects the most suitable implementation by inspecting the
  ;; expanded routes. The implementation can be set manually using `:router`
  ;; option, see Configuring Routers above.

  ;; router description
  ;; - `:linear-router`: matches the routes one-by-one starting from the top
  ;;   until a match is found. Slow, but works with all route trees.
  ;; - `:trie-router`: router that creates a optimized search trie out of an
  ;;   route table. Much faster than `:linear-router` for wildcard routes.
  ;;   Valid only if there are no route conflicts.
  ;; - `:lookup-router`: fast router, uses hash-lookup to resolve the route.
  ;;   Valid if no paths have path or catch-all parameters and there are no
  ;;   route conflicts.
  ;; - `:single-static-path-router`: super fast router: string-matches a route.
  ;;   Valid only if there is one static route.
  ;; - `:mixed-router`: contains two routers: `:trie-router` for wildcard routes
  ;;   and a `:lookup-router` or `:single-static-path-router` for static routes.
  ;;   Valid only if there are no route conflicts.
  ;; - `:quarantine-router`: contains two routers: `:mixed-router` for
  ;;   non-conflicting routes and a `:linear-router` for conflicting routes.

  ;; the router name can be asked from the router:

  (def router
    (r/router
     [["/ping" ::ping]
      ["/api/:users" ::users]]))

  (r/router-name router)

  ;; overriding the router implementation:

  (def router
    (r/router
     [["/ping" ::ping]
      ["/api/:users" ::users]]
     {:router r/linear-router}))

  (r/router-name router)

)


;; route validation

(comment

  ;; namespace `reitit.spec` contains `clojure.spec` definitions for raw-routes,
  ;; routes, router and router options.


  ;; example

  (def routes-from-db
    ["tenant1" ::tenant1])

  ;; according to docs, should be false; weird
  (s/valid? ::spec/raw-routes routes-from-db)
  (s/explain ::spec/raw-routes routes-from-db)

  ;; evidently, specs have changed (mightily relaxed) since docs were written
  ;; this one fail at last
  (s/valid? ::spec/raw-routes [1])
  (s/explain ::spec/raw-routes [1])


  ;; at development time

  ;; `reitit.core/router` can be instrumented and use a tool like `expound` to
  ;; pretty-print the spec problems.

  ;; first add a `:dev` dependency to:

  ;; [expound "0.4.0"] ; or higher

  ;; some bootstrapping:

  (stest/instrument `r/router)
  (set! s/*explain-out* expound/printer)

  ;; and we are ready to go:

  ;; should also fail, but no
  (def router (r/router
               ["/api"
                ["/public"
                 ["/ping"]
                 ["pong"]]]))

  (r/routes router)

  ;; this one will fail, but not as expected
  (r/router
   ["/api"
    ["/public"
     ["/ping"]
     [1 {}]]])

)


;; dev workflow

(comment

  ;; many applications will require the routes to span multiple namespaces. It
  ;; is quite easy to do so with reitit, but we might hit a problem during
  ;; development.


  ;; an example

  ;; consider this sample routing :

  (ns ns1)

  (def routes
    ["/bar" ::bar])

  (ns ns2)
  (require '[ns1])

  (def routes
    [["/ping" ::ping]
     ["/more" ns1/routes]])

  (ns ns3)
  (require '[ns1])
  (require '[ns2])
  (require '[reitit.core :as r])

  (def routes
    ["/api"
     ["/ns2" ns2/routes]
     ["/ping" ::ping]])

  (def router (r/router routes))

  ;; we may query the top router and get the expected result:

  (r/match-by-path router "/api/ns2/more/bar")
  ;; #reitit.core.Match{:template "/api/ns2/more/bar", :data
  ;; {:name :ns1/bar}, :result nil, :path-params {}, :path "/api/ns2/more/bar"}

  ;; notice the route name: `:ns1/bar`

  ;; when we change the routes in `ns1` like this:

  (ns ns1
    (:require [reitit.core :as r]))

  (def routes
    ["/bar" ::bar-with-new-name])

  ;; after we recompile the `ns1` namespace, and query again

  ns1/routes
  ;; ["/bar" :ns1/bar-with-new-name]
  ;; The routes var in `ns1` was changed indeed

  (r/match-by-path router "/api/ns2/more/bar")
  ;; #reitit.core.Match{:template "/api/ns2/more/bar", :data
  ;; {:name :ns1/bar}, :result nil, :path-params {}, :path "/api/ns2/more/bar"}

  ;; the route name is still `:ns1/bar`!

  ;; while we could use the reloaded workflow to reload the whole routing tree,
  ;; it is not always possible, and quite frankly a bit slower than we might
  ;; want for fast iterations.


  ;; a crude solution

  ;; in order to see the changes without reloading the whole route tree, we can
  ;; use functions.

  (ns ns1)

  (defn routes [] ;; Now a function !
    ["/bar" ::bar])

  (ns ns2)
  (require '[ns1])

  (defn routes [] ;; Now a function !
    [["/ping" ::ping]
     ["/more" (ns1/routes)]]) ;; Now a function call

  (ns ns3)
  (require '[ns1])
  (require '[ns2])
  (require '[reitit.core :as r])

  (defn routes [] ;; Now a function !
    ["/api"
     ["/ns2" (ns2/routes)] ;; Now a function call
     ["/ping" ::ping]])

  (def router #(r/router (routes))) ;; Now a function

  ;; let's query again

  (r/match-by-path (router) "/api/ns2/more/bar")
  ;; #reitit.core.Match{:template "/api/ns2/more/bar", :data
  ;; {:name :ns1/bar}, :result nil, :path-params {}, :path "/api/ns2/more/bar"}

  ;; notice that's we're now calling a function rather than just passing router
  ;; to the matching function.

  ;; now let's again change the route name in `ns1`, and recompile that
  ;; namespace.

  (ns ns1)

  (defn routes []
    ["/bar" ::bar-with-new-name])

  ;; let's see the query result:

  (r/match-by-path (router) "/api/ns2/more/bar")
  ;; #reitit.core.Match{:template "/api/ns2/more/bar", :data
  ;; {:name :ns1/bar-with-new-name}, :result nil, :path-params
  ;; {}, :path "/api/ns2/more/bar"}

  ;; notice that the name is now correct, without reloading every namespace
  ;; under the sun.


  ;; why is this a crude solution?

  ;; the astute reader will have noticed that we're recompiling the full routing
  ;; tree on every invocation. While this solution is practical during
  ;; development, it goes contrary to the performance goals of reitit.

  ;; we need a way to only do this once at production time.


  ;; an easy fix

  ;; let's apply a small change to our `ns3`. We'll replace our router by two
  ;; different routers, one for dev and one for production.

  (ns ns3)
  (require '[ns1])
  (require '[ns2])
  (require '[reitit.core :as r])

  (defn routes []
    ["/api"
     ["/ns2" (ns2/routes)]
     ["/ping" ::ping]])

  (def dev-router #(r/router (routes))) ;; A router for dev
  (def prod-router (constantly (r/router (routes)))) ;; A router for prod

  ;; and there you have it, dynamic during dev, performance at production. We
  ;; have it all!

)


;; shared routes

(comment

  ;; as `reitit-core` works with both Clojure & ClojureScript, one can have a
  ;; shared routing table for both the frontend and the backend application,
  ;; using the Clojure Common Files.

  ;; for backend, you need to define a `:handler` for the request processing,
  ;; for frontend, `:name` enables the use of reverse routing.

  ;; there are multiple options to use shared routing table.


  ;; using reader conditionals

  ;; define the handlers for clojure

  #?(:clj (declare get-kikka))
  #?(:clj (declare post-kikka))

  ;; `:name` for both, `:handler` just for clojure
  (def routes
    ["/kikka"
     {:name ::kikka
      #?@(:clj [:get {:handler get-kikka}])
      #?@(:clj [:post {:handler post-kikka}])}])


  ;; using custom expander

  ;; raw-routes can have any non-sequential data as a route argument, which gets
  ;; expanded using the `:expand` option given to the `reitit.core.router`
  ;; function. It defaults to `reitit.core/expand` multimethod.

  ;; first, define the common routes (in a .cljc file):

  (def routes
    [["/kikka" ::kikka]
     ["/bar" ::bar]])

  ;; those can be used as-is from ClojureScript:

  (def router
    (r/router routes))

  (r/match-by-name router ::kikka)

  ;; for the backend, we can use a custom expander to expand the routes:

  (defn my-expand [registry]
    (fn [data opts]
      (if (keyword? data)
        (some-> data
                registry
                (r/expand opts)
                (assoc :name data))
        (r/expand data opts))))

  ;; the handler functions

  (defn get-kikka [_] {:status 200, :body "get"})
  (defn post-kikka [_] {:status 200, :body "post"})
  (defn bar [_] {:status 200, :body "bar"})

  (def app
    (ring/ring-handler
     (ring/router
      [["/kikka" ::kikka]
       ["/bar" ::bar]]
      ;; use a custom expander
      {:expand (my-expand
                {::kikka {:get get-kikka
                          :post post-kikka}
                 ::bar bar})})))

  (app {:request-method :get, :uri "/kikka"})
  (app {:request-method :post, :uri "/kikka"})
  (app {:request-method :get, :uri "/bar"})

)
