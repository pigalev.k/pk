(ns hello-reitit.coercion
  (:require
   [clojure.spec.alpha :as s]
   [malli.util :as mu]
   [reitit.coercion :as coercion]
   [reitit.coercion.malli]
   [reitit.coercion.schema]
   [reitit.coercion.spec :as rcs]
   [reitit.core :as r]
   [schema.core :as sc]
   [spec-tools.core :as st]))


;; coercion

;; https://cljdoc.org/d/metosin/reitit/0.6.0/doc/coercion


;; coercion explained

(comment

  ;; coercion is a process of transforming parameters (and responses) from one
  ;; format into another. Reitit separates routing and coercion into two
  ;; separate steps.

  ;; by default, all wildcard and catch-all parameters are parsed into strings:

  (def router
    (r/router
     ["/:company/users/:user-id" ::user-view]))

  ;; match with the parsed `:path-params` as strings:

  (r/match-by-path router "/metosin/users/123")

  ;; to enable parameter coercion, the following things need to be done:
  ;; - define a `Coercion` for the routes
  ;; - define types for the parameters
  ;; - compile coercers for the types
  ;; - apply the coercion


  ;; define coercion

  ;; `reitit.coercion/Coercion` is a protocol defining how types are defined,
  ;; coerced and inventoried.

  ;; reitit ships with the following coercion modules:

  ;; - `reitit.coercion.malli/coercion` for malli
  ;; - `reitit.coercion.schema/coercion` for plumatic schema
  ;; - `reitit.coercion.spec/coercion` for both `clojure.spec` and data-specs
  ;;   (`spec-tools`)

  ;; coercion can be attached to route data under `:coercion` key. There can be
  ;; multiple `Coercion` implementations within a single router, normal scoping
  ;; rules apply.


  ;; defining parameters

  ;; route parameters can be defined via route data `:parameters`. It has keys
  ;; for different type of parameters: `:query`, `:body`, `:form`, `:header` and
  ;; `:path`. Syntax for the actual parameters depends on the `Coercion`
  ;; implementation.

  ;; example with Schema path-parameters:

  (def router
    (r/router
     ["/:company/users/:user-id" {:name ::user-view
                                  :coercion reitit.coercion.schema/coercion
                                  :parameters {:path {:company sc/Str
                                                      :user-id sc/Int}}}]))

  ;; a `Match`:

  (r/match-by-path router "/metosin/users/123")

  ;; coercion was not applied. Why? In Reitit, routing and coercion are separate
  ;; processes and we have done just the routing part. We need to apply coercion
  ;; after the successful routing.

  ;; but now we should have enough data on the match to apply the coercion.


  ;; compiling coercers

  ;; before the actual coercion, we need to compile the coercers against the
  ;; route data. Compiled coercers yield much better performance and the manual
  ;; step of adding a coercion compiler makes things explicit and non-magical.

  ;; compiling can be done via a Middleware, Interceptor or a Router. We apply
  ;; it now at router-level, effecting all routes (with `:parameters`
  ;; and `:coercion` defined).

  ;; there is a helper function `reitit.coercion/compile-request-coercers` just
  ;; for this:

  (def router
    (r/router
     ["/:company/users/:user-id" {:name ::user-view
                                  :coercion reitit.coercion.schema/coercion
                                  :parameters {:path {:company sc/Str
                                                      :user-id sc/Int}}}]
     {:compile coercion/compile-request-coercers}))

  ;; routing again:

  (r/match-by-path router "/metosin/users/123")

  ;; the compiler added a `:result` key into the match (done just once, at
  ;; router creation time), which holds the compiled coercers. We are almost
  ;; done.


  ;; applying coercion

  ;; we can use a helper function `reitit.coercion/coerce!` to do the actual
  ;; coercion, based on a `Match`:

  (coercion/coerce!
   (r/match-by-path router "/metosin/users/123"))

  ;; we get the coerced parameters back. If a coercion fails, a
  ;; typed (`:reitit.coercion/request-coercion`) `ExceptionInfo` is thrown, with
  ;; data about the actual error:

  (coercion/coerce!
   (r/match-by-path router "/metosin/users/ikitommi"))


  ;; full example

  ;; here's a full example for doing routing and coercion with Reitit and
  ;; Schema:

  (def router
    (r/router
     ["/:company/users/:user-id" {:name ::user-view
                                  :coercion reitit.coercion.schema/coercion
                                  :parameters {:path {:company sc/Str
                                                      :user-id sc/Int}}}]
     {:compile coercion/compile-request-coercers}))

  (defn match-by-path-and-coerce! [path]
    (when-let [match (r/match-by-path router path)]
      (assoc match :parameters (coercion/coerce! match))))

  (match-by-path-and-coerce! "/metosin/users/123")
  (match-by-path-and-coerce! "/metosin/users/ikitommi")


  ;; Ring coercion

  ;; for a full-blown http-coercion, see the examples in the Ring guide.

)


;; Plumatic Schema coercion

(comment

  ;; Plumatic Schema is a Clojure(Script) library for declarative data
  ;; description and validation.

  (def router
    (r/router
     ["/:company/users/:user-id" {:name ::user-view
                                  :coercion reitit.coercion.schema/coercion
                                  :parameters {:path {:company sc/Str
                                                      :user-id sc/Int}}}]
     {:compile coercion/compile-request-coercers}))

  (defn match-by-path-and-coerce! [path]
    (when-let [match (r/match-by-path router path)]
      (assoc match :parameters (coercion/coerce! match))))

  ;; successful coercion:

  (match-by-path-and-coerce! "/metosin/users/123")

  ;; failing coercion:

  (match-by-path-and-coerce! "/metosin/users/ikitommi")

)


;; `clojure.spec` coercion

(comment

  ;; the `clojure.spec` library specifies the structure of data, validates or
  ;; destructures it, and can generate data based on the spec.

  ;; warning: `clojure.spec` by itself doesn't support coercion. reitit uses
  ;; `spec-tools` that adds coercion to spec. Like `clojure.spec`, it's alpha as
  ;; it leans both on spec walking and `clojure.spec.alpha/conform`, which is
  ;; considered a spec internal, that might be changed or removed later.


  ;; usage

  ;; for simple specs (core predicates, `spec-tools.core/spec`, `s/and`, `s/or`,
  ;; `s/coll-of`, `s/keys`, `s/map-of`, `s/nillable` and `s/every`), the
  ;; transformation is inferred using spec-walker and is automatic. To support
  ;; all specs (like regex-specs), specs need to be wrapped into Spec Records.


  ;; example

  ;; simple specs, inferred

  (s/def ::company string?)
  (s/def ::user-id int?)
  (s/def ::path-params (s/keys :req-un [::company ::user-id]))

  (def router
    (r/router
     ["/:company/users/:user-id" {:name ::user-view
                                  :coercion reitit.coercion.spec/coercion
                                  :parameters {:path ::path-params}}]
     {:compile coercion/compile-request-coercers}))

  (defn match-by-path-and-coerce! [path]
    (when-let [match (r/match-by-path router path)]
      (assoc match :parameters (coercion/coerce! match))))

  ;; successful coercion:

  (match-by-path-and-coerce! "/metosin/users/123")

  ;; failing coercion:

  (match-by-path-and-coerce! "/metosin/users/ikitommi")


  ;; deeply nested

  ;; spec-tools allow deeply nested specs to be coerced. One can test the
  ;; coercion easily in the REPL.

  ;; define some specs:

  (s/def :sku/id keyword?)
  (s/def ::sku (s/keys :req-un [:sku/id]))
  (s/def ::skus (s/coll-of ::sku :into []))

  (s/def :photo/id int?)
  (s/def ::photo (s/keys :req-un [:photo/id]))
  (s/def ::photos (s/coll-of ::photo :into []))

  (s/def ::my-json-api (s/keys :req-un [::skus ::photos]))

  ;; apply a string->edn coercion to the data:

  (st/coerce
   ::my-json-api
   {:skus [{:id "123"}]
    :photos [{:id "123"}]}
   st/string-transformer)

  ;; apply a json->edn coercion to the data:

  (st/coerce
   ::my-json-api
   {:skus [{:id "123"}]
    :photos [{:id "123"}]}
   st/json-transformer)

  ;; by default, reitit uses custom transformers that also strip out extra keys
  ;; from `s/keys` specs:

  (st/coerce
   ::my-json-api
   {:TOO "MUCH"
    :skus [{:id "123"
            :INFOR "MATION"}]
    :photos [{:id "123"
              :HERE "TOO"}]}
   rcs/json-transformer)


  ;; defining optional keys

  ;; suppose you want the `::my-json-api` to have optional remarks as string and
  ;; each photo to have an optional height and width as integer. The `s/keys`
  ;; accepts `:opt-un` to support optional keys.

  (s/def :sku/id keyword?)
  (s/def ::sku (s/keys :req-un [:sku/id]))
  (s/def ::skus (s/coll-of ::sku :into []))
  (s/def ::remarks string?)  ;; define remarks as string

  (s/def :photo/id int?)
  (s/def :photo/height int?) ;; define height as int
  (s/def :photo/width int?)  ;; define width as int
  (s/def ::photo (s/keys :req-un [:photo/id]
                         :opt-un [:photo/height :photo/width]))
  (s/def ::photos (s/coll-of ::photo :into []))

  (s/def ::my-json-api (s/keys :req-un [::skus ::photos]
                               :opt-un [::remarks]))

  ;; apply a string->edn coercion to the data:

  ;; omit optional keys

  (st/coerce
   ::my-json-api
   {:skus [{:id "123"}]
    :photos [{:id "123"}]}
   st/string-transformer)

  ;; coerce the optional keys if present

  (st/coerce
   ::my-json-api
   {:skus [{:id "123"}]
    :photos [{:id "123" :height "100" :width "100"}]
    :remarks "some remarks"}
   st/string-transformer)

  (st/coerce
   ::my-json-api
   {:skus [{:id "123"}]
    :photos [{:id "123" :height "100"}]}
   st/string-transformer)

)


;; data-spec coercion

(comment

  ;; data-specs is alternative, macro-free syntax to define `clojure.spec`s. As
  ;; a bonus, supports the runtime transformations via conforming
  ;; out-of-the-box.

  (def router
    (r/router
     ["/:company/users/:user-id" {:name ::user-view
                                  :coercion reitit.coercion.spec/coercion
                                  :parameters {:path {:company string?
                                                      :user-id int?}}}]
     {:compile coercion/compile-request-coercers}))

  (defn match-by-path-and-coerce! [path]
    (when-let [match (r/match-by-path router path)]
      (assoc match :parameters (coercion/coerce! match))))

  ;; successful coercion:

  (match-by-path-and-coerce! "/metosin/users/123")

  ;; failing coercion:

  (match-by-path-and-coerce! "/metosin/users/ikitommi")

)


;; malli coercion

(comment

  ;; malli is data-driven Schema library for Clojure/Script.


  ;; default syntax

  ;; by default, vector syntax is used:

  (def router
    (r/router
     ["/:company/users/:user-id" {:name       ::user-view
                                  :coercion   reitit.coercion.malli/coercion
                                  :parameters {:path [:map
                                                      [:company string?]
                                                      [:user-id int?]]}}]
     {:compile coercion/compile-request-coercers}))

  (defn match-by-path-and-coerce! [path]
    (when-let [match (r/match-by-path router path)]
      (assoc match :parameters (coercion/coerce! match))))

  ;; successful coercion:

  (match-by-path-and-coerce! "/metosin/users/123")

  ;; failing coercion:

  (match-by-path-and-coerce! "/metosin/users/ikitommi")


  ;; lite syntax

  ;; same using lite syntax:

  (def router
    (r/router
     ["/:company/users/:user-id" {:name       ::user-view
                                  :coercion   reitit.coercion.malli/coercion
                                  :parameters {:path {:company string?
                                                      :user-id int?}}}]
     {:compile coercion/compile-request-coercers}))


  ;; configuring coercion

  ;; using `create` with options to create the coercion instead of `coercion`:

  (def malli-coercion
    (reitit.coercion.malli/create
     {:transformers
      {:body
       {:default reitit.coercion.malli/default-transformer-provider
        :formats
        {"application/json" reitit.coercion.malli/json-transformer-provider}}
       :string   {:default reitit.coercion.malli/string-transformer-provider}
       :response {:default reitit.coercion.malli/default-transformer-provider}}
      ;; set of keys to include in error messages
      :error-keys
      #{:type :coercion :in :schema :value :errors :humanized #_:transformed}
      ;; support lite syntax?
      :lite             true
      ;; schema identity function (default: close all map schemas)
      :compile          mu/closed-schema
      ;; validate request & response
      :validate         true
      ;; top-level short-circuit to disable request & response coercion
      :enabled          true
      ;; strip-extra-keys (effects only predefined transformers)
      :strip-extra-keys true
      ;; add/set default values
      :default-values   true
      ;; malli options
      :options          nil}))

  (def router
    (r/router
     ["/:company/users/:user-id" {:name       ::user-view
                                  :coercion   malli-coercion
                                  :parameters {:path {:company string?
                                                      :user-id int?}}}]
     {:compile coercion/compile-request-coercers}))

  ;; successful coercion:

  (match-by-path-and-coerce! "/metosin/users/123")

  ;; failing coercion:

  (match-by-path-and-coerce! "/metosin/users/ikitommi")

)
