(ns hello-reitit.frontend
  (:require
   [reitit.frontend.controllers :as rfc]
   [reitit.frontend.easy :as rfe]))


;; Frontend

;; https://cljdoc.org/d/metosin/reitit/0.6.0/doc/frontend


;; basics

(comment

  ;; reitit frontend integration is built from multiple layers:

  ;; - core functions with some additional browser-oriented features
  ;; - browser integration for attaching Reitit to hash-change or HTML history
  ;;   events
  ;; - stateful wrapper for easy use of history integration
  ;; - optional controller extension


  ;; core functions

  ;; `reitit.frontend` provides some useful functions wrapping core functions:

  ;; - `match-by-path` version which parses a URI using JavaScript, including
  ;;   query-string, and also coerces the parameters. Coerced parameters are
  ;;   stored in match `:parameters` property. If coercion is not enabled, the
  ;;   original parameters are stored in the same property, to allow the same
  ;;   code to read parameters regardless if coercion is enabled.
  ;; - `router` which compiles coercers by default.
  ;; - `match-by-name` and `match-by-name!` with optional path-paramers and
  ;;   logging errors to `console.warn` instead of throwing errors to prevent
  ;;   React breaking due to errors.

)


;; browser integration

(comment

  ;; reitit includes two browser history integrations.

  ;; functions follow HTML5 History API: `push-state` to change route,
  ;; `replace-state` to change route without leaving previous entry in browser
  ;; history.


  ;; fragment router

  ;; fragment is simple integration which stores the current route in URL
  ;; fragment, i.e. after `#`. This means the route is never part of the request
  ;; URI and server will always know which file to return ("index.html").


  ;; HTML5 router

  ;; HTML5 History API can be used to modify the URL in browser without making
  ;; request to the server. This means the URL will look normal, but the
  ;; downside is that the server must respond to all routes with correct
  ;; file ("index.html"). Check examples for simple Ring handler example.


  ;; anchor click handling

  ;; HTML5 History router will handle click events on anchors where the href
  ;; matches the route tree (and other rules). If you have need to control this
  ;; logic, for example to handle some anchor clicks where the href matches
  ;; route tree normally (i.e. browser load) you can provide
  ;; `:ignore-anchor-click?` function to add your own logic to event handling:

  (rfe/start!
   router
   on-navigate-fn
   {:use-fragment false
    :ignore-anchor-click?
    (fn [router e el uri]
      ;; Add additional check on top of the default checks
      (and (rfh/ignore-anchor-click? router e el uri)
           (not= "false" (gobj/get (.-dataset el) "reititHandleClick"))))})

  ;; use data-reitit-handle-click to disable Reitit anchor handling
  [:a
   {:href (rfe/href ::about)
    :data-reitit-handle-click false}
   "About"]


  ;; easy

  ;; reitit frontend routers require storing the state somewhere and passing it
  ;; to all the calls. Wrapper `reitit.frontend.easy` is provided which manages
  ;; a router instance and passes the instance to all calls. This should allow
  ;; easy use in most applications, as browser anyway can only have single event
  ;; handler for page change events.


  ;; history manipulation

  ;; reitit doesn't include functions to manipulate the history stack, i.e. go
  ;; back or forwards, but calling History API functions directly should work:

  (.go js/window.history -1)
  ;; or
  (.back js/window.history)

)


;; controllers

(comment

  ;; https://github.com/metosin/reitit/tree/master/examples/frontend-controllers

  ;; controllers run code when a route is entered and left. This can be useful
  ;; to:

  ;; - load resources
  ;; - update application state


  ;; how controllers work

  ;; a controller map can contain these properties:

  ;; - `identity` function which takes a `Match` and returns an arbitrary value,
  ;; - or `parameters` value, which declares which parameters should affect
  ;;   controller identity
  ;; - `start` & `stop` functions, which are called with controller identity

  ;; when you navigate to a route that has a controller, controller identity is
  ;; first resolved by using parameters declaration, or by calling identity
  ;; function, or if neither is set, the identity is nil. Next, the controller
  ;; is initialized by calling start with the controller identity value. When
  ;; you exit that route, stop is called with the last controller identity
  ;; value.

  ;; if you navigate to the same route with different match, identity gets
  ;; resolved again. If the identity changes from the previous value, controller
  ;; is reinitialized: `stop` and `start` get called again.

  ;; you can add controllers to a route by adding them to the route data in
  ;; the `:controllers` vector. For example:

  ["/item/:id"
   {:controllers
    [{:parameters {:path [:id]}
      :start  (fn [parameters]
                (js/console.log :start (-> parameters :path :id)))
      :stop   (fn [parameters]
                (js/console.log :stop (-> parameters :path :id)))}]}]

  ;; you can leave out start or stop if you do not need both of them.


  ;; enabling controllers

  ;; you need to call `reitit.frontend.controllers/apply-controllers` whenever
  ;; the URL changes. You can call it from the `on-navigate` callback of
  ;; `reitit.frontend.easy`:

  (defonce match-a (atom nil))

  (def routes
    ["/" ...])

  (defn init! []
    (rfe/start!
     routes
     (fn [new-match]
       (swap! match-a
              (fn [old-match]
                (when new-match
                  (assoc new-match
                         :controllers (rfc/apply-controllers
                                       (:controllers old-match) new-match))))))))

  ;; see also the full example:
  ;; https://github.com/metosin/reitit/tree/master/examples/frontend-controllers


  ;; nested controllers

  ;; when you nest routes in the route tree, the controllers get concatenated
  ;; when route data is merged. Consider this route tree:

  ["/" {:controllers [{:start (fn [_] (js/console.log "root start"))}]}
   ["/item/:id"
    {:controllers
     [{:parameters {:path [:id]}
       :start (fn [parameters]
                (js/console.log "item start" (-> parameters :path :id)))
       :stop  (fn [parameters]
                (js/console.log "item stop" (-> parameters :path :id)))}]}]]

  ;; - when you navigate to any route at all, the root controller gets started.
  ;; - if you navigate to "/item/something", the root controller gets started
  ;;   first and then the item controller gets started.
  ;; - if you then navigate from "/item/something" to "/item/something-else",
  ;;   first the item controller gets stopped with parameter `something` and then
  ;;   it gets started with the parameter `something-else`. The root controller
  ;;   stays on the whole time since its parameters do not change.


  ;; tips

  ;; authentication

  ;; controllers can be used to load resources from a server. If and when your
  ;; API requires authentication you will need to implement logic to prevent
  ;; controllers trying to do requests if user isn't authenticated yet.

  ;; run controllers and check authentication

  ;; If you have both unauthenticated and authenticated resources, you can run
  ;; the controllers always and then check the authentication status on
  ;; controller code, or on the code called from controllers (e.g. re-frame
  ;; event handler).

  ;; disable controllers until user is authenticated

  ;; if all your resources require authentication an easy way to prevent bad
  ;; requests is to enable controllers only after authentication is done. To do
  ;; this you can check authentication status and call `apply-controllers` only
  ;; after authentication is done (also remember to manually call
  ;; `apply-controllers` with current match when authentication is done). Or if
  ;; no navigation is possible before authentication is done, you can start the
  ;; router only after authentication is done.


  ;; alternatives

  ;; similar solution could be used to describe required resources as
  ;; data (maybe even GraphQL query) per route, and then have code automatically
  ;; load missing resources.


  ;; controllers elsewhere

  ;; controllers in Keechma: https://keechma.com/guides/controllers/

)
