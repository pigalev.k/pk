(ns hello-reitit.ring
  (:require
   [buddy.auth.accessrules :as accessrules]
   [clojure.core :refer [slurp]]
   [clojure.java.io :as io]
   [clojure.set :as set]
   [clojure.spec.alpha :as spec]
   [clojure.string :as string]
   [expound.alpha :as expound]
   [muuntaja.core :as m]
   [reitit.coercion :as rc]
   [reitit.coercion.schema]
   [reitit.coercion.spec :as rcs]
   [reitit.core :as r]
   [reitit.middleware :as middleware]
   [reitit.ring :as ring]
   [reitit.ring.coercion :as rrc]
   [reitit.ring.middleware.dev :as dev]
   [reitit.ring.middleware.exception :as exception]
   [reitit.ring.middleware.multipart :as multipart]
   [reitit.ring.middleware.muuntaja :as muuntaja]
   [reitit.ring.middleware.parameters :as parameters]
   [reitit.ring.spec :as rrs]
   [reitit.spec :as rs]
   [reitit.swagger :as swagger]
   [reitit.swagger-ui :as swagger-ui]
   [ring.adapter.jetty :as jetty]
   [ring.middleware.params :as params]
   [schema.core :as s])
  (:import
   (java.sql SQLException)))


;; Ring

;; https://cljdoc.org/d/metosin/reitit/0.6.0/doc/ring


;; Ring router

(comment

  ;; Ring is a Clojure web applications library inspired by Python's WSGI and
  ;; Ruby's Rack. By abstracting the details of HTTP into a simple, unified API,
  ;; Ring allows web applications to be constructed of modular components that
  ;; can be shared among a variety of applications, web servers, and web
  ;; frameworks.

  ;; [metosin/reitit-ring "0.6.0"]


  ;; `reitit.ring/router`

  ;; `reitit.ring/router` is a higher order router, which adds support for
  ;; `:request-method` based routing, handlers and middleware.

  ;; it accepts the following options:
  ;; - `:reitit.middleware/transform`: function of [Middleware] => [Middleware]
  ;;   to transform the expanded Middleware (default: `identity`).
  ;; - `:reitit.middleware/registry`: map of keyword => `IntoMiddleware` to
  ;;   replace keyword references into Middleware
  ;; - `:reitit.ring/default-options-endpoint`: default endpoint for `:options`
  ;;   method (default: default-options-endpoint)


  ;; example router:

  (defn handler [_]
    {:status 200, :body "ok"})

  (def router
    (ring/router
     ["/ping" {:get handler}]))

  ;; match contains `:result` compiled by `reitit.ring/router`:

  (r/match-by-path router "/ping")


  ;; `reitit.ring/ring-handler`

  ;; given a router from `reitit.ring/router`, optional default-handler &
  ;; options, `ring-handler` function will return a valid ring handler
  ;; supporting both synchronous and asynchronous request handling. The
  ;; following options are available:
  ;; - `:middleware`: optional sequence of middlewares that wrap the ring-handler
  ;; - `:inject-match?`: boolean to inject match into request under
  ;;   `:reitit.core/match` key (default true)
  ;; - `:inject-router?` boolean to inject router into request under
  ;;   `:reitit.core/router` key (default true)

  ;; simple Ring app:

  (def app (ring/ring-handler router))

  ;; applying the handler:

  (app {:request-method :get, :uri "/favicon.ico"})

  (app {:request-method :get, :uri "/ping"})

  ;; the router can be accessed via `get-router`:

  (-> app (ring/get-router) (r/compiled-routes))


  ;; request-method based routing

  ;; handlers can be placed either to the top-level (all methods) or under a
  ;; specific method (`:get`, `:head`, `:patch`, `:delete`, `:options`, `:post`,
  ;; `:put` or `:trace`). Top-level handler is used if request-method based
  ;; handler is not found.

  ;; by default, the `:options` route is generated for all paths - to enable
  ;; thing like CORS.

  (def app
    (ring/ring-handler
     (ring/router
      [["/all" handler]
       ["/ping" {:name ::ping
                 :get handler
                 :post handler}]])))

  ;; top-level handler catches all methods:

  (app {:request-method :delete, :uri "/all"})

  ;; method-level handler catches only the method:

  (app {:request-method :get, :uri "/ping"})

  (app {:request-method :put, :uri "/ping"})

  ;; by default, `:options` is also supported (see router options to change
  ;; this):

  (app {:request-method :options, :uri "/ping"})


  ;; name-based reverse routing:

  (-> app
      (ring/get-router)
      (r/match-by-name ::ping)
      (r/match->path))


  ;; middleware

  ;; middleware can be mounted using a `:middleware` key - either to top-level
  ;; or under request method submap. Its value should be a vector of
  ;; `reitit.middleware/IntoMiddleware` values. These include:

  ;; - normal ring middleware function `handler -> request -> response`
  ;; - vector of middleware function `[handler args*] -> request -> response`
  ;;   and it's arguments
  ;; - a data-driven middleware record or a map
  ;; - a keyword name, to lookup the middleware from a Middleware Registry

  ;; a middleware and a handler:

  (defn wrap [handler id]
    (fn [request]
      (handler (update request ::acc (fnil conj []) id))))

  (defn handler [{::keys [acc]}]
    {:status 200, :body (conj acc :handler)})

  ;; app with nested middleware:

  (def app
    (ring/ring-handler
     (ring/router
      ;; a middleware function
      ["/api" {:middleware [#(wrap % :api)]}
       ["/ping" handler]
       ;; a middleware vector at top level
       ["/admin" {:middleware [[wrap :admin]]}
        ["/db" {:middleware [[wrap :db]]
                ;; a middleware vector at under a method
                :delete {:middleware [[wrap :delete]]
                         :handler handler}}]]])))

  ;; middleware is applied correctly:

  (app {:request-method :delete, :uri "/api/ping"})

  (app {:request-method :delete, :uri "/api/admin/db"})

  ;; top-level middleware, applied before any routing is done:

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [[wrap :api]]}
       ["/get" {:get handler}]])
     nil
     {:middleware [[wrap :top]]}))

  (app {:request-method :get, :uri "/api/get"})

)


;; reverse routing with Ring

(comment

  ;; both the router and the match are injected into Ring Request (as
  ;; `::r/router` and `::r/match`) by the `reitit.ring/ring-handler` and with
  ;; that, available to middleware and endpoints. To convert a `Match` into a
  ;; path, one can use `r/match->path`, which optionally takes a map of
  ;; query-parameters too.

  ;; below is an example how to do reverse routing from a ring handler:

  (def app
    (ring/ring-handler
     (ring/router
      [["/users"
        {:get (fn [{::r/keys [router]}]
                {:status 200
                 :body (for [i (range 10)]
                         {:uri (-> router
                                   (r/match-by-name ::user {:id i})
                                   ;; with extra query-params
                                   (r/match->path {:iso "möly"}))})})}]
       ["/users/:id"
        {:name ::user
         :get (constantly {:status 200, :body "user..."})}]])))

  (app {:request-method :get, :uri "/users"})
  (app {:request-method :get, :uri "/users/12"})

)


;; default handler

(comment

  ;; by default, if no routes match, nil is returned, which is not a valid
  ;; response in Ring:

  (defn handler [_]
    {:status 200, :body ""})

  (def app
    (ring/ring-handler
     (ring/router
      ["/ping" handler])))

  (app {:uri "/invalid"})

  ;; setting the default-handler as a second argument to `ring-handler`:

  (def app
    (ring/ring-handler
     (ring/router
      ["/ping" handler])
     (constantly {:status 404, :body ""})))

  (app {:uri "/invalid"})

  ;; to get more correct http error responses, `ring/create-default-handler` can
  ;; be used. It differentiates `:not-found` (no route matched),
  ;; `:method-not-allowed` (no method matched) and `:not-acceptable` (handler
  ;; returned nil).

  ;; with defaults:

  (def app
    (ring/ring-handler
     (ring/router
      [["/ping" {:get handler}]
       ["/pong" (constantly nil)]])
     (ring/create-default-handler)))

  (app {:request-method :get, :uri "/ping"})
  (app {:request-method :get, :uri "/"})
  (app {:request-method :post, :uri "/ping"})
  (app {:request-method :get, :uri "/pong"})

  ;; with custom responses:

  (def app
    (ring/ring-handler
     (ring/router
      [["/ping" {:get handler}]
       ["/pong" (constantly nil)]])
     (ring/create-default-handler
      {:not-found (constantly {:status 404, :body "kosh"})
       :method-not-allowed (constantly {:status 405, :body "kosh"})
       :not-acceptable (constantly {:status 406, :body "kosh"})})))

  (app {:request-method :get, :uri "/ping"})
  (app {:request-method :get, :uri "/"})
  (app {:request-method :post, :uri "/ping"})
  (app {:request-method :get, :uri "/pong"})

)


;; slash handler

(comment

  ;; The router works with precise matches. If a route is defined without a
  ;; trailing slash, for example, it won't match a request with a slash.

  (def app
    (ring/ring-handler
     (ring/router
      ["/ping" (constantly {:status 200, :body ""})])))

  (app {:uri "/ping/"})

  ;; sometimes it is desirable that paths with and without a trailing slash are
  ;; recognized as the same.

  ;; setting the `redirect-trailing-slash-handler` as a second argument to
  ;; `ring-handler`:

  (def app
    (ring/ring-handler
     (ring/router
      [["/ping" (constantly {:status 200, :body ""})]
       ["/pong/" (constantly {:status 200, :body ""})]])
     (ring/redirect-trailing-slash-handler)))

  (app {:uri "/ping/"})
  (app {:uri "/pong"})

  ;; `redirect-trailing-slash-handler` accepts an optional `:method` parameter
  ;; that allows configuring how (whether) to handle missing/extra slashes. The
  ;; default is to handle both.

  (def app
    (ring/ring-handler
     (ring/router
      [["/ping" (constantly {:status 200, :body ""})]
       ["/pong/" (constantly {:status 200, :body ""})]])
     ;; only handle extra trailing slash
     (ring/redirect-trailing-slash-handler {:method :strip})))

  (app {:uri "/ping/"})
  (app {:uri "/pong"})

  (def app
    (ring/ring-handler
     (ring/router
      [["/ping" (constantly {:status 200, :body ""})]
       ["/pong/" (constantly {:status 200, :body ""})]])
     ;; only handle missing trailing slash
     (ring/redirect-trailing-slash-handler {:method :add})))

  (app {:uri "/ping/"})
  (app {:uri "/pong"})

  ;; `redirect-trailing-slash-handler` can be composed with the default handler
  ;; using `ring/routes` for more correct http error responses:

  (def app
    (ring/ring-handler
     (ring/router
      [["/ping" (constantly {:status 200, :body ""})]
       ["/pong/" (constantly {:status 200, :body ""})]])
     (ring/routes
      (ring/redirect-trailing-slash-handler {:method :add})
      (ring/create-default-handler))))

  (app {:uri "/ping/"})
  (app {:uri "/pong"})

)


;; static resources (Clojure only)

(comment


  ;; static resources can be served by using the following two functions:

  ;; - `reitit.ring/create-resource-handler`, which returns a Ring handler
  ;;   that serves files from classpath, and
  ;; - `reitit.ring/create-file-handler`, which returns a Ring handler that
  ;;   serves files from file system

  ;; there are two ways to mount the handlers. The examples below use
  ;; `reitit.ring/create-resource-handler`, but
  ;; `reitit.ring/create-file-handler` works the same way.


  ;; internal routes

  ;; this is good option if static files can be from non-conflicting paths,
  ;; e.g. "/assets/*".

  (ring/ring-handler
   (ring/router
    [["/ping" (constantly {:status 200, :body "pong"})]
     ["/assets/*" (ring/create-resource-handler)]])
   (ring/create-default-handler))

  ;; to serve static files with conflicting routes, e.g. "/*", one needs to
  ;; disable the conflict resolution:

  (ring/ring-handler
   (ring/router
    [["/ping" (constantly {:status 200, :body "pong"})]
     ["/*" (ring/create-resource-handler)]]
    {:conflicts (constantly nil)})
   (ring/create-default-handler))


  ;; external routes

  ;; a better way to serve files from conflicting paths, e.g. "/*", is to serve
  ;; them from the default-handler. One can compose multiple default locations
  ;; using `reitit.ring/ring-handler`. This way, they are only served if none of
  ;; the actual routes have matched.

  (ring/ring-handler
   (ring/router
    ["/ping" (constantly {:status 200, :body "pong"})])
   (ring/routes
    (ring/create-resource-handler {:path "/"})
    (ring/create-default-handler)))


  ;; configuration

  ;; `reitit.ring/create-file-handler` and `reitit.ring/create-resource-handler`
  ;; take optionally an options map to configure how the files are being served.
  ;; - `:parameter`: optional name of the wildcard parameter, defaults to
  ;;   unnamed keyword `:`
  ;; - `:root`: optional resource root, defaults to "public"
  ;; - `:path`: path to mount the handler to. Required when mounted outside of
  ;;   a router, does not work inside a router.
  ;; - `:loader`: optional class loader to resolve the resources
  ;; - `:index-files`: optional vector of index-files to look in a resource
  ;;   directory, defaults to ["index.html"]
  ;; - `:not-found-handler`: optional handler function to use if the requested
  ;;   resource is missing (404 Not Found)

  ;; TODO

  ;; - support for things like `:cache`, `:etag`, `:last-modified?`, and `:gzip`
  ;; - support for ClojureScript

)


;; dynamic extensions

(comment

  ;; `ring-handler` injects the `Match` into a request and it can be extracted
  ;; at runtime with `reitit.ring/get-match`. This can be used to build ad hoc
  ;; extensions to the system.

  ;; this example shows a middleware to guard routes based on user roles:

  (defn wrap-enforce-roles [handler]
    (fn [{:keys [my-roles] :as request}]
      (let [required (some-> request (ring/get-match) :data ::roles)]
        (if (and (seq required) (not (set/subset? required my-roles)))
          {:status 403, :body "forbidden"}
          (handler request)))))

  ;; mounted to an app via router data (affecting all routes):

  (def handler (constantly {:status 200, :body "ok"}))

  (def app
    (ring/ring-handler
     (ring/router
      [["/api"
        ["/ping" handler]
        ["/admin" {::roles #{:admin}}
         ["/ping" handler]]]]
      {:data {:middleware [wrap-enforce-roles]}})))

  ;; anonymous access to public route:

  (app {:request-method :get, :uri "/api/ping"})

  ;; anonymous access to guarded route:

  (app {:request-method :get, :uri "/api/admin/ping"})

  ;; authorized access to guarded route:

  (app {:request-method :get, :uri "/api/admin/ping", :my-roles #{:admin}})

  ;; dynamic extensions are nice, but we can do much better. See Data-driven
  ;; Middleware and Compiling Middleware below.

)


;; data-driven middleware

(comment

  ;; Ring defines middleware as a function of type `handler & args => request =>
  ;; response.` It is relatively easy to understand and allows for good
  ;; performance. A downside is that the middleware chain is just an opaque
  ;; function, making things like debugging and composition hard. It is too easy
  ;; to apply the middlewares in wrong order.

  ;; reitit defines middleware as data:

  ;; - a middleware can be defined as first-class data entries
  ;; - a middleware can be mounted as a duct-style vector (of middlewares)
  ;; - a middleware can be optimized & compiled against an endpoint
  ;; - a middleware chain can be transformed by the router


  ;; middleware as data

  ;; all values in the `:middleware` vector of route data are expanded into
  ;; `reitit.middleware/Middleware` records by using the
  ;; `reitit.middleware/IntoMiddleware` protocol. By default, functions, maps
  ;; and `Middleware` records are allowed.

  ;; records can have arbitrary keys, but the following keys have special
  ;; purpose:
  ;; - `:name`: name of the middleware as a qualified keyword
  ;; - `:spec`: `clojure.spec` definition for the route data, see route data
  ;;   validation (optional)
  ;; - `:wrap`: the actual middleware function of handler & `args => request
  ;;   => response`
  ;; - `:compile`: middleware compilation function, see Compiling Middleware.

  ;; `Middleware` records are accessible in their raw form in the compiled route
  ;; results, and thus are available for inventories, creating api-docs, etc.

  ;; for the actual request processing, the records are unwrapped into normal
  ;; functions and composed into a middleware function chain, yielding zero
  ;; runtime penalty.


  ;; creating middleware

  ;; the following examples produce identical middleware runtime functions.

  ;; function

  (defn wrap [handler id]
    (fn [request]
      (handler (update request ::acc (fnil conj []) id))))

  ;; map

  (def wrap3
    {:name ::wrap3
     :description "Middleware that does things."
     :wrap wrap})

  ;; record

  (def wrap2
    (middleware/map->Middleware
     {:name ::wrap2
      :description "Middleware that does things."
      :wrap wrap}))


  ;; using middleware

  ;; `:middleware` is merged to endpoints by the router.

  (defn handler [{::keys [acc]}]
    {:status 200, :body (conj acc :handler)})

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [[wrap 1] [wrap2 2]]}
       ["/ping" {:get {:middleware [[wrap3 3]]
                       :handler handler}}]])))

  ;; all the middlewares are applied correctly:

  (app {:request-method :get, :uri "/api/ping"})


  ;; compiling middleware

  ;; middlewares can be optimized against an endpoint using middleware
  ;; compilation (see Compiling Middleware below).


  ;; ideas for the future

  ;; - support middleware dependency resolution with new keys `:requires`
  ;; and `:provides`. Values are set of top-level keys of the request. e.g.
  ;;   - `InjectUserIntoRequestMiddleware` requires `#{:session}` and provides
  ;;     `#{:user}`
  ;;   - `AuthorizationMiddleware` requires `#{:user}`

  ;; ideas welcome & see issues for details.

)


;; transforming the middleware chain

(comment

  ;; there is an extra option in the Ring router (actually, in the underlying
  ;; middleware-router): `:reitit.middleware/transform` to transform the
  ;; middleware chain per endpoint. Value should be a function or a vector of
  ;; functions that get a vector of compiled middleware and should return a new
  ;; vector of middleware.


  ;; example application

  (defn wrap [handler id]
    (fn [request]
      (handler (update request ::acc (fnil conj []) id))))

  (defn handler [{::keys [acc]}]
    {:status 200, :body (conj acc :handler)})

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [[wrap 1] [wrap 2]]}
       ["/ping" {:get {:middleware [[wrap 3]]
                       :handler handler}}]])))

  (app {:request-method :get, :uri "/api/ping"})


  ;; reversing the middleware chain

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [[wrap 1] [wrap 2]]}
       ["/ping" {:get {:middleware [[wrap 3]]
                       :handler handler}}]]
      {::middleware/transform reverse})))

  (app {:request-method :get, :uri "/api/ping"})


  ;; interleaving middleware

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [[wrap 1] [wrap 2]]}
       ["/ping" {:get {:middleware [[wrap 3]]
                       :handler handler}}]]
      {::middleware/transform #(interleave % (repeat [wrap :debug]))})))

  (app {:request-method :get, :uri "/api/ping"})


  ;; printing request diffs

  ;; [metosin/reitit-middleware "0.6.0"]

  ;; using `reitit.ring.middleware.dev/print-request-diffs` transformation, the
  ;; request diffs between each middleware are printed out to the console. To
  ;; use it, add the following router option:

  ;; `:reitit.middleware/transform`
  ;; `reitit.ring.middleware.dev/print-request-diffs`

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [[wrap 1] [wrap 2]]}
       ["/ping" {:get {:middleware [[wrap 3]]
                       :handler handler}}]]
      {::middleware/transform reitit.ring.middleware.dev/print-request-diffs})))

  (app {:request-method :get, :uri "/api/ping"})

)


;; middleware registry

(comment

  ;; the `:middleware` syntax in `reitit-ring` also supports keywords. Keywords
  ;; are looked up from the Middleware Registry, which is a map of `keyword =>
  ;; IntoMiddleware`. Middleware registry should be stored under key
  ;; `:reitit.middleware/registry` in the router options. If a middleware
  ;; keyword isn't found in the registry, router creation fails fast with a
  ;; descriptive error message.


  ;; application using middleware defined in the Middleware Registry:

  (defn wrap-bonus [handler value]
    (fn [request]
      (handler (update request :bonus (fnil + 0) value))))

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [[:bonus 20]]}
       ["/bonus" {:middleware [:bonus10]
                  :get (fn [{:keys [bonus]}]
                         {:status 200, :body {:bonus bonus}})}]]
      {::middleware/registry {:bonus wrap-bonus
                              :bonus10 [:bonus 10]}})))

  (app {:request-method :get, :uri "/api/bonus"})

  ;; router creation fails fast if the registry doesn't contain the middleware:

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [[:bonus 20]]}
       ["/bonus" {:middleware [:bonus10]
                  :get (fn [{:keys [bonus]}]
                         {:status 200, :body {:bonus bonus}})}]]
      {::middleware/registry {:bonus wrap-bonus}})))


  ;; when to use the registry?

  ;; middleware as keywords helps to keep the routes (all but handlers) as
  ;; literal data (i.e. data that evaluates to itself), enabling the routes to
  ;; be persisted in external formats like EDN files and databases. Duct is a
  ;; good example, where the middleware can be referenced from EDN files. It
  ;; should be easy to make Duct configuration a Middleware Registry in
  ;; reitit-ring.

  ;; on the other hand, it's an extra level of indirection, making things more
  ;; complex and removing the default IDE support of "go to definition" or "look
  ;; up source".


  ;; TODO

  ;; - a prefilled registry of common middleware in the reitit-middleware

)


;; exception handling with Ring

(comment

  ;; [metosin/reitit-middleware "0.6.0"]

  ;; exceptions thrown in router creation can be handled with custom exception
  ;; handler. By default, exceptions thrown at runtime from a handler or a
  ;; middleware are not caught by the `reitit.ring/ring-handler`. A good
  ;; practice is to have a top-level exception handler to log and format errors
  ;; for clients.


  ;; `exception/exception-middleware`

  ;; a preconfigured middleware using `exception/default-handlers`. Catches:

  ;; - request & response coercion exceptions
  ;; - muuntaja decode exceptions
  ;; - exceptions with `:type` of `:reitit.ring/response`, returning `:response`
  ;;   key from `ex-data`.
  ;; - safely all other exceptions

  (def app
    (ring/ring-handler
     (ring/router
      ["/fail" (fn [_] (throw (Exception. "fail")))]
      {:data {:middleware [exception/exception-middleware]}})))

  (app {:request-method :get, :uri "/fail"})


  ;; `exception/create-exception-middleware`

  ;; creates the exception-middleware with custom options. Takes a map of
  ;; `identifier => exception request => response` that is used to select the
  ;; exception handler for the thrown/raised exception identifier. Exception
  ;; identifier is either a keyword or an Exception Class.

  ;; the following handlers are available by default:
  ;; - `:reitit.ring/response`: value in `ex-data` key `:response` will be
  ;;   returned
  ;; - `:muuntaja/decode`: handle Muuntaja decoding exceptions
  ;; - `:reitit.coercion/request-coercion`: request coercion errors
  ;;   (http 400 response)
  ;; - `:reitit.coercion/response-coercion`: response coercion errors
  ;;   (http 500 response)
  ;; - `::exception/default`: a default exception handler if nothing else
  ;;   matched (default `exception/default-handler`).
  ;; - `::exception/wrap`: a 3-arity handler to wrap the actual handler
  ;;   `handler exception request => response` (no default).

  ;; the handler is selected from the options map by exception identifier in the
  ;; following lookup order:

  ;; 1. `:type` of exception `ex-data`
  ;; 2. Class of exception
  ;; 3. `:type` ancestors of exception `ex-data`
  ;; 4. Super Classes of exception
  ;; 5. the `::default` handler

  ;; type hierarchy
  (derive ::error ::exception)
  (derive ::failure ::exception)
  (derive ::horror ::exception)

  (defn handler [message exception request]
    {:status 500
     :body {:message message
            :exception (.getClass exception)
            :data (ex-data exception)
            :uri (:uri request)}})

  (def exception-middleware
    (exception/create-exception-middleware
     (merge
      exception/default-handlers
      {;; ex-data with :type ::error
       ::error (partial handler "error")
       ;; ex-data with ::exception or ::failure
       ::exception (partial handler "exception")
       ;; SQLException and all it's child classes
       SQLException (partial handler "sql-exception")
       ;; override the default handler
       ::exception/default (partial handler "default")
       ;; print stack-traces for all exceptions
       ::exception/wrap (fn [handler e request]
                          (println "ERROR" (pr-str (:uri request)))
                          (handler e request))})))

  (def app
    (ring/ring-handler
     (ring/router
      ["/fail" (fn [_] (throw (ex-info "fail" {:type ::failure})))]
      {:data {:middleware [exception-middleware]}})))

  (app {:request-method :get, :uri "/fail"})

)


;; default middleware

(comment

  ;; [metosin/reitit-middleware "0.6.0"]

  ;; any Ring middleware can be used with `reitit-ring`, but using data-driven
  ;; middleware is preferred as they are easier to manage and in many cases
  ;; yield better performance. `reitit-middleware` contains a set of common ring
  ;; middleware, lifted into data-driven middleware.


  ;; parameters handling

  ;; `reitit.ring.middleware.parameters/parameters-middleware` to capture query-
  ;; and form-params. Wraps `ring.middleware.params/wrap-params`.

  ;; NOTE: this middleware will be factored into two parts: a query-parameters
  ;; middleware and a Muuntaja format responsible for the the
  ;; 'application/x-www-form-urlencoded' body
  ;; format. cf. https://github.com/metosin/reitit/issues/134


  ;; exception handling

  ;; see Exception Handling with Ring above.


  ;; content negotiation

  ;; see Content Negotiation below.


  ;; multipart request handling

  ;; wrapper for Ring multipart middleware. Emits swagger `:consumes`
  ;; definitions automatically.

  ;; expected route data:
  ;; - `[:parameters :multipart]`: mounts only if defined for a route.

  ;; - `multipart/multipart-middleware` a preconfigured middleware for
  ;;   multipart handling
  ;; - `multipart/create-multipart-middleware` to generate with custom
  ;;   configuration


  ;; inspecting middleware chain

  ;; `reitit.ring.middleware.dev/print-request-diffs` is a middleware chain
  ;; transforming function. It prints a request and response diff between each
  ;; middleware. To use it, add the following router option:

  ;; `:reitit.middleware/transform`
  ;; `reitit.ring.middleware.dev/print-request-diffs`

  ;; see Transforming the Middleware Chain above for examples


  ;; example app

  ;; see an example app with the default middleware in action:
  ;; https://github.com/metosin/reitit/blob/master/examples/ring-swagger/src/example/server.clj.

)


;; content negotiation

(comment

  ;; wrapper for Muuntaja middleware for content negotiation, request decoding
  ;; and response encoding. Takes explicit configuration via `:muuntaja` key in
  ;; route data. Emits swagger `:produces` and `:consumes` definitions
  ;; automatically based on the Muuntaja configuration.

  ;; negotiates a request body based on "Content-Type" header and response body
  ;; based on "Accept" and "Accept-Charset" headers. Publishes the negotiation
  ;; results as `:muuntaja/request` and `:muuntaja/response` keys into the
  ;; request.

  ;; decodes the request body into `:body-params` using the `:muuntaja/request`
  ;; key in request if the `:body-params` doesn't already exist.

  ;; encodes the response body using the `:muuntaja/response` key in request if
  ;; the response doesn't have "Content-Type" header already set.

  ;; expected route data:
  ;; - `:muuntaja`: `muuntaja.core/Muuntaja` instance, does not mount if not set.

  ;; - `muuntaja/format-middleware`: negotiation, request decoding and response
  ;;   encoding in a single Middleware
  ;; - `muuntaja/format-negotiate-middleware`: negotiation
  ;; - `muuntaja/format-request-middleware`: request decoding
  ;; - `muuntaja/format-response-middleware`: response encoding

  (def app
    (ring/ring-handler
     (ring/router
      [["/math"
        {:post {:summary "negotiated request & response (json, edn, transit)"
                :parameters {:body {:x int?, :y int?}}
                :responses {200 {:body {:total int?}}}
                :handler (fn [{{{:keys [x y]} :body} :parameters}]
                           {:status 200
                            :body {:total (+ x y)}})}}]
       ["/xml"
        {:get {:summary "forced xml response"
               :handler (fn [_]
                          {:status 200
                           :headers {"Content-Type" "text/xml"}
                           :body "<kikka>kukka</kikka>"})}}]]
      {:data {:muuntaja m/instance
              :coercion rcs/coercion
              :middleware [muuntaja/format-middleware
                           rrc/coerce-exceptions-middleware
                           rrc/coerce-request-middleware
                           rrc/coerce-response-middleware]}})))

  (def server (jetty/run-jetty #'app {:port 3000, :join? false}))

  ;; see `test.org` in the project root for usage


  ;; changing default parameters

  ;; the current JSON formatter used by `reitit` already has the option to parse
  ;; keys as keyword which is a sane default in Clojure. However, if you would
  ;; like to parse all the double as bigdecimal you'd need to change an option
  ;; of the JSON formatter (`jsonista`; see for more options for JSON and EDN).

  (def new-muuntaja-instance
    (m/create
     (assoc-in
      m/default-options
      [:formats "application/json" :decoder-opts :bigdecimals]
      true)))

  ;; now you should change the `m/instance` installed in the router with the
  ;; new-muuntaja-instance.


  ;; adding custom encoder

  ;; the example below is from muuntaja explaining how to add a custom encoder
  ;; to parse a `java.util.Date` instance.

  (def muuntaja-instance
    (m/create
     (assoc-in
      m/default-options
      [:formats "application/json" :encoder-opts]
      {:date-format "yyyy-MM-dd"})))

  (->> {:value (java.util.Date.)}
       (m/encode "application/json")
       slurp)


  ;; putting it all together

  ;; if you inspect `m/default-options`, you'll find it's only a map. This means
  ;; you can compose your new muuntaja instance with as many options as you
  ;; need.

  (def new-muuntaja
    (m/create
     (-> m/default-options
         (assoc-in [:formats "application/json"
                    :decoder-opts :bigdecimals] true)
         (assoc-in [:formats "application/json"
                    :encoder-opts :date-format] "yyyy-MM-dd"))))

)


;; Ring coercion

(comment

  ;; basic coercion is explained in detail in the Coercion Guide. With Ring,
  ;; both request parameters and response bodies can be coerced.

  ;; following request parameters are currently supported:
  ;;   type       request source
  ;; - `:query`:  `:query-params`
  ;; - `:body`:   `:body-params`
  ;; - `:form`:   `:form-params`
  ;; - `:header`: `:header-params`
  ;; - `:path`:   `:path-params`

  ;; to enable coercion, the following things need to be done:

  ;; - define a  `reitit.coercion/Coercion` for the routes
  ;; - define types for the parameters and/or responses
  ;; - mount Coercion Middleware to apply to coercion
  ;; - use the coerced parameters in a handler/middleware


  ;; define coercion

  ;; `reitit.coercion/Coercion` is a protocol defining how types are defined,
  ;; coerced and inventoried.

  ;; `reitit` ships with the following coercion modules:

  ;; - `reitit.coercion.malli/coercion` for malli
  ;; - `reitit.coercion.schema/coercion` for plumatic schema
  ;; - `reitit.coercion.spec/coercion` for both `clojure.spec` and data-specs

  ;; coercion can be attached to route data under `:coercion` key. There can be
  ;; multiple Coercion implementations within a single router, normal scoping
  ;; rules apply.


  ;; defining parameters and responses

  ;; parameters are defined in route data under `:parameters` key. It's value
  ;; should be a map of parameter `:type -> Coercion Schema`.

  ;; responses are defined in route data under `:responses` key. It's value
  ;; should be a map of http status code to a map which can contain `:body` key
  ;; with Coercion Schema as value.

  ;; below is an example with Plumatic Schema. It defines schemas for `:query`,
  ;; `:body` and `:path` parameters and for http 200 response `:body`.

  ;; handlers can access the coerced parameters via the `:parameters` key in the
  ;; request.

  (def PositiveInt (s/constrained s/Int pos? 'PositiveInt))

  (def plus-endpoint
    {:coercion reitit.coercion.schema/coercion
     :parameters {:query {:x s/Int}
                  :body {:y s/Int}
                  :path {:z s/Int}}
     :responses {200 {:body {:total PositiveInt}}}
     :handler (fn [{:keys [parameters]}]
                (let [total (+ (-> parameters :query :x)
                               (-> parameters :body :y)
                               (-> parameters :path :z))]
                  {:status 200
                   :body {:total total}}))})


  ;; coercion middleware

  ;; defining a coercion for a route data doesn't do anything, as it's just
  ;; data. We have to attach some code to apply the actual coercion. We can use
  ;; the middleware from `reitit.ring.coercion`:

  ;; - `coerce-request-middleware` to apply the parameter coercion
  ;; - `coerce-response-middleware` to apply the response coercion
  ;; - `coerce-exceptions-middleware` to transform coercion exceptions into
  ;;   pretty responses


  ;; full example

  ;; here is a full example for applying coercion with Reitit, Ring and Schema:

  (def PositiveInt (s/constrained s/Int pos? 'PositiveInt))

  (def app
    (ring/ring-handler
     (ring/router
      ["/api"
       ["/ping" {:name ::ping
                 :get (fn [_]
                        {:status 200
                         :body "pong"})}]
       ["/plus/:z" {:name ::plus
                    :post {:coercion reitit.coercion.schema/coercion
                           :parameters {:query {:x s/Int}
                                        :body {:y s/Int}
                                        :path {:z s/Int}}
                           :responses {200 {:body {:total PositiveInt}}}
                           :handler (fn [{:keys [parameters]}]
                                      (let [total (+ (-> parameters :query :x)
                                                     (-> parameters :body :y)
                                                     (-> parameters :path :z))]
                                        {:status 200
                                         :body {:total total}}))}}]]
      {:data {:middleware [rrc/coerce-exceptions-middleware
                           rrc/coerce-request-middleware
                           rrc/coerce-response-middleware]}})))

  ;; valid request:

  (app {:request-method :post
        :uri "/api/plus/3"
        :query-params {"x" "1"}
        :body-params {:y 2}})

  ;; invalid request:

  (app {:request-method :post
        :uri "/api/plus/3"
        :query-params {"x" "abba"}
        :body-params {:y 2}})

  ;; invalid response:

  (app {:request-method :post
        :uri "/api/plus/3"
        :query-params {"x" "1"}
        :body-params {:y -10}})


  ;; pretty printing spec errors

  ;; spec problems are exposed as is in request & response coercion
  ;; errors. Pretty-printers like expound can be enabled like this:

  (defn coercion-error-handler [status]
    (let [printer (expound/custom-printer
                   {:theme :figwheel-theme, :print-specs? false})
          handler (exception/create-coercion-handler status)]
      (fn [exception request]
        (printer (-> exception ex-data :problems))
        (handler exception request))))

  (def app
    (ring/ring-handler
     (ring/router
      ["/plus"
       {:get
        {:parameters {:query {:x int?, :y int?}}
         :responses {200 {:body {:total pos-int?}}}
         :handler (fn [{{{:keys [x y]} :query} :parameters}]
                    {:status 200, :body {:total (+ x y)}})}}]
      {:data {:coercion reitit.coercion.spec/coercion
              :middleware [(exception/create-exception-middleware
                            (merge
                             exception/default-handlers
                             {:reitit.coercion/request-coercion
                              (coercion-error-handler 400)
                              :reitit.coercion/response-coercion
                              (coercion-error-handler 500)}))
                           rrc/coerce-request-middleware
                           rrc/coerce-response-middleware]}})))

  (app
   {:uri "/plus"
    :request-method :get
    :query-params {"x" "1", "y" "fail"}})

  (app
   {:uri "/plus"
    :request-method :get
    :query-params {"x" "1", "y" "-2"}})


  ;; optimizations

  ;; the coercion middlewares are compiled against a route. In the middleware
  ;; compilation step the actual coercer implementations are constructed for the
  ;; defined models. Also, the middleware doesn't mount itself if a route
  ;; doesn't have `:coercion` and `:parameters` or `:responses` defined.

  ;; we can query the compiled middleware chain for the routes:

  (-> (ring/get-router app)
      (r/match-by-name ::plus)
      :result :post :middleware
      (->> (mapv :name)))

  ;; route without coercion defined:

  (app {:request-method :get, :uri "/api/ping"})

  ;; has no mounted middleware:

  (-> (ring/get-router app)
      (r/match-by-name ::ping)
      :result :get :middleware
      (->> (mapv :name)))

)


;; route data validation

(comment

  ;; Ring route validation works just like with core router, with few
  ;; differences:

  ;; - `reitit.ring.spec/validate` should be used instead of
  ;;   `reitit.spec/validate` - to support validating all endpoints (`:get`,
  ;;   `:post` etc.)
  ;; - with `clojure.spec` validation, middleware can contribute to route spec
  ;;   via `:specs` key. The effective route data spec is router spec merged
  ;;   with middleware specs.


  ;; example

  ;; a simple app with spec-validation turned on:

  (defn handler [_]
    {:status 200, :body "ok"})

  (def app
    (ring/ring-handler
     (ring/router
      ["/api"
       ["/public"
        ["/ping" {:get handler}]]
       ["/internal"
        ["/users" {:get {:handler handler}
                   :delete {:handler handler}}]]]
      {:validate rrs/validate
       ::rs/explain expound/expound-str})))

  ;; all good:

  (app {:request-method :get
        :uri "/api/internal/users"})


  ;; explicit specs via middleware

  ;; middleware that requires `:zone` to be present in route data:

  (spec/def ::zone #{:public :internal})

  (def zone-middleware
    {:name ::zone-middleware
     :spec (spec/keys :req-un [::zone])
     :wrap (fn [handler]
             (fn [request]
               (let [zone (-> request (ring/get-match) :data :zone)]
                 (println zone)
                 (handler request))))})

  ;; missing route data fails fast at router creation:

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [zone-middleware]} ;; <--- added
       ["/public"
        ["/ping" {:get handler}]]
       ["/internal"
        ["/users" {:get {:handler handler}
                   :delete {:handler handler}}]]]
      {:validate rrs/validate
       ::rs/explain expound/expound-str})))

  ;; adding the `:zone` to route data fixes the problem:

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [zone-middleware]}
       ["/public" {:zone :public} ;; <--- added
        ["/ping" {:get handler}]]
       ["/internal" {:zone :internal} ;; <--- added
        ["/users" {:get {:handler handler}
                   :delete {:handler handler}}]]]
      {:validate rrs/validate
       ::rs/explain expound/expound-str})))

  (app {:request-method :get
        :uri "/api/internal/users"})


  ;; implicit specs

  ;; by design, `clojure.spec` validates all fully-qualified keys with `s/keys`
  ;; specs even if they are not defined in that keyset. Validation is implicit
  ;; but powerful.

  ;; let's reuse the `wrap-enforce-roles` from Dynamic extensions and define
  ;; specs for the data:

  (spec/def ::role #{:admin :manager})
  (spec/def ::roles (spec/coll-of ::role :into #{}))

  (defn wrap-enforce-roles [handler]
    (fn [{::keys [roles] :as request}]
      (let [required (some-> request (ring/get-match) :data ::roles)]
        (if (and (seq required) (not (set/subset? required roles)))
          {:status 403, :body "forbidden"}
          (handler request)))))

  ;; `wrap-enforce-roles` silently ignores if the `::roles` is not present:

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [zone-middleware
                            wrap-enforce-roles]} ;; <--- added
       ["/public" {:zone :public}
        ["/ping" {:get handler}]]
       ["/internal" {:zone :internal}
        ["/users" {:get {:handler handler}
                   :delete {:handler handler}}]]]
      {:validate rrs/validate
       ::rs/explain expound/expound-str})))

  (app {:request-method :get
        :uri "/api/internal/users"})

  ;; but fails if they are present and invalid:

  (def app
    (ring/ring-handler
     (ring/router
      ["/api" {:middleware [zone-middleware
                            wrap-enforce-roles]}
       ["/public" {:zone :public}
        ["/ping" {:get handler}]]
       ["/internal" {:zone :internal}
        ["/users" {:get {:handler handler
                         ::roles #{:manager}} ;; <--- added
                   :delete {:handler handler
                            ::roles #{:adminz}}}]]] ;; <--- added
       {:validate rrs/validate
        ::rs/explain expound/expound-str})))


  ;; pushing the data to the endpoints

  ;; ability to define (and reuse) route-data in mid-paths is a powerful
  ;; feature, but having data defined all around might be harder to reason
  ;; about. There is always an option to define all data at the endpoints.

  (def app
    (ring/ring-handler
     (ring/router
      ["/api"
       ["/public"
        ["/ping" {:zone :public
                  :get handler
                  :middleware [zone-middleware
                               wrap-enforce-roles]}]]
       ["/internal"
        ["/users" {:zone :internal
                   :middleware [zone-middleware
                                wrap-enforce-roles]
                   :get {:handler handler
                         ::roles #{:manager}}
                   :delete {:handler handler
                            ::roles #{:admin}}}]]]
      {:validate rrs/validate
       ::rs/explain expound/expound-str})))

  ;; or even flatten the routes:

  (def app
    (ring/ring-handler
     (ring/router
      [["/api/public/ping" {:zone :public
                            :get handler
                            :middleware [zone-middleware
                                         wrap-enforce-roles]}]
       ["/api/internal/users" {:zone :internal
                               :middleware [zone-middleware
                                            wrap-enforce-roles]
                               :get {:handler handler
                                     ::roles #{:manager}}
                               :delete {:handler handler
                                        ::roles #{:admin}}}]]
      {:validate rrs/validate
       ::rs/explain expound/expound-str})))

  ;; the common middleware can also be pushed to the router, here cleanly
  ;; separating behavior and data:

  (def app
    (ring/ring-handler
     (ring/router
      [["/api/public/ping" {:zone :public
                            :get handler}]
       ["/api/internal/users" {:zone :internal
                               :get {:handler handler
                                     ::roles #{:manager}}
                               :delete {:handler handler
                                        ::roles #{:admin}}}]]
      {:data {:middleware [zone-middleware wrap-enforce-roles]}
       :validate rrs/validate
       ::rs/explain expound/expound-str})))

)


;; compiling middleware

(comment

  ;; the dynamic extensions are an easy way to extend the system. To enable fast
  ;; lookup of route data, we can compile them into any shape (records,
  ;; functions etc.), enabling fast access at request-time.

  ;; but, we can do much better. As we know the exact route that a
  ;; middleware/interceptor is linked to, we can pass the (compiled) route
  ;; information into the middleware at creation-time. It can do local
  ;; reasoning: Extract and transform relevant data just for it and pass the
  ;; optimized data into the actual request-handler via a closure - yielding
  ;; much faster runtime processing. A middleware can also decide not to mount
  ;; itself by returning nil. (E.g. Why mount a `wrap-enforce-roles` middleware
  ;; for a route if there are no roles required for it?)

  ;; to enable this we use middleware records `:compile` key instead of the
  ;; normal `:wrap`. `:compile` expects a function of `route-data router-opts =>
  ;; ?IntoMiddleware`.

  ;; to demonstrate the two approaches, below is the response coercion
  ;; middleware written as normal ring middleware function and as middleware
  ;; record with `:compile`.


  ;; normal middleware

  ;; - reads the compiled route information on every request. Everything is done
  ;;   at request-time.

  (defn wrap-coerce-response
    "Middleware for pluggable response coercion.
  Expects a :coercion of type `reitit.coercion/Coercion` and :responses from
  route data, otherwise will do nothing."
    [handler]
    (fn
      ([request]
       (let [response (handler request)
             method (:request-method request)
             match (ring/get-match request)
             responses (-> match :result method :data :responses)
             coercion (-> match :data :coercion)
             opts (-> match :data :opts)]
         (if (and coercion responses)
           (let [coercers (rc/response-coercers coercion responses opts)]
             (rc/coerce-response coercers request response))
           response)))
      ([request respond raise]
       (let [method (:request-method request)
             match (ring/get-match request)
             responses (-> match :result method :data :responses)
             coercion (-> match :data :coercion)
             opts (-> match :data :opts)]
         (if (and coercion responses)
           (let [coercers (rc/response-coercers coercion responses opts)]
             (handler request #(respond
                                (rc/coerce-response coercers request %))))
           (handler request respond raise))))))


  ;; compiled middleware

  ;; - route information is provided at creation-time
  ;; - coercers are compiled at creation-time
  ;; - middleware mounts only if `:coercion` and `:responses` are defined for
  ;;   the route
  ;; - also defines spec for the route data `:responses` for the route data
  ;;   validation.

  (def coerce-response-middleware
    "Middleware for pluggable response coercion.
  Expects a :coercion of type `reitit.coercion/Coercion`
  and :responses from route data, otherwise does not mount."
    {:name ::coerce-response
     :spec ::rs/responses
     :compile (fn [{:keys [coercion responses]} opts]
                (when (and coercion responses)
                  (let [coercers (rc/response-coercers coercion responses opts)]
                    (fn [handler]
                      (fn
                        ([request]
                         (rc/coerce-response coercers request
                                              (handler request)))
                        ([request respond raise]
                         (handler request #(respond
                                            (rc/coerce-response
                                             coercers request %)) raise)))))))})

  ;; it has 50% less code, it's much easier to reason about and is much faster.


  ;; require keys on routes at creation time

  ;; often it is useful to require a route to provide a specific key.

  (spec/def ::authorize
    (spec/or :handler :accessrules/handler :rule :accessrules/rule))

  (def authorization-middleware
    {:name ::authorization
     :spec (spec/keys :req-un [::authorize])
     :compile
     (fn [route-data _opts]
       (when-let [rule (:authorize route-data)]
         (fn [handler]
           (accessrules/wrap-access-rules handler {:rules [rule]}))))})

  ;; in the example above the `:spec` expresses that each route is required to
  ;; provide the `:authorize` key. However, in this case the compile function
  ;; returns nil when that key is missing, which means the middleware will not
  ;; be mounted, the spec will not be considered, and the compiler will not
  ;; enforce this requirement as intended.

  ;; if you just want to enforce the spec return a map without `:wrap` or
  ;; `:compile` keys, e.g. an empty map, {}.

  (def authorization-middleware
    {:name ::authorization
     :spec (spec/keys :req-un [::authorize])
     :compile
     (fn [route-data _opts]
       (if-let [rule (:authorize route-data)]
         (fn [handler]
           (accessrules/wrap-access-rules handler {:rules [rule]}))
         ;; return empty map just to enforce spec
         {}))})

  ;; the middleware (and associated spec) will still be part of the chain, but
  ;; will not process the request.

)


;; Swagger support

(comment

  ;; [metosin/reitit-swagger "0.6.0"]

  ;; reitit supports Swagger2 documentation, thanks to schema-tools and
  ;; spec-tools. Documentation is extracted from route definitions, coercion
  ;; `:parameters` and `:responses` and from a set of new documentation keys.

  ;; to enable swagger-documentation for a Ring router:

  ;; - annotate your routes with swagger-data
  ;; - mount a swagger-handler to serve the swagger-spec
  ;; - optionally mount a swagger-ui to visualize the swagger-spec


  ;; Swagger data

  ;; the following route data keys contribute to the generated swagger
  ;; specification:
  ;; - `:swagger`: map of any swagger-data. Can have `:id` (keyword or sequence
  ;;   of keywords) to identify the api
  ;; - `:no-doc`: optional boolean to exclude endpoint from api docs
  ;; - `:tags`: optional set of string or keyword tags for an endpoint api docs
  ;; - `:summary`: optional short string summary of an endpoint
  ;; - `:description`: optional long description of an endpoint. Supports
  ;;   http://spec.commonmark.org/
  ;; - `:operationId`: optional string specifying the unique ID of an Operation

  ;; coercion keys also contribute to the docs:
  ;; - `:parameters`: optional input parameters for a route, in a format defined
  ;;   by the coercion
  ;; - `:responses`: optional descriptions of responses, in a format defined by
  ;;   coercion

  ;; there is a `reitit.swagger.swagger-feature`, which acts as both a
  ;; Middleware and an Interceptor that is not participating in any request
  ;; processing - it just defines the route data specs for the routes it's
  ;; mounted to. It is only needed if the route data validation is turned on.


  ;; Swagger spec

  ;; to serve the actual Swagger specification, there is
  ;; `reitit.swagger/create-swagger-handler`. It takes no arguments and returns
  ;; a ring-handler which collects at request-time data from all routes for the
  ;; same swagger api and returns a formatted Swagger specification as Clojure
  ;; data, to be encoded by a response formatter.

  ;; if you need to post-process the generated spec, just wrap the handler with
  ;; a custom Middleware or an Interceptor.


  ;; swagger-ui

  ;; `swagger-ui` is a user interface to visualize and interact with the Swagger
  ;; specification. To make things easy, there is a pre-integrated version of
  ;; the swagger-ui as a separate module.

  ;; [metosin/reitit-swagger-ui "0.6.0"]

  ;; `reitit.swagger-ui/create-swagger-ui-handler` can be used to create a
  ;; ring-handler to serve the swagger-ui. It accepts the following options:
  ;; - `:parameter`: optional name of the wildcard parameter, defaults to
  ;;   unnamed keyword `:`
  ;; - `:root`: optional resource root, defaults to "swagger-ui"
  ;; - `:url`: path to swagger endpoint, defaults to "/swagger.json"
  ;; - `:path`: optional path to mount the handler to. Works only if mounted
  ;;   outside of a router.
  ;; - `:config`: parameters passed to swagger-ui as-is. See the docs

  ;; we use swagger-ui from `ring-swagger-ui`, which can be easily configured
  ;; from routing application. It stores files swagger-ui in the resource
  ;; classpath.

  ;; webjars also hosts a version of the swagger-ui.

  ;; NOTE: currently, swagger-ui module is just for
  ;; Clojure. ClojureScript-support welcome as a PR!

  ;; NOTE: If you want to use swagger-ui 2.x you can do so by explicitly
  ;; downgrading `metosin/ring-swagger-ui` to 2.2.10.

  ;; NOTE: If you use swagger-ui 3.x, you need to include `:responses` for
  ;; Swagger-UI to display the response when trying out endpoints. You can
  ;; define `:responses {200 {:schema s/Any}}` at the top-level to show
  ;; responses for all endpoints.


  ;; examples

  ;; simple example

  ;; - two routes
  ;; - swagger-spec served from "/swagger.json"
  ;; - swagger-ui mounted to "/api-docs"
  ;; - note that for real-world use, you need a content-negotiation middleware,
  ;;   see the next example

  (def app
    (ring/ring-handler
     (ring/router
      [["/api"
        ["/ping" {:get (constantly {:status 200, :body "ping"})}]
        ["/pong" {:post (constantly {:status 200, :body "pong"})}]]
       ["" {:no-doc true}
        ["/swagger.json" {:get (swagger/create-swagger-handler)}]
        ["/api-docs/*" {:get (swagger-ui/create-swagger-ui-handler)}]]])))

  ;; the generated swagger spec:

  (app {:request-method :get :uri "/swagger.json"})

  ;; swagger-ui:

  (app {:request-method :get, :uri "/api-docs/index.html"})

  ;; you might be interested in adding a trailing slash handler to the app to
  ;; serve the swagger-ui from "/api-docs" (without the trailing slash) too.

  ;; another way to serve the swagger-ui is using the default handler:

  (def app
    (ring/ring-handler
     (ring/router
      [["/api"
        ["/ping" {:get (constantly {:status 200, :body "ping"})}]
        ["/pong" {:post (constantly {:status 200, :body "pong"})}]]
       ["/swagger.json"
        {:get {:no-doc true
               :handler (swagger/create-swagger-handler)}}]])
     (swagger-ui/create-swagger-ui-handler {:path "/api-docs"})))


  ;; more complete example

  ;; - `clojure.spec` coercion
  ;; - swagger data (`:tags`, `:produces`, `:summary`, `:basePath`)
  ;; - swagger-spec served from "/swagger.json"
  ;; - swagger-ui mounted to "/"
  ;; - set of middleware for content negotiation, exceptions, multipart etc.
  ;; - missed routes are handled by `create-default-handler`
  ;; - served via `ring-jetty`

  ;; whole example project is in "/examples/ring-swagger".

  (def app
    (ring/ring-handler
     (ring/router
      [["/swagger.json"
        {:get {:no-doc true
               :swagger {:info {:title "my-api"}
                         :basePath "/"} ;; prefix for all paths
               :handler (swagger/create-swagger-handler)}}]

       ["/files"
        {:swagger {:tags ["files"]}}

        ["/upload"
         {:post {:summary "upload a file"
                 :parameters {:multipart {:file multipart/temp-file-part}}
                 :responses {200 {:body {:file multipart/temp-file-part}}}
                 :handler (fn [{{{:keys [file]} :multipart} :parameters}]
                            {:status 200
                             :body {:file file}})}}]

        ["/download"
         {:get {:summary "downloads a file"
                :swagger {:produces ["image/png"]}
                :handler (fn [_]
                           {:status 200
                            :headers {"Content-Type" "image/png"}
                            :body (io/input-stream
                                   (io/resource "reitit.png"))})}}]]

       ["/math"
        {:swagger {:tags ["math"]}}

        ["/plus"
         {:get {:summary "plus with spec query parameters"
                :parameters {:query {:x int?, :y int?}}
                :responses {200 {:body {:total int?}}}
                :handler (fn [{{{:keys [x y]} :query} :parameters}]
                           {:status 200
                            :body {:total (+ x y)}})}
          :post {:summary "plus with spec body parameters"
                 :parameters {:body {:x int?, :y int?}}
                 :responses {200 {:body {:total int?}}}
                 :handler (fn [{{{:keys [x y]} :body} :parameters}]
                            {:status 200
                             :body {:total (+ x y)}})}}]]]

      {:data {:coercion reitit.coercion.spec/coercion
              :muuntaja m/instance
              :middleware [;; query-params & form-params
                           parameters/parameters-middleware
                           ;; content-negotiation
                           muuntaja/format-negotiate-middleware
                           ;; encoding response body
                           muuntaja/format-response-middleware
                           ;; exception handling
                           exception/exception-middleware
                           ;; decoding request body
                           muuntaja/format-request-middleware
                           ;; coercing response bodys
                           rrc/coerce-response-middleware
                           ;; coercing request parameters
                           rrc/coerce-request-middleware
                           ;; multipart
                           multipart/multipart-middleware]}})
     (ring/routes
      (swagger-ui/create-swagger-ui-handler {:path "/"})
      (ring/create-default-handler))))

  (defn start []
    (jetty/run-jetty #'app {:port 3000, :join? false})
    (println "server running in port 3000"))

  (start)

  ;; http://localhost:3000 should render now the swagger-ui


  ;; multiple swagger apis

  ;; there can be multiple swagger apis within a router. Each route can be part
  ;; of 0..n swagger apis. Swagger apis are identified by value in route data
  ;; under key path `[:swagger :id]`. It can be either a keyword or a sequence
  ;; of keywords. Normal route data scoping rules rules apply.

  ;; example with:

  ;; - 4 routes
  ;; - 2 swagger apis `::one` and `::two`
  ;; - 3 swagger specs

  (def ping-route
    ["/ping" {:get (constantly {:status 200, :body "ping"})}])

  (def spec-route
    ["/swagger.json"
     {:get {:no-doc true
            :handler (swagger/create-swagger-handler)}}])

  (def app
    (ring/ring-handler
     (ring/router
      [["/common" {:swagger {:id #{::one ::two}}} ping-route]
       ["/one" {:swagger {:id ::one}} ping-route spec-route]
       ["/two" {:swagger {:id ::two}} ping-route spec-route
        ["/deep" {:swagger {:id ::one}} ping-route]]
       ["/one-two" {:swagger {:id #{::one ::two}}} spec-route]])))

  (-> {:request-method :get, :uri "/one/swagger.json"} app :body :paths keys)
  (-> {:request-method :get, :uri "/two/swagger.json"} app :body :paths keys)
  (-> {:request-method :get, :uri "/one-two/swagger.json"} app :body :paths keys)


  ;; TODO

  ;; - ClojureScript
  ;; - example for Macchiato
  ;; - body formatting
  ;; - resource handling

)


;; RESTful form methods

(comment

  ;; when designing RESTful applications you will be doing a lot of "PATCH"
  ;; and "DELETE" request, but most browsers don't support methods other
  ;; than "GET" and "POST" when it comes to submitting forms.

  ;; there is a pattern to solve this (pioneered by Rails) using a
  ;; hidden "_method" field in the form and swapping out the "POST" method for
  ;; whatever is in that field.

  ;; we can do this with middleware in reitit like this:

  (defn- hidden-method
    [request]
    ;; look for "_method" field in :form-params
    (some-> (or (get-in request [:form-params "_method"])
                ;; or in :multipart-params
                (get-in request [:multipart-params "_method"]))
            string/lower-case
            keyword))

  (def wrap-hidden-method
    {:name ::wrap-hidden-method
     :wrap (fn [handler]
             (fn [request]
               ;; if this is a :post request
               (if-let [fm (and (= :post (:request-method request))
                                ;; and there is a "_method" field
                                (hidden-method request))]
                 ;; replace :request-method
                 (handler (assoc request :request-method fm))
                 (handler request))))})

  ;; and apply the middleware like this:

  (reitit.ring/ring-handler
   (reitit.ring/router ...)
   (reitit.ring/create-default-handler)
   {:middleware
    ;; needed to have :form-params in the request map
    [reitit.ring.middleware.parameters/parameters-middleware
     ;; needed to have :multipart-params in the request map
     reitit.ring.middleware.multipart/multipart-middleware
     ;; our hidden method wrapper
     wrap-hidden-method]})

  ;; (NOTE: This middleware must be placed here and not inside the route data
  ;; given to `reitit.ring/handler`. This is so that our middleware is applied
  ;; before reitit matches the request with a specific handler using the wrong
  ;; method.)

)
