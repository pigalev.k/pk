(ns watch
  (:require [pod.babashka.fswatcher :as fw]))


(fw/watch "." prn {:delay-ms 5000})
(println "Watching current directory for changes... Press Ctrl-C to quit.")
@(promise)
