(ns input
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]))

;; echo -e "Some input\nand another line" | bb src/input.clj


;; one value
(comment (edn/read *in*))

;; all values
(comment (let [reader  (java.io.PushbackReader. (io/reader *in*))]
           (take-while #(not (identical? ::eof %)) (repeatedly #(edn/read {:eof ::eof} reader)))))

;; sequence of lines
(line-seq (io/reader *in*))
