(ns process
  (:require [babashka.process :as p]))

(defn io-prepl []
  (let [cmd  ["clojure" "-M"
             "-e" "(require '[clojure.core.server :as s])"
             "-e" "(s/io-prepl)"]
        proc (p/process cmd
                        {:inherit  true
                         :shutdown p/destroy-tree})]
    proc))

@(io-prepl)
