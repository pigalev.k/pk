(ns shell
  (:require [clojure.java.shell :refer [sh]]
            [clojure.string :as s]))


(sh "ls")

(sh "ls" "foo")

(defn user-id
  "Get operating system's user ID by user name.
  If user name is empty or not specified, return ID of the current user.
  If user with given name does not exist, return nil."
  ([]
   (user-id nil))
  ([user-name]
   (let [result (if (empty? user-name)
                  (sh "id" "-u")
                  (sh "id" "-u" user-name))]
     (if (empty? (:err result))
       (-> (:out result)
           s/trim
           Integer/parseInt)
       nil))))
