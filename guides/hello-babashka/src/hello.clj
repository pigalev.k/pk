(ns hello
  (:require [clojure.string :as str]))

;; can't be avoided

(str/join " " ["Hello" "inner" "world!"])

["It's" "me," "your" "wacky" "subconscious!"]

nil

(str "Hello from " *ns* ", inner world!")
