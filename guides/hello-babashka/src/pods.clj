(ns pods
  (:require [babashka.pods :as pods]
            [pod.babashka.fswatcher :as fw])) ;; pod defined in bb.edn

;; load a pod

(pods/load-pod 'org.babashka/buddy "0.1.0")
(require '[pod.babashka.buddy.core.hash :as hash])

;; use vars exposed by the pod

(hash/md5 "foo")


;; another pod

;; searches executable in the local filesystem (PATH)
(comment (pods/load-pod "pod-babashka-hsqldb"))

(pods/load-pod 'org.babashka/hsqldb "0.1.0")
(require '[pod.babashka.hsqldb :as sql])

(def db "jdbc:hsqldb:mem:testdb;sql.syntax_mys=true")
(sql/execute! db ["create table foo ( foo int );"])
(sql/execute! db ["insert into foo (foo) values (123);"])
(sql/execute! db ["insert into foo values (1, 2, 3);"])
(sql/execute! db ["select * from foo;"])


;; and another

(pods/load-pod "bootleg")
(require '[pod.retrogradeorbit.bootleg.utils :as utils])

(-> [:div
      [:h1 "Using Bootleg From Babashka"]
      [:p "This is a demo"]]
  (utils/convert-to :html))

;; and another

(pods/load-pod 'org.babashka/go-sqlite3 "0.1.0")
(require '[pod.babashka.go-sqlite3 :as sqlite])

(sqlite/execute! "/tmp/foo.db"
                 ["create table if not exists foo (the_text TEXT, the_int INTEGER, the_real REAL, the_blob BLOB)"])

(sqlite/execute! "/tmp/foo.db"
                 ["insert into foo (the_text, the_int, the_real, the_blob) values (?,?,?,?)" "foo" 1 3.14 nil])

(sqlite/query "/tmp/foo.db" ["select * from foo order by the_int asc"])


;; and yet another

(fw/watch "." prn {:delay-ms 5000})
(println "Watching current directory for changes... Press Ctrl-C to quit.")

@(promise)
