# hello-babashka

Exploring Babashka --- native, fast starting Clojure interpreter for scripting.

- https://github.com/babashka/babashka
- https://book.babashka.org/

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
