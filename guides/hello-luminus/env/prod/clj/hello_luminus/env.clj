(ns hello-luminus.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[hello-luminus started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[hello-luminus has shut down successfully]=-"))
   :middleware identity})
