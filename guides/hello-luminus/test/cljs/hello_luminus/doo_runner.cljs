(ns hello-luminus.doo-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [hello-luminus.core-test]))

(doo-tests 'hello-luminus.core-test)

