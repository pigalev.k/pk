(ns hello-column-db.duckdb
  (:require [tmducken.duckdb :as duckdb]
            [tech.v3.dataset :as ds]))


;; basics

;; - https://techascent.com/blog/just-ducking-around.html
;; - https://github.com/techascent/tmducken

(comment

  ;; create dataset from csv file

  (def stocks
        (ds/->dataset "resources/stocks.csv"
                      {:key-fn keyword
                       :dataset-name :stocks}))

  (ds/head stocks)

  ;; initialize DuckDB

  ;; `libduckdb.so` should be present in `binaries` folder; see
  ;; https://github.com/duckdb/duckdb/releases (also install `duckdb` binary
  ;; in PATH)

  (duckdb/initialize!)

  ;; open a database

  (def db (duckdb/open-db))

  ;; connect to the database

  (def conn (duckdb/connect db))

  ;; create table and insert dataset to db

  (duckdb/create-table! conn stocks)
  (duckdb/insert-dataset! conn stocks)

  ;; issue queries

  (ds/head (duckdb/sql->dataset conn "select * from stocks"))

  (def stmt (duckdb/prepare conn "select * from stocks"))

  (def res (stmt))

  (ds/head (first res))

  ;; close connection and db

  (duckdb/disconnect conn)
  (duckdb/close-db db)

)
