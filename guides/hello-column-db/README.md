# hello-column-db

Exploring column databases.

- DuckDB
  - https://duckdb.org/
  - https://github.com/duckdb/duckdb/releases

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
