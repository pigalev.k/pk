# Implementation overview

An interpreter/compiler for the Lox language is implemented in a series of
steps, adding one feature (or subfeature) at a time; later features build on the
results of previous ones. Each implementation step leaves interpreter in a
working state, so we always have functioning software with a steadily growing
set of features.

## Implementation path



### jlox

### clox

## Concepts

### Garbage collection

Objects that cannot be referenced from the garbage-collection **roots**
(**garbage**), may be freed, otherwise object is **live**.

The garbage-collection cycle consists of two phases: **mark** all live objects
and **sweep** all unmarked ones.

The **tricolor abstraction** aids to distinguish live objects from garbage while
avoiding reference cycles. Each color represents a state the object is in, and
what should be done with it.

- **white**: initial color; means we have not reached or processed the object
- **gray**: reached first time; means the object itself is reachable and thus
  live, but its references have not been traversed yet; gray objects are the
  **worklist**
- **black**: gray objects with all references traversed; processing is done, and
  the object is live

The marking process:
1. Start with all objects white.
2. Find all roots and mark them gray.
3. Repeat while there are still gray objects:
   1. Pick a gray object; turn any white objects that the object mentions to
      gray.
   2. Mark the original gray object black.

The **tricolor invariant**: no black node ever points to a white node; no live
object is ever collected.
