#ifndef lox_debug_h
#define lox_debug_h

#include "chunk.h"
#include "vm.h"


void disassembleChunk(Chunk* chunk, const char* name);
int disassembleInstruction(Chunk* chunk, int offset);
void dumpStack(Value stack[], Value* stackTop);

#endif
