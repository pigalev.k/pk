#ifndef lox_object_h
#define lox_object_h

#include "common.h"
#include "value.h"
#include "chunk.h"
#include "table.h"


#define OBJ_TYPE(value) (AS_OBJ(value)->type)

#define IS_BOUND_METHOD(value) isObjType(value, OBJ_BOUND_METHOD)
#define IS_CLASS(value) isObjType(value, OBJ_CLASS)
#define IS_CLOSURE(value) isObjType(value, OBJ_CLOSURE)
#define IS_FUNCTION(value) isObjType(value, OBJ_FUNCTION)
#define IS_INSTANCE(value) isObjType(value, OBJ_INSTANCE)
#define IS_NATIVE(value) isObjType(value, OBJ_NATIVE)
#define IS_STRING(value) isObjType(value, OBJ_STRING)

#define AS_BOUND_METHOD(value) ((ObjBoundMethod*)AS_OBJ(value))
#define AS_CLASS(value) ((ObjClass*)AS_OBJ(value))
#define AS_CLOSURE(value) ((ObjClosure*)AS_OBJ(value))
#define AS_FUNCTION(value) ((ObjFunction*)AS_OBJ(value))
#define AS_INSTANCE(value) ((ObjInstance*)AS_OBJ(value))
#define AS_NATIVE(value) (((ObjNative*)AS_OBJ(value))->function)
#define AS_STRING(value) ((ObjString*)AS_OBJ(value))
#define AS_CSTRING(value) (((ObjString*)AS_OBJ(value))->chars)


typedef enum {
    OBJ_BOUND_METHOD,
    OBJ_CLASS,
    OBJ_CLOSURE,
    OBJ_FUNCTION,
    OBJ_INSTANCE,
    OBJ_NATIVE,
    OBJ_STRING,
    OBJ_UPVALUE
} ObjType;


struct Obj {
    ObjType type;
    // objects allocated on the heap form an intrusive linked list so they can
    // always be found, e.g, by garbage collector
    struct Obj* next;
    // if object is marked for garbage-collection purposes
    bool isMarked;
};


typedef struct {
    Obj obj;
    // number of parameters
    int arity;
    // count of the upvalues
    int upvalueCount;
    // bytecode for function's body
    Chunk chunk;
    ObjString* name;
} ObjFunction;

typedef Value (*NativeFn)(int argCount, Value* args);

ObjFunction* newFunction();


typedef struct {
    Obj obj;
    // native function values do not have bytecode chunks, they refer to some
    // native code instead
    NativeFn function;
} ObjNative;

ObjNative* newNative(NativeFn function);


typedef struct ObjUpvalue {
    Obj obj;
    // a reference, not a value --- can be mutated in closures
    Value* location;
    // holds the location value in closed upvalue (i.e., taken off the stack and
    // placed on the heap)
    Value closed;
    // an intrusive linked list of all VM's open upvalues (still on the stack),
    // sorted by the stack slot index they point to
    struct ObjUpvalue* next;
} ObjUpvalue;

ObjUpvalue* newUpvalue(Value* slot);


typedef struct {
    Obj obj;
    ObjFunction* function;
    // different closures can contain different number of upvalues, we need a
    // dynamic array
    ObjUpvalue** upvalues;
    int upvalueCount;
} ObjClosure;

ObjClosure* newClosure(ObjFunction* function);


typedef struct {
    Obj obj;
    ObjString* name;
    Table methods;
} ObjClass;

ObjClass* newClass(ObjString* name);


typedef struct {
    Obj obj;
    ObjClass* klass;
    Table fields;
} ObjInstance;

ObjInstance* newInstance(ObjClass* klass);


typedef struct {
    Obj obj;
    Value receiver;
    ObjClosure* method;
} ObjBoundMethod;

ObjBoundMethod* newBoundMethod(Value receiver, ObjClosure* method);


struct ObjString {
    // state inheritance through composition: Obj is at the beginning of
    // ObjString* in memory, so ObjString* can be cast to Obj* and members of
    // Obj (e.g., type) safely accessed
    Obj obj;
    // Lox strings are immutable, so length can be just stored on creation
    int length;
    // null-terminated, allocated on the heap
    char* chars;
    // Lox strings are immutable, so hash could be computed on creation
    uint32_t hash;
};

ObjString* takeString(char* chars, int length);
ObjString* copyString(const char* chars, int length);


void printObject(Value value);


/**
 * Returns true if the value is an object of the given type. Used to avoid
 * multiple evaluations of value in macros.
 */
static inline bool isObjType(Value value, ObjType type) {
    return IS_OBJ(value) && AS_OBJ(value)->type == type;
}

#endif
