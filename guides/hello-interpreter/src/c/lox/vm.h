#ifndef lox_vm_h
#define lox_vm_h

#include "chunk.h"
#include "value.h"
#include "table.h"
#include "object.h"


#define FRAMES_MAX 64
#define STACK_MAX (FRAMES_MAX * UINT8_COUNT)


// represents a single ongoing function call
typedef struct {
    // a closure for a function being called
    ObjClosure* closure;
    // an instruction pointer of this function
    uint8_t* ip;
    // points at the first slot of the VM's value stack that this function can
    // use for locals
    Value* slots;
} CallFrame;

typedef struct {
    CallFrame frames[FRAMES_MAX];
    int frameCount;
    // a value stack
    Value stack[STACK_MAX];
    // points past the last value in the value stack
    Value* stackTop;
    // global variables
    Table globals;
    // all strings are automatically interned and unique
    Table strings;
    // name of the instance initializer method
    ObjString* initString;
    // an intrusive linked list of all VM's open upvalues (still on the stack),
    // sorted by the stack slot index they point to
    ObjUpvalue* openUpvalues;

    // garbage-collector-related data

    // total bytes allocated on the heap
    size_t bytesAllocated;
    // threshold for triggering the GC
    size_t nextGC;
    // an intrusive linked list of all objects allocated on the heap
    Obj* objects;
    // garbage collector's worklist
    int grayCount;
    int grayCapacity;
    Obj** grayStack;
} VM;

typedef enum {
    INTERPRET_OK,
    INTERPRET_COMPILE_ERROR,
    INTERPRET_RUNTIME_ERROR
} InterpretResult;

// the global VM instance
extern VM vm;

// initialize/destroy the VM
void initVM();
void freeVM();

// interpret the source
InterpretResult interpret(const char* source);

// manipulate the VM's value stack
void push(Value value);
Value pop();

#endif
