#ifndef lox_common_h
#define lox_common_h

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

// can be set with -D compiler option at build time, see Makefile
/* #define DEBUG_TRACE_EXECUTION */
/* #define DEBUG_PRINT_CODE */
/* #define DEBUG_STRESS_GC */
/* #define DEBUG_LOG_GC */

#define UINT8_COUNT (UINT8_MAX + 1)

#endif
