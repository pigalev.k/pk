#ifndef lox_table_h
#define lox_table_h

#include "common.h"
#include "value.h"


// a hash table data structure, that uses open addressing (closed hashing) with
// linear probing algorithm


typedef struct {
    ObjString* key;
    Value value;
} Entry;

typedef struct {
    int count;
    int capacity;
    Entry* entries;
} Table;


// initialize and clear the table
void initTable(Table* table);
void freeTable(Table* table);

// add, find and remove entries
bool tableGet(Table* table, ObjString* key, Value* value);
bool tableSet(Table* table, ObjString* key, Value value);
bool tableDelete(Table* table, ObjString* key);
void tableAddAll(Table* from, Table* to);
ObjString* tableFindString(Table* table, const char* chars,
                           int length, uint32_t hash);
void markTable(Table* table);
void tableRemoveWhite(Table* table);

#endif
