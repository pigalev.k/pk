#include <stdlib.h>
#include <string.h>

#include "memory.h"
#include "object.h"
#include "table.h"
#include "value.h"


// maximum entries/buckets ratio (count/capacity), after which capacity of the
// table (length of array of entries) should be increased
#define TABLE_MAX_LOAD 0.75


static Entry* findEntry(Entry* entries, int capacity, ObjString* key);
static void adjustCapacity(Table* table, int capacity);


// public interface

/**
 * Initializes the new hash table.
 */
void initTable(Table* table) {
    table->count = 0;
    table->capacity = 0;
    table->entries = NULL;
}

/**
 * Resets the table to the initial state (clears the table contents).
 */
void freeTable(Table* table) {
    FREE_ARRAY(Entry, table->entries, table->capacity);
    initTable(table);
}

/**
 * Retrieves the value for the key from table.
 */
bool tableGet(Table* table, ObjString* key, Value* value) {
    if (table->count == 0) return false;

    Entry* entry = findEntry(table->entries, table->capacity, key);
    if (entry->key == NULL) return false;

    *value = entry->value;
    return true;
}

/**
 * Adds the given key-value pair to the given table. If an entry for that key is
 * already present, the new value overwrites the old value. Returns true if a
 * new entry was added. Checks the table load and adjusts the capacity, if
 * needed, before adding the entry.
 */
bool tableSet(Table* table, ObjString* key, Value value) {
    if (table->count + 1 > table->capacity * TABLE_MAX_LOAD) {
        int capacity = GROW_CAPACITY(table->capacity);
        adjustCapacity(table, capacity);
    }

    Entry* entry = findEntry(table->entries, table->capacity, key);
    bool isNewKey = entry->key == NULL;
    if (isNewKey && IS_NIL(entry->value)) table->count++;

    entry->key = key;
    entry->value = value;
    return isNewKey;
}

/**
 * Deletes the entry with a given key from the table.
 */
bool tableDelete(Table* table, ObjString* key) {
    // if we just delete an entry from hash table that uses open addressing, it
    // may break probe sequences for some other entries; so the entry delendum
    // is replaced with a sentinel entry (tombstone) instead, that treated as
    // occupied on lookup and as empty on insertion

    if (table->count == 0) return false;

    // find the entry
    Entry* entry = findEntry(table->entries, table->capacity, key);
    if (entry->key == NULL) return false;

    // found, place a tombstone here (NULL key, true value)
    entry->key = NULL;
    entry->value = BOOL_VAL(true);
    // we do not decrease count here, because tombstones are counted as full
    // entries

    return true;
}

/**
 * Copies all entries of the from table to the to table. Leaves target table's
 * entries with keys not in source table intact.
 */
void tableAddAll(Table* from, Table* to) {
    for (int i = 0; i < from->capacity; i++) {
        Entry* entry = &from->entries[i];
        if (entry->key != NULL) {
            tableSet(to, entry->key, entry->value);
        }
    }
}

/**
 * Returns a pointer to the table entry key that is the same as chars, or NULL
 * if not found.
 */
ObjString* tableFindString(Table* table, const char* chars,
                           int length, uint32_t hash) {
    if (table->count == 0) return NULL;

    uint32_t index = hash % table->capacity;
    for (;;) {
        Entry* entry = &table->entries[index];
        if (entry->key == NULL) {
            // stop if empty non-tombstone entry is found
            if (IS_NIL(entry->value)) return NULL;
        } else if (entry->key->length == length &&
                   entry->key->hash == hash &&
                   memcmp(entry->key->chars, chars, length) == 0) {
            // we found it
            return entry->key;
        }

        index = (index + 1) % table->capacity;
    }
}

/**
 * Marks all table keys and values.
 */
void markTable(Table* table) {
    for (int i = 0; i < table->capacity; i++) {
        Entry* entry = &table->entries[i];
        markObject((Obj*)entry->key);
        markValue(entry->value);
    }
}

/**
 * Removes the table entries whose values are white (not marked).
 */
void tableRemoveWhite(Table* table) {
    for (int i = 0; i < table->capacity; i++) {
        Entry* entry = &table->entries[i];
        if (entry->key != NULL && !entry->key->obj.isMarked) {
            tableDelete(table, entry->key);
        }
    }
}


// private helpers

/**
 * Returns a pointer to entry with the given key from the array of entries, or
 * to first empty entry/tombstone encountered during lookup if not found.
 */
static Entry* findEntry(Entry* entries, int capacity, ObjString* key) {
    uint32_t index = key->hash % capacity;
    // if entry will not be found but tombstone(s) encountered, we will return
    // the first tombstone (in case lookup was performed for insertion)
    Entry* tombstone = NULL;

    for (;;) {
        Entry* entry = &entries[index];
        if (entry->key == NULL) {
            // seems like empty entry, let's take a look-see
            if (IS_NIL(entry->value)) {
                // really empty entry
                return tombstone != NULL ? tombstone : entry;
            } else {
                // a tombstone
                if (tombstone == NULL) tombstone = entry;
            }
        } else if (entry->key == key) {
            // the key is found; all strings are interned so address equality is
            // sufficient
            return entry;
        }

        // linear probing
        index = (index + 1) % capacity;
    }
}

/**
 * Adjusts the table for the given new capacity.
 */
static void adjustCapacity(Table* table, int capacity) {
    // create a new array of required capacity for entries and initialize it
    // with empty entries
    Entry* entries = ALLOCATE(Entry, capacity);
    for (int i = 0; i < capacity; i++) {
        entries[i].key = NULL;
        entries[i].value = NIL_VAL;
    }

    // we will not reinsert tombstones, so count may change
    table->count = 0;
    // reinsert the old entries, if any, to the new array
    for (int i = 0; i < table->capacity; i++) {
        Entry* entry = &table->entries[i];
        if (entry->key == NULL) continue;

        Entry* dest = findEntry(entries, capacity, entry->key);
        dest->key = entry->key;
        dest->value = entry->value;
        table->count++;
    }

    // free old entries, set new entries and update capacity of the table
    FREE_ARRAY(Entry, table->entries, table->capacity);
    table->entries = entries;
    table->capacity = capacity;
}
