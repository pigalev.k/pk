#include <stdlib.h>

#include "chunk.h"
#include "memory.h"
#include "vm.h"


/**
 * Chunk is a dynamic array of bytes.
 */


/**
 * Initializes the chunk.
 */
void initChunk(Chunk* chunk) {
    chunk->count = 0;
    chunk->capacity = 0;
    chunk->code = NULL;
    chunk->lines = NULL;
    initValueArray(&chunk->constants);
}

/**
 * Appends a byte to the end of the chunk.
 */
void writeChunk(Chunk* chunk, uint8_t byte, int line) {
    // chunk's code array is already full, grow it first
    if (chunk->capacity < chunk->count + 1) {
        int oldCapacity = chunk->capacity;
        chunk->capacity = GROW_CAPACITY(oldCapacity);
        chunk->code = GROW_ARRAY(uint8_t, chunk->code,
                                 oldCapacity, chunk->capacity);
        chunk->lines = GROW_ARRAY(int, chunk->lines,
                                  oldCapacity, chunk->capacity);
    }
    // count conveniently point to an element just after the
    // current last one
    // append the new byte to the end of the code array
    chunk->code[chunk->count] = byte;
    // append the line number to the end of the lines array
    chunk->lines[chunk->count] = line;
    chunk->count++;
}

/**
 * Deallocates the chunk's memory and inits it to the empty state.
 */
void freeChunk(Chunk* chunk) {
    FREE_ARRAY(uint8_t, chunk->code, chunk->capacity);
    FREE_ARRAY(uint8_t, chunk->lines, chunk->capacity);
    freeValueArray(&chunk->constants);
    initChunk(chunk);
}

/**
 * Adds the constant to the chunk's array of constants, returns its index.
 */
int addConstant(Chunk* chunk, Value value) {
    push(value);
    writeValueArray(&chunk->constants, value);
    pop(value);
    return chunk->constants.count - 1;
}
