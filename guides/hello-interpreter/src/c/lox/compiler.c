#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "compiler.h"
#include "scanner.h"
#include "memory.h"

#ifdef DEBUG_PRINT_CODE
#include "debug.h"
#endif


// it is a one-pass compiler, that parses the sequence of lexemes (frontend) and
// generates (byte)code from it (backend) in one go without any intermediate
// representation

// parser enters panic mode on syntax errors and suppresses all subsequent
// syntax errors while it lasts; panic mode ends when the parser reaches a
// synchronization point (statement boundary)

// as the VM is stack-based, bytecode for instructions and operands should be
// generated in the Reverse Polish Notation; instructions can put constants
// (from the constant pool) onto the stack, or take value(s) from the stack,
// perform some operation on them and put the result back onto the stack

// global variables are resolved at runtime (dynamically) and stored in the
// global hashmap, local variables are resolved at compile-time (statically),
// have lexical scope and are stored in the compiler's locals array at
// compile-time and on the VM's value stack at runtime


// parsing the sequence of tokens

typedef struct {
    // just read from scanner
    Token current;
    Token previous;
    // had syntax error(s)
    bool hadError;
    // do not report errors or execute bytecode
    bool panicMode;
} Parser;


typedef enum {
    PREC_NONE,
    PREC_ASSIGNMENT, // =
    PREC_OR,         // or
    PREC_AND,        // and
    PREC_EQUALITY,   // == !=
    PREC_COMPARISON, // < > <= >=
    PREC_TERM,       // + -
    PREC_FACTOR,     // * /
    PREC_UNARY,      // ! -
    PREC_CALL,       // . ()
    PREC_PRIMARY
} Precedence;


// ParseFn is a pointer to function returning void
typedef void (*ParseFn)(bool canAssign);

typedef struct {
    ParseFn prefix;
    ParseFn infix;
    Precedence precedence;
} ParseRule;


static void advance();
static void consume(TokenType type, const char* message);
static bool match(TokenType type);
static bool check(TokenType type);
static void error(const char* message);
static void errorAt(Token* token, const char* message);
static void errorAtCurrent(const char* message);
static void synchronize();
static ParseRule* getRule(TokenType type);
static void parsePrecedence(Precedence precedence);


// tracking local variables and upvalues in lexical scopes

typedef struct {
    // name of the local variable
    Token name;
    // depth of the lexical scope where this local is defined; additionally, -1
    // means that the variable is uninitialized (declared but not defined yet in
    // this scope)
    int depth;
    // true if local is captured by some closure and need to be closed (moved to
    // the heap) before going out of scope and off the stack
    bool isCaptured;
} Local;


typedef struct {
    // slot number on the stack of the enclosing function's local/upvalue
    // captured by this upvalue
    uint8_t index;
    // true if the upvalue captures an enclosing function's local (not another,
    // upper upvalue, that captures a local of the function enclosing enclosing
    // function)
    bool isLocal;
} Upvalue;


typedef enum {
    // an ordinary function
    TYPE_FUNCTION,
    // an instance initializer
    TYPE_INITIALIZER,
    // a class method
    TYPE_METHOD,
    // an implicit top-level function
    TYPE_SCRIPT
} FunctionType;


typedef struct Compiler {
    // can't reference typedef but can reference struct's tag
    struct Compiler* enclosing;
    // a function currently being compiled
    ObjFunction* function;
    FunctionType type;
    // list of all locals declared in a program; used to resolve local names at
    // compile-time and to initially populate the VM's value stack at runtime
    Local locals[UINT8_COUNT];
    int localCount;
    // upvalues for the compiler's function
    Upvalue upvalues[UINT8_COUNT];
    // depth of the current lexical scope (0 is global, 1 is top-level block
    // etc)
    int scopeDepth;
} Compiler;

static void initCompiler(Compiler* compiler, FunctionType type);


typedef struct ClassCompiler {
    struct ClassCompiler* enclosing;
    bool hasSuperclass;
} ClassCompiler;


// shared state

Parser parser;
Compiler* current = NULL;
ClassCompiler* currentClass = NULL;
Chunk* compilingChunk;


// emitting bytecode

static void declaration();
static void classDeclaration();
static void method();
static void this_(bool canAssign);
static void super_(bool canAssign);
static void funDeclaration();
static void function();
static void varDeclaration();
static void variable(bool canAssign);
static void namedVariable(Token name, bool canAssign);
static void statement();
static void returnStatement();
static void ifStatement();
static void and_(bool canAssign);
static void or_(bool canAssign);
static void whileStatement();
static void forStatement();
static void printStatement();
static void expressionStatement();
static void block();
static void expression();
static void number(bool canAssign);
static void string(bool canAssign);
static void grouping(bool canAssign);
static void unary(bool canAssign);
static void binary(bool canAssign);
static void literal(bool canAssign);
static void call(bool canAssign);
static void dot(bool canAssign);
static uint8_t argumentList();
static void emitByte(uint8_t byte);
static void emitBytes(uint8_t instruction, uint8_t operand);
static int emitJump(uint8_t instruction);
static void patchJump(int offset);
static void emitLoop(int loopStart);
static void emitConstant(Value value);
static void defineVariable(uint8_t global);
static void declareVariable();
static void emitReturn();
static ObjFunction* endCompilation();


// helpers

static Chunk* currentChunk();
static uint8_t makeConstant(Value value);
static uint8_t identifierConstant(Token* name);
static uint8_t parseVariable(const char* errorMessage);
static void beginScope();
static void endScope();
static void addLocal(Token name);
static int resolveLocal(Compiler* compiler, Token* name);
static bool identifiersEqual(Token* a, Token* b);
static void markInitialized();
static int resolveUpvalue(Compiler* compiler, Token* name);
static int addUpvalue(Compiler* compiler, uint8_t index, bool isLocal);
static Token syntheticToken(const char* text);


// Pratt parser rules table (data-driven parsing?)

ParseRule rules[] = {
    // & is not needed to take function's address
    [TOKEN_LEFT_PAREN]    = { grouping, call,   PREC_CALL },
    [TOKEN_RIGHT_PAREN]   = { NULL,     NULL,   PREC_NONE },
    [TOKEN_LEFT_BRACE]    = { NULL,     NULL,   PREC_NONE },
    [TOKEN_RIGHT_BRACE]   = { NULL,     NULL,   PREC_NONE },
    [TOKEN_COMMA]         = { NULL,     NULL,   PREC_NONE },
    [TOKEN_DOT]           = { NULL,     dot,    PREC_CALL },
    [TOKEN_MINUS]         = { unary,    binary, PREC_TERM },
    [TOKEN_PLUS]          = { NULL,     binary, PREC_TERM },
    [TOKEN_SEMICOLON]     = { NULL,     NULL,   PREC_NONE },
    [TOKEN_SLASH]         = { NULL,     binary, PREC_FACTOR },
    [TOKEN_STAR]          = { NULL,     binary, PREC_FACTOR },
    [TOKEN_BANG]          = { unary,    NULL,   PREC_NONE },
    [TOKEN_BANG_EQUAL]    = { NULL,     binary, PREC_EQUALITY },
    [TOKEN_EQUAL]         = { NULL,     NULL,   PREC_NONE },
    [TOKEN_EQUAL_EQUAL]   = { NULL,     binary, PREC_EQUALITY },
    [TOKEN_GREATER]       = { NULL,     binary, PREC_COMPARISON },
    [TOKEN_GREATER_EQUAL] = { NULL,     binary, PREC_COMPARISON },
    [TOKEN_LESS]          = { NULL,     binary, PREC_COMPARISON },
    [TOKEN_LESS_EQUAL]    = { NULL,     binary, PREC_COMPARISON },
    [TOKEN_IDENTIFIER]    = { variable, NULL,   PREC_NONE },
    [TOKEN_STRING]        = { string,   NULL,   PREC_NONE },
    [TOKEN_NUMBER]        = { number,   NULL,   PREC_NONE },
    [TOKEN_AND]           = { NULL,     and_,    PREC_AND },
    [TOKEN_CLASS]         = { NULL,     NULL,   PREC_NONE },
    [TOKEN_ELSE]          = { NULL,     NULL,   PREC_NONE },
    [TOKEN_FALSE]         = { literal,  NULL,   PREC_NONE },
    [TOKEN_FOR]           = { NULL,     NULL,   PREC_NONE },
    [TOKEN_FUN]           = { NULL,     NULL,   PREC_NONE },
    [TOKEN_IF]            = { NULL,     NULL,   PREC_NONE },
    [TOKEN_NIL]           = { literal,  NULL,   PREC_NONE },
    [TOKEN_OR]            = { NULL,     or_,    PREC_OR },
    [TOKEN_PRINT]         = { NULL,     NULL,   PREC_NONE },
    [TOKEN_RETURN]        = { NULL,     NULL,   PREC_NONE },
    [TOKEN_SUPER]         = { super_,   NULL,   PREC_NONE },
    [TOKEN_THIS]          = { this_,    NULL,   PREC_NONE },
    [TOKEN_TRUE]          = { literal,  NULL,   PREC_NONE },
    [TOKEN_VAR]           = { NULL,     NULL,   PREC_NONE },
    [TOKEN_WHILE]         = { NULL,     NULL,   PREC_NONE },
    [TOKEN_ERROR]         = { NULL,     NULL,   PREC_NONE },
    [TOKEN_EOF]           = { NULL,     NULL,   PREC_NONE },
};


// public interface

/**
 * Compiles the source to bytecode.
 */
ObjFunction* compile(const char* source) {
    initScanner(source);
    Compiler compiler;
    initCompiler(&compiler, TYPE_SCRIPT);

    parser.hadError = false;
    parser.panicMode = false;

    advance();

    while(!match(TOKEN_EOF)) {
        declaration();
    }

    ObjFunction* function = endCompilation();
    return parser.hadError ? NULL : function;
}

/**
 * Marks compile-time garbage-collection roots.
 */
void markCompilerRoots() {
    Compiler* compiler = current;
    while (compiler != NULL) {
        markObject((Obj*)compiler->function);
        compiler = compiler->enclosing;
    }
}


// private helpers

/**
 * Emits bytecode for a declaration.
 */
static void declaration() {
    if (match(TOKEN_CLASS)) {
        classDeclaration();
    } else if (match(TOKEN_FUN)) {
        funDeclaration();
    } else if (match(TOKEN_VAR)) {
        varDeclaration();
    } else {
        statement();
    }

    if (parser.panicMode) synchronize();
}

/**
 * Emits bytecode for a class declaration.
 */
static void classDeclaration() {
    consume(TOKEN_IDENTIFIER, "Expect class name.");
    Token className = parser.previous;

    uint8_t nameConstant = identifierConstant(&parser.previous);
    declareVariable();

    emitBytes(OP_CLASS, nameConstant);
    defineVariable(nameConstant);

    // class chain to detect inappropriate use of 'this', i.e., outside of a
    // class declaration
    ClassCompiler classCompiler;
    classCompiler.hasSuperclass = false;
    classCompiler.enclosing = currentClass;
    currentClass = &classCompiler;

    // superclass
    if (match(TOKEN_LESS)) {
        consume(TOKEN_IDENTIFIER, "Expect superclass name.");
        variable(false);

        if (identifiersEqual(&className, &parser.previous)) {
            error("A class can't inherit from itself.");
        }

        beginScope();
        addLocal(syntheticToken("super"));
        defineVariable(0);

        namedVariable(className, false);
        emitByte(OP_INHERIT);
        classCompiler.hasSuperclass = true;
    }

    namedVariable(className, false);

    consume(TOKEN_LEFT_BRACE, "Expect '{' before class body.");
    while(!check(TOKEN_RIGHT_BRACE) && !check(TOKEN_EOF)) {
        method();
    }
    consume(TOKEN_RIGHT_BRACE, "Expect '}' after class body.");

    emitByte(OP_POP);

    if (classCompiler.hasSuperclass) {
        endScope();
    }

    currentClass = currentClass->enclosing;
}

/**
 * Emits bytecode for a method declaration.
 */
static void method() {
    consume(TOKEN_IDENTIFIER, "Expect method name.");
    uint8_t constant = identifierConstant(&parser.previous);

    FunctionType type = TYPE_METHOD;
    if (parser.previous.length == 4 &&
        memcmp(parser.previous.start, "init", 4) == 0) {
        type = TYPE_INITIALIZER;
    }
    function(type);

    emitBytes(OP_METHOD, constant);
}

/**
 * Emits bytecode for a 'this' keyword access.
 */
static void this_(bool canAssign) {
    if (currentClass == NULL) {
        error("Can't use 'this' outside of a class.");
        return;
    }

    variable(false);
}

/**
 * Emits bytecode for a 'super' keyword access.
 */
static void super_(bool canAssign) {
    if (currentClass == NULL) {
        error("Can't use 'super' ouside of a class.");
    } else if (!currentClass->hasSuperclass) {
        error("Can't use 'super' in a class with no superclass.");
    }

    consume(TOKEN_DOT, "Expect '.' after 'super'.");
    consume(TOKEN_IDENTIFIER, "Expect superclass method name.");
    uint8_t name = identifierConstant(&parser.previous);

    namedVariable(syntheticToken("this"), false);
    if (match(TOKEN_LEFT_PAREN)) {
        uint8_t argCount = argumentList();
        namedVariable(syntheticToken("super"), false);
        emitBytes(OP_SUPER_INVOKE, name);
        emitByte(argCount);
    } else {
        namedVariable(syntheticToken("super"), false);
        emitBytes(OP_GET_SUPER, name);
    }
}

/**
 * Emits bytecode for a function declaration.
 */
static void funDeclaration() {
    uint8_t global = parseVariable("Expect function name.");
    markInitialized();
    function(TYPE_FUNCTION);
    defineVariable(global);
}

/**
 * Emits bytecode for creating a function value constant.
 */
static void function(FunctionType type) {
    // each, function has its own compiler
    Compiler compiler;
    // make the compiler the current one, so all emitted bytecode will go to the
    // function's chunk
    initCompiler(&compiler, type);
    beginScope();

    // parameter list
    consume(TOKEN_LEFT_PAREN, "Expect '(' after function name.");
    if (!check(TOKEN_RIGHT_PAREN)) {
        do {
            current->function->arity++;
            if (current->function->arity > 255) {
                errorAtCurrent("Can't have more than 255 function parameters");
            }
            // function formal parameters are just local variables, but they are
            // initialized later, during the function call
            uint8_t constant = parseVariable("Expect function parameter name.");
            defineVariable(constant);
        } while (match(TOKEN_COMMA));
    }
    consume(TOKEN_RIGHT_PAREN, "Expect ')' after parameters.");
    consume(TOKEN_LEFT_BRACE, "Expect '{' before function body.");

    // body
    block();

    // no need to close the scope before the end of compilation
    ObjFunction* function = endCompilation();
    // wrap the result of compilation in closure and store the created closure
    // object in the constant table of the surrounding function
    emitBytes(OP_CLOSURE, makeConstant(OBJ_VAL(function)));

    // emit two bytes for each of the closure's upvalues --- whether it refers
    // to a local or to another upvalue, and the local slot/upvalue's index it
    // captures
    for (int i = 0; i < function->upvalueCount; i++) {
        emitByte(compiler.upvalues[i].isLocal ? 1 : 0);
        emitByte(compiler.upvalues[i].index);
    }
}

/**
 * Emits bytecode for a variable declaration.
 */
static void varDeclaration() {
    uint8_t global = parseVariable("Expect variable name.");

    // if variable is not explicitly initialized, it is implicitly initialized
    // to nil
    if (match(TOKEN_EQUAL)) {
        expression();
    } else {
        emitByte(OP_NIL);
    }

    consume(TOKEN_SEMICOLON, "Expect ';' after variable declaration.");
    defineVariable(global);
}

/**
 * Emits bytecode for a variable access.
 */
static void variable(bool canAssign) {
    namedVariable(parser.previous, canAssign);
}

/**
 * Emits bytecode for accessing a named variable and placing the value onto the
 * stack.
 */
static void namedVariable(Token name, bool canAssign) {
    uint8_t getOp, setOp;

    // try to find a local with the given name first, else try to find it in
    // upvalues, else assume it is a global
    int arg = resolveLocal(current, &name);
    if (arg != -1) {
        getOp = OP_GET_LOCAL;
        setOp = OP_SET_LOCAL;
    } else if ((arg = resolveUpvalue(current, &name)) != -1) {
        getOp = OP_GET_UPVALUE;
        setOp = OP_SET_UPVALUE;
    } else {
        arg = identifierConstant(&name);
        getOp = OP_GET_GLOBAL;
        setOp = OP_SET_GLOBAL;
    }

    if (canAssign && match(TOKEN_EQUAL)) {
        expression();
        emitBytes(setOp, (uint8_t)arg);
    } else {
        emitBytes(getOp, (uint8_t)arg);
    }
}

/**
 * Emits bytecode for a statement.
 */
static void statement() {
    if (match(TOKEN_PRINT)) {
        printStatement();
    } else if (match(TOKEN_FOR)) {
        forStatement();
    } else if (match(TOKEN_IF)) {
        ifStatement();
    } else if (match(TOKEN_RETURN)) {
        returnStatement();
    } else if (match(TOKEN_WHILE)) {
        whileStatement();
    } else if (match(TOKEN_LEFT_BRACE)) {
        beginScope();
      block();
      endScope();
    } else {
        expressionStatement();
    }
}

/**
 * Emits bytecode for a return statement.
 */
static void returnStatement() {
    if (current->type == TYPE_SCRIPT) {
        error("Can't return from top-level code.");
    }

    if (match(TOKEN_SEMICOLON)) {
        emitReturn();
    } else {
        if (current->type == TYPE_INITIALIZER) {
            error("Can't return a value from an initializer.");
        }
        expression();
        consume(TOKEN_SEMICOLON, "Expect ';' after return value.");
        emitByte(OP_RETURN);
    }
}

/**
 * Emits bytecode for an if statement.
 */
static void ifStatement() {
    consume(TOKEN_LEFT_PAREN, "Expect '(' after 'if'.");
    expression();
    consume(TOKEN_RIGHT_PAREN, "Expect ')' after condition.");

    // then clause

    // emit the jump instruction with a placeholder offset operand
    int thenJump = emitJump(OP_JUMP_IF_FALSE);
    // emit the pop instruction to pop the value of condition expression from
    // the stack
    emitByte(OP_POP);
    // emit bytecode for then clause
    statement();
    // emit the jump instruction with a placeholder offset operand
    // (unconditionally jump over else clause after then clause is executed)
    int elseJump = emitJump(OP_JUMP);
    // replace the placeholder offset with the length of the then clause (and
    // unconditional jump after it) bytecode
    patchJump(thenJump);

    // else clause

    // emit the pop instruction to pop the value of condition expression from
    // the stack
    emitByte(OP_POP);
    // emit bytecode for else clause, if any
    if (match(TOKEN_ELSE)) statement();
    // replace the placeholder offset with the length of the else clause
    // bytecode
    patchJump(elseJump);
}

/**
 * Emits bytecode for an and logical operator.
 */
static void and_(bool canAssign) {
    // value of the left-hand expression is already on top of the stack, if it
    // is falsey, we skip the right operand and leave this value as the result
    // of the entire expression
    int endJump = emitJump(OP_JUMP_IF_FALSE);
    emitByte(OP_POP);
    parsePrecedence(PREC_AND);
    // will jump here if the left-hand value is falsey and skip popping and all
    // right-hand operand instructions that will be emitted (in 2 lines above)
    patchJump(endJump);
}

/**
 * Emits bytecode for an or logical operator.
 */
static void or_(bool canAssign) {
    // value of the left-hand expression is already on top of the stack, if it
    // is truthy, we skip over the right operand and leave this value as the
    // result of the entire expression
    int elseJump = emitJump(OP_JUMP_IF_FALSE);
    int endJump = emitJump(OP_JUMP);
    // will jump here if the left-hand value is falsey and skip the
    // unconditional jump above
    patchJump(elseJump);
    emitByte(OP_POP);
    parsePrecedence(PREC_OR);
    // will jump here if the left-hand value is truthy and the conditional jump
    // was not triggered, and skip popping and all right-hand operand
    // instructions that will be emitted (in 3 lines above)
    patchJump(endJump);
}

/**
 * Emits bytecode for a while loop.
 */
static void whileStatement() {
    int loopStart = currentChunk()->count;

    // condition
    consume(TOKEN_LEFT_PAREN, "Expect '(' after 'while'.");
    expression();
    consume(TOKEN_RIGHT_PAREN, "Expect ')' after condition.");

    // jump out if the condition is false
    int exitJump = emitJump(OP_JUMP_IF_FALSE);
    emitByte(OP_POP);

    // body
    statement();
    emitLoop(loopStart);

    patchJump(exitJump);
    emitByte(OP_POP);
}

/**
 * Emits bytecode for a for loop.
 */
static void forStatement() {
    beginScope();
    consume(TOKEN_LEFT_PAREN, "Expect '(' after 'for'.");

    // initializer
    if (match(TOKEN_SEMICOLON)) {
        // no initializer
    } else if (match(TOKEN_VAR)) {
        varDeclaration();
    } else {
        expressionStatement();
    }

    int loopStart = currentChunk()->count;
    int exitJump = -1;

    // condition
    if (!match(TOKEN_SEMICOLON)) {
        expression();
        consume(TOKEN_SEMICOLON, "Expect ';' after loop condition.");

        // jump out if the condition is false
        exitJump = emitJump(OP_JUMP_IF_FALSE);
        emitByte(OP_POP);
    }

    // increment
    if (!match(TOKEN_RIGHT_PAREN)) {
        int bodyJump = emitJump(OP_JUMP);
        int incrementStart = currentChunk()->count;
        expression();
        emitByte(OP_POP);
        consume(TOKEN_RIGHT_PAREN, "Expect ')' after for clauses.");

        emitLoop(loopStart);
        loopStart = incrementStart;
        patchJump(bodyJump);
    }

    // body
    statement();

    emitLoop(loopStart);

    if (exitJump != -1) {
        patchJump(exitJump);
        emitByte(OP_POP);
    }

    endScope();
}

/**
 * Emits bytecode for a print statement.
 */
static void printStatement() {
    expression();
    consume(TOKEN_SEMICOLON, "Expect ';' after value.");
    emitByte(OP_PRINT);
}

/**
 * Emits bytecode for an expression statement.
 */
static void expressionStatement() {
    expression();
    consume(TOKEN_SEMICOLON, "Expect ';' after expression.");
    emitByte(OP_POP);
}

/**
 * Emits bytecode for contents of a block.
 */
static void block() {
    while (!check(TOKEN_RIGHT_BRACE) && !check(TOKEN_EOF)) {
        declaration();
    }

    consume(TOKEN_RIGHT_BRACE, "Expect '}' after block.");
}

/**
 * Emits bytecode for an expression.
 */
static void expression() {
    parsePrecedence(PREC_ASSIGNMENT);
}

/**
 * Emits bytecode for a number literal expression.
 */
static void number(bool canAssign) {
    double value = strtod(parser.previous.start, NULL);
    emitConstant(NUMBER_VAL(value));
}

/**
 * Emits bytecode for a string literal expression.
 */
static void string(bool canAssign) {
    emitConstant(OBJ_VAL(copyString(parser.previous.start + 1,
                                    parser.previous.length - 2)));
}

/**
 * Emits bytecode for a grouping expression (actually just for the expression
 * between parentheses, no code emitted for the parens themselves).
 */
static void grouping(bool canAssign) {
    expression();
    consume(TOKEN_RIGHT_PAREN, "Expect ')' after expression.");
}

/**
 * Emits bytecode for an unary operator expression.
 */
static void unary(bool canAssign) {
    TokenType operatorType = parser.previous.type;

    // compile the operand, respecting operator precedence rules
    parsePrecedence(PREC_UNARY);

    // emit the operator instruction
    switch (operatorType) {
    case TOKEN_BANG: emitByte(OP_NOT); break;
    case TOKEN_MINUS: emitByte(OP_NEGATE); break;
    default: return; // unreachable
    }
}

/**
 * Emits bytecode for an binary operator expression.
 */
static void binary(bool canAssign) {
    TokenType operatorType = parser.previous.type;
    ParseRule* rule = getRule(operatorType);
    // parsing right-hand operand with precedence 1 level higher than this
    // operator's level, because binary operators are left-associative (to
    // consume this closest operand first rather than firstly defer to parsing
    // the expression to the right, as it will be with right-associative
    // operators like assignment); e.g. 1 + 2 + 3 -> ((1 + 2) + 3)
    parsePrecedence((Precedence)(rule->precedence + 1));

    switch (operatorType) {
    case TOKEN_BANG_EQUAL: emitBytes(OP_EQUAL, OP_NOT); break;
    case TOKEN_EQUAL_EQUAL: emitByte(OP_EQUAL); break;
    case TOKEN_GREATER: emitByte(OP_GREATER); break;
    case TOKEN_GREATER_EQUAL: emitBytes(OP_LESS, OP_NOT); break;
    case TOKEN_LESS: emitByte(OP_LESS); break;
    case TOKEN_LESS_EQUAL: emitBytes(OP_GREATER, OP_NOT); break;
    case TOKEN_PLUS: emitByte(OP_ADD); break;
    case TOKEN_MINUS: emitByte(OP_SUBTRACT); break;
    case TOKEN_STAR: emitByte(OP_MULTIPLY); break;
    case TOKEN_SLASH: emitByte(OP_DIVIDE); break;
    default: return; // unreachable
    }
}

/**
 * Emits bytecode for literals (boolean and nil).
 */
static void literal(bool canAssign) {
    switch (parser.previous.type) {
    case TOKEN_FALSE: emitByte(OP_FALSE); break;
    case TOKEN_NIL: emitByte(OP_NIL); break;
    case TOKEN_TRUE: emitByte(OP_TRUE); break;
    default: return; // unreachable
    }
}

/**
 * Emits bytecode for a function call.
 */
static void call(bool canAssign) {
    uint8_t argCount = argumentList();
    emitBytes(OP_CALL, argCount);
}

/**
 * Emits bytecode for a property (field or method) access.
 */
static void dot(bool canAssign) {
    consume(TOKEN_IDENTIFIER, "Expect property name after '.'.");
    uint8_t name = identifierConstant(&parser.previous);

    if (canAssign && match(TOKEN_EQUAL)) {
        expression();
        emitBytes(OP_SET_PROPERTY, name);
    } else if (match(TOKEN_LEFT_PAREN)) {
        uint8_t argCount = argumentList();
        emitBytes(OP_INVOKE, name);
        emitByte(argCount);
    } else {
        emitBytes(OP_GET_PROPERTY, name);
    }
}

/**
 * Emits bytecode for a function's arguments list and returns number of the
 * passed arguments.
 */
static uint8_t argumentList() {
    uint8_t argCount = 0;
    if (!check(TOKEN_RIGHT_PAREN)) {
        do {
            expression();
            if (argCount == 255) {
                error("Can't have more than 255 arguments.");
            }
            argCount++;
        } while (match(TOKEN_COMMA));
    }
    consume(TOKEN_RIGHT_PAREN, "Expect ')' after function arguments.");
    return argCount;
}

/**
 * Advances parser's current position. Saves the current token position, takes
 * next token from the scanner and makes it the current (next to be
 * parsed). Reports and skips tokens representing lexing errors.
 */
static void advance() {
    parser.previous = parser.current;

    for (;;) {
        parser.current = scanToken();
        if (parser.current.type != TOKEN_ERROR) break;

        errorAtCurrent(parser.current.start);
    }
}

/**
 * If current (next to be parsed) token is of the given type, advance the
 * parser, else report an error using the given message for details.
 */
static void consume(TokenType type, const char* message) {
    if (parser.current.type == type) {
        advance();
        return;
    }

    errorAtCurrent(message);
}

/**
 * Returns true and advances parser's current position if the current token has
 * the given type, else returns false.
 */
static bool match(TokenType type) {
    if (!check(type)) return false;
    advance();
    return true;
}

/**
 * Returns true if the current token has the given type.
 */
static bool check(TokenType type) {
    return parser.current.type == type;
}

/**
 * Reports a syntax error at the token previous to the one being parsed.
 */
static void error(const char* message) {
    errorAt(&parser.previous, message);
}

/**
 * Reports a syntax error using the token and message for details. Sets the
 * error condition flag and switches parser to panic mode.
 */
static void errorAt(Token* token, const char* message) {
    // enter panic mode on first syntax error and ignore all subsequent syntax
    // errors while in panic mode
    if (parser.panicMode) return;
    parser.panicMode = true;

    fprintf(stderr, "[line %d] Error", token->line);
    if (token->type == TOKEN_EOF) {
        fprintf(stderr, " at end");
    } else if (token->type == TOKEN_ERROR) {
        // nothing
    } else {
        fprintf(stderr, " at '%.*s'", token->length, token->start);
    }
    fprintf(stderr, ": %s\n", message);

    parser.hadError = true;
}

/**
 * Reports a syntax error at the token being parsed.
 */
static void errorAtCurrent(const char* message) {
    errorAt(&parser.current, message);
}

/**
 * Synchronizes the parser state. Skips tokens until a statement boundary and
 * exits panic mode so parser may continue (to report more syntax errors).
 */
static void synchronize() {
    parser.panicMode = false;

    while (parser.current.type != TOKEN_EOF) {
        if (parser.previous.type == TOKEN_SEMICOLON) return;
        switch (parser.current.type) {
        case TOKEN_CLASS:
        case TOKEN_FUN:
        case TOKEN_VAR:
        case TOKEN_FOR:
        case TOKEN_IF:
        case TOKEN_WHILE:
        case TOKEN_PRINT:
        case TOKEN_RETURN:
            return;
        default:; // do nothing
        }

        advance();
    }
}

/**
 * Appends the byte (instruction or operand) to the current bytecode chunk.
 */
static void emitByte(uint8_t byte) {
    writeChunk(currentChunk(), byte, parser.previous.line);
}

/**
 * Appends two bytes (e.g., instruction and its operand) to the current bytecode
 * chunk.
 */
static void emitBytes(uint8_t byte1, uint8_t byte2) {
    emitByte(byte1);
    emitByte(byte2);
}

/**
 * Emits bytecode for the jump instruction with placeholder for the jump
 * offset. Returns the offset for the emitted jump instruction bytecode from the
 * beginning.
 */
static int emitJump(uint8_t instruction) {
    emitByte(instruction);
    // jump offset is 2 bytes
    emitByte(0xff);
    emitByte(0xff);
    return currentChunk()->count - 2;
}

/**
 * Sets the operand of the jump instruction at offset from the beginning to the
 * number of bytes to skip to arrive after the currently last emitted
 * instruction.
 */
static void patchJump(int offset) {
    // some bytecode probably was added after the jump instruction at offset; -2
    // to adjust for the bytecode for the jump offset operand itself
    int jump = currentChunk()->count - offset - 2;

    if (jump > UINT16_MAX) {
        error("Too much code to jump over.");
    }

    // unpack the 2-byte integer jump into two separate bytes in the bytecode
    // sequence: higher byte of the jump becomes the first byte of offset, the
    // lower byte --- the second
    currentChunk()->code[offset] = (jump >> 8) & 0xff;
    currentChunk()->code[offset + 1] = jump & 0xff;
}

/**
 * Emits bytecode for jumping back to instruction at loopStart offset from the
 * beginning.
 */
static void emitLoop(int loopStart) {
    emitByte(OP_LOOP);

    int offset = currentChunk()->count - loopStart + 2;
    if (offset > UINT16_MAX) error("Loop body too large.");

    emitByte((offset >> 8) & 0xff);
    emitByte(offset & 0xff);
}

/**
 * Emits bytecode instruction to put a constant onto the evaluation stack.
 */
static void emitConstant(Value value) {
    emitBytes(OP_CONSTANT, makeConstant(value));
}

/**
 * Emits bytecode for a (global) variable definition.
 */
static void defineVariable(uint8_t global) {
    // local variables are in a static (lexical) scope, not dynamic (runtime)
    // scope, as global variables; locals are resolved at compile-time
    if (current->scopeDepth > 0) {
        markInitialized();
        return;
    }
    emitBytes(OP_DEFINE_GLOBAL, global);
}

/**
 * Emits bytecode instruction for return operation.
 */
static void emitReturn() {
    if (current->type == TYPE_INITIALIZER) {
        // instance initializer always returns the initialized instance
        emitBytes(OP_GET_LOCAL, 0);
    } else {
        // implicit return from a function returns nil
        emitByte(OP_NIL);
    }
    emitByte(OP_RETURN);
}

/**
 * Finishes the compilation process and restores the enclosing compiler as
 * current.
 */
static ObjFunction* endCompilation() {
    emitReturn();
    ObjFunction* function = current->function;

#ifdef DEBUG_PRINT_CODE
    if (!parser.hadError) {
        disassembleChunk(currentChunk(), function->name != NULL ?
                         function->name->chars : "<script>");
    }
#endif

    current = current->enclosing;
    return function;
}

/**
 * Returns a pointer to the bytecode chunk currently being compiled.
 */
static Chunk* currentChunk() {
    return &current->function->chunk;
}

/**
 * Dispatches parsing/bytecode-generating functions according to the parsing
 * rules table, using given precedence (of the expression being parsed) and
 * types of tokens encountered in the process.
 */
static void parsePrecedence(Precedence precedence) {
    // consumes the prefix operator
    advance();

    // looks for a prefix expression parser for the current (about to be parsed)
    // subexpression, using type of the previous token (part of operator or
    // operand); the first token is always going to belong to some kind of
    // prefix expression, by definition
    ParseFn prefixRule = getRule(parser.previous.type)->prefix;
    if (prefixRule == NULL) {
        error("Expect expression.");
        return;
    }
    // parses the prefix expression with the found parser, maybe consuming more
    // tokens in process
    bool canAssign = precedence <= PREC_ASSIGNMENT;
    prefixRule(canAssign);

    // looks for an infix expression parser for the current (about to be parsed)
    // subexpression; if next token (presumably infix operator), according to
    // rules table, has lower precedence than the given one or isn't an infix
    // operator at all, does nothing; otherwise
    while (precedence <= getRule(parser.current.type)->precedence) {
        // consumes the infix operator and
        advance();
        // parses the infix expression with the found parser, maybe consuming
        // more tokens in process
        ParseFn infixRule = getRule(parser.previous.type)->infix;
        infixRule(canAssign);
        // look for more infix operators with high enough precedence
    }

    if (canAssign && match(TOKEN_EQUAL)) {
        error("Invalid assignment target.");
    }

    // evaluating single expression (chapter Compiling Expressions)
    // "|" divides previous and current tokens
    // previous and current start as NULL
    // call stack is main() -> repl()/runFile() -> interpret() -> compile()

    // example: parsing -1
    // compile()>             -1    advances, calls expression()
    //  expression()>         |-1   calls parsePrecedence(PREC_ASSIGNMENT)
    //   parsePrecedence()>   |-1   advances, gets the rule for token "-", gets
    //                              prefix parser from the rule (unary()) and
    //                              calls it
    //    unary()>            -|1   calls parsePrecedence(PREC_UNARY)
    //     parsePrecedence()> -|1   advances, gets the rule for token "1", gets
    //                              prefix parser from the rule (number()) and
    //                              calls it
    //      number()>         -1|   calls strtod("1")
    //       strtod()>        -1|   converts "1" to double, emits constant 1.0
    //     parsePrecedence()> -1|   gets the rule for token EOF, its precedence
    //                              is PREC_NONE, what is lower than PREC_UNARY,
    //                              so it returns to unary()
    //    unary()>            -1|   switches on the type of token "-", emits
    //                              instruction OP_NEGATE
    //   parsePrecedence()>   -1|   gets the rule for token EOF, its precedence
    //                              is PREC_NONE, what is lower than PREC_UNARY,
    //                              so it returns to expression()
    //  expression()>         -1|   returns to compile()
    // compile()>             -1|   consumes EOF and ends compilation


    // example: parsing 1+2
    // compile()>              1+2  advances, calls expression()
    //  expression()>         |1+2  calls parsePrecedence(PREC_ASSIGNMENT)
    //   parsePrecedence()>   |1+2  advances, gets the rule for token "1", gets
    //                              prefix parser from the rule (number()) and
    //                              calls it
    //    number()>           1|+2  calls strtod("1")
    //     strtod()>          1|+2  converts "1" to double, emits constant 1.0
    //   parsePrecedence()>   1|+2  gets the rule for token "+", its precedence
    //                              is PREC_TERM, what is greater than
    //                              PREC_ASSIGNMENT, so it advances the
    //                              position and calls infix parser from the
    //                              rule (binary())
    //    binary()>           1+|2  gets the rule for token "+", calls
    //                              parsePrecedence() with the rule
    //                              precedence + 1 (PREC_TERM + 1)
    //     parsePrecedence()> 1+|2  advances, gets the rule for token "2", gets
    //                              prefix parser from the rule (number()) and
    //                              calls it
    //      number()>         1+2|  calls strtod("2")
    //       strtod()>        1+2|  converts "2" to double, emits constant 2.0
    //    binary()>           1+2|  switches on the type of token "+", emits
    //                              instruction OP_PLUS
    //   parsePrecedence()>   1+2|  gets the rule for token EOF, its
    //                              precedence is PREC_NONE, what is lower
    //                              than PREC_ASSIGNMENT, so it returns to
    //                              expression()
    //  expression()>         1+2|  returns to compile()
    // compile()>             1+2|  consumes EOF and ends compilation
}

/**
 * Returns a parse rule for the given token type.
 */
static ParseRule* getRule(TokenType type) {
    return &rules[type];
}

/**
 * Returns bytecode for a constant.
 */
static uint8_t makeConstant(Value value) {
    int constant = addConstant(currentChunk(), value);
    if (constant > UINT8_MAX) {
        error("Too many constants in one chunk.");
        return 0;
    }

    return (uint8_t)constant;
}

/**
 * Returns bytecode for a variable name constant.
 */
static uint8_t parseVariable(const char* errorMessage) {
    consume(TOKEN_IDENTIFIER, errorMessage);

    declareVariable();
    if (current->scopeDepth > 0) return 0;

    return identifierConstant(&parser.previous);
}

/**
 * Returns bytecode for an identifier constant.
 */
static uint8_t identifierConstant(Token* name) {
    return makeConstant(OBJ_VAL(copyString(name->start, name->length)));
}

/**
 * Initializes a compiler instance for a function of given type.
 */
static void initCompiler(Compiler* compiler, FunctionType type) {
    compiler->enclosing = current;

    compiler->function = NULL;
    compiler->type = type;
    compiler->localCount = 0;
    compiler->scopeDepth = 0;
    compiler->function = newFunction();
    current = compiler;
    // extract the function's name from previous token
    if (type != TYPE_SCRIPT) {
        current->function->name = copyString(parser.previous.start,
                                             parser.previous.length);
    }

    Local* local = &current->locals[current->localCount++];
    local->depth = 0;
    local->isCaptured = false;
    if (type != TYPE_FUNCTION) {
        local->name.start = "this";
        local->name.length = 4;
    } else {
        local->name.start = "";
        local->name.length = 0;
    }
}

/**
 * Introduces a new lexical scope and enters it.
 */
static void beginScope() {
    current->scopeDepth++;
}

/**
 * Exits the outermost lexical scope.
 */
static void endScope() {
    current->scopeDepth--;

    // pop the closing scope's locals from the stack
    while (current->localCount > 0 &&
           current->locals[current->localCount - 1].depth >
           current->scopeDepth) {
        if (current->locals[current->localCount - 1].isCaptured) {
            emitByte(OP_CLOSE_UPVALUE);
        } else {
            emitByte(OP_POP);
        }
        current->localCount--;
    }
}

/**
 * Declares a local variable.
 */
static void declareVariable() {
    // globals are defined differently
    if (current->scopeDepth == 0) return;

    Token* name = &parser.previous;
    // declaring several variables with the same name in the same scope is not
    // allowed (but is allowed in different scopes)
    for (int i = current->localCount - 1; i >= 0; i--) {
        Local* local = &current->locals[i];
        if (local->depth != -1 && local->depth < current->scopeDepth) {
            break;
        }

        if (identifiersEqual(name, &local->name)) {
            error("Already a variable with this name in this scope.");
        }
    }

    addLocal(*name);
}

/**
 * Defines a local variable in the current lexical scope.
 */
static void addLocal(Token name) {
    if (current->localCount == UINT8_COUNT) {
        error("Too many local variables in function.");
        return;
    }

    Local* local = &current->locals[current->localCount++];
    local->name = name;
    local->depth = -1;
    local->isCaptured = false;
}

/**
 * Returns an index of the last declared local with the given name in the
 * compiler's list of locals, or -1 if not found.
 */
static int resolveLocal(Compiler* compiler, Token* name) {
    for (int i = compiler->localCount - 1; i >= 0; i--) {
        Local* local = &compiler->locals[i];
        if (identifiersEqual(name, &local->name)) {
            if (local->depth == -1) {
                error("Can't read local variable in its own initializer.");
            }
            return i;
        }
    }

    return -1;
}

/**
 * Returns true if the two identifiers are the same.
 */
static bool identifiersEqual(Token* a, Token* b) {
    if (a->length != b->length) return false;
    return memcmp(a->start, b->start, a->length) == 0;
}

/**
 * Mark the last declared local as initialized.
 */
static void markInitialized() {
    if (current->scopeDepth == 0) return;
    current->locals[current->localCount - 1].depth =
        current->scopeDepth;
}

/**
 * Returns an upvalue index for the local/upvalue with given name in surrounding
 * functions of the given compiler's function, or -1 if not found.
 */
static int resolveUpvalue(Compiler* compiler, Token* name) {
    // top-level compiler does not have upvalues for certain
    if (compiler->enclosing == NULL) return -1;

    // name is a local in enclosing function?
    int local = resolveLocal(compiler->enclosing, name);
    if (local != -1) {
        compiler->enclosing->locals[local].isCaptured = true;
        return addUpvalue(compiler, (uint8_t)local, true);
    }

    // name is an upvalue in enclosing function? (recursive call)
    int upvalue = resolveUpvalue(compiler->enclosing, name);
    if (upvalue != -1) {
        return addUpvalue(compiler, (uint8_t)upvalue, false);
    }

    // neither local nor upvalue --- the name must be a global (or not even a
    // global, but it remains to be seen at runtime)
    return -1;
}

/**
 * Adds an upvalue for enclosing function's local with given index to the given
 * compiler's upvalues array. Returns index of the new upvalue in the upvalues
 * array.
 */
static int addUpvalue(Compiler* compiler, uint8_t index, bool isLocal) {
    int upvalueCount = compiler->function->upvalueCount;

    // do not duplicate upvalues even if they are referenced more than once
    for (int i = 0; i < upvalueCount; i++) {
        Upvalue* upvalue = &compiler->upvalues[i];
        if (upvalue->index == index && upvalue->isLocal == isLocal) {
            return i;
        }
    }

    if (upvalueCount == UINT8_COUNT) {
        error("Too many closure variables in function.");
        return 0;
    }

    compiler->upvalues[upvalueCount].isLocal = isLocal;
    compiler->upvalues[upvalueCount].index = index;
    return compiler->function->upvalueCount++;
}

/**
 * Creates a token from the arbitrary text.
 */
static Token syntheticToken(const char* text) {
    Token token;
    token.start = text;
    token.length = (int)strlen(text);
    return token;
}
