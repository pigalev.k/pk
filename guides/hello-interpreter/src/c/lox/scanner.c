#include <stdio.h>
#include <string.h>

#include "common.h"
#include "scanner.h"


typedef struct {
    const char* start;
    const char* current;
    int line;
} Scanner;

Scanner scanner;
static char peek();
static char peekNext();
static char advance();
static bool match(char expected);
static bool isAtEnd();
static bool isDigit(char c);
static bool isAlpha(char c);
static void skipWhitespaceAndComments();
static Token makeToken();
static Token errorToken();
static Token string();
static Token number();
static Token identifier();
static TokenType identifierType();
static TokenType checkKeyword(int start, int length,
                              const char* rest, TokenType type);


/**
 * Initializes the scanner.
 */
void initScanner(const char* source) {
    scanner.start = source;
    // the scanner's current character is the next to be read
    scanner.current = source;
    scanner.line = 1;
}

/**
 * Returns the next token from the scanner.
 */
Token scanToken() {
    skipWhitespaceAndComments();

    scanner.start = scanner.current;
    if (isAtEnd()) return makeToken(TOKEN_EOF);

    char c = advance();
    // to avoid adding a case for each digit and letter
    if (isAlpha(c)) return identifier();
    if (isDigit(c)) return number();

    switch (c) {
        // one character tokens
    case '(': return makeToken(TOKEN_LEFT_PAREN);
    case ')': return makeToken(TOKEN_RIGHT_PAREN);
    case '{': return makeToken(TOKEN_LEFT_BRACE);
    case '}': return makeToken(TOKEN_RIGHT_BRACE);
    case ';': return makeToken(TOKEN_SEMICOLON);
    case ',': return makeToken(TOKEN_COMMA);
    case '.': return makeToken(TOKEN_DOT);
    case '-': return makeToken(TOKEN_MINUS);
    case '+': return makeToken(TOKEN_PLUS);
    case '/': return makeToken(TOKEN_SLASH);
    case '*': return makeToken(TOKEN_STAR);
        // one or two character tokens
    case '!': return makeToken(match('=') ? TOKEN_BANG_EQUAL : TOKEN_BANG);
    case '=': return makeToken(match('=') ? TOKEN_EQUAL_EQUAL : TOKEN_EQUAL);
    case '<': return makeToken(match('=') ? TOKEN_LESS_EQUAL : TOKEN_LESS);
    case '>': return makeToken(match('=') ? TOKEN_GREATER_EQUAL : TOKEN_GREATER);
        // literals
    case '"': return string();

    }


    return errorToken("Unexpected character.");
}


/**
 * Returns the current (next to be read) character.
 */
static char peek() {
    return *scanner.current;
}

/**
 * Returns the next (next to the next to be read) character.
 */
static char peekNext() {
    if (isAtEnd()) return '\0';
    return scanner.current[1];
}

/**
 * Returns the current (next to be read) character and advances the current
 * position by one character.
 */
static char advance() {
    scanner.current++;
    return scanner.current[-1];
}

/**
 * Returns true and advances the current position if the current (next to be
 * read) character is the same as expected.
 */
static bool match(char expected) {
    if (isAtEnd()) return false;
    if (*scanner.current != expected) return false;
    scanner.current++;
    return true;
}

/**
 * Returns true if the end of the source string is reached.
 */
static bool isAtEnd() {
    return *scanner.current == '\0';
}

/**
 * Returns true if c is a decimal digit.
 */
static bool isDigit(char c) {
    return c >= '0' && c <= '9';
}

/**
 * Returns true if c is an ASCII letter or underscore.
 */
static bool isAlpha(char c) {
    return (c >= 'a' && c <= 'z') ||
        (c >= 'A' && c <= 'Z') ||
        c == '_';
}

/**
 * Skips whitespace characters and line comments, advancing the current
 * position.
 */
static void skipWhitespaceAndComments() {
    for (;;) {
        char c = peek();
        switch (c) {
        case ' ':
        case '\r':
        case '\t':
            advance();
            break;
        case '\n':
            scanner.line++;
            advance();
            break;
        case '/':
            if (peekNext() == '/') {
                while (peek() != '\n' && !isAtEnd()) advance();
            } else {
                return;
            }
            break;
        default:
            return;
        }
    }
}

/**
 * Returns a new token of the given type.
 */
static Token makeToken(TokenType type) {
    Token token;

    token.type = type;
    token.start = scanner.start;
    token.length = (int)(scanner.current - scanner.start);
    token.line = scanner.line;

    return token;
}

/**
 * Returns a new token representing a lexical error.
 */
static Token errorToken(const char* message) {
    Token token;

    token.type = TOKEN_ERROR;
    token.start = message;
    token.length = (int)strlen(message);
    token.line = scanner.line;

    return token;
}

/**
 * Reads and returns a string literal token.
 */
static Token string() {
    char c;

    while ((c = peek()) != '"' && !isAtEnd()) {
        if (c == '\n') scanner.line++;
        advance();
    }

    if (isAtEnd()) return errorToken("Unterminated string.");

    // skip the closing quote
    advance();

    return makeToken(TOKEN_STRING);
}

/**
 * Reads and returns a numeric literal token.
 */
static Token number() {
    // integral part
    while (isDigit(peek())) advance();

    // fractional part
    if (peek() == '.' && isDigit(peekNext())) {
        // consume the dot
        advance();
        while (isDigit(peek())) advance();
    }

    return makeToken(TOKEN_NUMBER);
}

/**
 * Reads and returns an identifier token.
 */
static Token identifier() {
    char c;

    while ((c = isAlpha(peek())) || isDigit(c)) advance();
    return makeToken(identifierType());
}

/**
 * Returns the type of the just read identifier token.
 */
static TokenType identifierType() {
    switch (scanner.start[0]) {
    case 'a': return checkKeyword(1, 2, "nd", TOKEN_AND);
    case 'c': return checkKeyword(1, 4, "lass", TOKEN_CLASS);
    case 'e': return checkKeyword(1, 3, "lse", TOKEN_ELSE);
    case 'f':
        if (scanner.current - scanner.start > 1) {
            switch(scanner.start[1]) {
            case 'a': return checkKeyword(2, 3, "lse", TOKEN_FALSE);
            case 'o': return checkKeyword(2, 1, "r", TOKEN_FOR);
            case 'u': return checkKeyword(2, 1, "n", TOKEN_FUN);
            }
        }
        break;
    case 'i': return checkKeyword(1, 1, "f", TOKEN_IF);
    case 'n': return checkKeyword(1, 2, "il", TOKEN_NIL);
    case 'o': return checkKeyword(1, 1, "r", TOKEN_OR);
    case 'p': return checkKeyword(1, 4, "rint", TOKEN_PRINT);
    case 'r': return checkKeyword(1, 5, "eturn", TOKEN_RETURN);
    case 's': return checkKeyword(1, 4, "uper", TOKEN_SUPER);
    case 't':
        if (scanner.current - scanner.start > 1) {
            switch (scanner.start[1]) {
            case 'h': return checkKeyword(2, 2, "is", TOKEN_THIS);
            case 'r': return checkKeyword(2, 2, "ue", TOKEN_TRUE);
            }
        }
        break;
    case 'v': return checkKeyword(1, 2, "ar", TOKEN_VAR);
    case 'w': return checkKeyword(1, 4, "hile", TOKEN_WHILE);
    }

    return TOKEN_IDENTIFIER;
}

/**
 * Returns the given token type if the identifier just read is a keyword (Lox
 * reserved word) of this type, or a generic identifier type, if it is not.
 */
static TokenType checkKeyword(int start, int length,
                              const char* rest, TokenType type) {
    if (scanner.current - scanner.start == start + length &&
        memcmp(scanner.start + start, rest, length) == 0) {
        return type;
    }

    return TOKEN_IDENTIFIER;
}
