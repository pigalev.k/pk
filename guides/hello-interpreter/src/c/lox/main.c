#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "chunk.h"
#include "debug.h"
#include "vm.h"
#include "io.h"


static void repl();
static void runFile(const char* script);


/**
 * An entry point for Lox compiler and bytecode interpreter (VM).
 */
int main(int argc, const char* argv[]) {

    // initialize VM
    initVM();

    if (argc == 1) repl();
    else if (argc == 2) runFile(argv[1]);
    else {
        fprintf(stderr, "Usage: %s [script]\n", argv[0]);
        exit(64);
    }

    // close the VM, free resources
    freeVM();

    return 0;
}

/**
 * Starts a Lox interactive session (REPL).
 */
static void repl() {
    char line[1024];

    for (;;) {
        printf("> ");

        if (!fgets(line, sizeof line, stdin)) {
            printf("\n");
            break;
        }

        interpret(line);
    }
}

/**
 * Loads and runs a Lox script.
 */
static void runFile(const char* script) {
    char* source = readFile(script);
    InterpretResult result = interpret(source);
    free(source);

    if (result == INTERPRET_COMPILE_ERROR) exit(65);
    if (result == INTERPRET_RUNTIME_ERROR) exit(70);
}
