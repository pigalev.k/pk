(ns lox.grammars
  (:require
   [clojure.spec.alpha :as s]))


;; lexical grammar


;; syntactical grammar

(s/def ::number number?)
(s/def ::string string?)
(s/def ::literal (s/or :number ::number
                       :string ::string
                       :other #{"true" "false" "nil"}))
(s/def ::operator #{"==" "!=" "<" "<=" ">" ">=" "+" "-" "*" "/"})
(s/def ::grouping (s/cat :left-paren #{"("}
                         :expression ::expression
                         :right-paren #{")"}))
(s/def ::unary (s/cat :prefix-operator (s/or :minus #{"-"}
                                             :bang #{"!"})
                      :expression ::expression))
(s/def ::binary (s/cat :left-expression ::expression
                       :infix-operator ::operator
                       :right-expression ::expression))
(s/def ::expression (s/or :literal ::literal
                          :unary ::unary
                          :binary ::binary
                          :grouping ::grouping) )


(comment

  (->> (s/exercise ::expression 1)
       ffirst)

)
