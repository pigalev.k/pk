package lox;

import java.util.ArrayList;
import java.util. HashMap;
import java.util.List;
import java.util.Map;


// lexical grammar

// NUMBER     -> DIGIT+ ("." DIGIT+)?;
// STRING     -> "\"" <any char except "\"">* "\"";
// IDENTIFIER -> ALPHA (ALPHA | DIGIT)*;
// ALPHA      -> "a" ... "z" | "A" ... "Z" | "_";
// DIGIT      -> "0" ... "9";


/**
 * Creating a sequence of tokens from the source string.
 */
public class Scanner {
    private final String source;
    private final List<Token> tokens = new ArrayList<>();
    private int start = 0;
    private int current = 0;
    private int line = 1;

    private static final Map<String, TokenType> keywords;

    static {
        keywords = new HashMap<>();
        keywords.put("and", TokenType.AND);
        keywords.put("class", TokenType.CLASS);
        keywords.put("else", TokenType.ELSE);
        keywords.put("false", TokenType.FALSE);
        keywords.put("for", TokenType.FOR);
        keywords.put("fun", TokenType.FUN);
        keywords.put("if", TokenType.IF);
        keywords.put("nil", TokenType.NIL);
        keywords.put("or", TokenType.OR);
        keywords.put("print", TokenType.PRINT);
        keywords.put("return", TokenType.RETURN);
        keywords.put("super", TokenType.SUPER);
        keywords.put("this", TokenType.THIS);
        keywords.put("true", TokenType.TRUE);
        keywords.put("var", TokenType.VAR);
        keywords.put("while", TokenType.WHILE);
    }

    public Scanner(String source) {
        this.source = source;
    }


    // public interface

    /**
     * Scans the entire source, creates and returns a sequence of tokens.
     */
    public List<Token> scanTokens() {
        while (!isAtEnd()) {
            // we are at the beginning of the next lexeme, reset the start
            // position
            start = current;
            scanToken();
        }

        // finally add the "end of file" token
        tokens.add(new Token(TokenType.EOF, "", null, line));
        return tokens;
    }


    // token construction mechanism

    /**
     * Consumes the next lexeme from the source and creates the corresponding
     * token. Lexeme can consist of one or more characters.
     */
    private void scanToken() {
        char c = advance();
        switch (c) {
            // single character tokens
        case '(': addToken(TokenType.LEFT_PAREN); break;
        case ')': addToken(TokenType.RIGHT_PAREN); break;
        case '{': addToken(TokenType.LEFT_BRACE); break;
        case '}': addToken(TokenType.RIGHT_BRACE); break;
        case ',': addToken(TokenType.COMMA); break;
        case '.': addToken(TokenType.DOT); break;
        case '-': addToken(TokenType.MINUS); break;
        case '+': addToken(TokenType.PLUS); break;
        case ';': addToken(TokenType.SEMICOLON); break;
        case '*': addToken(TokenType.STAR); break;
            // two character tokens
        case '!':
            addToken(match('=') ?
                     TokenType.BANG_EQUAL : TokenType.BANG);
            break;
        case '=':
            addToken(match('=') ?
                     TokenType.EQUAL_EQUAL : TokenType.EQUAL);
            break;
        case '<':
            addToken(match('=') ?
                     TokenType.LESS_EQUAL : TokenType.LESS);
            break;
        case '>':
            addToken(match('=') ?
                     TokenType.GREATER_EQUAL : TokenType.GREATER);
            break;
            // longer lexemes
        case '/':
            if (match('/')) {
                // a comment goes until the end of the line
                while (peek() != '\n' && !isAtEnd()) advance();
            } else {
                addToken(TokenType.SLASH);
            }
            break;
            // whitespace
        case ' ':
        case '\r':
        case '\t':
            break;
            // line breaks
        case '\n':
            line++;
            break;
            // string literals
        case '"': string(); break;
        default:
            if (isDigit(c)) {
                number();
            } else if (isAlpha(c)) {
                identifier();
            } else {
                Lox.error(line, "Unexpected character.");
            }
            break;
        }
    }


    // token constructors

    /**
     * Adds a token for the number literal. A number literal is a sequence of
     * digits optionally followed by a dot and one or more trailing
     * digits. Integer and floating-point literals are supported. Leading or
     * trailing decimal point is not supported.
     */
    private void number() {
        // advance while there are digits
        while (isDigit(peek())) advance();

        // if decimal dot is found, advance past it and any trailing digits
        if (peek() == '.' && isDigit(peekNext())) {
            advance();
            while (isDigit(peek())) advance();
        }

        addToken(TokenType.NUMBER,
                 Double.parseDouble(source.substring(start, current)));
    }

    /**
     * Adds a token for the string literal. A string literal is a sequence of
     * characters delimited by double quotes. Can contain newlines (multiline
     * string). Does not support escape sequences (yet).
     */
    private void string() {
        // advance till double quote if found or end of source is reached,
        // incrementing line counter on newlines
        while (peek() != '"' && !isAtEnd()) {
            if (peek() == '\n') line++;
            advance();
        }

        // reached the end of source without encountering closing double quote?
        // string never ended, report an error
        if (isAtEnd()) {
            Lox.error(line, "Unterminated string.");
            return;
        }

        // closing double quote found? advance over it
        advance();

        // double quotes are not part of a string literal, strip em;
        // if escape sequences were supported, they would be unescaped here
        String value = source.substring(start + 1, current - 1);

        addToken(TokenType.STRING, value);
    }

    /**
     * Adds a token for an identifier. An identifier literal is a sequence of
     * characters that starts with an alphabetic character or underscore
     * optionally followed by alphanumeric characters or underscores.
     */
    private void identifier() {
        // advance till any character unsuitable for identifier
        while (isAlphaNumeric(peek())) advance();

        // extract the identifier
        String text = source.substring(start, current);

        // see if identifier is a reserved word, and set the appropriate token
        // type
        TokenType type = keywords.get(text);
        if (type == null) type = TokenType.IDENTIFIER;

        addToken(type);
    }


    // helper methods

    /**
     * Returns true if the current character matches the expected. If so, also
     * advances the current position.
     */
    private boolean match(char expected) {
        if (isAtEnd()) return false;
        if (source.charAt(current) != expected) return false;

        current++;
        return true;
    }

    /**
     * Returns the current character in the source. Does not advance the current
     * position. If end of source has been reached, returns '\0'.
     */
    private char peek() {
        if (isAtEnd()) return '\0';
        return source.charAt(current);
    }

    /**
     * Returns the next character in the source. Does not advance the current
     * position. If end of source has been reached, returns '\0'.
     */
    private char peekNext() {
        if (current + 1 >= source.length()) return '\0';
        return source.charAt(current + 1);
    }


    /**
     * Returns the current character in the source. Advances the current
     * position.
     */
    private char advance() {
        return source.charAt(current++);
    }

    /**
     * Returns true if end of source has been reached.
     */
    private boolean isAtEnd() {
        return current >= source.length();
    }

    /**
     * Returns true if c is a digit.
     */
    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    /**
     * Returns true if c is an alphabetic character or underscore.
     */
    private boolean isAlpha(char c) {
        return (c >= 'a' && c <= 'z') ||
            (c >= 'A' && c <= 'Z') ||
            c == '_';
    }

    /**
     * Returns true if c is an alphanumeric character or underscore.
     */
    private boolean isAlphaNumeric(char c) {
        return isAlpha(c) || isDigit(c);
    }

    /**
     * Adds a token to the sequence of tokens. Uses null for literal.
     */
    private void addToken(TokenType type) {
        addToken(type, null);
    }

    /**
     * Adds a token to the sequence of tokens. Uses given literal.
     */
    private void addToken(TokenType type, Object literal) {
        String text = source.substring(start, current);
        tokens.add(new Token(type, text, literal, line));
    }
}
