package lox;

import java.util.List;
import java.util.Map;


/**
 * Representation of a Lox class --- an associative data structure with methods
 * operating on it.
 */
class LoxClass implements LoxCallable {
    final String name;
    final LoxClass superclass;
    private final Map<String, LoxFunction> methods;


    LoxClass(String name,
             LoxClass superclass,
             Map<String, LoxFunction> methods) {
        this.name = name;
        this.superclass = superclass;
        this.methods = methods;
    }


    /**
     * Returns the method with the given name, or null if not found.
     */
    LoxFunction findMethod(String name) {
        if (methods.containsKey(name)) {
            return methods.get(name);
        }

        if (superclass != null) {
            return superclass.findMethod(name);
        }

        return null;
    }


    @Override
    public Object call(Interpreter interpreter,
                       List<Object> arguments) {
        // create an instance
        LoxInstance instance = new LoxInstance(this);

        // call the initializer (constructor), if present
        LoxFunction initializer = findMethod("init");
        if (initializer != null) {
            initializer.bind(instance).call(interpreter, arguments);
        }

        return instance;
    }

    @Override
    public int arity() {
        LoxFunction initializer = findMethod("init");
        if (initializer == null) return 0;
        return initializer.arity();
    }

    @Override
    public String toString() {
        return name;
    }
}
