package lox;


/**
 * An exception to be thrown at runtime (syntax tree interpretation) phase.
 */
class RuntimeError extends RuntimeException {
    final Token token;

    RuntimeError(Token token, String message) {
        super(message);
        this.token = token;
    }
}
