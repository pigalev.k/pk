package lox;

import java.util.HashMap;
import java.util.Map;


/**
 * Holds the variable bindings.
 */
class Environment {
    final Environment enclosing;
    private final Map<String, Object> values = new HashMap<>();


    Environment() {
        enclosing = null;
    }

    Environment(Environment enclosing) {
        this.enclosing = enclosing;
    }


    // public interface

    /**
     * Binds (or rebinds) the value to the name.
     */
    public void define(String name, Object value) {
        values.put(name, value);
    }

    /**
     * Returns the value bound to the name. Throws if the name is
     * unbound.
     */
    public Object get(Token name) {
        if (values.containsKey(name.lexeme)) {
            return values.get(name.lexeme);
        }

        if (enclosing != null) return enclosing.get(name);

        throw new RuntimeError(name,
                               "Undefined variable '" + name.lexeme + "'.");
    }

    /**
     * Assigns the value to the name. Throws if the name is unbound.
     */
    public void assign(Token name, Object value) {
        if (values.containsKey(name.lexeme)) {
            values.put(name.lexeme, value);
            return;
        }

        if (enclosing != null) {
            enclosing.assign(name, value);
            return;
        }

        throw new RuntimeError(name,
                               "Undefined variable '" + name.lexeme + "'.");
    }

    /**
     * Returns the value for the name, retrieving it from ancestor environment
     * located up the environment chain at the distance.
     */
    public Object getAt(int distance, String name) {
        return ancestor(distance).values.get(name);
    }

    /**
     * Assigns the value for the name, placing it in ancestor environment
     * located up the environment chain at the distance.
     */
    void assignAt(int distance, Token name, Object value) {
        ancestor(distance).values.put(name.lexeme, value);
    }


    // helper methods

    /**
     * Returns the ancestor environment located up the environment chain at the
     * distance.
     */
    private Environment ancestor(int distance) {
        Environment environment = this;
        for (int i = 0; i < distance; i++) {
            environment = environment.enclosing;
        }

        return environment;
    }
}
