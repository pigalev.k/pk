package lox;

import java.util.HashMap;
import java.util.Map;


/**
 * Representation of a Lox object --- an instance of a Lox class.
 */
class LoxInstance {
    private LoxClass klass;
    private final Map<String, Object> fields = new HashMap<>();

    LoxInstance(LoxClass klass) {
        this.klass = klass;
    }

    /**
     * Gets the value of the field.
     */
    Object get(Token name) {
        if (fields.containsKey(name.lexeme)) {
            return fields.get(name.lexeme);
        }

        LoxFunction method = klass.findMethod(name.lexeme);
        if (method != null) return method.bind(this);

        throw new RuntimeError(name,
                               "Undefined property '" + name.lexeme + "'.");
    }

    /**
     * Sets the value of the field.
     */
    void set(Token name, Object value) {
        fields.put(name.lexeme, value);
    }


    @Override
    public String toString() {
        return klass.name + " instance";
    }
}
