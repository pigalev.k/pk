# hello-interpreter

Exploring creation of interpreters and compilers for programming languages.

Sources:
- Crafting Interpreters by Robert Nystrom
  - https://craftinginterpreters.com/the-lox-language.html
  - https://github.com/munificent/craftinginterpreters

## Getting started

### Clojure

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.

### Java

Use `maven` to run tasks on Java sources, e.g.

```
mvn compile
mvn package
mvn exec:java
```

### C

Use `make` to run tasks on C sources; executable will be located in
`target/clox`.

```
make
make clean
```

To enable debugging output

```
DEBUG=true make
```

To explore the call graph

```
# generate a callgrind.out.<PID> file
valgrind --tool=callgrind target/clox [script]

# open a GUI tool to visualize callgrind data
kcachegrind callgrind.out.<PID>
```
