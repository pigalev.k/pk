# hello-hiccup

Exploring Hiccup --- fast library for rendering HTML in Clojure.

- https://github.com/weavejester/hiccup

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
