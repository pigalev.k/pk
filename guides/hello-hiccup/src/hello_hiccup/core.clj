(ns hello-hiccup.core
  (:require
   [hiccup.core :as h]
   [hiccup.def :as d]
   [hiccup.element :as e]
   [hiccup.form :as f]
   [hiccup.page :as p]))




(comment

  ;; `hiccup.core`: render a tree of vectors into a string of HTML

  (h/h [:div "alert(1);"])
  (h/html [:span {:class "foo"} "bar"])
  (h/html [:div#foo.bar.baz "bang"])
  (h/html [:ul
           (for [x (range 1 4)]
             [:li x])])


  ;; `hiccup.def`: macros for defining functions that generate HTML

  (d/defhtml foo
    [a-name]
    [:div (str "Hello " a-name "!")])

  (foo "anonymous")


  ;; `hiccup.element`: functions for creating HTML elements

  (h/html
   (e/image {:id "i1"} "http://pic.me/123456")
   (e/javascript-tag "var a = 1; alert(a);")
   (e/link-to {:id "l1"} "https://some.site" "Go ahead")
   (e/ordered-list {:id "ol1"} [1 2 3 4 5]))


  ;; `form`: functions for generating HTML forms and input fields

  (h/html
   (f/form-to [:post "/submit"]
              [:fieldset.mds-border {:style "padding-top: 20px"}
               (f/check-box {:id "cb1"} "a-checkbox" false false)
               (f/text-field {:id "tf1" :class "active"} "a-text-field" "Foo")
               (f/submit-button {:id "sb1" :style {:color "red"}} "Go!")]))


  ;; `page`: functions for setting up HTML pages

  (h/html
   (p/html5
    [:body
     [:div#app.bar.baz "bang"]]))

)
