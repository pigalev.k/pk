# hello-cljs-ajax

Exploring cljs-ajax --- simple asynchronous AJAX client for ClojureScript and
Clojure .

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
