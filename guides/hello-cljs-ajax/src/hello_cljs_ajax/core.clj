(ns hello-cljs-ajax.core
  (:require
   [ajax.core :as ajax]
   [portal.api :as pa]))


;; portal

(comment

  ;; open a new inspector
  (def p (pa/open))
  ;; add portal as a tap> target
  (add-tap #'pa/submit)
  ;; start tapping out values
  (tap> :hello)
  ;; bring selected value back into repl
  (prn @p)
  ;; remove portal from tap> targetset
  (remove-tap #'pa/submit)
  ;; close the inspector when done
  (pa/close)

)


;; requires `url-shortener-api` service

(comment

  (def url-shortener-api-url "http://localhost:7777/api/v1")
  (def get-all-aliases-url (str url-shortener-api-url "/alias"))
  (def create-alias-url get-all-aliases-url)
  (defn get-alias-url
    [short-string]
    (str url-shortener-api-url "/alias/" short-string))
  (def delete-alias-url get-alias-url)

  (def on-success tap>)
  (def on-failure tap>)


  (ajax/GET get-all-aliases-url)
  (ajax/GET get-all-aliases-url {:params {:foo "foo"}})
  (ajax/GET get-all-aliases-url {:handler on-success
                                 :error-handler on-failure
                                 :response-format :json
                                 :keywords? true})

  (ajax/GET (get-alias-url "ywuu") {:handler on-success
                                    :error-handler on-failure})

  (ajax/POST create-alias-url {:format :json
                               :params {:original "http://do.ya.fine"}
                               :handler on-success
                               :error-handler on-failure})

  (ajax/DELETE (delete-alias-url "ywuu") {:handler on-success
                                          :error-handler on-failure})

)
