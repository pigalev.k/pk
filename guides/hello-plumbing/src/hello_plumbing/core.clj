(ns hello-plumbing.core
  (:require
   [plumbing.core :as pc :refer [sum fnk defnk for-map]]
   [plumbing.graph :as graph]
   [plumbing.fnk.pfnk :as pfnk]
   [schema.core :as s]))


;; basics: defining and modifying computational graphs

(comment

  ;; univariate statistics: a plain function
  (defn stats
    "Take a map {:xs xs} and return a map of simple statistics on xs"
    [{:keys [xs] :as m}]
    (assert (contains? m :xs))
    (let [n  (count xs)
          m  (/ (pc/sum identity xs) n)
          m2 (/ (pc/sum #(* % %) xs) n)
          v  (- m2 (* m m))]
      {:n n   ; count
       :m m   ; mean
       :m2 m2 ; mean-square
       :v v   ; variance
       }))

  ;; univariate statistics: a computational graph
  (def stats-graph
    "A graph specifying the same computation as 'stats'"
    {:n  (pc/fnk [xs]   (count xs))
     :m  (pc/fnk [xs n] (/ (pc/sum identity xs) n))
     :m2 (pc/fnk [xs n] (/ (pc/sum #(* % %) xs) n))
     :v  (pc/fnk [m m2] (- m2 (* m m)))})

  ;; compile the graph to obtain a usable function
  (def stats-eager (graph/compile stats-graph))

  ;; and use the function
  (= {:n 4
      :m 3
      :m2 (/ 25 2)
      :v (/ 7 2)}
     (into {} (stats-eager {:xs [1 2 3 6]})))

  (stats-eager {:ys [1 2 3]})


  ;; we can modify and extend stats-graph using ordinary operations on maps
  (def extended-stats
    (graph/compile
     (assoc stats-graph
            :sd (fnk [^double v] (Math/sqrt v)))))

  (= {:n 4
      :m 3
      :m2 (/ 25 2)
      :v (/ 7 2)
      :sd (Math/sqrt 3.5)}
     (into {} (extended-stats {:xs [1 2 3 6]})))
)


;; using computational graphs

(comment

  ;; a graph encodes the structure of a computation, but not how it happens,
  ;; allowing for many execution strategies. For example, we can compile a graph
  ;; lazily so that step values are computed as needed.
  (def lazy-stats (graph/lazy-compile stats-graph))

  ;; nothing has actually been computed yet
  (def output (lazy-stats {:xs [1 2 3 6]}))

  ;; now `:n` and `:m2` have been computed, but `:v` and `:m` are still behind a
  ;; `delay`
  (= (/ 25 2) (:m2 output))


  ;; or, we can parallel-compile the graph so that independent step functions
  ;; are run in separate threads
  (def par-stats (graph/par-compile stats-graph))

  ;; nodes are being computed in futures, with `:m` and `:m2` going in parallel
  ;; after `:n`
  (def output (par-stats {:xs [1 2 3 6]}))

  (= (/ 7 2) (:v output))


  ;; we can also ask a graph for information about its inputs and
  ;; outputs (automatically computed from its definition)

  ;; stats-graph takes a map with one required key, `:xs`
  (= {:xs s/Any}
     (pfnk/input-schema stats-graph))

  ;; stats-graph outputs a map with four keys, `:n`, `:m`, `:m2`, and `:v`
  (= {:n s/Any :m s/Any :m2 s/Any :v s/Any}
     (pfnk/output-schema stats-graph))

  ;; we can also have higher-order functions on graphs to wrap the behavior on
  ;; each step. For instance, we can automatically profile each sub-function in
  ;; 'stats' to see how long it takes to execute

  (def profiled-stats (graph/compile (graph/profiled :profile-data stats-graph)))

  ;; times in milliseconds for each step
  (= {:n 1.001, :m 0.728, :m2 0.996, :v 0.069}
     @(:profile-data (profiled-stats {:xs (range 10000)})))
)


;; defining keyword functions with `defnk`, using fnk-style destructuring

(comment

  ;; many of the functions take a single (nested) map argument with keyword keys
  ;; and have expectations about which keys must be present and which are
  ;; optional. 'Keyword functions' (defined by defnk) use a new style of
  ;; destructuring binding for just that kind of input.

  (defnk simple-fnk [a b c]
    (+ a b c))

  (= 6 (simple-fnk {:a 1 :b 2 :c 3}))
  (simple-fnk {:a 1 :b 2})

  ;; optional kays amnd defaults
  (defnk simple-opt-fnk [a b {c 1}]
    (+ a b c))

  (= 4 (simple-opt-fnk {:a 1 :b 2}))

  ;; nested map bindings
  (defnk simple-nested-fnk [a [:b b1] c]
    (+ a b1 c))

  (= 6 (simple-nested-fnk {:a 1 :b {:b1 2} :c 3}))
  (simple-nested-fnk {:a 1 :b 1 :c 3})

  ;; multiple variables and multiple levels of nesting
  (defnk simple-nested-fnk2 [a [:b b1 [:c {d 3}]]]
    (+ a b1 d))

  (= 4 (simple-nested-fnk2 {:a 1 :b {:b1 2 :c {:d 1}}}))
  (= 5 (simple-nested-fnk2 {:a 1 :b {:b1 1 :c {}}}))

)


;; more good stuff

(comment

  ;; `for` but for maps
  (for-map [i (range 3)
            j (range 3)
	        :let [s (+ i j)]
			:when (< s 3)]
	       [i j] s)


  ;; `safe-get` is like `get` but throws when the key doesn't exist
  (pc/safe-get {:a 1 :b 2} :c)

  ;; map over map values
  (pc/map-vals inc {:a 0 :b 0})

  ;; ever wanted to conditionally do steps in a `->>` or `->`? Now you can with
  ;; our 'penguin' operators. Here's a few examples:

  (let [add-b? false]
    (-> {:a 1}
        (merge {:c 2})
        (pc/?> add-b? (assoc :b 2))))


  (let [inc-all? true]
    (->> (range 10)
         (filter even?)
         (pc/?>> inc-all? (map inc))))

)
