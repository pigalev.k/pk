# hello-plumbing

Exploring Plumbing --- Prismatic's Clojure(Script) utility belt.

- https://github.com/plumatic/plumbing

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
