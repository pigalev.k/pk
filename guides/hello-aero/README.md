# hello-aero

Exploring Aero --- a small library for explicit, intentful configuration.

- https://github.com/juxt/aero

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
