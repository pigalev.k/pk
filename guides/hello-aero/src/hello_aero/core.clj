(ns hello-aero.core
  (:require
   [aero.core :as a]
   [clojure.java.io :as io]))


;; parsing configs

(a/read-config (io/resource "config.aero.edn"))

(a/read-config (io/resource "config.aero.edn") {:profile  :prod
                                                :hostname "neon"
                                                :user     :nervous
                                                :resolver a/resource-resolver})


;; defining custom tag literals

(defmethod a/reader 'mytag
 [{:keys [profile] :as opts} tag value]
  (if (= value :favourite)
     :chocolate
     :vanilla))

(comment

  (System/getenv)

)
