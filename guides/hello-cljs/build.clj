(require '[cljs.build.api :as b])

(b/build "." {:main          'hello-cljs.libs
              :output-to     "out/main-prod.js"
              :output-dir    "out"
              :source-map    "out/main-prod.js.map"
              :verbose       true
              :optimizations :advanced})
