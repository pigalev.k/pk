define(function() {
    var hello = {};

    hello.version = 1;

    hello.hello = function(name) {
      return 'Hello, ' + name + ', from AMD lib!';
    };

    return hello;
});
