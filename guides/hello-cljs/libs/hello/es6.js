const hello = (name) => 'Hello, ' + name + ', from ES6 lib!';

export {
  hello,
};
