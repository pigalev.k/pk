var hello = function(name) {
  return 'Hello, ' + name + ', from CommonJS lib!';
};

module.exports = {
  hello: hello
};
