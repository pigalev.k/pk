// See https://developers.google.com/closure/compiler/docs/externs-and-exports

// The `@externs` annotation is the best way to indicate a file contains externs.

/**
 * @fileoverview Public API of 'goog/hello.js'.
 * @externs
 */

var googHello = function(name) {}
