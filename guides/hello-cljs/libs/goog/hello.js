goog.provide("goog.hello");

var hello = function (name) {
  return 'Hello, ' + name + ', from GCL-compatible lib!';
};

// Google Clojure Compiler will not touch a function name if it is called
// and externs will not be necessary
// console.log(hello('cljs'));

goog.hello.hello = hello;
