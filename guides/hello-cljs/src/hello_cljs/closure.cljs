(ns hello-cljs.closure
  "Using Google Closure Library."
  (:require
   [goog.dom :as dom])
  (:import (goog.math Vec3)
           (goog.string StringBuffer)
           (goog userAgent)))


;; http://funcool.github.io/clojurescript-unraveled/#the-closure-library
;; https://google.github.io/closure-library/api/

(enable-console-print!)
(println "Hello GCL!")

(defn log
  [x]
  (js/console.log x))


(comment

  ;; some modules are like namespaces containing symbols that can be just
  ;; required and used

  (def element (dom/getElement "app"))
  (log element)


  ;; some modules are like classes that should be imported and instantiated
  ;; before use

  (def v (Vec3. 1 2 3))
  (log v)

  (def s (StringBuffer.))
  (.append s "We all say ")
  (.append s "HAYO!")
  (log (.toString s))

  ;; some are weird
  (def ua userAgent)
  (log ua)
  (log (.getNavigator ua))
  (log (.getUserAgentString ua))

  (take 3 (into (sorted-map) (seq (js->clj ua :keywordize-keys true))))

)
