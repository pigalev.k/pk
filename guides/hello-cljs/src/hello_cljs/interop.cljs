(ns hello-cljs.interop
  "Using JS interop facilities.")


;; http://funcool.github.io/clojurescript-unraveled/
;; https://cljs.info/cheatsheet/
;; https://lwhorton.github.io/2018/10/20/clojurescript-interop-with-javascript.html

(enable-console-print!)
(println "Hello interop!")


;; JS interop

(comment

  ;; objects

  ;; create object
  (def o #js {:id 1 :name "Ololosha" :foo (fn [x] x)})
  (def o1 (js-obj :id 1 :name "Ololosha"))
  (object? o)

  ;; call "method" (function member)
  (.foo o 12)

  ;; get property
  (.-id o)
  (. o -id)

  ;; set property
  (set! (.-id o) 2)
  (set! (.-updatedAt o) (js/Date.))

  ;; delete property
  (js-delete o "updated_at")

  ;; convert to cljs and back to js
  (js->clj o)
  (clj->js (js->clj o))
  (js->clj o :keywordize-keys true)


  ;; arrays

  ;; create array
  (def a #js [1 2 3 "foo"])
  (def a1 (array 1 2 3 "foo"))
  (def a2 (make-array 3))
  (def a3 (aclone a))
  (array? a)

  ;; get element
  (aget a 3)
  (aget a 4)

  ;; set element
  (aset a 0 -1)

  ;; delete element at index (set value to nil)
  (js-delete a 1)

  ;; array methods
  (.push a 12)
  (.pop a)
  (.unshift a -1)
  (.shift a)
  ;; remove element(s) at index, shifting other indices
  (.splice a 1 1)


  ;; platform APIs

  ;; browser
  (js/alert "Hello, JS!")
  (js/console.log o)
  (js/console.log
   (.querySelector js/document "#app"))

)
