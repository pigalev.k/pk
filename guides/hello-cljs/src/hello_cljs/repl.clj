(ns hello-cljs.repl
  (:require
   [cljs.repl :as repl]
   [cljs.repl.browser :as browser]
   [cljs.repl.node :as node]))


;; Starting a cljs REPL from clj REPL

;; https://clojurescript.org/reference/repl


;; browser

(comment

  ;; create a browser evaluation environment

  (def env (browser/repl-env :launch-browser false))

  ;; start a REPL

  (repl/repl env)

  ;; open http://localhost:9000/ in a browser.

)


;; node

(comment

  ;; create a browser evaluation environment

  (def env (node/repl-env))

  ;; start a REPL

  (repl/repl env)

)
