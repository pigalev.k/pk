(ns hello-cljs.node
  "Building for Node execution environment."
  (:require))


(enable-console-print!)
(println "Hello Node!")


;;;; Node APIs (v14.17.1)
;; https://nodejs.org/docs/latest-v14.x/api/os.html

(def log js/console.log)

(defn obj
  "Constructs a new regular JS Object from another object.
  Useful for cloning null-prototype objects, e.g. for inspection."
  [o]
  (js/Object.assign #js{} o))

(defn ->clj
  "Call `js->clj`, keywordizing object keys."
  [x]
  (js->clj x :keywordize-keys true))


;; os

(comment

  (def os (js/require "os"))
  (sort (keys (->clj os)))
  ;; (:EOL :arch :constants :cpus :endianness :freemem :getPriority :homedir
  ;;  :hostname :loadavg :networkInterfaces :platform :release :setPriority
  ;;  :tmpdir :totalmem :type :uptime :userInfo :version)


  ;; methods
  (.arch os)
  (-> (.cpus os)
      count)
  (-> (.cpus os)
      first
      .-model)
  (->> (.cpus os)
       js->clj
       (map #(get % "speed")))

  (.endianness os)

  (.freemem os)
  (str "Free mem: "
       (int (/ (.freemem os) (* 1024 1024 1024)))
       "GB")

  (.getPriority os)

  (.homedir os)

  (.hostname os)

  (.loadavg os)

  (-> (.networkInterfaces os)
      js->clj
      keys)
  (-> (.networkInterfaces os)
      ->clj
      :enp8s0
      first
      :address)

  (.platform os)

  (.release os)

  (.setPriority os 0)

  (.tmpdir os)

  (.totalmem os)
  (str "Total mem: "
       (int (/ (.totalmem os) (* 1024 1024 1024)))
       " GB")

  (.type os)

  (.uptime os)
  (str "Up: "
       (int (/ (.uptime os) (* 60 60 24)))
       " days")

  (.userInfo os)

  (.version os)


  ;; fields
  (.-EOL os)

  (sort (keys (->clj (obj (.. os -constants)))))
  ;; (:UV_UDP_REUSEADDR :dlopen :errno :priority :signals)

  (.. os -constants -UV_UDP_REUSEADDR)
  (obj (.. os -constants -dlopen))
  (obj (.. os -constants -errno))
  (obj (.. os -constants -signals))
  (obj (.. os -constants -priority))

)


;; globals: objects available in all modules

(comment

  ;; module-specific
  js/__dirname
  js/__filename
  js/exports
  (-> js/module
      obj
      js->clj
      keys)
  js/require

  ;; global
  (.keys js/Object js/global)

  (-> js/console
      ->clj
      keys)

  (js/Buffer "ouch")

  (.keys js/Object js/process)
  (-> js/process
      obj
      ->clj
      :env
      obj
      ->clj
      :SHELL)
  (.cwd js/process)
  (.-argv js/process)
  (.-config js/process)
  (.memoryUsage js/process)
  (.-release js/process)
  (.-title js/process)
  (.-version js/process)
  (.-versions js/process)

)
