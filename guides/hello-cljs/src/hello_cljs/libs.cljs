(ns hello-cljs.libs
  "Using foreign JS libs."
  (:require
   react-dom
   [goog.string.format]
   [goog.string :as s]
   ;; import JS libs specified in build file
   ;; lib that defines globals: require the lib and use defined globals
   [globalHello]
   ;; Google Closure Library module: require what is provided
   [goog.hello :as g]
   ;; commonJS module: require what is exported
   [commonjsHello :as c]
   ;; ES6 module: require what is exported
   [es6Hello :as e]
   ;; AMD module: require what is defined
   ;; currently not working
   #_[amdHello :as a]))


(enable-console-print!)


(defn square [x]
  (* x x))

(println "Hello world!")
;; please note: goog.string.format defies advanced optimization and it provides
;; few of the capabilities of Clojure's format - which does a lot because of
;; java.util.Formatter
;; https://stackoverflow.com/questions/34667532/clojure-clojurescript-e-g-the-format-function
(println (s/format "Square of %i is %i" 121 (square 121)))

(.render js/ReactDOM
  (.createElement js/React "h2" nil "Hello, cljs + React!")
  (.getElementById js/document "app"))


;; use JS libs

(def a-name "cljs in browser")

(js/alert (js/globalHello a-name))
(js/alert (g/hello a-name))
(js/alert (c/hello a-name))
(js/alert (e/hello a-name))
#_(js/alert (a/hello a-name))
