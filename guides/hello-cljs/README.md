# hello-cljs

Basics of ClojureScript development, building and testing.

## Starting REPLs, building code, serving results

### Cljs compiler API (`cljs.build.api/build`)

```
clojure -M build.clj
```

### Cljs compiler CLI (`cljs.main`)

See `Makefile` for example commands.

## Exploring and evaluating the source

### Emacs + CIDER

Open a cljs source file, run `cider-jack-in-cljs` (`C-c M-J` by default). Select
