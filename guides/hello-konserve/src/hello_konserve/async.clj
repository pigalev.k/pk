(ns hello-konserve.async)


;; macros for async (cljs) examples

;; cljs macros are tricky (let alone cljc macros ?:-[])
;; https://code.thheller.com/blog/shadow-cljs/2019/10/12/clojurescript-macros.html

(defmacro put-on
  "Takes the value from the channel returned by `expr` and puts it into `ch`."
  [ch expr]
  `(cljs.core.async/go
     (let [res# (cljs.core.async/<! ~expr)]
       (if res#
         (cljs.core.async/>! ~ch res#)
         (println
          "Expression returned nil - can't put it on a channel, sorry")))))
