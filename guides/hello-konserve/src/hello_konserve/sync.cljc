(ns hello-konserve.sync
  (:require
   [clojure.core :refer [byte-array]]
   [konserve.core :as k]
   [konserve.filestore :refer [connect-fs-store]]))


;; synchronous execution

(comment

  (def store (connect-fs-store "/tmp/store" :opts {:sync? true}))

  (k/assoc-in store ["foo" :bar] {:foo "baz"} {:sync? true})
  (k/get-in store ["foo"] nil {:sync? true})
  (k/exists? store "foo" {:sync? true})

  (k/assoc-in store [:bar] 42 {:sync? true})
  (k/update-in store [:bar] inc {:sync? true})
  (k/get-in store [:bar] nil {:sync? true})
  (k/dissoc store :bar {:sync? true})

  (k/append store :error-log {:type :horrible} {:sync? true})
  (k/log store :error-log {:sync? true})

  (let [ba (byte-array (* 10 1024 1024) (byte 42))]
    (time (k/bassoc store "banana" ba {:sync? true})))

  (k/bget store "banana"
          (fn [{is :input-stream}]
            (println (.read is)))
          {:sync? true})

)
