(ns hello-konserve.async
  (:require
   [konserve.core :as k]
   #_[konserve.indexeddb :refer [connect-idb-store]]
   [konserve.memory :refer [new-mem-store]]
   [cljs.core.async :as async :refer [go go-loop chan <! >!]])
  (:require-macros [hello-konserve.async :refer [put-on]]))


(enable-console-print!)


;; asynchronous execution
;; (seems to not work in browser yet, only node, so no IndexedDB)

;; create a channel and a process that will take out and print every value that
;; will be put in the channel, until the channel is closed
(def ch (chan))

(go-loop []
  (when-let [val (<! ch)]
    (println val)
    (recur)))

;; got my mojo working? you say true, we say thankya
(go (>! ch :value))

(comment

  ;; store constructor returns a channel from which we should draw the actual
  ;; store first

  ;; in-memory store (node, browser)
  (go (def store (<! (new-mem-store))))

  ;; IndexedDB store (browser)
  ;; (go (def idb (<! (connect-idb-store "hello-konserve"))))


  ;; put and get a value
  (go (>! ch (<! (k/assoc-in store ["foo" :bar] {:foo "baz"}))))
  (go (>! ch (<! (k/get-in store ["foo"]))))
  (go (>! ch (<! (k/exists? store "foo"))))
  ;; a convenience macro to spare us some `(go (>! ch (<! ...)`
  (put-on ch (k/get-in store ["foo"]))

  ;; put, update and remove a value
  (put-on ch (k/assoc-in store [:bar] 42))
  (put-on ch (k/update-in store [:bar] inc))
  (put-on ch (k/get-in store [:bar]))
  (put-on ch (k/dissoc store :bar))

  ;; append value to a collection and retrieve the collection
  (put-on ch (k/append store :error-log {:type :horrible}))
  (put-on ch (k/append store :error-log {:type :bearable}))
  (put-on ch (k/log store :error-log))

)
