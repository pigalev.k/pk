# hello-konserve

Exploring the API of `konserve` --- clojuresque key-value/document store
protocol with `core.async`.

- https://github.com/replikativ/konserve

## Getting started

Start a cljs REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
