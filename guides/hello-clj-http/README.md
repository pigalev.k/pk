# hello-clj-http

Exploring clj-http --- an idiomatic Clojure HTTP client wrapping the Apache
client.

- https://github.com/dakrone/clj-http

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
