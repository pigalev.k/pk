(ns hello-clj-http.core
  (:require
   [clj-http.client :as client]))


(comment

  ;; sync

  (client/head "http://localhost:8888")
  (client/head "http://localhost:8888" {:accept :json})

  (client/get "http://localhost:8888")
  (client/get "http://localhost:8888"
              {:query-params {"q" "clojure"}}
              {:as :byte-array})

  (client/put "http://localhost:8888" {:body "ololo"})

  (client/post "http://localhost:8888"
               {:basic-auth ["user" "pass"]
                :body "{\"json\": \"input\"}"
                :headers {"X-Api-Version" "2"}
                :content-type :json
                :socket-timeout 1000      ;; in milliseconds
                :connection-timeout 1000  ;; in milliseconds
                :accept :json})


  ;; async

  @(client/get "http://localhost:8888"
               {:async? true}
               ;; respond callback
               (fn [response] (println "response is:" response))
               ;; raise callback
               (fn [exception] (println "exception message is: "
                                        (.getMessage exception))))

)
