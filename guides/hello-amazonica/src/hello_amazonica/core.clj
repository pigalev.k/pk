(ns hello-amazonica.core
  (:require
   [aero.core :as a]
   [clojure.java.io :as io]
   [clojure.string :as s]
   [amazonica.core :refer [defcredential]]))


;; read AWS client configuration from the config file

(def dotenv-xform
  "Transformations for the sequence of lines read from the file containing
  environment variables. Result is a sequence of vectors (name-value pairs)
  ready to be placed into a map."
  (comp
   ;; remove comments from the lines
   (map #(s/replace % #"#.*" ""))
   ;; trim the lines
   (map s/trim)
   ;; extract variable names and values from the lines
   (map #(next (re-find #"(.*)=(.*)" %)))
   ;; remove nils (empty extraction results)
   (keep identity)
   ;; wrap name-value pairs in vectors
   (map vec)))

(defn dotenv
  "Read environment variables from the file."
  [filename]
  (let [dotenv (line-seq (io/reader filename))]
    (into {} dotenv-xform dotenv)))

(defmethod a/reader 'dotenv
 [{:keys [profile] :as opts} tag value]
  ((dotenv ".env") (str value)))

(def config (a/read-config (io/resource "resources/config.edn")))

;; define credentials (and other settings) for all clients' functions to use

(defcredential config)
