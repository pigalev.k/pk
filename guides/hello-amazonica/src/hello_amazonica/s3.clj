(ns hello-amazonica.s3
  (:require
   [amazonica.aws.s3 :as s3]
   [amazonica.aws.s3transfer :as s3t]
   [hello-amazonica.core :as c]))


(comment

  (s3/create-bucket "two-peas")

  ;; put object with server side encryption
  (s3/put-object :bucket-name "two-peas"
              :key "foo"
              :metadata {:server-side-encryption "AES256"}
              :file upload-file)

  (s3/copy-object bucket1 "key-1" bucket2 "key-2")

  (-> (get-object bucket2 "key-2")
      :input-stream
      slurp)
  ;; (note that the InputStream returned by GetObject should be closed,
  ;; e.g. via slurp here, or the HTTP connection pool will be exhausted
  ;; after several objects are retrieved)

  (delete-object :bucket-name "two-peas" :key "foo")

  (generate-presigned-url bucket1 "key-1" (-> 6 hours from-now))

  (def file "big-file.jar")
  (def down-dir (java.io.File. (str "/tmp/" file)))
  (def bucket "my-bucket")

  ;; set S3 Client Options
  (s3/list-buckets
   {:client-config {:path-style-access-enabled false
                    :chunked-encoding-disabled false
                    :accelerate-mode-enabled false
                    :payload-signing-enabled true
                    :dualstack-enabled true
                    :force-global-bucket-access-enabled true}})

  ;; list objects in bucket
  (list-objects-v2
   {:bucket-name bucket
    :prefix "keys/start/with/this"  ; optional
    :continuation-token (:next-continuation-token prev-response)})  ; when paging through results


  (def key-pair
    (let [kg (KeyPairGenerator/getInstance "RSA")]
      (.initialize kg 1024 (SecureRandom.))
      (.generateKeyPair kg)))

  ;; put object with client side encryption
  (put-object :bucket-name bucket1
              :key "foo"
              :encryption {:key-pair key-pair}
              :file upload-file)

  ;; get object and decrypt
  (get-object :bucket-name bucket1
              :encryption {:key-pair key-pair}
              :key "foo")

  ;; get tags for the bucket
  (get-bucket-tagging-configuration {:bucket-name bucket})

  ;; get just object metadata, e.g. content-length without fetching content:
  (get-object-metadata :bucket-name bucket1
                       :key "foo")

  ;; put object from stream
  (def some-bytes (.getBytes "Amazonica" "UTF-8"))
  (def input-stream (java.io.ByteArrayInputStream. some-bytes))
  (put-object :bucket-name bucket1
            :key "stream"
            :input-stream input-stream
            :metadata {:content-length (count some-bytes)}
            :return-values "ALL_OLD")


  (let [upl (upload bucket
                    file
                    down-dir)]
    ((:add-progress-listener upl) #(println %)))

  (let [dl  (download bucket
                      file
                      down-dir)
        listener #(if (= :completed (:event %))
                    (println ((:object-metadata dl)))
                    (println %))]
    ((:add-progress-listener dl) listener))


  ;; setup S3 bucket for static website hosting
  (create-bucket bucket-name)

  (put-object bucket-name
              "index.html"
            (java.io.File. "index.html"))
  (let [policy {:Version "2012-10-17"
                :Statement [{
                             :Sid "PublicReadGetObject"
                             :Effect "Allow"
                             :Principal "*"
                             :Action ["s3:GetObject"]
                             :Resource [(str "arn:aws:s3:::" bucket-name "/*")]}]}
        json (cheshire.core/generate-string policy true)]
    (set-bucket-policy bucket-name json))

  (set-bucket-website-configuration
   :bucket-name bucket-name
   :configuration {
                   :index-document-suffix "index.html"})

  (s3/set-bucket-notification-configuration
   :bucket-name "my.bucket.name"
   :notification-configuration
   {:configurations
    {:some-config-name
     {:queue "arn:aws:sqs:eu-west-1:123456789012:my-sqs-queue-name"
      :events #{"s3:ObjectCreated:*"}
         ;; list of key value pairs as maps or nexted 2 element list
      :filter [{"foo" "bar"}
               {:baz "quux"}
               ["key" "value"]]}}})


  (s3/set-bucket-tagging-configuration
   :bucket-name "my.bucket.name"
   :tagging-configuration
   {:tag-sets [{:Formation "notlive" :foo "bar" :baz "quux"}]})

)
