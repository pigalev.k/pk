# hello-amazonica

Exploring Amazonica --- a comprehensive Clojure client for the entire Amazon AWS
API.

- https://github.com/mcohen01/amazonica

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.

## Dependencies

### S3

Start local MinIO instance

```
podman-compose -f compose-minio.yml
```
