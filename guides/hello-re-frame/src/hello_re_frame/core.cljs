(ns hello-re-frame.core
  (:require
   [reagent.dom]
   [re-frame.core :as rf]
   [clojure.string :as str]))

;; A detailed walk-through of this source code is provided in the docs:
;; https://day8.github.io/re-frame/dominoes-live/

;; -- Domino 1 - Event Dispatch -----------------------------------------------

(defn dispatch-timer-event
  []
  (let [now (js/Date.)]
    (rf/dispatch [:timer now])))  ;; <-- dispatch used

;; Call the dispatching function every second.
;; `defonce` is like `def` but it ensures only one instance is ever
;; created in the face of figwheel hot-reloading of this file.
(defonce do-timer (js/setInterval dispatch-timer-event 1000))


;; -- Domino 2 - Event Handlers -----------------------------------------------

(rf/reg-event-db              ;; sets up initial application state
 :initialize                 ;; usage:  (dispatch [:initialize])
 (fn [_ _]                   ;; the two parameters are not important here, so use _
   {:time (js/Date.)         ;; What it returns becomes the new application state
    :time-color "#f88"}))    ;; so the application state will initially be a map with two keys


(rf/reg-event-db                ;; usage:  (dispatch [:time-color-change 34562])
 :time-color-change            ;; dispatched when the user enters a new colour into the UI text field
 (fn [db [_ new-color-value]]  ;; -db event handlers given 2 parameters:  current application state and event (a vector)
   (assoc db :time-color new-color-value)))   ;; compute and return the new application state


(rf/reg-event-db                 ;; usage:  (dispatch [:timer a-js-Date])
 :timer                         ;; every second an event of this kind will be dispatched
 (fn [db [_ new-time]]          ;; note how the 2nd parameter is destructured to obtain the data value
   (assoc db :time new-time)))  ;; compute and return the new application state


;; -- Domino 4 - Query  -------------------------------------------------------

(rf/reg-sub
 :time
 (fn [db _]     ;; db is current app state. 2nd unused param is query vector
   (:time db))) ;; return a query computation over the application state

(rf/reg-sub
 :time-color
 (fn [db _]
   (:time-color db)))


;; -- Domino 5 - View Functions ----------------------------------------------

(defn clock
  []
  [:div.example-clock
   {:style {:color @(rf/subscribe [:time-color])}}
   (-> @(rf/subscribe [:time])
       .toTimeString
       (str/split " ")
       first)])

(defn color-input
  []
  [:div.color-input
   "Time color: "
   [:input.color-input__input
    {:type "search"
     :name "color"
     :list "colors"
     :value @(rf/subscribe [:time-color])
     :on-change #(rf/dispatch [:time-color-change (-> % .-target .-value)])}]
   [:datalist#colors
    [:option {:label "Meh"
              :value "navy"}]
    [:option {:label "Good"
              :value "teal"}]
    [:option {:label "Awesome!"
              :value "steelblue"}]]])  ;; <---

(defn help
  []
  [:div.help "Development tools:"
   [:ul
    [:li.help__entry
     [:kbd.help__entry-kbd-key "C-h"]
     [:span.help__entry-label "- state management"]]
    [:li.help__entry
     [:kbd.help__entry-kbd-key "C-g"]
     [:span.help__entry-label "- event graph"]]]])

(defn ui
  []
  [:div
   [:h1 "Hello world, it is now:"]
   [clock]
   [color-input]
   [help]])

;; -- Entry Point -------------------------------------------------------------

(defn render
  "Render the app and mount it into the DOM."
  []
  (reagent.dom/render [ui] (js/document.getElementById "app")))

;; The `:dev/after-load` metadata causes this function to be called
;; after shadow-cljs hot-reloads code. We force a UI update by clearing
;; the re-frame subscription cache.
(defn ^:dev/after-load clear-cache-and-render!
  []
  (rf/clear-subscription-cache!)
  (render))

;; put a value into application state
;; mount the application's ui into '<div id="app" />'
(defn run
  "Application entry point."
  []
  (rf/dispatch-sync [:initialize])
  (render))
