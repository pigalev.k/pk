(ns hello-re-frame-again.views.app
  (:require
   [hello-re-frame-again.views.components.app-header :refer [app-header]]
   [hello-re-frame-again.views.components.app-shell :refer [app-shell]]
   [hello-re-frame-again.views.pages.main :refer [main]]))

(defn app
  "An application view."
  []
  [app-shell {:header  [app-header {:title "Short URL"}]
              :drawer  nil
              :content [main]}])
