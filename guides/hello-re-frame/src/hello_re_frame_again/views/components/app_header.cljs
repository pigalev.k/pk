(ns hello-re-frame-again.views.components.app-header
  (:require
   [reagent-mui.icons.menu :refer [menu] :rename {menu menu-icon}]
   [reagent-mui.material.app-bar :refer [app-bar]]
   [reagent-mui.material.button :refer [button]]
   [reagent-mui.material.icon-button :refer [icon-button]]
   [reagent-mui.material.toolbar :refer [toolbar]]
   [reagent-mui.material.typography :refer [typography]]))

(defn app-header
  "Application header with drawer button, title and profile menu."
  [{:keys [title]}]
  [app-bar {:position :static
             :color    :transparent
             :elevation 0}
    [toolbar
     [icon-button {:size       :large
                   :edge       :start
                   :color      :inherit
                   :aria-label "menu"
                   :sx         {:mr 2}}
      [menu-icon]]
     [typography {:variant   :h6
                  :component :div
                  :sx        {:flex-grow 1}}
      title]
     [button {:color :inherit} "Login"]]])
