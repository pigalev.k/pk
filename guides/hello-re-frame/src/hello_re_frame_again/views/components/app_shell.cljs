(ns hello-re-frame-again.views.components.app-shell
  (:require
   [reagent-mui.material.css-baseline :refer [css-baseline]]))


(defn app-shell
  "Application shell --- top-level responsible layout for the app
  content, with header and drawer."
  [{:keys [header drawer content]}]
  [:<>
   [css-baseline]
   (when header header)
   (when drawer drawer)
   (when content content)])
