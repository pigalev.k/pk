(ns hello-re-frame-again.views.pages.main
  (:require
   [hello-re-frame-again.domain :as d]
   [reagent-mui.icons.arrow-circle-right-outlined
    :refer [arrow-circle-right-outlined]]
   [reagent-mui.material.alert :refer [alert]]
   [reagent-mui.material.container :refer [container]]
   [reagent-mui.material.icon-button :refer [icon-button]]
   [reagent-mui.material.input-adornment :refer [input-adornment]]
   [reagent-mui.material.snackbar :refer [snackbar]]
   [reagent-mui.material.stack :refer [stack]]
   [reagent-mui.material.text-field :refer [text-field]]
   [reagent-mui.material.typography :refer [typography]]
   [reagent.core :as r]))


(defn go-button
  "A primary action --- shorten the URL."
  [{:keys [on-click disabled]}]
  [icon-button {:edge     :end
                :on-click on-click
                :disabled disabled}
   [arrow-circle-right-outlined]])

(defn url-input-field
  "A text field for URL with a primary action button."
  [{:keys [on-action on-input action-disabled]}]
  [text-field {:variant      :outlined
               :margin       :normal
               :label        "Your URL"
               :on-input     on-input
               :on-key-press (fn [event]
                               (when (and (not action-disabled)
                                          (= (.-key event) "Enter"))
                                 (on-action)))
               :InputProps   {:end-adornment
                              (r/as-element
                               [input-adornment {:position :end}
                                [go-button {:on-click on-action
                                            :disabled action-disabled}]])}}])

(defn main
  "Main page."
  []
  (let [url            (r/atom "")
        short-url      (r/atom "")
        alert-shown    (r/atom false)
        copy           (fn [s]
                         (.writeText (.-clipboard js/navigator) s))
        on-alert-close (fn [_] (reset! alert-shown false))
        on-input       (fn [event]
                         (reset! url (.-value (.-target event)))
                         (reset! short-url ""))
        on-action      (fn [_]
                         (on-alert-close nil)
                         (reset! alert-shown true)
                         (reset! short-url
                                 (str (.-href js/location)
                                      (d/rand-string 5)))
                         (copy @short-url))]
    (fn []
      [container {:sx {:height "90vh"}}
       [stack {:direction       :column
               :align-items     :center
               :justify-content :center
               :sx              {:height "100%"}}
        [typography {:color "text.secondary"}
         "Shorten your URL and get it back"]
        [url-input-field {:action-disabled (not (seq @url))
                          :on-action       on-action
                          :on-input        on-input}]
        (if (seq @short-url)
          [typography {:color "info.main"} @short-url]
          [typography {:color "text.secondary"}
           "paste the URL and hit enter"])
         [snackbar {:open               @alert-shown
                    :on-close           on-alert-close
                    :auto-hide-duration 1000
                    :anchor-origin      {:vertical   :bottom
                                         :horizontal :center}}
          [alert {:severity :info
                  :variant  :filled
                  :on-close on-alert-close}
           "Copied to clipboard"]]]])))
