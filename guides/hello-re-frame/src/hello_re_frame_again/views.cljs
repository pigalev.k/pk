(ns hello-re-frame-again.views
  (:require
   [hello-re-frame-again.events :as events]
   [hello-re-frame-again.subscriptions :as subs]
   [re-frame.core :as rf]))


;; pure, presentational views (reagent components); depend on nothing but their
;; parameters

(defn set-time-button
  [{:keys [on-click] :as props}]
  [:button {:on-click on-click} "Update now"])

(defn time-label
  "A label showing date and time of last update."
  [now-string]
  [:h2 "Last updated at " @now-string])

(defn session-id-label
  "A label showing the current session ID."
  [session-id-string]
  [:h3 "Session id: " @session-id-string])


;; impure, container views (re-frame components); depend on re-frame

(defn example-app
  "Application' s top-level view."
  []
  (let [session-id-string (rf/subscribe [::subs/session-id-string])
        now-string        (rf/subscribe [::subs/now-string])
        set-now           (fn [_] (rf/dispatch [::events/set-now]))]
    [:div
     [:h1.outline--header "🏗️ Under construction"]
     [time-label now-string]
     [session-id-label session-id-string]
     [set-time-button {:on-click set-now}]]))
