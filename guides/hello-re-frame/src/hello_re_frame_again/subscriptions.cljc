(ns hello-re-frame-again.subscriptions
  (:require
   [hello-re-frame-again.domain :as d]
   [re-frame.core :as rf]))


;; extractor subscriptions: simple, no computations

(defn now-subscription-handler
  "Returns a date from db."
  [db _]
  (:now db))

(rf/reg-sub ::now now-subscription-handler)


(defn session-id-subscription-handler
  "Returns a session ID from db."
  [db _]
  (:session-id db))

(rf/reg-sub ::session-id session-id-subscription-handler)


;; computation subscriptions (materialized views): no direct access to db, get
;; data from extractors only, delegate computations to specialized functions

(defn now-string-subscription-signal
  "Provides a current date for current date string subscription."
  [_]
  (rf/subscribe [::now]))

(defn now-string-subscription-handler
  "Formats a current date into a string."
  [now _]
  (d/format-date now))

(rf/reg-sub ::now-string
            now-string-subscription-signal
            now-string-subscription-handler)


(defn session-id-string-subscription-signal
  "Provides a session ID for session ID string subscription. "
  [_]
  (rf/subscribe [::session-id]))

(defn session-id-string-subscription-handler
  "Formats a session ID into a string."
  [session-id]
  (str session-id))

(rf/reg-sub ::session-id-string
            session-id-string-subscription-signal
            session-id-string-subscription-handler)
