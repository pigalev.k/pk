(ns hello-re-frame-again.coeffects
  (:require
   [hello-re-frame-again.domain :as d]
   [re-frame.core :as rf]))


(defn uuid-cofx-handler
  "Provides an UUID."
  [cofx _]
  (assoc cofx :uuid (random-uuid)))

(rf/reg-cofx
 ::uuid
 uuid-cofx-handler)


(defn now-cofx-handler
  "Provides a current date."
  [cofx _]
  (assoc cofx :now (d/now)))

(rf/reg-cofx
 ::now
 now-cofx-handler)
