(ns hello-re-frame-again.events
  (:require
   [hello-re-frame-again.coeffects :as cofx]
   [hello-re-frame-again.config :as config]
   [hello-re-frame-again.interceptors :as i]
   [hello-re-frame-again.spec :as spec]
   [re-frame.core :as rf]))


;; common interceptors (reusable between events)

(def default-interceptors
  "A chain of default interceptors."
  [(when config/dev? rf/debug)
   #_i/alert
   #_(i/print-context [:effects])
   (if config/dev?
     (i/validate-effect :db ::spec/db {:log-ok? true})
     (i/validate-effect :db ::spec/db))])

(def add-now-cofx-intc
  "Adds a current date coeffect."
  (rf/inject-cofx ::cofx/now))
(def add-uuid-cofx-intc
  "Adds a random UUID coeffect."
  (rf/inject-cofx ::cofx/uuid))


;; app initialization

(defn app-initialize-event-fx-handler
  "Creates an app initialization event."
  [{:keys [uuid now] :as cofx} _]
  {:db {:session-id uuid
        :now        now}})

(rf/reg-event-fx
 ::initialize-app
 [add-now-cofx-intc add-uuid-cofx-intc default-interceptors]
 app-initialize-event-fx-handler)


;; changing the date/time

(defn set-now-event-fx-handler
  "Creates the current date/time change event."
  [{:keys [db now] :as cofx} _]
  {:db (assoc db :now now)})

(rf/reg-event-fx
 ::set-now
 [add-now-cofx-intc default-interceptors]
 set-now-event-fx-handler)
