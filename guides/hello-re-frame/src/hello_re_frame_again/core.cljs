(ns hello-re-frame-again.core
  (:require
   [hello-re-frame-again.coeffects]
   [hello-re-frame-again.config :as config]
   [hello-re-frame-again.effects]
   [hello-re-frame-again.events :as events]
   [hello-re-frame-again.subscriptions]
   [hello-re-frame-again.views.app :refer [app]]
   [re-frame.core :as rf]
   [re-frame.loggers :as log]
   [reagent.dom :as rdom]))




(defn mount-app
  "Mount the app into DOM and do initial render."
  []
  (reagent.dom/render [app] (js/document.getElementById "app")))

(defn ^:dev/after-load clear-cache-and-render!
  []
  (rf/clear-subscription-cache!)
  (mount-app))

(defn run
  "Application entry point."
  []
  (log/console :log (str "Starting the application in "
                         (if config/dev? "development" "production")
                         " mode."))
  (rf/dispatch-sync [::events/initialize-app])
  (mount-app))
