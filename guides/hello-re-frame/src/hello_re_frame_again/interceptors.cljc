(ns hello-re-frame-again.interceptors
  (:require
   #?(:clj [clojure.spec.alpha :as s]
      :cljs [cljs.spec.alpha :as s])
   [hello-re-frame-again.config :as config]
   [hello-re-frame-again.effects :as effects]
   [hello-re-frame-again.spec :as spec]
   [re-frame.interceptor :as rfi]
   [re-frame.loggers :as log]))


(defn validate-effect
  "Returns an interceptor that checks that effect description set by preceding
  handler/interceptor conforms to spec. If it does not, cancel the effect and
  log a warning. If effect is not present, do nothing."
  [effect-key effect-value-spec
   & {:keys [log-ok? ok-log-lvl fail-log-lvl]
      :or {log-ok? false ok-log-lvl :log fail-log-lvl :error}
      :as opts}]
  (rfi/->interceptor
   :id ::validate
   :after
   (fn validate-effect [ctx]
     (let [event-name (first (get-in ctx [:coeffects :event]))]
       (if-let [effect-value (rfi/get-effect ctx effect-key)]
         (if (s/valid? effect-value-spec effect-value)
           (do
             (when log-ok?
               (log/console :group "effect validation passed")
               (log/console ok-log-lvl "effect" effect-key)
               (log/console ok-log-lvl "event" event-name)
               (log/console :groupEnd))
             ctx)
           (do
             (log/console :group "effect validation failed, ignoring the effect")
             (log/console fail-log-lvl "effect" effect-key)
             (log/console fail-log-lvl "event" event-name)
             (log/console fail-log-lvl "cause"
                          (s/explain-str effect-value-spec effect-value))
             (log/console :groupEnd)
             (update-in ctx [:effects] dissoc effect-key)))
         ctx)))))

(def alert
  "An interceptor that adds the `::effects/alert` effect with the value of the
  event."
  (rfi/->interceptor
   :id ::alert
   :after
   (fn [ctx]
     (let [alert-fx [::effects/alert (rfi/get-coeffect ctx :event)]]
       (rfi/update-effect ctx :fx (fnil conj []) alert-fx)))))

(defn print-context
  "Returns an interceptor that prints the given keys of context map on it's way
  forward and back the interceptor chain. Default keys are
  '[:effects :coeffects]'. Adjust it's position in the chain to make sure the
  necessary data can be captured."
  ([]
   (print-context [:effects :coeffects]))
  ([keys]
   (rfi/->interceptor
    :id ::print-context
    :before
    (fn [ctx]
      (log/console :log "ctx before:" (select-keys ctx keys))
      ctx)
    :after
    (fn [ctx]
      (log/console :log "ctx after:" (select-keys ctx keys))
      ctx))))
