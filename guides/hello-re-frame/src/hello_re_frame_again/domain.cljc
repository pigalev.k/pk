(ns hello-re-frame-again.domain
  #?(:clj (:import (java.text SimpleDateFormat)
                   (java.util Date))))


(def lowercase-latin-chars
  "Sequence of lowercase latin characters."
  (map char (range 97 123)))

(defn rand-string
  "Construct random string of given length from given chars.
   Length defaults to 1, chars default to lowercase alphanumeric characters."
  ([]
   (rand-string 1 lowercase-latin-chars))
  ([length]
   (rand-string length lowercase-latin-chars))
  ([length character-set]
   (apply str (take length (repeatedly #(rand-nth character-set))))))

(defn now
  "Create a new date representing the current moment."
  []
  #?(:clj (Date.)
     :cljs (js/Date.)))

(defn format-date
  "Format a date as a string with space-separated date and time parts."
  [date]
  #?(:clj
     (let [formatter (SimpleDateFormat. "dd.MM.yyyy HH:mm:ss")]
       (.format formatter date))
     :cljs
     (let [date-string (.toLocaleDateString date)
           time-string (.toLocaleTimeString date)]
       (str date-string " " time-string))))
