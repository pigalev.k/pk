(ns hello-re-frame-again.effects
  (:require
   [re-frame.core :as rf]))


(defn alert-fx-handler
  "Shows an alert message with given value."
  [value]
  #?(:clj (println "Alert:" value)
     :cljs (js/alert value)))

(rf/reg-fx ::alert alert-fx-handler)
