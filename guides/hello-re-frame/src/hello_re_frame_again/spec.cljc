(ns hello-re-frame-again.spec
  #?(:clj (:require
           [clojure.spec.alpha :as s])
     :cljs (:require
            [cljs.spec.alpha :as s])))


(s/def ::session-id uuid?)
(s/def ::now inst?)
(s/def ::db (s/keys :req-un [::session-id ::now]))
