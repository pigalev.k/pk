(ns hello-re-frame-again.events-test
  (:require
   #?(:clj [clojure.test :refer [deftest testing is run-tests]]
      :cljs [cljs.test :refer-macros [deftest testing is] :refer [run-tests]])
   [hello-re-frame-again.domain :as d]
   [hello-re-frame-again.events :as e]))


(deftest app-initialize-event-fx-handler-test
  (testing ":app/initialize event handler"
    (let [event       [:app-initialize]
          uuid        (random-uuid)
          now         (d/now)
          cofx        {:uuid uuid
                       :now  now}
          expected-fx {:db {:session-id uuid
                            :now        now }}]
      (is (= expected-fx (e/app-initialize-event-fx-handler cofx event))
          "returns correct effects"))))


(deftest now-set-event-fx-handler-test
  (testing ":now-set event handler"
    (let [event [:now-set]
          now   (d/now)
          cofx {:now now}
          expected-fx {:db {:now now}}]
      (is (= expected-fx (e/set-now-event-fx-handler cofx event))
          "returns correct effects"))))


(comment

  (run-tests)

)
