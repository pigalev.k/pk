(ns hello-re-frame-again.coeffects-test
  (:require
   #?(:clj [clojure.test :refer [deftest testing is run-tests]]
      :cljs [cljs.test :refer-macros [deftest testing is] :refer [run-tests]])
   [hello-re-frame-again.coeffects :as c]))


(deftest uuid-cofx-handler-test
  (testing "uuid-cofx-handler"
    (let [cofx {}
          event [:app/initialize]]
      (is (uuid? (:uuid (c/uuid-cofx-handler cofx event)))
          "provides UUIDs"))))


(comment

  (run-tests)

)
