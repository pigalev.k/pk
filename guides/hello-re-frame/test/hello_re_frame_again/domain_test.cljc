(ns hello-re-frame-again.domain-test
  (:require
   #?@(:clj [[clojure.test :refer [deftest testing is run-tests]]
             [clojure.core.async
              :refer [chan thread go-loop <! >! <!! >!! close! timeout]]]
       :cljs [[cljs.test
               :refer-macros [deftest testing is async]
               :refer [run-tests]]
              [cljs.core.async
               :refer-macros [go go-loop]
               :refer [chan <! >! close! timeout]]])
   [hello-re-frame-again.domain :as d]))


(deftest now-test
  (testing "now"
    #?(:clj (let [a-now-c (chan)
                  a-now-gen (go-loop []
                              (<! (timeout 100))
                              (>! a-now-c (d/now))
                              (recur))]
              (let [a-now (<!! a-now-c)
                    another-now (<!! a-now-c)]
                (is (and (inst? a-now)
                         (inst? another-now))
                    "returns dates")
                (is (not= a-now another-now)
                    "returns another date each time")
                (close! a-now-gen)
                (close! a-now-c)))
       :cljs (let [a-now-c (chan)
                   a-now-gen (go-loop []
                               (<! (timeout 100))
                               (>! a-now-c (d/now))
                               (recur))]
               (async done
                      (go
                        (let [a-now (<! a-now-c)
                              another-now (<! a-now-c)]
                          (is (and (inst? a-now)
                                   (inst? another-now))
                              "returns dates")
                          (is (not= a-now another-now)
                              "returns another date each time")
                          (close! a-now-gen)
                          (close! a-now-c)
                          (done))))))))


(deftest format-date-test
  (testing "format-date"
    (let [formatted-date (d/format-date (d/now))]
      (is (string? formatted-date)
          "returns strings"))))

(comment

  (run-tests)

  )
