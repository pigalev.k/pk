(ns hello-re-frame-again.subscriptions-test
  (:require
   #?(:clj [clojure.test :refer [deftest testing is run-tests]]
      :cljs [cljs.test :refer-macros [deftest testing is] :refer [run-tests]])
   [hello-re-frame-again.domain :as d]
   [hello-re-frame-again.subscriptions :as s]))


(deftest session-id-string-subscription-handler-test
  (testing ":session-id/string subscription"
    (let [signal       (random-uuid)
          expected-sub (str signal)]
      (is (= expected-sub (s/session-id-string-subscription-handler signal)))
          "returns correct subscription")))


(comment

  (run-tests)

)
