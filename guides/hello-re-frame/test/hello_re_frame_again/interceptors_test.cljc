(ns hello-re-frame-again.interceptors-test
  (:require
   #?(:clj [clojure.test :refer [deftest testing is run-tests]]
      :cljs [cljs.test :refer-macros [deftest testing is] :refer [run-tests]])
   [hello-re-frame-again.domain :as d]
   [hello-re-frame-again.interceptors :as i]
   [hello-re-frame-again.spec :as spec]))


(deftest validate-effect-interceptor-test
  (testing "validate-effect :db with valid effect"
    (let [session-id (random-uuid)
          now (d/now)
          ctx {:coeffects {:event [:now/set]}
               :effects {:db {:session-id session-id
                              :now now}}}
          intc (:after (i/validate-effect :db ::spec/db))
          expected-result ctx]
      (is (= expected-result (intc ctx)))
      "returns unchanged context"))
  (testing "validate-effect :db with invalid effect"
    (let [session-id (random-uuid)
          now (d/now)
          ctx {:coeffects {:event [:now/set]}
               :effects {:db {:now now}}}
          intc (:after (i/validate-effect :db ::spec/db))
          expected-result (update-in ctx [:effects] dissoc :db)]
      (is (= expected-result (intc ctx)))
          "removes effect that fails validation")))


(comment

  (run-tests)

)
