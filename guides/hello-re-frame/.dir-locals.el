;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

(;; configure the default Cider cjs REPL
 (nil . ((cider-preferred-build-tool . shadow-cljs)
         (cider-default-cljs-repl . shadow)
         (cider-clojure-cli-global-options . "-A:dev:cljs"))))
