# hello-re-frame

To learn and explore re-frame.

Based on the `simple` example from the re-frame source repo.

## Get Started

1. Install `node`
2. Install dependencies (`yarn` or `npm i`)
3. Run `yarn watch`" to compile the app and start up `shadow-cljs` hot-reloading
4. When build is complete, open `http://localhost:8700/`

## Develop

Run the build with source hot-reloading

```
yarn watch
```

## Build

Run the production build

```
yarn release
```

then serve the build folder

```
yarn serve
```

then open URL printed in terminal.

## Test

Unit tests (with Clojure)

```
clojure -M:test
```
