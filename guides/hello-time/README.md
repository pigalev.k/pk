# hello-time

Exploring date/time manipulation libraries for Clojure(Script).

Main

- cljc.java-time: `java.time` for Clojure(Script)
  https://github.com/henryw374/cljc.java-time
- tick: a Clojure(Script) & babashka library for dealing with time
  https://github.com/juxt/tick
- clojure.java-time: Java 8 Date-Time API for Clojure
  https://github.com/dm3/clojure.java-time

Additional

- Joda Time: widely used replacement for the Java date and time classes prior to
  Java SE 8 https://github.com/JodaOrg/joda-time

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
