(ns hello-time.tick
  (:require
   [tick.core :as t]))


;; basics

;; https://github.com/juxt/tick


;; in both Clojure and Clojurescript versions, tick is just calling through to
;; `java.time` methods. Understanding the main entities of `java.time` is
;; necessary to use tick.

;; https://docs.oracle.com/javase/tutorial/datetime/iso/overview.html

(comment

  ;; get the current time

  (t/instant)

)

;; common use-cases

(comment



)

;; TODO: add usage examples, common tasks, API overview
