(ns hello-time.java-time
  (:require
   [java-time.api :as jt]
   [java-time.repl :as r])
  (:import (java.util Date)
           (java.time Instant Period)
           (java.time.temporal ChronoUnit)))


;; Clojure wrapper for Java 8 Time API.

;; https://docs.oracle.com/javase/tutorial/datetime/iso/index.html
;; https://dm3.github.io/clojure.java-time/index.html


;; concepts

(comment

  ;; local dates and/or times

  ;; `LocalDate`, `LocalTime` and `LocalDateTime` are used to represent a date,
  ;; time and date-time respectively without an offset or a time zone. The local
  ;; time entities are used to represent human-based dates/times. They are a
  ;; good fit for representing the time of various events:

  ;; - `LocalDate`: birthday, holiday; see `jt/local-date`
  ;; - `LocalTime`: bus schedule, opening time of a shop; see `jt/local-time`
  ;; - `LocalDateTime`: start of a competition; see `jt/local-date-time`

  (jt/local-date 2015 10 1)
  (jt/local-time 10)
  (jt/local-date-time 2015 10)


  ;; zoned dates

  ;; there are two types which deal with zones:

  ;; - `OffsetDateTime`: see `jt/offset-date-time`
  ;; - `ZonedDateTime`: see `jt/zoned-date-time`

  ;; they do pretty much what you would expect from their name. You can think of
  ;; the offset time as a more concrete version of the zoned time. For example,
  ;; the same time zone can have different offsets throughout the year due to
  ;; DST or governmental regulations.

  (jt/offset-time 10)
  (jt/offset-date-time 2015 10)
  (jt/zoned-date-time 2015 10)

  ;; offset/zone times only take the offset/zone as the last arguments for the
  ;; maximum arity constructor. You can influence the zone/offset by using the
  ;; `jt/with-zone` or `jt/with-offset` functions, like so:

  (jt/with-zone
    (jt/zoned-date-time 2015 10) "UTC")
  (jt/with-zone-same-instant
    (jt/zoned-date-time 2015 10) "UTC")
  (jt/with-clock (jt/system-clock "UTC")
    (jt/zoned-date-time 2015 10))


  ;; instant

  ;; an `Instant` is used to generate a timestamp representing machine time. It
  ;; doesn't have an offset or a time zone. You can think of it as of a number
  ;; of milliseconds since epoch (1970-01-01T00:00:00Z). An instant is directly
  ;; analogous to `java.util.Date`:

  (java.util.Date.)

  ;; the same

  (Instant/now)
  (jt/instant)

  ;; from ISO-8601 date/time string: the same

  (Instant/parse "2023-03-31T09:37:55.810494300Z")
  (jt/instant "2023-03-31T09:37:55.810494300Z")

  ;; to `java.util.Date`: the same

  (Date/from
   (Instant/parse "2023-03-31T09:37:55.810494300Z"))

  (jt/to-java-date
   (jt/instant "2023-03-31T09:37:55.810494300Z"))

  ;; every other date entity can be converted to an instant (local ones will
  ;; require an additional zone information).


  ;; period and duration

  ;; Java Time `Period` entities are considerably simpler than the Joda-Time
  ;; periods. They are fixed containers of years, months and days. You can use
  ;; them to represent any period of time with a granularity larger or equal to
  ;; a single day. `Duration`, on the other hand, represents a standard duration
  ;; less than or equal to a single standard (24-hour) day.

  (let [start (jt/local-date 2022 1 31)
        now   (jt/local-date)]
    (jt/time-between start now :days))

  (let [start (jt/local-date 2022 1 31)
        now   (jt/local-date)]
    (.between ChronoUnit/DAYS start now))

  ;; not the total number of days, just the remainder that cannot be expressed
  ;; in years and months
  (let [start (jt/local-date 2022 1 31)
        now   (jt/local-date)]
    (-> (Period/between start now)
        .getDays))


  ;; clocks

  ;; Java Time introduced a concept of `Clock` - a time entity which can seed
  ;; the dates, times and zones. However, there's no built-in facility which
  ;; would allow you to influence the date-times created using default
  ;; constructors à la Joda's
  ;; DateTimeUtils/setCurrentMillisSystem. `clojure.java-time` tries to fix that
  ;; with the `with-clock` macro and the corresponding `with-clock-fn` function:

  (jt/zone-id)

  (jt/with-clock (jt/system-clock "UTC")
    (jt/zone-id))

  ;; in addition to the built-in Java Time clocks, we provide a mock clock which
  ;; can be very handy in testing:

  (def clock (jt/mock-clock 0 "UTC"))

  (jt/with-clock clock
    (jt/instant))

  (jt/advance-clock! clock (jt/plus (jt/hours 5) (jt/minutes 20)))

  (jt/with-clock clock
    (jt/instant))

  (jt/set-clock! clock 0)

  (jt/with-clock clock
    (jt/instant))

  ;; clock overrides works for all of the date-time types.

)


;; common use-cases

(comment

  ;; what is the current date?

  (def now (jt/local-date))

  ;; what's the next day?

  (jt/plus now (jt/days 1))

  ;; the previous day?

  (jt/minus now (jt/days 1))

  ;; three days starting at now?

  (take 3 (jt/iterate jt/plus now (jt/days 1)))

  ;; when is the first Monday in month?

  (jt/adjust now :first-in-month :monday)

  ;; date with some of its fields truncated:

  (jt/truncate-to (jt/local-date-time 2015 9 28 10 15) :days)

  ;; date-time adjusted to the given hour:

  (jt/adjust (jt/local-date-time 2015 9 28 10 15) (jt/local-time 6))

  ;; the latest of the given dates?

  (jt/max (jt/local-date 2015 9 20)
          (jt/local-date 2015 9 28)
          (jt/local-date 2015 9 1))

  ;; the shortest of the given durations?

  (jt/min (jt/duration 10 :seconds)
          (jt/duration 5 :hours)
          (jt/duration 3000 :millis))

  ;; get the year field out of the date:

  (jt/as (jt/local-date 2015 9 28) :year)

  ;; get multiple fields:

  (jt/as (jt/local-date 2015 9 28) :year :month-of-year :day-of-month)

  ;; get the duration in a different unit:

  (jt/plus (jt/hours 3) (jt/minutes 2))

  (jt/as *1 :minutes)

  ;; format a date:

  (jt/format "MM/dd" (jt/zoned-date-time 2015 9 28))

  ;; parse a date:

  (jt/local-date "MM/yyyy/dd" "09/2015/28")

  ;; zoned date-times and offset date-times/times always take the zone/offset as
  ;; the last argument. Offsets can be specified as float values:

  (jt/zone-offset +1.5)
  (jt/zone-offset -1.5)

  ;; compare dates:

  (jt/before? (jt/year 2020) (jt/year 2021))
  (jt/after? (jt/year 2021) (jt/year 2021))

  (let [expiration-date (jt/year 2010)
        purchase-date (jt/year 2010)]
    (jt/not-before? expiration-date purchase-date))

  (let [start-date (jt/year 2011)
        cutoff-date (jt/year 2010)]
    (jt/not-after? start-date cutoff-date))

)


;; conversions

(comment

  ;; time entities can be converted to other time entities if the target
  ;; contains less information, e.g. (assuming we're in UTC time zone):

  (jt/zoned-date-time (jt/offset-date-time 2015 9 28 1))
  (jt/instant (jt/offset-date-time 2015 9 28 1))
  (jt/offset-time (jt/offset-date-time 2015 9 28 1))
  (jt/local-date-time (jt/offset-date-time 2015 9 28 1))
  (jt/local-time (jt/offset-time 1))

  ;; converting an `Instant` to `ZonedDateTime` requires a time zone:

  (jt/zoned-date-time (jt/instant 100) "UTC")

)


;; legacy date-time types

(comment

  ;; any date which can be converted to an instant, can also be converted to a
  ;; `java.util.Date`:

  (jt/java-date (jt/zoned-date-time 2015 9 28))
  (jt/java-date 50000)

  ;; an instance of `java.util.Date` serves the same purpose as the new
  ;; `java.time.Instant`. It's a machine timestamp which isn't aware of the time
  ;; zone. Please, do not get confused by the way it is printed by the Clojure
  ;; printer - the UTC time zone is applied during formatting.

  ;; sometimes you'll have to work with the legacy
  ;; `java.sql.{Date,Time,Timestamp}` types. The correspondence between the
  ;; legacy types and the new Date-Time entities is as follows:

  ;; `java.sql.Time` <-> `java.time.LocalTime`
  ;; `java.sql.Date` <-> `java.time.LocalDate`
  ;; `java.sql.Timestamp` <-> `java.time.LocalDateTime`

  (jt/sql-date 2015 9 28)
  (jt/sql-timestamp 2015 9 28 10 20 30 4000000)
  (jt/sql-time 10 20 30)

  ;; the results of the above calls get printed as #inst because all of the
  ;; `java.sql.{Date,Time,Timestamp}` are subtypes of
  ;; `java.util.Date`. Coincidentally, this makes it impossible to plug the
  ;; `java.sql.*` types into the Clojure.Java-Time conversion graph.

  ;; conversions to the legacy types also go the other way around:

  (jt/local-date (jt/sql-date 2015 9 28))
  (jt/local-date-time (jt/sql-timestamp 2015 9 28 10 20 30 4000000))
  (jt/local-time (jt/sql-time 10 20 30))

)


;; interop with external libraries

(comment

  ;; threeten-extra

  ;; if you add an optional `threeten-extra` dependency to the project, you will
  ;; get an `Interval`, `AmPm`, `DayOfMonth`, `DayOfYear`, `Quarter` and
  ;; `YearQuarter` data types as well as a couple more adjusters.

  ;; an interval can be constructed from two entities that can be converted to
  ;; instants:

  (jt/interval (jt/offset-date-time 2015 1 1) (jt/zoned-date-time 2016 1 1))
  (jt/move-start-by *1 (jt/duration 5 :days))
  (jt/move-end-by *1 (jt/duration 5 :days))
  (jt/contains? *1 (jt/offset-date-time 2015 1 1))

  ;; joda-time

  ;; if you have Joda Time on the classpath (either directly, or via clj-time),
  ;; you can seamlessly convert from Joda Time to Java Time types:

  (r/show-path org.joda.time.DateTime java.time.OffsetTime)

  (jt/offset-time (org.joda.time.DateTime/now))

  ;; Clojure 1.9 added an `Inst` protocol which is implemented for
  ;; `java.util.Date` and `java.time.Instant` by default. If you're stuck on
  ;; Joda-Time, you can extend the `org.joda.time.ReadableInstant`, which
  ;; includes both `Instant` and `DateTime` using the following:

  (jt/when-joda-time-loaded
   (extend-type org.joda.time.ReadableInstant
     Inst (inst-ms* [inst] (.getMillis inst))))

  ;; this snippet isn't included in the Clojure.Java-Time code by default as
  ;; both the `Inst` protocol and the Joda-Time types are external to the
  ;; library.

)


;; introspection: fields, units and properties

(comment

  ;; Date-Time entities are composed of date fields, while `Duration` entities
  ;; are composed of time units. You can see all of the predefined fields and
  ;; units via the `java-time.repl` ns:

  (r/show-fields)
  (r/show-units)

  ;; you can obtain any field/unit like this:

  (jt/field :year)
  (jt/unit :days)
  (jt/field (jt/local-date 2015) :year)

  ;; you can obtain all of the fields/units of the temporal entity:

  (jt/fields (jt/local-date))
  (jt/units (jt/duration))

  ;; by themselves the fields and units aren't very interesting. You can get the
  ;; range of valid values for a field and a duration between two dates, but
  ;; that's about it:

  (jt/range (jt/field :year))
  (jt/range (jt/field :day-of-month))
  (jt/time-between (jt/local-date 2015 9) (jt/local-date 2015 9 28) :days)

  ;; fields and units become interesting in conjunction with
  ;; properties. Java-Time doesn't support the concept of properties which is
  ;; present in Joda-Time. There are reasons for that which I feel are only
  ;; valid in a statically-typed API like Java's. In Clojure, properties allow
  ;; expressing time entity modifications and queries uniformly across all of
  ;; the entity types.

  (def prop (jt/property (jt/local-date 2015 2 28) :day-of-month))

  (jt/value prop)
  (jt/with-min-value prop)
  (jt/with-value prop 20)
  (jt/with-max-value prop)
  (jt/properties (jt/local-date 2015 9 28))

)


;; implementation details

(comment

  ;; conversion graph

  ;; most of the temporal entity constructors with arities 1 to 3 use the
  ;; conversion graph underneath. This provides for a very flexible way of
  ;; defining the conversions while avoiding huge conditional statements and
  ;; multiple definitions of the identical conversion logic. However, the
  ;; flexibility comes with a cost:

  ;; - the first call to a constructor will take a long time as it will try to
  ;;   find a path in the conversion graph. Subsequent calls will reuse the path.
  ;; - it's not trivial to evaluate the impact of adding and removing
  ;;   conversions both on the performance and the conversion path chosen for
  ;;   certain arguments.
  ;; - you might get nonsensical results for some of the paths in the graph that
  ;;   you might expect would make sense.

  ;; hopefully, the performance issue will be resolved in the future...

  ;; you can play with the conversion graph using the following helpers:

  (r/show-path org.joda.time.DateTime java.time.OffsetTime)


  ;; notes on performance

  ;; because of the above, the current incarnation of the library is relatively
  ;; slow while calling the 2-3 arity
  ;; `zoned-date-time`/`offset-time`/`offset-date-time` constructors for the
  ;; first time in a given Clojure runtime. If you need predictable latency at
  ;; the time of the first call in your business logic, please warm the
  ;; constructors you are going to use up by calling them beforehand, e.g.:

  (defn warm-up []
    (jt/zoned-date-time 2015 1 1)
    (jt/zoned-date-time 2015 1)
    (jt/zoned-date-time 2015))

  ;; the "constructor" here refers to an arity of a function together with its
  ;; type signature. For example, a (jt/zoned-date-time 2015)
  ;; and (jt/zoned-date-time (jt/system-clock)) are different constructors.

)
