(ns hello-time.java-time
  (:require
   [cljc.java-time.format.date-time-formatter :as dtf]
   [cljc.java-time.instant :as i]
   [cljc.java-time.local-date :as ld]
   [cljc.java-time.local-date-time :as ldt]
   [cljc.java-time.zoned-date-time :as zdt]))


;; basics

;; https://github.com/henryw374/cljc.java-time


;; uses `java.time` in Clojure, `js-joda` in ClojureScript.

;; https://docs.oracle.com/javase/tutorial/datetime/iso/overview.html
;; https://github.com/js-joda/js-joda

;; for every class in `java.time`, there is a clojure namespace.

(comment

  ;; create a date

  (def a-date (ld/parse "2019-01-01"))

  ;; add some days

  (ld/plus-days a-date 99)

  ;; roundtripping with legacy Date

  #?(:cljs (-> (js/Date.)
               (.getTime)
               (i/of-epoch-milli)
               (i/to-epoch-milli)
               (js/Date.)))

  #?(:clj (-> (Date.)
              (.getTime)
              (i/of-epoch-milli)
              (i/to-epoch-milli)
              (Date.)))

)


;; common use-cases

(comment

  ;; what is the current date?

  (def now (ld/now))

  ;; what's the next day?

  (ld/plus-days now 1)

  ;; the previous day?

  (ld/minus-days now 1)

  ;; three days starting at now?

  ;; (take 3 (jt/iterate jt/plus now (jt/days 1)))

  ;; when is the first Monday in month?

  ;; (jt/adjust now :first-in-month :monday)

  ;; date with some of its fields truncated:

  ;; (jt/truncate-to (jt/local-date-time 2015 9 28 10 15) :days)

  ;; date-time adjusted to the given hour:

  ;; (jt/adjust (jt/local-date-time 2015 9 28 10 15) (jt/local-time 6))

  ;; the latest of the given dates?

  ;; (jt/max (jt/local-date 2015 9 20)
  ;;         (jt/local-date 2015 9 28)
  ;;         (jt/local-date 2015 9 1))

  ;; the shortest of the given durations?

  ;; (jt/min (jt/duration 10 :seconds)
  ;;         (jt/duration 5 :hours)
  ;;         (jt/duration 3000 :millis))

  ;; get the year field out of the date:

  ;; (jt/as (jt/local-date 2015 9 28) :year)

  ;; get multiple fields:

  ;; (jt/as (jt/local-date 2015 9 28) :year :month-of-year :day-of-month)

  ;; get the duration in a different unit:

  ;; (jt/plus (jt/hours 3) (jt/minutes 2))

  ;; (jt/as *1 :minutes)

  ;; format a date:

  (def now (ldt/now))

  (ld/format now dtf/basic-iso-date)
  (ld/format now (dtf/of-pattern "dd.MM.yyyy HH:mm"))

  ;; parse a date:

  ;; (jt/local-date "MM/yyyy/dd" "09/2015/28")

  (ld/parse "2023-04-07T07:13:33Z" dtf/iso-zoned-date-time)
  (ldt/parse "2023-04-07T07:13:33Z" dtf/iso-zoned-date-time)
  (zdt/parse "2023-04-07T07:13:33Z" dtf/iso-zoned-date-time)

  ;; zoned date-times and offset date-times/times always take the zone/offset as
  ;; the last argument. Offsets can be specified as float values:

  ;; (jt/zone-offset +1.5)
  ;; (jt/zone-offset -1.5)

  ;; compare dates:

  ;; (jt/before? (jt/year 2020) (jt/year 2021))
  ;; (jt/after? (jt/year 2021) (jt/year 2021))

  ;; (let [expiration-date (jt/year 2010)
  ;;       purchase-date (jt/year 2010)]
  ;;   (jt/not-before? expiration-date purchase-date))

  ;; (let [start-date (jt/year 2011)
  ;;       cutoff-date (jt/year 2010)]
  ;;   (jt/not-after? start-date cutoff-date))

)


;; TODO: add usage examples, common tasks, API overview
