(ns hello-manifold.core
  (:require
   [clojure.core.async :as a]
   [manifold.deferred :as d]
   [manifold.executor :as e]
   [manifold.go-off :as g]
   [manifold.stream :as s]))

;; Manifold provides basic building blocks for asynchronous programming, and can
;; be used as a translation layer between libraries which use similar, but
;; incompatible, abstractions.

;; Manifold provides two core abstractions:
;; * deferreds, which represent a single asynchronous value
;; * streams, which represent an ordered sequence of asynchronous values


;; deferreds

(comment

  ;; a deferred in Manifold is similar to a Clojure `promise`

  (def d (d/deferred))
  (d/success! d :foo)
  @d

  ;; however, similar to Clojure's futures, deferreds in Manifold can also
  ;; represent errors. Crucially, they also allow for callbacks to be
  ;; registered, rather than simply blocking on dereferencing.

  (def d (d/deferred))
  (d/error! d (Exception. "boom"))
  @d

  (def d (d/deferred))
  (d/on-realized d
    (fn [x] (println "success!" x))
    (fn [x] (println "error!" x)))

  (d/success! d :foo)
  (d/error! d (Exception. "boom"))

  ;; composing with deferreds

  ;; callbacks are a useful building block, but they're a painful way to create
  ;; asynchronous workflows. In practice, no one should ever need to use
  ;; `on-realized`. Manifold provides a number of operators for composing over
  ;; deferred values.


  ;; instead, they should use `manifold.deferred/chain`, which chains together
  ;; callbacks, left to right
  (def d (d/deferred))
  (d/chain d inc inc inc #(println "x + 3 =" %))
  (d/success! d 0)

  ;; chain returns a deferred representing the return value of the right-most
  ;; callback. If any of the functions returns a deferred or a value that can be
  ;; coerced into a deferred, the chain will be paused until the deferred yields
  ;; a value.

  ;; values that can be coerced into a deferred include Clojure futures, Java
  ;; futures, and Clojure promises.
  (def d (d/deferred))
  (d/chain d
           #(future (inc %))
           #(println "the future returned" %))

  (d/success! d 0)

  ;; if any stage in chain throws an exception or returns a deferred that yields
  ;; an error, all subsequent stages are skipped, and the deferred returned by
  ;; chain yields that same error. To handle these cases, you can use
  ;; `manifold.deferred/catch`
  (def d (d/deferred))
  (-> d
      (d/chain dec #(/ 1 %))
      (d/catch Exception #(println "whoops, that didn't work:" %)))

  (d/success! d 1)

  ;; using the `->` threading operator, chain and catch can be easily and
  ;; arbitrarily composed.

  ;; to combine multiple deferrable values into a single deferred that yields
  ;; all their results, we can use `manifold.deferred/zip`
  @(d/zip (future 1) (future 2) (future 3))

  ;; finally, we can use `manifold.deferred/timeout!` to register a timeout on
  ;; the deferred which will yield either a specified timeout value or a
  ;; `TimeoutException` if the deferred is not realized within `n` milliseconds.
  @(d/timeout!
    (d/future (Thread/sleep 1000) :foo)
    100
    ::timeout)

  ;; note that if a timeout is placed on a deferred returned by chain, the
  ;; timeout elapsing will prevent any further stages from being executed.

  ;; `future` vs `manifold.deferred/future`

  ;; Clojure's futures can be treated as deferreds, as can Clojure's
  ;; promises. However, since both of these abstractions use a blocking
  ;; dereference, in order for Manifold to treat it as an asynchronous deferred
  ;; value it must allocate a thread.

  ;; wherever possible, use `manifold.deferred/deferred` instead of promise, and
  ;; `manifold.deferred/future` instead of future. They will behave identically
  ;; to their Clojure counterparts (deliver can be used on a Manifold deferred,
  ;; for instance), but allow for callbacks to be registered, so no additional
  ;; threads are required.

  ;; `let-flow`

  ;; let's say that we have two services which provide us numbers, and want to
  ;; get their sum. By using `zip` and `chain` together, this is relatively
  ;; straightforward
  (defn call-service-a
    []
    (future 1))

  (defn call-service-b
    []
    (future 2))

  (defn deferred-sum []
    (let [a (call-service-a)
          b (call-service-b)]
      (d/chain (d/zip a b)
             (fn [[a b]]
               (+ a b)))))

  @(deferred-sum)

  ;; however, this isn't a very direct expression of what we're doing. For more
  ;; complex relationships between deferred values, our code will become even
  ;; more difficult to understand. In these cases, it's often best to use
  ;; `let-flow`.

  (defn deferred-sum-1 []
    (d/let-flow [a (call-service-a)
                 b (call-service-b)]
      (+ a b)))

  @(deferred-sum-1)

  ;; in `let-flow`, we can treat deferred values as if they're realized. This is
  ;; only true of values declared within or closed over by `let-flow`,
  ;; however. So we can do this
  @(let [a (future 1)]
     (d/let-flow [b (future (+ a 1))
                  c (+ b 1)]
                 (+ c 1)))

  ;; but not this
  (d/let-flow [a (future 1)
               b (let [c (future 1)]
                   (+ a c))]
              (+ b 1))

  ;; in this example, `c` is declared within a normal let binding, and as such
  ;; we can't treat it as if it were realized.

  ;; it can be helpful to think of `let-flow` as similar to Prismatic's Graph
  ;; library, except that the dependencies between values are inferred from the
  ;; code, rather than explicitly specified. Comparisons to `core.async`'s
  ;; goroutines are less accurate, since `let-flow` allows for concurrent
  ;; execution of independent paths within the bindings, whereas operations
  ;; within a goroutine are inherently sequential.

  ;; `go-off`

  ;; an alternate way to write code using deferreds is the macro
  ;; `manifold.go-off/go-off`. This macro is an almost-exact mirror of the `go`
  ;; macro from `core.async`, to the point where it actually utilizes the state
  ;; machine functionality from `core.async`. In order to use this macro,
  ;; `core.async` must be provided as a dependency by the user.

  ;; there are a few major differences between `go` and `go-off`. First,
  ;; `go-off` (unsurprisingly) works with Manifold deferreds and streams instead
  ;; of `core.async` channels. Second, in addition to the `<!` fn, which always
  ;; returns a value, there's also the `<!?` macro, which behaves the same
  ;; unless a `Throwable` is retrieved, in which case it's automatically
  ;; rethrown for you. Finally, there's no `>!` equivalent in `go-off`, as
  ;; there's no way without altering the syntax to distinguish between success
  ;; and error when putting into a deferred.

  ;; the benefit of `go-off` over `let-flow` is that it gives complete control
  ;; of when deferreds should be realized to the user, removing any potential
  ;; surprises (especially around timeouts).

  ;; basic usage
  @(g/go-off (+ (g/<! (d/future 10))
                (g/<! (d/future 20))))

  ;; <!? usage
  ;; if you forget to catch an exception in go, it won't be returned
  (a/<!! (a/go (try (a/<! (a/go (/ 5 0)))
                    (catch Exception e
                      "ERROR"))))

  ;; d/future returns any exceptions, and <!? rethrows it for you
  @(g/go-off (try (g/<!? (d/future (/ 5 0)))
                  (catch Exception e
                    "ERROR")))

  ;; `loop`

  ;; manifold also provides a `loop` macro, which allows for asynchronous loops
  ;; to be defined. Consider `manifold.stream/consume`, which allows a function
  ;; to be invoked with each new message from a stream. We can implement similar
  ;; behavior like so
  (defn my-consume [f stream]
    (d/loop []
      (d/chain (s/take! stream ::drained)
               ;; if we got a message, run it through `f`
               (fn [msg]
                 (if (identical? ::drained msg)
                   ::drained
                   (f msg)))
               ;; wait for the result from `f` to be realized, and
               ;; recur, unless the stream is already drained
               (fn [result]
                 (when-not (identical? ::drained result)
                   (d/recur))))))

  (def s (s/stream))
  (s/put! s 2)
  (my-consume println s)

  ;; here we define a loop which takes messages one at a time from stream, and
  ;; passes them into `f`. If `f` returns an unrealized value, the loop will
  ;; pause until it's realized. To recur, we make sure the value returned from
  ;; the final stage is (`manifold.deferred/recur` & args), which will cause the
  ;; loop to begin again from the top.
)

;; streams

(comment

  ;; Manifold's streams provide mechanisms for asynchronous puts and takes,
  ;; timeouts, and backpressure. They are compatible with Java's BlockingQueues,
  ;; Clojure's lazy sequences, and `core.async`'s channels. Methods for
  ;; converting to and from each are provided.

  ;; Manifold differentiates between sources, which emit messages, and sinks,
  ;; which consume messages. We can interact with sources using `take!` and
  ;; `try-take!`, which return deferred values representing the next message. We
  ;; can interact with sinks using `put!` and `try-put!`, which return a
  ;; deferred values which will yield true if the put is successful, or false
  ;; otherwise.

  ;; we can create a stream using (`manifold.stream/stream`)
  (def s (s/stream))
  (s/put! s 1)
  @(s/take! s)
  @(s/try-take! s ::default 1000 ::timeout)

  (s/close! s)
  (s/closed? s)
  (s/drained? s)
  @(s/take! s ::drained)

  ;; a stream is both a sink and a source; any message sent via `put!` can be
  ;; received via `take!`. We can also create sinks and sources from other
  ;; stream representations using `->sink` and `->source`
  (def c (a/chan))
  (def s (s/->source c))
  (a/go (a/>! c 1))
  @(s/take! s)

  ;; we can also turn a Manifold stream into a different representation by using
  ;; `connect` to join them together
  (def s (s/stream))
  (def c (a/chan))
  (s/connect s c)
  (s/put! s 1)
  (a/<!! c)

  ;; Manifold can use any transducer, which are applied via `transform`. It also
  ;; provides stream-specific transforms, including `zip`, `reduce`, `buffer`,
  ;; `batch`, and `throttle`.


  ;; stream operators

  ;; the simplest thing we can do with a stream is `consume` every message that
  ;; comes into it
  (def s (s/stream))
  (s/consume #(prn 'message! %) s)
  @(s/put! s 1)


  ;; however, we can also create derivative streams using operators analogous to
  ;; Clojure's sequence operators
  (->> [1 2 3]
       s/->source
       (s/map inc)
       s/stream->seq)

  ;; here, we've mapped inc over a stream, transforming from a sequence to a
  ;; stream and then back to a sequence for the sake of a concise example. Note
  ;; that calling `manifold.stream/map` on a sequence will automatically call
  ;; `->source`, so we can actually omit that, leaving just
  (->> [1 2 3]
       (s/map inc)
       s/stream->seq)


  ;; since streams are not immutable, in order to treat it as a sequence we must
  ;; do an explicit transformation via `stream->seq`
  (->> [1 2 3]
       s/->source
       s/stream->seq
       (map inc))

  ;; note that we can create multiple derived streams from the same source
  (def s (s/stream))
  (def a (s/map inc s))
  (def b (s/map dec s))
  @(s/put! s 0)
  @(s/take! a)
  @(s/take! b)

  ;; here, we create a source stream `s`, and map `inc` and `dec` over it. When
  ;; we put our message into `s` it immediately is accepted, since `a` and `b`
  ;; are downstream. All messages put into `s` will be propagated into both `a`
  ;; and `b`.

  ;; if s is closed, both `a` and `b` will be closed, as will any other
  ;; downstream sources we've created. Likewise, if everything downstream of `s`
  ;; is closed, `s` will also be closed. This is almost always desirable, as
  ;; failing to do this will simply cause `s` to exert backpressure on
  ;; everything upstream of it. However, if we wish to avoid this behavior, we
  ;; can create a `permanent-stream`, which cannot be closed.

  ;; for any Clojure operation that doesn't have an equivalent in
  ;; `manifold.stream`, we can use `manifold.stream/transform` with a
  ;; transducer
  (->> [1 2 3]
       (s/transform (map inc))
       s/stream->seq)

  ;; there's also `(periodically period f)`, which behaves like `(repeatedly
  ;; f)`, but will emit the result of `(f)` every `period` milliseconds.
  (->> (s/periodically 1000 rand)
       (s/transform (take 3))
       s/stream->seq)

  ;; connecting streams

  ;; having created an event source through composition of operators, we will
  ;; often want to feed all messages into a sink. This can be accomplished via
  ;; `connect`
  (def a (s/stream))
  (def b (s/stream))
  (s/connect a b)
  @(s/put! a 1)
  @(s/take! b)

  ;; again, we see that our message is immediately accepted into `a`, and can be
  ;; read from `b`. We may also pass an options map into `connect`, with any of
  ;; the following keys
  ;; field	       description
  ;; `downstream?` whether the source closing will close the sink, defaults to true
  ;; `upstream?`   whether the sink closing will close the source, even if there are other sinks downstream of the source, defaults to false
  ;; `timeout` the maximum time that will be spent waiting to convey a message into the sink before the connection is severed, defaults to nil
  ;; `description` a description of the connection between the source and sink, useful for introspection purposes

  ;; after connecting two streams, we can inspect any of the streams using
  ;; description, and follow the flow of data using downstream
  (def a (s/stream))
  (def b (s/stream))
  (s/connect a b {:description "a connection"})
  (s/description a)
  (s/downstream a)

  ;; we can recursively apply `downstream` to traverse the entire topology of
  ;; our streams. This can be a powerful way to reason about the structure of
  ;; our running processes.

  ;; sometimes we want to change the message from the source before it's placed
  ;; into the sink. For this, we can use `connect-via`
  (def a (s/stream))
  (def b (s/stream))
  (s/connect-via a #(s/put! b (inc %)) b)
  @(s/put! a 1)
  @(s/take! b)

  ;; Note that connect-via takes an argument between the source and sink, which
  ;; is a single-argument callback. This callback will be invoked with messages
  ;; from the source, under the assumption that they will be propagated to the
  ;; sink. This is the underlying mechanism for map, filter, and other stream
  ;; operators; it allow us to create complex operations that are visible via
  ;; `downstream`
  (def a (s/stream))
  (s/map inc a)
  (s/downstream a)

  ;; each element returned by downstream is a 2-tuple, the first element
  ;; describing the connection, and the second element describing the stream
  ;; it's feeding into.

  ;; the value returned by the callback for `connect-via` provides backpressure
  ;; - if a deferred value is returned, further messages will not be passed in
  ;; until the deferred value is realized.


  ;; buffers and backpressure

  ;; we saw above that if we attempt to put a message into a stream, it won't
  ;; succeed until the value is taken out. This is because the default stream
  ;; has no buffer; it simply conveys messages from producers to consumers. If
  ;; we want to create a stream with a buffer, we can simply call (stream
  ;; buffer-size). We can also call (buffer size stream) to create a buffer
  ;; downstream of an existing stream.

  ;; we may also call (buffer metric limit stream), if we don't want to measure
  ;; our buffer's size in messages. If, for instance, each message is a
  ;; collection, we could use count as our metric, and set limit to whatever we
  ;; want the maximum aggregate count to be.

  ;; to limit the rate of messages from a stream, we can use (throttle max-rate
  ;; stream).

  ;; event buses and publish/subscribe models

  ;; manifold provides a simple publish/subscribe mechanism in the
  ;; `manifold.bus` namespace. To create an event bus, we can
  ;; use (`event-bus`). To publish to a particular topic on that bus, we
  ;; use (`publish!` bus topic msg). To get a stream representing all messages
  ;; on a topic, we can call (`subscribe` bus topic).

  ;; calls to `publish!` will return a deferred that won't be realized until all
  ;; streams have accepted the message. By default, all streams returned by
  ;; subscribe are unbuffered, but we can change this by providing a
  ;; stream-generator to event-bus, such as (`event-bus` #(stream 1e3))
)


;; execution

(comment

  (def executor (e/fixed-thread-executor 42))

  @(-> (d/future 1)
       (d/onto executor)
       (d/chain inc inc inc))

  (->> (s/->source (range 1e2))
       (s/onto executor)
       (s/map inc)
       s/stream->seq)
)
