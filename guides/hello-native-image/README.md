# hello-native-image

An example of building the native image from Java classes with GraalVM.

## Compile sources into classes

Java

```
javac -d target/java HelloJavaNativeImage.java
```

Clojure

```
mkdir -p target/clojure && clojure -M -e "(binding [*compile-path* \"target/clojure\"] (compile 'hello-clojure-native-image))"
```

## Build native images

Java

```
native-image -cp target/java HelloJavaNativeImage -o target/hello-java-native-image
```

Clojure

```
native-image --initialize-at-build-time -cp `clojure -Spath` hello_clojure_native_image -o target/hello-clojure-native-image

```

## Run native images

Java

```
target/hello-java-native-image
```

Clojure

```
target/hello-clojure-native-image
```
