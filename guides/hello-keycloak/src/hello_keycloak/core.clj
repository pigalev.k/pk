(ns hello-keycloak.core
  (:require [keycloak.deployment :refer [keycloak-client client-conf] :as deployment]
            [keycloak.admin :as admin]
            [keycloak.user :as user]))

;;;;;;;;;; Setup ;;;;;;;;;;

;; create a connection to Keycloak

(def kc
  "A connection to Keycloak instance."
  (-> (client-conf {:auth-server-url "http://localhost:8080"
                    :realm           "master"
                    :client-id       "admin-cli"})
      (keycloak-client "admin" "topsecret")))


;;;;;;;;;; Administrative tasks ;;;;;;;;;;

;; create a Realm

(admin/create-realm! kc "example-realm")

;; create Clients

(doto kc
    (admin/create-client! "example-realm" (admin/client "myfrontend" true))
    (admin/create-client! "example-realm" (admin/client "mybackend" false)))

;; create Realm Roles

(doto kc
  (admin/create-role! "example-realm" "employee")
  (admin/create-role! "example-realm" "manager"))

;; create Users

(doto kc
  (admin/create-user! "example-realm"
                      "user1"
                      "pwd1")
  (user/create-or-update-user! "example-realm"
                               {:username "bcarter"
                                :first-name "Bob"
                                :last-name "Carter"
                                :password "abcdefg"
                                :email "bcarter@example.com"}
                               ["employee" "manager"] nil))

;; assign Realm Roles to Users

(user/add-realm-roles! kc "example-realm" "bcarter" ["manager"])

;; create Groups

(admin/create-group! kc "example-realm" "mygroup")
(admin/create-group! kc "example-realm" "mygroup1")

;; assign Users to Groups

(comment
  "Do not work, some weird ClassCastException"
  (admin/add-username-to-group-name! kc "example-realm" "mygroup" "bcarter"))

(admin/add-user-to-group! kc
                          "example-realm"
                          "f268bc7a-b837-4aca-998e-469b90a945ee"
                          "1514a910-67bf-4eb7-bf35-14239aea342b")
