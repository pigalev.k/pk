# hello-fulcro

Exploring Fulcro --- a library for development of single-page full-stack web
applications in Clojure(Script).

- https://github.com/fulcrologic/fulcro

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
