Console.WriteLine("Hello, top-level statement!");

// exit code
Console.WriteLine("wait for it...");
await Task.Delay(1000);
Console.WriteLine("Please give an integer to be used as exit code:");
string? s = Console.ReadLine();
int exitCode = int.Parse(s ?? "-1");

Console.WriteLine("Have a nice day!");
return exitCode;
