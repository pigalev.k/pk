﻿using clojure.lang;

Console.WriteLine("Hello, Clojure CLR!");

IFn plus = clojure.clr.api.Clojure.var("clojure.core", "+");
Console.WriteLine($"1 plus 2 plus 3 is {plus.invoke(1, 2, 3)}");
