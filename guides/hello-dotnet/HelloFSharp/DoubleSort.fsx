module DoubleSort =

    // warming up

    let rec find pred xs =
        match xs with
        | [] -> None
        | head :: tail -> if pred head then Some(head) else find pred tail

    let xs = [ 1; 10; 20; -1; -4; 5; -2; 8 ]
    let found = (find (fun x -> x < 0) xs).Value
    printfn "found first negative element in %A: %A" xs found

    // sort negative elements of `ys`, leaving nonnegative ones alone in their places

    let doubleSort ys =
        let sortedNegs =
            List.filter (fun x -> x < 0) ys |> List.sortWith (fun a b -> compare b a)

        let rec step xs negs sorted =
            match xs with
            | head :: tail ->
                if head < 0 then
                    step tail (List.tail negs) ((List.head negs) :: sorted)
                else
                    step tail negs (head :: sorted)
            | [] -> sorted

        List.rev (step ys sortedNegs [])

    let ys = [ 1; 10; 20; -1; -4; 5; -2; 8 ]
    printfn "ys:\n %A" ys
    printfn "doublesorted ys:\n %A" (doubleSort ys)
