module DSLs =

    // a simple DSL for querying dates

    type DateScale =
        | Hour
        | Hours
        | Day
        | Days
        | Week
        | Weeks

    type DateDirection =
        | Ago
        | Hence

    let getDate interval scale direction =
        let absHours =
            match scale with
            | Hour
            | Hours -> interval
            | Day
            | Days -> 24 * interval
            | Week
            | Weeks -> 24 * 7 * interval

        let signedHours =
            match direction with
            | Ago -> -1 * absHours
            | Hence -> absHours

        System.DateTime.Now.AddHours(float signedHours)

    // usage examples
    printfn "now: %A" (getDate 0 Hours Ago)
    printfn "5 days ago: %A" (getDate 5 Days Ago)
    printfn "1 hour hence: %A" (getDate 1 Hour Hence)
