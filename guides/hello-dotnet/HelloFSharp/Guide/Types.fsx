module Types =


    (* https://fsharpforfunandprofit.com/posts/conciseness-type-definitions/ *)

    // algebraic types: composed from simpler ones: product (records), sum (unions)

    // DDD with algebraic types

    type PersonalName = { FirstName: string; LastName: string }


    type StreetAddress =
        { Line1: string
          Line2: string
          Line3: string }

    type ZipCode = ZipCode of string

    type StateAbbrev = StateAbbrev of string

    type ZipAndState = { State: StateAbbrev; Zip: ZipCode }

    type USAddress =
        { Street: StreetAddress
          Region: ZipAndState }

    type UKPostCode = PostCode of string

    type UKAddress =
        { Street: StreetAddress
          Region: UKPostCode }


    type InternationalAddress =
        { Street: StreetAddress
          Region: string
          CountryName: string }


    type Address =
        | USAddress of USAddress
        | UKAddress of UKAddress
        | InternationalAddress of InternationalAddress


    type Email = Email of string


    type CountryPrefix = Prefix of int

    type Phone =
        { CountryPrefix: CountryPrefix
          LocalNumber: string }

    type Contact =
        { PersonalName: PersonalName
          // optional fields
          Address: Address option
          Email: Email option
          Phone: Phone option }


    // putting it all together

    type CustomerAccountId = AccountId of string

    type CustomerType =
        | Prospect
        | Active
        | Inactive

    // an entity type, with custom equality
    [<CustomEquality; NoComparison>]
    type CustomerAccount =
        { CustomerAccountId: CustomerAccountId
          CustomerType: CustomerType
          ContactInfo: Contact }

        override this.Equals(other) =
            match other with
            | :? CustomerAccount as otherCustomer -> (this.CustomerAccountId = otherCustomer.CustomerAccountId)
            | _ -> false

        override this.GetHashCode() = hash this.CustomerAccountId
