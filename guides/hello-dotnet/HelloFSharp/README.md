# hello-fsharp

A guide for F# programming language.

## Getting Started

Run scripts (`.fsx` files):

```
dotnet fsi Hello.fsx
```

TODO: compiling F# projects

## Compiling F# to JS/TS

Install Fable 

```
dotnet tool install fable
```

Compile the source file

```
# to JS
dotnet fable HelloFSharp/Reference/Sequences.fsx 
# to TS
dotnet fable HelloFSharp/Reference/Sequences.fsx --lang typescript
```