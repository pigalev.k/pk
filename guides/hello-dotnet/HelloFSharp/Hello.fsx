#!/usr/bin/env -S dotnet fsi

#load "Reference/Functions.fsx"

// instead of bringing all names in a module directly into the current module
// with `open`, create a short alias for it
module Fns = Functions.Functions

module Hello =

    (* F# core library docs: https://fsharp.github.io/fsharp-core-docs/ *)

    open System

    let t: Type = typeof<int>

    printfn "Hello, F# script!"
    printfn "type %A resides in assembly %A" t t.Assembly.FullName

    // using values and functions from other modules

    printfn "hello Functions module: cube of 8.0 is %.0f" (Functions.Functions.cube 8.0)

    printfn "hello Functions module again: square (%A) of 8 is %d" Fns.square (Fns.square 8)
