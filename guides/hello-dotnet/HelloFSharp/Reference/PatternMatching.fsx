module PatternMatching =

    (* https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/pattern-matching *)

    // a record of person
    type Person = { First: string; Last: string }

    // a discriminated union of 3 different kinds of employees
    type Employee =
        | Engineer of engineer: Person
        | Manager of manager: Person * reports: List<Employee>
        | Executive of executive: Person * reports: List<Employee> * assistant: Employee

    // counts everyone underneath the employee in the management hierarchy,
    // including the employee
    //
    // the matches bind names to the properties
    // of the cases so that those names can be used inside the match branches;
    // note that the names used for binding do not need to be the same as the
    // names given in the DU definition
    let rec countReports (emp: Employee) =
        1
        + match emp with
          | Engineer(person) -> 0
          // _ matches anything
          | Manager(_, reports) -> reports |> List.sumBy countReports
          | Executive(_, reports, assistant) -> (reports |> List.sumBy countReports) + countReports assistant
          // _ matches anything, not necessary here
          | _ -> 0


    let dave =
        Manager(
            manager = { First = "Dave"; Last = "Dangerous" },
            reports = [ Engineer(engineer = { First = "Al"; Last = "Cunning" }) ]
        )

    printfn "Dave is %A" dave

    // active patterns

    open System

    // shorthand function construct for pattern matching,
    //useful when writing functions which make use of partial application
    let private parseHelper (f: string -> bool * 'T) =
        f
        >> function
            | (true, item) -> Some item
            | (false, _) -> None

    let parseDateTimeOffset = parseHelper DateTimeOffset.TryParse

    let result = parseDateTimeOffset "1970-01-01"

    match result with
    | Some dto -> printfn "It parsed!"
    | None -> printfn "It didn't parse!"

    // define some more functions which parse with the helper function
    let parseInt = parseHelper Int32.TryParse
    let parseDouble = parseHelper Double.TryParse
    let parseTimeSpan = parseHelper TimeSpan.TryParse

    let (|Int|_|) = parseInt
    let (|Double|_|) = parseDouble
    let (|Date|_|) = parseDateTimeOffset
    let (|TimeSpan|_|) = parseTimeSpan

    // pattern matching via 'function' keyword and active patterns often looks like this
    let printParseResult =
        function
        | Int x -> printfn $"%d{x}"
        | Double x -> printfn $"%f{x}"
        | Date d -> printfn $"%O{d}"
        | TimeSpan t -> printfn $"%O{t}"
        | _ -> printfn "Nothing was parse-able!"

    // Call the printer with some different values to parse.
    printParseResult "12"
    printParseResult "12.045"
    printParseResult "12/28/2016"
    printParseResult "9:01PM"
    printParseResult "banana!"

module Options =

    // a zip code defined via single-case discriminated union
    type ZipCode = ZipCode of string

    // define a type where ZipCode is optional
    type Customer = { ZipCode: ZipCode option }

    // define an interface type that represents an object to compute the shipping zone for the customer's zip code,
    // given implementations for the 'getState' and 'getShippingZone' abstract methods
    type IShippingCalculator =
        abstract GetState: ZipCode -> string option
        abstract GetShippingZone: string -> int

    // calculate a shipping zone for a customer using a calculator instance;
    // this uses combinators in the Option module to allow a functional pipeline for
    // transforming data with Optionals
    let CustomerShippingZone (calculator: IShippingCalculator, customer: Customer) =
        customer.ZipCode
        |> Option.bind calculator.GetState
        |> Option.map calculator.GetShippingZone

// TODO: add usage example

module UnitsOfMeasure =

    //  open a collection of common unit names
    open Microsoft.FSharp.Data.UnitSystems.SI.UnitNames

    // define a unitized constant
    let sampleValue1 = 1600.0<meter>

    // define a new unit type
    [<Measure>]
    type mile =
        // conversion factor mile to meter
        static member asMeter = 1609.34<meter / mile>

    // define a unitized constant
    let sampleValue2 = 500.0<mile>

    // compute metric-system constant
    let sampleValue3 = sampleValue2 * mile.asMeter

    // values using units of measure can be used just like the primitive numeric type for things like printing
    printfn $"After a %f{sampleValue1} race I would walk %f{sampleValue2} miles which would be %f{sampleValue3} meters"

module ActivePatterns =

    // defining custom matchers? conformers?

    let (|Even|Odd|) input = if input % 2 = 0 then Even else Odd

    let TestNumber input =
        match input with
        | Even -> printfn "%d is even" input
        | Odd -> printfn "%d is odd" input

    TestNumber 7
    TestNumber 11
    TestNumber 32

    // partial active patterns: return option types

    let (|Integer|_|) (str: string) =
        let mutable intvalue = 0

        if System.Int32.TryParse(str, &intvalue) then
            Some(intvalue)
        else
            None

    let (|Float|_|) (str: string) =
        let mutable floatvalue = 0.0

        if System.Double.TryParse(str, &floatvalue) then
            Some(floatvalue)
        else
            None

    let parseNumeric str =
        match str with
        | Integer i -> printfn "%d : Integer" i
        | Float f -> printfn "%f : Floating point" f
        | _ -> printfn "%s : Not matched." str

    parseNumeric "1.1"
    parseNumeric "0"
    parseNumeric "0.0"
    parseNumeric "10"
    parseNumeric "Something else"
