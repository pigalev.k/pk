module Strings =

    (* https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/strings *)

    let s = "Hello, F#!"

    // indexing
    printfn "indexing: %c %c" s[1] s.[1]

    // slicing (both indices included)
    printfn "slicing: %s %s" s[1..3] s[7..8]

    // concatenating
    printfn "concatenating: %s" ("foo" + "bar")

    // interpolating; can use typed format specifiers;
    // printf- (%s) or .NET-style ({val:specifier}) format specifiers are supported
    let x = 12.34
    printfn $"interpolating: {s} {x}"
    printfn $"interpolating: %.1f{x}"
    printfn $"""interpolating: {"embedded string literal"}"""
    // extended interpolation: as many braces as $s are required to start interpolating
    printfn $$"""interpolating: %%.3f{{x + 12.1}} {{1:D}}"""
    let data = [ 0..4 ]
    printfn $"interpolating: the data is %A{data}"
    // aligning
    printfn $"""|{"Left", -7}|{"Right", 7}|"""
