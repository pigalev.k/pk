module Sequences =

    // logical series of elements of the same type; lazy

    let seq1 = Seq.empty

    let seq2 =
        seq {
            yield "hello"
            yield "world"
            yield "and"
            yield "hello"
            yield "world"
            yield "again"
        }

    let numbersSeq = seq { 1..1000 }

    let seq3 =
        seq {
            for word in seq2 do
                if word.Contains("l") then
                    yield word
        }

    let evenNumbers = Seq.init 1001 (fun n -> n * 2) |> Seq.truncate 3 |> Seq.toList

    printfn "some even numbers: %A" evenNumbers

    let oddNumbers =
        evenNumbers |> Seq.map (fun x -> x + 1) |> Seq.truncate 3 |> Seq.toList

    printfn "some odd numbers: %A" oddNumbers

    let rnd = System.Random()

    // an infinite sequence --- a random walk
    let rec randomWalk x =
        seq {
            yield x
            yield! randomWalk (x + rnd.NextDouble() - 0.5)
        }

    // the first 100 elements of the random walk
    let first100ValuesOfRandomWalk = randomWalk 5.0 |> Seq.truncate 100 |> Seq.toList

    printfn $"First 100 elements of a random walk: {first100ValuesOfRandomWalk}"
