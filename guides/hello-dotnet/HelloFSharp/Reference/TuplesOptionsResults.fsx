module Tuples =

    (* https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/tuples *)

    let tuple1 = (1, 2, 3)

    let swapElems (a, b) = (b, a)

    printfn $"The result of swapping (1, 2) is {(swapElems (1, 2))}"

    let tuple2 = (1, "fred", 3.1415)

    printfn $"tuple1: {tuple1}\ttuple2: {tuple2}"

    // struct tuples (normally they are reference types)
    let sampleStructTuple = struct (1, 2)

    let convertFromStructTuple (struct (a, b)) = (a, b)
    let convertToStructTuple (a, b) = struct (a, b)

    printfn
        $"struct tuple: {sampleStructTuple}\nreference tuple made from the struct tuple: {(sampleStructTuple |> convertFromStructTuple)}"

    let tuple3 = (4, 8)
    printfn "first element of tuple2 is %d, second element is %d" (fst tuple3) (snd tuple3)

module Options =

    (* https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/options *)

    // an option has an underlying type; it can hold a value of that type, or it might not have a value

    let keepIfPositive (a: int) = if a > 0 then Some(a) else None

    // matching options
    let exists (x: int option) =
        match x with
        | Some(x) -> true
        | None -> false

    printfn "number exists: %A" (keepIfPositive 5 |> exists)
    printfn "number exists: %A" (keepIfPositive -1 |> exists)
