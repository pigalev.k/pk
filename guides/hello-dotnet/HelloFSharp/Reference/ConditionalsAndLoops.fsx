module Conditionals =
    let test x y =
        if x = y then "equals"
        elif x < y then "is less than"
        else "is greater than"

    printfn "%d %s %d." 10 (test 10 20) 20

    let age = System.Int32.Parse "2"

    if age < 10 then
        printfn "You are only %d years old and already learning F#? Wow!" age

module Loops =

    // loop over iterables

    // looping over a list
    let list1 = [ 1; 5; 100; 450; 788 ]

    for i in list1 do
        printfn "%d" i

    // looping over a sequence (with tuple pattern matching)
    let seq1 = seq { for i in 1..10 -> (i, i * i) }

    for (a, asqr) in seq1 do
        printfn "%d squared is %d" a asqr

    // looping over a range
    for i in 1..3 do
        printfn "range element %A" i

    // binding to wildcard character to ignore the value
    let mutable count = 0

    for _ in list1 do
        count <- count + 1

    printfn "Number of elements in list1: %d" count


    // loop over integers only

    for i = 1 to 3 do
        printf "integer: %d " i

    printfn ""

    // loop until condition is met

    let mutable i = 0

    while i < 3 do
        printfn "while loop counter: %A" i
        i <- i + 1
