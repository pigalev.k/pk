module Bindings =

    (*
        https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/values/
        https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/functions/let-bindings
    *)

    open System

    // any expression can be used as an initializer
    // immutable by default
    let a: int64 = 1
    let b = 100u
    let str = "text"
    printfn "binding values: %d %d %s" a b str

    // explicitly mutable
    let mutable x = 123
    x <- x + 1
    printfn "mutating variables: %d" x

    // binding multiple values (tuples)
    let i, j, k = (1, 2, 3)
    printfn "binding tuples: %d %d %d" i j k

    // using body expressions as initializers
    let result =
        let i, j, k = (4, 5, 6)
        // body expression (that contains names)
        i + 2 * j + 3 * k

    printfn "using body expressions: %d" result

    // function bindings
    let inc (x: uint) = x + 1u
    // parameters are patterns (e.g., tuples)
    let add (a, b) = a + b

    [<Obsolete>]
    let product =
        let fn (a, b) = a * b
        100 * fn (2, 3)

    printfn "using function bindings: %d %d %d" (inc 1u) (add (2, 3)) product

    // do bindings: executing code for side-effects
    let print = fun x -> printfn "executing code without binding values: printing %A" x
    do print [ 1; 2; 3 ]
