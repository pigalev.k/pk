module Async =

    (* https://learn.microsoft.com/en-us/dotnet/fsharp/tutorials/async *)

    open System
    open System.IO

    // asynchronous computations are specifications of work
    // rather than a representation of work that is already executing,
    // so they must be explicitly started

    // there is no affinity between an asynchronous computation and a thread,
    // unless explicitly started on the current thread

    // perform an asynchronous read of a file using 'async';
    // returns Async<unit> (like a Promise<void> in JS/TS?)
    let printTotalFileBytesAsync (path: string) =
        async {
            // another async operation, should be awaited
            let! bytes = File.ReadAllBytesAsync(path) |> Async.AwaitTask
            let fileName = Path.GetFileName(path)
            printfn $"using async: file {fileName} has %d{bytes.Length} bytes"
        }

    // asynchronous computations must be explicitly started to actually execute
    printTotalFileBytesAsync "Reference/Async.fsx" |> Async.RunSynchronously

    // multiple ways to start an async computation
    // - Async.StartChild (common cancellation token)
    // - Async.StartImmediate (on the current thread)
    // - Async.StartAsTask (in a thread pool)
    // - Async.Parallel
    // - Async.Sequential
    // - Async.AwaitTask (wait for the given task to complete and return its result as Async<'T>)
    // - Async.Catch (returns result or exception)
    // - Async.Ignore (executes and drops the result)
    // - Async.RunSynchronously (runs on the current thread and blocks awaiting)
    // - Async.Start (fire-and-forget in a thread pool, drops the result, swallows exceptions)


    // usage example: dotnet fsi Reference/Async.fsx Hello.fsx Reference/Classes.fsx Reference/Functions.fsx
    let argv = Environment.GetCommandLineArgs()
    printfn "got command-line arguments: %A" argv

    if argv.Length < 3 then
        printfn "usage: %s %s <file> ..." argv[0] argv[1]
    else
        argv[2 .. argv.Length - 1]
        |> Seq.map printTotalFileBytesAsync
        // output will be interleaved, so run sequentially
        // |> Async.Parallel
        |> Async.Sequential
        |> Async.Ignore
        |> Async.RunSynchronously

module Tasks =

    open System
    open System.IO

    let printTotalFileBytesUsingTasks (path: string) =
        task {
            let! bytes = File.ReadAllBytesAsync(path)
            let fileName = Path.GetFileName(path)
            printfn $"using tasks: file {fileName} has %d{bytes.Length} bytes"
        }

    let task = printTotalFileBytesUsingTasks "Hello.fsx"
    task.Wait()
