module Exceptions = 

    exception MyError of string

    let oops =
        // try..with and try..finally are different blocks and should be nested if 
        // both are needed
        try
            try 
                raise (MyError("Oopsie!"))
            with 
                | MyError(str) -> printfn "Error was raised and handled: %s" str
                | :? System.DivideByZeroException -> printfn "Division by zero!"
                | _ -> printfn "some weird things occurred, beware"
        finally 
            printfn "And finally, we are done"

    oops
 
    // `use` and `using`: like try-with-resources in Java
    open System.IO

    let readLineFromFile filename =
        use file = File.OpenText(filename)
        let s = file.ReadLine()
        printfn "the line just read: %s" s
        // file.Dispose() is called implicitly here

    readLineFromFile "Hello.fsx"

    let readLineFromFileAgain (file1 : System.IO.StreamReader) =
        let s = file1.ReadLine();
        printfn "and the same line read again: %s" s

    using (System.IO.File.OpenText("Hello.fsx")) readLineFromFileAgain

    // `failwith`: generates a Failure (descendant of System.Exception)

    let divide x y =
        if (y = 0) then failwith "Divisor cannot be zero."
        else x / y

    let testDivideFailwith x y =
        try
            divide x y
        with
            | Failure(msg) -> printfn "%s" msg; 0

    testDivideFailwith 100 0

    // `assert`: debug-mode assertions (DEBUG constant must be set), cannot be caught

    let DEBUG = true

    let subtractUnsigned (x : uint32) (y : uint32) =
        assert (x > y)
        let z = x - y
        z
    // will not generate an assertion failure
    let result1 = subtractUnsigned 2u 1u
    // will generate an assertion failure
    let result2 = subtractUnsigned 1u 2u
