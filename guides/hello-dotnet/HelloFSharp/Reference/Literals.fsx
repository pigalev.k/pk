module Literals =

    (* https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/literals *)

    // numbers
    // underscores allowed

    // sbyte, ubyte, int16, uint16, int (int32), uint (uint32), nativeint, unativeint, int64, uint64, bigint
    // other bases: 0b, 0o, 0x

    printfn
        "integers: %d %d %d %d %d %d %d %d %d %d %A"
        86y
        86uy
        8_6s
        86us
        86l
        86ul
        86n
        0x00002D3Fun
        86L
        86UL
        9999999999999999999999999999I

    // single (float32), double (float), decimal
    // infinities supported
    printfn "floating-point numbers: %f %f %.2f %f" 4.14f 4.14 0.78_73m -infinity

    // characters and strings

    // Char (unicode char), byte (ASCII char)
    printfn "characters: %c %c %d" 'a' '\u4361' 'a'B

    // String (unicode), byte[] (array of ASCII chars)

    printfn
        "strings: %s %s %s %s %A %A"
        "\U0001F47Dtext\n" // supports escape sequences
        @"c:\filename" // verbatim string (no escape sequences)
        """<book title="Paradise Lost">""" // multiline string (no escape sequences, can contain double quotes)
        ("string1:" + "string2")

        "ASCII"B // supports escape sequences, except unicode ones
        @"\\server\share"B

module NamedLiterals =
    // evaluated at compile-time; recommended to capitalize the name
    [<Literal>]
    let MyNamedLiteral = "a" + "b"

    printfn "named literal: %A" MyNamedLiteral
