module Generics =

    // implicitly generic function (automatically generalized)
    let makeList x y = [ x; y ]

    // explicitly generic functions (different syntax, same result)
    let makeList1 (x: 'a) (y: 'a) = [ x; y ]
    let makeList2<'a> (x: 'a) (y: 'a) = [ x; y ]

    // wilcard type arguments: explicitly tell compiler to infer the type
    let printSequence (sequence1: Collections.seq<_>) =
        Seq.iter (fun elem -> printf "%s " (elem.ToString())) sequence1

    // instantiating (constructing) the type
    printSequence (makeList2<int64> 1 2)
