(* https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/functions/ *)

module Functions =

    let square x = x * x
    let cube = fun x -> x * x * x

    printfn "square of %A is %A" 5 (square 5)
    printfn "cube of %A is %A" 5 (cube 5.0)
    printfn $"square of {12} is {square (12)}, and cube is {cube (12)}"


module RecursiveFunctions =

    let rec factorial n =
        if n = 0 then 1 else n * factorial (n - 1)

    printfn $"Factorial of 6 is: %d{factorial 6}"

    // TCO: since all of the recursive calls are tail calls,
    // the compiler will turn the function into a loop

    let rec greatestCommonFactor a b =
        if a = 0 then b
        elif a < b then greatestCommonFactor a (b - a)
        else greatestCommonFactor (a - b) b

    printfn $"The Greatest Common Factor of 300 and 620 is %d{greatestCommonFactor 300 620}"

    // 'sumList' is not tail-recursive
    [<TailCall>] // should produce a compile-time warning, but no
    let rec sumList xs =
        match xs with
        | [] -> 0
        | y :: ys -> y + sumList ys

    //  makes 'sumList' tail recursive
    let rec private sumListTailRecHelper accumulator xs =
        match xs with
        | [] -> accumulator
        | y :: ys -> sumListTailRecHelper (accumulator + y) ys

    let sumListTailRecursive xs = sumListTailRecHelper 0 xs

    let oneThroughTen = [ 1; 2; 3; 4; 5; 6; 7; 8; 9; 10 ]

    printfn $"The sum 1-10 is %d{sumListTailRecursive oneThroughTen}"

module InlineFunctions =

    let inline printAsFloatingPoint number = printfn "%f" (float number)

    printfn "calling inline function: %d as float is" 12
    printAsFloatingPoint 12
