(* https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/fsharp-collection-types *)

module Lists =

    // linked lists

    // literal lists

    let list1 = []
    let list2 = [ 1; 2; 3 ]
    // also can separate elements by placing them on their own lines (autoformatter prevents this here)
    let list3 = [ 1; 2; 3 ]
    printfn "literal lists: %A %A %A" list1 list2 list3

    // expressions evaluated to lists

    let numberList = [ 1..1000 ]

    let daysList =
        [ for month in 1..12 do
              for day in 1 .. System.DateTime.DaysInMonth(2017, month) do
                  yield System.DateTime(2017, month, day) ]

    printfn "expressions evaluated to lists: %A %A" numberList daysList
    printfn $"the first 5 days of 2017 are: {daysList |> List.take 5}"

    let blackSquares =
        [ for i in 0..7 do
              for j in 0..7 do
                  if (i + j) % 2 = 1 then
                      yield (i, j) ]

    printfn "the list of black chessboard squares: %A" blackSquares


    // using list combinators

    let squares = numberList |> List.map (fun x -> x * x)

    let sumOfSquares =
        numberList |> List.filter (fun x -> x % 3 = 0) |> List.sumBy (fun x -> x * x)

    printfn $"the sum of the squares of numbers up to 1000 that are divisible by 3 is: %d{sumOfSquares}"


module Arrays =

    // fixed-size, mutable collections of elements of the same type, support fast random access

    let array1 = [||]
    let array2 = [| "hello"; "world"; "and"; "hello"; "world"; "again" |]
    let array3 = [| 1..1000 |]

    let array4 =
        [| for word in array2 do
               if word.Contains("l") then
                   yield word |]

    let evenNumbers = Array.init 1001 (fun n -> n * 2)
    let evenNumbersSlice = evenNumbers[0..500]

    for word in array4 do
        printfn $"word: {word}"

    array2[1] <- "WORLD!"

    let sumOfLengthsOfWords =
        array2
        |> Array.filter (fun x -> x.StartsWith "h")
        |> Array.sumBy (fun x -> x.Length)

    printfn $"The sum of the lengths of the words in Array 2 is: %d{sumOfLengthsOfWords}"
