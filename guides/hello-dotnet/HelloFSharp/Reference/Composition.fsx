#load "Functions.fsx"

module Fns = Functions.Functions

module Composition =

    let invert x = -x
    let dec x = x - 1
    let inc x = x + 1

    // applying a function partially

    let incrementAll = Seq.map inc

    printfn "incremented all elements: %A" (incrementAll [ 1; 2; 3; 4; 5 ])


    // piping the value of previous expression to the next one

    let oddSquares xs =
        Seq.map Fns.square xs |> Seq.filter (fun x -> x % 2 <> 0)

    // using seq functions allows to pass any sequential collection to `oddSquares`
    printfn "odd squares: %A" (oddSquares [ 1; 2; 3; 4; 5 ])
    printfn "odd squares: %A" (oddSquares [| 1; 2; 3; 4; 5 |])

    // composing two (or more) functions

    let squareAndInvert = Fns.square >> invert

    printfn "inverted square of %A: %A" 12 (squareAndInvert 12)
    printfn "inverted square of %A, decremented by 1: %A" 12 ((squareAndInvert >> dec) 12)

    let listOfFunctions =
        [ Fns.square
          inc
          (fun x ->
              printfn "result of squaring, negating and incrementing: %A" x
              x) ]

    let allFunctions = List.reduce (>>) listOfFunctions

    allFunctions 5
