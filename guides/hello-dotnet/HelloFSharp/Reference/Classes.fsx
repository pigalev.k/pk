(* https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/classes *)

module DefiningClasses =

    // a simple two-dimensional Vector class
    //
    // the class's constructor is on the first line,
    // and takes two arguments: dx and dy, both of type 'double'
    type Vector2D(dx: double, dy: double) =

        // this internal field stores the length of the vector, computed when the
        // object is constructed
        let length = sqrt (dx * dx + dy * dy)

        // 'this' specifies a name for the object's self-identifier;
        // in instance methods, it must appear before the member name

        // properties
        member this.DX = dx
        member this.DY = dy
        member this.Length = length

        // method
        member this.Scale(k) = Vector2D(k * this.DX, k * this.DY)

    // instantiate the Vector2D class
    let vector1 = Vector2D(3.0, 4.0)

    // get a new scaled vector object, without modifying the original object
    let vector2 = vector1.Scale(10.0)

    printfn $"Length of vector1: %f{vector1.Length}\nLength of vector2: %f{vector2.Length}"

module DefiningGenericClasses =

    type StateTracker<'T>(initialElement: 'T) =

        // this internal field store the states in a list
        let mutable states = [ initialElement ]

        // add a new element to the list of states
        member this.UpdateState newState = states <- newState :: states

        // get the entire list of historical states
        member this.History = states

        // get the latest state
        member this.Current = states.Head

    // an 'int' instance of the state tracker class; note that the type parameter is inferred
    let tracker = StateTracker 10

    // add a state
    tracker.UpdateState 17
    tracker.UpdateState 123

    printfn "state tracker's current value is %A, history of states is %A" tracker.Current tracker.History

module ImplementingInterfaces =

    // a type that implements IDisposable
    type ReadFile() =

        let file = new System.IO.StreamReader("readme.txt")

        member this.ReadLine() = file.ReadLine()

        // implementation of IDisposable members
        interface System.IDisposable with
            member this.Dispose() = file.Close()


    // an object that implements IDisposable via an Object Expression;
    // unlike other languages such as C# or Java, a new type definition is not needed
    // to implement an interface
    let interfaceImplementation =
        { new System.IDisposable with
            member this.Dispose() = printfn "disposed" }
