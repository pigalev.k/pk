module Records =

    (* https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/records *)

    // fundamental data type, like POCOs/POJOs
    // structural equality semantics

    // define
    type ContactCard =
        { Name: string
          Phone: string
          Verified: bool }

    // instantiate; can be done on the same line with ';' separators (autoformatter prevents this here)
    let contact1 =
        { Name = "Alf"
          Phone = "(206) 555-0157"
          Verified = false }

    // "copy-and-update"
    let contact2 =
        { contact1 with
            Phone = "(206) 555-0112"
            Verified = true }

    let showContactCard (c: ContactCard) =
        c.Name + " Phone: " + c.Phone + (if not c.Verified then " (unverified)" else "")

    printfn $"Alf's Contact Card: {showContactCard contact1}"

    // a eecord with a member
    type ContactCardAlternate =
        { Name: string
          Phone: string
          Address: string
          Verified: bool }

        /// members can implement object-oriented members
        member this.PrintedContactCard =
            this.Name
            + " Phone: "
            + this.Phone
            + (if not this.Verified then " (unverified) " else "")
            + this.Address

    let contactAlternate =
        { Name = "Alf"
          Phone = "(206) 555-0157"
          Verified = false
          Address = "111 Alf Street" }

    printfn $"Alf's alternate contact card is {contactAlternate.PrintedContactCard}"

    // a value type
    [<Struct>]
    type ContactCardStruct =
        { Name: string
          Phone: string
          Verified: bool }


module DiscriminatedUnions =

    (* https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/discriminated-unions *)

    // means of type combination

    // represents the suit of a playing card
    type Suit =
        | Hearts
        | Clubs
        | Diamonds
        | Spades

    // represents the rank of a playing card
    type Rank =
        // represents the rank of cards 2 .. 10
        | Value of int
        | Ace
        | King
        | Queen
        | Jack

        // discriminated unions can also implement object-oriented members
        static member GetAllRanks() =
            [ yield Ace
              for i in 2..10 do
                  yield Value i
              yield Jack
              yield Queen
              yield King ]

    type Card = { Suit: Suit; Rank: Rank }

    // a list representing all the cards in the deck
    let fullDeck =
        [ for suit in [ Hearts; Diamonds; Clubs; Spades ] do
              for rank in Rank.GetAllRanks() do
                  yield { Suit = suit; Rank = rank } ]

    // converts a 'Card' object to a string
    let showPlayingCard (c: Card) =
        let rankString =
            match c.Rank with
            | Ace -> "Ace"
            | King -> "King"
            | Queen -> "Queen"
            | Jack -> "Jack"
            | Value n -> string n

        let suitString =
            match c.Suit with
            | Clubs -> "clubs"
            | Diamonds -> "diamonds"
            | Spades -> "spades"
            | Hearts -> "hearts"

        rankString + " of " + suitString

    // prints all the cards in a playing deck
    let printAllCards () =
        for card in fullDeck do
            printfn $"{showPlayingCard card}"

    printAllCards ()

    // single-case DUs, often used for domain modeling
    // cannot be implicitly converted to or from the type they wrap
    type Address = Address of string
    type Name = Name of string
    type SSN = SSN of int


    let address = Address "111 Alf Way"
    let name = Name "Alf"
    let ssn = SSN 1234567890

    // unwrapping the underlying value with a simple function
    let unwrapAddress (Address a) = a
    let unwrapName (Name n) = n
    let unwrapSSN (SSN s) = s

    printfn $"Address: {address |> unwrapAddress}, Name: {name |> unwrapName}, and SSN: {ssn |> unwrapSSN}"

    // DUs support recursive definitions

    // represents a Binary Search Tree (BST), with one case being the Empty tree,
    // and the other being a Node with a value and two subtrees
    type BST<'T> =
        | Empty
        | Node of value: 'T * left: BST<'T> * right: BST<'T>

    // returns true if the item exists in the Binary Search Tree
    let rec exists item bst =
        match bst with
        | Empty -> false
        | Node(x, left, right) ->
            if item = x then true
            elif item < x then (exists item left)
            else (exists item right)

    // inserts an item in the Binary Search Tree
    // finds the place to insert recursively using pattern matching, then inserts a new node;
    // if the item is already present, does not insert anything
    let rec insert item bst =
        match bst with
        | Empty -> Node(item, Empty, Empty)
        | Node(x, left, right) as node ->
            if item = x then node // no need to insert, it already exists; return the node
            elif item < x then Node(x, insert item left, right) // call into left subtree
            else Node(x, left, insert item right) // call into right subtree
