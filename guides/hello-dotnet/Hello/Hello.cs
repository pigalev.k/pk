class Hello
{
    public static int Main(string[] args)
    {
        Console.WriteLine("Hello, .Net!");
        if (args == null || args.Length == 0)
        {
            Console.WriteLine("no args");
        }
        else
        {
            Console.WriteLine($"args: {string.Join(" ", args)}");
        }

        return 0;
    }
}
