(ns hello-reaver.core
  (:require
   [reaver :as r :refer [parse extract-from text attr]]
   [clojure.spec-alpha2 :as s]
   [clojure.data.json :as json]
   [clojure.edn :as edn]
   [clojure.string :as str :refer [join]]
   [expound.alpha :as e]
   [tick.core :as t]))


;; helper functions for conformers

(defn left-zero-pad
  "Pad string of length 1 with one zero at left side."
  [string]
  (if (= (count string) 1)
    (str "0" string)
    string))

(defn date-string
  "Make sure that date string is in the format parseable by tick/parse."
  [string]
  (->> (re-seq #"\d+" string)
       (map left-zero-pad)
       (join "-")))


(comment

  ;; spec for the comic data

  (s/def ::name string?)
  (s/def ::id int?)
  (s/def ::->id
    (s/conformer
     (fn [value]
       (if-let [id (edn/read-string (re-find #"\d+" value))]
         id
         ::s/invalid))))
  (s/def ::->date
    (s/conformer
     (fn [value]
       (try
         (t/date (date-string value))
         (catch Exception e
           ::s/invalid)))))

  (s/def ::comic (s/schema {:id ::->id :name ::name :date ::->date}))
  (s/def ::comics (s/coll-of ::comic))


  ;; fetch, parse and extract

  (def xkcd-url "https://xkcd.com")
  (def archive-url "/archive")

  (defonce archive-page (slurp (str xkcd-url archive-url)))
  (def archive (parse archive-page))

  (def comics (extract-from archive "#middleContainer a"
                            [:id :name :date]
                            "a" (attr :href)
                            "a" text
                            "a" (attr :title)))

  (def comic (first comics))


  ;; validation

  (s/valid? ::->id (:id comic))
  (s/valid? ::name (:name comic))
  (s/valid? ::->date (:date comic))

  (s/valid? ::comic comic)
  (s/explain ::comic comic)

  (s/valid? ::comics comics)
  (s/explain ::comics comics)


  (s/conform ::name (:name comic))
  (s/conform ::->id (:id comic))
  (s/conform ::->date (:date comic))

  (s/def :user/name string?)
  (e/expound string? 1)
  (e/expound string? comic)

  (def c (s/conform ::comics comics))


  ;; partial (context-dependent) schema with `s/select`

  (s/def ::comic-without-date (s/select ::comic [{:id ::->id :name ::name}]))
  (s/def ::comics-without-date (s/coll-of ::comic-without-date))
  (def cwd (s/conform ::comics-without-date comics))


  (defn get-comic-info
    "Get JSON representation of comic by comic ID."
    [id]
    (try
      (json/read-str (slurp (str xkcd-url "/" id "/info.0.json")))
      (catch Exception e)))

)
