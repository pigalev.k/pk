# hello-reaver

Exploring Reaver --- a Clojure library for extracting data from HTML.

- https://github.com/mischov/reaver

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
