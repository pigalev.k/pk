# hello-buddy

Exploring Buddy --- a security library for Clojure.

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
