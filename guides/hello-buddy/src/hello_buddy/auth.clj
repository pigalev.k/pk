(ns hello-buddy.auth
  (:require
   [buddy.auth :as auth :refer [authenticated? throw-unauthorized]]
   [buddy.auth.accessrules
    :as accessrules :refer [error restrict success wrap-access-rules]]
   [buddy.auth.backends :as backends]
   [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
   [buddy.core.keys :as k]
   [buddy.sign.jwt :as jwt]
   [cheshire.core :as json]
   [ring.util.response :refer [response]])
  (:import
   (java.util Base64)))


;; Authentication and Authorization facilities for Ring-based web applications.

;; http://funcool.github.io/buddy-auth/latest/index.html


;; authentication

(comment

  ;; the buddy's approach for authentication is pretty simple and explicit. In
  ;; contrast to the vast majority of authentication libraries that I know,
  ;; buddy does not mix authentication process with the authorization.

  ;; it is implemented as a pluggable backend that can be picked as is or you
  ;; can implement a new one with simple steps. This is a list of builtin
  ;; backends:
  ;;   backend name   namespace
  ;; - HTTP Basic:    `buddy.auth.backends/basic`
  ;; - Session:       `buddy.auth.backends/session`
  ;; - Token:         `buddy.auth.backends/token`
  ;; - Signed JWT:    `buddy.auth.backends/jws`
  ;; - Encrypted JWT: `buddy.auth.backends/jwe`

  ;; if you are not happy with the built-in backends, you can implement your own
  ;; and use it with `buddy-auth` middleware without any problems.

  ;; the authentication process works mainly in two steps:

  ;; - **parse**: that is responsible for analyzing the request and read the auth
  ;;   related data (e.g. Authorization header, url params, etc..)
  ;; - **auth**: with the data just obtained from parse step try to authenticate
  ;;   the the request (e.g. simple access to database for obtain the possible
  ;;   user, using a self contained jws/jwe token, check a key in the session,
  ;;   etc…)

  ;; this step does not raise any exceptions and is completely transparent to
  ;; the user. The authentication process responsibility is to determine if a
  ;; request is anonymous or is authenticated, nothing more.


  ;; backends


  ;; HTTP-Basic

  ;; the HTTP Basic authentication backend is one of the simplest and most
  ;; insecure authentication systems, but is a good first step to understanding
  ;; how `buddy-auth` authentication works.

  ;; a simple ring handler. This can also be a compojure router handler or
  ;; anything else compatible with ring middleware.

  (defn my-handler
    "Uses `:identity` value in request to decide how to handle it."
    [request]
    (if (:identity request)
      (response (format "Hello %s" (:identity request)))
      {:status 403 :body "Forbidden"}))

  ;; the basic step to check if a request is authenticated or not, is just to
  ;; check if it comes with an `:identity` key and it contains a logical
  ;; true (exists and contains something different to nil or false).

  ;; this is how the authentication backend should be setup:

  (defn my-authfn
    "Receives credentials extracted from request headers by buddy middleware and
  produces an identity value, that will be placed in the request under
  `:identity` key."
    [request credentials]
    (let [username (:username credentials)
          password (:password credentials)]
      username))

  (def backend (backends/basic {:realm "MyApi"
                                :authfn my-authfn}))

  ;; the `:authfn` is responsible for the second step of authentication. It
  ;; receives the parsed auth data from request and should return a logical true
  ;; value (e.g a user id, user instance, mainly something different to nil and
  ;; false). And it will be called only if step 1 (parse) returns something.

  ;; and finally, you should wrap your ring handler with authentication and
  ;; authorization middleware:

  ;; define the main handler with *app* name wrapping it with authentication
  ;; middleware using an instance of the just created http-basic backend.

  ;; define app var with handler wrapped with `buddy-auth`'s authentication and
  ;; authorization middleware using the previously defined backend.

  (def app (-> my-handler
               (wrap-authentication backend)
               (wrap-authorization backend)))

  ;; from now, all requests that reach `my-handler` will be properly
  ;; authenticated.

  ;; the process of authentication of this backend consists in parsing the
  ;; “Authorization” header, extracting the credentials and, in case the
  ;; credentials were extracted successfully, call the `authfn` with the
  ;; credentials map.

  ;; authorization header example:
  ;; "Authorization: Basic am9lOnNlY3JldA=="

  (defn str->b64 [string]
    (.encodeToString (Base64/getEncoder) (.getBytes string)))

  (def username "joe")
  (def password "secret")
  (def credentials (str->b64 (str username ":" password)))

  ;; unauthenticated
  (app {:uri "/hello" :body "HAYO!"})
  ;; identity added manually
  (app {:uri "/hello" :body "HAYO!" :identity "molly"})
  ;; proper basic auth with valid credentials
  (app {:uri "/hello"
        :body "HAYO!"
        :headers {"authorization" (str "Basic " credentials)}})


  ;; session

  ;; the session backend has the simplest implementation because it relies
  ;; entirely on Ring session support.

  ;; the authentication process of this backend consists of checking the
  ;; `:identity` keyword in session. If it exists and is a logical true, it is
  ;; automatically forwarded to the request under the `:identity` property.

  ;; create an instance

  (def backend (backends/session))

  ;; wrap the ring handler

  (defn my-handler
    "Uses `:identity` value in request to decide how to handle it."
    [request]
    (if (:identity request)
      (response (format "Hello %s" (:identity request)))
      {:status 403 :body "Forbidden"}))

  (def app (-> my-handler
               (wrap-authentication backend)))

  ;; TODO: add working Ring session auth example

  ;; unauthenticated
  (app {:uri "/hello" :body "HAYO!"})
  ;; identity added manually
  (app {:uri "/hello" :body "HAYO!" :identity "molly"})
  ;; session added manually
  (app {:uri "/hello" :body "HAYO!" :session {:identity "molly"}})
  ;; proper session auth
  (app {:uri "/hello"
        :body "HAYO!"
        ;; ???
        :headers {}})


  ;; token

  ;; this is a backend that uses tokens for authenticating the user. It behaves
  ;; very similarly to the basic-auth backend with the difference that instead
  ;; of authenticating with credentials it authenticates with a simple token.

  ;; define a in-memory relation between tokens and users:

  (def tokens {:2f904e245c1f5 :admin
               :45c1f5e3f05d0 :user})

  ;; define an `:authfn`, function with the responsibility to authenticate the
  ;; incoming token and return an identity instance

  (defn my-authfn
    "Receives token extracted from headers by buddy middleware and produces an
  identity value, that will be placed in the request under `:identity` key."
    [request token]
    (let [token (keyword token)]
      (get tokens token nil)))

  ;; create a backend instance

  (def backend (backends/token {:authfn my-authfn
                                :token-name "Bearer"}))

  ;; wrap the ring handler

  (defn my-handler
    "Uses `:identity` value in request to decide how to handle it."
    [request]
    (if (:identity request)
      (response (format "Hello %s" (:identity request)))
      {:status 403 :body "Forbidden"}))

  (def app (-> my-handler
               (wrap-authentication backend)))

  ;; the process of authentication of this backend consists in parsing the
  ;; “Authorization” header, extracting the token and in case the token is
  ;; extracted successfully, call the `authfn` with extracted token.

  ;; authorization header example
  ;; "Authorization: Bearer 45c1f5e3f05d0"

  ;; the responsibility of buddy is just to parse the request and call the user
  ;; function to authenticate it. The token building and storage is a user
  ;; responsability.

  ;; unauthenticated
  (app {:uri "/hello" :body "HAYO!"})
  ;; identity added manually
  (app {:uri "/hello" :body "HAYO!" :identity "molly"})
  ;; proper token auth
  (app {:uri "/hello"
        :body "HAYO!"
        :headers {"Authorization" "Bearer NO-WAY"}})
  (app {:uri "/hello"
        :body "HAYO!"
        :headers {"Authorization" "Bearer 2f904e245c1f5"}})


  ;; signed JWT

  ;; a backend that uses signed and self contained tokens to authenticate the
  ;; user.

  ;; it behaves very similarly to the Token backend (previously explained) with
  ;; the difference that this one does not need additional user defined logic to
  ;; validate tokens, because as we previously said, everything is self
  ;; contained.

  ;; this type of token mechanism enables a complete stateless authentication
  ;; because the server does not need to store the token and related
  ;; information, the token will contain all the needed information for
  ;; authentication.

  (def secret "topsecret")
  (def backend (backends/jws {:secret secret
                              :token-name "Bearer"}))

  ;; and wrap your ring application with the authentication middleware

  (defn my-handler
    "Uses `:identity` value in request to decide how to handle it."
    [request]
    (if (:identity request)
      (response (format "Hello %s" (:identity request)))
      {:status 403 :body "Forbidden"}))

  (def app (-> my-handler
               (wrap-authentication backend)))

  ;; now you should have a login endpoint in your ring application that will
  ;; have the responsibility of generating valid tokens:

  (defn find-user
    "Returns a user id by username and password."
    [username password]
    {:id username})

  (defn login-handler
    "Authenticates a user, returns a signed bearer JWT with the user id."
    [request]
    (let [data (:body-params request)
          user (find-user (:username data)
                          (:password data))
          token (jwt/sign {:user (:id user)} secret)]
      {:status 200
       :body (json/encode {:token token})
       :headers {:content-type "application/json"}}))

  ;; get the signed JWT token
  (def signed-jwt-token
    (-> (login-handler {:body-params
                        {:username "joe" :password "secret"}})
        :body
        json/decode
        (get "token")))

  ;; check the token contents
  (jwt/unsign signed-jwt-token secret)

  ;; unauthenticated
  (app {:uri "/hello" :body "HAYO!"})
  ;; identity added manually
  (app {:uri "/hello" :body "HAYO!" :identity "molly"})
  ;; proper signed JWT token auth
  (app {:uri "/hello"
        :body "HAYO!"
        :headers {"Authorization" "Bearer NO-WAY"}})
  (app {:uri "/hello"
        :body "HAYO!"
        :headers {"Authorization" (str "Bearer " signed-jwt-token)}})

  ;; for more details about jwt, see the `buddy-sign` documentation. Some
  ;; valuable resources for learning about stateless authentication are:
  ;; - http://lucumr.pocoo.org/2013/11/17/my-favorite-database/


  ;; encrypted JWT

  ;; this backend is almost identical to the previous one (signed JWT).

  ;; the main difference is that the backend uses JWE (Json Web Encryption)
  ;; instead of JWS (Json Web Signature) and it has the advantage that the
  ;; content of the token is encrypted instead of simply signed. This is useful
  ;; when token may contain some additional user information that should not be
  ;; public.

  ;; it will look similar to the previous (jws) example but instead using jwe
  ;; with asymmetric key encryption algorithm.

  ;; in order to use any asymmetric encryption algorithm, you should have
  ;; private/public key pair. If you don’t have one, don’t worry, it is very
  ;; easy to generate it using openssl, see this faq:
  ;; https://funcool.github.io/buddy-sign/latest/05-faq.html

  (def pubkey (k/public-key "resources/example_openssl_rsa.pem.pub"))
  (def privkey (k/private-key "resources/example_openssl_rsa.pem" "secret"))
  (def encryption-params {:alg :rsa-oaep :enc :a128cbc-hs256})

  (def backend
    (backends/jwe {:secret privkey
                   :token-name "Bearer"
                   :options encryption-params}))

  ;; and wrap your ring application with
  ;; the authentication middleware

  (defn my-handler
    "Uses `:identity` value in request to decide how to handle it."
    [request]
    (if (:identity request)
      (response (format "Hello %s" (or (-> request :identity :user :id)
                                       (-> request :identity))))
      {:status 403 :body "Forbidden"}))

  (def app (-> my-handler
               (wrap-authentication backend)))

  ;; the corresponding login endpoint should have a similar aspect to this:

  (defn find-user
    "Returns a user id by username and password."
    [username password]
    {:id username
     :pass password})

  (defn login-handler
    "Authenticates a user, returns an encrypted bearer JWT with user id and some
  private information (password)."
    [request]
    (let [data (:body-params request)
          user (find-user (:username data)
                          (:password data))
          token (jwt/encrypt {:user user} pubkey encryption-params)]
      {:status 200
       :body (json/encode {:token token})
       :headers {:content-type "application/json"}}))

  ;; get the encrypted JWT token
  (def encrypted-jwt-token
    (-> (login-handler {:body-params
                        {:username "joe" :password "secret"}})
        :body
        json/decode
        (get "token")))

  ;; check the token contents
  (jwt/decrypt encrypted-jwt-token privkey encryption-params)

  ;; unauthenticated
  (app {:uri "/hello" :body "HAYO!"})
  ;; identity added manually
  (app {:uri "/hello" :body "HAYO!" :identity "molly"})
  ;; proper encrypted JWT token auth
  (app {:uri "/hello"
        :body "HAYO!"
        :headers {"Authorization" "Bearer NO-WAY"}})
  (app {:uri "/hello"
        :body "HAYO!"
        :headers {"Authorization" (str "Bearer " encrypted-jwt-token)}})

)


;; authorization

(comment

  ;; the authorization system is split into two parts: generic authorization and
  ;; access-rules (explained in the next section).

  ;; the generic one is based on exceptions, and consists in raising an
  ;; "unauthorized" exception in case the request is considered
  ;; unauthorized. The access rules system is based on some kind of rules
  ;; attached to the handler or an URI and that rules determine if a request is
  ;; authorized or not.


  ;; exception-based

  ;; this authorization approach is based on wrapping everything in a try/catch
  ;; block which only handles specific exceptions. When an unauthorized
  ;; exception is caught, it executes a specific function to handle it or
  ;; reraises the exception.

  ;; with this approach, you can define your own middlewares/decorators using
  ;; custom authorization logic with fast skip, raising an unauthorized
  ;; exception using the `throw-unauthorized` function.

  (defn my-handler
    [request]
    (when (not (authenticated? request))
      (throw-unauthorized {:message "Not authorized"}))
    (response (str "Hello, " (:identity request))))

  ;; just like the authentication system, authorization is also implemented
  ;; using pluggable backends.

  ;; all built-in backends already implement the authorization protocol with
  ;; default behavior. The default behavior can be overridden passing the
  ;; `:unauthorized-handler` option to the backend constructor:

  ;; simple custom handler for unauthorized requests.

  (defn my-auth-fn
    "Receives credentials extracted from request headers by buddy middleware and
  produces an identity value, that will be placed in the request under
  `:identity` key."
    [request credentials]
    (let [username (:username credentials)
          password (:password credentials)]
      (when password username)))

  (defn my-unauthorized-handler
    [request metadata]
    (-> (response "Unauthorized request")
        (assoc :status 403)))

  (def backend (backends/basic
                {:realm "API"
                 :authfn my-auth-fn
                 :unauthorized-handler my-unauthorized-handler}))

  (def app (-> my-handler
               (wrap-authentication backend)
               (wrap-authorization backend)))

  ;; usage

  (defn str->b64 [string]
    (.encodeToString (Base64/getEncoder) (.getBytes string)))

  (def username "joe")
  (def password "secret")
  (def credentials (str->b64 (str username ":" password)))

  ;; unauthorized
  (app {:uri "/hello" :body "HAYO!"})
  ;; identity added manually
  (app {:uri "/hello" :body "HAYO!" :identity "molly"})
  ;; proper basic auth(z)
  (app {:uri "/hello"
        :body "HAYO!"
        :headers {"authorization" (str "Basic " "NO-WAY")}})
  (app {:uri "/hello"
        :body "HAYO!"
        :headers {"authorization" (str "Basic " credentials)}})


  ;; access rules

  ;; the access rules system is another part of authorization. It consists of
  ;; matching an url to specific access rules logic.

  ;; the access rules consist of an ordered list that contains mappings between
  ;; urls and rule handlers using
  ;; [clout](https://github.com/weavejester/clout) url matching syntax or
  ;; regular expressions.


  (def access-rules '[{:uri "/foo"
                       :handler authenticated-user}
                      {:uri "/admin"
                       :handler admin-user}
                      {:uris ["/foo" "/bar"]
                       :handler authenticated-user}
                      {:pattern #"^/foo$"
                       :handler authenticated-user}])

  ;; an access rule can also match against certain HTTP methods, by using the
  ;; `:request-method` option. `:request-method` can be a keyword or a set of
  ;; keywords.

  ;; an example of an access rule that matches only GET requests:

  (def get-rules '[{:uri "/foo"
                    :handler authenticated-user
                    :request-method :get}])


  ;; rules handlers

  ;; the rule handler is a plain function that accepts a request as a parameter
  ;; and should return an instance of `accessrules/success` or
  ;; `accessrules/error`.

  ;; the `success` is a simple mark that means that handlers pass the validation
  ;; and error is a mark that means the opposite, that the handler does not pass
  ;; the validation. Instead of returning plain boolean values, this approach
  ;; allows handlers to return errors messages or even a ring response.

  ;; these values are considered success marks: `true` and `success`
  ;; instances. These are considered error marks: `nil`, `false`, and `error`
  ;; instances. Error instances may contain a string as an error message or a
  ;; Ring response hash-map.

  ;; also, a rule handler can be a composition of several rule handlers using
  ;; logical operators.

  '{:and [authenticated-user admin-user]}
  '{:or [authenticated-user admin-user]}

  ;; logical expressions can be nested as deep as you wish with hypothetical
  ;; rule handlers with self-descriptive names.
  '{:or [admin-user
         {:and [from-known-ip
                authenticated-user]}]}

  ;; this is an example of how a composed rule handler can be used in an access
  ;; rules list:

  '[{:pattern #"^/foo$"
     :handler {:and [authenticated-user admin-user]}}]

  ;; additionally, if you are using `clout`-based syntax for matching access
  ;; rules, the request in a rule handler will contain `:match-params` with
  ;; clout matched uri params.


  ;; usage

  ;; `buddy-auth` exposes two ways to do it:

  ;; - using a `wrap-access-rules` middleware.
  ;; - using a `restrict` decorator for assigning specific rules handlers
  ;;   to a concrete Ring handler.

  (defn authenticated-user
    "Checks if request comes from authenticated user."
    [request]
    (if (:identity request)
      (success)
      (error "Only authenticated users allowed")))

  (defn admin-user
    "Checks if request comes from the user with admin role."
    [request]
    (if (contains? (:roles request) :admin)
      (success)
      (error "Only admin users allowed")))

  (defn operator-user
    "Checks if request comes from the user with operator role."
    [request]
    (if (contains? (:roles request) :operator)
      (success)
      (error "Only operator users allowed")))

  (defn any-user
    "Allows all requests."
    [request] true)

  (def rules [{:pattern #"^/admin/.*"
               :handler admin-user}
              {:pattern #"^/operator/.*"
               :handler {:or [admin-user operator-user]}}
              {:pattern #"^/login$"
               :handler any-user}
              {:pattern #"^/.*"
               :handler authenticated-user}])

  ;; define default behavior for unauthorized requests. This function works like
  ;; a default Ring-compatible handler and should implement the default behavior
  ;; for requests which are not authorized by any defined rule.

  (defn on-error
    [request value]
    {:status 403
     :headers {}
     :body "Not authorized"})

  ;; wrap the handler with access rules

  (defn my-handler
    [request]
    (response (str "Hello, " (:identity request "anonymous"))))


  (def app (wrap-access-rules my-handler {:rules rules :on-error on-error}))

  ;; unauthenticated user
  (app {:uri "/admin/profile"})
  (app {:uri "/operator/profile"})
  (app {:uri "/hello"})
  (app {:uri "/login"})

  ;; authenticated user
  (app {:uri "/admin/profile" :identity "joe"})
  (app {:uri "/operator/profile" :identity "joe"})
  (app {:uri "/hello" :identity "joe"})
  (app {:uri "/login" :identity "joe"})

  ;; authenticated user, operator role
  (app {:uri "/admin/profile" :identity "joe" :roles #{:operator}})
  (app {:uri "/operator/profile" :identity "joe" :roles #{:operator}})
  (app {:uri "/hello" :identity "joe" :roles #{:operator}})
  (app {:uri "/login" :identity "joe" :roles #{:operator}})

  ;; authenticated user, admin role
  (app {:uri "/admin/profile" :identity "joe" :roles #{:admin}})
  (app {:uri "/operator/profile" :identity "joe" :roles #{:admin}})
  (app {:uri "/hello" :identity "joe" :roles #{:admin}})
  (app {:uri "/login" :identity "joe" :roles #{:admin}})

  ;; if a request uri does not match any regular expression then the default
  ;; policy is used. The default policy in `buddy-auth` is allow but you can
  ;; change the default behavior specifying a `:reject` value in the `:policy`
  ;; option.

  ;; additionally, instead of specifying the global `on-error` handler, you can
  ;; set a specific behavior on a specific access rule, or use the `:redirect`
  ;; option to simply redirect a user to specific url.

  (def local-rules
    [{:pattern #"^/admin/.*"
      :handler {:or [admin-user operator-user]}
      :redirect "/notauthorized"}
     {:pattern #"^/login$"
      :handler any-user}
     {:pattern #"^/.*"
      :handler authenticated-user
      :on-error (fn [req _] (response "Not authorized ;)"))}])

  ;; the access rule options always take precedence over the global ones.


  ;; if you don’t want an external rules list and simply want to apply some
  ;; rules to specific ring views/handlers, you can use the `restrict`
  ;; decorator.

  (defn my-handler
    [request]
    (response (str "Hello, " (:identity request "anonymous"))))

  (defn authenticated-user
    "Checks if request comes from authenticated user."
    [request]
    (if (:identity request)
      true
      (error "Only authenticated users allowed")))

  (defn on-error
    [request value]
    {:status 403
     :headers {}
     :body "Not authorized"})

  (def app (restrict my-handler {:handler authenticated-user
                                 :on-error on-error}))

  ;; unauthenticated user
  (app {:uri "/hello"})

  ;; authenticated user
  (app {:uri "/hello" :identity "joe"})


  ;; examples

  ;; In the `buddy-auth` repo, see `examples` directory.

)
