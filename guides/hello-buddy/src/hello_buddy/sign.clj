(ns hello-buddy.sign
  (:require
   [buddy.core.bytes :as bytes]
   [buddy.core.hash :as hashes]
   [buddy.core.keys :as keys]
   [buddy.core.nonce :as nonce]
   [buddy.sign.compact :as cms]
   [buddy.sign.jwe :as jwe]
   [buddy.sign.jws :as jws]
   [buddy.sign.jwt :as jwt]
   [java-time.api :as time]))


;; A high level abstraction for web ready message signing and encryption.

;; http://funcool.github.io/buddy-sign/latest/index.html


;; intro

(comment

  ;; it can be used for several purposes:

  ;; - you can serialize and sign or encrypt a user ID for unsubscribing of
  ;;   newsletters into URLs. This way you don't need to generate one-time
  ;;   tokens and store them in the database.
  ;; - same thing with any kind of activation link for accounts and similar
  ;;   things.
  ;; - signed or encrypted objects can be stored in cookies or other untrusted
  ;;   sources which means you don't need to have sessions stored on the server,
  ;;   which reduces the number of necessary database queries.
  ;; - signed information can safely do a roundtrip between server and client
  ;;   in general which makes them useful for passing server-side state to a
  ;;   client and then back.
  ;; - safely send and receive signed or encrypted messages between components
  ;;   or microservices.
  ;; - self-contained token generation for use with completely stateless token
  ;;   based authentication.

  ;; involved RFC’s?

  ;; - https://tools.ietf.org/html/rfc7519
  ;; - https://tools.ietf.org/html/rfc7518
  ;; - https://tools.ietf.org/html/rfc7516
  ;; - https://tools.ietf.org/html/rfc7515
  ;; - http://tools.ietf.org/html/draft-mcgrew-aead-aes-cbc-hmac-sha2-05
  ;; - https://tools.ietf.org/html/rfc3394
  ;; - https://tools.ietf.org/html/rfc7517
  ;; - https://tools.ietf.org/html/rfc7638
  ;; - https://tools.ietf.org/html/rfc8037

)


;; JWT (JSON Web Token)

(comment

  ;; a compact claims representation format intended for space-constrained
  ;; environments such as HTTP Authorization headers and URI query
  ;; parameters. JWTs encode claims to be transmitted as a JavaScript Object
  ;; Notation (JSON) object that is used as the payload of a JSON Web
  ;; Signature (JWS) structure or as the plaintext of a JSON Web
  ;; Encryption (JWE) structure, enabling the claims to be digitally signed or
  ;; MACed and/or encrypted.


  ;; supported algorithms

  ;; here a table of supported algorithms for signing JWT claims using JWS (Json
  ;; Web Signature):
  ;; algorithm name        hash algorithms keywords            priv/pub key?
  ;; - Elliptic Curve DSA  sha256, sha512  `:es256`, `:es512`  yes
  ;; - Edwards Curve DSA   sha512          `:eddsa`            yes
  ;; - RSASSA PSS          sha256, sha512  `:ps256`, `:ps512`  yes
  ;; - RSASSA PKCS1 v1_5   sha256, sha512  `:rs256`, `:rs512`  yes
  ;; - HMAC                sha256*, sha512 `:hs256`, `:hs512`  no


  ;; the JWE (Json Web Encryption), unlike JWS, uses two types of algoritms: key
  ;; encryption algorithms and content encryption algorithms.

  ;; the key encryption algorithms are responsible of encrypt the key that will
  ;; be used to encrypt the content. This is a table that shows the currently
  ;; supported Key Encryption Algorithms (specified in the JWA RFC):
  ;; algorithm name  description  keyword  shared key size
  ;; DIR          Direct use of a      `:dir`          (depends on content
  ;;              shared symmetric key                 encryption algorithm)
  ;; A128KW       AES128 Key Wrap      `:a128kw`       16 bytes
  ;; A192KW       AES192 Key Wrap      `:a192kw`       24 bytes
  ;; A256KW       AES256 Key Wrap      `:a256kw`       32 bytes
  ;; RSA1_5       RSA PKCS1 V1_5       `:rsa1_5`       Asymmetric key pair
  ;; RSA-OAEP     RSA OAEP with SHA1   `:rsa-oaep`     Asymmetric key pair
  ;; RSA-OAEP-256 RSA OAEP with SHA256 `:rsa-oaep-256` Asymmetric key pair

  ;; the content encryption algorithms are responsible for encryption of the
  ;; content. This is a table that shows the currently supported Content
  ;; Encryption Algorithms (specified in the JWA RFC):
  ;; algorithm name description                       keyword    shared key size
  ;; A128CBC-HS256  AES128, CBC mode and HMAC-SHA256  `:a128cbc-hs256`  32 bytes
  ;; A192CBC-HS384  AES192, CBC mode and HMAC-SHA384  `:a192cbc-hs384`  48 bytes
  ;; A256CBC-HS512  AES256, CBC mode and HMAC-SHA512  `:a256cbc-hs512`  64 bytes
  ;; A128GCM        AES128, GCM mode                  `:a128gcm`        16 bytes
  ;; A192GCM        AES192, GCM mode                  `:a192gcm`        24 bytes
  ;; A256GCM        AES256, GCM mode                  `:a256gcm`        32 bytes


)


;; symmetric signing and encryption (with one shared key)

(comment

  ;; signing data

  ;; typically a map
  (def claims {:userid 1})
  (def a-key "secret")
  (def a-signature (jwt/sign claims a-key))

  ;; the `sign` function return a encoded and signed token as plain String
  ;; instance or an exception in case of something goes wrong. As you can
  ;; observe, no algorithm is passed as parameter. In this situations the
  ;; default one will be used, and in this case this is `:hs256`.

  ;; NOTE: Due to the nature of the storage format, the input is restricted
  ;; mainly to json objects in the current version.


  ;; unsigning data

  ;; unsigning verifies the signature of the incoming data and returns the plain
  ;; data without signature.

  (jwt/unsign a-signature a-key)


  ;; claims validation

  ;; `buddy-sign` JWT implements validation of a concrete subset of claims:
  ;; `:iat` (issue time), `:exp` (expiration time), `:nbf` (not before),
  ;; `:iss` (issuer) and `:aud` (audience).

  ;; the validation is performed when decoding the token. If `:exp` claim is
  ;; found and is posterior to the current date time (UTC) a validation
  ;; exception will be raised. Alternatively, the time to validate token against
  ;; can be specified as `:now` option to `unsign`.

  ;; additionally, if you want to provide some leeway for the claims validation,
  ;; you can pass the `:leeway` option to the `unsign` function.


  ;; define claims with `:exp` key

  (def now (time/zoned-date-time))

  (def claims
    {:user 1 :exp (time/instant (time/plus now (time/seconds 5)))})

  ;; sign the claims

  (def token (jwt/sign claims "key"))

  ;; wait 5 seconds and try unsign it

  (jwt/unsign token "key")

  ;; use timestamp in the past (5 seconds before `now`)

  (jwt/unsign token "key" {:now (time/instant
                                 (time/minus now (time/seconds 5)))})


  ;; encrypting and decrypting data

  ;; hash your secret key with sha256 to create a byte array of 32 bytes because
  ;; is a requirement for default content encryption algorithm

  (def secret (hashes/sha256 "mysecret"))
  (def claims {:user 1})

  ;; encrypt the claims using the previously hashed key

  (def encrypted-jwt
    (jwt/encrypt claims secret {:alg :dir :enc :a128cbc-hs256}))

  ;; the `encrypt` function, like `sign` from JWT, returns a plain string with
  ;; encrypted and encoded content using a provided algorithm and shared secret
  ;; key.

  ;; the `decrypt` is a inverse process, that takes encrypted data and the
  ;; shared key, and returns the plain data. For it, buddy-sign exposes the
  ;; decrypt function. Let see how you can use it:

  (jwt/decrypt encrypted-jwt secret)

)


;; asymmetric signing and encryption (with public/private keypair)

(comment

  ;; in order to use any of asymmetric digital signature or encryption
  ;; algorithms you must have a private/public keypair. If you don't have one,
  ;; it is easy to generate it using `openssl` (see the FAQ:
  ;; http://funcool.github.io/buddy-sign/latest/05-faq.html).


  ;; signing data

  ;; create key instances (ECDSA)

  (def ec-privkey (keys/private-key "resources/example_openssl_ecdsa.pem"))
  (def ec-pubkey (keys/public-key "resources/example_openssl_ecdsa.pem.pub"))

  ;; use them just like a symmetric key: private key for signing

  (def claims {:foo "bar"})
  (def signed-claims (jwt/sign claims ec-privkey {:alg :es256}))

  ;; and public key for unsigning

  (jwt/unsign signed-claims ec-pubkey {:alg :es256})


  ;; encrypting and decrypting data

  ;; create key instances (RSA)

  (def rsa-privkey
    (keys/private-key "resources/example_openssl_rsa.pem" "secret"))
  (def rsa-pubkey (keys/public-key "resources/example_openssl_rsa.pem.pub"))

  ;; encrypt the data using public key

  (def claims {:foo "bar"})
  (def encrypted-claims (jwt/encrypt claims
                                     rsa-pubkey
                                     {:alg :rsa-oaep
                                      :enc :a128cbc-hs256}))

  ;; decrypt using private key

  (jwt/decrypt encrypted-claims rsa-privkey {:alg :rsa-oaep
                                             :enc :a128cbc-hs256})

)


;; JWS (JSON Web Signature)

(comment

  ;; JSON Web Signature (JWS) is a signing part of Json Web Token (JWT)
  ;; specification and represents a content secured with digital signatures or
  ;; Message Authentication Codes (MACs) using JavaScript Object Notation (JSON)
  ;; as serialization format.

  ;; in difference to JWT, this is more lowlevel signing primitive and allows
  ;; signing arbitrary binary data (instead of JSON-formatted claims):

  (def data (nonce/random-bytes 1024))
  (def message (jws/sign data "secret"))

  (bytes/equals? (jws/unsign message "secret") data)

  ;; supported algorithms are documented in the JWT section above.

)


;; JWE (Json Web Encryption)

(comment

  ;; JSON Web Encryption (JWE) is an encryption part of Json Web Token (JWT)
  ;; specification and represents a encrypted content using JavaScript Object
  ;; Notation (JSON) data structures.

  ;; in same way as JWS, this is a low-level primitive that allows create fully
  ;; encrypted messages of arbitrary data:

  (def key32 (nonce/random-bytes 32))
  (def data (nonce/random-bytes 1024))

  (def message (jwe/encrypt data key32))
  (bytes/equals? (jwe/decrypt message key32) data)

  ;; supported algorithms are documented in the JWT section above.

)


;; CMS (Compact Message Signing)

(comment

  ;; CMS is highly influenced by Django’s cryptographic library and JSON Web
  ;; Signature/Encryption signing algorithms with focus on having a compact
  ;; representation. It's built on top of `ptaoussanis/nippy` serialization
  ;; library.

  ;; in order to use this you shall manually include the `nippy` library because
  ;; buddy-sign does not have a hardcoded dependency to it.

  ;; in the same way as JWS, it support a great number of different signing
  ;; algorithms that can be used for signing of your messages:
  ;; algorithm name     hash algorithms keywords             priv/pub key?
  ;; Elliptic Curve DSA sha256, sha512  `:es256`, `:es512`   yes
  ;; RSASSA PSS         sha256, sha512  `:ps256`, `:ps512`   yes
  ;; RSASSA PKCS1 v1_5  sha256, sha512  `:rs256`, `:rs512`   yes
  ;; Poly1305           aes, twofish,   `:poly1305-aes`,     no
  ;;                    serpent         `:poly1305-serpent`,
  ;;                                    `:poly1305-twofish`
  ;; HMAC               sha256*, sha512 `:hs256`, `:hs512`   no

  ;; in difference with JWT, this implementation is not limited to map-like
  ;; objects, and you can sign any valid Clojure type.

  (def data (cms/sign #{:foo :bar} "secret"))

  (cms/unsign data "secret")

  ;; then, you also will be able validate the signed message based on its age:

  (cms/unsign data "secret" {:max-age (* 15 60)})

  ;; NOTE: only `:max`-age validation is bundled, all other validations are
  ;; delegated to the user code.

)
