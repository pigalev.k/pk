(ns hello-buddy.core
  (:refer-clojure :exclude [bytes key keys])
  (:require
   [buddy.core.bytes :as bytes]
   [buddy.core.certificates :as certs]
   [buddy.core.codecs :as codecs]
   [buddy.core.crypto :as crypto]
   [buddy.core.dsa :as dsa]
   [buddy.core.hash :as hashes]
   [buddy.core.kdf :as kdf]
   [buddy.core.keys :as keys]
   [buddy.core.mac :as mac]
   [buddy.core.nonce :as nonce]
   [buddy.core.padding :as padding]
   [clojure.java.io :as io])
  (:import (java.security KeyPairGenerator SecureRandom)))


;; A cryptographic API for Clojure.

;; http://funcool.github.io/buddy-core/latest/index.html


;; intro

(comment

  ;; this library includes:

  ;; - cryptographic hash algorithms (digest)
  ;; - key derivation algorithms (kdf)
  ;; - digital signatures
  ;; - message authentication (mac)
  ;; - block ciphers
  ;; - stream ciphers
  ;; - padding schemes
  ;; - nonces and salts
  ;; - X.509 certificates

)


;; hash algorithms (digest)

(comment

  ;; available hash algorithms:

  ;; algo name 	digest size

  ;; SHA1       160
  ;; SHA2       256, 384, 512
  ;; SHA3       256, 384, 512
  ;; MD5        128
  ;; Tiger      192
  ;; Blake2b    512
  ;; Skein      256, 512, 1024, arbitrary size
  ;; Whirlpool  512
  ;; RIPEMD128  128
  ;; RIPEMD160  160
  ;; RIPEMD256  256
  ;; RIPEMD320  320


  ;; basic usage


  ;; sha1

  (hashes/sha1 "foo bar")

  (-> (hashes/sha1 "foo bar")
      codecs/bytes->hex)

  ;; sha2

  (-> (hashes/sha256 "foo bar")
      codecs/bytes->hex)

  (-> (hashes/sha384 "foo bar")
      codecs/bytes->hex)

  (-> (hashes/sha512 "foo bar")
      codecs/bytes->hex)


  ;; sha3

  (-> (hashes/sha3-256 "foo bar")
      codecs/bytes->hex)

  (-> (hashes/sha3-384 "foo bar")
      codecs/bytes->hex)

  (-> (hashes/sha3-512 "foo bar")
      codecs/bytes->hex)


  ;; MD5

  (-> (hashes/md5 "foo bar")
      codecs/bytes->hex)


  ;; Tiger

  (-> (hashes/digest "foo bar" :tiger)
      codecs/bytes->hex)


  ;; Blake2b

  (-> (hashes/blake2b-128 "foo bar")
      codecs/bytes->hex)

  (-> (hashes/blake2b-256 "foo bar")
      codecs/bytes->hex)

  (-> (hashes/blake2b "foo bar" 3)
      codecs/bytes->hex)

  ;; Skein

  (-> (hashes/skein-256 "foo bar")
      codecs/bytes->hex)

  (-> (hashes/skein-512 "foo bar")
      codecs/bytes->hex)

  (-> (hashes/skein-1024 "foo bar")
      codecs/bytes->hex)


  ;; Whirlpool

  (-> (hashes/whirlpool "foo bar")
      codecs/bytes->hex)


  ;; RIPEMD

  (-> (hashes/ripemd128 "foo bar")
      codecs/bytes->hex)

  (-> (hashes/ripemd160 "foo bar")
      codecs/bytes->hex)

  (-> (hashes/ripemd256 "foo bar")
      codecs/bytes->hex)

  (-> (hashes/ripemd320 "foo bar")
      codecs/bytes->hex)


  ;; advanced usage

  ;; hash functions are implemented using protocols and can be extended to other
  ;; types. The default implementations come with support for file-like
  ;; objects (File, URL, URI* and InputStream).

  ;; make hash of file:

  (-> (hashes/sha256 (io/input-stream "/etc/hosts"))
      codecs/bytes->hex)

  ;; you can extend it for your own types using the `buddy.core.hash/IDigest`
  ;; protocol:

  ;; (defprotocol Digest
  ;;   (-digest [data engine]))

  ;; Note: functions like `sha256` are aliases for the more generic function
  ;; `digest`.

)


;; MAC algorithms

(comment

  ;; buddy comes with three mac implementations: hmac, shmac and poly1305


  ;; HMAC

  ;; is a specific construction for calculating a message authentication
  ;; code (MAC) involving a cryptographic hash function in combination with a
  ;; secret cryptographic key.

  ;; any cryptographic hash function, such as MD5 or SHA-1, may be used in the
  ;; calculation of an HMAC; the resulting MAC algorithm is termed HMAC-MD5 or
  ;; HMAC-SHA1 accordingly. The cryptographic strength of the HMAC depends upon
  ;; the cryptographic strength of the underlying hash function, the size of its
  ;; hash output, and on the size and quality of the key.


  ;; basic usage

  ;; generating a hmac from plain string using `sha256` digest

  ;; generate sha256 hmac over string

  (def a-hmac (-> (mac/hash "foo bar" {:key "mysecretkey" :alg :hmac+sha256})
                  (codecs/bytes->hex)))


  ;; verifying a hmac

  (mac/verify "foo bar" (codecs/hex->bytes a-hmac)
              {:key "mysecretkey" :alg :hmac+sha256})


  ;; the key parameter can be any type that implements the `ByteArray` protocol
  ;; defined in the `buddy.core.codecs` namespace. It comes with default
  ;; implementations for `byte[]` and `java.lang.String` and nil.


  ;; Poly1305

  ;; Poly1305 is a cryptographic message authentication code (MAC) written by
  ;; Daniel J. Bernstein. It can be used to verify the data integrity and the
  ;; authenticity of a message.

  ;; the security of Poly1305 is very close to the block cipher algorithm. As a
  ;; result, the only way for an attacker to break Poly1305 is to break the
  ;; cipher.

  ;; Poly1305 offers cipher replaceability. If anything goes wrong with one, it
  ;; can be substituted by another with identical security guarantees.

  ;; unlike hmac, it requires an initialization vector (IV). An IV is like a
  ;; salt. It should be generated using a strong random number generator for
  ;; security guarantees. Also, the IV should be of the same length as the
  ;; chosen cipher block size.


  ;; using poly1305 mac algorithm for generate mac from string:

  (def a-key (nonce/random-bytes 32))
  (def iv (nonce/random-bytes 16))

  (def a-poly1305-aes
    (-> (mac/hash "foo bar" {:key a-key :iv iv :alg :poly1305+aes})
        (codecs/bytes->hex)))

  (mac/verify "foo bar" (codecs/hex->bytes a-poly1305-aes)
              {:key a-key :iv iv :alg :poly1305+aes})

  ;; the default specification talks about AES as default block cipher but the
  ;; algorithm in fact can work other block ciphers without any problem. So you
  ;; can use serpent and twofish among the default aes:

  (def a-poly1305-twofish
    (-> (mac/hash "foo bar" {:key a-key :iv iv :alg :poly1305+twofish})
        (codecs/bytes->hex)))

  (mac/verify "foo bar" (codecs/hex->bytes a-poly1305-twofish)
              {:key a-key :iv iv :alg :poly1305+twofish})


  ;; advanced usage

  ;; generate mac for file-like objects

  ;; like with hash functions, you can use `String`, `byte[]`, `File`, `URL`,
  ;; `URI` and `InputStream` as input value for mac functions.


  ;; generating hmac from input stream:

  (-> (io/input-stream "/etc/hosts")
      (mac/hash {:key "mysecretkey" :alg :hmac+sha256})
      (codecs/bytes->hex))


  ;; generating hmac from file:

  (-> (io/file "/etc/hosts")
      (mac/hash {:key "mysecretkey" :alg :hmac+sha256})
      (codecs/bytes->hex))


  ;; low-level api

  ;; behind the scenes of the high level api, a low level api is already defined
  ;; with protocols and you can use it for your purposes:

  (let [engine (mac/engine {:alg :hmac+sha256})]
    (mac/-update engine (codecs/str->bytes "hello") 0 5)
    (codecs/bytes->hex (mac/-end engine)))

  ;; this also applies to the rest of mac implementations found in `buddy-core`
  ;; library.

)


;; keys

(comment

  ;; reading PEM formatted keys

  ;; before explaining digital signatures, you need to read public/private
  ;; keypairs and convert them to usable objects. Buddy has limited support for
  ;; reading:

  ;; - RSA keypair
  ;; - ECDSA keypair


  ;; RSA keypair

  ;; an RSA keypair is obviously used for RSA encryption/decryption, but it is
  ;; also used for making digital signatures with RSA-derived algorithms.

  ;; generate a RSA keypair using openssl:

  ;; generate AES-256 encrypted private key
  ;; openssl genrsa -aes256 -out resources/example_openssl_rsa.pem 4096

  ;; generate public key from previously created private key
  ;; openssl rsa -pubout -in resources/example_openssl_rsa.pem \
  ;;         -out resources/example_openssl_rsa.pem.pub

  ;; read keys:

  ;; the last parameter is only mandatory if a private key is encrypted
  (def privkey-rsa
    (keys/private-key "resources/example_openssl_rsa.pem" "secret"))
  (def pubkey-rsa
    (keys/public-key "resources/example_openssl_rsa.pem.pub"))


  ;; ECDSA keypair

  ;; like RSA keypairs, ECDSA is also used for making digital signatures and can
  ;; be read like in the RSA examples.

  ;; generate a ECDSA Keypair using openssl:

  ;; generate a params file
  ;; openssl ecparam -name prime256v1 \
  ;;         -out resources/example_openssl_ecdsa_params.pem
  ;; generate a private key from params file
  ;; openssl ecparam -in resources/example_openssl_ecdsa_params.pem \
  ;;         -genkey -noout -out resources/example_openssl_ecdsa.pem
  ;; generate a public key from private key
  ;; openssl ec -in resources/example_openssl_ecdsa.pem\
  ;;         -pubout -out resources/example_openssl_ecdsa.pem.pub

  ;; read keys:

  ;; the last parameter is only mandatory if a private key is encrypted
  (def privkey-ecdsa
    (keys/private-key "resources/example_openssl_ecdsa.pem" "secret"))
  (def pubkey-ecdsa
    (keys/public-key "resources/example_openssl_ecdsa.pem.pub"))


  ;; JSON Web Key (JWK)

  ;; a JSON Web Key (JWK) is a JavaScript Object Notation (JSON) data structure
  ;; that represents cryptographic keys of different types.

  ;; `buddy-core` provides functions for reading and saving JCA keys in JWK
  ;; format

  ;; currently supported JWK key types are

  ;; - RSA key pairs (No RSA-CRT support yet)
  ;; - OKP key pairs (Ed25519)
  ;; - EC key pairs (P-256, P-384, P-521 curves)


  ;; example of JWS signing for Ed25519 keys:

  (def edkey {:kty "OKP",
              :crv "Ed25519",
              :d "nWGxne_9WmC6hEr0kuwsxERJxWl7MmkZcDusAxyuf2A",
              :x "11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"})

  (def privkey (keys/jwk->private-key edkey))

  ;; you can also use keys imported from PEM files.

  ;; JWK requires both public and private keys for export
  (def jwk (keys/jwk privkey-rsa pubkey-rsa))
  (def jwk-pub (keys/public-key->jwk pubkey-rsa))

  ;; also, you can generate and save keys in JWK format like this

  (defn generate-keypair-ed25519
    []
    (let [kg (KeyPairGenerator/getInstance "EdDSA")]
      (.initialize kg
                   256
                   (SecureRandom/getInstanceStrong))
      (.genKeyPair kg)))

  (let [pair (generate-keypair-ed25519)]
    (keys/jwk (.getPrivate pair) (.getPublic pair)))

  ;; you can also calculate JWK thumbprint using `jwk-thumbprint` function

  (def edkey {:kty "OKP",
              :crv "Ed25519",
              :d "nWGxne_9WmC6hEr0kuwsxERJxWl7MmkZcDusAxyuf2A",
              :x "11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"})

  (-> (keys/jwk-thumbprint edkey)
      (codecs/bytes->hex))

)


;; digital signatures

(comment

  ;; digital signature algorithms has similar purposes that MAC but comes with
  ;; some tradeoffs such as them provides additional security
  ;; feature (Non-repudiation) with cost in the performance. You can read a
  ;; great explanation about the differences with MAC here:
  ;; http://crypto.stackexchange.com/a/5647


  ;; signing string using rsassa-pss+sha256:

  ;; read private key
  (def privkey (keys/private-key "resources/example_openssl_rsa.pem" "secret"))

  ;; make signature
  (def signature (dsa/sign "foo" {:key privkey :alg :rsassa-pss+sha256}))

  ;; now signature contains a `byte[]` with signature of "foo" string

  ;; verifying signature:

  ;; read public key
  (def pubkey (keys/public-key "resources/example_openssl_rsa.pem.pub"))

  ;; Make verification
  (dsa/verify "foo" signature {:key pubkey :alg :rsassa-pss+sha256})


  ;; here is a table with complete list of supported algorithms and its
  ;; variants:
  ;;   algorithm name   `:alg` keyword value
  ;; - RSASSA-PSS:      `:rsassa-pss+sha256`, `:rsassa-pss+sha384`,
  ;;                    `:rsassa-pss+sha512`
  ;; - RSASSA-PKCS 1.5: `:rsassa-pkcs15+sha256`, `:rsassa-pkcs15+sha384`,
  ;;                    `:rsassa-pkcs15+sha512`
  ;; - ECDSA:           `:ecdsa+sha256`, `:ecdsa+sha384`, `:ecdsa+sha512`

  ;; note: ECDSA algorithm requires EC type of asymentric key pair.

)


;; key derivation functions (KDF)

(comment

  ;; key derivation functions are often used in conjunction with non-secret
  ;; parameters to derive one or more keys from a common secret value.

  ;; buddy comes with several of them:

  ;;   name `:alg` description
  ;; - HKDF: `:hkdf+sha256`, `:hkdf+sha384`, `:hkdf+sha512`: HMAC-based
  ;;   Extract-and-Expand Key Derivation Function
  ;; - KDF1: `:kdf1+sha256`, `:kdf1+sha384`, `:kdf1+sha512`: KDF v1
  ;; - KDF2: `:kdf2+sha256`, `:kdf2+sha384`, `:kdf2+sha512`: KDF v2
  ;; - CMKDF: `:cmkdf+sha256`, `:cmkdf+sha384`, `:cmkdf+sha512`: Counter-Mode key
  ;;   derivation function (as defined in NIST SP800-108)
  ;; - FMKDF: `:fmkdf+sha256`, `:fmkdf+sha384`, `:fmkdf+sha512`: Feedback-Mode
  ;;   key derivation function (as defined in NIST SP800-108)
  ;; - DPIMKDF: `:dpimkdf+sha256`, `:dpimkdf+sha384`, `:dpimkdf+sha512`:
  ;;   Double-Pipeline Iteration Mode key derivation function (as defined in
  ;;   NIST SP800-108)
  ;; - PBKDF2: `:pbkdf2+sha256`, `:pbkdf2+sha384`, `:pbkdf2+sha512`:
  ;;   Password-Based Key Derivation Function 2 (a.k.a. RSA PKCS #5 v2.0, also
  ;;   published in RFC 2898)


  ;; using KDF with HKDF key derivation function:

  ;; hkdf derivation functions require a key, salt and optionally accept info
  ;; field that can contain any random data.

  (def hkdf (kdf/engine {:alg :hkdf+sha256
                         :key "secret"
                         :salt "salt"}))

  (-> (kdf/get-bytes hkdf 8)
      (codecs/bytes->hex))

  ;; or using different digest algorithm:

  (def hkdf (kdf/engine {:alg :hkdf
                         :digest :blake2b-512
                         :key "secret"
                         :salt "salt"}))

  (-> (kdf/get-bytes hkdf 8)
      (codecs/bytes->hex))

  ;; using PBKDF2 with sha256:

  (def pbkdf2 (kdf/engine {:key "secret"
                           :salt (nonce/random-bytes 8)
                           :alg :pbkdf2
                           :digest :sha256
                           :iterations 1}))

  (-> (kdf/get-bytes pbkdf2 8)
      (codecs/bytes->hex))

  ;; WARNING: PBKDF2 works slightly different to the rest of KDF
  ;; implementations. You should pass the number of iterations explicltly and
  ;; `get-bytes` always returns the same value in contrast to the others where
  ;; `get-bytes` works as consumer of infinite stream.

  ;; note the same output for multiple requests:

  (-> (kdf/get-bytes pbkdf2 8)
      (codecs/bytes->hex))

  (-> (kdf/get-bytes pbkdf2 8)
      (codecs/bytes->hex))

  ;; note that each request returns the next bytes of the stream:

  (-> (kdf/get-bytes hkdf 8)
      (codecs/bytes->hex))

  (-> (kdf/get-bytes hkdf 8)
      (codecs/bytes->hex))

  ;; WARNING: this is a low-level kdf primitive and if you want a password
  ;; hasher, please use `buddy-hashers` library instead of this.

)


;; ciphers

(comment

  ;; block ciphers

  ;; in cryptography, a block cipher is a deterministic algorithm operating on
  ;; fixed-length groups of bits, called blocks, with an unvarying
  ;; transformation that is specified by a symmetric key.

  ;; this is a table of currently supported block ciphers in buddy-core:
  ;; algorithm name keyword
  ;; - AES:         `:aes`
  ;; - Twofish:     `:twofish`
  ;; - Blowfish:    `:blowfish`

  ;; additionally, for good security, is mandatory to combine a block cipher
  ;; with some cipher mode of operation.

  ;; this is a table of currently supported of cipher mode of operation:
  ;; algorithm name 	keyword
  ;; - SIC (CTR): `:ctr`, `:sic`
  ;; - CBC:       `:cbc`
  ;; - OFB:       `:ofb`
  ;; - GCM:       `:gcm`

  ;; NOTE: currently buddy comes with limited number of ciphers and modes, but
  ;; in near future more many more options should be added.


  ;; encrypting and decrypting:

  (def iv16 (nonce/random-nonce 16))
  (def key32 (nonce/random-nonce 32))
  (def data (codecs/hex->bytes "000000000000000000000000000000AA"))

  (def encrypted
    (let [eng (crypto/block-cipher :twofish :cbc)]
      (crypto/init! eng {:key key32 :iv iv16 :op :encrypt})
      (crypto/process-block! eng data)))

  (codecs/bytes->hex encrypted)

  (def decrypted
    (let [eng (crypto/block-cipher :twofish :cbc)]
      (crypto/init! eng {:key key32 :iv iv16 :op :decrypt})
      (crypto/process-block! eng encrypted)))

  (codecs/bytes->hex decrypted)

  ;; AEAD mode of operations also exposes additional function for calculate the
  ;; total size of the output including the authentication tag: `output-size`.


  ;; stream ciphers

  ;; stream ciphers differ from block ciphers, in that they works with arbitrary
  ;; length input and do not require any additional mode of operation.

  ;; this is a table of currently supported of stream ciphers in buddy:
  ;; algorithm name keyword
  ;; - ChaCha:      `:chacha`


  ;; encrypting and decrypting:

  (def iv8 (nonce/random-nonce 8))
  (def key32 (nonce/random-nonce 32))
  (def data (codecs/hex->bytes "0011"))

  (def encrypted
    (let [eng (crypto/stream-cipher :chacha)]
      (crypto/init! eng {:key key32 :iv iv8 :op :encrypt})
      (crypto/process-bytes! eng data)))

  (codecs/bytes->hex encrypted)

  (def decrypted
    (let [eng (crypto/stream-cipher :chacha)]
      (crypto/init! eng {:key key32 :iv iv8 :op :decrypt})
      (crypto/process-bytes! eng encrypted)))

  (codecs/bytes->hex decrypted)


  ;; NOTE: the `:iv` and `:key` size depends estrictly on cipher engine, in this
  ;; case, chacha engine requires 8 bytes iv.

  ;; NOTE: for decrypt, only change `:op` value to `:decrypt`

  ;; NOTE: You can call `crypto/init!` as many times as you want, it simply
  ;; reinitializes the engine.


  ;; high level encryption schemes

  ;; since version 0.6.0, `buddy-core` comes with high-level crypto interface
  ;; that allows user encrypt arbitrary length data using one of the well
  ;; established encryption schemes.

  ;; the api consists in two simple functions. Let see an example of how to
  ;; encrypt arbitrary length text and decrypt it:

  (def original-text
    (codecs/to-bytes "Hello World."))

  (def iv (nonce/random-bytes 16))
  (def key (hashes/sha256 "secret"))

  ;; encrypt the original text using previously declared `iv` and `key`.

  (def encrypted
    (crypto/encrypt original-text key iv {:alg :aes128-cbc-hmac-sha256}))

  ;; and now, decrypt it using the same parameters:

  (-> (crypto/decrypt encrypted key iv {:alg :aes128-cbc-hmac-sha256})
      (codecs/bytes->str))


  ;; a complete list of currently supported encryption schemes:

  ;; - `:aes128-cbc-hmac-sha256` (default)
  ;; - `:aes192-cbc-hmac-sha384`
  ;; - `:aes256-cbc-hmac-sha512`
  ;; - `:aes128-gcm`
  ;; - `:aes192-gcm`
  ;; - `:aes256-gcm`

)


;; paddings

(comment

  ;; padding schemes are often used for fill the empty bytes of byte array of
  ;; data to an concrete blocksize.

  ;; this is a table of currently supported padding schemes:
  ;; algorithm name keyword
  ;; - Zero Byte:   `:zerobyte`
  ;; - PKCS7:       `:pkcs7`
  ;; - TBC:         `:tbc`


  ;; let see an example on how to use it:

  (def data (byte-array 10))

  ;; fill the array with byte value 10

  (bytes/fill! data 10)

  ;; add padding to the byte array with offset value 7. This is a side effect
  ;; and it will mutate the data byte array.

  (padding/pad! data 7 :pkcs7)
  (vec data)

  ;; also it has the side effect-free version of it, that returns a new byte
  ;; array.

  (vec (padding/pad data 7 :pkcs7))

  ;; show the size of applied padding

  (padding/count data :pkcs7)

  ;; remove the padding

  (vec (padding/unpad data :pkcs7))

  ;; the default padding scheme is `:pkcs7` and that parameter can be ommited.

)


;; nonces and salts

(comment

  ;; this library comes with helpers for generate random salts and
  ;; cryptographically secure nonces.


  ;; generate a cryptographically secure nonce:

  (vec (nonce/random-nonce 16))
  (vec (nonce/random-nonce 32))

  ;; the `random-nonce` function returns a byte array with minimum length of 8
  ;; bytes, because is the size of the current time in miliseconds.


  ;; generate a cryptographically secure salt:

  (vec (nonce/random-bytes 1))
  (vec (nonce/random-bytes 16))

  ;; like `random-nonce` function, `random-bytes` returns a byte array but it
  ;; not have the limitation of minimum 8 bytes of size.

)


;; certificates (X.509)

(comment

  ;; you can load certificates, check date validity, and check to see if a
  ;; certificate is signed by a known public key.

  (def cert (certs/certificate "resources/example_cert.crt"))

  (certs/valid-on-date? cert)

  ;; self-signed, ok
  (certs/verify-signature cert (certs/certificate "resources/example_cert.crt"))

)


;; codecs and bytes

(comment

  ;; this library comes with helpers for working with codecs (hex, base64, …)
  ;; and byte arrays.

  ;; a brief list of available functions:
  ;; namespace/function 	description
  ;; - `buddy.core.codecs/str->bytes`: converts a string into byte array
  ;; - `buddy.core.codecs/bytes->str`: converts byte array to string using
  ;;   UTF8 encoding
  ;; - `buddy.core.codecs/bytes->hex`: converts byte array to hexadecimal string
  ;; - `buddy.core.codecs/hex->bytes`: converts hexadecimal strings into byte
  ;;   array
  ;; - `buddy.core.codecs/bytes->b64`: encodes byte array to base64 byte array
  ;; - `buddy.core.codecs/b64->bytes`: decodes base64 byte array into byte array
  ;; - `buddy.core.codecs/bytes->b64u` encodes byte array to base64 byte array
  ;;   (using url-safe variant)
  ;; - `buddy.core.codecs/b64u->bytes`: decodes base64 byte array into byte array
  ;;   (using url-safe variant)
  ;; - `buddy.core.codecs/long->bytes`: get byte array representation of long
  ;; - `buddy.core.codecs/bytes->long`: get long from byte array
  ;; - `buddy.core.bytes/bytes?` predicate to test byte arrays
  ;; - `buddy.core.bytes/fill!`: fill byte array with data
  ;; - `buddy.core.bytes/slice`: create a new byte array as slice of other
  ;; - `buddy.core.bytes/copy`: copy the byte array
  ;; - `buddy.core.bytes/equals?`: constant time equals predicate for byte arrays
  ;; - `buddy.core.bytes/concat`: concat two or more byte arrays

)
