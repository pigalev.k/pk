(ns hello-buddy.hashers
  (:require
   [buddy.hashers :as hashers]))


;; A collection of secure password hashers

;; http://funcool.github.io/buddy-hashers/latest/index.html


;; user guide

(comment

  ;; provides a collection of secure password hashers with lightweight and
  ;; extensible abstraction for build powerful password authentication
  ;; processes.


  ;; supported password hashers algorithms:
  ;; identifier            can i use it? description
  ;; `:bcrypt+blake2b-512` recommended   BCrypt password hasher combined with
  ;;                                     blake2b-512
  ;; `:argon2id`           recommended   Argon2id password hasher
  ;; `:pbkdf2+blake2b-512` yes           Password-Based Key Derivation Function
  ;;                                     2 with blake2b-512
  ;; `:pbkdf2+sha512`      yes           Password-Based Key Derivation Function
  ;;                                     2 with SHA256
  ;; `:pbkdf2+sha3-256`    yes           Password-Based Key Derivation Function
  ;;                                     2 with SHA3-256
  ;; `:bcrypt+sha512`      yes           BCrypt password hasher combined with
  ;;                                     sha512 (default)
  ;; `:pbkdf2+sha256`:     yes           Password-Based Key Derivation Function
  ;;                                     2 with SHA256
  ;; `:bcrypt+sha384`      yes           BCrypt password hasher combined with
  ;;                                     sha384
  ;; `:pbkdf2+sha1`        yes           Password-Based Key Derivation Function
  ;;                                     2 (as defined in RFC2898)
  ;; `:scrypt`             yes           Password-Based Key Derivation Function
  ;;                                     created by Colin Percival
  ;; `:argon2id`           yes           Memory-Hard Key Derivation Function for
  ;;                                     password hashing and other applications


  ;; the public interface consists of two main parts: `derive` and
  ;; `check`/`verify` (latter also accepts additional options and returns
  ;; suggestion about password update).


  ;; generate hash from plain password

  (def a-hash (hashers/derive "secretpassword"))

  ;; verify a password string with the generated hash

  (hashers/check "nope" a-hash)
  (hashers/verify "nope" a-hash)
  (hashers/check "secretpassword" a-hash)
  (hashers/verify "secretpassword" a-hash)

  ;; if no algorithm is specified, the `:bcrypt+sha512` will be used by
  ;; default. We highly recommend setting your own default for prevent any
  ;; unexpected situations when the library changes the default.

  ;; if you want to use a specific one, you can specify it using the optional
  ;; options parameter:

  (def a-hash (hashers/derive "secretpassword" {:alg :pbkdf2+sha256}))
  (hashers/verify "nope" a-hash)
  (hashers/verify "secretpassword" a-hash)


  ;; advanced options

  ;; algorithm tuning params

  ;; each algorithm can be tweaked passing additional parameters on the second
  ;; argument to `derive` function. Options vary depending on the used
  ;; algorithm.

  ;; algorithm             available options      defaults
  ;; `:bcrypt+blake2b-512` `:salt`, `:iterations` iterations=12,
  ;;                                              salt=(random 16 bytes)
  ;; `:bcrypt+sha384`      `:salt`, `:iterations` iterations=12,
  ;;                                              salt=(random 16 bytes)
  ;; `:pbkdf2+blake2b-512` `:salt`, `:iterations` iterations=50000,
  ;;                                              salt=(random 12 bytes)
  ;; `:pbkdf2+sha512`      `:salt`, `:iterations` iterations=100000,
  ;;                                              salt=(random 12 bytes)
  ;; `:pbkdf2+sha3-256`    `:salt`, `:iterations` iterations=100000,
  ;;                                              salt=(random 12 bytes)
  ;; `:pbkdf2+sha1`        `:salt`, `:iterations` iterations=100000,
  ;;                                              salt=(random 12 bytes)
  ;; `:scrypt`             `:salt`, `:cpucost`,   salt=(random 12 bytes),
  ;;                       `:memcost`,            cpucost=65536,
  ;;                       `:parallelism`         memcost=8, parallelism=1
  ;; `:bcrypt+sha512`      `:salt`, `:iterations` iterations=12,
  ;;                                              salt=(random 12 bytes)
  ;; `:pbkdf2+sha256`      `:salt`, `:iterations` iterations=100000,
  ;;                                              salt=(random 12 bytes)
  ;; `:argon2id`           `:salt`, `:memory`,    salt=(random 16 bytes),
  ;;                       `:iterations`,         memory=65536,
  ;;                       `:parallelism`         iterations=2, parallelism=1


  ;; limiting algorithms

  ;; sometimes you don’t want to use all the supported algorithms and you only
  ;; want to use a own set of algorithms in the password check process. That can
  ;; be done passing additional parameter to the check function:

  (def trusted-algs #{:pbkdf2+sha256 :bcrypt+sha512})

  (def incoming-pwd "secretpassword")
  (def derived-pwd (hashers/derive incoming-pwd {:alg :argon2id}))

  (hashers/verify incoming-pwd derived-pwd)
  (hashers/verify incoming-pwd derived-pwd {:limit trusted-algs})

  ;; the `verify` function will return false if the incoming password uses an
  ;; algorithm that is not allowed.


  ;; password updating

  ;; choice of a strong algorithm is important thing, but have a good update
  ;; password-hashes policy is also very important and usually completely
  ;; forgotten. The password generated 3 years ago is potentially weaker that
  ;; one generated today. `buddy-hashers` comes with a solution for make this
  ;; task easier.

  ;; note: seems like this feature is not about arbitrary password-updating
  ;; policies, but takes in consideration only technical characteristics of the
  ;; derived password itself.

  ;; the object returned by the `verify` function contains a prop `:update` that
  ;; indicates if the derived password should be updated or not (if it passes
  ;; certain algorithm-specific quality requirements like length or number of
  ;; iterations).

  (def incoming-pwd "secretpassword")

  ;; recommended minimum for the default algorithm is 12 iterations
  (def derived-pwd (hashers/derive incoming-pwd {:iterations 4}))

  ;; so `verify` will suggest to update the derived password
  (let [result (hashers/verify incoming-pwd derived-pwd)]
    (when (:valid result)
      (when (:update result)
        (printf
         "Derived password updated, new derived password for '%s': %s\n"
         incoming-pwd (hashers/derive incoming-pwd {:iterations 15})))))

)
