# hello-signals

Exploring libraries that implement input data (signals), derived data, and
effects --- as building blocks to implement reactive systems.

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
