(ns hello-signals.signaali
  (:require [signaali.reactive :as sr]))


;; input data and derived data

(def signal (sr/create-signal "Sig the arctic fox"))
@signal

(def derived (sr/create-derived (fn [] (str "Hello, " @signal "!"))))
@derived

(reset! signal "Sig naali")
@derived


;; effects

(def effect (sr/create-effect (fn [] (prn @derived))))

;; can be run by hand
;; prints derived if signal was changed (once)
@effect

;; or added to the list of effects to run later
(sr/enlist-stale-effectful-node effect)

;; nothing is printed
(reset! signal "Bob")

;; prints derived if signal was changed (once)
(sr/re-run-stale-effectful-nodes)


;; clean up

(sr/dispose effect)

;; prints nothing
(reset! signal "Foobar")
(sr/re-run-stale-effectful-nodes)
