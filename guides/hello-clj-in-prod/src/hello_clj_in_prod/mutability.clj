(ns hello-clj-in-prod.mutability
  (:require
   [clojure.pprint :as p]))


;; mutability and state management

(comment

  ;; atoms

  (def store (atom 3))
  @store

  (reset! store 4)
  (swap! store inc)

  (set-validator! store (complement neg?))
  (reset! store -1)
  (reset! store :a)

  (add-watch
   store
   :debug (fn [key atom old-val new-val]
            (printf "Watcher '%s': changing %s (value %s) from %s to %s\n"
                                    key atom @atom old-val new-val)))

  (swap! store inc)


  ;; volatile (no watchers, no validators, not thread-safe, but faster)

  (def vstore (volatile! 3))
  @vstore

  (vreset! vstore 4)
  (vswap! vstore inc)


  ;; transients

  (let [items* (transient [1 2 3])]
    (conj! items* :a)
    (conj! items* :b)
    (pop! items*)
    (persistent! items*))

  ;; the same
  (-> [1 2 3]
      (transient)
      (conj! :a)
      (conj! :b)
      (pop!)
      (persistent!))


  (let [params* (transient {:a 1})]
    (assoc! params* :b 2)
    (assoc! params* :c 3)
    (dissoc! params* :b)
    (persistent! params*))

  ;; the same
  (-> {:a 1}
      (transient)
      (assoc! :b 2)
      (assoc! :c 3)
      (dissoc! :b)
      (persistent!))

  ;; can't be used after call to `persistent!`

  (let [params* (transient {:a 1})]
    (assoc! params* :b 2)
    (persistent! params*)
    (assoc! params* :c 3))


  ;; vars

  (def a 1)
  (alter-var-root #'a inc)

  ;; monkey-patching

  (def data [{:foo 42
              :bar [1 2 3 4 5 6 7 8 9 0 {:foo 42
                                         :bar [1 2 3 4 5 6 7 8 9 0 {4 3}]}]}])

  (println data)

  (def old-println println)
  (alter-var-root #'println (constantly p/pprint))
  (println data)
  (old-println data)


  ;; assignment with `set!`

  ;; used to set thread-local-bound vars, Java object instance fields, and Java
  ;; class static fields

  ;; nope

  (def a 1)
  (set! a 2)
  (def ^:dynamic *b* 2)
  (set! *b* 3)

  ;; yar

  (binding [*b* 4]
    (println *b*)
    (set! *b* 12)
    (println *b*))

  (set! *print-length* 3)

  (def data [{:foo 42
              :bar [1 2 3 4 5 6 7 8 9 0 {:foo 42
                                         :bar [1 2 3 4 5 6 7 8 9 0 {4 3}]}]}])

  (println data)
  (old-println data)
  (set! *print-length* nil)


  ;; some Clojure dynamic vars

  *print-level*
  *print-length*
  *warn-on-reflection*
  *assert*
  *print-meta*


  ;; thread-local `binding`: dynamic vars only

  (binding [*print-meta* true]
    (println (var +)))


  (def ^:dynamic *locale*)
  (def tr-map {:en {:hello "Hello"
                    :world "World"}
               :ru {:hello "Привет"
                    :world "Мир"}})

  (defn tr [tag]
    (get-in tr-map [*locale* tag]))

  (str (tr :hello) ", " (tr :world) "!")

  (defmacro with-locale
    [locale & body]
    `(binding [*locale* ~locale]
       ~@body))

  (with-locale :en
    (str (tr :hello) ", " (tr :world) "!"))

  (with-locale :ru
    (str (tr :hello) ", " (tr :world) "!"))


  ;; local vars: `var-set` and `deref`

  (with-local-vars [a 0] a)
  (with-local-vars [a 0] @a)
  (with-local-vars [a 0 b 2]
    (var-set a (* 4 @b))
    @a)


  ;; global `with-redefs`: until the end of block

  (with-redefs [println
                (fn [& _]
                  (print "fake print\n"))]
    (println {:some "data"}))

  ;; in other threads too

  (with-redefs
    [println (constantly (print "fake print\n"))]
    @(future (println "hello")))

)
