(ns hello-clj-in-prod.web
  (:require
   [bidi.bidi :as bidi]
   [clj-http.client :as client]
   [clojure.walk :as w]
   [compojure.core :as c]
   [reitit.core :as r]
   [reitit.ring :as ring]
   [ring.adapter.jetty :as jetty]
   [ring.middleware.cookies :as cookies]
   [ring.middleware.json :as json]
   [ring.middleware.keyword-params :as kp]
   [ring.middleware.params :as p]
   [ring.middleware.session :as session]
   [ring.mock.request :as mock]
   [ring.util.response :as resp])
  (:import (java.util UUID)))


;; handling HTTP with Ring


;; requests, responses, handlers and adapters

(defonce server (atom nil))

(defn start!
  [handler]
  (reset! server (jetty/run-jetty handler {:port 3000 :join? false})))

(defn stop! []
  (when @server
    (.stop @server)))

(comment

  ;; handler is a function producing response from request

  (defn handler [request]
    (let [{:keys [uri request-method headers]} request
          {:strs [accept]} headers]
      {:status 200
       :headers {"Content-Type" "text/plain"}
       :body (format "You requested %s %s, accept %s"
                     (-> request-method name .toUpperCase)
                     uri
                     accept)}))

  ;; request and response are maps that have a certain structure

  (handler {:request-method :get
            :uri "/index.html"
            :headers {"accept" "text/plain"}})

  ;; adapter is the code that can interact with a HTTP server implementation:
  ;; - get raw HTTP request from the HTTP server
  ;; - transform it into request map
  ;; - pass it to a handler, receive a response map back
  ;; - transform it to raw HTTP response and pass it back to the HTTP server

  (do
    (stop!)
    (start! handler))

  ;; curl -H 'Accept: text/plain' http://localhost:3000/hello


)


;; routing

(comment

  ;; compojure

  ;; 1. define handlers

  (defn page-index
    [request]
    (resp/response "learning clojure"))

  (defn page-hello
    [request]
    (let [name (get-in request [:params :name])]
      (resp/response (str "hello, " (or name "anonymous")))))

  ;; 2. define routes

  (c/defroutes app
    (c/GET "/" request (page-index request))
    (c/context "/hello" []
               (c/GET "/" request (page-hello request))
               (c/GET "/:name" [name :as request] (page-hello request)))
    (c/ANY "/health" _ "ok")
    (constantly (resp/not-found nil)))


  (app (mock/request :get "/"))
  (app (mock/request :get "/hello"))
  (app (mock/request :get "/hello/joe"))
  (app (mock/request :get "/health"))
  (app (mock/request :get "/nope"))


  ;; bidi

  ;; 1. describe routes

  (def routes
    ["/" {""               :page-index
          "hello"          :page-hello
          ["hello/" :name] :page-hello
          "health"         :health
          true             :not-found}])

  ;; matches and returns handler name
  (bidi/match-route routes "/hello")
  (bidi/match-route routes "/hello/joe")
  (bidi/match-route routes "/nope")

  (def hello-request (mock/request :get "/hello"))

  ;; matches and puts handler name in request
  (bidi/match-route* routes (:uri hello-request) hello-request)

  ;; 2. define middleware that adds handler to request

  (defn wrap-add-handler [handler]
    (fn [request]
      (let [{:keys [uri]} request
            request*      (bidi/match-route* routes uri request)]
        (handler request*))))

  (def wrapped (wrap-add-handler identity))
  (wrapped hello-request)

  ;; 3. define router multimethod

  (defmulti router
    :handler)

  (defmethod router :page-index
    [request]
    (resp/response "learning clojure"))

  (defmethod router :page-hello
    [request]
    (let [name (-> request :route-params :name)]
      (resp/response (str "hello, " (or name "anonymous")))))

  (defmethod router :health
    [request]
    (resp/response "ok"))

  (defmethod router :not-found
    [request]
    (resp/not-found nil))

  ;; 4. use the router
  (def app
    (-> router
        wrap-add-handler))

  (app (mock/request :get "/"))
  (app (mock/request :get "/hello"))
  (app (mock/request :get "/hello/joe"))
  (app (mock/request :get "/health"))
  (app (mock/request :get "/nope"))


  ;; reitit (reitit-ring)

  ;; 1. define handlers

  (defn page-index
    [request]
    (resp/response "learning clojure"))

  (defn page-hello
    [request]
    (let [name (get-in request [::r/match :path-params :name])]
      (resp/response (str "hello, " (or name "anonymous")))))

  (defn health
    [request]
    (resp/response "ok"))

  ;; 2. describe routes

  (def routes
    [["/" page-index]
     ["/hello"
      ["" {:name ::hello-anonymous
           :get  page-hello}]
      ["/:name" {:name ::hello-user
                 :get  page-hello}]]
     ["/health" {:name ::health
                 :get  health}]])

  ;; 3. define router

  (def router (ring/router routes))

  ;; 4. use the router

  (def app
    (ring/ring-handler
     router
     (ring/routes
      (ring/redirect-trailing-slash-handler)
      (ring/create-default-handler))))

  (app (mock/request :get "/"))
  (app (mock/request :get "/hello"))
  (app (mock/request :get "/hello/joe"))
  (app (mock/request :get "/health"))
  (app (mock/request :get "/nope"))

)


;; middleware pattern

(comment

  ;; middleware, decorators, advice --- the Decorator pattern:
  ;; wrap the function with additional functionality

  (defn wrap-echo [func]
    (fn [& args]
      (apply println "The args are" args)
      (let [result (apply func args)]
        (println "The result is" result)
        result)))

  (def +* (wrap-echo +))
  (+* 1 2 3)

  (defn wrap-catch [func]
    (fn [& args]
      (try
        (apply func args)
        (catch Throwable e (.getMessage e)))))


  ;; stacking middleware

  (def safe-echoing-division
    (-> /
        wrap-catch
        wrap-echo))

  (safe-echoing-division 41 2)
  (safe-echoing-division 1 0)


  ;; composing middleware

  (def wrap-catch-echo
    (comp wrap-catch wrap-echo))

  (def safe-echoing-division*
    (-> /
        wrap-catch-echo))

  (safe-echoing-division 41 2)
  (safe-echoing-division 1 0)

)


;; Ring middleware

(comment

  ;; it wraps handlers

  (defn handler [{:keys [params]}]
    (resp/response {:params params}))


  ;; stacking Ring middleware

  (def app
    (-> handler
        kp/wrap-keyword-params
        p/wrap-params))

  (app (mock/request :get "hello?name=Joe"))
  (app (-> (mock/request :get "hello?name=Joe")
           (mock/body {"also" "barnacles"})))


  ;; composing Ring middleware

  (def wrap-params+
    (comp p/wrap-params kp/wrap-keyword-params))

  (def app
    (-> handler
        wrap-params+))

  (app (mock/request :get "hello?name=Joe"))
  (app (-> (mock/request :get "hello?name=Joe")
           (mock/body {"also" "barnacles"})))

)


;; built-in Ring middleware

(comment

  ;; cookies

  (defn page-seen [request]
    (let [{:keys [cookies]} request
          seen-path ["seen" :value]
          seen? (get-in cookies seen-path)
          cookies* (assoc cookies "seen"
                          {:value true :http-only true})]
      {:status 200
       :cookies cookies*
       :body (if seen? "Already seen." "The first time you see it!")}))

  (def app
    (-> page-seen
        cookies/wrap-cookies))

  (do
    (stop!)
    (start! app))

  ;; open http://localhost:3000 in a browser, refresh, inspect cookies


  ;; sessions

  (defn page-counter [request]
    (let [{:keys [session]} request
          session* (update session :counter (fnil inc 0))
          counter (:counter session*)]
      {:status 200
       :session session*
       :body (format "Seen %s time(s)" counter)}))

  (def app
    (-> page-counter
        session/wrap-session))

  (do
    (stop!)
    (start! app))

  ;; open http://localhost:3000 in a browser, refresh several times


  ;; JSON params/body/response

  ;; returning JSON response

  (defn get-user-by-id [id]
    (if (> 0.5 (rand))
      nil
      {:user/id id
       :person/name "Joe"
       :person/email "joe@example.com"}))

  (defn page-data [request]
    (let [user-id (-> request :params :id)]
      (if-let [user (get-user-by-id user-id)]
        {:status 200 :body user}
        {:status 404
         :body {:error_code "MISSING_USER"
                :error_message "No such user"}})))

  (def app
    (-> page-data
        kp/wrap-keyword-params
        p/wrap-params
        json/wrap-json-response))

  (do
    (stop!)
    (start! app))

  ;; open http://localhost:3000?id=12 in a browser, see the JSON
  ;; response (success or error)

  ;; parsing JSON body

  (defn create-user [username city] {})

  (defn page-body [request]
    (let [{:keys [body]} request
          {:keys [username city]} body]
      (create-user username city)
      {:status 200
       :body {:code "CREATED"
              :message (str "User created: " username)}}))

  (def app
    (-> page-body
        (json/wrap-json-body {:keywords? true})
        json/wrap-json-response))

  (do
    (stop!)
    (start! app))

  ;; curl -X POST -H 'Content-Type: application/json' -d '{"username":"John", "city":"NY"}' http://localhost:3000

)


;; custom Ring middleware

(comment

  ;; add request id

  (defn handler [_]
    (resp/response "ok"))

  (defn wrap-request-id [handler]
    (fn [request]
      (let [path [:headers :x-request-id]
            uuid (or (get-in request path)
                     (str (UUID/randomUUID)))]
        (-> request
            (assoc-in path uuid)
            (assoc :request-id uuid)
            handler
            (assoc :request-id uuid)
            (assoc-in path uuid)))))

  (def app
    (-> handler
        wrap-request-id))

  (handler (mock/request :get "/hello"))
  (app (mock/request :get "/hello"))


  ;; keywordize header names

  (defn handler [{:keys [headers]}]
    (resp/response {:headers headers}))

  (defn wrap-keyword-headers [handler]
    (fn [request]
      (-> request
          (update :headers w/keywordize-keys)
          handler
          (update :headers w/stringify-keys))))

  (def app
    (-> handler
        wrap-keyword-headers))

  (handler (-> (mock/request :get "/hello")
               (mock/header "X-Request-Id" "12345")))
  (app (-> (mock/request :get "/hello")
           (mock/header "X-Request-Id" "12345")))


  ;; interrupting request handling

  (defn handler [_]
    (resp/response "ok"))

  (defn wrap-auth-user-only [handler]
    (fn [request]
      (if (:user request)
        (handler request)
        {:status 403
         :headers {"content-type" "text/plain"}
         :body "Please sign in to access this page."})))

  (def app
    (-> handler
        wrap-auth-user-only))

  (handler (mock/request :get "/hello"))
  (app (mock/request :get "/hello"))


  ;; handling exceptions

  ;; there can be several exception-handling middleware on a stack ---
  ;; e.g. catching business exceptions just before handler, and catching
  ;; technical errors at the top of the stack.

  (defn handler [_]
    (throw (ex-info "Boom!" {:cause :boredom})))

  (defn wrap-exception [handler]
    (fn [request]
      (try
        (handler request)
        (catch Throwable e
          (let [{:keys [uri request-method]} request]
            (printf "Error: %s, method: %s, path: %s\n"
                    (.getMessage e) request-method uri)
            {:status 500
             :headers {"content-type" "text/plain"}
             :body "Sorry, please try later."})))))

  (def app
    (-> handler
        wrap-exception))

  (handler (mock/request :get "/hello"))
  (app (mock/request :get "/hello"))

)


;; misc

(comment

  ;; proxying streaming responses

  (defn app-proxy [request]
    (let [opt {:stream? true}
          response (client/get "https://ya.ru" opt)
          {:keys [status headers body]} response
          headers* (select-keys headers ["Content-Type"])]
      {:status status
       :headers headers*
       :body body}))

  ;; or the same

  (defn app-proxy [request]
    (-> "https://ya.ru"
        (client/get {:stream? true})
        (select-keys [:status :body :headers])
        (update :headers select-keys ["Content-Type"])))

 (do
   (stop!)
   (start! app-proxy))

  ;; open http://localhost:3000 in a browser, see the Yandex start page

)
