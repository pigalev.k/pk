(ns hello-clj-in-prod.exceptions
  (:require
   [clojure.pprint :as pprint]
   [clojure.spec.alpha :as s]
   [clojure.stacktrace :as trace]
   [clojure.tools.logging :as log])
  (:import (java.io IOException)))


;; using exceptions


(comment

  ;; Java exceptions are organized into hierarchy by inheritance

  (supers IOException)


  ;; exceptions can be organized in causal chains: when function catches the
  ;; exception it cannot deal with (what can be decided from context), it throws
  ;; a new exception (probably including more info from context) with the
  ;; original one as a cause.

  ;; systems usually have an outermost exception handler (handler of last
  ;; resort) to catch abnormal behaviors what were not handled somewhere else,
  ;; to avoid program termination and have a last chance to e.g. log the
  ;; anomaly.


  ;; in Clojure exceptions are created with `ex-info`

  (defn ex-chain
    "Returns a cause chain for the exception `e`."
    [e]
    (loop [e e result []]
      (if (nil? e)
        result
        (recur (ex-cause e) (conj result e)))))

  (def ex (ex-info "Oops!"
                   {:value :none}
                   (ex-info "Ouch!" {:value :one})))

  (map ex-message (ex-chain ex))

  (keys (bean ex))

  (ex-message ex)
  (ex-data ex)
  (ex-message (ex-cause ex))
  (ex-cause (ex-cause ex))


  ;; exceptions can be thrown and caught

  (try
    (/ 1 0)
    (catch ArithmeticException e
      (println (ex-message e)))
    (finally
      (println "done.")))

  (try
    (+ 1 nil)
    (catch ArithmeticException e
      (println "Weird arithmetics"))
    (catch NullPointerException e
      (println "You've got a null value")))

  (try
    [(/ 1 0) (+ 1 nil)]
    (catch Throwable e
      (println "I catch everything!")))


  ;; throwing and handling exceptions


  ;; retrieving exception data

  (defn get-user
    [id]
    (throw (ex-info
            "Cannot fetch user."
            {:user-id     id
             :http-status 404
             :http-method "GET"
             :http-url    (format "https://host.com/users/%s" id)})))

  (try
    (get-user 5)
    (catch clojure.lang.ExceptionInfo e
      (let [{:keys [http-method http-url]} (ex-data e)]
        (format "HTTP error: %s %s" http-method http-url))))


  ;; using spec to validate values and generate exception data

  (s/def ::data (s/coll-of int?))

  (when-let [explanation (s/explain-data ::data [1 2 nil])]
    (throw (ex-info "Some item is not an integer"
                    {:explanation explanation})))

  (s/check-asserts true)
  (s/assert ::data [1 2 3])
  (s/assert ::data [0 1 2 nil])

  ;; some helpers for exception logging

  (with-out-str
    (trace/print-throwable (ex-info "Oops!" {:value :none})))

  (with-out-str
    (trace/print-stack-trace (ex-info "Oops!" {:value :none}) 3))

  (with-out-str
    (trace/print-cause-trace (ex-info "Oops!" {:value :none}) 2))


  ;; logging

  ;; `tools.logging` using logback backend

  (log/info "A message from my module")

  (def ex (ex-info "User not found" {:user-id 5}
                   (ex-info "Connection refused" {:host "localhost"
                                                  :port 3000})))
  (log/error ex "HTTP Error")

  (defn ex-print
    [^Throwable e]
    (let [indent " "]
      (doseq [e (ex-chain e)]
        (println (-> e class .getCanonicalName))
        (print indent)
        (println (ex-message e))
        (when-let [data (ex-data e)]
          (print indent)
          (pprint/pprint data)))))

  (ex-print ex)

  (defn log-error
    [^Throwable e & [^String message]]
    (log/error
     (with-out-str
       (println (or message "Error"))
       (ex-print e))))

  (log-error ex "HTTP Error 500")

)
