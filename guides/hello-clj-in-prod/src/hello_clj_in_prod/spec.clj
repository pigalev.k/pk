(ns hello-clj-in-prod.spec
  (:require
   [clojure.spec.alpha :as s]
   [clojure.string :as string]
   [expound.alpha :as expound]
   [java-time.api :as jt]
   [muuntaja.core :as m]
   [spec-tools.core :as st]))


;; basics


;; defining specs

(s/def ::string string?)

(s/def ::not-empty-string
  (fn [val]
    (and (string? val)
         (seq val))))

(s/def ::not-empty-string*
  (every-pred string? not-empty))

(s/def ::status #{:todo :in-progress :done})

(comment

  ;; get the spec value

  (s/get-spec ::string)


  ;; validating data

  (s/valid? ::string 1)
  (s/valid? ::string "test")

  (s/valid? ::not-empty-string "test")
  (s/valid? ::not-empty-string "")

  (s/valid? ::not-empty-string* "test")
  (s/valid? ::not-empty-string* "")


  ;; avoiding exceptions

  (s/def ::url
    (partial re-matches #"(?i)^http(s?)://.*"))

  (s/valid? ::url "test")
  (s/valid? ::url "http://test.com")
  ;; throws
  (s/valid? ::url nil)

  (s/def ::url
    (s/and ::not-empty-string
           (partial re-matches #"(?i)^http(s?)://.*")))

  ;; does not throw
  (s/valid? ::url nil)


  ;; conforming data

  (s/def ::->int
    (s/conformer
     (fn [value]
       (try
         (Integer/parseInt value)
         (catch Exception e
           ::s/invalid)))))

  (s/conform ::->int "42")
  (s/conform ::->int "nope")


  ;; combining validation and conforming

  (s/def ::->int*
    (s/and ::not-empty-string ::->int))

  (s/conform ::->int* "123")
  (s/conform ::->int* nil)


  (s/def ::->local-date
    (s/and ::not-empty-string
           (s/conformer
            (fn [value]
              (try
                (jt/local-date "yyyy.MM.dd" value)
                (catch Exception e
                  ::s/invalid))))))

  (s/conform ::->local-date "2019.12.31")
  (s/conform ::->local-date "2019.19.19.19")


  (def bits-map {"32" 32 "64" 64})

  (s/def ::->bits
    (s/conformer
     #(get bits-map % ::s/invalid)))

  (s/conform ::->bits "32")
  (s/conform ::->bits "42")


  (s/def ::->bool
    (s/and ::not-empty-string
           (s/conformer string/lower-case)
           (s/conformer
            (fn [value]
              (case value
                ("true" "1" "on" "yes")  true
                ("false" "0" "off" "no") false
                ::s/invalid)))))

  (s/conform ::->bool "True")
  (s/conform ::->bool "yes")
  (s/conform ::->bool "0")
  (s/conform ::->bool "FALSE")
  (s/conform ::->bool "nope")
  (s/conform ::->bool nil)


  ;; convenience helpers

  ;; enum

  (defn enum [& args]
    (let [arg-set (set args)]
      (fn [value]
        (contains? arg-set value))))

  (s/def ::status
    (enum :todo :in-progress :done))

  (s/conform ::status :todo)
  (s/conform ::status :nope)

  ;; with-conformer

  (defmacro with-conformer
    [[bind] & body]
    `(s/conformer
      (fn [~bind]
        (try
          ~@body
          (catch Exception e#
            ::s/invalid)))))

  (s/def ::->int
    (s/and ::not-empty-string
           (with-conformer [val]
             (Integer/parseInt val))))

  (s/conform ::->int "123")
  (s/conform ::->int "nope")


  ;; branching

  (s/def ::smart-port
    (s/or :string ::->int :num int?))

  (s/conform ::smart-port 8080)
  (s/conform ::smart-port "8080")


  ;; unforming

  (s/def ::->int
    (s/and ::not-empty-string
           (s/conformer
            (fn [string] ;; conform func
              (Integer/parseInt string))
            (fn [integer] ;; unform func
              (str integer)))))

  (s/def ::smart-port
    (s/or :string ::->int :num int?))

  (s/unform ::smart-port [:num 8080])
  (s/unform ::smart-port [:string "8080"])


  (s/def ::->int
    (s/and ::not-empty-string
           (s/conformer
            (fn [string] ;; conform func
              (Integer/parseInt string))
            identity))) ;; unform func

  (s/def ::smart-port
    (s/or :string ::->int :num int?))

  (s/unform ::smart-port [:num 8080])
  (s/unform ::smart-port [:string 8080])


  ;; error analysis

  (s/def ::username ::not-empty-string)
  (s/def ::email (s/and
                  ::not-empty-string
                  (partial re-matches #"(.+?)@(.+?)\.(.+?)")))
  (s/def ::sample
    (s/keys :req-un [::username
                     ::email]))

  (s/explain ::sample {:username 42})
  (s/explain-str ::sample {:username 42})
  (s/explain-data ::sample {:username 42})
  (s/explain ::sample {:username ""})


  ;; readable errors

  (def spec-errors {::not-empty-string "String should not be empty"
                    ::email            "Invalid email"
                    ::default          "Invalid data"})

  (defn problem-attrs-and-specs
    "Returns a seq of pairs, each relating attribute name with vector of
  all spec names representing causes of problems arisen when
  validating that attribute (in general to specific order)."
    [explain-data]
    (let [problems (::s/problems explain-data)]
      (map (juxt :in :via) problems)))

  (defn problem-attrs-and-last-specs
    "Returns a seq of pairs of attribute names and last (leaf, predicate)
  specs that were violated in this attributes."
    [explain-data]
      (map #(map peek %) (problem-attrs-and-specs explain-data)))

  (defn problem-messages [explain-data]
    (let [attrs-and-last-specs (problem-attrs-and-last-specs explain-data)]
      (map (fn [[attr spec]]
             (vector attr
                     (spec-errors spec (::default spec-errors))))
           attrs-and-last-specs)))

  (def an-explain-data (s/explain-data ::sample {:username "" :email ""}))
  (def another-explain-data (s/explain-data ::status 12))
  (def yet-another-explain-data (s/explain-data ::sample
                                                {:username "test"
                                                 :email    "test"}))

  (problem-attrs-and-specs an-explain-data)
  (problem-attrs-and-specs another-explain-data)
  (problem-attrs-and-specs yet-another-explain-data)

  (problem-attrs-and-last-specs an-explain-data)
  (problem-attrs-and-last-specs another-explain-data)
  (problem-attrs-and-last-specs yet-another-explain-data)

  (problem-messages an-explain-data)
  (problem-messages another-explain-data)
  (problem-messages yet-another-explain-data)

  ;; to prevent `s/explain` from omitting intermediate specs from `:via` chain,
  ;; redefine them as predicates or wrap them in `s/spec`.


  ;; parsing

  (def users
    [["1" "test@test.com" "active"]
     ["blocked" "2" "ivan@test.com" "pending"]
     ["3" "user@test.com" "active"]])

  (defmacro with-conformer
    [[bind] & body]
    `(s/conformer
      (fn [~bind]
        (try
          ~@body
          (catch Exception e#
            ::s/invalid)))))

  (s/def ::->int
    (s/and ::not-empty-string
           (with-conformer [val]
             (Integer/parseInt val))))

  (s/def ::->lower
    (s/and string?
           (s/conformer string/lower-case identity)))

  (s/def ::email (s/and
                  ::not-empty-string
                  (partial re-matches #"(.+?)@(.+?)\.(.+?)")))

  (s/def ::blocked
    (s/and ::->lower
           (s/conformer (partial = "blocked"))))

  (s/def :user/status
    (s/and ::->lower
           (with-conformer [val]
              (case val
                "active" :active
                "pending" :pending))))

  (s/def ::user
    (s/cat :blocked (s/? ::blocked)
           :id ::->int
           :email ::email
           :status :user/status))

  (s/def ::users
    (s/coll-of ::user))

  (s/conform ::user (first users))
  (s/conform ::user (second users))
  (s/conform ::users users)


  ;; coercion and transformation

  (def commands-json (slurp "resources/commands.json"))
  (def commands (m/decode "application/json" commands-json))

  (s/def :command/id uuid?)
  (s/def :command/name string?)
  (s/def :command/target string?)
  (s/def :command/instance (s/keys :opt-un [:command/id
                                            :command/name
                                            :command/target]))
  (s/def :sample/commands (s/coll-of :command/instance))
  (s/def :sample/instance (s/keys :opt-un [:sample/commands]))

  (st/coerce :sample/instance commands st/json-transformer)


  ;; `expound` for error messages

  (s/def ::username ::not-empty-string)
  (s/def ::email (s/and
                  ::not-empty-string
                  (partial re-matches #"(.+?)@(.+?)\.(.+?)")))
  (s/def ::sample
    (s/keys :req-un [::username
                     ::email]))

  (expound/expound ::sample {:username 42})
  (expound/expound ::sample {:username ""})
  (expound/expound ::email "foo")
  (expound/expound-str ::email "foo")


)
