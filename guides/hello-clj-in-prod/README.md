# hello-clj-in-prod

Exploring examples from Clojure In Production book.

- https://grishaev.me/clojure-in-prod/

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
