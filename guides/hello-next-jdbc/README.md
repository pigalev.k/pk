# hello-next-jdbc

Exploring next-jdbc --- a modern low-level Clojure wrapper for JDBC-based access
to databases.

- https://github.com/seancorfield/next-jdbc
- https://www.sqltutorial.org/wp-content/uploads/2016/04/SQL-cheat-sheet.pdf

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
