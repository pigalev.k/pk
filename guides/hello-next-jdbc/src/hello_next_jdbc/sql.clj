(ns hello-next-jdbc.sql
  (:require
   [clojure.java.io :as io]
   [honey.sql :as sql]
   [next.jdbc :as jdbc]))


;; SQL cheatsheet
;; https://www.sqltutorial.org/wp-content/uploads/2016/04/SQL-cheat-sheet.pdf


;;;; prepare the database

;; podman run --rm --name some-postgres -e POSTGRES_PASSWORD=secret -p 5432:5432 postgres

(def db-spec {:dbtype "postgres" :dbname "postgres" :user "postgres" :password "secret"})
(def db (jdbc/get-datasource db-spec))
(def db-schema (slurp (io/resource "schema.postgres.sql")))
(def db-data (slurp (io/resource "data.sql")))
(jdbc/execute! db [db-schema])
(jdbc/execute! db [db-data])

(defn q
  "Executes `query` on `db`."
  [query]
  (jdbc/execute! db query))


;;;; querying data from a table


(comment

  ;; 1. query all rows and columns from the table

  (q (sql/format {:select :*
                  :from   :regions}))

  ;; 2. query data in the given columns from the table

  (q (sql/format {:select [:country_id :country_name]
                  :from   :countries}))


  ;; 3. query data and filter rows with a condition

  (q (sql/format {:select [:country_id :country_name]
                  :from   :countries
                  :where  [:like :country_name "%ae%"]}))

  ;; 4. query distinct rows from a table

  (q (sql/format {:select-distinct [:country_name]
                  :from            :countries}))

  ;; 5. sort the result set in ascending or descending order

  (q (sql/format {:select   [:region_id]
                  :from     :regions
                  :order-by [[:region_id :asc]]}))

  (q (sql/format {:select   [:region_id]
                  :from     :regions
                  :order-by [[:region_id :desc]]}))

  ;; 6. skip rows and limit the result size

  (q (sql/format {:select [:country_id :country_name]
                  :from   :countries
                  :offset 2
                  :limit  3}))

  ;; the same, but present in SQL standard (SQL:2008)

  (q (sql/format {:select [:country_id :country_name]
                  :from   :countries
                  :offset 2
                  :fetch 3}))

  ;; 7. group rows using an aggregate function

  (q (sql/format {:select [:region_id :%count.country_id]
                  :from :countries
                  :group-by [:region_id]}))

  ;; 8. filter groups

  (q (sql/format {:select [:country_id :%count.*]
                  :from :countries
                  :group-by [:country_id]
                  :having [:= "IL" :country_id]}))

)


;; querying from multiple tables

(comment

  ;; 1. (inner) join (combines and returns only records that have values
  ;; matched by condition)

  (q (sql/format {:select [:c.country_id :c.region_id]
                  :from [[:countries :c]]
                  :join [[:regions :r] [:= :c.region_id :r.region_id]]}))

  ;; 2. left (outer) join (returns all records from the left table, whether
  ;; they have a corresponding record from the right table to combine with or
  ;; not)

  (q (sql/format {:select [:c.country_id :c.region_id]
                  :from [[:countries :c]]
                  :left-join [[:regions :r] [:= :c.region_id :r.region_id]]}))

  ;; 3. right (outer) join (returns all records from the right table, whether
  ;; they have a corresponding record from the left table to combine with or
  ;; not)

  (q (sql/format {:select [:c.country_id :c.region_id]
                  :from [[:countries :c]]
                  :right-join [[:regions :r] [:= :c.region_id :r.region_id]]}))

  ;; 4. full (outer) join (returns all records from both tables, whether they
  ;; have a corresponding record from the other table to combine with or not)

  (q (sql/format {:select [:c.country_id :c.region_id]
                  :from [[:countries :c]]
                  :full-join [[:regions :r] [:= :c.region_id :r.region_id]]}))

  ;; 5. cross join (Cartesian product) (returns all records from both tables
  ;; combined pairwise, without conditions)

  (q (sql/format {:select [:l.location_id :r.region_id]
                  :from [[:locations :l]]
                  :cross-join [[:regions :r]]}))

  ;; the same

  (q (sql/format {:select [:l.location_id :r.region_id]
                  :from [[:locations :l] [:regions :r]]}))

  ;; 6. self-join (returns all records in the table that have values matched
  ;; by condition)

  (q (sql/format {:select :*
                  :from [[:countries :c]]
                  :join [[:countries :c1] [:and
                                           [:<> :c.country_id :c1.country_id]
                                           [:= :c.region_id :c1.region_id]]]}))

)
