(ns hello-next-jdbc.sqlite
  (:require
   [clojure.java.io :as io]
   [next.jdbc :as jdbc]))



(comment

  ;; create a db connection

  (def db {:dbtype "sqlite" :dbname "hr.db"})
  (def ds (jdbc/get-datasource db))


  ;; define the db schema and load data

  ;; whether you can execute multiple statements in a single JDBC operation is
  ;; database-dependent. Some databases allow multiple statements in a single
  ;; operation, separated by a semicolon. If the JDBC driver supports it,
  ;; next.jdbc will also support it.
  ;; https://stackoverflow.com/questions/60468070/next-jdbc-execute-multiple-statements

  ;; load and apply the db schema

  (def db-schema (slurp (io/resource "schema.sqlite.sql")))
  (jdbc/execute! ds [db-schema])

  ;; load and apply the db data

  (def db-data (slurp (io/resource "data.sql")))
  (jdbc/execute! ds [db-data])

  ;; query the data

  (jdbc/execute! ds ["select * from regions"])
  (jdbc/execute! ds ["select * from countries"])

)
