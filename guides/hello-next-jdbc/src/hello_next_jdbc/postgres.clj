(ns hello-next-jdbc.postgres
  (:require
   [clojure.java.io :as io]
   [next.jdbc :as jdbc]
   [clojure.datafy :as d]
   [dotenv :refer [env]]))


(comment

  ;; start a postgres instance

  ;; podman run --rm --name some-postgres -e POSTGRES_PASSWORD=secret -p 5432:5432 postgres


  ;; create a db connection

  (def db-spec {:dbtype "postgres" :dbname "postgres" :user "postgres" :password "secret"})
  (def db (jdbc/get-datasource db-spec))


  ;; define the db schema and load data

  ;; whether you can execute multiple statements in a single JDBC operation is
  ;; database-dependent. Some databases allow multiple statements in a single
  ;; operation, separated by a semicolon. If the JDBC driver supports it,
  ;; next.jdbc will also support it.
  ;; https://stackoverflow.com/questions/60468070/next-jdbc-execute-multiple-statements


  ;; load and apply the db schema

  (def db-schema (slurp (io/resource "schema.postgres.sql")))
  (jdbc/execute! db [db-schema])


  ;; load and apply the db data

  (def db-data (slurp (io/resource "data.sql")))
  (jdbc/execute! db [db-data])


  ;; query the data

  (jdbc/execute! db ["select * from regions"])
  (jdbc/execute! db ["select * from countries"])


  ;; datafy & nav

  (def dm (d/datafy (.getMetaData (jdbc/get-connection db))))


  ;; environment variables from .env file via dotenv

  (env "APP_DB_TYPE")

)
