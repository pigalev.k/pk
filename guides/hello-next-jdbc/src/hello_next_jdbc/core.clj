(ns hello-next-jdbc.core
  (:require
   [next.jdbc :as jdbc]))



(comment

  ;; create a db connection

  (def db {:dbtype "h2" :dbname "example"})
  (def ds (jdbc/get-datasource db))


  ;; define schema

  (jdbc/execute! ds ["
create table address (
  id int auto_increment primary key,
  name varchar(32),
  email varchar(255)
)"])

  (jdbc/execute! ds ["
create table foo (
  id int auto_increment primary key,
  bar varchar(32),
  frob varchar(255)
)"])

  (jdbc/execute! ds ["
create table quux (
  id int auto_increment primary key,
  foo_id int,
  frob varchar(255),
  foreign key (foo_id) references foo
)"])


  ;; insert data

  (jdbc/execute! ds ["insert into address (name,email) values('Sean Corfield','sean@corfield.org')"])
  (jdbc/execute! ds ["insert into address (name,email) values('John Doe','doe@john.org')"])

  (jdbc/execute! ds ["insert into foo (bar,frob) values('ololo','trololo')"])
  (jdbc/execute! ds ["insert into foo (bar,frob) values('hayo','world')"])
  (jdbc/execute! ds ["insert into foo (bar,frob) values('get','high')"])

  (jdbc/execute! ds ["insert into quux (foo_id,frob) values(3,'higher')"])
  (jdbc/execute! ds ["insert into quux (foo_id,frob) values(3,'highest')"])


  ;; select data

  (jdbc/execute! ds ["select * from address"])
  (jdbc/execute! ds ["select * from foo"])
  (jdbc/execute! ds ["select * from quux"])

  (jdbc/execute! ds ["select distinct name from address"])
  (jdbc/execute! ds ["select all name from address"])

  (jdbc/execute! ds ["select id + 1 as new_id, upper(name) from address where id = ?" 2])

  (jdbc/execute! ds ["select quux.id, bar, frob from quux, foo where quux.foo_id = foo.id"])

  (jdbc/execute! ds ["select count(*) as c from address"])
  (jdbc/execute! ds ["select avg(id) as a from address"])

  (jdbc/execute! ds ["select avg(num) as e from quux"])

  ;; alter the schema

  (jdbc/execute! ds ["alter table quux drop frob"])
  (jdbc/execute! ds ["alter table quux add etc varchar(12)"])

)
