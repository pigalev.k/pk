# hello-transit-cljs

Exploring the [transit-cljs](https://github.com/cognitect/transit-cljs),
ClojureScript implementation of the
[transit](https://github.com/cognitect/transit-format) data interchange format.

## Getting started

Start a cljs REPL in terminal

```
clj -M -m cjls.main
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
