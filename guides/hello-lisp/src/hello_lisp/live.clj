(ns hello-lisp.live
  (:require
   [nextjournal.clerk :as clerk]))


;; basics

(comment

  ;; start Clerk's built-in webserver on the default port 7777, opening the
  ;; browser when done
  (clerk/serve! {:browse? true})

  ;; either call `clerk/show!` explicitly
  (clerk/show! "src/hello_lisp/functions.clj")
  (clerk/show! "src/hello_lisp/little_schemer/commandments.clj")
  (clerk/show! "src/hello_lisp/little_schemer/intermission.clj")

  ;; or let Clerk watch the given `:paths` for changes
  (clerk/serve! {:watch-paths ["src"]})

  ;; stop Clerk's webserver and file watcher
  (clerk/halt!)

)
