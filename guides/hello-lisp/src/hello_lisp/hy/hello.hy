#!/usr/bin/env hy

(print "Make me executable, and run me!")


;; https://docs.hylang.org/en/stable/tutorial.html


;; setting variables

(setv a 1)
a


;; calling Python functions

(print "Hy, world!")
(len [1 2 3])


;; defining functions

(defn test [a b [c None] [d "x"] #* e]
  "Returns a list of values."
  [a b c d e])

(print (test 1 2))
(print (test 1 2 3 4 5 6 7))


;; defining macros

(defmacro comment
  [#* body] None)

(defmacro do-while [condition #* body]
  `(do
    ~@body
    (while ~condition
      ~@body)))

(comment

  (setv x 0)
  (do-while x
            (print "This line is executed once."))

)


;; printing values

(comment

  (print '(1 2 3))

  (hy.repr '(1 2 3))

)


;; define classes

(comment

  (defclass FooBar []
    (defn __init__ [self x]
      (setv self.x x))
    (defn get-x [self]
      self.x))

  (setv fb (FooBar 15))
  (print fb.x)
  (print (. fb x))
  (print (.get-x fb))
  (print (fb.get-x))

)


;; import modules

(comment

  (import math)
  (print (math.sqrt 2))

)
