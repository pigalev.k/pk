(ns hello-lisp.little-schemer.chapter-10
  (:require
   [hello-lisp.functions :refer [atom? *build *first *second *third
                                 lookup-in-entry lookup-in-entry-help
                                 lookup-in-table
                                 *const *quote *identifier *lambda
                                 *cond *application
                                 atom-to-action list-to-action new-entry
                                 expression-to-action evlis evcon
                                 table-of formals-of body-of
                                 function-of arguments-of
                                 question-of answer-of cond-lines-of
                                 meaning value *apply
                                 apply-closure apply-primitive]]))


;; Source: The Little Schemer, 4th edition

;; see also https://github.com/quux00/little-schemer


;; # 10. What Is the Value of All of This?

(comment

  ;; 1. An entry is a pair of lists whose first list is a set. Also, the two
  ;; lists must be of equal length. Make up some examples for entries.

  ;; Here are our examples:

  '((appetizer entrée beverage)
    (paté boeuf vin))

  '((appetizer entrée beverage)
    (beer beer beer))

  '((beverage dessert)
    ((food is) (number one with us)))


  ;; 2. How can we build an entry from a set of names and a list of values?

  (def new-entry-1 *build)

  ;; Try go build our examples with this function.


  ;; 3. What is

  (let [name  'entrée
        entry '((appetizer entrée beverage)
                (food tastes good))]
    (lookup-in-entry name entry))

  ;; 'tastes.


  ;; 4. What if `name` is 'dessert?

  ;; In this case we would like to leave the decision about what to do with the
  ;; user of `lookup-in-entry`.


  ;; 5. How can we accomplish this?

  ;; `lookup-in-entry` takes an additional argument that is invoked when `name`
  ;; is not found in the first list of an entry.


  ;; 6. How many arguments do you think this extra function should take?

  ;; We think it should take one, `name`. Why?


  ;; 7. Here is our definition of `lookup-in-entry`.

  (defn lookup-in-entry-1 [name entry entry-f]
    (lookup-in-entry-help name
                          (*first entry)
                          (*second entry)
                          entry-f))

  ;; Finish the function `lookup-in-entry-help`.

  (defn lookup-in-entry-help-1 [name names values entry-f]
    (cond
      (empty? names)         (entry-f name)
      (= (first names) name) (first values)
      :else                  (lookup-in-entry-help-1 name
                                                     (rest names)
                                                     (rest values)
                                                     entry-f)))


  ;; 8. A table (also called an environment) is a list of entries. Here is one
  ;; example: the empty table, represented by '(). Make up some others.

  ;; Here is another one:

  '(((appetizer entrée beverage)
     (paté boeuf vin))
    ((beverage dessert)
     ((food is) (number one with us))))


  ;; 9. Define the function `extend-table` which takes an entry and a
  ;; table (possibly the empty one) and creates a new table by putting the new
  ;; entry in front of the old table.

  (def extend-table cons)


  ;; 10. What is

  (let [name    'entrée
        table   '(((entrée dessert)
                   (spaghetti spumoni))
                  ((appetizer entrée beverage)
                   (food tastes good)))
        table-f (constantly :not-found)]
    (lookup-in-table name table table-f))

  ;; It could be either 'spaghetti or 'tastes, but `lookup-in-table` searches
  ;; the list of entries in order. So it is 'spaghetti.


  ;; 11. Write `lookup-in-table`. Hint: Don't forget to get some help.

  (defn lookup-in-table-1 [name table table-f]
    (cond
      (empty? table) (table-f name)
      :else          (lookup-in-entry name
                                      (first table)
                                      (fn [name]
                                        (lookup-in-table-1 name
                                                           (rest table)
                                                           table-f)))))


  ;; 12. Can you describe what the following function represents:

  (fn [name]
    (lookup-in-table name
                     (rest table)
                     table-f))

  ;; This function is the action to take when the name is not found in the first
  ;; entry.


  ;; 13. In the preface we mentioned that sans serif typeface would be used to
  ;; represent atoms. To this point it has not mattered. Henceforth, you must
  ;; notice whether or not an atom is in san serif. (We cannot make this
  ;; distinction in this walkthrough. It does not matter for its touch type
  ;; exercise aspect, though.)

  ;; Remember to be very conscious as to whether or not an atom is in sans serif.


  ;; 14. Did you notice that "sans serif" was not in sans serif?

  ;; We hope so. This is "sans serif" in sans serif. (As if.)


  ;; 15. Have we chosen a good representation for expressions?

  ;; Yes. They are all S-expressions so they can be data for functions.


  ;; 16. What kind of functions?

  ;; For example, `value`.


  ;; 17. Do you remember `value` from chapter 6?

  ;; Recall that `value` is the function that returns the natural value of
  ;; expressions.


  ;; 18. What is the value of

  (first '(a b c))

  ;; We don't event know what '(a b c) is.


  ;; 19. What is the value of

  (let [rep-a 'a
        rep-b 'b
        rep-c 'c]
    (cons rep-a
          (cons rep-b
                (cons rep-c
                      '()))))

  ;; It is the same as '(a b c).


  ;; 20. Great. And what is the value of

  (let [rep-first 'first
        rep-quote 'quote
        rep-a     'a
        rep-b     'b
        rep-c     'c]
    (cons rep-first
          (cons (cons rep-quote
                      (cons
                       (cons rep-a
                             (cons rep-b
                                   (cons rep-c
                                         (quote ()))))
                       (quote ())))
                (quote ()))))

  ;; It is a representation of the expression:

  (first (quote (a b c)))


  ;; 21. What is the value of

  (first (quote (a b c)))

  ;; 'a.


  ;; 22, 23. What is

  (let [e (first (quote (a b c)))]
    (value e))

  ;; 'a.


  ;; 24. What is

  (let [e (quote (first (quote (a b c))))]
    (value e))

  ;; '(first (quote (a b c))).


  ;; 25. What is

  (let [e '(inc 6)]
    (value e))

  ;; 7.


  ;; 26. What is

  (let [e 6]
    (value e))

  ;; 6, because numbers are constants.


  ;; 27. What is

  (let [e '(quote nothing)]
    (value e))

  ;; 'nothing.


  ;; 28. What is

  (let [e 'nothing]
    (value e))

  ;; 'nothing has no value.


  ;; 29. What is

  (let [e ((fn [nothing]
             (cons nothing (quote ())))
           '(quote
             (from nothing comes something)))]
    (value e))

  ;; '((from nothing comes something)).


  ;; 30. What is

  (let [e ((fn [nothing]
             (cond
               nothing (quote something)
               :else   (quote nothing)))
           true)]
    (value e))

  ;; 'something.


  ;; 31. What is the type of `e`, where `e` is 6?

  ;; `*const`.


  ;; 32. What is the type of `e` where `e` is false?

  ;; `*const`.


  ;; 33. What is

  (let [e false]
    (value e))

  ;; false.


  ;; 34. What is the type of `e` where `e` is `cons`?

  ;; `*const`.


  ;; 35. What is

  (let [e first]
    (value e))

  ;; '(primitive first).


  ;; 36. What is the type of `e` where `e` is (quote nothing)?

  ;; `*quote`.


  ;; 37. What is the type of `e` where `e` is 'nothing?

  ;; `*identifier`.


  ;; 38. What is the type of `e` where `e` is

  (fn [x y] (cons x y))

  ;; `*lambda`.


  ;; 39. What is the type of `e` where `e` is

  ((fn [nothing]
     (cond
       nothing 'something
       :else   'nothing))
   true)

  ;; `*application`.


  ;; 40. What is the type of `e` where `e` is

  (cond
    nothing 'something
    :else   'nothing)

  ;; `*cond`.


  ;; 41. How many types do you think there are?

  ;; We found six: `*const`, `*quote`, `*identifier`, `*lambda`, `*cond`, and
  ;; `*application`.


  ;; 42. How do you think we should represent types?

  ;; We choose functions. We call these functions "actions."


  ;; 43. If actions are functions that do "the right thing" when applied to the
  ;; appropriate type of expression, what should `value` do?

  ;; You guessed it. It would have to fing out the type of expression it was
  ;; passed and then use the associated action.


  ;; 44. Do you remember `atom-to-function` from chapter 8?

  ;; We found `atom-to-function` useful when we rewrote `value` for numbered
  ;; expressions.


  ;; 45. Belos is a function that produces the correct action (or function) for
  ;; each possible S-expression:

  (defn expression-to-action-1 [e]
    (cond
      (atom? e) (atom-to-action e)
      :else     (list-to-action e)))

  ;; Define the function `atom-to-action`. (Ill-formed S-expressions such
  ;; as (quote a b), (fn [true] true), (fn [5] 5), (fn a), (1 2) are not
  ;; considered here. They can be detected by an appropriate function to which
  ;; S-expressions are submitted before they are passed on to
  ;; `value`.) (Probably using `clojure.spec`.)


  ;; 46.

  (defn atom-to-action-1 [e]
    (cond
      (number? e)   *const
      (true? e)     *const
      (false? e)    *const
      (= e cons)    *const
      (= e first)   *const
      (= e empty?)  *const
      (= e =)       *const
      (= e atom?)   *const
      (= e zero?)   *const
      (= e inc)     *const
      (= e dec)     *const
      (= e number?) *const
      :else         *identifier))


  ;; 47. Now define the help function `list-to-action`.

  (defn list-to-action-1 [e]
    (cond
      (atom? (first e)) (cond
                          (= (first e) 'quote)  *quote
                          (= (first e) 'lambda) *lambda
                          (= (first e) 'cond)   *cond
                          :else                 *application)
      :else             *application))


  ;; 48. Assuming that `expression-to-acion` works, we can use it to define
  ;; `value` and `meaning`.

  (defn meaning-1 [e table]
    ((expression-to-action e) e table))

  (defn value-7 [e]
    (meaning e '()))

  ;; What is '() in the definition of `value`?

  ;; It is the empty table. The function `value`, together with all the
  ;; functions it uses, is called an interpreter. (The function `value`
  ;; approximates the function `eval` available in Scheme and other Lisps.)


  ;; Actions do speak louder than words.


  ;; 49. How many arguments should actions take according to the above?

  ;; Two, the expression `e` and a table.


  ;; 50. Here is the action for constants.

  (defn *const-1 [e table]
    (cond
      (number? e) e
      (true? e)   true
      (false? e)  false
      :else       (*build 'primitive e)))

  ;; Is it correct?

  ;; Yes, for numbers, it just returns the expression, and this is all we have
  ;; to do for 0, 1, 2, .... For true, it returns true. For false, it returns
  ;; false. And all other atoms of constant type represent primitives.


  ;; 50. Here is the action for `*quote`.

  (defn *quote-1 [e table]
    (text-of e))

  ;; Define the help function `text-of`.

  (def text-of-1 *second)


  ;; 51. Have we used the table yet?

  ;; No, but we will in a moment.


  ;; 52. Why do we need the table?

  ;; To remember the values of identifiers.


  ;; 53. Given that the table contains the values of identifiers, write the
  ;; action `*identifier`.

  (defn *identifier-1 [e table]
    (lookup-in-table e table initial-table))


  ;; 54. Here is `initial-table`.

  (defn initial-table-1 [name]
    (first '()))

  ;; When is it used?

  ;; Let's hope never. Why?


  ;; 55. What is the value of

  (fn [x] x)

  ;; We don't know yet, but we know that it must be the representation of a
  ;; non-primitive function.


  ;; 56. How are non-primitive functions different from primitives?

  ;; We know what primitives do; non-primitives are defined by their arguments
  ;; and their function bodies.


  ;; 57. So when we want to use a non-primitive we need to remember its formal
  ;; arguments and its function body.

  ;; At least. Fortunately this is just the `rest` of a `fn` expresssion.


  ;; 58. And what else do we need to remember?

  ;; We will also put the table in, just in case we might need it later.


  ;; 59. And how do we represent this?

  ;; In a list, of course.


  ;; 60. Here is the action `*lambda`.

  (defn *lambda-1 [e table]
    (*build 'non-primitive
            (cons table (rest e))))

  ;; What is

  (let [e     '(fn [c] (cons x y))
        table '(((y z) ((8) 9)))]
    (meaning e table))

  '(non-primitive '( (((y z) ((8) 9)))      (x)      (cons x y) ))
  ;;               ^----table--------^  ^-formals-^  ^--body--^


  ;; 61. It is probably a good idea to define some help functions for getting
  ;; back the parts in this three element list (i.e., the table, the formal
  ;; arguments, and the body). Write `table-of`, `formals-of` and `body-of`.

  (def table-of-1 *first)
  (def formals-of-1 *second)
  (def body-of-1 *third)


  ;; 62. Describe `cond` in your own words.

  ;; It is a special form that takes any number of `cond`-lines. It considers
  ;; each line in turn. If the question part on the left is false, it looks at
  ;; the rest of the lines. Otherwise it proceeds to answer the right part. If
  ;; it sees an `:else`-line, it treats that `cond`-line as if its question part
  ;; were true.


  ;; 63. Here us tge function `evcon` that does what we just said in words:

  (defn evcon-1 [lines table]
    (cond
      (else? (question-of (first lines)))         (meaning
                                                   (answer-of (first lines))
                                                   table)
      (meaning (question-of (first lines)) table) (meaning
                                                   (answer-of (first lines))
                                                   table)
      :else                                       (evcon-1 (rest lines) table)))

  ;; Write `else?` and the help functions `question-of` and `answer-of`.

  (defn else?-1 [x]
    (cond
      (atom? x) (= x :else)
      :else     false))

  (def question-of-1 *first)
  (def answer-of-1 *second)


  ;; 64. Didn't we violate The First Commandment?

  ;; Yes, we don't ask (`empty?` `lines`), so one of the questions in every
  ;; `cond` better be true.


  ;; 65. Now use the function `evcon` to write the `*cond` action.

  (def cond-lines-of-1 rest)

  (defn *cond-1 [e table]
    (evcon (cond-lines-of e) table))


  ;; 66. Aren't these help functions useful?

  ;; Yes, they make things quite a bit more readable. But you already knew that.


  ;; 67. Do you understand `*cond` now?

  ;; Perhaps not.


  ;; 68. How can you become familiar with it?

  ;; The best way is to try an example. A good one is:

  (let [e     '(cond 'coffee 'clatsch :else 'party)
        table '(((coffee) (true))
                ((klatsch party) (5 6)))]
    (*cond e table))

  ;; 69. Have we seen how the table gets used?

  ;; Yes, `*lambda` and `*identifier` use it.


  ;; 70. But how do the identifiers get into the table?

  ;; In the only action we have not defined: `*application`.


  ;; 71. How is an application represented?

  ;; An application is a list of expressions whose `first` position contains an
  ;; expression whose value is a function.


  ;; 72. How does an application differ from a special form, like `and`, `or`,
  ;; or `cond`?

  ;; An application must always determine the meaning of all its arguments.


  ;; 73. Before we can apply a function, do we have to get the meaning of all of
  ;; its arguments?

  ;; Yes.


  ;; 74. Write a function `evlis` that takes a list of (representations of)
  ;; arguments and a table, and returns a list composed of the meaning of each
  ;; argument.

  (defn evlis-1 [args table]
    (cond
      (empty? args) '()
      :else         (cons (meaning (first args) table)
                          (evlis-1 (rest args) table))))


  ;; 75. What else do we need before we can determine the meaning of an
  ;; application?

  ;; We need to find out what its `function-of` means.


  ;; 76. And what then?

  ;; Then we apply the meaning of the function to the meaning of the arguments.


  ;; 77. Here is `*application`:

  (defn *application-1 [e table]
    (apply
     (meaning (function-of e) table)
     (evlis (arguments-of e) table)))

  ;; Is it correct?


  ;; 78. Of course. We just have to define `apply`, `function-of`, and
  ;; `arguments-of` correctly.


  ;; 79. Write `function-of` and `arguments-of`.

  (def function-of-1 first)
  (def arguments-of-1 rest)


  ;; 80. How many different kinds of functions are there?

  ;; Two: primitives and non-primitives.


  ;; 81. What are the two representations of functions?

  '(primitive primitive-name)

  ;; and

  '(non-primitive (table formals body))

  ;; The list '(table formals body) is called a closure record.


  ;; 82. Write `primitive?` and `non-primitive?`.

  (defn primitive?-1 [l]
    (= (*first l) 'primitive))

  (defn non-primitive?-1 [l]
    (= (*first l) 'non-primitive))


  ;; 83. Now we can write the function `apply`.

  ;; Here it is:

  (defn apply-1 [fun vals]
    (cond
      (primitive?-1 fun)     (apply-primitive (*second fun) vals)
      (non-primitive?-1 fun) (apply-closure (*second fun) vals)))

  ;; (If `fun` does not evaluate to either a primitive or non-primitive as in
  ;; the expression ((fn [x] (x 5)) 3), there is no answer. The function `apply`
  ;; approximates the function `apply` available in Scheme and other Lisps.)


  ;; 84. This is the definition of `apply-primitive`:

  (defn *atom?-1 [x]
    (cond
      (atom? x)                    true
      (empty? x)                   false
      (= (first x) 'primitive)     true
      (= (first x) 'non-primitive) true
      :else                        false))

  (defn apply-primitive-1 [name vals]
    (cond
      (= name 'cons)    (cons (*first vals) (*second vals))
      (= name 'first)   (first (*first vals))
      (= name 'rest)    (rest (*first vals))
      (= name 'empty?)  (empty? (*first vals))
      (= name '=)       (= (*first vals) (*second vals))
      (= name 'atom?)   (*atom?-1 (*first vals))
      (= name 'zero?)   (zero? (*first vals))
      (= name 'inc)     (inc (*first vals))
      (= name 'dec)     (dec (*first vals))
      (= name 'number?) (number? (*first vals))))

  ;; (The function `apply-primitive` could check for applications of `rest` to
  ;; the empty list or `dec` to 0, etc.)


  ;; 85. Is `apply-closure` the only function left?

  ;; Yes, and `apply-closure` must extend the table.


  ;; 86. How could we find the result of

  (let [f (fn [x y] (cons x y))
        a 1
        b '(2)]
    (f a b))

  ;; That's tricky. But we know what to do to find the meaning of

  (cons x y)

  ;; where `table` is

  '(((x y) (1 (2))))


  ;; 87. Why can we do this?

  ;; Here, we don't need `apply-closure`.


  ;; 88. Can you generalize the last two steps?

  ;; Applying a non-primitive function --- a closure --- to a list of values is
  ;; the same as finding the meaning of the closure's body with its table
  ;; extended by an entry of the form '(`formals` `values`). In this entry,
  ;; `formals` is the `formals` of the closure and `values` is the result of
  ;; `evlis`.


  ;; 89. Have you followed all this?

  ;; If not, here is the definition of `apply-closure`.

  (defn apply-closure-1 [closure vals]
    (meaning (body-of closure)
             (extend-table (new-entry (formals-of closure) vals)
                           (table-of closure))))


  ;; 90. This is a complicated function and it deserves an example.

  ;; In the following, `closure` is

  '((((u v w)
      (1 2 3))
     ((x y z)
      (4 5 6)))
    (x y)
    (cons z x))

  ;; and `vals` is

  '((a b c) (d e f))


  ;; 91. What will be the new arguments of `meaning`?

  ;; The new `e` for `meaning` will be '(`cons` `z` `x`) and the new `table` for
  ;; `meaning` will be

  '(((x y)
     ((a b c) (d e f)))
    ((u v w)
     (1 2 3))
    ((x y z)
     (4 5 6)))


  ;; 92. What is the meaning of

  (let [x '(a b c)
        z 6]
    (cons z x))

  ;; The same as

  (let [e     '(cons z x)
        table '(((x y)
                 ((a b c) (d e f)))
                ((u v w)
                 (1 2 3))
                ((x y z)
                 (4 5 6)))]
    (meaning e table))


  ;; 93. Let's find the meaning of all the arguments. What is

  (let [args  '(z x)
        table '(((x y)
                 ((a b c) (d e f)))
                ((u v w)
                 (1 2 3))
                ((x y z)
                 (4 5 6)))]
    (evlis args table))

  ;; In order to do this, we must find both

  (let [e 'z]
    (meaning e table))

  ;; and

  (let [e 'x]
    (meaning e table))


  ;; 94. What is

  (let [e 'z]
    (meaning e table))

  ;; 6, by using `*identifier`.


  ;; 95. What is

  (let [e 'x]
    (meaning e table))

  ;; '(a b c), by using `*identifier`.


  ;; 96. So, what is the result of `evlis`?

  ;; '(6 (a b c)), because `evlis` returns a list of the meanings.


  ;; 97. What is

  (let [e cons]
    (meaning e table))

  ;; '(primitive cons), by using `*const`.


  ;; 98. We are now ready to

  (let [fun  '(primitive cons)
        vals '(6 (a b c))]
    (*apply fun vals))

  ;; Which path should we take?

  ;; The `apply-primitive` path.


  ;; 99. Which `cond`-line is chosen for

  (let [name 'cons
        vals '(6 (a b c))]
    (apply-primitive name vals))

  ;; The third:

  (= (name 'cons)) (cons (*first vals) (*second vals))


  ;; 100. Are we finished now?

  ;; Yes, we are exhausted.


  ;; 101. But what about `defn`?

  ;; It isn't needed because recursion can be obtained from the Y combinator.


  ;; 102. Is `defn` really not needed?

  ;; Yes, but see The Seasoned Schemer.


  ;; 103. Does that mean we can run the interpreter on the interpreter if we so
  ;; the transformation with the Y combinator?

  ;; Yes, but don't bother.


  ;; 104. What makes `value` unusual?

  ;; It sees representations of expressions.


  ;; 105. Should `will-stop?` see representations of expressions?

  ;; That may help a lot.


  ;; 106. Does it help?

  ;; No, don't bother --- we can play the same game again. We would be able to
  ;; define a function like `last-try?` that will show that we cannot `defn` the
  ;; new and improved `will-stop?`.


  ;; 107. `:else`?

  ;; Yes, it's time for a banquet.

)
