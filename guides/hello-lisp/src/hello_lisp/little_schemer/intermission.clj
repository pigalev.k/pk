(ns hello-lisp.little-schemer.intermission)


;; Source: The Little Schemer, 4th edition


;; # Intermission


;; You've reached the intermission. What are your options? You could quickly
;; run out and get the rest of the show, The Seasoned Schemer, or you could
;; read some of the books that we mention below. All of these books are
;; classics and some of them are quite old; nevertheless they have stood the
;; test of time and are all worthy of your notice. Some have nothing
;; whatsoever to do with mathematics or logic, some have to do with
;; mathematics, but only by way of telling an interesting story, and still
;; others are just worth discovering. There should be no confusion: these
;; books are not here to prepare you to read the sequel, they are just for
;; your entertainment. At the end of The Seasoned Schemer you can find a set
;; of references to Scheme and the reference to Common Lisp. Do not feel
;; obliged to jump ahead to the next book. Take some time off and read some of
;; these books instead. Then, when you have relaxed a bit, perhaps removed
;; some of the calories that were foisted upon you, go ahead and dive into the
;; sequel. Enjoy!

;; - Abbott, Edwin A. "Flatland." Dover Publications, Inc., New York,
;;   1952. (Original publication: Seeley and Co., Ltd., London, 1884.)

;; - Carroll, Lewis. "The Annotated Alice: Alice's Adventures in Wonderland
;;   and Through the Looking Glass." Clarkson M. Potter, Inc., New York,
;;   1960. Introduction and notes by Martin Gardner. Original publications
;;   under different titles: "Alice's Adventures Under Ground" and "Through
;;   the Looking Glass and What Alice Found There", Macmillan and Company,
;;   London 1865 and 1872, respectively.

;; - Halmos, Paul R. "Naive Set Theory." Litton Educational Publishers, New
;;   York, 1960.

;; - Hein, Piet. "Grooks." The MIT Press, Cambridge, Massachusetts, 1960.

;; - Hofstadter, Douglas R. "Gödel, Escher, Bach: an Eternal Golden Braid."
;;   Basic Books, Inc., New York, 1979.

;; - Nagel, Ernest and James R. Newman. "Gödel's proof." New York University
;;   Press, New York, 1958.

;; - Pólya, György. "How to Solve It." Doubleday and Co., New York, 1957.

;; - Smullyan, Raymond. "To Mock a Mockingbird And Other Logic Puzzles
;;   Including an Amazing Adventure in Combinatory Logic." Alfred A. Knopf,
;;   Inc., New York, 1985.

;; - Suppes, Patrick. "Introduction to Logic." Van Nostrand Co., Princeton,
;;   New Jersey, 1957.
