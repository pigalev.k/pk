(ns hello-lisp.little-schemer.chapter-8
  (:require
   [hello-lisp.functions :refer [atom? pow length
                                 rember  equal?
                                 operator first-sub-exp second-sub-exp
                                 rember-f =-c *rember-= *rember-f seql seqr seqs
                                 insert-g multirember-f multirember-t
                                 multirember&co a-friend multiinsertlr&co
                                 evens-only* evens-only*&co]]))


;; Source: The Little Schemer, 4th edition

;; see also https://github.com/quux00/little-schemer


;; # 8. Lambda the Ultimate

(comment

  ;; 1. Remember what we did in `rember` and `insertl` at the end of chapter 5?

  ;; We used `=` to compare any values?


  ;; 2. Can you write a function `rember-f` that would use any supplied function
  ;; instead of `=`?

  ;; No, because we have not yet told you how.


  ;; 3. How can you make `rember` remove the first 'a from '(b c a)?

  ;; By passing 'a and '(b c a) as arguments to `rember`.


  ;; 4. How can you make `rember` remove the first 'c from '(b c a)?

  ;; By passing 'c and '(b c a) as arguments to `rember`.


  ;; 5. How can you make `rember-f` use `==`? instead of `=`?

  ;; By passing `==` as an argument to `rember-f`.


  ;; 6. What is

  (let [test? =
        a     5
        l     '(6 2 5 3)]
    (rember-f test? a l))

  ;; '(6 2 3).


  ;; 7. What is

  (let [test? =
        a     'jelly
        l     '(jelly beans are good)]
    (rember-f test? a l))

  ;; '(beans are good). (There are no core function in Clojure that compares
  ;; only non-numeric atoms, so we just use `=` everywhere by default.)


  ;; 8. And what is

  (let [test? equal?
        a     '(pop corn)
        l     '(lemonade (pop corn) and (cake))]
    (rember-f test? a l))

  ;; '(lemonade and (cake)).


  ;; 9. Try to write `rember-f`.

  (defn rember-f-1 [test? a l]
    (cond
      (empty? l) l
      :else      (cond
                   (test? (first l) a) (rest l)
                   :else               (cons (first l)
                                             (rember-f-1 test? a (rest l))))))

  ;; This is good!


  ;; 10. What about the short version?

  (defn rember-f-2 [test? a l]
    (cond
      (empty? l)          l
      (test? (first l) a) (rest l)
      :else               (cons (first l)
                                (rember-f-2 test? a (rest l)))))


  ;; 11. How does

  (rember-f test? a l)

  ;; act where `test?` is `=`?

  ;; It acts just like `rember`.


  ;; 12. And what about

  (rember-f test? a l)

  ;; where `test?` is `equal?`

  ;; This is just `rember` with `=` replaced by `equal?`.


  ;; 13. Now we have four functions that do almost the same thing.

  ;; Yes:
  ;; - `rember` with `=`
  ;; - `rember` with `equal?`
  ;; - `rember` with `==`
  ;; and
  ;; - `rember-f`.


  ;; 14. And `rember-f` can behave like all the others.

  ;; Let's generate all versions with `rember-f`.


  ;; 15. What kind of values can functions return?

  ;; Lists and atoms.


  ;; 16. What about functions themselves?

  ;; Yes, but you probably did not know that yet.


  ;; 17. Can you say what

  (fn [a l] ...)

  ;; is?

  ;; It is a function of two arguments, `a` and `l`.


  ;; 18. Now what is

  (fn [a]
    (fn [x]
      (= x a)))

  ;; It is a function that, when passed an argument `a`, returns the function

  (fn [x]
    (= x a))

  ;; where `a` is just that argument.


  ;; 19. Is this called "Curry-ing"?

  ;; Thank you, Moses Schönfinkel (1889-1982).


  ;; 20. It is not called "Schönfinkel-ing".

  ;; Thank you, Haskell B. Curry (1900-1982).


  ;; 21. Using `defn`, give the preceding function a name.

  (defn =-c-1 [a]
    (fn [x]
      (= x a)))

  ;; This is our choice.


  ;; 22. What is

  (let [k 'salad]
    (=-c k))

  ;; Its value is a function that takes `x` as an argument and tests whether it
  ;; is `=` to 'salad.


  ;; 23. So let's give it a name using `def`.

  (def =-salad? (=-c 'salad))

  ;; Okay.


  ;; 24. What is

  (let [y 'salad]
    (=-salad? y))

  ;; true.


  ;; 25. And what is

  (let [y 'tuna]
    (=-salad? y))

  ;; false.


  ;; 26. Do we need to give a name to `=-salad?`

  ;; No, we may just as well ask

  (let [x 'salad
        y 'tuna]
    ((=-c x) y))


  ;; 27. Now rewrite `rember-f` as a function of one argument `test` that
  ;; returns an argument like `rember` with `=` replaced by `test?`.

  (defn *rember-f-1 [test?]
    (fn [a l]
      (cond
        (empty? l)          l
        (test? (first l) a) (rest l)
        :else               (cons (first l) ...))))

  ;; is a good start.


  ;; 28. Describe in your own words the result of

  (let [test? =]
    (*rember-f test?))

  ;; It is a function that takes two arguments, `a` and `l`. It compares the
  ;; elements of the list with `a`, and the first one that is `=` to `a` is
  ;; removed.


  ;; 29. Give a name to the function returned by

  (let [test? =]
    (*rember-f test?))

  (def *rember-=-1 (*rember-f =))


  ;; 30. What is

  (let [a 'tuna
        l '(tuna salad is good)]
    (*rember-= a l))

  ;; '(salad is good).


  ;; 31. Did we need to give the name `*rember-=` to the function

  (*rember-f =)

  ;; No, we could have written

  (let [test? =
        a     'tuna
        l     '(tuna salad is good)]
    ((*rember-f test?) a l))


  ;; 32. Now, complete the definition of `*rember-f`.

  (defn *rember-f-2 [test?]
    (fn [a l]
      (cond
        (empty? l)          l
        (test? (first l) a) (rest l)
        :else               (cons (first l)
                                  ((*rember-f-2 test?) a (rest l))))))


  ;; 33. What is

  (let [a 'tuna
        l '(shrimp salad and tuna salad)]
    ((*rember-f =) a l))

  ;; '(shrimp salad and salad).


  ;; 34. What is

  (let [a 'eq?
        l '(equal? eq? eqan? eqlist? eqpair?)]
    ((*rember-f =) a l))

  ;; '(equal? eqan? eqlist? eqpair?).


  ;; 35. And now transform `insertl` to `insertl-f` the same way we have
  ;; transformed `rember` ino `rember-f`.

  (defn insertl-f-1 [test?]
    (fn [new old l]
      (cond
        (empty? l)            l
        (test? (first l) old) (cons new (cons old (rest l)))
        :else                 (cons (first l)
                                    ((insertl-f-1 test?) new old (rest l))))))


  ;; 36. And now, just for the exercise, do it to `insertr`.

  (defn insertr-f-1 [test?]
    (fn [new old l]
      (cond
        (empty? l)            l
        (test? (first l) old) (cons old (cons new (rest l)))
        :else                 (cons (first l)
                                    ((insertr-f-1 test?) new old (rest l))))))


  ;; 37. Are `insertr` and `insertl` similar?

  ;; Only the middle piece is a bit different.


  ;; 38. Can you write a function `insert-g` that would insert either at the
  ;; left or at the right?

  ;; If you can, get yourself some coffee cake and relax! Otherwise, don't give
  ;; up. You'll see it in a minute.


  ;; 40. Which pieces differ?

  ;; The second lines differ from each other. In `insertl` it is:

  (= (first l) old) (cons new (cons old (rest l)))

  ;; but in `insertr` it is:

  (= (first l) old) (cons old (cons new (rest l)))


  ;; 41. Put the difference in words!

  ;; We say: "The two functions `cons` `old` and `new` in a different order onto
  ;; the `rest` of the list `l`."


  ;; 42. So how can we get rid of the difference?

  ;; You probably guessed it: by passing in a functions that expresses the
  ;; appropriate `cons`ing.


  ;; 43. Define a function `seql` that
  ;; 1. takes three arguments, and
  ;; 2. `cons`es the first argument
  ;;      onto the result of `cons`ing
  ;;        the second argument onto
  ;;          the third argument.

  (defn seql-1 [new old l]
    (cons new (cons old l)))


  ;; 44. What is:

  (defn seqr-1 [new old l]
    (cons old (cons new l)))

  ;; A function that
  ;; 1. takes three arguments, and
  ;; 2. `cons`es the second argument
  ;;      onto the result of `cons`ing
  ;;        the first argument onto
  ;;          the third argument.


  ;; 45. Do you know why we wrote these functions?

  ;; Because they express what the two differing lines in `insertl` and
  ;; `insertr` express.


  ;; 46. Try to write the function `insert-g` of one argument `seq` which
  ;; returns `insertl` where `seq` is `seql` and which returns `insertr` where
  ;; `seq` is `seqr`.

  (defn insert-g-1 [seq]
    (fn [new old l]
      (cond
        (empty? l)        l
        (= (first l) old) (seq new old (rest l))
        :else             (cons (first l)
                                ((insert-g-1 seq) new old (rest l))))))


  ;; 47. Now define `insertl` with `insert-g`.

  (def insertl-2 (insert-g seql))


  ;; 48. And `insertr`.

  (def insertr-4 (insert-g seqr))


  ;; 49. Is there something unusual about these two definitions?

  ;; Yes. Earlier we would probably have written

  (let [seq seql]
    (def insertl-3 (insert-g seq)))

  ;; and something similar with `seqr`. But, using "where" is unnecessary when
  ;; you pass functions as arguments.


  ;; 50. Is it necessary to give names to `seql` and `seqr`?

  ;; Not really. We could have passed their definitions instead.


  ;; 51. Define `insertl` again with `insert-g`. Do not pass in `seql` this time.

  (def insertl-4 (fn [new old l] (cons new (cons old l))))


  ;; 52. Is this better?

  ;; Yes, because you do not need to remember as many names. You can

  (rember func-name "your-mind")

  ;; where `func-name` is 'seql.


  ;; 53. Do you remember the definition of `subst`?

  ;; Here is one.

  (defn subst-2 [new old l]
    (cond
      (empty? l)        l
      (= (first l) old) (cons new (rest l))
      :else             (cons (first l)
                              (subst-2 new old (rest l)))))


  ;; 54. Does this look familiar?

  ;; Yes, it looks like `insertl` or `insertr`. Just the answer of the second
  ;; `cond`-line is different.


  ;; 55. Define a function like `seql` or `seqr` for `subst`.

  ;; What do you think about this?

  (defn seqs-1 [new old l]
    (cons new l))


  ;; 56. And now define `subst` using `insert-g`.

  (def subst-3 (insert-g seqs))


  ;; 57. And what do you think `yyy` is

  (defn seqrem-1 [_ _ l] l)

  (defn yyy-1 [a l]
    ((insert-g seqrem-1) false a l))

  ;; Surprise! It is our old friend `rember`. Hint: Step through the evaluation
  ;; of

  (let [a 'sausage
        l '(pizza with sausage and bacon)]
    (yyy-1 a l))

  ;; What role does false play?


  ;; 58. What you have just seen is the power of abstraction.

  ;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Ninth Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Abstract common patterns with a new function.


  ;; 59. Have we seen similar functions before?

  ;; Yes, we have even seen functions with similar lines.


  ;; 60. Do you remember `value` from chapter 6?

  (defn value-5 [nexp]
    (cond
      (atom? nexp)           nexp
      (= (operator nexp) '+) (+ (value-5 (first-sub-exp nexp)))
      (= (operator nexp) '*) (* (value-5 (second-sub-exp nexp)))
      :else                  (pow (value-5 (first-sub-exp nexp))
                                  (value-5 (second-sub-exp nexp)))))


  ;; 61. Do you see the similarities?

  ;; The last three answers are the same except for the `+`, `*`, and `pow`.


  ;; 62. Can you write the function `atom-to-function` which:
  ;; 1. takes one argument `x` and
  ;; 2. returns the function `+` if `x` is '+
  ;;    returns the function `*` if `x` is '*
  ;;    returns the function `pow` otherwise?

  (defn atom-to-function-1 [x]
    (cond
      (= x '+) +
      (= x '*) *
      :else    pow))


  ;; 63. What is

  (let [nexp '(+ 5 3)]
    (atom-to-function-1 nexp))

  ;; #function[hello-lisp.functions/pow]. The function `+`, not atom '+.


  ;; 64. Can you use `atom-to-function` to rewrite `value` with only two
  ;; `cond`-lines?

  ;; Of course.

  (defn value-6 [nexp]
    (cond
      (atom? nexp) nexp
      :else        ((atom-to-function-1 (operator nexp))
                    (value-6 (first-sub-exp nexp))
                    (value-6 (second-sub-exp nexp)))))


  ;; 65. Is this quite a bit shorter than the first version?

  ;; Yes, but that's okay. We haven't changed its meaning.


  ;; 66. Time for an apple?

  ;; One a day keeps the doctor away.


  ;; 67. Here is `multirember` again.

  (defn multirember-2 [a lat]
    (cond
      (empty? lat)      lat
      (= (first lat) a) (multirember-2 a (rest lat))
      :else             (cons (first lat)
                              (multirember-2 a (rest lat)))))

  ;; Write `multirember-f`.

  ;; No problem.

  (defn multirember-f-1 [test?]
    (fn [a lat]
      (cond
        (empty? lat)          lat
        (test? a (first lat)) ((multirember-f-1 test?) a (rest lat))
        :else                 (cons (first lat)
                                    ((multirember-f-1 test?) a (rest lat))))))


  ;; 68. What is

  (let [test? =
        a     'tuna
        lat   '(shrimp salad tuna salad and tuna)]
    ((multirember-f-1 test?) a lat))

  ;; '(shrimp salad salad and).


  ;; 69. Wasn't that easy?

  ;; Yes.


  ;; 70. Define `multirember-equal?` using `multirember-f`.

  (def multirember-equal?-1 (multirember-f equal?))


  ;; 71. Do we really need to tell `multirember-f` about 'tuna?

  ;; As `multirember-f` visits all the elements in `lat`, it always looks for
  ;; 'tuna.


  ;; 72. Does `test?` change as `multirember-f` goes through `lat`?

  ;; No, `test?` always stands for `=`, just as `a` always stands for 'tuna.


  ;; 73. Can we combine `a` and `test?`

  ;; Well, `test?` could be a function of just one argument and could compare
  ;; that argument to 'tuna.


  ;; 74. How would it do that?

  ;; The new `test?` takes one argument and compares it to 'tuna.


  ;; 75. Here is one way to write this function.

  (let [k 'tuna]
    (def =-tuna-1 (=-c k)))

  ;; Can you think of a different way of writing this function?

  ;; Yes, and here is a different way:

  (def =-tuna-2 (=-c 'tuna))


  ;; 76. Have you ever seen definitions that contain atoms?

  ;; Yes, 0, '*, '+, and many more.


  ;; 77. Perhaps we should now write `multirember-t` which is similar to
  ;; `multirember-f`. Instead of taking `test?` and returning a function,
  ;; `multirember-t` takes a function like `=-tuna` and a lat and then does its
  ;; work.

  (defn multirember-t-1 [test? lat]
    (cond
      (empty? lat)        lat
      (test? (first lat)) (multirember-t-1 test? (rest lat))
      :else               (cons (first lat)
                                (multirember-t-1 test? (rest lat)))))


  ;; 78. What is

  (let [test? (fn [x] (= 'tuna x))
        lat   '(shrimp salad tuna salad and tuna)]
    (multirember-t test? lat))

  ;; '(shrimp salad salad and).


  ;; 79. Is this easy?

  ;; It's not bad.


  ;; 80. How about this?

  (defn multirember&co-1 [a lat col]
    (cond
      (empty? lat)      (col '() '())
      (= (first lat) a) (multirember&co-1 a (rest lat)
                                          (fn [newlat seen]
                                            (col newlat
                                                 (cons (first lat) seen))))
      :else             (multirember&co-1 a (rest lat)
                                          (fn [newlat seen]
                                            (col (cons (first lat)
                                                       newlat) seen)))))

  ;; Now that looks really complicated!


  ;; 81. Here is something simpler:

  (defn a-friend-1 [x y]
    (empty? y))

  ;; Yes, it is simpler. It is a function that takes two arguments and asks
  ;; whether the second one is the empty list. It ignores its first argument.


  ;; 82. What is the value of

  (let [a   'tuna
        lat '(strawberries tuna and swordfish)
        col a-friend]
    (multirember&co a lat col))

  ;; This is not simple.


  ;; 83. So let's try a friendlier example. What is the value of

  (let [a   'tuna
        lat '()
        col a-friend]
    (multirember&co a lat col))

  ;; true, because `a-friend` is immediately used in the first answer on two
  ;; empty lists, and `a-friend` makes sure that it's second argument is empty.


  ;; 84. And what is

  (let [a   'tuna
        lat '(tuna)
        col a-friend]
    (multirember&co a lat col))

  ;; `multirember&co` asks

  (= (first lat) 'tuna)

  ;; where `lat` is '(tuna). Then it recurs on '().


  ;; 85. What are the other arguments that `multirember&co` uses for the natural
  ;; recursion?

  ;; The first one is clearly 'tuna. The third argument is a new function.


  ;; 86. What is the name of the third argument?

  ;; `col`.


  ;; 87. Do you know what `col` stands for?

  ;; The name `col` is short foe "collector." A collector is sometimes calles
  ;; a "continuation."


  ;; 88. Here is the new collector:

  (let [col a-friend
        lat '(tuna)]
    (defn new-friend-1 [newlat seen]
      (col newlat
           (cons (first lat) seen))))

  ;; Can you write this definition differently?

  ;; Do you mean the new way where we put 'tuna into the definition?

  (let [col a-friend]
    (defn new-friend-2 [newlat seen]
      (col newlat
           (cons 'tuna seen))))


  ;; 89. Can we also replace `col` with `a-friend` in such definitions because
  ;; `col` is to `a-friend` what (`first` `lat`) is to 'tuna?

  ;; Yes, we can:

  (defn new-friend-3 [newlat seen]
    (a-friend newlat
              (cons 'tuna seen)))


  ;; 90. And now?

  ;; `multirember&co` finds out that (`empty?` `lat`) is true, which means that
  ;; it uses the collector on two empty lists.


  ;; 91. Which collector is this?

  ;; It is `new-friend`.


  ;; 92. How does `a-friend` differ from `new-friend`?

  ;; `new-friend` uses `a-friend` on the empty list and the value of

  (cons 'tuna '())


  ;; 93. And what does the old collector do with such arguments?

  ;; It answers false, because its second argument is '(tuna), which is not the
  ;; empty list.


  ;; 94. What is the value of

  (let [a   'tuna
        lat '(and tuna)]
    (multirember&co a lat a-friend))

  ;;  This time around `multirember&co` recurs with yet another friend.

  (defn latest-friend [newlat seen]
    (a-friend (cons 'and newlat)
              seen))


  ;; 95. And what is the value of this recursive use of `multirember&co`?

  ;; false, since

  (a-friend '(and) '(tuna))

  ;; is false.


  ;; 96. What does (`multirember&co` `a` `lat` `f`) do?

  ;; It looks at every atom of the `lat` to see whether it is `=` to `a`. Those
  ;; atoms that are not are collected in one list `non-matches`; the others for
  ;; which the answer is true are collected in a second list `matches`. Finally,
  ;; it determines the value of (`f` `non-matches` `matches`).


  ;; 97. Final question: What is the value of

  (let [lat '(strawberries tuna and swordfish)
        col (fn last-friend [non-matches matches]
              (length non-matches))]
    (multirember&co 'tuna lat col))

  ;; 3, because `lat` contains three things that are not 'tuna, and therefore
  ;; `last-friend` is used on '(strawberries and swordfish) and '(tuna).


  ;; 98. Yes!

  ;; It's a strange meal, but we have seen foreign foods before.


  ;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Tenth Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Build functions to collect more than one value at a time.


  ;; 99. Here is an old friend.

  (defn multiinsertl-3 [new old lat]
    (cond
      (empty? lat)        lat
      (= (first lat) old) (cons new
                                (cons old
                                      (multiinsertl-3 new old (rest lat))))
      :else               (cons (first lat)
                                (multiinsertl-3 new old (rest lat)))))

  ;; Do you also remember `multiinsertr`?

  ;; No problem.

  (defn multiinsertr-2 [new old lat]
    (cond
      (empty? lat)        lat
      (= (first lat) old) (cons old
                                (cons new
                                      (multiinsertl-3 new old (rest lat))))
      :else               (cons (first lat)
                                (multiinsertl-3 new old (rest lat)))))


  ;; 100. Now try `multiinsertlr`. Hint: `multiinsertlr` inserts `new` to the
  ;; left of `oldl` and to the right of `oldr` in `lat` if `oldl` and `oldr` are
  ;; different.

  ;; This is a way of combining the two functions.

  (defn multiinsertlr-1 [new oldl oldr lat]
    (cond
      (empty? lat)         lat
      (= (first lat) oldl) (cons new
                                 (cons oldl
                                       (multiinsertlr-1 new oldl oldr
                                                        (rest lat))))
      (= (first lat) oldr) (cons oldr
                                 (cons new
                                       (multiinsertlr-1 new oldl oldr
                                                        (rest lat))))
      :else                (cons (first lat)
                                 (multiinsertlr-1 new oldl oldr (rest lat)))))


  ;; 101. The function `multiinsertlr&co` is to `multiinsertlr` what
  ;; `multirember&co` is to `multirember`.

  ;; Does this mean that `multiinsertlr&co` takes one more argument than
  ;; `multiinsertlr`?


  ;; 102. Yes, and what kind of argument is it?

  ;; It is a collector function.


  ;; 103. When `multiinsertlr&co` is done, it will use `col` on the new lat, on
  ;; the number of left insertions and the number of right insertions. Can you
  ;; write an outline of `multiinsertlr&co`?

  ;; Sure, it is just like `multiinsertlr`.

  (defn multiinsertlr&co-1 [new oldl oldr lat col]
    (cond
      (empty? lat)         (col '() 0 0)
      (= (first lat) oldl) (multiinsertlr&co-1
                            new oldl oldr (rest lat) (fn [newlat l r]
                                                       ...))
      (= (first lat) oldr) (multiinsertlr&co-1
                            new oldl oldr (rest lat) (fn [newlat l r]
                                                       ...))
      :else                (multiinsertlr&co-1 new oldl oldr (rest lat)
                                               (fn [newlat l r]
                                                 ...))))


  ;; 104. Why is `col` used on '(), 0 and 0 when (`empty?` `lat`) is true?

  ;; The empty `lat` contains neither `oldl` nor `oldr`. And this means that 0
  ;; occurrences of `oldl` and 0 occurrences of `oldr` are found and that
  ;; `multiinsertlr` will return '() when `lat` is empty.


  ;; 105. So what is the value of

  (multiinsertlr&co 'cranberries 'fish 'chips '() col)

  ;; It is the value of (col '() 0 0), which we cannot determine because we
  ;; don't know what `col` is.


  ;; 106. Is it true that `multiinsert&co` will use the new collector on three
  ;; arguments when (`first` `lat`) is equal to neither `oldl` nor `oldr`?

  ;; Yes, the first is the lat that `multiinsertlr` would have produced
  ;; for (`rest` `lat`), `oldl`, and `oldr`. The second and third are the number
  ;; of insertions that occurred to the left and right of `oldl` and `oldr`,
  ;; respectively.


  ;; 107. Is it true that `multiinsertlr&co` then uses the function `col` on

  (cons (first lat) newlat)

  ;; because it copies the list unless an `oldl` or `oldr` appears?

  ;; Yes, it is true, so we know what the new collector for the last case is:

  (fn [newlat l r]
    (col (cons (first lat) newlat) l r))


  ;; 108. Why are `col`'s second and third arguments just `l` and `r`?

  ;; If (`first` `lat`) is neither `oldl` nor `oldr`, we do not need to insert
  ;; any new elements. So, `l` and `r` are the correct results for both (`rest`
  ;; `lat`) and all of `lat`.


  ;; 109. Here is what we have so far. And we have even thrown in an extra
  ;; collector:

  (defn multiinsertlr&co-2 [new oldl oldr lat col]
    (cond
      (empty? lat)         (col '() 0 0)
      (= (first lat) oldl) (multiinsertlr&co-2
                            new oldl oldr (rest lat)
                            (fn [newlat l r]
                              (col (cons new (cons oldl newlat)) (inc l) r)))
      (= (first lat) oldr) (multiinsertlr&co-2
                            new oldl oldr (rest lat) (fn [newlat l r]
                                                       ...))
      :else                (multiinsertlr&co-2 new oldl oldr (rest lat)
                                               (fn [newlat l r]
                                                 (col (cons (first lat) newlat) l r)))))

  ;; Can you fill in the dots?

  ;; The incomplete collector is similar to the extra collector. Instead of
  ;; adding one to `l`, it adds one to `r`, and instead of `cons`ing `new` onto
  ;; `cons`ing `oldl` onto `newlat`, it `cons`es `oldr` onto the result of
  ;; `cons`ing `new` onto `newlat`.


  ;; 110. So can you fill in the dots?

  ;; Yes, the final collector is

  (fn [newlat l r]
    (col (cons oldr (cons new newlat)) l (inc r)))


  ;; 111. What is the value of

  (let [new  'salty
        oldl 'fish
        oldr 'chips
        lat  '(chips and fist or fish and chips)]
    (multiinsertlr&co new oldl oldr lat col))

  ;; It is the value of (`col` `newlat` 2 2) where `newlat` is

  '(chips salty and salty fish or salty fish and chips salty)


  ;; 112. Is this healthy?

  ;; Looks like lots of salt. Perhaps dessert is sweeter.


  ;; 113. Do you remember what `*`-functions are?

  ;; Yes, all `*`-functions work on lists that are either
  ;; - empty,
  ;; - an atom `cons`ed onto a list, or
  ;; - a list `cons`ed onto a list.


  ;; 114. Now write the function `evens-only*` which removes all odd numbers
  ;; from a list of nested lists. Here is `even?`

  (defn even?-1 [n]
    (= (* (/ n 2) 2) n))


  ;; 115. Now that we have practiced this way of writing functions,
  ;; `evens-only*` is just an exercise:

  (defn evens-only*-1 [l]
    (cond
      (empty? l)        l
      (atom? (first l)) (cond
                          (even? (first l)) (cons (first l)
                                                  (evens-only*-1 (rest l)))
                          :else             (evens-only*-1 (rest l)))
      :else             (cons (evens-only*-1 (first l))
                              (evens-only*-1 (rest l)))))


  ;; 116. What is the value of

  (let [l '((9 1 2 8) 3 10 ((9 9) 7 6) 2)]
    (evens-only* l))

  ;; '((2 8) 10 (() 6) 2).


  ;; 117. What is the sum of the odd numbers in `l` where `l` is

  '((9 1 2 8) 3 10 ((9 9) 7 6) 2)

  ;; 9 + 1 + 3 + 9 + 9 + 7 = 38.


  ;; 118. What is the product of the even numbers in `l` where `l` is

  '((9 1 2 8) 3 10 ((9 9) 7 6) 2)

  ;; 2 * 8 * 10 * 6 * 2 = 1920.


  ;; 119. Can you write the function `evens-only*&co`? It builds a nested list
  ;; of even numbers by removing the odd ones from its argument and
  ;; simultaneously multiplies the even numbers and sums up the odd numbers that
  ;; occur in its argument.

  ;; This is full of stars!


  ;; 120. Here is an outline. Can you explain what (`evens-only*&co` (`first`
  ;; `l`) ...) accomplishes?

  (defn evens-only*&co-1 [l col]
    (cond
      (empty? l)        (col '() 1 0)
      (atom? (first l)) (cond
                          (even? (first l)) (evens-only*&co-1
                                             (rest l)
                                             (fn [newl p s]
                                               (col (cons (first l) newl)
                                                    (* (first l) p)
                                                    s)))
                          :else             (evens-only*&co-1
                                             (rest l)
                                             (fn [newl p s]
                                               (col newl
                                                    p
                                                    (+ (first l) s)))))
      :else (evens-only*&co-1 (first l)
                              ...)))

  ;; It visits every number in the `first` of `l` and collects the list without
  ;; odd numbers, and the sum of the odd numbers.


  ;; 121. What does the function `evens-only*&co` do after visiting all the
  ;; numbers in (`first` `l`)?

  ;; It uses the collector, which we haven't defined yet.


  ;; 122. And what does the collector do?

  ;; It uses `evens-only*&co` to visit the `rest` of `l` and to collect the list
  ;; that is like (`rest` `l`), without the odd numbers of course, as well as
  ;; the product of the even numbers and the sum of the odd numbers.


  ;; 123. Does this mean the unknown collector looks roughly like this:

  (fn [al ap as]
    (evens-only*&co (rest l)
                    ...))

  ;; Yes.


  ;; 124. And when

  (evens-only*&co (rest l) ...)

  ;; is done with its job, what happens then?

  ;; The yet-to-be-determined collector is used, just as before.


  ;; 125. What does the collector for

  (evens-only*&co (rest l) ...)

  ;; do?


  ;; 126. It `cons`es together the results for the lists in the `first` and the
  ;; `rest` and multiplies and adds the respective products and sums. Then it
  ;; passes these values to the old collector:

  (fn [al ap as]
    (evens-only*&co (rest l)
                    (fn [dl dp ds]
                      (col (cons al dl)
                           (* ap dp)
                           (+ as ds)))))


  ;; 127. Dies this all make sense now?

  ;; Perfect.


  ;; 128. What is the value of

  (let [l '((9 1 2 8) 3 10 ((9 9) 7 6) 2)
        the-last-friend (fn [newl product sum]
                          (cons sum (cons product newl)))]
    (evens-only*&co l the-last-friend))

  ;; '(38 1920 (2 8) 10 (() 6) 2).


  ;; Whew! Is your brain twisted up now?

  ;; Go eat a pretzel; don't forget the mustard.

)
