(ns hello-lisp.little-schemer.chapter-9
  (:require
   [hello-lisp.functions :refer [atom? length pick one? a-pair? revpair
                                 *build *first *second
                                 eternity looking keep-looking shift
                                 weight* *shuffle Y]]))


;; Source: The Little Schemer, 4th edition

;; see also https://github.com/quux00/little-schemer


;; # 9. ...and Again, and Again, and Again,...

(comment

  ;; 1. Are you in the mood for 'caviar?

  ;; Then we must go `looking` for it.


  ;; 2. What is

  (let [a   'caviar
        lat '(6 2 4 caviar 5 7 3)]
    (looking a lat))

  ;; true, 'caviar is obviously in `lat`.


  ;; 3.

  (let [a   'caviar
        lat '(6 2 grits caviar 5 7 3)]
    (looking a lat))

  ;; false.


  ;; 4. Were you expecting something different?

  ;; Yes, 'caviar is still in `lat`.


  ;; 5. True enough, but what is the first number in the `lat`?

  ;; 6.


  ;; 6. And what is the sixth element of `lat`?

  ;; 7.


  ;; 7. And what is the seventh element?

  ;; 3.


  ;; 8. So `looking` clearly can't find 'caviar.

  ;; True enough, because the third element is 'grits, which does not even
  ;; resemble 'caviar.


  ;; 9. Here is `looking`

  (defn looking-1 [a lat]
    (keep-looking a (pick 1 lat) lat))

  ;; Write `keep-looking`.

  ;; We did not expect you to know this.


  ;; 10.

  (let [a   'caviar
        lat '(6 2 4 caviar 5 7 3)]
    (looking a lat))

  ;; true, because (`keep-looking` `a` 6 `lat`) has the same answer
  ;; as (`keep-looking` `a` (`pick` 1 `lat`) `lat`).


  ;; 11. What is

  (let [lat '(6 2 4 caviar 5 7 3)]
    (pick 6 lat))

  ;; 7.


  ;; 12. So what do we do?

  (let [a   'caviar
        lat '(6 2 4 caviar 5 7 3)]
    (keep-looking a 7 lat))


  ;; 13. What is

  (let [lat '(6 2 4 caviar 5 7 3)]
    (pick 7 lat))

  ;; 3.


  ;; 14. So what is

  (let [a   'caviar
        lat '(6 2 4 caviar 5 7 3)]
    (keep-looking a 3 lat))

  ;; It is the same as (`keep-looking` `a` 4 `lat`).


  ;; 15. Which is?

  ;; true.


  ;; 16. Write `keep-looking`.

  (defn keep-looking-1 [a sorn lat]
    (cond
      (number? sorn) (keep-looking-1 a (pick sorn lat) lat)
      :else          (= sorn a)))


  ;; 17. Can you guess what `sorn` stands for?

  ;; Symbol or number.


  ;; 18. What is unusual about `keep-looking`?

  ;; It does not recur on a part of `lat`.


  ;; 19. We call this "unnatural" recursion.

  ;; It is truly unnatural.


  ;; 20. Does `keep-looking` appear to get closer to its goal?

  ;; Yes, from all available evidence.


  ;; 21. Does it always get closer to its goal?

  ;; Sometimes the list may contain neither 'caviar nor 'grits.


  ;; 22. That is correct. A list may be a tup.

  ;; Yes, if we start `looking` in '(7 2 4 7 5 6 3), we will never stop looking.


  ;; 23. What is

  (let [a 'caviar
        lat '(7 1 2 caviar 5 6 3)]
    (looking a lat))

  ;; This is strange! (StackOverflowError)


  ;; 24. Yes, it is strange. What happens?

  ;; We keep looking and looking and looking...


  ;; 25. Functions like `looking` are called partial functions. What do you
  ;; think the functions we have seen so far are called?

  ;; They are called total.


  ;; 26. Can you define a shorter function that does not reach its goal for some
  ;; of its arguments?

  (defn eternity [x]
    (eternity x))


  ;; 27. For how many of its arguments does `eternity` reach its goal?

  ;; None, and this is most unnatural recursion possible.


  ;; 28. Is `eternity` partial?

  ;; It is the most partial function.


  ;; 29. What is

  (let [x '((a b) c)]
    (shift x))

  ;; '(a (b c)).


  ;; 30. What is

  (let [x '((a b) (c d))]
    (shift x))

  ;; '(a (b (c d))).


  ;; 31. Define `shift`.

  ;; This is trivial; it's not even recursive!

  (defn shift-1 [pair]
    (*build (*first (*first pair))
            (*build (*second (*first pair))
                    (*second pair))))


  ;; 32. Describe what `shift` does.

  ;; Here are our words: "The function `shift` takes a pair whose first
  ;; component is a pair and builds a pair by shifting the second part of the
  ;; first component onto the second component."


  ;; 33. Now look at this function:

  (defn align-1 [pora]
    (cond
      (atom? pora) pora
      (a-pair? (*first pora)) (align-1 (shift pora))
      :else (*build (*first pora)
                    (align-1 (*second pora)))))

  ;; What does it have in common with `keep-looking`?

  ;; Both functions change their arguments for their recursive uses but in
  ;; neither case is the change guaranteed to get us closer to the goal.


  ;; 34. Why are we not guaranteed that `align` makes progress?

  ;; In the second `cond`-line `shift` creates an argument for `align` that is
  ;; not a part of the original argument.


  ;; 35. Which Commandment does that violate?

  ;; The Seventh Commandment.


  ;; 36. Is the new argument at least smaller than the original one?

  ;; It does not look that way.


  ;; 37. Why not?

  ;; The function `shift` only rearranges the pair it gets.


  ;; 38. And?

  ;; Both the result and the argument of `shift` have the same number of atoms.


  ;; 39. Can you write a function that counts the number of atoms in `align`'s
  ;; arguments?

  ;; No problem:

  (defn length*-1 [pora]
    (cond
      (atom? pora) 1
      :else (+ (length*-1 (*first pora))
               (length*-1 (*second pora)))))


  ;; 40. Is `align` a partial function?

  ;; We don't know yet. There may be arguments for which it keeps aligning
  ;; things.


  ;; 41. Is there something else that changes about the arguments to `align` and
  ;; its recursive uses?

  ;; Yes, there is. The first component of a pair becomes simpler, though the
  ;; second component becomes more complicated.


  ;; 42. In what way is the first component simpler?

  ;; It is only a part of the original pair's first component.


  ;; 43. Doesn't this mean that `length*` is the wrong function for determining
  ;; the length of the argument? Can you find a better function?

  ;; A better function should pay more attention to the first component.


  ;; 44. How much more attention should we pay to the first component?

  ;; At least twice as much.


  ;; 45. Do you mean something like `weight*`?

  (defn weight*-1 [pora]
    (cond
      (atom? pora) 1
      :else (+ (* (weight*-1 (*first pora)) 2)
               (weight*-1 (*second pora)))))

  ;; That looks right.


  ;; 46. What is

  (let [x '((a b) c)]
    (weight* x))

  ;; 7.


  ;; 47. And what is

  (let [x '(a (b c))]
    (weight* x))

  ;; 5.


  ;; 48. Does this mean that the arguments get simpler?

  ;; Yes, the `weight*`'s of `align`'s arguments become successively smaller.


  ;; 49. Is `align` a partial function?

  ;; No, it yields a value for every argument.


  ;; 50. Here is `*shuffle` which is like `align` but uses `revpair` from chapter
  ;; 7, instead of `shift`:

  (defn *shuffle-1 [pora]
    (cond
      (atom? pora) pora
      (a-pair? (*first pora)) (*shuffle-1 (revpair pora))
      :else (*build (*first pora)
                    (*shuffle-1 (*second pora)))))

  ;; The functions `*shuffle` and `revpair` swap the components of pairs when
  ;; the first component is a pair.


  ;; 51. Does this mean that `*shuffle` is total?

  ;; We don't know.


  ;; 52. Let's try it. What is the value of

  (let [x '(a (b c))]
    (*shuffle x))

  ;; '(a (b c)).


  ;; 53.

  (let [x '(a b)]
    (*shuffle x))

  ;; '(a b).


  ;; 54. Okay, let's try something interesting. What is the value of

  (let [x '((a b) (c d))]
    (*shuffle x))

  ;; To determine this value, we need to find out what is

  (let [pora '((a b) (c d))]
    (*shuffle (revpair pora)))


  ;; 55. And how are we going to do that?

  ;; We are going to determine the value of

  (let [pora '((c d) (a b))]
    (*shuffle pora))


  ;; 56. Doesn't this mean that we need to know the value of

  (*shuffle (revpair pora))

  ;; where (`revpair` `pora`) is '((a b) (c d))?

  ;; Yes, we do.


  ;; 57. And?

  ;; The function `*shuffle` is not total because it now swaps the components of
  ;; the pair again, which means that we start all over.


  ;; 58. Is this function total?

  (defn C-1 [n]
    (cond
      (one? n) 1
      :else (cond
              (even? n) (C-1 (/ n 2))
              :else (C-1 (inc (* 3 n))))))

  ;; It doesn't yield a value for 0, but otherwise nobody knows. Thank you,
  ;; Lothar Collatz (1910-1990).


  ;; 59. What is the value of

  (A 1 0)

  ;; 2.


  ;; 60.

  (A 1 1)

  ;; 3.


  ;; 61.

  (A 2 2)

  ;; 7.


  ;; 62. Here is the definition of `A`

  (defn A-1 [n m]
    (cond
      (zero? n) (inc m)
      (zero? m) (A-1 (dec n) 1)
      :else (A-1 (dec n)
                 (A-1 n (dec m)))))

  ;; Thank you, Wilhelm Ackermann (1853-1946).


  ;; 63. What does `A` have in common with `*shuffle` and `looking`?

  ;; `A`'s arguments, like `*shuffle`'s and `looking`'s, do not necessarily
  ;; decrease for the recursion.


  ;; 64. How about an example?

  ;; That's easy: (`A` 1 2) needs the value of (`A` 0 (`A` 1 1)). And that means
  ;; we need the value of (`A` 0 3).


  ;; 65. Does `A` always give an answer?

  ;; Yes, it is total.


  ;; 66. Then what is (`A` 4 3)?

  ;; For all practical purposes, there is no answer.


  ;; 67. What does that mean?

  ;; The page that you are reading now will have decayed long before we could
  ;; possibly have calculated the value of (`A` 4 3).

  ;; But answer came there none ---
  ;;   And this was scarcely odd, because
  ;; They'd eaten every one.

  ;; The Walrus and The Carpenter
  ;; -- Lewis Carroll


  ;; 68. Wouldn't it be great if we could write a function that tells us whether
  ;; some function returns with a value for every argument?

  ;; It sure would. Now that we have seen functions that never return a value or
  ;; return a value so late that it is too late, we should have some tool like
  ;; this around.


  ;; 69. Okay, let's write it.

  ;; It sounds complicated. A function can work for many different arguments.


  ;; 70. Then let's make it simpler. For a warm-up exercise, let's focus on a
  ;; function that checks whether some function stops for just the empty list,
  ;; the simplest of all arguments.

  ;; That would simplify it a lot.


  ;; 71. Here is the beginning of this function:

  (defn will-stop?-1 [f]
    ...)

  ;; Can you fill in the dots?

  ;; What does it do?


  ;; 72. Does `will-stop?` return a value for all arguments?

  ;; That's the easy part: we said that it either returns true or false,
  ;; depending on whether the argument stops when applied to '().


  ;; 73. Is `will-stop?` total then?

  ;; Yes, it is. It always returns true or false.


  ;; 74. Then let's make up some examples. Here is the first one. What is the
  ;; value of

  (let [f length]
    (will-stop? f))

  ;; We know that (`length` `l`) is 0 where `l` is '().


  ;; 75. So?

  ;; Then the value of (`will-stop?` `length`) should be true.


  ;; 76. Absolutely. How about another example? What is the value of

  (will-stop? eternity)

  ;; (`eternity` '()) doesn't return a value. We just saw that.


  ;; 77. Does this mean the value of (`will-stop?` `eternity`) is false?

  ;; Yes, it does.


  ;; 78. Do we need more examples?

  ;; Perhaps we should do one more example.


  ;; 79. Okay, here is a function that could be an interesting argument for
  ;; `will-stop?`.

  (defn last-try-1 [x]
    (and (will-stop? last-try-1)
         (eternity x)))

  ;; What is (`will-stop?` `last-try`)?

  ;; What does it do?


  ;; 80. We need to test it on '().

  ;; If we want the value of (`last-try` '()), we must determine the value of

  (and (will-stop? last-try)
       (eternity '()))


  ;; 81. What is this value?

  ;; That depends on the value of

  (will-stop? last-try)


  ;; 82. Ther eare only two possibilities. Let's say it is false.

  ;; Okay, then

  (and false (eternity '()))

  ;; is false, since (`and` false ...) is always false.


  ;; 83. So (`last-try` '()) stopped, right?

  ;; Yes, it did.


  ;; 84. But didn't `will-stop?` predict just the opposite?

  ;; Yes, it did. We said that the value of (`will-stop?` `last-try`) was false,
  ;; which really means that `last-try` will not stop.


  ;; 85. So we must have been wrong about (`will-stop?` `last-try`)?

  ;; That's correct. It must return true, because `will-stop?` always gives an
  ;; answer. We said it was total.


  ;; 86. Fine. If (`will-stop?` `last-try`) is true, what is the value
  ;; of (`last-try` '())?

  ;; Now we just need to determine the value of

  (and true (eternity '()))

  ;; which is the same as the value of

  (eternity '())


  ;; 87. What is this value?

  ;; It doesn't have a value. We know that it doesn't stop.


  ;; 88. But that means we were wrong again!

  ;; True, since this time we said that (`will-stop?` `last-try`) was true.


  ;; 89. What do you think this means?

  ;; Here is our meaning: "We took a really close look at the two possible
  ;; cases. If we can define `will-stop?`, then (`will-stop?` `last-try`) must
  ;; yield either true or false. But it cannot --- due to the very definition of
  ;; what `will-stop?` is supposed to do. This must mean that `will-stop?`
  ;; cannot be defined."


  ;; 90. Is this unique?

  ;; Yes, it is. It makes `will-stop?` the first function that we can describe
  ;; precisely but cannot define (`def`) in our language.


  ;; 91. Is there any way around this problem?

  ;; No. Thank you, Alan M. Turing (1912-1954), and Kurt Gödel (1906-1978).


  ;; 92. What is `def`?

  ;; This is an interesting question. We just saw that `def` doesn't work for
  ;; `will-stop?`.


  ;; 93. So what are recursive definitions?

  ;; Hold tight, take a deep breath, and plunge forward when you're ready.


  ;; 94. Is this the function `length`?

  (defn length-2 [l]
    (cond
      (empty? l) 0
      :else (inc (length-2 (rest l)))))

  ;; It sure is.


  ;; 95. What if we didn't have `def` and `defn` anymore? Could we still define
  ;; `length`?

  ;; Without `defn` nothing, and especially not the body of `length`, could
  ;; refer to `length`.


  ;; 96. What does this function do?

  (fn [l]
    (cond
      (empty? l) 0
      :else (inc (eternity (rest l)))))

  ;; It determines the length of the empty list and nothing else.


  ;; 97. What happens when we use it on a non-empty list?

  ;; No answer. If we give `eternity` an argument, it gives no answer.


  ;; 98. What does it mean for this function that looks like `length`?

  ;; If just won't give any answer for non-empty lists.


  ;; 99. Suppose we could name this new function. What would be a good name?

  ;; `length0`, because the function can only determine the length of the empty
  ;; list.


  ;; 100. How would you write a function that determines the length of lists
  ;; that contain one or fewer items?

  ;; Well, we could try the following.

  (fn [l]
    (cond
      (empty? l) 0
      :else (inc (length0 (rest l)))))


  ;; 101. Almost, but `defn` doesn't workf for `length0`.

  ;; So, replace `length0` by its definition.

  (fn [l]
    (cond
      (empty? l) 0
      :else (inc ((fn [l]
                    (cond
                      (empty? l) 0
                      :else (inc (eternity (rest l)))))
                  (rest l)))))


  ;; 102. And what's a good name for this function?

  ;; That's easy: `length<=1`.


  ;; 103. Is this the function that would determine the lengths of lists that
  ;; contain two or fewer items?

  (fn [l]
    (cond
      (empty? l) 0
      :else (inc ((fn [l]
                    (cond
                      (empty? l) 0
                      :else (inc ((fn [l]
                                    (cond
                                      (empty? l) 0
                                      :else (inc (eternity (rest l)))))
                                  (rest l)))))
                  (rest l)))))

  ;; Yes, this is `length<=2`. We just replace `eternity` with the next version
  ;; of `length`.


  ;; 104. Now, what do you think recursion is?

  ;; What do you mean?


  ;; 105. Well, we have seen how to determine the length of a list with no
  ;; items, with no more than one item, with no more than two items, and so
  ;; on. How could we get the function `length` back?


  ;; 106. If we could write an infinite function in the style of `length0`,
  ;; `length<=1`, `length<=2`, ..., then we could write `length-inf`, which
  ;; would determine the length of all lists that we can make.


  ;; 106. How long are the lists that we can make?

  ;; Well, a list is either empty, or it contains one element, or two elements,
  ;; or three, or four, ..., or 1001, ...


  ;; 107. But we can't write an infinite function.

  ;; No, we can't.


  ;; 108. And we still have all these repetitions and patterns in these
  ;; functions.

  ;; Yes, we do.


  ;; 109. What do these patterns look like?

  ;; All these programs contain a function that looks like `length`. Perhaps we
  ;; should abstract out this function: see The Ninth Commandment.


  ;; 110. Let's do it!

  ;; We need a function that looks just like `length` but starts
  ;; with (`fn` [`length`] ...).


  ;; 111. Do you mean this?

  ((fn [length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc (length (rest l))))))
   eternity)

  ;; Yes, that's okay. It creates `length0`.


  ;; 112. Rewrite `length<=1` in the same style.

  ((fn [f]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc (f (rest l))))))
   ((fn [g]
      (fn [l]
        (cond
          (empty? l) 0
          :else (inc (g (rest l))))))
    eternity))


  ;; 113. Do we have to use `length` to name the argument?

  ;; No, we just used `f` and `g`. As long as we are consistent, everything's
  ;; okay.


  ;; 114. How about `length<=2`?

  ((fn [length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc (length (rest l))))))
   ((fn [length]
      (fn [l]
        (cond
          (empty? l) 0
          :else (inc (length (rest l))))))
    ((fn [length]
       (fn [l]
         (cond
           (empty? l) 0
           :else (inc (length (rest l))))))
     eternity)))


  ;; 115. Close, but there are still repetitions.

  ;; True. Let's get rid of them.


  ;; 116. Where should we start?

  ;; Name the function that takes `length` as an argument and that returns a
  ;; function that looks like `length`.


  ;; 117. What's a good name for this function?

  ;; How about `mk-length` for "make `length`"?


  ;; 118. Okay, do this to `length0`.

  ((fn [mk-length]
     (mk-length eternity))
   (fn [length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc (length (rest l)))))))


  ;; 119. Is this `length<=1`?

  ((fn [mk-length]
     (mk-length
      (mk-length
       eternity)))
   (fn [length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc (length (rest l)))))))

  ;; It sure is. And this is `length<=2`.

  ((fn [mk-length]
     (mk-length
      (mk-length
       (mk-length
        eternity))))
   (fn [length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc (length (rest l)))))))


  ;; 120. Can you write `length<=3` in this style?

  ;; Sure. Here it is.

  ((fn [mk-length]
     (mk-length
      (mk-length
       (mk-length
        (mk-length
         eternity)))))
   (fn [length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc (length (rest l)))))))


  ;; 121. What is recursion like?

  ;; It is like an infinite tower of applications of `mk-length` to an arbitrary
  ;; function.


  ;; 122. Do we really need an infinite tower?

  ;; Not really of course. Everytime we use `length` we only need a finite
  ;; number, but we never know how many.


  ;; 123. Could we guess how many we need?

  ;; Sure, but we may not guess a large enough number.


  ;; 124. When do we fing out that we didn't guess a large enough number?

  ;; When we apply the function `eternity` that is passed to the innermost
  ;; `mk-length`.


  ;; 125. What if we could create another application of `mk-length` to
  ;; `eternity` at this point?

  ;; That would only postpone the problem by one, and besides, how could we do
  ;; that?


  ;; 126. Well, since nobody cares what function we pass to `mk-length` we could
  ;; pass it `mk-length` initially.

  ;; That's the right idea. And then we invoke `mk-length` on `eternity` and the
  ;; result of this on the `rest` so that we get one more piece of the tower.


  ;; 127. Then is this still `length0`?

  ((fn [mk-length]
     (mk-length mk-length))
   (fn [length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc (length (rest l)))))))

  ;; Yes, we could even use `mk-length` instead of `length`.

  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc (mk-length (rest l)))))))


  ;; 128. Why would we want to do that?

  ;; All names are equal, but some names are more equal than others. (With
  ;; apologies to George Orwell (1903-1950).)


  ;; 129. True: as long as we use the names consistently, we are just fine.

  ;; And `mk-length` is a far more equal name than `length`. If we use a name
  ;; like `mk-length`, it is a constant reminder that the first argument to
  ;; `mk-length` is `mk-length`.


  ;; 130. Now that `mk-length` is passed to `mk-length` can we use the argument
  ;; to create an additional recursive use?

  ;; Yes, when we apply `mk-length` once, we get `length<=1`:

  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc ((mk-length eternity) (rest l)))))))


  ;; 131. What is the value of

  (let [l '(apples)]
    (((fn [mk-length]
        (mk-length mk-length))
      (fn [mk-length]
        (fn [l]
          (cond
            (empty? l) 0
            :else (inc ((mk-length eternity) (rest l)))))))
     l))

  ;; This is a good exercise. Work it out with paper and pencil.


  ;; 132. Could we do this more than once?

  ;; Yes, just keep passing `mk-length` to itself, and we can do this as often
  ;; as we need to!


  ;; 133. What would you call this function?

  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc ((mk-length mk-length) (rest l)))))))

  ;; It is `length`, of course.


  ;; 134. How does it work?

  ;; It keeps adding recursive uses by passing `mk-length` to itself, just as it
  ;; is about to expire.


  ;; 135. One problem is left: it no longer contains the function that looks
  ;; like `length`:

  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc ((mk-length mk-length) (rest l)))))))
  ;;                 ^-------------------^

  ;; Can you fix that?

  ;; We could extract this new application of `mk-length` to itself and call it
  ;; `length`.


  ;; 136. Why?

  ;; Because it really makes the function `length`.


  ;; 137. How about this?

  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     ((fn [length]
        (fn [l]
          (cond
            (empty? l) 0
            :else (inc (length (rest l))))))
      (mk-length mk-length))))

  ;; Yes, this looks just fine.


  ;; 138. Let's see whether it works.

  ;; Okay.


  ;; 139. What is the value of

  (let [l '(apples)]
    (((fn [mk-length]
        (mk-length mk-length))
      (fn [mk-length]
        ((fn [length]
           (fn [l]
             (cond
               (empty? l) 0
               :else (inc (length (rest l))))))
         (mk-length mk-length))))
     l)) ; throws StackOverflowError

  ;; It should be 1.


  ;; 140. First, we need the value of

  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     ((fn [length]
        (fn [l]
          (cond
            (empty? l) 0
            :else (inc (length (rest l))))))
      (mk-length mk-length))))

  ;; That's true, because the value of this expression is the function that we
  ;; need to apply to `l` where `l` is '(apples).


  ;; 141. So we really need the value of

  ((fn [mk-length]
     ((fn [length]
        (fn [l]
          (cond
            (empty? l) 0
            :else (inc (length (rest l))))))
      (mk-length mk-length)))
   (fn [mk-length]
     ((fn [length]
        (fn [l]
          (cond
            (empty? l) 0
            :else (inc (length (rest l))))))
      (mk-length mk-length))))

  ;; True enough.


  ;; 142. But then we really need to know the value of

  ((fn [length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc (length (rest l))))))
   ((fn [mk-length]
      ((fn [length]
         (fn [l]
           (cond
             (empty? l) 0
             :else (inc (length (rest l))))))
       (mk-length mk-length)))
    (fn [mk-length]
      ((fn [length]
         (fn [l]
           (cond
             (empty? l) 0
             :else (inc (length (rest l))))))
       (mk-length mk-length)))))

  ;; Yes, that's true, too. Where is the end of this? Don't we also need to know
  ;; the value of

  ((fn [length]
        (fn [l]
          (cond
            (empty? l) 0
            :else (inc (length (rest l))))))
   ((fn [length]
      (fn [l]
        (cond
          (empty? l) 0
          :else (inc (length (rest l))))))
    ((fn [mk-length]
       ((fn [length]
          (fn [l]
            (cond
              (empty? l) 0
              :else (inc (length (rest l))))))
        (mk-length mk-length)))
     (fn [mk-length]
       ((fn [length]
          (fn [l]
            (cond
              (empty? l) 0
              :else (inc (length (rest l))))))
        (mk-length mk-length))))))


  ;; 143. Yes, there is no end to it. Why?

  ;; Because we just keep applying `mk-length` to itself again and again and
  ;; again...


  ;; 144. Is this strange?

  ;; It is because `mk-length` used to return a function when we applied it to
  ;; an argument. Indeed, it didn't matter what we applied it to.


  ;; 145. But now that we have extracted (`mk-length` `mk-length`) from the
  ;; function that makes `length` it does not return a function anymore.

  ;; No it doestn't. So what do we do?


  ;; 146. Turn the application of `mk-length` to itself in our last correct
  ;; version of `length` into a function:

  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc ((mk-length mk-length) (rest l)))))))

  ;; How?


  ;; 147. Here is a different way. If `f` is a function of one argument, is

  (fn [x] (f x))

  ;; a function of one argument?

  ;; Yes, it is.


  ;; 148. If (`mk-length` `mk-length`) returns a function of one argument, does

  (fn [x]
    ((mk-length mk-length) x))

  ;; return a function of one argument?

  ;; Actually,

  (fn [x]
    ((mk-length mk-length) x))

  ;; is a function!


  ;; 149. Okay, let's do this to the application of `mk-length` to itself.

  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc ((fn [x]
                       ((mk-length mk-length) x))
                   ;;^--------------------------^
                     (rest l)))))))


  ;; 150. Move out the new function so that we get `length` back.

  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     ((fn [length]
        (fn [l]
          (cond
            (empty? l) 0
            :else (inc (length (rest l))))))
    ;;^------------------------------------^
      (fn [x]
        ((mk-length mk-length) x)))))


  ;; 151. Is it okay to move out the function?

  ;; Yes, we just always did the opposite by replacing a name with its
  ;; value. Here we extract a value and give it a name.


  ;; 152. Can we extract the function in the box that looks like `length` and
  ;; give it a name?

  ;; Yes, it does not depend on `mk-length` at all!


  ;; 153. Is this the right function?

  ((fn [le]
     ((fn [mk-length]
        (mk-length mk-length))
      (fn [mk-length]
        (le (fn [x]
              ((mk-length mk-length) x))))))
   (fn [length]
     (fn [l]
       (cond
         (empty? l) 0
         :else (inc (length (rest l)))))))

  ;; Yes.


  ;; 154. What did we actually get back?

  ;; We extracted the original function `mk-length`.


  ;; 155. Let's separate the function that makes `length` from the function that
  ;; looks like `length`.

  ;; That's easy.

  (fn [le]
    ((fn [mk-length]
       (mk-length mk-length))
     (fn [mk-length]
       (le (fn [x]
             ((mk-length mk-length) x))))))


  ;; 156. Does this function have a name?

  ;; Yes, it is called the applicative-order Y combinator.

  (defn Y-1 [le]
    ((fn [f]
       (f f))
     (fn [f]
       (le (fn [x] ((f f) x))))))


  ;; 157. Does `defn` work again?

  ;; Sure, now that we know what recursion is.


  ;; 158. Do you know why `Y` works?

  ;; Read this chapter just one more time and you will.


  ;; 159. What is

  (Y Y)

  ;; Who knows, but it works very hard.


  ;; 160. Does your hat still fit?

  ;; Perhaps not after such a mind stretcher.


  ;; Stop the World --- I Want to Get Off.
  ;; -- Leslie Bricusse and Anthony Newley

)
