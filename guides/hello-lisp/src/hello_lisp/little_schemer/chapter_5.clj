(ns hello-lisp.little-schemer.chapter-5
  (:require
   [hello-lisp.functions :refer [atom? lat?
                                 member* rember* insertl* insertr* subst*
                                 leftmost eqlist? equal?]]))


;; Source: The Little Schemer, 4th edition

;; see also https://github.com/quux00/little-schemer


;; # 5. *Oh My Gawd*: It's Full of Stars

(comment

  ;; 1. What is

  (let [a 'cup
        l '((coffee) cup ((tea) cup) (and (hick)) cup)]
    (rember* a l))

  ;; `rember*` is pronounced "rember-star".

  ;; '((coffee) ((tea)) (and (hick))).


  ;; 2. What is

  (let [a 'sauce
        l '(((tomato sauce)) ((bean) sauce) (and ((flying)) sauce))]
    (rember* a l))

  ;; '(((tomato)) ((bean)) (and ((flying)))).


  ;; 3. Now write `rember*`.

  (defn rember*-1 [a l]
    (cond
      (empty? l)        l
      (atom? (first l)) (cond
                          (= (first l) a) (rember*-1 a (rest l))
                          :else           (cons (first l)
                                                (rember*-1 a (rest l))))
      :else             (cons (rember*-1 a (first l))
                              (rember*-1 a (rest l)))))

  ;; Using arguments from one of our previous examples, follow through this to
  ;; see how it works. Notice that now we are recurring down the `first` of the
  ;; list, instead of just the `rest` of the list.


  ;; 4.

  (let [l '(((tomato sauce)) ((bean) sauce) (and ((flying)) sauce))]
    (lat? l))

  ;; false.


  ;; 5. Is (`first` `l`) an atom where `l` is '(((tomato sauce)) ((bean)
  ;; sauce) (and ((flying)) sauce))?

  ;; No.


  ;; 6. What is

  (let [new 'roast
        old 'chuck
        l   '((how much (wood))
              could
              ((a (wood) chuck))
              (((chuck)))
              (if (a) ((wood chuck)))
              could chuck wood)]
    (insertr* new old l))

  ;; ((how much (wood)) could ((a (wood) chuck roast)) (((chuck
  ;; roast))) (if (a) ((wood chuck roast))) could chuck roast wood).


  ;; 7. Now write the function `insertr*` which inserts the atom `new` to the
  ;; right of `old` regardless of where `old` occurs.

  (defn insertr*-1 [new old l]
    (cond
      (empty? l)        l
      (atom? (first l)) (cond
                          (= (first l) old)
                          (cons old
                                (cons new
                                      (insertr*-1 new old (rest l))))
                          :else (cons (first l)
                                      (insertr*-1 new old (rest l))))
      :else             (cons (insertr*-1 new old (first l))
                              (insertr*-1 new old (rest l)))))


  ;; 8. How are `insertr*` and `rember*` similar?

  ;; Each function asks three questions.


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The First Commandment (final version)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; When recurring on a list of atoms, `lat`, ask two questions about
  ;; it: (`empty?` `lat`) and `:else`. When recurring on a number, `n`, ask two
  ;; questions about it: (`zero?` `n`) and `:else`. When recurring on a list of
  ;; S-expressions, `l`, ask three questions about it: (`empty?`
  ;; `l`), (`atom?` (`first` `l`)), and `:else`.


  ;; 9. How are `insertr*` and `rember*` similar?

  ;; Each function recurs on the `first` of its argument when it finds out that
  ;; the argument's `first` is a list.


  ;; 10. How are `rember*` and `multirember` different?

  ;; The function `multirember` does not recur with the `first`. The function
  ;; `rember*` recurs with the `first` as well as with the `rest`. It recurs
  ;; with the `first` when it finds out that the `first` is a list.


  ;; 11. How are `insertr*` and `rember*` similar?

  ;; They both recur with the `first`, whenever the `first` is a list, as well
  ;; as with the `rest`.


  ;; 12. How are all `*`-functions similar?

  ;; They all ask three questions and recur with the `first` as well as with the
  ;; `rest`, whenever the `first` is a list.


  ;; 13. Why?

  ;; Because all `*`-functions work on lists that are either
  ;; - empty,
  ;; - an atom `cons`ed onto a list, or
  ;; - a list `cons`ed onto a list.


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Fourth Commandment (final version)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Always change at least one argument while recurring. When recurring on a
  ;; list of atoms, `lat`, use (`rest` `lat`). When recurring on a number, `n`,
  ;; use (`dec` `n`). An when recurring on a list of S-expressions, `l`,
  ;; use (`first` `l`) and (`rest` `l`) if neither (`empty?` `l`)
  ;; nor (`atom?` (`first` `l`)) are true.

  ;; It must be changed to be closer to termination. The changing argument must
  ;; be tested in the termination condition:
  ;; - when using `rest`, test termination with `empty?` and
  ;; - when using `dec`, test termination with `zero?`.


  ;; 14.

  (let [a 'banana
        l '((banana)
            (split ((((banana ice)))
                    (cream (banana))
                    sherbet))
            (banana)
            (bread)
            (banana brandy))]
    (occursomething a l))

  ;; 5.


  ;; 15. What is a better name for `occursomething`?

  ;; `occur*`.


  ;; 16. Write `occur*`.

  (defn occur*-1 [a l]
    (cond
      (empty? l)        0
      (atom? (first l)) (cond
                          (= (first l) a) (inc (occur*-1 a (rest l)))
                          :else           (occur*-1 a (rest l)))
      :else             (+ (occur*-1 a (first l))
                           (occur*-1 a (rest l)))))


  ;; 17.

  (let [new 'orange
        old 'banana
        l   '((banana)
              (split ((((banana ice)))
                      (cream (banana))
                      sherbet))
              (banana)
              (bread)
              (banana brandy))]
    (subst* new old l))

  ;; ((orange) (split ((((orange ice))) (cream (orange))
  ;; sherbet)) (orange) (bread) (orange brandy)).


  ;; 18. Write `subst*`.

  (defn subst*-1 [new old l]
    (cond
      (empty? l)        l
      (atom? (first l)) (cond
                          (= (first l) old) (cons new
                                                  (subst*-1 new old (rest l)))
                          :else             (cons (first l)
                                                  (subst*-1 new old (rest l))))
      :else             (cons (subst*-1 new old (first l))
                              (subst*-1 new old (rest l)))))


  ;; 19. What is

  (let [new 'pecker
        old 'chuck
        l   '((how much (wood))
              could
              ((a (wood) chuck))
              (((chuck)))
              (if (a) ((wood chuck)))
              could chuck wood)]
    (insertl* new old l))

  ;; ((how much (wood)) could ((a (wood) pecker chuck)) (((pecker
  ;; chuck))) (if (a) ((wood pecker chuck))) could pecker chuck wood).


  ;; 20. Write `insertl*`.

  (defn insertl*-1 [new old l]
    (cond
      (empty? l)        l
      (atom? (first l)) (cond
                          (= (first l) old)
                          (cons new
                                (cons old
                                      (insertl*-1 new old (rest l))))
                          :else (cons (first l)
                                      (insertl*-1 new old (rest l))))
      :else             (cons (insertl*-1 new old (first l))
                              (insertl*-1 new old (rest l)))))


  ;; 21.

  (let [a 'chips
        l '((potato) (chips ((with) fish) (chips)))]
    (member* a l))

  ;; true, because the atom 'chips appears in the list `l`.


  ;; 22. Write `member*`.

  (defn member*-1 [a l]
    (cond
      (empty? l)        false
      (atom? (first l)) (or (= (first l) a)
                            (member*-1 a (rest l)))
      :else             (or (member*-1 a (first l))
                            (member*-1 a (rest l)))))


  ;; 23. What is

  (let [a 'chips
        l '((potato) (chips ((with) fish) (chips)))]
    (member* a l))

  ;; true.


  ;; 24. Which 'chips did it find?

  ;; A leftmost one.


  ;; 25. What is

  (let [l '((potato) (chips ((with) fish) (chips)))]
    (leftmost l))

  ;; 'potato.


  ;; 26. What is

  (let [l '(((hot) (tuna (and))) cheese)]
    (leftmost l))

  ;; 'hot.


  ;; 27. What is

  (let [l '(((() four)) 17 (seventeen))]
    (leftmost l))

  ;; No answer (nil).


  ;; 28. What is

  (leftmost '())

  ;; No answer (nil).


  ;; 29. Can you describe what `leftmost` does?

  ;; Here is our description: "The function `leftmost` finds the leftmost atom
  ;; in a non-empty list of S-expressions that does not contain the empty list."


  ;; 30. Is `leftmost` a `*`-function?

  ;; It works on lists of S-expressions, but it only recurs on the `first`.


  ;; 31. Does `leftmost` need to ask questions about all three possible cases?

  ;; No, it only needs to ask two questions. We agreed that `leftmost` works on
  ;; non-empty lists that don't contain empty lists.


  ;; 32. Now see if you can write the function `leftmost`.

  (defn leftmost-1 [l]
    (cond
      (atom? (first l)) (first l)
      :else             (leftmost-1 (first l))))


  ;; 33. Do you remember what `or` does?

  ;; `or` asks questions one at a time until it finds one that is true. Then
  ;; `or` stops, making its value true. If it cannot find a true argument, the
  ;; value of `or` is false.


  ;; 34. What is

  (let [x 'pizza
        l '(mozzarella pizza)]
    (and (atom? (first l))
         (= (first l) x)))

  ;; false.


  ;; 35. Why is it false?

  ;; Since `and` asks (`atom?` (`first` `l`)), which is true, it then
  ;; asks (`=` (`first` `l`) `x`), which is false; hence it is false.


  ;; 36. What is

  (let [x 'pizza
        l '((mozarella mushroom) pizza)]
    (and (atom? (first l))
         (= (first l) x)))

  ;; false.


  ;; 37. Why is it false?

  ;; Since `and` asks (`atom?` (`first` `l`)), and (`first` `l`) is not an atom;
  ;; so it is false.


  ;; 38. Give an example for `x` and `l` where

  (and (atom? (first l))
       (= (first l) x))

  ;; is true.

  ;; Here's one: `x` is 'pizza and `l` is '(pizza (tastes good)).


  ;; 39. Put in your own words what `and` does.

  ;; We put it in our words: "`and` asks questions one at a time until it finds
  ;; one whose value is false. Then `and` stops with false. If none of the
  ;; expressions are false, `and` is true."


  ;; 40. true or false: it is possible that one of the arguments of `and` and
  ;; `or` is not considered?

  ;; true, because `and` stops if the first argument has the value false, and
  ;; `or` stops if the first argument has the value true.

  ;; (`cond` also had the property of not considering all of its
  ;; arguments. Because of this property, however, neither `and` nor `or` can be
  ;; defined as functions.)


  ;; 41.

  (let [l1 '(strawberry ice cream)
        l2 '(strawberry ice cream)]
    (eqlist? l1 l2))

  ;; true.


  ;; 42.

  (let [l1 '(strawberry ice cream)
        l2 '(strawberry cream ice)]
    (eqlist? l1 l2))

  ;; false.


  ;; 43.

  (let [l1 '(banana ((split)))
        l2 '((banana (split)))]
    (eqlist? l1 l2))

  ;; false.


  ;; 44.

  (let [l1 '(beef ((sausage)) (and (soda)))
        l2 '(beef ((salami)) (and (soda)))]
    (eqlist? l1 l2))

  ;; false, but almost true.


  ;; 45.

  (let [l1 '(beef ((sausage)) (and (soda)))
        l2 '(beef ((sausage)) (and (soda)))]
    (eqlist? l1 l2))

  ;; true. That's better.


  ;; 46. What is `eqlist?`

  ;; It is a function that determines if two lists are equal.


  ;; 47. How many questions will `eqlist?` have to ask about its arguments?

  ;; Nine.


  ;; 48. Can you explain why there are nine questions?

  ;; Here are our words: "Each argument may be either
  ;; - empty,
  ;; - an atom `cons`ed onto a list, or
  ;; - a list `cons`ed onto a list.

  ;; For example, at the same time as the first argument may be the empty list,
  ;; the second argument could be the empty list or have an atom or a list in
  ;; the `first` position."


  ;; 49. Write `eqlist?` using `=`.

  (defn eqlist?-1 [l1 l2]
    (cond
      (and (empty? l1)
           (empty? l2))        true
      (and (empty? l1)
           (atom? l2))         false
      (empty? l1)              false
      (and (atom? (first l1))
           (empty? l2))        false
      (and (atom? (first l1))
           (atom? (first l2))) (and (= (first l1) (first l2))
                                    (eqlist?-1 (rest l1) (rest l2)))
      (atom? (first l2))       false
      (empty? l2)              false
      (atom? (first l2))       false
      :else                    (and (eqlist?-1 (first l1) (first l2))
                                    (eqlist?-1 (rest l1) (rest l2)))))


  ;; 50. Is it okay to ask (`atom?` (`first` `l2`)) in the second question?

  ;; Yes, because we know that the second list cannot be empty. Otherwise the
  ;; first question would have been true.


  ;; 51. And why is the third question (`empty?` l1)?

  ;; At that point, we know that when the first argument is empty, the second
  ;; argument is neither the empty list nor a list with an atom as the first
  ;; element. If (`empty?` `l1`) is true now, the second argument must be a list
  ;; whose first element is also a list.


  ;; 52. true or false: if the first argument is '(), `eqlist?` responds with
  ;; true in only one case.

  ;; true. For

  (eqlist? '() l2)

  ;; to be true, `l2` must also be the empty list.


  ;; 52. Does this mean that the questions

  (and (empty? l1) (empty? l2))

  ;; and

  (or (empty? l1) (empty? l2))

  ;; suffice to determine the answer in the first three cases?

  ;; Yes. If the first question is true, `eqlist?` responds with true;
  ;; otherwise, the answer is false.


  ;; 54. Rewrite `eqlist?`.

  (defn eqlist?-2 [l1 l2]
    (cond
      (and (empty? l1) (empty? l2)) true
      (or (empty? l1) (empty? l2))  false
      (and (atom? (first l1))
           (atom? (first l2)))      (and (= (first l1) (first l2))
                                         (eqlist?-2 (rest l1) (rest l2)))
      (or (atom? (first l1))
          (atom? (first l2)))       false
      :else                         (and (eqlist?-2 (first l1) (first l2))
                                         (eqlist?-2 (rest l1) (rest l2)))))


  ;; 55. What is an S-expression?

  ;; An S-expression is either an atom or a (possibly empty) list of
  ;; S-expressions.


  ;; 56. How many questions does `equal?` ask to determine whether two
  ;; S-expressions are the same?

  ;; Four. The first argument may be an atom or a list of S-expressions at the
  ;; same time as the second argument may be an atom or a list of S-expressions.


  ;; 57. Write `equal?`.

  (defn equal?-1 [s1 s2]
    (cond
      (and (atom? s1)
           (atom? s2)) (= s1 s2)
      (atom? s1)       false
      (atom? s2)       false
      :else            (eqlist? s1 s2)))


  ;; 58. Why is the second question (`atom?` `s1`)?

  ;; If it is true, we know that the first argument is an atom and the second
  ;; argument is a list.


  ;; 59. And why is the third question (`atom?` `s2`)?

  ;; By the time we ask the third question we know that the first argument is
  ;; not an atom. So all we need to know in order to distinguish between the two
  ;; remaining cases is whether or not the second argument is an atom. The first
  ;; argument must be a list.


  ;; 60. Can we summarize the second question and the third question with `or`?

  ;; Yes, we can!


  ;; 61. Simplify `equal?`.

  (defn equal?-2 [s1 s2]
    (cond
      (and (atom? s1)
           (atom? s2)) (= s1 s2)
      (or (atom? s1)
          (atom? s2))  false
      :else            (eqlist? s1 s2)))


  ;; 62. Does `equal?` ask enough questions?

  ;; Yes. The questions cover all four possible cases.


  ;; 63. Now, rewrite `eqlist?` using `equal?`.

  (defn eqlist?-3 [l1 l2]
    (cond
      (and (empty? l1) (empty? l2)) true
      (or (empty? l1) (empty? l2))  false
      :else (and (equal? (first l1) (first l2))
                 (eqlist?-3 (rest l1) (rest l2)))))


  ;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Sixth Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Simplify only after the function is correct.


  ;; 64. Here is `rember*` after we replace `lat` by a list `l` of S-expressions
  ;; and `a` by any S-expression.

  (defn rember-4 [s l]
    (cond
      (empty? l) l
      (atom? (first l)) (cond
                          (equal? (first l) s) (rest l)
                          :else (cons (first l) (rember-4 s (rest l))))
      :else (cond
              (equal? (first l) s) (rest l)
              :else (cons (first l)
                          (rember-4 s (rest l))))))

  ;; Can we simplify it?

  ;; Obviously!

  (defn rember-5 [s l]
    (cond
      (empty? l) l
      :else (cond
              (equal? (first l) s) (rest l)
              :else (cons (first l)
                          (rember-5 s (rest l))))))


  ;; 65. And how does that differ?

  ;; The function `rember` now removes the first matching S-expression `s` in
  ;; `l`, instead of the first matching atom `a` in `lat`.


  ;; 66. Is `rember` a "star" function now?

  ;; No.


  ;; 67. Why not?

  ;; Because `rember` recurs with the `rest` of `l` only.


  ;; 68. Can `rember` be further simplified?

  ;; Yes, the inner `cond` asks questions that the outer `cond` could ask!


  ;; 69. Do it!

  (defn rember-6 [s l]
    (cond
      (empty? l) l
      (equal? (first l) s) (rest l)
      :else (cons (first l)
                  (rember-6 s (rest l)))))


  ;; 70. Does this new definition look simpler?

  ;; Yes, it does!


  ;; 71. And does it work just as well?

  ;; Yes, because we knew that all the cases and all the recursions were right
  ;; before we simplified.


  ;; 72. Simplify `insertl*`.

  ;; We can't. Before we can ask (`=` (`first` `l`) `old`), we need to know
  ;; that (`first` `l`) is an atom.


  ;; 73. When functions are correct and well-designed, we can think about them
  ;; easily.

  ;; And that saved us this time from getting it wrong.


  ;; 74. Can all functions that use `=` and `==` be generalized by replacing `=`
  ;; and `==` by the function `equal?`

  ;; Not quite; this won't work for `equan?`, but will work for all others. In
  ;; fact, disregarding the trivial example of `equan?`, that is exactly what we
  ;; shall assume. (Clojure semantics may differ, though.)


  ;; (No treats, no joy.)

)
