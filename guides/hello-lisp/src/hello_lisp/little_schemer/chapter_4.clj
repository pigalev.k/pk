(ns hello-lisp.little-schemer.chapter-4
  (:require
   [hello-lisp.functions :refer [atom?  pow length rember  pick rempick one?]]))


;; Source: The Little Schemer, 4th edition

;; see also https://github.com/quux00/little-schemer


;; # 4. Numbers Games

(comment

  ;; 1. Is 14 an atom?

  ;; Yes, because all numbers are atoms.


  ;; 2. Is true or false

  (let [n 14]
    (atom? n))

  ;; true, because 14 is an atom.


  ;; 3. Is -3 a number?

  ;; Yes, but we do not consider negative numbers.


  ;; 4. Is 3.14159 a number?

  ;; Yes, but we consider only whole numbers.


  ;; 5. Are -3 and 3.14159 numbers?

  ;; Yes, but the only numbers we use are the nonnegative integers (i.e., 0, 1,
  ;; 2, 3, 4, ...).


  ;; 6. What is (`inc` `n`) where `n` is 67?

  (let [n 67]
    (inc n))

  ;; 68.


  ;; 7. What is

  (inc 67)

  ;; Also 68, because we don't need to say "where `n` is 67" when the argument
  ;; is a number.


  ;; 8. What is

  (let [n 5]
    (dec n))

  ;; 4.


  ;; 9. What is

  (dec 0)

  ;; No answer (actually -1, but we do not consider negative numbers).


  ;; 10. Is true or false

  (zero? 0)

  ;; true.


  ;; 11. Is true or false

  (zero? 1492)

  ;; false.


  ;; 12. What is

  (+ 46 12)

  ;; 58.


  ;; 13. Try to write the function `+`. Hint: It uses `zero?`, `inc`, and `dec`.

  (defn +-1 [n m]
    (cond
      (zero? m) n
      :else     (inc (+-1 n (dec m)))))

  ;; Wasn't that easy?


  ;; 14. But didn't we just violate The First Commandment?

  ;; Yes, but we can treat `zero?` like `empty?` since `zero?` asks if a number
  ;; is empty and `empty?` asks if a list is empty.


  ;; 15. If `zero?` is like `empty?`, is `inc` like `cons`?

  ;; Yes! `cons` builds lists and `inc` builds numbers.


  ;; 16. What is

  (- 14 3)

  ;; 11.


  ;; 17. What is

  (- 17 9)

  ;; 8.


  ;; 18. What is

  (- 18 25)

  ;; No answer. There are no negative numbers.


  ;; 19. Try to write the function `--1`. Hint: Use `dec`.

  ;; How about this:

  (defn --1 [n m]
    (cond
      (zero? m) n
      :else     (dec (--1 n (dec m)))))


  ;; 20. Can you describe how (`--1` `n` `m`) works?

  ;; It takes two numbers as arguments, and reduces the second until it hits
  ;; zero. It subtracts one from the result as many times as it did to cause the
  ;; second one to reach zero.


  ;; 21. Is this a tup?

  '(2 11 3 79 47 6)

  ;; Yes: tup is short for tuple.


  ;; 22. Is this a tup?

  '(8 55 5 5555)

  ;; Yes, of course, it is also a list of numbers.


  ;; 23. Is this a tup?

  '(1 2 8 apple 4 3)

  ;; No, it's just a list of atoms.


  ;; 24. Is this a tup?

  '(3 (7 4) 13 9)

  ;; No, because it is not a list of numbers. '(7 4) is not a number.


  ;; 25. Is this a tup?

  '()

  ;; Yes, it is a list of zero numbers. This special case is the empty tup.


  ;; 26. What is (`addtup` `tup`) where `tup` is '(3 5 2 8)?

  ;; 18.


  ;; 27. What is (`addtup` `tup`) where `tup` is '(15 6 7 12 3)?

  ;; 43.


  ;; 28. What does `addtup` do?

  ;; It builds a number by totaling all the numbers in its argument.


  ;; 29. What is the natural way to build numbers from a list?

  ;; Use `+` in place of `cons`: `+` builds numbers in the same way as `cons`
  ;; builds lists.


  ;; 30. When building lists with `cons` the value of the terminal condition is
  ;; '(). What should be the value of the terminal condition when building
  ;; numbers with `+`?

  ;; 0.


  ;; 31. What is the natural terminal condition for a list?

  ;; (`empty?` `l`).


  ;; 32. What is the natural terminal condition for a tup?

  ;; (`empty?` `tup`).


  ;; 33. When we build a number from a list of numbers, what should the terminal
  ;; condition line look like?

  ;; "(`empty?` `tup`) 0", just as "(`empty?` `l`) `l`" is often the terminal
  ;; condition lint for lists.


  ;; 34. What is the terminal condition line of `addtup`?

  ;; "(`empty?` `tup`) 0".


  ;; 35. How is a lat defined?

  ;; It is either an empty list, or it contains an atom, (`first` `lat`), and a
  ;; rest, (`rest` `lat`), that is also a lat.


  ;; 36. How is a tup defined?

  ;; It is either an empty list, or it contains a number and a rest that is also
  ;; a tup.

  ;; 37. What is used in the natural recursion on a list?

  ;; (`rest` `lat`).


  ;; 38. What is used in the natural recursion on a tup?

  ;; (`rest` `lat`).


  ;; 39. Why?

  ;; Because the rest of a non-empty list is a list and the rest of a non-empty
  ;; tup is a tup.


  ;; 40. How many questions do we need to ask about a list?

  ;; Two.


  ;; 41. How many questions do we need to ask about a tup?

  ;; Two, because it is either empty or it is a number and a rest, which is
  ;; again a tup.


  ;; 42. How is a number defined?

  ;; It is either zero or it is one added to a rest, where rest is again a
  ;; number.


  ;; 43. What is the natural terminal condition for numbers?

  ;; (`zero?` `n`).


  ;; 44. What is the natural recursion on a number?

  ;; (`dec` `n`).


  ;; 45. How many questions do we need to ask about a number?

  ;; Two.


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The First Commandment (first revision)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; When recurring on a list of atoms, `lat`, ask two questions about
  ;; it: (`empty?` `lat`) and `:else`. When recurring on a number, `n`, ask two
  ;; questions about it: (`zero?` `n`) and `:else`.


  ;; 46. What does `cons` do?

  ;; It builds lists.


  ;; 47. What does `addtup` do?

  ;; It builds a number by totaling all the numbers in a tup.


  ;; 48. What is the terminal condition line of `addtup`?

  ;; "(`empty?` `tup`) 0".


  ;; 49. What is the natural recursion for `addtup`?

  ;; (`addtup` (`rest` `tup`)).


  ;; 50. What does `addtup` use to build a number?

  ;; It uses `+`, because `+` builds numbers, too!


  ;; 51. Fill the dots in the following definition:

  (defn addtup-1 [tup]
    (cond
      (empty? tup) 0
      :else        ...))

  ;; Here is what we filled in:

  (+ (first tup) (addtup-1 (rest tup))).

  ;; Notice the similarity between this line, and the last line of the function
  ;; `rember`:

  (cons (first lat) (rember a (rest lat)))


  ;; 52. What is (`*` 5 3)?

  ;; 15.


  ;; 54. What is (`*` 13 4)?

  ;; 52.


  ;; 55. What does (`*` `n` `m`) do?

  ;; It builds up a number by adding `n` up `m` times.


  ;; 56. What is the terminal condition line for `*`?

  ;; "(`zero?` `m`) 0", because `n` `*` 0 = 0.


  ;; 57. Since (`zero?` `m`) is the terminal condition, `m` must eventually be
  ;; reduced to zero. What function is used to do this?

  ;; `dec`.


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Fourth Commandment (first revision)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Always change at least one argument while recurring. It must be changed to
  ;; be closer to termination. The changing argument must be tested in the
  ;; termination condition: when using `rest`, test termination with `empty?`
  ;; and when using `dec`, test termination with `zero?`.


  ;; 58. What is another name for (`*` `n` (`dec` `m`)) in this case?

  ;; It's the natural recursion for `*`.


  ;; 59. Try to write the function `*`.

  (defn *-1 [n m]
    (cond
      (zero? m) 0
      :else     (+ n (*-1 n (dec m)))))


  ;; 60. What is

  (*-1 12 3)

  ;; 36, but let's follow through the function one time to see how we get this
  ;; value.


  ;; 61. (`zero?` `m`)?

  ;; No.


  ;; 62. What is the meaning of

  (+ n (*-1 n (dec m)))

  ;; It adds `n` (where `n` = 12) to the natural recursion. If `*-1` is correct
  ;; then

  (*-1 12 (dec 3))

  ;; should be 24.


  ;; 63. What are the new arguments of

  (*-1 n m)

  ;; `n` is 12, and `m` is 2.


  ;; 64. (`zero?` `m`)?

  ;; No.


  ;; 65. What is the meaning of

  (+ n (*-1 n (dec m)))

  ;; It adds n (where `n` = 12) to the natural recursion.


  ;; 66. What are the new arguments of

  (*-1 n m)

  ;; `n` is 12, and `m` is 1.


  ;; 67. (`zero?` `m`)?

  ;; No.


  ;; 68. What is the meaning of

  (+ n (*-1 n (dec m)))

  ;; It adds `n` (where `n` = 12) to the natural recursion.


  ;; 69. What is the value of the line

  (zero? m) 0

  ;; 0, because (`zero?` `m`) is now true.


  ;; 70. Are we finished yet?

  ;; No.


  ;; 71. Why not?

  ;; Because we still have three `+`es to pick up.


  ;; 72. What is the value of the original application?

  ;; Add 12 to 12 to 12 to 0 yielding 36. Notice that `n` has been `+`ed `m`
  ;; times.


  ;; 73. Argue, using equations, that `*-1` is the conventional multiplication
  ;; of nonnegative integers, where `n` is 12 and `m` is 3.

  (= (*-1 12 3)
     (+ 12 (*-1 12 2))
     (+ 12 12 (*-1 12 1))
     (+ 12 12 12 (*-1 12 0))
     36)

  ;; which is as we expected. This technique works for all recursive functions,
  ;; not just those that use numbers. You can use this approach to write
  ;; functions as well as to argue their correctness.


  ;; 74. Again, why is 0 the value for the terminal condition line in `*-1`?

  ;; Because 0 will not affect `+`. That is,

  (= (+ n 0) n)


  ;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Fifth Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;

  ;; When building a value with `+`, always use 0 for the value of the
  ;; terminating line, for adding 0 does not change the value of an
  ;; addition. When building a value with `*`, always use 1 for the value of the
  ;; terminating line, for multiplying by 1 does not change the value of a
  ;; multiplication. When building a value with `cons`, always consider '() for
  ;; the value of the terminating line.


  ;; 75. What is (`tup+` `tup1` `tup2`) where `tup1` is '(2 3) and `tup2` is '(4
  ;; 6)?

  ;; '(6 9).


  ;; 76. What does (`tup+` `tup1` `tup2`) do?

  ;; It adds the first number of `tup1` to the first number of `tup2`, then it
  ;; adds the second number of `tup1` to the second number of `tup2`, and so on,
  ;; building a tup of the answers, for tups of the same length.


  ;; 77. What is unusual about `tup+`?

  ;; It looks at each element of two tups at the same time, or in other words,
  ;; it recurs on two tups.


  ;; 78. If you recur on one tup how many questions do you have to ask?

  ;; Two, they are (`empty?` `tup`) and `:else`.


  ;; 79. When recurring on two tups, how many questions need to be asked about
  ;; the tups?

  ;; Four: if the first tup is empty or non-empty, and if the second tup is
  ;; empty or non-empty.


  ;; 80. Do you mean the questions

  (and (empty? tup1)
       (empty? tup2))
  (empty? tup1)
  (empty? tup2)
  :else

  ;; Yes.


  ;; 81. Can the first tup be '() at the same time as the second is other than
  ;; '()?

  ;; No, because the tups must have the same length.


  ;; 82. Does this mean

  (and (empty? tup1)
       (empty? tup2))

  ;; and

  :else

  ;; are the only questions we need to ask?

  ;; Yes, because (`empty?` `tup1`) is true exactly when (`empty?` `tup2`) is
  ;; true.


  ;; 83. Write the function `tup+`.

  (defn tup+-1 [tup1 tup2]
    (cond
      (and (empty? tup1)
           (empty? tup2)) '()
      :else               (cons (+ (first tup1) (first tup2))
                                (tup+-1 (rest tup1) (rest tup2)))))


  ;; What are the arguments of `+` in the last line?

  (first tup1)

  ;; and

  (first tup2)


  ;; 84. What are the arguments of `cons` in the last line?

  (+ (first tup1) (first tup2))

  ;; and

  (tup+-1 (rest tup1) (rest tup2))


  ;; 85. What is

  (let [tup1 '(3 7)
        tup2 '(4 6)]
    (tup+-1 tup1 tup2))

  ;; '(7 13). But let's see how it works.


  ;; 86. (`empty?` `tup1`)?

  ;; No.


  ;; 87.

  (cons (+ (first tup1) (first tup2))
        (tup+-1 (rest tup1) (rest tup2)))

  ;; `cons` 7 onto the natural recursion:

  (tup+-1 (rest tup1) (rest tup2))


  ;; 88. Why does the natural recursion include the `rest` of both arguments?

  ;; Because the typical element of the final value uses the `first` of both
  ;; tups, so now we are ready to consider the rest of both tups.


  ;; 89.

  (empty? tup1)

  ;; where `tup1` is now '(7) and `tup2` is now '(6)?

  ;; No.


  ;; 90.

  (cons (+ (first tup1) (first tup2))
        (tup+-1 (rest tup1) (rest tup2)))

  ;; `cons` 13 onto the natural recursion.


  ;; 91.

  (empty? tup1)

  ;; Yes.


  ;; 92. Then, what must be the value?

  ;; '(), because (`empty?` `tup2`) must be true.


  ;; 93. What is the value of the application?

  ;; '(7 13). That is, the `cons` of 7 onto the `cons` of 13 onto '().


  ;; 94. What problem arises when we want

  (let [tup1 '(3 7)
        tup2 '(4 6 8 1)]
    (tup+-1 tup1 tup2))

  ;; No answer, since `tup1` will become empty before `tup2`. See the First
  ;; Commandment: We did not ask all the necessary questions! But, we would like
  ;; the final value to be '(7 13 8 1).


  ;; 95. Can we still write `tup+` even if the tups are not the same length?

  ;; Yes!


  ;; 96. What new terminal condition line can we add to get the correct final
  ;; value?

  ;; Add "(empty? tup1) tup2".

  (defn tup+-2 [tup1 tup2]
    (cond
      (empty? tup1)       tup2
      (and (empty? tup1)
           (empty? tup2)) '()
      :else               (cons (+ (first tup1) (first tup2))
                                (tup+-2 (rest tup1) (rest tup2)))))



  ;; 97. What is

  (let [tup1 '(3 7 8 1)
        tup2 '(4 6)]
    (tup+-2 tup1 tup2))

  ;; No answer, since `tup2` will become empty before `tup1`. See The First
  ;; Commandment: We did not ask all the necessary questions!


  ;; 98. What do we need to include in our function?

  ;; We need to ask two more questions:

  (empty? tup1)

  ;; and

  (empty? tup2)


  ;; 99. What does the second new line look like?

  (empty? tup2) tup1


  ;; 100. Here is a definition of `tup+` that works for any two tups:

  (defn tup+-3 [tup1 tup2]
    (cond
      (and (empty? tup1)
           (empty? tup2)) '()
      (empty? tup1)       tup2
      (empty? tup2)       tup1
      :else               (cons (+ (first tup1)
                                   (first tup2))
                                (tup+-3 (rest tup1)
                                        (rest tup2)))))

  ;; Can you simplify it?

  (defn tup+-4 [tup1 tup2]
    (cond
      (empty? tup1) tup2
      (empty? tup2) tup1
      :else         (cons (+ (first tup1)
                             (first tup2))
                          (tup+-4 (rest tup1)
                                  (rest tup2)))))


  ;; 101. Does the order of the two terminal conditions matter?

  ;; No.


  ;; 102. Is `:else` the last question?

  ;; Yes, because either (`empty?` `tup1`) or (`empty?` `tup2`) is true if
  ;; either one of them does not contain at least one number.


  ;; 103. What is

  (> 12 133)

  ;; false.


  ;; 104. What is

  (> 120 11)

  ;; true.


  ;; 105. On how many numbers do we have to recur?

  ;; Two, `n` and `m`.


  ;; 106. How do we recur?

  ;; With (`dec` `n`) and (`dec` `m`).


  ;; 107. When do we recur?

  ;; When we know neither number is equal to 0.


  ;; 108. How many questions do we have to ask about `n` and `m`?

  ;; Three: (`zero?` `n`), (`zero?` `m`), and `:else`.


  ;; 109. Can you write the function `>` now using `zero?` and `dec`?

  ;; How about

  (defn >-1 [n m]
    (cond
      (zero? m) true
      (zero? n) false
      :else     (>-1 (dec n) (dec m))))


  ;; 110. Is the way we wrote `>-1` correct?

  ;; No, try it for the case where `n` and `m` are the same number. Let `n` and
  ;; `m` be 3.

  (>-1 3 3)


  ;; 111.

  (zero? 3)

  ;; No, so move to the next question.


  ;; 112.

  (zero? 3)

  ;; No, so move to the next question.


  ;; 113. What is the meaning of

  (>-1 (dec n) (dec m))

  ;; Recur, but with both arguments reduced by one.


  ;; 114.

  (zero? 2)

  ;; No, so move to the next question.


  ;; 115.

  (zero? 2)

  ;; No, so move to the next question.


  ;; 116. What is the meaning of

  (>-1 (dec n) (dec m))

  ;; Recur, but with both arguments closer to zero by one.


  ;; 117.

  (zero? 1)

  ;; No, so move to the next question.


  ;; 118.

  (zero? 1)

  ;; No, so move to the next question.


  ;; 119. What is the meaning of

  (>-1 (dec n) (dec m))

  ;; Recur, but with both arguments reduced by one.


  ;; 120.

  (zero? 0)

  ;; Yes, so value of (`>-1` `n` `m`) is true.


  ;; 121. Is this correct?

  ;; No, because 3 is not greater than 3.


  ;; 122. Does the order of the two terminal conditions matter?

  ;; Think about it.


  ;; 123. Does the order of the two terminal conditions matter?

  ;; Try it out!


  ;; 124. Does the order of the two previous answers matter?

  ;; Yes. Think first, then try.


  ;; 125. How can we change the function `>-1` to take care of this subtle
  ;; problem?

  ;; Switch the `zero?` lines:

  (defn >-2 [n m]
    (cond
      (zero? n) false
      (zero? m) true
      :else     (>-2 (dec n) (dec m))))

  (>-2 3 3)


  ;; 126. What is

  (< 4 6)

  ;; true.


  ;; 127. What is

  (< 8 3)

  ;; false.


  ;; 128. What is

  (< 6 6)

  ;; false.


  ;; 128. Now try to write `<-1`.

  (defn <-1 [n m]
    (cond
      (zero? m) false
      (zero? n) true
      :else     (<-1 (dec n) (dec m))))


  ;; 129. Here is the definition of `=`:

  (defn =-1 [n m]
    (cond
      (zero? m) (zero? n)
      (zero? n) false
      :else     (=-1 (dec n) (dec m))))

  ;; Rewrite `=` using `<` and `>`.

  (defn =-2 [n m]
    (cond
      (> n m) false
      (< n m) false
      :else   true))


  ;; 130. Does that mean we have two different functions for testing equality of
  ;; atoms?

  ;; Yes, they are `=` for atoms that are numbers and `eq?` for the others. (In
  ;; Clojure, `=` is enough for any value.)


  ;; 131.

  (pow 1 1)

  ;; 1.


  ;; 132.

  (pow 2 3)

  ;; 8.


  ;; 133.

  (pow 5 3)

  ;; 125.


  ;; 134. Now write the function `pow`. Hint: See the The First and Fifth
  ;; Commandments.

  (defn pow-1 [n m]
    (cond
      (zero? m) 1
      :else     (* n (pow-1 n (dec m)))))


  ;; 135. What is a good name for this function?

  (defn ??? [n m]
    (cond
      (< n m) 0
      :else   (inc (??? (- n m) m))))

  ;; We have never seem this kind of definition before; the natural recursion
  ;; also looks strange.


  ;; 136. What does the first question check?

  ;; It determines whether the first argument is less than the second one.


  ;; 137. And what happens in the second line?

  ;; We recur with a first argument from which we subtract the second
  ;; argument. When the function returns, we add 1 to the result.


  ;; 138. So what does the function do?

  ;; It counts how many times the second argument fits into the first one.


  ;; 139. And what do we call this?

  ;; Division.

  (defn div-1 [n m]
    (cond
      (< n m) 0
      :else   (inc (div-1 (- n m) m))))


  ;; 140. What is

  (div-1 15 4)

  ;; Easy, it's 3.


  ;; 141. How do we get there?

  ;; Easy, too:

  (= (div-1 15 4)
     (inc (div-1 11 4))
     (inc (inc (div-1 7 4)))
     (inc (inc (inc (div-1 3 4))))
     (inc (inc (inc 0)))
     3)


  ;; Wouldn't a '(ham and cheese on rye) be good right now? Don't forget the
  ;; mustard!


  ;; 142. What is the value of

  (let [lat '(hotdogs with mustard sauerkraut and pickles)]
    (length lat))

  ;; 6.


  ;; 143. What is

  (let [lat '(ham and cheese on rye)]
    (length lat))

  ;; 5.


  ;; 144. Now try to write the function `length`.

  (defn length-1 [lat]
    (cond
      (empty? lat) 0
      :else        (inc (length-1 (rest lat)))))

  (let [lat '(ham and cheese on rye)]
    (length-1 lat))


  ;; 145. What is

  (let [n   4
        lat '(lasagna spaghetti ravioli macaroni meatball)]
    (pick n lat))

  ;; 'macaroni.


  ;; 146. What is

  (pick 0 '(a))

  ;; No answer (StackOverflowError).


  ;; 147. Try to write the function `pick`.

  (defn pick-1 [n lat]
    (cond
      (zero? (dec n)) (first lat)
      :else           (pick-1 (dec n) (rest lat))))

  (let [n   4
        lat '(lasagna spaghetti ravioli macaroni meatball)]
    (pick-1 n lat))

  (pick-1 0 '(a))


  ;; 148. What is

  (let [n   3
        lat '(hotdogs with hot mustard)]
    (rempick n lat))

  ;; '(hotdogs with mustard).


  ;; 149. Now try to write `rempick`.

  (defn rempick-1 [n lat]
    (cond
      (zero? (dec n)) (rest lat)
      :else           (cons (first lat)
                            (rempick-1 (dec n) (rest lat)))))

  (let [n   3
        lat '(hotdogs with hot mustard)]
    (rempick-1 n lat))


  ;; 150. Is true or false

  (number? 'tomato)

  ;; false.


  ;; 151. Is true or false?

  (number? 76)

  ;; true.


  ;; 152. Can you write `number?` which is true if its argument is a numeric
  ;; atom and false if it is anything else?

  ;; No: `number?`, like `inc`, `dec`, `zero?`, `first`, `rest`, `cons`,
  ;; `empty?`, `=`, etc. is a primitive function.


  ;; 153. Now using `number?` write the function `no-nums` which gives as a
  ;; final value a lat obtained by removing all the numbers from the lat. For
  ;; example, where `lat` is '(5 pears 6 prunes 9 dates) the value of (`no-nums`
  ;; `lat`) is '(pears prunes dates).

  (defn no-nums-1 [lat]
    (cond
      (empty? lat) lat
      :else        (cond
                     (number? (first lat)) (no-nums-1 (rest lat))
                     :else                 (cons (first lat)
                                                 (no-nums-1 (rest lat))))))

  (no-nums-1 '(5 pears 6 prunes 9 dates))


  ;; 154. Now write `all-nums` which extracts a tup from a lat using all the
  ;; numbers in the lat.

  (defn all-nums-1 [lat]
    (cond
      (empty? lat) lat
      :else        (cond
                     (number? (first lat)) (cons (first lat)
                                                 (all-nums-1 (rest lat)))
                     :else                 (all-nums-1 (rest lat)))))

  (all-nums-1 '(5 pears 6 prunes 9 dates))


  ;; 155. Write the function `equan?` which is true if its two arguments are the
  ;; same atom. Remember to use `==` for numbers and `=` for all other atoms.

  (defn equan?-1 [a1 a2]
    (cond
      (and (number? a1)
           (number? a2)) (== a1 a2)
      (or (number? a1)
          (number? a2))  false
      :else              (= a1 a2)))


  ;; 156. Can we assume that all functions written using `=` can be generalized
  ;; by replacing `=` by `equan?`

  ;; Yes, except, of course, for `equan?` itself.


  ;; 157. Now write the function `occur` which counts the number of times an
  ;; atom `a` appears in a `lat`.

  (defn occur-1 [a lat]
    (cond
      (empty? lat) 0
      :else (cond
              (= (first lat) a) (inc (occur-1 a (rest lat)))
              :else (occur-1 a (rest lat)))))


  ;; 158. Write the function `one?` where (`one?` `n`) is true if `n` is 1 and
  ;; false otherwise.

  (defn one?-1 [n]
    (cond
      (zero? n) false
      :else (zero? (dec n))))

  ;; or

  (defn one?-2 [n]
    (cond
      :else (= n 1)))


  ;; 159. Guess how we can further simplify this function, making it a one-liner.

  ;; By removing the `cond`:

  (defn one?-3 [n]
    (= n 1))


  ;; 160. Now rewrite the function `rempick` that removes the `n`-th atom from a
  ;; lat. For example,

  (let [n 3
        lat '(lemon meringue salty pie)]
    (rempick n lat))

  ;; is '(lemon meringue pie). Use the function `one?` in your answer.

  (defn rempick-2 [n lat]
    (cond
      (one? n) (rest lat)
      :else (cons (first lat)
                  (rempick-2 (dec n)
                             (rest lat)))))


  ;; (No treat again? Meh.)

)
