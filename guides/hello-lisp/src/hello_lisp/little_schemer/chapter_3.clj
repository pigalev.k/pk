(ns hello-lisp.little-schemer.chapter-3)


;; Source: The Little Schemer, 4th edition

;; see also https://github.com/quux00/little-schemer


;; # 3. Cons the Magnificent

(comment

  ;; 1. What is (`rember` `a` `lat`) where `a` is 'mint and `lat` is '(lamb
  ;; chops and mint jelly)?

  ;; '(lamb chops and jelly). `rember` stands for "remove a member".


  ;; 2. (`rember` `a` `lat`) where `a` is 'mint and `lat` is '(lamb chops and
  ;; mint flavored mint jelly)?

  ;; '(lamb chops and flavored mint jelly).


  ;; 3. (`rember` `a` `lat`) where `a` is 'toast and `lat` is '(bacon lettuce
  ;; and tomato)?

  ;; '(bacon lettuce and tomato).


  ;; 4. (`rember` `a` `lat`) where `a` is 'cup and `lat` is '(coffee cup tea cup
  ;; and hick cup)?

  ;; '(coffee tea cup and hick cup).


  ;; 5. What does (`rember` `a` `lat`) do?

  ;; It takes an atom and a lat as its arguments, and makes a new lat with the
  ;; first occurrence of the atom in the old lat removed.


  ;; 6. What steps should we use to do this?

  ;; First we will test (`empty?` `lat`) --- The First Commandment.


  ;; 7. And if (`empty?` `lat`) is true?

  ;; Return '().


  ;; 8. What do we know if (`empty?` `lat`) is not true?

  ;; We know that there must be at least one atom in the lat.


  ;; 9. Is there any other question we should ask about the lat?

  ;; No. Either a lat is empty or it contains at least one atom.


  ;; 10. What do we do if we know that the lat contains at least one atom?

  ;; We ask whether `a` is equal to (`first` `lat`).


  ;; 11. How do we ask questions?

  ;; By using `cond`.


  ;; 12. How do we ask if `a` is the same as (`first` `lat`)?

  ;; (`=` (`first` `lat`) `a`).


  ;; 13. What would be the value of (`rember` `a` `lat`) if `a` were the same
  ;; as (`first` `lat`)?

  ;; (`rest` `lat`).


  ;; 14. What do we do if `a` is not the same as (`first` `lat`)?

  ;; We want to keep (`first` `lat`), but also find out if `a` is somewhere in
  ;; the rest of the lat.


  ;; 15. How do we remove the first occurrence of `a` in the rest of `lat`?

  ;; (`rember` `a` (`rest` `lat`)).


  ;; 16. Is there any other questions we should ask?

  ;; No.


  ;; 17. Now, let's write down what we have so far.

  (defn rember-1
    [a lat]
    (cond
      (empty? lat) lat
      :else (cond
            (= (first lat) a) (rest lat)
            :else (rember-1 a (rest lat)))))

  ;; What is the value of

  (let [a 'bacon
        lat '(bacon lettuce and tomato)]
    (rember-1 a lat))

  ;; (lettuce and tomato). Hint: Write down the function `rember-1` and its
  ;; arguments, and refer to them as you go through the next sequence of
  ;; questions.


  ;; 18. Now, let's see if this function works. What is the first question?

  ;; (`empty?` `lat`).


  ;; 19. What do we do now?

  ;; Move to the next line and ask the next question.


  ;; 20. `:else`?

  ;; Yes.


  ;; 21. What next?

  ;; Ask the next question.


  ;; 22. (`=` (`first` `lat`) `a`)?

  ;; Yes, so the value is (`rest` `lat`). In this case, it is the list '(lettuce
  ;; and tomato).


  ;; 23. Is this the correct value?

  ;; Yes, because it is the original list without the atom 'bacon.


  ;; 24. But did we really use a good example?

  ;; Who knows? But the proof of the pudding is in the eating, so let's try
  ;; another example.


  ;; 25. What does `rember-1` do?

  ;; It takes an atom and a lat as its arguments, and makes a new lat with the
  ;; first occurrence of the atom in the old lat removed.


  ;; 26. What do we do now?

  ;; We compare each atom of the lat with the atom `a`, and if the comparison
  ;; fails we build a list that begins with the atom we just compared.


  ;; 27. What is the value of (`rember-1` `a` `lat`) where `a` is 'and and `lat`
  ;; is '(bacon lettuce and tomato)?

  ;; '(bacon lettuce tomato).


  ;; 28. Let us see if our function `rember-1` works. What is the first question
  ;; asked by `rember-1`?

  ;; (`empty?` `lat`).


  ;; 29. What do we do now?

  ;; Move to the next line, and ask the next question.


  ;; 30. `:else`?

  ;; Okay, so ask the next question.


  ;; 31. (`=` (`first` `lat`) `a`)?

  ;; No, so move to the next line.


  ;; 32. What is the meaning of "`:else` (`rember-1` `a` (`rest` `lat`))"?

  ;; `:else` asks if `:else` is true --- as it always is --- and the rest of the
  ;; line says to recur with `a` and (`rest` `lat`), where `a` is 'and
  ;; and (`rest` `lat`) is '(lettuce and tomato).


  ;; 33. (`empty?` `lat`)?

  ;; No, so move to the next line.


  ;; 34. `:else`?

  ;; Sure.


  ;; 35. (`=` (`first` `lat`) `a`)?

  ;; No, so move to the next line.


  ;; 36. What is the meaning of "(`rember-1` `a` (`rest` `lat`))"?

  ;; Recur where `a` is 'and and (`rest` `lat`) is '(and tomato).


  ;; 37. (`empty?` `lat`)?

  ;; No, so move to the next line, and ask the next question.


  ;; 38. `:else`?

  ;; Of course.


  ;; 39. (`=` (`first` `lat`) `a`)?

  ;; Yes.


  ;; 40. So what is the result?

  ;; (`rest` `lat`) --- '(tomato).


  ;; 41. Is this correct?

  ;; No, since '(tomato) is not the list '(bacon lettuce tomato) with just `a`
  ;; --- 'and --- removed.


  ;; 42. What did we do wrong?

  ;; We dropped 'and, but we also lost all the atoms preceding 'and.


  ;; 43. How can we keep from losing the atoms 'bacon and 'lettuce?

  ;; We use Cons the Magnificent. Remember `cons`, from chapter 1?


  ;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Second Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Use `cons` to build lists.


  ;; 43. Let's see what happens when we use `cons`.

  (defn rember-2 [a lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) a) (rest lat)
              :else (cons (first lat) (rember-2 a (rest lat))))))

  ;; What is the value of

  (let [a 'and
        lat '(bacon lettuce and tomato)]
    (rember-2 a lat))

  ;; (bacon lettuce tomato). Hint: Make a copy of this function with `cons` and
  ;; the arguments `a` and `lat` so you can refer to it for the following
  ;; questions.


  ;; 44. What is the first question?

  ;; (`empty?` `lat`).


  ;; 45. What do we do now?

  ;; Ask the next question.


  ;; 46. `:else`?

  ;; Yes, of course.


  ;; 47. (`=` (`first` `lat`) `a`)?

  ;; No, so move to the next line.


  ;; 48. What is the meaning of "(`cons` (`first` `lat`) (`rember-2` `a` (`rest`
  ;; `lat`)))", where `a` is 'and and `lat` is '(bacon lettuce and tomato)?

  ;; It says to `cons` the `first` of `lat` --- 'bacon --- onto the value
  ;; of (`rember-2` `a` (`rest` `lat`)). But since we don't know the value
  ;; of (`rember-2` `a` (`rest` `lat`)) yet, we must find it before we can
  ;; `cons` (`first` `lat`) onto it.


  ;; 49. What is the meaning of (`rember-2` `a` (`rest` `lat`))?

  ;; This refers to the function with `lat` replaced by (`rest` `lat`) ---
  ;; '(lettuce and tomato).


  ;; 50. (`empty?` `lat`)?

  ;; No, so move to the next line.


  ;; 51. `:else`?

  ;; Yes, ask the next question.


  ;; 52. (`=` (`first` `lat`) `a`)?

  ;; No, so move to the next line.


  ;; 53. What is the meaning of "(`cons` (`first` `lat`) (`rember-2` `a` (`rest`
  ;; `lat`)))"?

  ;; It says to `cons` the `first` of `lat` --- 'lettuce --- onto the value
  ;; of (`rember-2` `a` (`rest` `lat`)). But since we don't know the value
  ;; of (`rember-2` `a` (`rest` `lat`)) yet, we must find it before we can
  ;; `cons` (`first` `lat`) onto it.


  ;; 54. What is the meaning of "(`rember-2` `a` (`rest` `lat`))"?

  ;; This refers to the function with `lat` replaced by (`rest` `lat`), that is,
  ;; '(and tomato).


  ;; 55. (`empty?` `lat`)?

  ;; No, so ask the next question.


  ;; 56. `:else`?

  ;; Still.


  ;; 57. (`=` (`first` `lat`) `a`)?

  ;; Yes.


  ;; 58. What is the value of the line "(`=` (`first` `lat`) `a`) (`rest`
  ;; `lat`)"?

  ;; (`rest` `lat`) --- '(tomato).


  ;; 59. Are we finished?

  ;; Certainly not! We know what (`rember-2` `a` `lat`) is when `lat` is '(and
  ;; tomato), but we don't yet know what it is when `lat` is '(lettuce and
  ;; tomato) or '(bacon lettuce and tomato).


  ;; 60. We now have a value for (`rember-2` `a` (`rest` `lat`)) where `a` is
  ;; 'and and (`rest` `lat`) is '(and tomato). This value is '(tomato). What
  ;; next?

  ;; Recall that we wanted to `cons` 'lettuce onto the value of (`rember-2`
  ;; `a` (`rest` `lat`)) where `a` was 'and and (`rest` `lat`) was '(and
  ;; tomato). Now that we have this value, which is '(tomato), we can `cons`
  ;; 'lettuce onto it.


  ;; 61. What is the result when we `cons` 'lettuce onto '(tomato)?

  ;; '(lettuce tomato).


  ;; 61. What does '(lettuce tomato) represent?

  ;; It represents the value of (`cons` (`first` `lat`) (`rember-2` `a` (`rest`
  ;; `lat`))), when `lat` is '(lettuce and tomato), and (`rember-2` `a` (`rest`
  ;; `lat`)) is '(tomato).


  ;; 62. Are we finished yet?

  ;; Not quite. So far we know what (`rember-2` `a` `lat`) is when `lat` is
  ;; '(lettuce and tomato), but we don't yet know what it is when `lat` is
  ;; '(bacon lettuce and tomato).


  ;; 63. We now have a value for (`rember-2` `a` (`rest` `lat`)) where `a` is
  ;; 'and and (`rest` `lat`) is '(lettuce and tomato). This value is '(lettuce
  ;; tomato). This is not the final value, so what must we do again?

  ;; Recall that, at one time, we wanted to `cons` 'bacon onto the value
  ;; of (`rember-2` `a` (`rest` `lat`)), where `a` was 'and and (`rest` `lat`)
  ;; was '(lettuce and tomato). Now that we have this value, which is '(lettuce
  ;; tomato), we can `cons` 'bacon onto it.


  ;; 64. What is the result when we `cons` 'bacon onto '(lettuce tomato)?

  ;; '(bacon lettuce tomato).


  ;; 65. What does '(bacon lettuce tomato) represent? (Lunch?)

  ;; It represents the value of (`cons` (`first` `lat`) (`rember-2` `a` (`rest`
  ;; `lat`))), when `lat` is '(bacon lettuce and tomato) and (`rember-2`
  ;; `a` (`rest` `lat`)) is '(lettuce tomato).


  ;; 66. Are we finished yet?

  ;; Yes.


  ;; 67. Can you put in your own words how we determined the final value '(bacon
  ;; lettuce tomato)?

  ;; In our words: "The function `rember-2` checked each atom of the `lat`, one
  ;; at a time, to see if it was the same as the atom 'and. If the `first` was
  ;; not the same as the atom, we saved it to be `cons`ed to the final value
  ;; later. When `rember-2` found the atom 'and, it dropped it, and `cons`ed the
  ;; previous atoms back onto the rest of the `lat`."


  ;; 68. Can you rewrite `rember-2` so that it reflects the above description?

  ;; Yes, we can simplify it.

  (defn rember-3 [a lat]
    (cond
      (empty? lat) lat
      (= (first lat) a) (rest lat)
      :else (cons (first lat) (rember-3 a (rest lat)))))


  ;; 69. Do you think this is simpler?

  ;; Functions like `rember-2` can always be simplified in this manner.


  ;; 70. So why don't we simplify right away?

  ;; Because then a function's structure does not coincide with its argument
  ;; structure.


  ;; Let's see if the new `rember-3` is the same as the old one. What is the
  ;; value of the application

  (let [a 'and
        lat '(bacon lettuce and tomato)]
    (rember-3 a lat))

  ;; (bacon lettuce tomato). Hint: Write down the function `rember-3` and its
  ;; arguments and refer to them as you go through the next sequence of
  ;; questions.


  ;; 71. (`empty?` `lat`)?

  ;; No.


  ;; 72. (`=` (`first` `lat`) `a`)?

  ;; No.


  ;; 73. `:else`?

  ;; Yes, so the value is (`cons` (`first` `lat`) (`rember-3` `a` (`rest`
  ;; `lat`))).


  ;; 74. What is the meaning of (`cons` (`first` `lat`) (`rember-3` `a` (`rest`
  ;; `lat`)))?

  ;; This says to refer to the function `rember-3` but with the argument `lat`
  ;; replaced (`rest` `lat`), and that after we arrive at a value
  ;; for (`rember-3` `a` (`rest` `lat`)) we must `cons` (`first` `lat`) ---
  ;; 'bacon --- onto it.


  ;; 75. (`empty?` `lat`)?

  ;; No.


  ;; 76. (`=` (`first` `lat`) `a`)?

  ;; No.


  ;; 77. `:else`?

  ;; Yes, so the value is (`cons` (`first` `lat`) (`rember-3` `a` (`rest`
  ;; `lat`))).


  ;; 78. What is the meaning of (`cons` (`first` `lat`) (`rember-3` `a` (`rest`
  ;; `lat`)))?

  ;; This says we recur using the function `rember-3`, with the argument `lat`
  ;; replaced by (`rest` `lat`), and that after we arrive at a value
  ;; for (`rember-3` `a` (`rest` `lat`)), we must `cons` (`first` `lat`) ---
  ;; 'lettuce --- onto it.


  ;; 79. (`empty?` `lat`)?

  ;; No.


  ;; 80. (`=` (`first` `lat`) `a`)?

  ;; Yes.


  ;; 81. What is the value of the line "(`=` (`first` `lat`) `a`) (`rest`
  ;; `lat`)"?

  ;; It is (`rest` `lat`) --- '(tomato).


  ;; 82. Now what?

  ;; Now `cons` (`first` `lat`) --- 'lettuce --- onto '(tomato).


  ;; 83. Now what?

  ;; Now `cons` (`first` `lat`) --- 'bacon --- onto '(lettuce tomato).


  ;; 84. Now that we have completed `rember-3` try this example:

  (let [a 'sauce
        lat '(soy sauce and tomato sauce)]
    (rember-3 a lat))

  ;; (soy and tomato sauce).


  ;; 85. What is (`firsts` `l`) where `l` is

  '((apple peach pumpkin)
    (plum pear cherry)
    (grape raisin pea)
    (bean carrot eggplant))

  ;; '(apple plum grape bean).


  ;; 86. What is (`firsts` `l`) where `l` is

  '((a b) (c d) (e f))

  ;; '(a c e).


  ;; 87. What is (`firsts` `l`) where `l` is

  '()

  ;; '().


  ;; 88. What is (`firsts` `l`) where `l` is

  '((five plums)
    (four)
    (eleven green oranges))

  ;; (five four eleven).


  ;; 89. What is (`firsts` `l`) where `l` is

  '(((five plums) four)
    (eleven green oranges)
    ((no) more))

  ;; ((five plums) eleven (no)).


  ;; 90. In your own words, what does (`firsts` `l`) do?

  ;; We tried the following: "The function `firsts` takes one argument, a list,
  ;; which is either an empty list or contains only non-empty lists. It builds
  ;; another list composed of the first S-expression of each internal list."


  ;; 91. See if you can write the function `firsts-1`. Remember the
  ;; Commandments!

  ;; This much is easy:

  (defn firsts-1 [l]
    (cond
      (empty? l) ...
      :else (cons ... (firsts-1 (rest l)))))


  ;; 92. Why `firsts-1` and [`l`]?

  ;; Because we always state the function name and the argument(s) of the
  ;; function.


  ;; 93. Why `cond`?

  ;; Because we need to ask questions about the actual arguments.


  ;; 94. Why "(`empty?` l) ..."?

  ;; The First Commandment.


  ;; 95. Why `:else`?

  ;; Because we only have two questions to ask about the list `l`: either it is
  ;; the empty list, or it contains at least one non-empty list.


  ;; 96. Why `:else`?

  ;; See above. And because the last question is always `:else`.


  ;; 97. Why `cons`?

  ;; Because we are building a list --- The Second Commandment.


  ;; 98. Why (`firsts-1` (`rest` `l`))?

  ;; Because we can only look at one S-expression at a time. To look at the
  ;; rest, we must recur.


  ;; 99. Why "))"?

  ;; Because these are the matching parentheses for `cond` and `defn`, and they
  ;; always appear at the end of a function definition.


  ;; 100. Keeping in mind the definition of (`firsts-1` `l`), what is a typical
  ;; element of the value of (`firsts-1` `l`) where `l` is

  '((a b) (c d) (e f))

  ;; 'a.


  ;; 101. What is another typical element?

  ;; 'c, or even 'e.


  ;; 102. Consider the function `seconds`. What would be a typical element of
  ;; the value of (`seconds` `l`) where `l` is

  '((a b) (c d) (e f))

  ;; 'b, d, or 'f.


  ;; 103. How do we describe a typical element for (`firsts-1` `l`)?

  ;; As the `first` of an element of `l` --- (`first` (`first` `l`)). See
  ;; chapter 1.


  ;; 104. When we find a typical element of (`firsts-1` `l`), what do we do with
  ;; it?

  ;; `cons` it onto the recursion --- (`firsts-1` (`rest` `l`)).


  ;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Third Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;

  ;; When building a list, describe the first typical element, and then `cons`
  ;; it onto the natural recursion.


  ;; 105. With The Third Commandment, we can now fill in more of the function
  ;; `firsts-1`. What does the last line look like now?

  ;; "`:else` (`cons` (`first` (`first` `l`)) (`firsts-1` (`rest` `l`)) )".
  ;;                  ^---typical element---^ ^---natural recursion---^


  ;; 106. What does (`firsts-1` `l`) do

  (defn firsts-1 [l]
    (cond
      (empty? l) ...
      :else (cons (first (first l))
                  (firsts-1 (rest l)))))

  ;; where `l` is

  '((a b) (c d) (e f))

  ;; Nothing yet. We are still missing one important ingredient in our
  ;; recipe. The line "(`empty?` `l`) ..." needs a value for the case where `l`
  ;; is the empty list. We can, however, proceed without it for now.


  ;; 107. (`empty?` `l`) where `l` is '((a b) (c d) (e f))?

  ;; No, so move to the next line.


  ;; 108. What is the meaning of

  (cons (first (first l)) (firsts-1 (rest l)))

  ;; It saves (`first` (`first` `l`)) to `cons` onto (`firsts-1` (`rest`
  ;; `l`)). To find (`firsts-1` (`rest` `l`)), we refer to the function with the
  ;; new argument (`rest` `l`).


  ;; 109. (`empty?` `l`) where `l` is

  '((c d) (e f))

  ;; No, so move to the next line.


  ;; 110. What is the meaning of

  (cons (first (first l)) (firsts-1 (rest l)))

  ;; Save (`first` (`first` `l`)), and recur with (`firsts-1` (`rest` `l`)).


  ;; 111. (`empty?` `l`) where `l` is

  '((e f))

  ;; No, so move to the next line.


  ;; 112. What is the meaning of

  (cons (first (first l)) (firsts-1 (rest l)))

  ;; Save (`first` (`first` `l`)), and recur with (`firsts-1` (`rest` `l`)).


  ;; 113. (`empty?` `l`)?

  ;; Yes.


  ;; 114. Now, what is the value of the line "(`empty?` `l`) ..."?

  ;; There is no value; something is missing.


  ;; 115. What do we need to `cons` atoms onto?

  ;; A list. Remember The Law of `cons`.


  ;; 116. For the purpose of `cons`ing, what value can we give when (`empty?`
  ;; `l`) is true?

  ;; Since the final value must be a list, we cannot use true or false. Let's
  ;; try '().


  ;; 117. With '() as a value, we now have three `cons` steps to go back and
  ;; pick up. We need to:

  ;; I. Either
  ;;   1. `cons` 'e onto '()
  ;;   2. `cons` 'c onto the value of 1.
  ;;   3. `cons` 'a onto the value of 2.
  ;; II. or
  ;;   1. `cons` 'a onto the value of 2.
  ;;   2. `cons` 'c onto the value of 3.
  ;;   3. `cons` 'e onto '()
  ;; III. or
  ;;   - `cons` 'a onto
  ;;     - the `cons` of 'c onto
  ;;       - the `cons` of 'e onto
  ;;         - '()

  ;; In any case, what is the value of (`firsts-1` `l`)?

  ;; '(a c e).


  ;; 118. With which of the three alternatives do you feel most comfortable?

  ;; Correct! Now you should use that one.


  ;; 119. What is (`insertr` `new` `old` `lat`) where `new` is 'topping, `old`
  ;; is 'fudge, and `lat` is '(ice cream with fudge for dessert)?

  ;; '(ice cream with fudge topping for dessert).


  ;; 120. (`insertr` `new` `old` `lat`) where `new` is 'jalapeño, `old` is 'and,
  ;; and `lat` is '(tacos tamales and salsa)?

  ;; '(tacos tamales and jalapeño salsa).


  ;; 121. (`insertr` `new` `old` `lat`) where `new` is 'e, `old` is 'd, and
  ;; `lat` is '(a b c d f g d h)?

  ;; '(a b c d e f g d h).


  ;; 122. In your own words, what does (`insertr` `new` `old` `lat`) do?

  ;; In our words: "It takes three arguments: the atoms `new` and `old`, and a
  ;; lat. The function `insertr` builds a lat with `new` inserted to the right
  ;; of the first occurrence of `old`."


  ;; 123. See if you can write the first three (actually two) lines of the
  ;; function `insertr-1`.

  (defn insertr-1 [new old lat]
    (cond ...))


  ;; 124. Which argument changes when we recur with `insertr-1`?

  ;; `lat`, because we can only look at one of its atoms a time.


  ;; 125. How many questions can we ask about the `lat`?

  ;; Two. A lat is either the empty list or a non-empty list of atoms.


  ;; 126. Which questions do we ask?

  ;; First, we ask (`empty?` `lat`). Second, we ask `:else`, because `:else` is
  ;; always the last question.


  ;; 127. What do we know if (`empty?` `lat`) is not true?

  ;; We know that `lat` has at least one element.


  ;; 128. Which questions do we ask about the first element?

  ;; First, we ask (`=` (`first` `lat`) `old`). Then we ask `:else`, because
  ;; there are no other interesting cases.


  ;; 129. Now see if you can write the whole function `insertr-1`.

  (defn insertr-1 [new old lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) old) (rest lat)
              :else (cons (first lat)
                          (insertr-1 new old (rest lat))))))


  ;; 130. What is the value of the application

  (let [new 'topping
        old 'fudge
        lat '(ice cream with fudge for dessert)]
    (insertr-1 new old lat))

  ;; (ice cream with for dessert).


  ;; 131. So far this is the same as `rember`. What do we do in `insertr-1`
  ;; when (`=` (`first` `lat`) `old`) is true?

  ;; When (`first` `lat`) is the same as `old`, we want to insert `new` to the
  ;; right.


  ;; 132. How is this done?

  ;; Let's try `cons`ing `new` onto (`rest` `lat`).


  ;; 133. Now we have

  (defn insertr-2 [new old lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) old) (cons new (rest lat))
              :else (cons (first lat)
                          (insertr-2 new old (rest lat))))))

  ;; Yes.


  ;; 134. So what is now

  (let [new 'topping
        old 'fudge
        lat '(ice cream with fudge for dessert)]
    (insertr-2 new old lat))

  ;; (ice cream with topping for dessert).


  ;; 135. Is this the list we wanted?

  ;; No, we have only replaced 'fudge with 'topping.


  ;; 136. What still need to be done?

  ;; Somehow we need to include the atom that is the same as `old` before the
  ;; atom `new`.


  ;; 137. How can we include `old` before `new`?

  ;; Try `cons`ing `old` onto (`cons` `new` (`rest` `lat`)).


  ;; 138. Now let's write the rest of the function `insertr-3`.

  (defn insertr-3 [new old lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) old) (cons old (cons new (rest lat)))
              :else (cons (first lat)
                          (insertr-3 new old (rest lat))))))

  ;; When `new` is 'topping, `old` is 'fudge, and `lat` is '(ice cream with
  ;; fudge topping for dessert), the value of the application

  (let [new 'topping
        old 'fudge
        lat '(ice cream with fudge for dessert)]
    (insertr-3 new old lat))

  ;; (ice cream with fudge topping for dessert). If you got this right, have one.


  ;; 139. Now try `insertl-1`. Hint: `insertl-1` inserts the atom `new` to the
  ;; left of the first occurrence of the atom `old` in `lat`.

  ;; This much is easy, right?

  (defn insertl-1 [new old lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) old) (cons new (cons old (rest lat)))
              :else (cons (first lat)
                          (insertl-1 new old (rest lat))))))


  ;; 140. Did you think of a different way to do it?

  ;; For example,

  (= (first lat) old) (cons new (cons old (rest lat)))

  ;; could have been

  (= (first lat) old) (cons new lat)

  ;; since (`cons` `old` (`rest` `lat`)) where `old` is `=` to (`first` `lat`)
  ;; is the same as `lat`.


  ;; 141. Now try `subst`. Hint: (`subst` `new` `old` `lat`) replaces the first
  ;; occurrence of `old` in the `lat` with `new`. For example, where `new` is
  ;; 'topping, `old` is 'fudge, and `lat` is '(ice cream with fudge for dessert)
  ;; the value is '(ice cream with topping for dessert). Now you have the idea.

  ;; Obviously,

  (defn subst-1 [new old lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) old) (cons new (rest lat))
              :else (cons (first lat)
                          (subst-1 new old (rest lat))))))

  ;; This is the same as one of our incorrect attempts at `insertr`.


  ;; Go `cons` a piece of cake onto your mouth.


  ;; 142. Now try `subst2`. Hint: (`subst2` `new` `o1` `o2` `lat`) replaces
  ;; either the first occurrence of `o1` or the first occurrence of `o2` by
  ;; `new`. For example, the value of

  (let [new 'vanilla
        o1 'chocolate
        o2 'banana
        lat '(banana ice cream with chocolate topping)]
    (subst2 new o1 o2 lat))

  ;; is '(vanilla ice cream with chocolate topping).

  (defn subst2-1 [new o1 o2 lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) o1) (cons new (rest lat))
              (= (first lat) o2) (cons new (rest lat))
              :else (cons (first lat)
                          (subst2-1 new o1 o2 (rest lat))))))


  ;; 143. Did you think of a better way?

  ;; Replace the two `=` lines about the (`first` `lat`) with one `or` clause.


  ;; If you got the last function, go and repeat the cake-`cons`ing.


  ;; 144. Do you recall what `rember` does?

  ;; The function `rember` looks at each atom of a lat to see if it is the same
  ;; as the atom `a`. If it is not, `rember` saves the atom and proceeds. When
  ;; it finds the first occurrence of `a`, it stops and gives the value (`rest`
  ;; `lat`), or the rest of the lat, so that the value returned is the original
  ;; list, with only that occurrence of `a` removed.


  ;; 145. Write the function `multirember` which gives as its final value the
  ;; lat with all occurrences of `a` removed. Hint: What do we want as the value
  ;; when (`=` (`first` `lat`) `a`) is true? Consider the example where `a` is
  ;; 'cup and `lat` is '(coffee cup tea cup and hick cup).

  (defn multirember-1 [a lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) a) (multirember-1 a (rest lat))
              :else (cons (first lat)
                          (multirember-1 a (rest lat))))))

  ;; After the first occurrence of `a`, we now recur with (`multirember-1`
  ;; `a` (`rest` `lat`)), in order to remove the other occurrences.

  ;; The value of the application is

  (let [a 'cup
        lat '(coffee cup tea cup and hick cup)]
    (multirember-1 a lat))

  ;; (coffee tea and hick).


  ;; 146. Can you see how `multirember-1` works?

  ;; Possibly not, so we will go through the steps necessary to arrive at the
  ;; value '(coffee tea and hick).


  ;; 147. (`empty?` `lat`)?

  ;; No, so move to the next line.


  ;; 148. `:else`?

  ;; Yes.


  ;; 149. (`=` (`first` `lat`) `a`)?

  ;; No, so move to the next line.


  ;; 150. What is the meaning of

  (cons (first lat) (multirember-1 a (rest lat)))

  ;; Save (`first` `lat`) --- 'coffee --- to be `cons`ed onto the value
  ;; of (`multirember-1` `a` (`rest` `lat`)).


  ;; 151. (`empty?` `lat`)?

  ;; No, so move to the next line.


  ;; 152. `:else`?

  ;; Naturally.


  ;; 153. (`=` (`first` `lat`) `a`)?

  ;; Yes, so forget (`first` `lat`), and determine (`multirember-1` `a` (`rest`
  ;; `lat`)).


  ;; 154. (`empty?` `lat`)?

  ;; No, so move to the next line.


  ;; 155. `:else`?

  ;; Yes!


  ;; 156. (`=` (`first` `lat`) `a`)?

  ;; No, so move to the next line.


  ;; 157. What is the meaning of

  (cons (first lat) (multirember-1 a (rest lat)))

  ;; Save (`first` `lat`) --- 'tea --- to be `cons`ed onto the value
  ;; of (`multirember-1` `a` (`rest` `lat`)) later. Now
  ;; determine (`multirember-1` `a` (`rest` `lat`)).


  ;; 158. (`empty?` `lat`)?

  ;; No, so move to the next line.


  ;; 159. `:else`?

  ;; Okay, move on.


  ;; 160. (`=` (`first` `lat`) `a`)?

  ;; Yes, so forget (`first` `lat`), and determine (`multirember-1` `a` (`rest`
  ;; `lat`)).


  ;; 161. (`empty?` `lat`)?

  ;; No, so move to the next line.


  ;; 162. (`=` (`first` `lat`) `a`)?

  ;; No, so move to the next line.


  ;; 163. What is the meaning of

  (cons (first lat) (multirember-1 a (rest lat)))

  ;; Save (`first` `lat`) --- 'and --- to be `cons`ed onto the value
  ;; of (`multirember-1` `a` (`rest` `lat`)) later. Now
  ;; determine (`multirember-1` `a` (`rest` `lat`)).


  ;; 164. (`empty?` `lat`)?

  ;; No, so move to the next line.


  ;; 165. (`=` (`first` `lat`) `a`)?

  ;; No, so move to the next line.


  ;; 166. What is the meaning of

  (cons (first lat) (multirember-1 a (rest lat)))

  ;; Save (`first` `lat`) --- 'hick --- to be `cons`ed onto the value
  ;; of (`multirember-1` `a` (`rest` `lat`)) later. Now
  ;; determine (`multirember-1` `a` (`rest` `lat`)).


  ;; 167. (`empty?` `lat`)?

  ;; No, so move to a next line.


  ;; 168. (`=` (`first` `lat`) `a`)?

  ;; Yes, so forget (`first` `lat`), and determine (`multirember-1` `a` (`rest`
  ;; `lat`)).


  ;; 169. (`empty?` `lat`)?

  ;; Yes, so the value is '().


  ;; 170. Are we finished?

  ;; No, we still have several `cons`es to pick up.


  ;; 171. What do we do next?

  ;; We `cons` the most recent (`first` `lat`) we have --- 'hick --- onto '().


  ;; 172. What do we do next?

  ;; We `cons` 'and onto '(hick).


  ;; 173. What do we do next?

  ;; We `cons` 'tea onto '(and hick).


  ;; 174. What do we do next?

  ;; We `cons` 'coffee onto '(tea and hick).


  ;; 175. Are we finished now?

  ;; Yes.


  ;; 176. Now write the function `multiinsertr`.

  (defn multiinsertr-1 [new old lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) old)
              (cons old
                    (cons new
                          (multiinsertr-1 new old (rest lat))))
              :else (cons (first lat)
                          (multiinsertr-1 new old (rest lat))))))


  ;; 176. Is this function defined correctly?

  (defn multiinsertl-1 [new old lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) old)
              (cons new
                    (cons old
                          (multiinsertl-1 new old lat)))
              :else (cons (first lat)
                          (multiinsertl-1 new old (rest lat))))))

  ;; Not quite. To find out why, go through

  (let [new 'fried
        old 'fish
        lat '(chips and fish or fish and fried)]
    (multiinsertl-1 new old lat))

  ;; (throws StackOverflowError)


  ;; 177. Was the terminal condition ever reached?

  ;; No, because we never get past the first occurrence of `old`.


  ;; 178. Now, try to write the function `multiinsertl` again:

  (defn multiinsertl-2 [new old lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) old)
              (cons new
                    (cons old
                          (multiinsertl-2 new old (rest lat))))
              :else (cons (first lat)
                          (multiinsertl-2 new old (rest lat))))))

  (let [new 'fried
        old 'fish
        lat '(chips and fish or fish and fried)]
    (multiinsertl-2 new old lat))


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Fourth Commandment (preliminary)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Always change at least one argument while recurring. It must be changed to
  ;; be closer to termination. The changing argument must be tested in the
  ;; termination condition: when using `rest`, test termination with `empty?`.


  ;; 179. Now write the function `multisubst`.

  (defn multisubst-1 [new old lat]
    (cond
      (empty? lat) lat
      :else (cond
              (= (first lat) old) (cons new (multisubst-1 new old (rest lat)))
              :else (cons (first lat) (multisubst-1 new old (rest lat))))))


  ;; (But where are my treats at the end of the chapter?)

)
