(ns hello-lisp.little-schemer.chapter-2
  (:require
   [hello-lisp.functions :refer [atom? lat? member?]]))


;; Source: The Little Schemer, 4th edition

;; see also https://github.com/quux00/little-schemer


;; # 2. Do It, Do It Again, and Again, and Again...

(comment

  ;; 1. true of false:

  (let [l '(Jack Sprat could eat no chicken fat)]
    (lat? l))

  ;; true, because each S-expression in `l` is an atom.


  ;; 2. true or false:

  (let [l '((Jack) Sprat could eat no chicken fat)]
    (lat? l))

  ;; false, since (`first` `l`) is a list.


  ;; 3. true or false:

  (let [l '(Jack (Sprat could) eat no chicken fat)]
    (lat? l))

  ;; false, since one of the S-expressions in `l` is a list.


  ;; 4. true or false:

  (let [l '()]
    (lat? l))

  ;; true, because `l` does not contain a list.


  ;; 5. true or false: a lat is a list of atoms.

  ;; true! Every lat is a list of atoms!


  ;; 6. Write the function `lat?` using some, but not necessarily all, of the
  ;; following functions: `first`, `rest`, `cons`, `empty?`, `atom?`, and `=`.

  ;; You were not expected to be able to do this yet, because you are still
  ;; missing some ingredients. Go on to the next question. Good luck.

  ;; implemented at the start of the chapter.


  ;; 7. What is the value of

  (let [l '(bacon and eggs)]
    (lat? l))

  ;; true. The application (`lat?` `l`), where `l` is '(bacon and eggs), has the
  ;; value true, because `l` is a lat.


  ;; 8. How do we determine the answer true for the application (`lat?` `l`)?

  ;; You wer not expected to know this one either. The answer is determined by
  ;; answering the questions asked by `lat?`. Hint: Write down the definition of
  ;; the function `lat?` and refer to it for the next group of questions.


  ;; 9. What is the first question asked by (`lat?` `l`)?

  ;; (`empty?` `l`).


  ;; 10. What is the meaning of the `cond`-line "(`empty?` `l`)" where `l` is
  ;; '(bacon and eggs)?

  ;; (`empty?` `l`) asks if the argument `l` is the empty list. If it is, the
  ;; value of the application is true. If it is not, we ask the next
  ;; question. In this case, `l` is not the empty list, so we ask the next
  ;; question.


  ;; 11. What is the next question?

  ;; (`atom?` (`first` `l`)).


  ;; 12. What is the meaning of the line (`atom?` (`first` `l`)) (lat? (`rest`
  ;; `l`)), where `l` is '(bacon and eggs)?

  ;; (`atom?` (`first` `l`)) asks if the first S-expression of the list `l` is
  ;; an atom. If it is, we want to know if the rest of `l` is also composed only
  ;; of atoms. If it is not, we ask the next question. In this case, (`first`
  ;; `l`) is an atom, so the value of the function is the value
  ;; of (`lat?` (`rest` `l`)).


  ;; 13. What is the meaning of "(`lat?` (`rest` `l`))"?

  ;; It finds out if the rest of the list `l` is composed only of atoms, by
  ;; referring to the function with a new argument.


  ;; 14. Now what is the argument `l` for `lat?`?

  ;; Now the argument `l` is (`rest` `l`), which is '(and eggs).


  ;; 15. What is the next question?

  ;; (`empty?` `l`).


  ;; 16. What is the meaning of the line "(`empty?` `l`) true" where `l` is now
  ;; '(and eggs)?

  ;; (`empty?` `l`) asks if the argument `l` is the empty list. If it is, the
  ;; value of the application is true. If it is not, we ask the next
  ;; question. In this case, `l` is not the empty list, so we ask the next
  ;; question.


  ;; 17. What is the next question?

  ;; (`atom?` (`first` `l`)).


  ;; 18. What is the meaning of the line "(`atom?` (`first`
  ;; `l`)) (`lat?` (`rest` `l`))", where `l` is '(and eggs)?

  ;; (`atom?` (`first` `l`)) asks if (`first` `l`) is an atom. If it is, the
  ;; value of the application is (`lat?` (`rest` `l`)). If not, we ask the next
  ;; question. In this case, (`first` `l`) is an atom, so we want to find out if
  ;; the rest of the list `l` is composed only of atoms.


  ;; 19. What is the meaning of (`lat?` (`rest` `l`))?

  ;; It finds out if the rest of `l` is composed only of atoms, by referring
  ;; again to the function `lat?`, but this time, with the argument (`rest`
  ;; `l`), which is '(eggs).


  ;; 20. What is the next question?

  ;; (`empty?` `l`).


  ;; 21. What is the meaning of the line "(`empty?` `l`) true" where `l` is now
  ;; '(eggs)?

  ;; (`empty?` `l`) asks if the argument `l` is the empty list. If it is, the
  ;; value of the application is true. If it is not, move to the next
  ;; question. In this case, `l` is not empty, so we ask the next question.


  ;; 22. What is the next question?

  ;; (`atom?` (`first` `l`)).


  ;; 23. What is the meaning of the line "(`atom?` (`first`
  ;; `l`)) (`lat?` (`rest` `l`))" where `l` is now '(eggs)?

  ;; (`atom?` (`first` `l`)) asks if (`first` `l`) is an atom. If it is, the
  ;; value of the application is (`lat?` (`rest` `l`)). If it is not, ask the
  ;; next question. In this case, (`rest` `l`) is an atom, so once again we look
  ;; at (`lat?` (`rest` `l`)).


  ;; 24. What is the meaning of (`lat?` (`rest` `l`))?

  ;; It finds out if the rest of the list `l` is composed only of atoms, by
  ;; referring to the function `lat?`, with `l` becoming the value of (`rest`
  ;; `l`).


  ;; 25. Now, what is the argument for `lat?`

  ;; '().


  ;; 26. What is the meaning of the line "(`empty?` `l`) true" where `l` is now
  ;; '()?

  ;; It asks if the argument `l` is the empty list. If it is, the value of the
  ;; application is the value of true. If it is not, we ask the next
  ;; question. In this case, '() is the empty list. So, the value of the
  ;; application (`lat?` `l`) where `l` is '(bacon and eggs), is true.


  ;; 27. Do you remember the question about (`lat?` `l`)?

  ;; Probably not. The application (`lat?` `l`) has the value true if the list
  ;; `l` is a list of atoms where `l` is '(bacon eggs).


  ;; 28. Can you describe what the function `lat?` does in your own words?

  ;; Here are our words: "`lat?` looks at each S-expression in a list, in turn,
  ;; and asks if each S-expression is an atom, until it runs out of
  ;; S-expressions. If it runs out without encountering a list, the value is
  ;; true. If it finds a list, the value is false." To see how we could arrive
  ;; at a value of false, consider the next few questions.


  ;; 29. What is the value of (`lat?` `l`) where `l` is now '(bacon (and eggs))?

  ;; false, since the list `l` contains an S-expression that is a list.


  ;; 30. What is the first question?

  ;; (`empty?` `l`).


  ;; 31. What is the meaning of the line "(`empty?` `l`) true" where `l` is
  ;; '(bacon and eggs)?

  ;; It asks if `l` is the empty list. If it is, the value is true. If l is not
  ;; empty, move to the next question. In this case, it is not empty, so we ask
  ;; the next question.


  ;; 32. What is the next question?

  ;; (`atom?` (`first` `l`)).


  ;; 33. What is the meaning of the line "(`atom?` (`first`
  ;; `l`)) (`lat?` (`rest` `l`))" where `l` is '(bacon (and eggs))?

  ;; It asks if (`first` `l`) is an atom. If it is, the value is (`lat?` (`rest`
  ;; `l`)). If it is not, we ask the next question. In this case, (`first` `l`)
  ;; is an atom, so we want to check if the rest of the list `l` is composed
  ;; only of atoms.


  ;; 34. What is the meaning of (`lat?` (`rest` `l`))?

  ;; It checks to see if the rest of the list `l` is composed only of atoms, by
  ;; referring to `lat?` with `l` replaced by (`rest` `l`).


  ;; 35. What is the meaning of the line "(`empty?` `l`) true" where `l` is now
  ;; '((and eggs))?


  ;; 36. It asks if `l` is the empty list. If it is, the value is true. If it is
  ;; not, we ask the next question. In this case, `l` is not empty, so move to
  ;; tne next question.


  ;; 37. What is the next question?

  ;; (`atom?` (`first` `l`)).


  ;; 38. What is the meaning of the line "(`atom?` (`first`
  ;; `l`)) (`lat?` (`rest` `l`))" where `l` is now '((and eggs))?

  ;; It asks if (`first` `l`) is an atom. If it is, the value is (`lat?` (`rest`
  ;; `l`)). If it is not, we move to the next question. In this case, (`first`
  ;; `l`) is not an atom, so we ask the next question.


  ;; 39. What is the next question?

  ;; `:else`.


  ;; 40. What is the meaning of the question `:else`?

  ;; It asks if `:else` is true.


  ;; 41. Is `:else` true?

  ;; Yes, because the question `:else` is always true!


  ;; 42. `:else`

  ;; Of course.


  ;; 43. Why is `:else` the last question?

  ;; Because we do not need to ask any more questions.


  ;; 44. Why do we not need to ask any more questions?

  ;; Because a list can be empty, can have an atom in the first position, or can
  ;; have a list in the first position.


  ;; 45. What is the meaning of the line "`:else` false"?

  ;; `:else` asks if `:else` is true. If `:else` is true --- as it always is ---
  ;; then the answer is false.


  ;; 46. What is ")))"?

  ;; These are closing or matching parentheses of forms (e.g., `cond` or
  ;; `defn`), which appear at the beginning of a function definition.


  ;; 47. Can you describe how we determined the value false for

  (let [l '(bacon (and eggs))]
    (lat? l))

  ;; Here is one way to say it: "(`lat?` `l`) looks at each item in its argument
  ;; to see if it is an atom. If it runs out of items before it finds a list,
  ;; the value of (`lat?` `l`) is true. If it finds a list, as it did in the
  ;; example '(bacon (and eggs)), the value of (`lat?` `l`) is false."


  ;; 48. Is true or false

  (let [l1 '()
        l2 '(d e f g)]
    (or (empty? l1) (atom? l2)))

  ;; true, because (`empty?` `l1`) is true where `l1` is '().


  ;; 49. Is true or false

  (let [l1 '(a b c)
        l2 '()]
    (or (empty? l1)
        (empty? l2)))

  ;; true, because (`empty?` `l2`) is true where `l2` is '().


  ;; 50. Is true or false

  (let [l1 '(a b c)
        l2 '(atom)]
    (or (empty? l1)
        (empty? l2)))

  ;; false, because neither (`empty?` `l1`) nor (`empty?` `l2`) is true here.


  ;; 51. What does `or` do?

  ;; It asks two (actually more) questions, one at a time. If the first one is
  ;; true it stops and answers true. Otherwise it asks the second question and
  ;; answers with whatever the second question answers (and so on until it runs
  ;; out of questions and returns false).


  ;; 52. Is it true or false that `a` is a member of `lat` wher `a` is 'tea and
  ;; `lat` is '(coffee tea or milk)?

  ;; true, because one of the atoms of the `lat` is the same as the atom `a` ---
  ;; 'tea.


  ;; 53. Is (`member?` `a` `lat`) true or false where `a` is 'poached and `lat`
  ;; is '(fried eggs and scrambled eggs)?

  ;; false, since `a` is not one of the atoms of the `lat`.


  ;; 54. Function `member?` was defined at the beginning of the chapter. What is
  ;; the value of

  (let [a 'meat
        lat '(mashed potatoes and meat gravy)]
    (member? a lat))

  ;; true, because the atom 'meat is one of the atoms of `lat`.


  ;; 55. How do we determine the value true for the above application?

  ;; The value is determined by asking the questions about (`member?` `a`
  ;; `lat`). Hint: Write down the definition of the function `member?` and refer
  ;; to it while you work on the next group of questions.


  ;; 56. What is the first question asked by (`member?` `a` `lat`)?

  ;; (`empty?` `lat`). This is also the first question asked by `lat?`.


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The First Commandment (preliminary)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Always ask `empty?` as the first question in expressing any function.


  ;; 57. What is the meaninf of the line "(`empty?` `lat`) false" where `lat` is
  ;; '(mashed potatoes and meat gravy)?

  ;; (`empty?` `lat`) asks if `lat` is the empty list. If it is, the value is
  ;; false, since the atom 'meat was not found in `lat`. If not, we ask the next
  ;; question. In this case, it is not empty, so we ask the next question.


  ;; 58. What is the next question?

  ;; `:else`.


  ;; 59. Why is `:else` the next question?

  ;; Because we do not need to ask any more questions.


  ;; 60. Is `:else` really a question?

  ;; Yes, `:else` is a question whose value is always true.


  ;; 61. What is the meaning of the line "`:else` (`or` (`=` (`first` `lat`)
  ;; `a`) (`member?` `a` (`rest` `lat`)))"?

  ;; Now that we know that `lat` is not empty, we have to find out whether the
  ;; `first` of `lat` is the same atom as `a`, or whether `a` is somewhere in
  ;; the rest of `lat`. The `or` form does this.


  ;; 62. true or false:

  (let [a 'meat
        lat '(mashed potatoes and meat gravy)]
    (or (= (first lat) a)
        (member? a (rest lat))))

  ;; we will find out by looking at each question in turn.


  ;; 63. Is true or false

  (let [a 'meat
        lat '(mashed potatoes and meat gravy)]
    (= (first lat) a))

  ;; false, because 'meat is not `=` to 'mashed.


  ;; 64. What is the second question of `or` form?

  ;; (`member?` `a` (`rest` `lat`)). This refers to the function with the
  ;; argument `lat` replaced by (`rest` `lat`).


  ;; 65. Now what are the arguments of `member?`?

  ;; `a` is 'meat, and `lat` is now (`rest` `lat`), specifically '(potatoes and
  ;; meat gravy).


  ;; 66. What is the next question?

  ;; (`empty?` `lat`). Remember The First Commandment.


  ;; 67. Is true or false

  (let [lat '(potatoes and meat gravy)]
    (empty? lat))

  ;; false.


  ;; 68. What do we do now?

  ;; Ask the next question.


  ;; 69. What is the next question?

  ;; `:else`.


  ;; 70. What is the meaning of (`or` (`=` (`first` `lat`) `a`) (`member?`
  ;; `a` (`rest` `lat`)))?

  ;; It finds out if `a` is `=` to the `first` of `lat` or if `a` is a member of
  ;; the `rest` of `lat` by referring to the function.


  ;; 71. Is `a` `=` to the `first` of `lat`?

  ;; No, because `a` is 'meat and the `first` of `lat` is 'potatoes.


  ;; 72. So what do we do next?

  ;; We ask (`member?` `a` (`rest` `lat`)).


  ;; 73. Now, what are the arguments of `member?`

  ;; `a` is 'meat, and `lat` is '(and meat gravy).


  ;; 74. What is the next question?

  ;; (`empty?` `lat`).


  ;; 75. What do we do now?

  ;; Ask the next question, since (`empty?` `lat`) is false.


  ;; 76. What is the next question?

  ;; `:else`.


  ;; 77. What is the value of (`or` (`=` (`first` `lat`) `a`) (`member?`
  ;; `a` (`rest` `lat`)))?

  ;; The value of (`member?` `a` (`rest` `lat`)).


  ;; 78. Why?

  ;; Because (`=` (`first` `lat`) `a`) is false.


  ;; 79. What do we do now?

  ;; Recur --- refer to the function with new arguments.


  ;; 80. What are the new arguments?

  ;; `a` is 'meat, and `lat` is '(meat gravy).


  ;; 81. What is the next question?

  ;; (`empty?` `lat`).


  ;; 82. What do we do now?

  ;; Since (`empty?` `lat`) is false, ask the next question.


  ;; 83. What is the next question?

  ;; `:else`.


  ;; 84. What is the value of (`or` (`=` (`first` `lat`) `a`) (`member?`
  ;; `a` (`rest` `lat`)))?

  ;; true, because (`first` `lat`), which is 'meat, and `a`, which is 'meat, are
  ;; the same atom. Therefore, `or` answers with true.


  ;; 85. What is the value of the application

  (let [a 'meat
        lat '(meat gravy)]
    (member? a lat))

  ;; true, because we have found that 'meat is a member of '(meat gravy).


  ;; 86. What is the value of the application

  (let [a 'meat
        lat '(and meat gravy)]
    (member? a lat))

  ;; true, because 'meat is also a member of the `lat` '(and meat gravy).


  ;; 87. What is the value of the application

  (let [a 'meat
        lat '(potatoes and meat gravy)]
    (member? a lat))

  ;; true, because 'meat is also a member of the `lat` '(potatoes and meat
  ;; gravy).


  ;; 88. What is the value of the application

  (let [a 'meat
        lat '(mashed potatoes and meat gravy)]
    (member? a lat))

  ;; true, because 'meat is also a member of the `lat` '(mashed potatoes and
  ;; meat gravy). Of course, this is our original `lat`.


  ;; 89. Just to make sure you have it right, let's quickly run through it
  ;; again. What is the value of

  (let [a 'meat
        lat '(mashed potatoes and meat gravy)]
    (member? a lat))

  ;; true. Hint: Write down the definition of the function `member?` and its
  ;; arguments and refer to them as you go through the next group of questions.


  ;; 90. (`empty?` `lat`)?

  ;; No. Move to the next line.


  ;; 91. (`or` (`=` (`first` `lat`) `a`) (`member?` `a` (`rest` `lat`)))?

  ;; Perhaps.


  ;; 92. (`=` (`first` `lat`) `a`)?

  ;; No. Ask the next question.


  ;; 93. What next?

  ;; Recur with `a` and (`rest` `lat`) where `a` is 'meat and (`rest` `lat`) is
  ;; '(potatoes and meat gravy).


  ;; 94. (`empty?` `lat`)?

  ;; No. Move to the next line.


  ;; 95. `:else`?

  ;; Yes, but (`=` (`first` `lat`) `a`) is false. Recur with `a` and (`rest`
  ;; `lat`) where `a` is 'meat and (`rest` `lat`) is '(and meat gravy).


  ;; 96. (`empty?` `lat`)?

  ;; No. Move to the next line.


  ;; 97. `:else`?

  ;; Yes, but (`=` (`first` `lat`) `a`) is false. Recur with `a` and (`rest`
  ;; `lat`) where `a` is 'meat and (`rest` `lat`) is '(meat gravy).


  ;; 98. (`empty?` `lat`)?

  ;; No. Move to the next line.


  ;; 99. (`=` (`first` `lat`) `a`)?

  ;; Yes, the value is true.


  ;; 100. (`or` (`=` (`first` `lat`) `a`) (`member?` `a` (`rest` `lat`)))?

  ;; true.


  ;; 101. What is the value of

  (let [a 'meat
        lat '(meat gravy)]
    (member? a lat))

  ;; true.


  ;; 102. What is the value of

  (let [a 'meat
        lat '(and meat gravy)]
    (member? a lat))

  ;; true.


  ;; 103. What is the value of

  (let [a 'meat
        lat '(potatoes and meat gravy)]
    (member? a lat))

  ;; true.


  ;; 104. What is the value of

  (let [a 'meat
        lat '(mashed potatoes and meat gravy)]
    (member? a lat))

  ;; true.


  ;; 105. What is the value of

  (let [a 'liver
        lat '(bagels and lox)]
    (member? a lat))

  ;; false.


  ;; 106. Let's work out why it is false. What's the first question `member?`
  ;; asks?

  ;; (`empty?` `lat`).


  ;; 107. (`empty?` `lat`)?

  ;; No. Move to the next line.


  ;; 108. `:else`?

  ;; Yes, but (`=` (`first` `lat`)) is false. Recur with `a` and (`rest` `lat`)
  ;; where `a` is 'liver and (`rest` `lat`) is '(and lox).


  ;; 109. (`empty?` `lat`)?

  ;; No. Move to the next line.


  ;; 110. `:else`?

  ;; Yes, but (`=` (`first` `lat`) `a`) is false. Recur with `a` and (`rest`
  ;; `lat`) where `a` is 'liver and (`rest` `lat`) is '(lox).


  ;; 111. (`empty?` `lat`)?

  ;; No. Move to the next line.


  ;; 112. `:else`?

  ;; Yes, but (`=` (`first` `lat`) `a`) is still false. Recur with `a`
  ;; and (`rest` `lat`) where `a` is 'liver and (`rest` `lat`) is '().


  ;; 113. (`empty?` `lat`)?

  ;; Yes.


  ;; 114. What is the value of

  (let [a 'liver
        lat '()]
    (member? a lat))

  ;; false.


  ;; 115. What is the value of

  (let [a 'liver
        lat '(lox)]
    (or (= (first lat) a)
        (member? a lat)))

  ;; false.


  ;; 116. What is the value of

  (let [a 'liver
        lat '(lox)]
    (member? a lat))

  ;; false.


  ;; 117. What is the value of

  (let [a 'liver
        lat '(and lox)]
    (or (= (first lat) a)
        (member? a (rest lat))))

  ;; false.


  ;; 118. What is the value of

  (let [a 'liver
        lat '(and lox)]
    (member? a lat))

  ;; false.


  ;; 119. What is the valuee of

  (let [a 'liver
        lat '(bagels and lox)]
    (or (= (first lat) a)
        (member? a (rest lat))))

  ;; false.


  ;; 120. What is the value of

  (let [a 'liver
        lat '(bagels and lox)]
    (member? a lat))

  ;; false.


  ;; Do you believe all of this? Then you may rest!

)
