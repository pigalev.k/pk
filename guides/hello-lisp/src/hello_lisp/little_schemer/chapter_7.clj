(ns hello-lisp.little-schemer.chapter-7
  (:require
   [hello-lisp.functions :refer [atom? member? firsts seconds multirember
                                 a-pair? make-set is-set? subset? eqset?
                                 intersect? intersect union
                                 *build *first *second fun? fullfun? revrel]]))


;; Source: The Little Schemer, 4th edition

;; see also https://github.com/quux00/little-schemer


;; # 7. Friends and Relations

(comment

  ;; 1. Is this a set?

  '(apple peaches apple plum)

  ;; No, since 'apple appears more than once.


  ;; 2. true or false:

  (let [lat '(apples peaches pears plums)]
    (is-set? lat))

  ;; true, because no atom appears more than once.


  ;; 3. How about

  (is-set? '())

  ;; true, because no atom appears more than once.


  ;; 4. Try to write `is-set?`.

  (defn is-set?-1 [lat]
    (cond
      (empty? lat) true
      :else        (cond
                     (member? (first lat) (rest lat)) false
                     :else                            (is-set?-1 (rest lat)))))


  ;; 5. Simplify `is-set?`.

  (defn is-set?-2 [lat]
    (cond
      (empty? lat)                     true
      (member? (first lat) (rest lat)) false
      :else                            (is-set?-2 (rest lat))))


  ;; 6. Does this work for the example

  (is-set?-2 '(apple 3 pear 4 apple 3 4))

  ;; Yes, since `member?` uses an equality operator that can compare any
  ;; atoms.


  ;; 7. Were you surprised to see the function `member?` appear in the
  ;; definition of `is-set?`

  ;; You should not be, because we have written `member?` already, and now we
  ;; can use it whenever we want.


  ;; 8. What is

  (let [lat '(apple peach pear peach plum apple lemon peach)]
    (make-set lat))

  ;; '(apple peach pear plum lemon).


  ;; 9. Try to write `make-set` using `member?`.

  (defn make-set-1 [lat]
    (cond
      (empty? lat)                     lat
      (member? (first lat) (rest lat)) (make-set-1 (rest lat))
      :else                            (cons (first lat)
                                             (make-set-1 (rest lat)))))


  ;; 10. Are you surprised to see how short this is?

  ;; We hope so. But don't be afraid: it's right.


  ;; 11. Using the previous definition, what is the result of

  (let [lat '(apple peach pear peach plum apple lemon peach)]
    (make-set-1 lat))

  ;; '(pear plum apple lemon peach).


  ;; 12. Try to write `make-set` using `multirember`.

  (defn make-set-2 [lat]
    (cond
      (empty? lat) lat
      :else        (cons (first lat)
                         (make-set-2 (multirember (first lat) (rest lat))))))


  ;; 13. What is the result of

  (let [lat '(apple peach pear peach plum apple lemon peach)]
    (make-set-2 lat))

  ;; '(apple peach pear plum lemon).


  ;; 14. Describe in your own words how the second definition of `make-set`
  ;; works.

  ;; Here are our words: "The function `make-set` remembers to `cons` the first
  ;; atom in the lat onto the result of the natural recursion, after removing
  ;; all occurrences of the first atom from the rest of the lat."


  ;; 15. Does the second `make-set` work for the example

  (make-set-2 '(apple 3 pear 4 9 apple 3 4))

  ;; Yes, since `multirember` is written using `=`, that can compare any
  ;; atoms (in Clojure).


  ;; 16. What is

  (let [set1 '(5 chicken wings)
        set2 '(5 hamburgers
                 2 pieces fried chicken and
                 light duckling wings)]
    (subset? set1 set2))

  ;; true, because each atom in `set1` is also in `set2`.


  ;; 17. What is

  (let [set1 '(4 pounds of horseradish)
        set2 '(four pounds chicken and
                    5 ounces horseradish)]
    (subset? set1 set2))

  ;; false.


  ;; 18. Write `subset?`.

  (defn subset?-1 [set1 set2]
    (cond
      (empty? set1) true
      :else         (cond
                      (member? (first set1) set2) (subset?-1 (rest set1) set2)
                      :else                       false)))


  ;; 19. Can you write a shorter version of `subset?`

  (defn subset?-2 [set1 set2]
    (cond
      (empty? set1)               true
      (member? (first set1) set2) (subset?-2 (rest set1) set2)))


  ;; 20. Try to write `subset?` with `and`.

  (defn subset?-3 [set1 set2]
    (cond
      (empty? set1) true
      :else         (and (member? (first set1) set2)
                         (subset?-3 (rest set1) set2))))



  ;; 21. What is

  (let [set1 '(6 large chickens with wings)
        set2 '(6 chickens with large wings)]
    (eqset? set1 set2))

  ;; true.


  ;; 22. Write `eqset?`.

  (defn eqset?-1 [set1 set2]
    (cond
      (subset? set1 set2) (subset? set2 set1)
      :else               false))


  ;; 23. Can you write `eqset?` with only one `cond`-line?

  (defn eqset?-2 [set1 set2]
    (cond
      :else (and (subset? set1 set2)
                 (subset? set2 set1))))


  ;; 24. Write one-liner.

  (defn eqset?-3 [set1 set2]
    (and (subset? set1 set2)
         (subset? set2 set1)))


  ;; 25. What is

  (let [set1 '(stewed tomatoes and macaroni)
        set2 '(macaroni and cheese)]
    (intersect? set1 set2))

  ;; true, because at least one atom in `set1` is in `set2`.


  ;; 26. Define the function `intersect?`.

  (defn intersect?-1 [set1 set2]
    (cond
      (empty? set1) false
      :else         (cond
                      (member? (first set1) set2) true
                      :else                       (intersect?-1 (rest set1)
                                                                set2))))


  ;; 27. Write the shorter version.

  (defn intersect?-2 [set1 set2]
    (cond
      (empty? set1)               false
      (member? (first set1) set2) true
      :else                       (intersect?-2 (rest set1) set2)))


  ;; 28. Try writing `intersect?` with `or`.

  (defn intersect?-3 [set1 set2]
    (cond
      (empty? set1) false
      :else         (or (member? (first set1) set2)
                        (intersect?-3 (rest set1) set2))))

  ;; Compare `subset?` and `intersect?`.


  ;; 29. What is

  (let [set1 '(stewed tomatoes and macaroni)
        set2 '(macaroni and cheese)]
    (intersect set1 set2))

  ;; '(and macaroni).


  ;; 30. Now you can write the short version of `intersect`.

  (defn intersect-1 [set1 set2]
    (cond
      (empty? set1)               '()
      (member? (first set1) set2) (cons (first set1)
                                        (intersect-1 (rest set1) set2))
      :else                       (intersect-1 (rest set1) set2)))


  ;; 31. What is

  (let [set1 '(stewed tomatoes and macaroni casserole)
        set2 '(macaroni and cheese)]
    (union set1 set2))

  ;; '(stewed tomatoes casserole macaroni and cheese).


  ;; 32. Write `union`.

  (defn union-1 [set1 set2]
    (cond
      (empty? set1)               set2
      (member? (first set1) set2) (union-1 (rest set1) set2)
      :else                       (cons (first set1)
                                        (union-1 (rest set1) set2))))


  ;; 33. What is this function?

  (defn xxx [set1 set2]
    (cond
      (empty? set1)               '()
      (member? (first set1) set2) (xxx (rest set1) set2)
      :else                       (cons (first set1)
                                        (xxx (rest set1) set2))))

  ;; In our words: "It is a function that returns all the atoms in `set1` that
  ;; are not in `set2`." That is, `xxx` is the (set) difference function.


  ;; 34. What is

  (let [l-set '((a b c) (c a d e) (e f g h a b))]
    (intersect-all l-set))

  ;; '(a).


  ;; 35. What is

  (let [l-set '((6 pears and)
                (3 peaches and 6 peppers)
                (8 pears and 6 plums)
                (and 6 prunes with some apples))]
    (intersect-all l-set))

  ;; '(6 and).


  ;; 36. Now, using whatever help functions you need, write `intersect-all`
  ;; assuming that the list of sets is non-empty.

  (defn intersect-all-1 [l-set]
    (cond
      (empty? (rest l-set)) (first l-set)
      :else                 (intersect (first l-set)
                       (intersect-all-1 (rest l-set)))))


  ;; 37. Is this a pair?

  '(pear pear)

  ;; Yes, because it is a list with only two atoms. (A pair in Scheme or Lisp is
  ;; a different but related object.)


  ;; 38. Is this a pair?

  '(3 7)

  ;; Yes.


  ;; 39. Is this a pair?

  '((2) (pair))

  ;; Yes, because it is a list with only two S-expressions.


  ;; 40.

  (let [l '(full (house))]
    (a-pair? l))

  ;; true, because it is a list with only two S-expressions.


  ;; 41. Define `a-pair?`.

  (defn a-pair?-1 [x]
    (cond
      (atom? x)                false
      (empty? x)               false
      (empty? (rest x))        false
      (empty? (rest (rest x))) true
      :else                    false))


  ;; 42. How can you refer to the first S-expression of a pair?

  ;; By taking the `first` of the pair.


  ;; 43. How can you refer to the second S-expression of a pair?

  ;; By taking the `first` of the `rest` of the pair.


  ;; 44. How can you build a pair with two atoms?

  ;; You `cons` the first one onto the `cons` of the second one onto '(). That
  ;; is,

  (cons x1 (cons x2 '()))


  ;; 45. How can you build a pair with two S-expressions?

  ;; You `cons` the first one onto the `cons` of the second one onto '(). That
  ;; is,

  (cons x1 (cons x2 '()))


  ;; 46. Did you notice the differences between the last two answers?

  ;; No, there aren't any.


  ;; 47.

  (defn *first-1 [p]
    (cond
      :else (first p)))

  (defn *second-1 [p]
    (cond
      :else (first (rest p))))

  (defn *build-1 [s1 s2]
    (cond
      :else (cons s1 (cons s2 '()))))

  ;; What possible uses do these three functions have?

  ;; They are used to make representations of pairs and to get parts of
  ;; representations of pairs. See chapter 6.

  ;; They will be used to improve readability, as you will soon see.

  ;; Redefine `*first`, `*second` and `*build` as one-liners.

  (defn *first-2 [p]
    (first p))

  (defn *second-2 [p]
    (first (rest p)))

  (defn *build-2 [s1 s2]
    (cons s1 (cons s2 '())))


  ;; 48. Can you write `*third` as a one-liner?

  (defn *third [l]
    (first (rest (rest l))))


  ;; 49. Is `l` a rel where `l` is

  '(apples peaches pumpkin pie)

  ;; No, since `l` is not a list of pairs. We use rel to stand for relation.


  ;; 50. Is `l` a rel where `l` is

  '((apples peaches)
    (pumpkin pie)
    (apples peaches))

  ;; No, since `l` is not a set of pairs.


  ;; 51. Is `l` a rel where `l` is

  '((apples peaches)
    (pumpkin pie))

  ;; Yes.


  ;; 52. Is `l` a rel where `l` is

  '((4 3) (4 2) (7 6) (6 2) (3 4))

  ;; Yes.


  ;; 53. Is `rel` a fun where `rel` is

  '((4 3) (4 2) (7 6) (6 2) (3 4))

  ;; No. We use fun to stand for function.


  ;; 54. What is

  (let [rel '((8 3) (4 2) (7 6) (6 2) (3 4))]
    (fun? rel))

  ;; true, because (`firsts` `rel`) is a set. See chapter 3.


  ;; 55. What is

  (let [rel '((d 4) (b 0) (b 9) (e 5) (g 4))]
    (fun? rel))

  ;; false, because 'b is repeated.


  ;; 56. Write `fun?` with `is-set?` and `firsts`.

  (defn fun?-1 [rel]
    (is-set? (firsts rel)))


  ;; 57. Is `fun?` a simple one-liner?

  ;; It sure is.


  ;; 58. How do we represent a finite function?

  ;; For us, a finite function is a list of pairs in which no first element of
  ;; any pair is the same as any other first element.


  ;; 59. What is

  (let [rel '((8 a) (pumpkin pie) (got sick))]
    (revrel rel))

  ;; '((a 8) (pie pumpkin) (sick got)).


  ;; 60. You can now write `revrel`.

  (defn revrel-1 [rel]
    (cond
      (empty? rel) rel
      :else (cons (*build (*second (first rel))
                          (*first (first rel)))
                  (revrel-1 (rest rel)))))


  ;; 61. Would the following also be correct:

  (defn revrel-2 [rel]
    (cond
      (empty? rel) rel
      :else (cons (cons (first (rest (first rel)))
                        (cons (first (first rel))
                              '()))
                  (revrel-2 (rest rel)))))

  ;; Yes, but now do you see how representation aids readability?


  ;; 62. Suppose we had the function `revpair` that reversed the two components
  ;; of a pair like this:

  (defn revpair-1 [pair]
    (*build (*second pair) (*first pair)))

  ;; How would you rewrite `revrel` to use this help function?


  ;; 63. No problem, and it is even easier to read.

  (defn revrel-3 [rel]
    (cond
      (empty? rel) rel
      :else (cons (revpair-1 (first rel))
                  (revrel-3 (rest rel)))))


  ;; 64. Can you guess why `fun` is not a fullfun where `fun` is

  '((8 3) (4 2) (7 6) (6 2) (3 4))

  ;; `fun` is not a fullfun, since the 2 appears more than once as a second item
  ;; of a pair.


  ;; 65. Why is true the value of

  (let [fun '((8 3) (4 8) (7 6) (6 2) (3 4))]
    (fullfun? fun))

  ;; Because '(3 8 6 2 4) is a set.


  ;; 66. What is

  (let [fun '((grape raisin)
              (plum prune)
              (stewed prune))]
    (fullfun? fun))

  ;; false.


  ;; 67. What is

  (let [fun '((grape raisin)
              (plum prune)
              (stewed grape))]
    (fullfun? fun))

  ;; true, because '(raisin prune grape) is a set.


  ;; 68. Define `fullfun?`.

  (defn fullfun?-1 [fun]
    (is-set? (seconds fun)))


  ;; 69. Can you define `seconds`?

  ;; It is just like `firsts`.


  ;; 70. What is another name for `fullfun?`

  ;; `one-to-one?`.


  ;; 71. Can you think of a second way to write `one-to-one?`

  (defn one-to-one?-1 [fun]
    (fun? (revrel fun)))


  ;; 72. Is '((chocolate chip) (doughy cookie)) a one-to-one function?

  ;; Yes, and you deserve one now!


  ;; Go and get one!

  ;; Or better yet, make your own.

  (defn cookies []
    (bake
     '(350 degrees)
     '(12 minutes)
     (mix
      '(walnuts 1 cup)
      '(chocolate-chips 16 ounces)
      (mix
       (mix
        '(flour 2 cups)
        '(oatmeal 2 cups)
        '(salt .5 teaspoon)
        '(baking-powder 1 teaspoon)
        '(baking-soda 1 teaspoon))
       (mix
        '(eggs 2 large)
        '(vanilla 1 teaspoon)
        (cream
         '(butter 1 cup)
         '(sugar 2 cups)))))))

  ;; (At last!)

)
