(ns hello-lisp.little-schemer.chapter-1
  (:require
   [hello-lisp.functions :refer [atom?]]))


;; Source: The Little Schemer, 4th edition

;; see also https://github.com/quux00/little-schemer


;; # 1. Toys

(comment

  ;; 1. Is it true that this is an atom?

  'atom

  ;; Yes, because 'atom is a string of characters beginning with the letter a.


  ;; 2. Is it true that this is an atom?

  'turkey

  ;; Yes, because 'turkey is a string of characters beginning with a letter.


  ;; 3. Is it true that this is an atom?

  1492

  ;; Yes, because 1492 is a string of digits.


  ;; 4. Is it true that this is an atom?

  'u

  ;; Yes, because 'u is a string of one character, which is a letter.


  ;; 5. Is it true that this is an atom?

  '*abc$

  ;; Yes, because '*abc$ is a string of characters beginning with a letter or
  ;; special character other than a left "(" or right ")" parenthesis.


  ;; 6. Is it true that this is a list?

  '(atom)

  ;; Yes, because '(atom) is an atom enclosed by parentheses.


  ;; 7. Is it true that this is a list?

  '(atom turkey or)

  ;; Yes, because it is a collection of atoms enclosed by parentheses.


  ;; 8. Is it true that this is a list?

  '(atom turkey) 'or

  ;; No, because these are actually two S-expressions not enclosed by
  ;; parentheses. The first one is a list containing two atoms, and the second
  ;; one is an atom.


  ;; 9. Is it true that this is a list?

  '((atom turkey) or)

  ;; Yes, because the two S-expressions are now enclosed by parentheses.


  ;; 10. Is it true that this is an S-expression?

  'xyz

  ;; Yes, because all atoms are S-expressions.


  ;; 11. Is it true that this is an S-expression?

  '(x y z)

  ;; Yes, because it is a list.


  ;; 12. Is it true that this is an S-expression?

  '((x y) z)

  ;; Yes, because all lists are S-expressions.


  ;; 13. Is it true that this is a list?

  '(how are you doing so far)

  ;; Yes, because it is a collection of S-expressions enclosed by parentheses.


  ;; 14. How many S-expressions are in the list

  '(how are you doing so far)

  ;; and what are they?

  ;; Six, 'how, 'are, 'you, 'doing, 'so, and 'far.


  ;; 15. Is it true that this is a list?

  '(((how) are) ((you) (doing so)) far)

  ;; Yes, because it is a collection of S-expressions enclosed by parentheses.


  ;; 16. How many S-expressions are in the previous list and what are they?

  ;; Three, '((how) are), '((you) (doing so)), and 'far.


  ;; 17. Is it true that this is a list?

  '()

  ;; Yes, because it contains zero S-expressions enclosed by parentheses. This
  ;; special S-expression is called the empty list.


  ;; 18. Is it true that this is an atom?

  '()

  ;; No, because '() is just a list.


  ;; 19. Is it true that this is a list?

  '(() () () ())

  ;; Yes, because it is a collection of S-expressions enclosed by parentheses.


  ;; 20. What is the `first` of `l` where `l` is the argument

  '(a b c)

  ;; 'a, because 'a is the first atom of this list.


  ;; 21. What is the `first` of `l` where `l` is

  '((a b c) x y z)

  ;; '(a b c), because it is the first S-expression of this non-empty list.


  ;; 22. What is the `first` of `l` where `l` is 'hotdog?

  ;; No answer. You cannot ask for the `first` of an atom.


  ;; 23. What is the `first` of `l` where `l` is '()?

  ;; No answer (in Clojure nil). You cannot ask for the `first` of the empty
  ;; list.


  ;;;;;;;;;;;;;;;;;;;;;
  ;; The Law of `first`
  ;;;;;;;;;;;;;;;;;;;;;

  ;; The primitive `first` is defined only for non-empty lists.


  ;; 24. What is the `first` of `l` where `l` is

  '(((hotdogs)) (and) (pickle) relish)

  ;; '((hotdogs)), read as: "The list of the list of 'hotdogs." '((hotdogs)) is
  ;; the first S-expression of `l`.


  ;; 25. What is the (`first` `l`) where `l` is

  '(((hotdogs)) (and) (pickle) relish)

  ;; ((hotdoge)), because (`first` `l`) is another way to ask for "the `first`
  ;; of the list `l`."


  ;; 26. What is (`first` (`first` `l`)) where `l` is

  '(((hotdogs)) (and))

  ;; (hotdogs).


  ;; 27. What is the `rest` of `l` where `l` is

  '(a b c)

  ;; '(b c), because '(b c) is the list `l` without (`first` `l`).


  ;; 28. What is the `rest` of `l` where `l` is

  '((a b c) x y z)

  ;; '(x y z).


  ;; 29. What is the `rest` of `l` where `l` is

  '(hamburger)

  ;; '().


  ;; 30. What is the (`rest` `l`) where `l` is

  '((x) t r)

  ;; '(t r), because (`rest` `l`) is just another way to ask for "the `rest` of
  ;; the list `l`."


  ;; 31. What is (`rest` `a`) where `a` is

  'hotdogs

  ;; No answer. You cannot ask for the `rest` of an atom.


  ;; 32. What is (`rest` `l`) where `l` is

  '()

  ;; No answer (nil in Clojure). You cannot ask for the `rest` of the empty list.


  ;;;;;;;;;;;;;;;;;;;;
  ;; The Law of `rest`
  ;;;;;;;;;;;;;;;;;;;;

  ;; The primitive `rest` is defined only for non-empty lists. The `rest` of any
  ;; non-empty list is always another list.


  ;; 33. What is (`first` (`rest` `l`)) where `l` is

  '((b) (x y) ((c)))

  ;; '(x y), because '((x y) ((c))) is (`rest` `l`), and '(x y) is the `first`
  ;; of (`rest` `l`).


  ;; 34. What is the (`rest` (`rest` `l`)) where `l` is

  '((b) (x y) ((c)))

  ;; (((c))), because '((x y) ((c))) is (`rest` `l`), and '(((c))) is the `rest`
  ;; of (`rest` `l`).


  ;; 35. What is (`rest` (`first` `l`)) where `l` is

  '(a (b (c)) d)

  ;; No answer, since (`first` `l`) is an atom, and `rest` does not take an atom
  ;; as an argument; see The Law of `rest`.


  ;; 36. What does `first` take as an argument?

  ;; It takes any non-empty list.


  ;; 37. What does `rest` take as an argument?

  ;; It takes any non-empty list.


  ;; 38. What is the `cons` of the atom `a` and the list `l` where `a` is
  ;; 'peanut and `l` is '(butter and jelly)? This can also be written "(`cons`
  ;; `a` `l`)". Read: "`cons` the atom `a` onto the list `l`."

  ;; '(peanut butter and jelly), because `cons` adds an atom to the front of a
  ;; list.


  ;; 39. What is the `cons` of `s` and `l` where `s` is

  '(banana and)

  ;; and `l` is

  '(peanut butter jelly)

  ;; '((banana and) peanut butter jelly), because `cons` adds any S-expression
  ;; to the front of a list.


  ;; 40. What is (`cons` `s` `l`) where `s` is

  '((help) this)

  ;; and `l` is

  '(is very ((hard) to learn))

  (cons '((help) this) '(is very ((hard) to learn)))

  ;; (((help) this) is very ((hard) to learn)).


  ;; 41. What does `cons` take as its arguments?

  ;; `cons` takes two arguments: the first one is any S-expression; the second
  ;; one is any list.


  ;; 42. What is

  (cons '(a b (c)) '())

  ;; ((a b (c))), because '() is a list.


  ;; 43. What is

  (cons 'a '())

  ;; (a).


  ;; 44. What is

  (cons '((a b c)) 'b)

  ;; No answer, since the second argument must be a list.


  ;; 45. What is

  (cons 'a 'b)

  ;; No answer. Why?


  ;;;;;;;;;;;;;;;;;;;;
  ;; The Law of `cons`
  ;;;;;;;;;;;;;;;;;;;;

  ;; The primitive `cons` takes two arguments. The second argument to `cons`
  ;; must be a list. The result is a list.


  ;; 46. What is

  (cons 'a (first '((b) c d)))

  ;; (a b). Why?


  ;; 47. What is

  (cons 'a (rest '((b) c d)))

  ;; (a c d). Why?


  ;; 48. Is it true that the list `l` is the empty list where `l` is '()?

  ;; Yes, because it is the list composed of zero S-expressions. This question
  ;; can also be written: (`empty?` `l`).


  ;; 49. What is

  (empty? '())

  ;; true, because '() is a notation for the empty list.


  ;; 50. Is

  (empty? '(a b c))

  ;; true or false?

  ;; false, because '(a b c) is a non-empty list.


  ;; 51. Is

  (empty? 'spaghetti)

  ;; true or false?

  ;; No answer, because you cannot ask `empty?` of an atom.


  ;;;;;;;;;;;;;;;;;;;;;;
  ;; The Law of `empty?`
  ;;;;;;;;;;;;;;;;;;;;;;

  ;; The primitive `empty?` is defined only for lists (in Clojure, also for
  ;; other collections, sequences and strings).


  ;; 52. Is it true of false that `s` is an atom where `s` is 'Harry?

  ;; True, because 'Harry is a string of characters beginning with a letter.


  ;; 53. Is

  (atom? 'Harry)

  ;; true or false?

  ;; true, because (`atom?` 'Harry) is just another way to ask "Is 'Harry an
  ;; atom?"


  ;; 54. Is

  (atom? '(Harry had a heap of apples))

  ;; true or false?

  ;; false, since it is a list.


  ;; 55. How many arguments does `atom?` take and what are they?

  ;; It takes one argument. The argument can be any S-expression.


  ;; 56. Is

  (atom? (first '(Harry had a heap of apples)))

  ;; true or false?

  ;; true, because `first` of the list is 'Harry, and 'Harry is an atom.


  ;; 57. Is

  (atom? (rest '(Harry had a heap of apples)))

  ;; true or false?

  ;; false.


  ;; 58. Is

  (atom? (rest '(Harry)))

  ;; true or false?

  ;; false, because the list '() is not an atom.


  ;; 59. Is

  (atom? (first (rest '(swing low sweet cherry oat))))

  ;; true or false?

  ;; true, because the `rest` of the list is '(low sweet cherry oat), and
  ;; `first` of the `rest` is 'low, which is an atom.


  ;; 60. Is

  (atom? (first (rest '(swing (low sweet) cherry oat))))

  ;; true or false?

  ;; false, since the `rest` of the list is '((low sweet) cherry oat), and
  ;; `first` of the `rest` is '(low sweet), which is a list.


  ;; 61. true or false: `a1` and `a2` are the same atom where `a1` is 'Harry and
  ;; `a2` is 'Harry?

  ;; true, because `a1` is the atom 'Harry and `a2` is the atom 'Harry.


  ;; 62. Is

  (= 'Harry 'Harry)

  ;; true or false?

  ;; true, because it is just another way to ask, "Are these S-expressions the
  ;; same non-numeric atom?" (in Clojure it is "are they the same value?").


  ;; 63. Is

  (= 'margarine 'butter)

  ;; true or false?

  ;; false, since they are different atoms.


  ;; 64. How many arguments does `=` take and what are they?

  ;; It takes two arguments. Both of them must be non-numeric atoms. (in Clojure
  ;; `=` is variadic and its arguments can be any values.)


  ;; 65. Is

  (= '() '(strawberry))

  ;; true or false?

  ;; No answer, the arguments are lists. (in Clojure, result is false).


  ;; 66. Is

  (= 6 7)

  ;; true or false?

  ;; No answer, 6 and 7 are numbers. (in Clojure, `=` accepts numbers.)


  ;;;;;;;;;;;;;;;;;
  ;; The Law of `=`
  ;;;;;;;;;;;;;;;;;

  ;; The primitive `=` takes two arguments. Each must be a non-numeric atom.


  ;; 67. Is

  (= (first '(Mary had a little lamb chop)) 'Mary)

  ;; true or false?

  ;; true, because `first` of the list is 'Mary, and the second argument is also
  ;; the atom 'Mary.


  ;; 68. Is

  (= (rest '(soured milk)) 'milk)

  ;; No answer. See the Laws of `=` and `rest`. (in Clojure, result is false.)


  ;; 69. Is

  (let [l '(beans beans we need jelly beans)]
    (= (first l)
       (first (rest l))))

  ;; true, because it compares the first and second atoms in the list.


  ;; Now go make yourself a peanut butter and jelly sandwich.

)
