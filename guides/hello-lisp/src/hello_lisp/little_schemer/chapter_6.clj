(ns hello-lisp.little-schemer.chapter-6
  (:require
   [hello-lisp.functions :refer [atom? lat? pow numbered?
                                 operator first-sub-exp second-sub-exp
                                 *zero? *inc *dec]]))


;; Source: The Little Schemer, 4th edition

;; see also https://github.com/quux00/little-schemer


;; # 6. Shadows

(comment

  ;; 1. Is 1 an arithmetic expression?

  ;; Yes.


  ;; 2. Is 3 an arithetic expression?

  ;; Yes, of course.


  ;; 3. Is 1 + 3 and arithmetic expression?

  ;; Yes!


  ;; 4. Is 1 + 3 * 4 an arithmetic expression?

  ;; Definitely.


  ;; 5. Is 'cookie an arithmetic expression?

  ;; Yes. Are you almost ready for one?


  ;; 6. And, what about 3 ^ y + 5?

  ;; Yes.


  ;; 7. What is an arithmetic expression in your words?

  ;; In ours: "For the purpose of this chapter, an arithmetic expression is
  ;; either an atom (including numbers), or two arithmetic expressions combined
  ;; by `+`, `*`, or `pow`."


  ;; 8. What is

  (quote a)

  ;; 'a.


  ;; 9. What is

  (quote +)

  ;; The atom '+, not the operation `+`.


  ;; 10. What '* stand for?

  ;; The atom '*, not the operation `*`.


  ;; 11. Is true or false

  (let [y 'a]
    (= 'a y))

  ;; true.


  ;; 12. Is true or false

  (let [x 'a
        y 'a]
    (= x y))

  ;; That's the same question again. And the answer is still true.


  ;; 13. Is (n + 3) an arithmetic expression?

  ;; Not really, since there are parentheses around n + 3. Our definition of
  ;; arithmetic expression does not mention parentheses.


  ;; 14. Could we think of (n + 3) as an arithmetic expression?

  ;; Yes, if we keep in mind that the parentheses are not really there.


  ;; 15. What would you call (n + 3)?

  ;; We call it a representation for n + 3.


  ;; 16. Why is (n + 3) a good representation?

  ;; Because
  ;; 1. (n + 3) is an S-expression. It can therefore serve as an argument for a
  ;;    function.
  ;; 2. It structurally resembles n + 3.


  ;; 17. true or false:

  (let [x 1]
    (numbered? x))

  ;; true.


  ;; 18. How do you represent 3 + 4 * 5?

  ;; (3 + (4 * 5)).


  ;; 19. true or false:

  (let [y '(3 + (4 pow 5))]
    (numbered? y))

  ;; true.


  ;; 20. true or false:

  (let [z '(2 * sausage)]
    (numbered? z))

  ;; false, because 'sausage is not a number.


  ;; 21. What is `numbered?`

  ;; It is a function that determines whether a representation of an arithmetic
  ;; expression contains only numbers besides the '+, '*, and 'pow.


  ;; 22. Now can you write a skeleton for `numbered?`

  (defn numbered?-1 [aexp]
    (cond ...))

  ;; is a good guess.


  ;; 23. What is the first question?

  (atom? aexp)


  ;; 24. What is

  (= (first (rest aexp)) '+)

  ;; it is the second question.


  ;; 25. Can you guess the third one?

  (= (first (rest aexp)) '*)

  ;; is perfect.


  ;; 26. And you must know the fourth one.,

  (= (first (rest aexp)) 'pow), of course.


  ;; 27. Should we ask another question about `aexp`?

  ;; No! So we could replace the previous question by `:else`.


  ;; 28. Why do we ask four, instead of two, questions about arithmetic
  ;; expressions? After all, arithmetic expressions like (1 + 3) are lats.


  ;; 29. Because we consider (1 + 3) as a representation of an arithmetic
  ;; expression in list form, not as a list itself. And, an arithmetic
  ;; expression is either a number, or two arithmetic expressions combined by
  ;; `+`, `*`, or `pow`.


  ;; 30. Now you can almost write `numbered?`

  ;; Here is our proposal:

  (defn numbered?-2 [aexp]
    (cond
      (atom? aexp) (number? aexp)
      (= (first (rest aexp)) '+) ...
      (= (first (rest aexp)) '*) ...
      (= (first (rest aexp)) 'pow)
      ...))


  ;; 31. Why do we ask (`number?` `aexp`) when we know that `aexp` is an atom?

  ;; Because we want to know if all arithmetic expressions that are atoms are
  ;; numbers.


  ;; 32. What do we need to know if the `aexp` consists of two arithmetic
  ;; expressions combined by `+`?

  ;; We need to find out whether the two subexpressions are numbered.


  ;; 33. In which position is the first subexpression?

  ;; It is the `first` of `aexp`.


  ;; 34. In which position is the second subexpression?

  ;; It is the `first` of the `rest` of the `rest` of `aexp`.


  ;; 35. So what do we need to ask?

  (numbered? (first aexp))

  ;; and

  (numbered? (first (rest (rest aexp))))

  ;; Both must be true.


  ;; 36. What is the second answer?

  (and (numbered? (first aexp))
       (numbered? (first (rest (rest aexp)))))


  ;; 37. Try `numbered?` again.

  (defn numbered?-3 [aexp]
    (cond
      (atom? aexp) (number? aexp)
      (= (first (rest aexp)) '+)
      (and (numbered?-3 (first aexp))
           (numbered?-3 (first (rest (rest aexp)))))
      (= (first (rest aexp)) '*)
      (and (numbered?-3 (first aexp))
           (numbered?-3 (first (rest (rest aexp)))))
      (= (first (rest aexp)) 'pow)
      (and (numbered?-3 (first aexp))
           (numbered?-3 (first (rest (rest aexp)))))))


  ;; 38. Since `aexp` was already understood to be an arithmetic expression,
  ;; could we have written `numbered?` in a simpler way?

  ;; Yes:

  (defn numbered?-4 [aexp]
    (cond
      (atom? aexp) (number? aexp)
      :else (and (numbered?-4 (first aexp))
                 (numbered?-4 (first (rest (rest aexp)))))))


  ;; 39. Why can we simplify?

  ;; Because we know we've got the function right.


  ;; 40. What is the

  (let [u 13]
    (value u))

  ;; 13.


  ;; 41.

  (let [x '(1 + 3)]
    (value x))

  ;; 4.


  ;; 42.

  (let [y '(1 + (3 pow 4))]
    (value y))

  ;; 82.


  ;; 43.

  (let [z 'cookie]
    (value z))

  ;; No answer.


  ;; 44. (`value` `nexp`) returns what we think is the natural value of a
  ;; numbered arithmetic expression.

  ;; We hope.


  ;; 45. How many questions does `value` ask about `nexp`?

  ;; Four.


  ;; 46. Now, let's attempt to write `value`.

  (defn value-1 [nexp]
    (cond
      (atom? nexp) ...
      (= (first (rest (rest nexp))) '+) ...
      (= (first (rest (rest nexp))) '*) ...
      :else ...))


  ;; 47. What is the natural value of an arithmetic expression that is a number?

  ;; It is just that number.


  ;; 48. What is the natural value of an arithmetic expression that consists of
  ;; two arithmetic expressions combined by `+`?

  ;; If we had the natural value of the two subexpressions, we could just add up
  ;; the two values.


  ;; 49. Can you think of a way to get the value of the two subexpressions in

  '(1 + (3 * 4))

  ;; Of course, by applying `value` to 1, and applying `value` to '(3 * 4).


  ;; 50. And in general?

  ;; By recurring with `value` on the subexpressions.


  ;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Seventh Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Recur on the subparts that are of the same nature:
  ;; - On the sublists of a list.
  ;; - On the subexpressions of an arithmetic expression.


  ;; 51. Give `value` another try.

  (defn value-2 [nexp]
    (cond
      (atom? nexp) nexp
      (= (first (rest  nexp)) '+)
      (+ (value-2 (first nexp))
         (value-2 (first (rest (rest nexp)))))
      (= (first (rest nexp)) '*)
      (* (value-2 (first nexp))
         (value-2 (first (rest (rest nexp)))))
      :else (pow (value-2 (first nexp))
                 (value-2 (first (rest (rest nexp)))))))


  ;; 52. Can you think of a different representation of arithmetic expressions?

  ;; There are several of them.


  ;; 53. Could '(3 4 +) represent 3 + 4?

  ;; Yes.


  ;; 54. Could '(+ 3 4)?

  ;; Yes.


  ;; 55. Or '(plus 3 4)?

  ;; Yes.


  ;; 56. Is '(+ (* 3 6) (pow 8 2)) a representation of an arithmetic expression?

  ;; Yes.


  ;; 57. Try to write the function `value` for a new kind of arithmetic
  ;; expression that is either:
  ;; - a number
  ;; - a list of the atom '+ followed by two arithmetic expressions,
  ;; - a list of the atom '* followed by two arithmetic expressions, or
  ;; - a list of the atom 'pow followed by two arithmetic expressions.

  ;; What about

  (defn value-3 [nexp]
    (cond
      (atom? nexp) nexp
      (= (first nexp) '+) (+ (value-3 (rest nexp))
                             (value-3 (rest (rest nexp))))
      (= (first nexp) '*) (* (value-3 (rest nexp))
                             (value-3 (rest (rest nexp))))
      :else (pow (value-3 (rest nexp))
                 (value-3 (rest (rest nexp))))))


  ;; 58. You guessed it.

  ;; It's wrong.


  ;; 59. Let's try an example.

  '(+ 1 3)


  ;; 60.

  (atom? '(+ 1 3))

  ;; No.


  ;; 61.

  (= (first '(+ 1 3)) '+)

  ;; Yes.


  ;; 62. And now recur.

  ;; Yes.


  ;; 63. What is

  (rest '(+ 1 3))

  ;; '(1 3).


  ;; 64. '(1 3) is not our representation of an arithmetic expression.

  ;; No, we violated The Seventh Commandment. '(1 3) is not a subpart that is a
  ;; representation of an arithmetic expression! We obviously recurred on a
  ;; list. But remember, not all lists are representations of arithmetic
  ;; expressions. We have to recur on subexpressions.


  ;; 65. How can we get the first subexpression of a representation of an
  ;; arithmetic expression?

  ;; By taking the `first` of the `rest`.


  ;; 66. Is

  (rest (rest '(+ 1 3)))

  ;; an arithmetic expression?

  ;; No, the `rest` of the `rest` is '(3), and '(3) is not an arithmetic
  ;; expression.


  ;; 67. Again, we were thinking of the list '(+ 1 3) instead of the
  ;; representation of an arithmetic expression.

  ;; Taking the `first` of the `rest` of the `rest` gets us back on the right
  ;; track.


  ;; 68. What do we mean if we say the `first` of the `rest` of `nexp`?

  ;; The first subexpression of the representation of an arithmetic expression.


  ;; 69. Let's write a function `first-sub-exp` for arithmetic expressions.

  (defn first-sub-exp-1 [aexp]
    (cond
      :else (first (rest aexp))))


  ;; 70. Why do we ask `:else`?

  ;; Because the first question is also the last question.


  ;; 71. Can we get by without `cond` if we don't need to ask questions?

  ;; Yes, remember one-liners from chapter 4.

  (defn first-sub-exp-2 [aexp]
    (first (rest aexp)))


  ;; 72. Write `second-sub-exp` for arithmetic expressions.

  (defn second-sub-exp-1 [aexp]
    (first (rest (rest aexp))))


  ;; 73. Finally, let's replace (`first` `nexp`) by (`operator` `nexp`).

  (defn operator-1 [aexp]
    (first aexp))


  ;; 74. Now write `value` again.

  (defn value-4 [nexp]
    (cond
      (atom? nexp) nexp
      (= (operator nexp) '+) (+ (value-4 (first-sub-exp nexp))
                                (value-4 (second-sub-exp nexp)))
      (= (operator nexp) '*) (* (value-4 (first-sub-exp nexp))
                                (value-4 (second-sub-exp nexp)))
      :else (pow (value-4 (first-sub-exp nexp))
                 (value-4 (second-sub-exp nexp)))))


  ;; 75. Can we use this `value` function for the first representation of
  ;; arithmetic expressions in this chapter?

  ;; Yes, by changing `first-sub-exp` and `operator`.


  ;; 76. Do it!

  (defn first-sub-exp-3 [aexp]
    (first aexp))

  (defn operator-2 [aexp]
    (first (rest aexp)))


  ;; 77. Wasn't this easy?

  ;; Yes, because we used help functions to hide the representation.


  ;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Eighth Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Use help functions to abstract from representations.


  ;; 78. Have we seen representations before?

  ;; Yes, we just did not tell you that they were representations.


  ;; 79. For what entities have we used representations?

  ;; Truth-values! Numbers!


  ;; 80. Numbers are representations?

  ;; Yes. For example, 4 stands for the concept "four". We chose that symbol
  ;; because we are accustomed to arabic representations.


  ;; 81. What else could we have used?

  ;; '(() () () ()) would have served just as well. What about '((((()))))? How
  ;; about '(I V)?


  ;; 82. Do you remember how many primitives we need for numbers?

  ;; Four: `number?`, `zero?`, `inc`, and `dec`.


  ;; 83. Let's try another representation for numbers. How shall we represent
  ;; zero now?

  ;; '() is our choice.


  ;; 84. How is "one" represented?,

  ;; '(()).


  ;; 85. How is two represented?

  ;; '(() ()).


  ;; 86. Got it? What's three?

  ;; Three is '(() () ()).


  ;; 87. Write a function to test for zero.

  (defn *zero?-1 [n]
    (empty? n))


  ;; 88. Can you write a function that is like `inc`?

  (defn *inc-1 [n]
    (cons '() n))


  ;; 89. What about `dec`?

  (defn *dec-1 [n]
    (rest n))


  ;; 90. Is this correct?

  ;; Let's see.


  ;; 91. What is

  (*dec '())

  ;; No answer (in Clojure '()), but that's fine. Recall The Law of `rest`.


  ;; 92. Rewrite `+` using this representation.

  (defn *+ [n m]
    (cond
      (*zero? m) n
      :else (*inc (*+ n (*dec m)))))


  ;; 93. Has the definition of `+` changed?

  ;; Yes and no. It changed, but only slightly.


  ;; 94. Recall `lat?`

  ;; Easy:

  (defn lat?-1 [l]
    (cond
      (empty? l) true
      (atom? (first l)) (lat?-1 (rest l))
      :else false))

  ;; But why did you ask?


  ;; 95. Do you remember what the value of

  (let [ls '(1 2 3)]
    (lat? ls))

  ;; true, of course.


  ;; 96. What is '(1 2 3) with our new numbers?

  ;; '((()) (() ()) (() () ())).


  ;; 97. What is

  (lat? '((()) (() ()) (() () ())))

  ;; it is very false.


  ;; 98. Is that bad?

  ;; You must beware of shadows.

)
