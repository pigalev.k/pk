;; no way around that

(print "Hello from Fennel, world!")


;;;; functions and lambdas

;; functions from Lua's standard library can be called right away
;; https://www.lua.org/manual/5.1/manual.html#5

;; does not check arity when called

(fn print-and-add [a b c]
  (print a)
  (+ b c))

;; checks arity

(lambda print-and-add-safe [a b c ?d]
  "Argument `d' is optional."
  (print a)
  (print ?d)
  (+ b c))

;; variadic

;; the ... form is not a list or first class value, it expands to multiple
;; values inline (like ...spread in JS); to access individual elements,
;; destructure with parentheses, or first wrap it in a table literal ([...])
;; and index like a normal table; or pass it directly to another function

;; ... cannot be closed over by inner functions

(fn print-each [...]
  (each [i v (ipairs [...])]
    (print (.. "Argument " i " is " v))))

(comment

 (print-and-add 1 2 3 4 5)
 (print-and-add-safe 1 2 3 4 5)
 (print-and-add-safe 1 2 3)
 ;; an error
 (print-and-add-safe 1 2)

 (print-each :a :b :c)

)


;;;; locals and variables

;; locals are not reassignable

;; bindings valid inside the `let' form

(let [x 3
      y 4
      sum-of-squares (fn [x y] (+ (* x x) (* y y)))]
  (sum-of-squares x y))

;; binding valid through the entire file

(local a 1)

;; vars are like locals, but reassignable with `set'

(var x 12)
(set x 14)
(print x)


;;;; numbers and strings

;; Lua 5.3+ has doubles and integers

(comment

 (let [x (+ 1 99)
       y (- x 12)
       z 100_000]
   (+ z (/ y 10)))

)

;; Lua 5.3+ also has UTF-8 support in strings

(comment

 (.. "🪨🪨🪨 " "lisp " "rocks, " "lua " "also rocks")

 ;; seems like it counts bytes

 (length "🪨🪨🪨")

 ;; keyword syntax is a shortcut for a string (with no spaces and reserved
 ;; characters)

 (type :a)

)


;;;; tables

;; nils cannot be keys, using nil as value removes the entry

(comment

 ;; associative use

 (local m {:id 1
           "name" "Joe"})

 ;; getting and setting values

 m.id
 (set m.id 11)
 m

 (. m :id)
 (tset m :id 2)
 m

 ;; if a table key has the same name as the source variable, the key name can
 ;; be omitted and a colon (:) used instead

 (let [id 111 name "Mary"
       m {: id : name}]
   m)

 ;; destructuring (associative)

 (let [pos {:x 23 :y 42}
       {:x x-pos :y y-pos} pos]
   (print x-pos y-pos))

 (let [pos {:x 23 :y 42}
       {: x : y} pos]
   (print x y))


 ;; sequential use

 ;; 1-based indexing

 (local v ["abc" "def" "xyz"])

 ;; the same

 (local av {1 "abc" 2 "def" 3 "xyz"})

 (length v)

 ;; destructuring (positional)

 (let [data [1 2 3]
       [fst snd thrd] data]
   (print fst snd thrd))

 ;; destructuring (all together)

 (let [f (fn [] ["abc" "def" {:x "xyz" :y "abc"}])
       [a d {:x x : y}] (f)]
   (print a d)
   (print x y))

)


;;;; iteration

(comment

 (each [key value (pairs {:key1 52 :key2 99})]
   (print key value))

 (each [index value (ipairs [:abc :def :xyz])]
   (print index value))

 ;; any iterator can be used

 (var sum 0)
 (each [digits (string.gmatch "244 127 163" "%d+")]
   (set sum (+ sum (tonumber digits))))
 sum

 ;; putting results to a table (table comprehension)

 (icollect [_ s (ipairs [:greetings :my :darling])]
   (if (not= :my s)
       (s:upper)))

 (collect [_ s (ipairs [:greetings :my :darling])]
   s (length s))

 ;; iterating over range

 (for [i 1 10 2]
   (print i))

)


;;;; conditionals

;; `if' behaves like `cond'; with odd number of clauses, the final clause is
;; interpreted as `else'

(let [x (math.random 64)]
  (if
   (= 0 (% x 2)) :even
   (= 0 (% x 9)) :multiple-of-nine
   :dunno))

(let [x (math.random 64)]
  (when (< 32 x)
    (print "example number:")
    (print (.. "greater than 32: " x))))


;;;; error handling

(comment

 ;; returning multiple values --- value and error; destructure with parens

 ;; does not compose well; the error status must be propagated all the way
 ;; along the call chain from inner to outer; errors are easy to ignore, hard
 ;; to handle properly

 (match (io.open "hello.fnl")
  ;; when `io.open' succeeds, it will return a file, but if it fails it will
  ;; return nil and an `err-msg' string describing why
   f (do (print (f:read :*all))
         (f:close))
   (nil err-msg) (print "Could not open file:" err-msg))

 ;; writing a function that returns multiple values

 (fn use-file [filename]
   "Validates and opens `filename', returning a file and error message."
   (if (valid-file-name? filename)
       (open-file filename)
       (values nil (.. "Invalid filename: " filename))))


 ;; using protected call (`pcall'); it returns a boolean indicating
 ;; success/failure and a value/error

 (let [(ok? val-or-msg) (pcall (fn [] (error "oops!")))]
   (if ok?
      (print "Got value" val-or-msg)
      (print "Could not get value:" val-or-msg)))


 ;; using `assert'; it takes a value and error message, calls `error' with
 ;; message if the value is nil and returns the value otherwise

 (let [filename "nonexistent.file"
       f (assert (io.open filename) (.. "Cannot open file: " filename))
       contents (f.read f "*all")]
   (f.close f)
   contents)

)


;;;; some file io

(comment

 (let [f (assert (io.open "hello.fnl"))]
   (print (f:read)) ; reads a single line by default
   (print (f:read "*a")) ; you can read the whole file
   (f:close))

)


;;;; modules and multiple files

;; modules are simply tables which contain functions and other values; the
;; last value (usually a table) in a file will be used as the value of the
;; module

;; seems like it is better to avoid hyphens in module names/paths

(local mm (require :external.mymath))
(print (.. "Square of 5 is " (mm.square 5)))


;;;; gotchas

;; arithmetic, comparison, and boolean operators are not first-class
;; functions; their number of arguments must be known at compile-time, so they
;; behave weirdly with multiple-return-valued functions

;; no `apply' function; use `table.unpack' to get multiple values inlined

;; tables are compared for identity

;; default `print' does not print Fennel values right outside the REPL, use
;; `fennel.view' to format Fennel values beforehand

(comment

 (local fennel (require :fennel))
 (print (fennel.view [:a :b :c]))

)

;; no nonlocal exits (early returns)
