(load "../practical_common_lisp/common.lisp")

;; creating native executables with SBCL

(defun hullo ()
  (format t "Hullo, world!~%"))

(comment

 ;; in the current dir eval (not in SLY/SLIME)
 (progn
   (load "native.lisp")
   (sb-ext:save-lisp-and-die "hullo" :toplevel #'hullo :executable t))

 ;; e.g.
 sbcl --eval "(progn \
   (load \"native.lisp\") \
   (sb-ext:save-lisp-and-die \"hullo\" :toplevel #'hullo :executable t))"

)
