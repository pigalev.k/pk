(defpackage :common-lisp-cookbook.os
  (:use #:cl))

(in-package :common-lisp-cookbook.os)
(load "../practical_common_lisp/common.lisp")

;; interfacing with OS

(comment

 ;; get environment variable
 (uiop:getenv "HOME")

 ;; get command-line args
 (uiop:command-line-arguments)

 ;; run a program
 ;; syncronously
 (uiop:run-program "evince")
 (uiop:run-program "uname -a" :output t)
 ;; asynchronously
 ;; see if process is still running
 (uiop:process-alive-p
  (uiop:launch-program "gimp"))
 ;; get exit code
 (uiop:wait-process
  (uiop:launch-program "gimp"))

)
