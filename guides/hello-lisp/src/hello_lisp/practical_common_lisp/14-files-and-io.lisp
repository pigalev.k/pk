(load "common")


;; - stream: an abstraction for reading and writing data

;; - pathname: an abstraction for manipulating filenames and paths in
;;   OS-independent way


;; reading file data

(comment

 ;; obtain a stream

 (defparameter *infile* (open "cds.db"))

 ;; reads signal an error on EOF by default

 ;; read a character
 (read-char *infile*)
 ;; read a line (to a string)
 (read-line *infile*)
 ;; read an S-expression (returns a Lisp object)
 (read *infile*)

 (close *infile*)


 (let ((in (open "14-files-and-io.lisp" :if-does-not-exist nil)))
  (when in
    (format t "~a~%" (read-line in))
    (close in)))

)


;; reading binary data

(comment

 (let ((in (open "14-files-and-io.lisp" :element-type '(unsigned-byte 8))))
   (when in
     (prog1
         (read-byte in)
       (close in))))

)


;; bulk reads

(comment

 ;; should be more efficient than reading byte by byte

 (let ((buf (make-array 100))
       (in (open "14-files-and-io.lisp" :element-type '(unsigned-byte 8))))
   (when in
     (prog2
         (read-sequence buf in)
         buf
       (close in))))

)


;; file output

(comment

 ;; open a file for writing (obtain a stream)
 (defparameter *outfile* (open "out.txt" :direction :output
                                         :if-exists :supersede))

 ;; write a char
 (write-char #\? *outfile*)
 ;; write a string
 (write-string "foo" *outfile*)
 ;; add a newline (maybe)
 (fresh-line *outfile*)
 (terpri *outfile*)

 ;; write an S-expression

 ;; human-readable
 (princ '(1 2 3 :foo :bar) *outfile*)
 ;; very human-readable
 (pprint '(1 2 3 :foo :bar) *outfile*)
 ;; arbitrarily human-readable
 (format *outfile* "The ~a is ~a~%" :a '(1 2 3))

 ;; machine-readable
 (prin1 '(1 2 3 :foo :bar) *outfile*)
 ;; machine-readable, newlines before and after
 (print '(1 2 3 :foo :bar) *outfile*)

 ;; machine-readable prints: when `*print-readably*' is non-nil, unreadable
 ;; objects will not be printed and error will be signaled, else they will be
 ;; printed in their special print syntax (hash notation)

 (prin1 #'first *outfile*)
 (let ((*print-readably* t))
   (prin1 #'first *outfile*))

 ;; flush the output stream (actually write to the file)
 (finish-output *outfile*)

 (close *outfile*)


 ;; open a binary file for writing (obtain a stream)
 (defparameter *binoutfile* (open "out.bin" :direction :output
                                            :element-type '(unsigned-byte 8)
                                            :if-exists :supersede))

 ;; write a byte sequence
 (write-sequence #(98 99 100 10 10) *binoutfile*)

 (close *binoutfile*)


 ;; ensure that the opened file is always closed
 (with-open-file (in "14-files-and-io.lisp")
   (read in))

)


;; filenames

;; string filenames are not portable, hence the pathname abstraction over
;; namestrings; streams also represent filenames

;; namestrings, pathnames and streams are pathname designators, and are
;; accepted by all built-in functions expecting a filename

(comment

 ;; pathname has six components: host, device, directory, name, type and
 ;; version

 *default-pathname-defaults*

 ;; constructors

 (defparameter *pathname* (pathname "./14-files-and-io.lisp"))

 ;; accepts any pathname designator
 (pathname *pathname*)
 (pathname (open "14-files-and-io.lisp"))

 (make-pathname :directory '(:absolute "etc")
                :name "hosts")
 (make-pathname :type "html" :defaults *pathname*)

 (merge-pathnames #p"foo/bar.html" #p"/www/html/")
 (merge-pathnames #p"foo/bar.html" #p"html/")

 (merge-pathnames
  (enough-namestring #p"/www/html/foo/bar/baz.html" #p"/www/")
  #p"/www-backups/")

 ;; accessors

 (namestring *pathname*)
 (directory-namestring *pathname*)
 (file-namestring *pathname*)
 (enough-namestring #p"/www/html/foo/bar.html" #p"/www/")

)


;; two representations of directory names: treating directories and files as
;; distinct types of objects or treating all of them as directories

(comment

 ;; file form (created unless the name ends in the path separator)
 (make-pathname :directory '(:absolute "foo") :name "bar")
 ;; directory form
 (make-pathname :directory '(:absolute "foo" "bar"))

)


;; interacting with the file system

(comment

 (defparameter *pathname* (pathname "./14-files-and-io.lisp"))

 ;; if the file exists, returns its truename (with absolute path and symlinks
 ;; resolved)
 (probe-file *pathname*)
 (probe-file "nope")

 ;; the same for directories
 (directory ".")
 (directory "foo")

 (delete-file "not-exist.txt")
 (rename-file "not-exist.txt" "still-not-exist.txt")

 ;; create directories (should be in the directory form)
 (ensure-directories-exist "foo/")

 (file-write-date *pathname*)
 (file-author *pathname*)
 ;; in bytes; stream only; binary stream for predictability
 (with-open-file (in *pathname* :element-type '(unsigned-byte 8))
   (list (file-length in) (file-position in)))

)


;; other kinds of io

(comment

 ;; basic string io

 (read (make-string-input-stream "foo"))

 (let ((out (make-string-output-stream)))
   (prin1 '(1 2 3) out)
   (get-output-stream-string out))

 ;; helper macros

 (with-input-from-string (s "foo")
   (read s))

 (with-output-to-string (out)
   (format out "Hello, world! ")
   (format out "~s" (list 1 2 3)))


 ;; stream plumbing: broadcast streams, concatenated streams, two-way and echo
 ;; streams

 (defparameter *bs* (make-broadcast-stream *standard-output*))
 (prin1 "foo" *bs*)

 (defparameter *inout* (make-two-way-stream *standard-input* *standard-output*))
 (read *inout*)
 (prin1 "foo" *inout*)
 (prin1 (read *inout*) *inout*)

 (defparameter *echo* (make-echo-stream *standard-input* *standard-output*))
 (read *echo*)
 (prin1 "yeah" *echo*)

)
