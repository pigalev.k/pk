(load "common")


;; all values are instances of some class in the class hierarchy rooted at the
;; class T

;; there are two families of classes --- built-in and user-defined; built-in
;; classes cannot be subclassed, but can be specialized on

;; a class defines only a data type, behaviors are defined by generic
;; functions that dispatch on classes; a class is defined by its name, its
;; relation to other classes, and the names of its slots; class names are in a
;; separate namespace from both functions and variables

;; if no superclasses are specified, the new class will directly subclass
;; `standard-object'; all superclasses must be other user-defined classes

;; object slots can be initialized using `:initarg' and `:initform' options on
;; its parameters, or by defining a method on the generic function
;; `initialize-instance'; access to unbound slot is an error

(defclass a-class () ((class :allocation :class
                             :initform "a-class"
                             :accessor a-class)
                      (id :reader an-id
                          :writer (setf an-id)
                          :documentation "An object identifier.")
                      (a-name :initarg :a-name
                              :initform "(no name)"
                              :documentation "Just a name.")))

(defmethod initialize-instance :after ((object a-class) &key id)
  (setf (slot-value object 'id) id))

(defmethod print-object ((object a-class) stream)
  (format stream "#<a-class id: ~a, name: ~a>"
          (slot-value object 'id) (slot-value object 'a-name)))

(comment

 ;; create instances
 (print-object (make-instance 'a-class) t)
 (defparameter *o* (make-instance 'a-class :id 1 :a-name "quux"))
 (print-object *o* t)

 ;; access slots
 (slot-value *o* 'a-name)

 ;; change the slot values
 (setf (slot-value *o* 'a-name) "foo")
 (print-object *o* t)

)


;; accessor functions

;; providing accessor/mutator functions instead of direct slot access helps to
;; incapsulate the object state; that allows the internal representation be
;; changed without breaking the code that uses the object, and to control the
;; ways the outside code can access or modify the object's state

;; it may be a good idea to define accessor function as generic ones, for use
;; in subclasses

(defun a-name (an-object)
  "Returns a name of AN-OBJECT."
  (slot-value an-object 'a-name))

(defun (setf a-name) (value an-object)
  "Sets the name of AN-OBJECT to VALUE."
  (setf (slot-value an-object 'a-name) value))

(comment

 (defparameter *o* (make-instance 'a-class :id (random 10) :a-name "frob"))
 (a-name *o*)
 (setf (a-name *o*) "yeah")
 (print-object *o* t)

 ;; use `:reader'/`:writer'/`:accessor' options of slots in `defclass' instead
 ;; of writing them manually (`:accessor' is a combination of the first two)

 (an-id *o*)
 (setf (an-id *o*) 12)
 (print-object *o* t)

 ;; convenience macros for usage of slots/accessors

 (with-slots (id a-name) *o*
   (format t "an object: id ~a, name ~a~%" id a-name))

 (with-accessors ((id an-id)) *o*
   (setf id 111)
   (format t "an object: id ~a~%" id))

 ;; class-allocated slots (shared between all instances, accessed through
 ;; instances only)

 (a-class *o*)
 (setf (a-class *o*) "yet-another-class")
 (a-class (make-instance 'a-class))

)


;; slots and inheritance

;; classes inherit slots as well as behavior from their superclasses; a given
;; object can have only one slot with a particular name --- if more than one
;; class defines the slot, all slot specifiers for that slot are merged

;; `:allocation' and `:initform' are used from the most specific class;
;; `:initarg's are taken from all specifiers and any of them can be used;
;; `:reader'/`:writer'/`:accessor' are not included in the merged slot
;; specifier (the superclass methods will already apply to the new class)


;; multiple inheritance

;; having more than one direct superclass complicates notion of class
;; specificity that is used both when building effective methods for a generic
;; function and when merging inherited slots; the rule of superclass-subclass
;; specificity alone is not sufficient, so there is a second rule to sort
;; unrelated superclasses --- by order they're listed in the superclass list,
;; with specificity decreasing left to right; thus every class have a linear
;; precedence list (but there is no global ordering of classes, as the same
;; classes may appear in different orders in different classes' precedence
;; lists)

(comment

 ;; given this class hierarchy

 (defclass bank-account () ())
 (defclass savings-account (bank-account) ())
 (defclass checking-account (bank-account) ())
 (defclass money-market-account (checking-account savings-account) ())

 ;; the precedence list of `money-market-account' will be

 '(money-market-account
   checking-account
   savings-account
   bank-account
   standard-object
   t)

)
