(load "common")


;; there are sequential and associative composite data types


;; vectors

;; vector is an integer-indexed sequential collection; it may be fixed-size of
;; resizable

(comment

 ;; fixed-size vector
 (vector 1 2 3 4 5)
 #(3 2 1)

 ;; any vector

 ;; fixed-size
 (make-array 5 :initial-element nil)

 ;; resizable (max 5 elements)
 (defparameter *v* (make-array 5 :fill-pointer 0))
 (vector-push 2 *v*)
 (vector-pop *v*)

 ;; resizable (any size)
 (defparameter *rv* (make-array 2 :fill-pointer 0 :adjustable t))
 (vector-push-extend 3 *rv*)

)


;; subtypes of vector

;; specialized vectors can hold certain types of elements

(comment

 ;; strings are vectors specialized to hold characters

 (defparameter *s* (make-array 5 :element-type 'character
                                 :initial-element #\x
                                 :fill-pointer 0
                                 :adjustable t))
 (vector-push-extend #\? *s*)

 ;; bit vectors have zeros and ones as elements

 #*101

 (defparameter *bv* (make-array 0 :element-type 'bit
                                  :fill-pointer 0
                                  :adjustable t))

 (vector-push-extend 1 *bv*)
 (vector-push-extend 0 *bv*)

)


;; sequences

(comment

 ;; all these functions also work on other sequences --- e.g., lists

 (defparameter *v* (vector 1 2 3))

 ;; basic operations

 (length *v*)
 (elt *v* 1)
 (setf (elt *v* 1) 12)

 ;; acting on certain elements

 (count 1 *v*)
 (find 3 *v*)
 (position 3 *v*)
 (remove 3 *v*)
 (substitute 13 3 *v*)

 ;; higher-order function variants

 (count-if #'evenp *v*)
 (find-if-not #'evenp *v*)
 (position-if #'digit-char-p "abcd0001")
 (remove-if-not #'evenp *v*)
 (substitute-if 13 #'oddp *v*)
 (remove-duplicates #(1 2 1 2 3 1 2 3 4))

 ;; whole sequence manipulation

 (copy-seq *v*)
 (reverse *v*)
 (concatenate 'list *v* *v*)

 ;; sorting and merging

 ;; destructive
 (sort (vector "foo" "bar" "baz") #'string<)
 (stable-sort (vector "foo" "bar" "baz") #'string<)

 (merge 'vector #(1 3 5) #(2 4 6) #'<)

 ;; subsequences

 (subseq "foobarbaz" 3 6)

 (defparameter *x* (copy-seq "foobarbaz"))
 (setf (subseq *x* 3 6) "xxx")
 (fill *x* #\Y :start 3 :end 6)

 (search "bar" "foobarbaz")
 (mismatch "foobarbaz" "foom")

 ;; sequence predicates

 (every #'evenp #(1 2 3 4 5))
 (some #'evenp #(1 2 3 4 5))
 (notany #'evenp #(1 2 3 4 5))
 (notevery #'evenp #(1 2 3 4 5))

 (every #'> #(1 2 3 4) #(5 4 3 2))
 (some #'> #(1 2 3 4) #(5 4 3 2))
 (notany #'> #(1 2 3 4) #(5 4 3 2))
 (notevery #'> #(1 2 3 4) #(5 4 3 2))

 ;; sequence mapping functions

 (map 'vector #'* #(1 2 3 4 5) #(10 9 8 7 6))
 (defparameter *x* (make-array 3))
 (map-into *x* #'identity '(1 2 3))
 (reduce #'+ #(1 2 3 4 5 6 7 8 9 10))

)


;; hash tables

(comment

 (make-hash-table)

 (defparameter *h* (make-hash-table))
 ;; note: multiple return values
 (gethash 'foo *h*)
 (setf (gethash 'foo *h*) 'quux)
 (gethash 'foo *h*)

 ;; using multiple return values
 (defun show-value (key hash-table)
   "Returns a string with the status of KEY in HASH-TABLE --- its value and
whether it is actually present."
   (multiple-value-bind (value present) (gethash key hash-table)
     (if present
         (format nil "Value ~a is actually present." value)
         (format nil "Value ~a because key not found." value))))

 (show-value 'foo *h*)
 (setf (gethash 'foo *h*) nil)
 (show-value 'foo *h*)
 (remhash 'foo *h*)
 (show-value 'foo *h*)

 ;; hash table iteration

 (setf (gethash 'foo *h*) 'quux)
 (setf (gethash 'bar *h*) 'frob)
 (maphash #'(lambda (k v) (format t "~a => ~a~%" k v)) *h*)
 ;; it is undefined behavior to add or remove elements while iterating; but ok
 ;; to `setf' or `remhash' the current element

 (loop for k being the hash-keys in *h* using (hash-value v)
       do (format t "~a => ~a~%" k v))

)
