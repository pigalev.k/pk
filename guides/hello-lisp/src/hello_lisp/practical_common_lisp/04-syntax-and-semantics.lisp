(load "common")


;; execution of Lisp programs are two-phase process: reading (strings of
;; characters to S-expressions) and evaluation (of S-expressions as Lisp forms)

;; thus, there are two levels of syntax: syntax of S-expressions in terms of
;; strings of characters and syntax of Lisp forms in terms of S-expressions

;; S-expressions is a ready to use serializable data format, and the fact that
;; code is data (i.e., some data is code) facilitates code analysis and
;; generation


;; S-expressions

;; there are two kinds of S-expressions: lists, delimited by parentheses and
;; containing arbitrary number of space-separated elements (that are themselves
;; S-expressions) and atoms (everything else) --- e.g. numbers, strings and
;; symbols; there are some extensions to the reader syntax --- reader macros


;; S-expressions as Lisp forms

;; any atom is a legal Lisp form; any list with a symbol as its first element is
;; a legal Lisp form

;; evaluator takes a syntactically well-formed Lisp form and returns a value ---
;; the value of the form

;; atoms can be divided into two categories: symbols and everything else;
;; symbol, when evaluated, is considered the name of a variable and evaluates to
;; the current value of the variable; all other atoms are self-evaluating ---
;; they are returned by evaluator as is

;; certain symbols are also self-evaluating --- e.g., `t', `nil' and keywords
;; (symbols starting with a colon)

;; all well-formed list forms start with are symbol, but there are three kinds
;; of forms that evaluate in three different ways: function call forms, macro
;; forms, and special forms; a symbol not yet defined assumed to be a function
;; name by default


;; function calls

;; the evaluation rule is this: evaluate the remaining elements of the list as
;; Lisp forms and pass the resulting values to the named function

;; special forms

;; special forms have different evaluation rules, specific to their operators;
;; some has specific reader syntax (like `quote')


;; macros

;; a macro is a function that takes unevaluated S-expressions as arguments and
;; returns a Lisp form (macro expansion) that's then evaluated in place of the
;; original macro form; argument S-expressions don't need to be well-formed Lisp
;; forms; a macro assigns a meaning to its arguments, thus a macro defines its
;; own local syntax


;; truth, falsehood and equality

;; `nil' is false, and everything else is true; the canonical value for truth is
;; `t'; `nil' is equivalent to the empty list, and is both an atom and a list

;; there are four generic equality predicates: `eq' tests for identity; `eql' is
;; like it but consider the objects representing the same value (of the same
;; class --- numeric or character) to be equivalent; `equal' is general --- can
;; operate on all types of objects and considers objects' structure and values;
;; `equalp' is even less discriminating

(comment

 (eq 1 1)
 (eql 1 1)
 (eql (- 3 2) (- 4 3))
 (equal '(1 2 3) '(1 2 3))
 (equalp 1 1.0)

)
