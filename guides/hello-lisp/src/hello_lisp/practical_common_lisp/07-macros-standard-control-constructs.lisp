(load "common")


;; conditionals

(comment

 ;; basic conditional construct

 (if t :ok :not-ok)

 ;; specialized versions

 (when t
   (format t "yeah ")
   (format t "right~%"))

 (unless nil
   (format t "nope ")
   (format t "not~%"))

 (cond
   (nil :not-ok)
   (1 :so-so)
   (t :ok)
   (:else :dunno))

)


;; looping constructs

(comment

 ;; basic looping construct

 ;; computes 11th Fibonacci number
 (do ((n 0 (1+ n)) ; bindings: (var init next)
      (cur 0 next)
      (next 1 (+ cur next)))
     ((= 10 n) cur)) ; result: (end-test result)
 ;; body: a list of forms (empty here)

 ;; specialized versions

 ;; looping over the list
 (dolist (element '(1 2 3))
   (format t "~a " element))

 ;; looping given number of times
 (dotimes (i 4)
   (format t "~a " i))

 ;; the looping mini-language

 ;; simple form
 (loop
   (format t "brrr"))

 ;; extended form
 (loop for i from 1 to 10 collecting i)
 (loop :for i :from 1 :to 10 :collecting i)

)
