(load "common")


;; functions are pieces of functionality


(defun hello-world ()
  "Greets a world."
  (format t "Hello, world!"))

;; required parameters
(defun square (x)
  "Squares X."
  (* x x))

;; optional parameter, with default value
(defun square-and-sum (x &optional (y 2))
  "Squares X and adds Y, that defaults to 2."
  (+ y (square x)))

;; rest parameters
(defun square-sum-and-cons (x &optional (y x y-supplied-p) &rest more)
  "Squares X, adds Y and adds the result to a list MORE."
  (unless y-supplied-p
    (format t "default value for `y': ~a~%" y))
  (cons (square-and-sum x y) more))

;; keyword parameters
(defun square-and-then-some
    (x &key (minus 10) ((:plus add) 100) print)
  "Squares X, then adds PLUS and subtracts MINUS."
  (let ((result (- (+ (square x) add) minus)))
    (when print
      (format t "result: ~a~%" result))
    result))

;; early returns

(defun foo (n)
  (dotimes (i 10)
    (dotimes (j 10)
      (when (> (* i j) n)
        (return-from foo (list i j))))))

(comment

 (documentation 'hello-world 'function)

 (square 5)

 (square-and-sum 5 10)
 (square-and-sum 5)

 (square-sum-and-cons 5 3 3 2 1)
 (square-sum-and-cons 5 3)
 (square-sum-and-cons 5)

 (square-and-then-some 5 :plus 1 :minus 1)
 (square-and-then-some 5)

 (foo 10)
 (foo 100)

 (function square-sum-and-cons)
 #'square-sum-and-cons
 (funcall #'square-sum-and-cons 5 6 7 8)
 (apply #'square-sum-and-cons '(5 6 7 8))

 (mapcar #'square '(1 2 3 4 5))

)
