(load "common")


;; a simple database of CDs


;; application state

(defvar *db* nil
  "The CD database.")


;; domain entities: data abstraction

(defun make-cd (title artist rating ripped)
  "Returns a CD record with TITLE, ARTIST, RATING and RIPPED fields."
  (list :title title :artist artist :rating rating :ripped ripped))

(defun add-record (cd)
  "Adds the CD to the database."
  (push cd *db*))


;; pretty-printing the db

(defun dump-db ()
  "Pretty-prints the database contents in tabular form."
  (dolist (cd *db*)
    (format t "~{~a:~10t~a~%~}~%" cd)))

(defun dump-db* ()
  "Pretty-prints the database contents in tabular form."
  (format t "~{~{~a:~10t~a~%~}~%~}" *db*))


;; persisting and restoring the db

(defun save-db (filename)
  "Saves the current database contents to a file with name FILENAME."
  (with-open-file (out filename
                       :direction :output
                       :if-exists :supersede)
    (with-standard-io-syntax
      (print *db* out))))

(defun load-db (filename)
  "Loads the current database from a file with name FILENAME."
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *db* (read in)))))

(defun clear-db ()
  "Clears the current database, removing all records."
  (setf *db* nil))


;; querying the db

(defun select-by-artist (artist)
  "Selects records whose `:artist' field is equal to ARTIST."
  (remove-if-not (lambda (cd) (equal (getf cd :artist) artist)) *db*))

(defun select (selector-fn)
  "Selects records for which SELECTOR-FN returns true."
  (remove-if-not selector-fn *db*))

(defun artist-selector (artist)
  "Returns a selector function that takes a plist and returns true if its
`:artist' field is equal to ARTIST."
  (lambda (cd) (equal (getf cd :artist) artist)))

(defun where (&key title artist rating (ripped nil ripped-p))
  "Returns a selector function that takes a plist and returns true if its
`:title', `:artist', `:rating' and `ripped' fields are equal correspondingly to
TITLE, ARTIST, RATING and RIPPED (those actually supplied)."
  (lambda (cd)
    (and (if title (equal (getf cd :title) title) t)
         (if artist (equal (getf cd :artist) artist) t)
         (if rating (equal (getf cd :rating) rating) t)
         (if ripped-p (equal (getf cd :ripped) ripped) t))))

(defun make-comparison-expr (field value)
  "Returns a predicate expression comparing VALUE (with `equal') to a field named
FIELD in a plist named CD."
  `(equal (getf cd ,field) ,value))

(defun make-comparisons-list (fields)
  "Returns a list of predicate expressions, applying `make-comparison-expr' to
pairs of elements of plist FIELDS."
  (loop while fields
        collecting (make-comparison-expr (pop fields) (pop fields))))

(defmacro where* (&rest clauses)
  "Returns a selector functions that takes a plist and returns true if it
satisfies all comparison predicates created from field-value pairs in CLAUSES."
  `(lambda (cd) (and ,@(make-comparisons-list clauses))))


;; updating the db

(defun update (selector-fn &key title artist rating (ripped nil ripped-p))
  "Updates the records satisfying the SELECTOR-FN, setting their `:title',
`:artist', `:rating' and `:ripped' fields to TITLE, ARTIST, RATING and
RIPPED (those actually supplied)."
  (setf *db*
        (mapcar (lambda (row)
                  (when (funcall selector-fn row)
                    (if title (setf (getf row :title) title))
                    (if artist (setf (getf row :artist) artist))
                    (if rating (setf (getf row :rating) rating))
                    (if ripped-p (setf (getf row :ripped) ripped)))
                  row)
                *db*)))

(defun delete-rows (selector-fn)
  "Deletes the records that satisfy SELECTOR-FN."
  (setf *db* (remove-if selector-fn *db*)))


;; user interaction

(defun prompt-read (prompt)
  "Displays PROMPT and returns user input."
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defun prompt-for-cd ()
  "Displays prompt for title, artist, rating and ripped flag, and returns a CD
record created from them."
  (make-cd
   (prompt-read "Title")
   (prompt-read "Artist")
   (or (parse-integer (prompt-read "Rating") :junk-allowed t) 0)
   (y-or-n-p "Ripped (y/n): ")))

(defun add-cds ()
  "Repeatedly asks user to input CD record information and adds the records to
the database."
  (loop (add-record (prompt-for-cd))
        (if (not (y-or-n-p "Another? (y/n): "))
            (return))))


(comment

 (add-record (make-cd "Roses" "Kathy Mattea" 7 t))
 (add-record (make-cd "Fly" "Dixie Chicks" 8 t))
 (add-record (make-cd "Home" "Dixie Chicks" 9 t))

 *db*

 (dump-db)
 (dump-db*)

 (add-cds)

 (save-db "cds.db")
 (clear-db)
 (load-db "cds.db")
 *db*

 (select-by-artist "Dixie Chicks")
 (select (lambda (cd) (null (getf cd :ripped))))
 (select (artist-selector "Dixie Chicks"))
 (select (where :rating 10 :ripped nil))

 (update (where :artist "Dixie Chicks") :rating 12)
 (delete-rows (where :rating 4))


 ;; remove duplication from `where' and make it open for extension
 ;; (e.g. handle additional fields in the record), using a macro

 (select (where* :artist "Dixie Chicks" :title "Home"))
 (macroexpand '(where* :artist "Dixie Chicks" :title "Home"))

)


;; an example macro

(defmacro backwards (expr)
  "Reverses the EXPR."
  (reverse expr))

(comment

 (backwards ("Hello, world!" "~a" t format))
 (macroexpand '(backwards ("Hello, world!" "~a" t format)))

)
