(load "common")


;; to write a macro, you need examples of the calls to this macro, showing the
;; input and the the desired expansion (starting at either end)

;; then write the code that transforms the inputs to the outputs

;; then make sure the abstraction the macro provides doesn't leak the
;; implementation details; rules of thumb: evaluate arguments once and in the
;; order they are supplied, use `gensym' for symbols bound during expansion
;; generation


;; simple conditionals

;; example: (when t (print "ok") :ok) ->
;;          (if t (progn (print "ok") :ok))

(defmacro when* (condition &rest body)
  "Evaluates BODY forms and returns the value of the last
form if CONDITION is met, returns nil otherwise."
  `(if ,condition (progn ,@body)))

;; example: (unless nil (print "ok") :ok) ->
;;          (if (not nil) (progn (print "ok") :ok))

(defmacro unless* (condition &rest body)
  "Evaluates BODY forms and returns the value of the last
form if CONDITION is not met, returns nil otherwise."
  `(if (not ,condition) (progn ,@body)))

(comment

 (when* t
        (format t "yeah ")
        (format t "right~%"))
 (macroexpand '(when* t
                (format t "yeah ")
                (format t "right~%")))


 (unless* nil
          (format t "nope ")
          (format t "not~%"))
 (macroexpand '(unless* nil
                (format t "nope ")
                (format t "not~%")))

)


;; looping over primes

(comment

 ;; example call

 (do-primes (p 0 19)
   (format t "~d " p))

 ;; desired expansion

 (defun primep (number)
   "Returns T if NUMBER is prime."
   (when (> number 1)
     (loop for fac from 2 to (isqrt number) never (zerop (mod number fac)))))

 (defun next-prime (number)
   (loop for n from number when (primep n) return n))

 (do ((p (next-prime 0) (next-prime (1+ p))))
     ((> p 19))
   (format t "~d " p))

 ;; implementation: version 1

 (defmacro do-primes-1 (var-and-range &rest body)
   "Returns a list of all primes in the given range."
   (let ((var (first var-and-range))
         (start (second var-and-range))
         (end (third var-and-range)))
     `(do ((,var (next-prime ,start) (next-prime (1+ ,var))))
          ((> ,var ,end))
        ,@body)))

 (do-primes-1 (p 0 19)
   (format t "~d " p))
 ;; or C-c RET
 (macroexpand-1 '(do-primes-1 (p 0 19)
                  (format t "~d " p)))

 ;; implementation: version 2; more readable, uses destructuring bindings

 (defmacro do-primes-2 ((var start end) &body body)
   "Returns a list of all primes in the given range."
   `(do ((,var (next-prime ,start) (next-prime (1+ ,var))))
        ((> ,var ,end))
      ,@body))

 (do-primes-2 (p 0 19)
   (format t "~d " p))

 ;; implementation: version 4; plugging the leaks (repeated evaluation of `end';
 ;; wrong order of evaluation of the arguments; capture of the symbol for ending
 ;; value)

 (defmacro do-primes ((var start end) &body body)
   "Returns a list of all primes in the given range."
   (let ((ending-value-name (gensym)))
     `(do ((,var (next-prime ,start) (next-prime (1+ ,var)))
           (,ending-value-name ,end))
          ((> ,var ,ending-value-name))
        ,@body)))

 (do-primes (p 0 19)
   (format t "~d " p))
 (do-primes (p 0 (random 100))
   (format t "~d " p))
 (do-primes (ending-value 0 19)
   (format t "~d " ending-value))

)


;; macro-writing macros

(comment

 ;; example call

 (with-gensyms (ending-value-name)
   ending-value-name)

 ;; desired expansion

 (let ((ending-value-name (gensym)))
   ending-value-name)

 ;; implementation

 (defmacro with-gensyms ((&rest names) &body body)
   "Evaluates BODY with NAMES bound to a new generated symbol each."
   `(let ,(loop for n in names collect `(,n (gensym)))
      ,@body))

 (with-gensyms (a b c)
   (list a b c))


 ;; usage

 (defmacro do-primes* ((var start end) &body body)
   "Returns a list of all primes in the given range."
   (with-gensyms (ending-value-name)
     `(do ((,var (next-prime ,start) (next-prime (1+ ,var)))
           (,ending-value-name ,end))
          ((> ,var ,ending-value-name))
        ,@body)))

 (do-primes* (p 0 19)
   (format t "~d " p))

)
