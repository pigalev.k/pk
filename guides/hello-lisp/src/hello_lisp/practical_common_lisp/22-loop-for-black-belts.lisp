(load "common")


;; `loop' captures common looping patterns and allows to express them directly

;; `loop' body is a set of clauses, each of which begins with a loop keyword;
;; main keywords are `for', `collecting', `summing', `counting', `do', and
;; `finally'


;; iteration control

;; `for' or its synonym `as', followed by the name of a variable

;; there can be multiple `for' clauses, and the loop terminates as soon as any
;; clause reaches its end

(comment

 (loop for item in '(:a :b :c)
       as i from 1 to 10
       do (format t "~a: ~a~%" item i))

)


;; counting loops

(comment

 (loop for i upto 10
       collect i)
 (loop for i from 0 downto -10
       collect i)
 (loop repeat 3
       do (format t "done~%"))

)


;; loops over collections and packages

(comment

 ;; over items
 (loop for i in (list 10 20 30 40)
       collect i)
 (loop for i in (list 10 20 30 40)
       by #'cddr
       collect i)

 ;; over cons cells
 (loop for i on (list 10 20 30 40)
       collect i)
 (loop for x on (list 10 20 30 40)
       by #'cddr
       collect x)

 ;; over vectors
 (loop for x across "abcd" collect x)

 ;; over hash maps
 (defparameter h (make-hash-table))
 (setf (gethash :f h) :foo)
 (setf (gethash :b h) :bar)
 (setf (gethash :q h) :quux)

 (loop for k being the hash-keys in h using (hash-value v)
       do (format t "~a => ~a~%" k v))

)


;; equals-then iteration

(comment

 ;; evaluates forms and binds variables one by one
 (loop repeat 5
       for x = 0 then y
       for y = 1 then (+ x y)
       collect y)

 ;; all forms evaluated before binding
 (loop repeat 5
       for x = 0 then y
       and y = 1 then (+ x y)
       collect y)

)

;; local variables

(comment

 (loop repeat 5
       with y = 1
       collect (incf y))

)


;; destructuring variables

(comment

 (loop for (a b) in '((1 2) (3 4) (5 6))
       do (format t "a: ~a; b: ~a~%" a b))

)


;; value accumulation

(comment

 (defparameter *random* (loop repeat 100 collect (random 10000)))

 (loop for i in *random*
       counting (evenp i) into evens
       counting (oddp i) into odds
       summing i into total
       maximizing i into max
       minimizing i into min
       finally (return (list min max total evens odds)))

)


;; unconditional execution

(comment

 (loop for i from 1 to 10 do (print i))

 (loop for i from 1 to 10 do (return i))

)


;; conditional execution

(comment

 (loop for i from 1 to 10 do (when (evenp i) (print i)))

 (loop for i from 1 to 10 when (evenp i) sum i)

)


;; setup and teardown

(comment

 ;; loops can also be named for non-local exits

 (loop named a-loop repeat 3
       initially (print "prologue")
       finally (progn
                 (print "epilogue")
                 (return-from a-loop))
       do (print "body"))

)


;; termination tests

(comment

 (if (loop for n in '(2 4 6) always (evenp n))
     (print "All numbers even."))

 (if (loop for n in '(2 4 6) never (oddp n))
     (print "All numbers even."))

 (loop for char across "abc123" thereis (digit-char-p char))
 (loop for char across "abcdef" thereis (digit-char-p char))

)
