(load "common")


;; condition can represent any event of interest to code at different levels
;; on the call stack; e.g. it may be used to emit errors or warnings while
;; allowing code higher on the call stack to decide what to do

;; the condition system splits the responsibilities into three parts ---
;; signaling a condition, handling the condition, and restarting; e.g.,
;; several variants of recovery code can be put in a low-level function,
;; leaving the decision on particular recovery strategy to a high-level
;; function


;; conditions

(comment

 ;; a condition is an object whose class indicates the general nature of the
 ;; condition, and whose data carries information about the details of the
 ;; particular circumstances that lead to this condition

 (define-condition 'malformed-log-entry-error (error)
   ((text :initarg :text :reader text)))

)


;; condition handlers

(comment

 (defclass log-entry () (text))
 (defun well-formed-log-entry-p (text) t)

 ;; error conditions are signaled with `error' (that uses `signal' under the
 ;; hood)

 (defun parse-log-entry (text)
   (if (well-formed-log-entry-p text)
       (make-instance 'log-entry text)
       (error 'malformed-log-entry-error :text text)))

 ;; to avoid landing in the debugger, you must establish a condition handler;
 ;; innermost applicable handler will be used

 ;; if the handler returns normally, the condition is not handled and `signal'
 ;; will search for the next applicable handler; to handle the condition,
 ;; handler must transfer control out of `signal' via nonlocal exit

 ;; usually condition handlers simply want to unwind the stack to the place
 ;; where they are established and run some code; it is what `handler-case' is
 ;; for

 (defun parse-log-file (file)
   (with-open-file (in file :direction :input)
     (loop for text = (read-line in nil nil) while text
           for entry = (handler-case (parse-log-entry text)
                         ;; (condition-type (var) code)
                         (malformed-log-entry-error () nil))
           ;; entry parser returns nils on errors, which won't be collected
           when entry collect it)))

 ;; but `parse-log-file' is too low-level to decide on the error recovery
 ;; strategy; placing the handler in higher-level function will unwind the
 ;; stack all the way to this function, the parsing will be stopped with no
 ;; way to recover from the error (e.g., skipping the malformed entry like it
 ;; was done earlier)

)


;; restarts

(comment

 ;; so there should be a way to provide the recovery strategy in low-level
 ;; functions without actually deciding right there which one to use

 ;; restarts are (lower-level) code that actually recovers from errors, and
 ;; (higher-level) condition handlers handle conditions by invoking an
 ;; appropriate restart; so restarts can be defined in mid- and lower-level
 ;; functions, and condition handlers --- on the upper levels of the
 ;; application

 ;; restarts are defined with `restart-case'

 (defun parse-log-file (file)
   (with-open-file (in file :direction :input)
     (loop for text = (read-line in nil nil) while text
           for entry = (restart-case (parse-log-entry text)
                         ;; (restart-name (var) code)
                         (skip-log-entry () nil))
           when entry collect it)))

 ;; having a error with restart without a handler will land you in the
 ;; debugger, and `skip-log-entry' will be among the available restarts there,
 ;; allowing to skip the entry manually

 ;; a condition handler established higher up the call stack can select and
 ;; invoke the desired restart automatically; it is done with `handler-bind'

 (defun analyze-log (log)
   (handler-bind (;; (condition-type handler-fn-of-one-arg)
                  (malformed-log-entry-error  #'(lambda (condition)
                                                  (invoke-restart
                                                   'skip-log-entry))))
     (dolist (entry
              ;; may signal a 'malformed-log-entry-error condition
              (parse-log-file log))
       (analyze-entry entry))))

 ;; a named restart function can be used instead of lambda

 (defun skip-log-entry (c)
   (invoke-restart 'skip-log-entry))

 ;; or guarding against the 'malformed-log-entry-error signaled from code that
 ;; does not define 'skip-log-entry restart (so `control-error' will not be
 ;; signaled)

 (defun skip-log-entry (c)
   (let ((restart (find-restart 'skip-log-entry)))
     (when restart (invoke-restart restart))))

)


;; multiple restarts

;; also defined with `restart-case'

(comment

 ;; moving restarts to the lowest possible level

 (defun parse-log-entry (text)
   (if (well-formed-log-entry-p text)
       (make-instance 'log-entry text)
       (restart-case (error 'malformed-log-entry-error :text text)
         ;; (restart-name (var) code)
         (use-value (value) value)
         ;; (restart-name (var) code)
         (reparse-entry (fixed-text) (parse-log-entry fixed-text)))))

 (defun parse-log-file (file)
   (with-open-file (in file :direction :input)
     (loop for text = (read-line in nil nil) while text
           for entry = (parse-log-entry text)
           when entry collect it)))

 ;; and handler --- to the high enough level to decide what restart to invoke

 (defclass malformed-log-entry () (text))

 (defun analyze-log (log)
   (handler-bind ((malformed-log-entry-error
                    #'(lambda (condition)
                        (use-value (make-instance 'malformed-log-entry
                                                  :text text)))))
     (dolist (entry
              ;; may signal a 'malformed-log-entry-error condition
              (parse-log-file log))
       (analyze-entry entry))))

)

;; different conditions can define a different handling protocols ---
;; e.g. errors invoke the debugger when not handled (when `signal' returns),
;; but warnings do not (they establish restarts that allow the caller to
;; proceed, possibly printing the warning)

;; some standard conditions are `error', `cerror', `warn'
