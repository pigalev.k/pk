(load "packages")

(in-package :hello-lisp.practical-common-lisp.pathnames)
(load "common")


;; pathname abstraction provides quite portable way to represent and
;; manipulate filenames, but the actual interaction with file systems is not
;; quite as portable; so there is need for a consistent interface for the most
;; common file system operations across different implementations of Common
;; Lisp


;; the API

;; - get a list of files in a directory

;; - determine whether a file or directory with a given name exists

;; - recursively walk a directory hierarchy, calling a given function for each
;;   pathname in the tree

;; in theory, directory listing and file existence operations are already
;; provided by the standard functions `directory' and `probe-file', but
;; behavior varies between implementations nevertheless


;; features and read-time conditionals

;; each symbol represent a feature that is present in the implementation or
;; underlying platform; at least one symbol that indicates the current
;; implementation should be present

*features*

;; feature expressions evaluate to true or false depending on the features
;; present; the simplest expression is the feature symbol name

;; reader recognizes feature expressions in the special syntax: #+ and #-;
;; the next form is read only if the feature expression after the #+ is true
;; (or the one after the #- is false)

(defun foo ()
  #+allegro (format nil "allegro")
  #+sbcl (format nil "sbcl")
  #+clisp (format nil "clisp")
  #+cmu (format nil "cmu")
  #-(or allegro sbcl clisp cmu) (error "unknown imlementation"))


(comment

 (foo)

)


;; listing a directory

(defun component-present-p (value)
  "Returns true if pathname component VALUE is present."
  (and value (not (eql value :unspecific))))

(defun directory-pathname-p (pathname)
  "Returns true if PATHNAME represents a directory."
  (and (not (component-present-p (pathname-name pathname)))
       (not (component-present-p (pathname-type pathname)))
       pathname))

(defun pathname-as-directory (name)
  "Convert NAME to directory form."
  (let ((pathname (pathname name)))
    (when (wild-pathname-p pathname)
      (error "Can't reliably convert wild pathnames."))
    (if (not (directory-pathname-p pathname))
        (make-pathname :directory (append (or (pathname-directory pathname)
                                              (list :relative))
                                          (list (file-namestring pathname)))
                       :name nil
                       :type nil
                       :defaults pathname)
        pathname)))

(defun directory-wildcard (dirname)
  "Returns DIRNAME with wildcard file name and type."
  (make-pathname :name :wild
                 :type #-clisp :wild #+clisp nil
                 :defaults (pathname-as-directory dirname)))

#+clisp
(defun clisp-subdirectories-wildcard (wildcard)
  "Returns Clisp-specific directory pathname WILDCARD to select its
subdirectories."
  (make-pathname :directory (append (pathname-directory wildcard)
                                    (list :wild))
                 :name nil
                 :type nil
                 :defaults wildcard))

(defun list-directory (dirname)
  "Returns the list of files in DIRECTORY."
  (when (wild-pathname-p dirname)
    (error "Can only list concrete directory names."))
  (let ((wildcard (directory-wildcard dirname)))

    #+(or sbcl cmu lispworks)
    (directory wildcard)

    #+openmcl
    (directory wildcard :directories t)

    #+allegro
    (directory wildcard :directories-are-files nil)

    #+clisp
    (nconc
     (directory wildcard)
     (directory (clisp-subdirectories-wildcard wildcard)))

    #-(or sbcl cmu lispworks openmcl allegro clisp)
    (error "list-directory not implemented")))


(comment

 (list-directory ".")
 (list-directory "/var/log/")

)


;; testing a file's existence

(defun pathname-as-file (name)
  "Returns NAME in file form."
  (let ((pathname (pathname name)))
    (when (wild-pathname-p pathname)
      (error "Can't reliably convert wild pathnames."))
    (if (directory-pathname-p pathname)
        (let* ((directory (pathname-directory pathname))
               (name-and-type (pathname (first (last directory)))))
          (make-pathname :directory (butlast directory)
                         :name (pathname-name name-and-type)
                         :type (pathname-type name-and-type)
                         :defaults pathname))
        pathname)))

(defun file-exists-p (pathname)
  "Returns an equivalent pathname if the file represented by PATHNAME exists."
  #+(or sbcl lispworks openmcl)
  (probe-file pathname)

  #+(or allegro cmu)
  (or (probe-file (pathname-as-directory pathname))
      (probe-file pathname))

  #+clisp
  (or (ignore-errors
       (probe-file (pathname-as-file pathname)))
      (ignore-errors
       (let ((directory-form (pathname-as-directory pathname)))
         (when (ext:probe-directory directory-form)
           directory-form))))

  #-(or sbcl cmu lispworks openmcl allegro clisp)
  (error "file-exists-p not implemented"))

(comment

 (file-exists-p "./15-practical-a-portable-pathname-library.lisp")
 (file-exists-p "./foo/")

)


;; walking a directory tree

(defun walk-directory (dirname fn &key directories (test (constantly t)))
  "Walks all files and subdirectories in DIRNAME, recursively, and applies FN to
each file for which TEST returns true. If DIRECTORIES is non-nil, tests and
applies FN to directories as well."
  (labels ((walk (name)
             (cond
               ((directory-pathname-p name)
                (when (and directories (funcall test name))
                  (funcall fn name))
                (dolist (x (list-directory name)) (walk x)))
               ((funcall test name) (funcall fn name)))))
    (walk (pathname-as-directory dirname))))

(comment

 (walk-directory "." #'print)

)
