(load "common")


;; CLOS is class-based; instances of built-in classes have opaque
;; representations accessible only via their standard functions, but instances
;; of user-defined classes have named parts --- slots, that hold (but not
;; incapsulate) the internal state of the instance

;; classes are arranged in the single hierarchy, a taxonomy for all objects; a
;; class can be defined as a subclass of other classes, called its
;; superclasses; a class inherits part of its definition from its superclasses
;; and instances of a class are considered instances of the superclasses
;; (inheritance); a single class can have multiple direct superclasses; the
;; class T is the single root of the class hierarchy

;; in object-oriented languages behavior is usually associated with classes
;; through methods (member functions) that belong to a particular class; the
;; name of a method and its arguments are passed to the object, whose class
;; determines (using its own definition and the class hierarchy) what code
;; runs (message-passing style, single dispatch); so objects of different
;; types can have different implementations of the same method (polymorphism)

;; polymorphic methods in CLOS are implemented as a new kind of functions ---
;; generic functions


;; generic functions and methods

;; a generic function defines an abstract operation, specifying its name and
;; a parameter list but no implementation; it is generic in the sense that it
;; can (in theory) accept any objects as arguments

(defgeneric draw (shape)
  (:documentation "Draws the given shape on the screen."))

;; the actual implementation(s) of a generic function is provided by methods,
;; one method for each particular class of arguments

;; methods don't belong to classes, but to the generic function, which is
;; responsible for determining which method to run for a particular
;; invocation (dispatch)

;; methods indicate what kinds of arguments they can handle by specializing
;; the required parameters of the generic function; when a generic function
;; is invoked, it compares the actual arguments it was passed with the
;; specializers of each of its methods to find the applicable methods (whose
;; specializers are compatible with the actual arguments)

(defclass shape () ())
(defclass circle (shape) ())
(defclass triangle (shape) ())
(defclass isosceles-triangle (triangle) ())
(defclass polygon (shape) ())

(defmethod draw ((shape shape))
  (format nil "drawing a shape..."))

(defmethod draw ((shape circle))
  (format nil "drawing a circle..."))

(defmethod draw ((shape triangle))
  (format nil "drawing a triangle..."))

(comment

 (draw (make-instance 'shape))
 (draw (make-instance 'circle))
 (draw (make-instance 'triangle))
 (draw (make-instance 'isosceles-triangle))
 (draw (make-instance 'polygon))

 ;; if only one method is applicable, it will handle the invocation; if there
 ;; are multiple applicable methods, they are combined into a single effective
 ;; method that handles the invocation; there are declarative constructs to
 ;; define method combinations

 ;; there are two ways to specialize a parameter --- to specify a class that
 ;; the argument must be an instance of, and to specify a particular object to
 ;; which the method applies (`eql'-specializer); a method can specialize on
 ;; multiple parameters (multiple dispatch)

)


;; generic function example

;; a `bank-account', that have a balance, can be a `checking-account' or a
;; `savings-account'

(defclass bank-account () ((balance :initarg :balance :initform 0)))
(defclass checking-account (bank-account) ())
(defclass savings-account (bank-account) ())

;; a `withdraw' operation is available for any account, that decreases its
;; balance by a specified amount; if the balance is less than the amount, it
;; signals an error and leaves the balance unchanged

(defgeneric withdraw (account amount)
  (:documentation "Withdraws the specified AMOUNT from the ACCOUNT. Returns the
resulting balance. Signals an error if the current balance is less than
AMOUNT."))

;; a method parameter list must be congruent with its generic function's (have
;; the same number of required and optional parameters, and be capable of
;; accepting &rest and &key parameters specified by the generic function)

;; a parameter specialized by class: (parameter-name class-name); parameters
;; not explicitly specialized are implicitly specialized on T and thus do not
;; affect applicability of the method

(defmethod withdraw ((account bank-account) amount)
  (when (< (slot-value account 'balance) amount)
    (error "Account overdrawn."))
  (decf (slot-value account 'balance) amount))

(comment

 (defparameter *acc* (make-instance 'bank-account :balance 100))
 (defparameter *savings-acc* (make-instance 'savings-account :balance 100))

 (withdraw *acc* 30)
 (slot-value *acc* 'balance)
 (withdraw *acc* 200)

 (withdraw *savings-acc* 75)
 (slot-value *savings-acc* 'balance)

)


;; all checking accounts have overdraft protection: each checking account is
;; linked to another bank account that's drawn upon when the balance of the
;; checking account itself can't cover a withdrawal

(defclass checking-account (bank-account)
  ((overdraft-account :initarg :overdraft-account)))

(defmethod withdraw ((account checking-account) amount)
  (let ((overdraft (- amount (slot-value account 'balance))))
    (when (plusp overdraft)
      (withdraw (slot-value account 'overdraft-account) overdraft)
      (incf (slot-value account 'balance) overdraft)))
  (call-next-method))

(comment

 (defparameter *checking-acc*
   (make-instance 'checking-account
                  :balance 200
                  :overdraft-account (make-instance 'savings-account
                                                    :balance 1000)))

 (withdraw *checking-acc* 300)
 (slot-value *checking-acc* 'balance)
 (slot-value (slot-value *checking-acc* 'overdraft-account) 'balance)

 )

;; method's parameters can also have `eql' specializers

;; form for `eql' to compare with is evaluated once, at the time the method is
;; defined

(defparameter *bank* (make-instance 'bank-account :balance 1000000))
(defparameter *account-of-bank-president* (make-instance 'bank-account
                                                         :balance 0))

(defmethod withdraw ((account (eql *account-of-bank-president*)) amount)
  (let ((overdraft (- amount (slot-value account 'balance))))
    (when (plusp overdraft)
      (format t "embezzling funds: ~a~%" overdraft)
      (withdraw *bank* overdraft)
      (incf (slot-value account 'balance) overdraft))
    (call-next-method)))

(comment

 ;; performed by professional embezzlers, don't try this at home

 (withdraw *account-of-bank-president* 700000)
 (slot-value *bank* 'balance)
 (slot-value *account-of-bank-president* 'balance)

)


;; method combination

;; the effective method is built in three steps:

;; - the generic function builds a list of applicable methods (whose
;;   specializers are compatible with actual arguments); when specializer is a
;;   class, it's compatible if it names the argument's class or one of
;;   superclasses; an `eql' specializer is compatible if the argument is `eql'
;;   to the object mentioned in specializer

;; - the list is sorted by specificity of parameter specializers; subclasses
;;   considered more specific than superclasses; an `eql' specializer is more
;;   specific than any class specializer

;; - methods are taken in order from the list and their code is combined; the
;; - standard method combination is used by default


;; the standard method combination

;; the standard method combination combines methods (called primary methods)
;; so that the most specific method runs first, and can call less specific
;; methods using `call-next-method'

;; there also three kinds of auxiliary methods: `:before', `:after', and
;; `:around', defined with corresponding method qualifier after the method
;; name; each kind of auxiliary methods is combined into the effective method
;; differently

;; all applicable (not just most specific) `:before' methods are run before
;; the most specific primary methods, in most-specific-first order; all
;; applicable `:after' methods run after all primary methods in
;; most-specific-last order; `:around' methods are run before anything else,
;; with `call-next-method' leading to the next `:around' method, and in the
;; end to the complex of `:before', `:primary', and `:after' methods (if an
;; `:around' method does not call `call-next-method', all other types of
;; methods will never be called)

;; `:before' methods usually used to perform setup, `:after' to perform
;; teardown/cleanup, and `:around' --- to establish some dynamic context in
;; which the rest of the methods will run (dynamic variable, error handler,
;; etc)


;; other method combinations

;; there are nine other built-in method combinations (simple built-in method
;; combinations); custom method combinations can be defined

;; all simple combinations follow the same pattern: instead of invoking the
;; most specific primary method and letting it invoke less-specific primary
;; methods via `call-next-method', they produce an effective method that
;; contains the code of all the primary methods, one after another (in
;; most-specific-first order by default), all wrapped in a call to the
;; function, macro, or special form that gives the method combination its
;; name; simple combinations support only two kinds of methods --- primary
;; (which are combined) and `:around' (which work as in the standard method
;; combination);

;; for example, `+' combination returns the sum of all the results returned by
;; its primary methods; others apply their own operations to the results
;; (`and' and `or' are short-circuiting)

(defclass task () ())
(defclass urgent-task (task) ())
(defclass low-priority-task (task) ())

(defgeneric priority (job)
  (:documentation "Return the priority at which the job should be run.")
  (:method-combination +
   :most-specific-last))

(defmethod priority + ((job task)) 1)
(defmethod priority + ((job urgent-task)) 10)
(defmethod priority + ((job low-priority-task)) -5)

(comment

 (priority (make-instance 'urgent-task))
 (priority (make-instance 'low-priority-task))

)

;; `define-method-combination' can be used for rare cases when existing
;; combinations do not suffice


;; multimethods

;; multimethods are methods that explicitly specialize more than one of the
;; generic function's required parameters

(defgeneric beat (drum stick)
  (:documentation
   "Produce a sound by hitting the DRUM with the STICK."))

(defclass drum () ())
(defclass snare-drum (drum) ())
(defclass tom-tom (drum) ())
(defclass stick () ())
(defclass wooden-drumstick (stick) ())
(defclass soft-mallet (stick) ())
(defclass brush (stick) ())

(defmethod beat ((drum snare-drum) (stick wooden-drumstick))
  (format t "drr! "))
(defmethod beat ((drum snare-drum) (stick brush))
  (format t "drrsh "))
(defmethod beat ((drum snare-drum) (stick soft-mallet))
  (format t "tr "))
(defmethod beat ((drum tom-tom) (stick wooden-drumstick))
  (format t "umm! "))
(defmethod beat ((drum tom-tom) (stick brush))
  (format t "ummsh "))
(defmethod beat ((drum tom-tom) (stick soft-mallet))
  (format t "ub "))

(comment

 (beat (make-instance 'snare-drum) (make-instance 'brush))
 (beat (make-instance 'snare-drum) (make-instance 'wooden-drumstick))
 (beat (make-instance 'tom-tom) (make-instance 'soft-mallet))

)
