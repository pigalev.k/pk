(load "common")


;; a simple unit-test framework

(defvar *test-name* nil)

(defun report-result (result form)
  "Reports the RESULT as the result of evaluation of FORM. Returns RESULT."
  (format t "~:[FAIL~;pass~] ... ~a: ~a~%" result *test-name* form)
  result)

(defmacro is (form)
  "Reports the result of evaluation of FORM."
  `(report-result ,form ',form))

(defmacro combine-results (&body forms)
  "Returns T if all forms evaluate to T, as with `and'. Does not short-circuit."
  (with-gensyms (result)
    `(let ((,result t))
       ,@(loop for f in forms collect `(unless ,f (setf ,result nil)))
       ,result)))

(defmacro are (&body forms)
  "Reports the results of evaluation of FORMS."
  `(progn
     ,@(loop for f in forms collect `(is ,f))))

(defmacro deftest (name &body body)
  "Defines a test with NAME and BODY containing assertions. Use assertion macros
like `is' and `are' to make assertions."
  `(defun ,name nil
     (let ((*test-name* (append *test-name* (list ',name))))
       ,@body)))

(comment

 (report-result (= (+ 1 2) 3) '(= (+ 1 2) 3))

 (is (= (+ 1 2) 3))

 (combine-results
   (= (+ 1 2) 3)
   (= (+ 2 2) 4)
   (= (+ 1 8) 8))
 (combine-results
   (= (+ 1 2) 3)
   (= (+ 2 2) 4))

 (are
   (= (+ 1 2) 3)
   (= (+ 2 2) 4)
   (= (+ 1 8) 8))
 (are
   (= (+ 1 2) 3)
   (= (+ 2 2) 4)
   (= (+ 1 8) 9))

)


;; defining and running tests: without the test abstraction

(defun test-+-failing ()
  (let ((*test-name* 'test-+-failing))
    (are
      (= (+ 1 2) 3)
      (= (+ 2 2) 4)
      (= (+ 1 8) 8))))
(defun test-+-passing ()
  (are
    (= (+ 1 2) 3)
    (= (+ 2 2) 4)
    (= (+ 1 8) 9)))

(defun test-*-passing ()
  (are
    (= (* 2 2) 4)
    (= (* 3 5) 15)))

(defun test-arithmetic-failing ()
  (combine-results
   (test-+-failing)
   (test-*)))
(defun test-arithmetic-passing ()
  (combine-results
   (test-+-passing)
   (test-*)))

(comment

 (test-+-failing)
 (test-+-passing)
 (test-*-passing)

 (test-arithmetic-failing)
 (test-arithmetic-passing)

)


;; defining and running tests: with test abstraction

(deftest test-+-failing*
    (are
      (= (+ 1 2) 3)
      (= (+ 2 2) 4)
      (= (+ 1 8) 8)))
(deftest test-+-passing*
  (are
    (= (+ 1 2) 3)
    (= (+ 2 2) 4)
    (= (+ 1 8) 9)))

(deftest test-*-passing*
  (are
    (= (* 2 2) 4)
    (= (* 3 5) 15)))

(deftest test-arithmetic-failing*
  (combine-results
    (test-+-failing*)
    (test-*-passing*)))
(deftest test-arithmetic-passing*
  (combine-results
    (test-+-passing*)
    (test-*-passing*)))

(comment

 (test-+-failing*)
 (test-+-passing*)
 (test-*-passing*)

 (test-arithmetic-failing*)
 (test-arithmetic-passing*)

)
