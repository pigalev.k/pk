
;; common helpers

(defmacro comment (&rest body)
  "Comments out BODY of one or more expressions."
  (declare (ignore body))
  nil)

(defmacro with-gensyms ((&rest names) &body body)
  "Evaluates BODY with NAMES bound to a new generated symbol each."
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))
