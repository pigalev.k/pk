(load "common")


;; cons cells can be used not only for lists; trees, sets, lookup tables, etc
;; can be built also


;; trees

;; tree structure can be represented as list of lists; it is traversed by
;; following both `car's and `cdr's as long as they point to other cons cells

(comment

 (defparameter *tree* '(1 (2 3) 4 (5 (6 (7)))))

 ;; shallow copy
 (copy-list *tree*)

 ;; deep copy
 (copy-tree *tree*)

 (tree-equal *tree* *tree*)
 (subst -6 6 *tree*)
 (subst-if -1 #'(lambda (x)
                  (and (integerp x) (oddp x)))
           *tree*)

)


;; sets

(comment

 (defparameter *set* (adjoin 12 '(1 2 3 -2)))

 ;; do not allow duplicates
 (adjoin 12 *set*)
 (pushnew 12 *set*)

 ;; return the cons cell of the requested element, if any
 (member -2 *set*)
 (member-if #'oddp *set*)

 (intersection *set* '(2 3 11))
 (union *set* '(2 3 11))
 (set-difference *set* '(2 3 11))

 (subsetp '(2 3) *set*)

)


;; lookup tables: alists and plists

(comment

 ;; association list (alist) maps keys to values and supports reverse lookups;
 ;; it is a sequence of mappings (pairs) from key to value; new mappings can
 ;; shadow and unshadow previous mappings for the same key, following stack
 ;; discipline

 (defparameter *alist* '((a . 1) (b . 2) (c . 3)))

 ;; returns a whole association (entry)
 (assoc 'a *alist*)
 (cdr (assoc 'a *alist*))
 ;; uses `eq' for key comparison
 (assoc 'a *alist* :test #'eq)
 (assoc-if #'(lambda (x)
               (string-greaterp (symbol-name x) "b"))
           *alist*)
 (rassoc 2 *alist*)
 (rassoc-if #'evenp *alist*)

 (cons (cons 'd 1) *alist*)
 (acons 'd 1 *alist*)
 (setf *alist* (acons 'd 1 *alist*))

 ;; semi-shallow copy (alist structure only)
 (copy-alist *alist*)

 (pairlis '(a b c) '(1 2 3))


 ;; property list (plist) also maps keys to values, but its keys and values go
 ;; in one plain list, alternating

 ;; it is more specialized (less flexible): one general access operator (no
 ;; reverse lookup), that uses `eq' for key comparison (so keys are usually
 ;; symbols)

 (defparameter *plist* '(:a 1 :b 2 :c 3))

 (getf *plist* :b :not-found)
 (getf *plist* :x :not-found)

 ;; modify or add mappings
 (setf (getf *plist* :b) 100)
 (setf (getf *plist* :x) -1)

 ;; remove mappings
 (remf *plist* :b)

 (get-properties *plist* '(:c :x))

 ;; each symbol has it's own plist (metadata)
 (symbol-plist 'foo)

 ;; adding/updating, getting and removing symbol props
 (setf (getf (symbol-plist 'foo) :meta) :present)
 (getf (symbol-plist 'foo) :meta)
 (remf (symbol-plist 'foo) :meta)
 ;; shorter
 (setf (get 'foo :doc) :absent)
 (get 'foo :doc)
 (remprop 'foo :doc)

)


;; `destructuring-bind'

(comment

 (destructuring-bind (x y z) '(1 2 3)
   (list :x x :y y :z z))

 (destructuring-bind (x y z) '(1 (2 20) 3)
   (list :x x :y y :z z))

 (destructuring-bind (x (y1 y2) z) '(1 (2 20) 3)
   (list :x x :y1 y1 :y2 y2 :z z))

 (destructuring-bind (x (y1 &optional y2) z) '(1 (2 20) 3)
   (list :x x :y1 y1 :y2 y2 :z z))

 (destructuring-bind (x (y1 &optional y2) z) '(1 (2) 3)
   (list :x x :y1 y1 :y2 y2 :z z))

 (destructuring-bind (&key x y z) '(:x 1 :y 2 :z 3)
   (list :x x :y y :z z))

 (destructuring-bind (&key x y z) '(:z 1 :y 2 :x 3)
   (list :x x :y y :z z))


 (destructuring-bind
     (&whole form (&whole plist &key x y) a b)
     '((:x 1 :y 2) 3 4)
   (list x y a b plist form))

 (destructuring-bind
     ((&whole plist &optional &key x (y 10)) a b)
     '((:x 1) 3 4)
   (list x y a b plist))

)
