(in-package :cl-user)
(load "common")

;; the Lisp reader translates textual names into Lisp objects (symbols) to be
;; passed to the evaluator

;; we want to avoid clashing symbol names, especially when using third-party
;; libraries; the solution is to use namespaces for symbols

;; in Common Lisp, namespaces are implemented by controlling how the reader
;; translates textual names into symbols


;; reader and packages

(comment

 ;; a package is kind of a table that maps strings to symbols

 ;; the current package
 *package*

 (find-package :cl)

 ;; it is accessed and changed by `find-symbol' and `intern'

 (find-symbol "+")
 (find-symbol "+" :cl)

 (intern "foo")

 ;; each time the reader reads the same name in the same package, it returns
 ;; the same symbol; symbols are compared using object identity

 (eq 'foo 'foo)

 ;; names that contain no colons are unqualified

 (defparameter foo 1)
 foo

 ;; names that contain one colon (package-qualified) refer to external symbols
 ;; (that the package exports for public use); the package-qualified name is
 ;; split on the colon, the first part is used as a package name, the second
 ;; as a symbol name

 cl:*package*
 #'cl:+

 ;; names with double-colons (also package-qualified) refer to any symbol from
 ;; the package

 cl::t

 ;; names starting with colon refer to keyword symbols, that are interned in
 ;; the package "KEYWORD" and automatically exported; also a constant variable
 ;; is defined with the symbol name both as name and value

 :foo

 ;; names starting with #: refer to uninterned symbols (not present in any
 ;; package)

 '#:bar
 (gensym)

)


;; accessing symbols in packages

(comment

 ;; all symbols that can be found in a given package (with `find-symbol') are
 ;; accessible in that package

 ;; accessible symbol may be present in the current package (its home package)
 ;; or inherited from some other package (used by the current package); only
 ;; external symbols are inherited; a symbol is made external by exporting it

 (defparameter foo 1)
 (find-symbol "foo")
 (find-symbol ">" :cl)

 ;; only one symbol with given name can be accessible in given package;
 ;; conflicts can be resolved by making one of the symbols a shadowing symbol,
 ;; that makes the other symbols of the same name inaccessible

 ;; an existing symbol can be imported into another package; thus, the same
 ;; symbol can be present in multiple packages

 ;; a present symbol can be uninterned from a package

)


;; standard packages

(comment

 ;; the default package
 (find-package :common-lisp-user)

 ;; the core Common Lisp package
 (find-package :common-lisp)
 (find-package :cl)

 ;; the keywords package
 (find-package :keyword)

)


;; loading third-party packages

;; load systems using Quicklisp package manager

(ql:quickload :alexandria)


;; defining packages

;; `defpackage' creates the package, specifies what packages it uses, what
;; symbols it exports, and what symbols it imports from other packages; also
;; resolves name conflicts by creating shadowing symbols

(defpackage :hello-lisp.practical-common-lisp.packages-and-symbols
  ;; inherits all symbols from :common-lisp package, by nickname
  (:use :cl)
  (:export :hello)
  ;; not in the standard, but widely supported
  (:local-nicknames (:a :alexandria)))

;; sets the current package for interning and searching symbols

(in-package :hello-lisp.practical-common-lisp.packages-and-symbols)

*package*

(defun hello ()
  (format t "Hello from a new package!~%"))

(comment

 ;; package and symbol names are specified with name designators --- strings,
 ;; symbols or keywords; strings should be uppercase, symbols need not to be

 ;; same as above

 (defpackage "HELLO-LISP.PRACTICAL-COMMON-LISP.PACKAGES-AND-SYMBOLS"
   ;; inherits all symbols from :common-lisp package
   (:use "COMMON-LISP"))

 ;; using internal symbols

 (defparameter foo 1)
 foo
 hello-lisp.practical-common-lisp.packages-and-symbols::foo
 (eq 'foo 'hello-lisp.practical-common-lisp.packages-and-symbols::foo)

 (hello)

 ;; using external (exported) symbols

 (a:flatten '(a (b c) d (e (f (g)))))

 (in-package :cl-user)
 (hello-lisp.practical-common-lisp.packages-and-symbols:hello)
 (hello-lisp.practical-common-lisp.packages-and-symbols::hello)

)


;; packaging mechanics

;; package definitions must be `load'ed before the packages can be used (by
;; `load' or `compile-file'); it can be made manually or using a system
;; definition facility (e.g., ASDF)

;; package name must be globally unique; Java-style package names (prefixed by
;; reversed domain name) are often used

;; one `in-package' form per file is recommended
