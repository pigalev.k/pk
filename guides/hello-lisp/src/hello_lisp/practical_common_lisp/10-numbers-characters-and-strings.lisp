(load "common")


;; data types are defined by functions that operate on them (data abstraction);
;; built-in data types often also have a special read/print syntax


;; numbers

;; integers of arbitrary precision, ratios, floating-point numbers, complex
;; numbers

(comment

 ;; the same object when read, so printed identically
 10
 20/2
 #xa

 ;; different bases
 #b1100
 #b11/101
 #o77
 #xDADA
 #36rABCDEFGHIJKLNMOPQRSTUVWXYZ

 ;; rational numbers (ratios)
 -2/3

 ;; floating-point numbers --- short, single, double, long
 123.0
 123e-3
 1e0
 1s0
 1d0
 1l0

 ;; complex numbers
 #c(2 1) ; cannot evaluate this in the buffer, but can in the REPL (some
         ; SLIME/SLY quirk, maybe --- reads the form as a list, without #C)
 (complex 23 -1)

)


;; basic math

(comment

 ;; nothing unusual there... maybe there is some fun in complex numbers?

 (+ #c(1 2) #c(3 4) #c(1.2 9))

 ;; floating-point and complex numbers are contagious; "smaller" representations
 ;; are converted to "larger" ones, as usual

 (floor 1.2)
 (ceiling 1.2)
 (truncate 1.2)
 (round 1.2)

 ;; mathematical equivalence
 (= 1 1.0 #c(1 0) #c(1.0 0.0))
 (/= 1 2 3.0 #c(3 4))

)


;; characters

;; characters are not numbers

(comment

 #\x
 #\space
 #\newline

 (char= #\x #\x)
 (char/= #\x #\X)
 (char< #\a #\x)
 (char<= #\a #\x)

 (char-equal #\x #\X)
 (char-not-equal #\x #\s)
 (char-lessp #\a #\X)
 (char-greaterp #\x #\A)
 (char-not-lessp #\X #\a)
 (char-not-greaterp #\A #\x)

)


;; strings

(comment

 (string= "foo" "foo")
 (string/= "Foo" "foo")

 (string-equal "foo" "Foo")
 (string-not-equal "foo" "bar")

 ;; the rest is the same as with characters

)
