(load "common")


;; variables are named places that can hold a value

;; variables are not typed but values are; Common Lisp is a strongly dynamically
;; typed language

;; all variables are references to objects; a variable can have many different
;; bindings across the time; nested bindings shadow each other

;; variable can have dynamic scope (special variables) and lexical (static,
;; textual) scope

(comment

 ;; can be unbound; once bound, will not be rebound again on source reload
 (defvar *count* 0
   "Counts everything it sees.")

 (setf *count* 12)

 ;; always assigns the initial value, will be always rebound on source reload
 (defparameter *tolerance* 0.001
   "As you can see, the tolerance is quite short.")

 (setf *tolerance* 0.1)

 ;; always assigns the value, cannot be rebound
 (defconstant +g+ 9.81
   "Some physical constant.")

 (setf +g+ 10)

)

;; there are several types of bindings (binding forms) --- function argument
;; list, `let' form etc; they all produce lexically scoped variables by default

(comment

 (defun square (n) (* n n))

 n

 (let ((x 12))
   (square x))

 x

 ;; `x' is not defined outside of `let', and `n' --- outside of `square'; a
 ;; lexical binding can be referenced by any code defined within the binding
 ;; form


 ;; when a dynamic variable is bound (in `let' or function parameters), its
 ;; current (global or rebound) value is replaced with the new value until the
 ;; binding form is exited; then its previous value is restored (stack
 ;; discipline)

 (let ((*tolerance* 0.1))
   *tolerance*)

 *tolerance*

 ;; a dynamic binding can be referenced by any code invoked during the execution
 ;; of the binding form

 (defvar *x* 10)

 (defun sayx () (format t "X: ~d~%" *x*))
 (defun sayx* ()
   (sayx)
   (let ((*x* 30))
     (sayx))
   (sayx))
 (defun sayx** ()
   (sayx)
   (let ((*x* 40))
     ;; changes the current binding
     (setf *x* (1+ *x*))
     (sayx))
   (sayx))

 (sayx)
 (let ((*x* 12))
   (sayx))
 (sayx)

 (sayx*)

 (sayx**)

)

;; the function that retain access to its (definition time) lexical scope (its
;; bindings) at run time is a closure

(comment

 (defparameter *fn*
   (let ((count 0)) #'(lambda ()
                        (setf count (1+ count)))))

 (funcall *fn*)
 (funcall *fn*)
 (funcall *fn*)

)


;; variables can be accessed (dereferenced) and set (assigned); a variable is
;; accessed by mentioning its name; `setf' is a general-purpose assignment
;; operator (can assign to generalized variables, i.e., different places)

(comment

 (defparameter *x* 0)
 (defparameter *y* 0)

 (setf *x* 1
       *y* 2)

 *x*
 *y*

 ;; specialized variants of `setf' (modify macros)

 (incf *x*)
 (decf *x*)

 (defparameter *list* '())
 (push 1 *list*)
 (pop *list*)

 (rotatef *x* *y*)
 (shiftf *x* *y* 10)

)
