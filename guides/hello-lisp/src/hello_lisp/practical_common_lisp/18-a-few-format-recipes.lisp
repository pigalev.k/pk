(load "common")


;; `format' and `loop' are the two most prominent examples of mini-languages
;; written in Lisp

;; `format' supports three different kinds of formatting: printing tables of
;; data, pretty-printing S-expressions, and generating human-readable messages
;; with interpolated values; mostly the latter will be considered here

(comment

 ;; the destination can be `t' (standard output stream), `nil' (a new string
 ;; that will be returned), stream (to which output will be written), or an
 ;; existing string with a fill pointer (to which output will be added)

 ;; the control string is a program in the `format' language --- a sequence of
 ;; directives; it's character-based (not S-expression-based), and optimized
 ;; for compactness

 ;; most directives interpolate an argument into the output in some way; some
 ;; directives don't consume any arguments, and some consume more than one;
 ;; arguments not consumed by the control string are silently ignored


 ;; directives

 ;; all directives start with a tilde (~) and end with a single character that
 ;; identifies the directive, case-insensitive; some directives take
 ;; positional prefix parameters (immediately following the tilde), separated
 ;; by commas; values of the parameters are either decimal numbers, or
 ;; characters (single quote followed by the desired character)

 ;; floating-point number
 (format nil "~$" pi)
 ;; digits after the decimal point
 (format nil "~5$" pi)
 ;; consume an argument and use it instead of 'v'
 (format nil "~v$" 3 pi)
 ;; use the number of remaining arguments instead of '#'
 (format nil "~#$" pi)

 ;; omitted (empty) parameter values must still be separated by commas

 ;; floating-point number, with digits after the decimal point
 (format nil "~,5f" pi)

 ;; : and @ modifiers can be placed after any parameters and before the
 ;; directive's identifying character

 (format nil "~d" 1000000)
 ;; insert group separators in the number
 (format nil "~:d" 1000000)
 ;; insert a plus sign if number if positive
 (format nil "~@d" 1000000)
 ;; or both (may differ in different directives)
 (format nil "~:@d" 1000000)


 ;; basic formatting

 ;; ~a (aesthetic, i.e., human-readable)
 (format nil "The value is: ~a" 10)
 (format nil "The value is: ~a" "foo")
 (format nil "The value is: ~a" :foo)
 (format nil "The value is: ~a" (list 1 2 3))

 ;; ~s (machine-`read'able)
 (format nil "The value is: ~s" 10)
 (format nil "The value is: ~s" "foo")
 (format nil "The value is: ~s" :foo)
 (format nil "The value is: ~s" (list 1 2 3))

 ;; both ~a and ~s take up to 4 prefix parameters (for padding); at modifier
 ;; determines the side of the padding, colon modifier means to emit `nil' as
 ;; ()
 (format nil "~3:@a" nil)

 ;; ~% (newline) and ~& (fresh line --- newline if not already at the
 ;; beginning of a line)
 (format nil "abc~&~%")
 ;; number of newlines (or maybe 1 less with fresh line directive)
 (format nil "~10%")

 ;; ~~ (literal tilde)
 (format nil "~12~")


 ;; characters and integers

 ;; ~c (characters)
 (format nil "~c" #\X)
 ;; nonprinting
 (format nil "~:c" #\tab)
 ;; character literal
 (format nil "~@c" #\?)
 ;; control key (not implemented everywhere)
 (format nil "~:c" #\ )

 ;; ~d, ~x, ~o, ~b (integers)
 (format nil "~d" 1000000)
 (format nil "~:d" 1000000)
 (format nil "~@d" 1000000)
 (format nil "~:@d" 1000000)
 ;; minimum width
 (format nil "~12d" 1000000)
 ;; padding character
 (format nil "~12,'-d" 1000000)
 ;; group separator and size
 (format nil "~,,'_,4:d" 100000000)
 ;; base 16
 (format nil "~x" 1000000)
 ;; base 8
 (format nil "~o" 1000000)
 ;; base 2
 (format nil "~b" 1000000)

 ;; ~f, ~e, ~g, ~$ (floating-point numbers)
 ;; may use scientific notation if needed
 (format nil "~f" pi)
 ;; always with a sign
 (format nil "~@f" pi)
 ;; fixed width
 (format nil "~5,2f" pi)
 ;; digits after the decimal point
 (format nil "~,4f" pi)
 ;; always use scientific notation
 (format nil "~e" pi)
 (format nil "~,4e" pi)
 ;; for tabular output
 (format nil "~g" 1234)
 ;; monetary (same as ~,2f by default)
 (format nil "~$" pi)
 ;; digits after the decimal point and minimum digits before the decimal point
 (format nil "~2,4$" pi)


 ;; english language

 ;; ~r (english words, roman numerals)
 ;; cardinals
 (format nil "~r" 1234)
 ;; ordinals
 (format nil "~:r" 1234)
 ;; roman numerals
 (format nil "~@r" 1234)
 ;; old-style roman numerals (IIII and VIIII instead of IV and IX)
 (format nil "~:@r" 1234)

 ;; ~p (pluralized)
 ;; produces 's' if argument is not 1
 (format nil "file~p" 1)
 (format nil "file~p" 10)
 (format nil "file~p" 0)
 ;; produces 'y' or 'ies'
 (format nil "famil~@p" 1)
 (format nil "famil~@p" 10)
 (format nil "famil~@p" 0)
 ;; reconsume the previous argument (already consumed by another directive)
 (format nil "~r famil~:@p" 1)
 (format nil "~r famil~:@p" 10)
 (format nil "~r famil~:@p" 0)

 ;; ~( and ~) (case)
 ;; lowercase
 (format nil "~(~a~)" "FOO")
 (format nil "~(~@r~)" 124)
 (format nil "~(~a~)" "tHe Quick BROWN foX")
 ;; capitalize the first word, lowercase others
 (format nil "~@(~a~)" "tHe Quick BROWN foX")
 ;; capitalize all words
 (format nil "~:(~a~)" "tHe Quick BROWN foX")
 ;; uppercase
 (format nil "~:@(~a~)" "tHe Quick BROWN foX")


 ;; conditional formatting

 ;; ~[ and ~] (conditional)
 (format nil "~[cero~;uno~;dos~]" 0)
 (format nil "~[cero~;uno~;dos~]" 1)
 (format nil "~[cero~;uno~;dos~]" 2)
 (format nil "~[cero~;uno~;dos~]" 3)
 ;; default value in last clause
 (format nil "~[cero~;uno~;dos~:;mucho~]" 3)
 (format nil "~[cero~;uno~;dos~:;mucho~]" 100)
 ;; select clause by prefix parameter
 (format nil "~#[cero~;uno~;dos~:;mucho~]" 1 1)
 (defparameter *list-etc*
   "~#[NONE~;~a~;~a and ~a~:;~a, ~a~]~#[~; and ~a~:;, ~a, etc~].")
 (format nil *list-etc*)
 (format nil *list-etc* 'a)
 (format nil *list-etc* 'a 'b)
 (format nil *list-etc* 'a 'b 'c)
 (format nil *list-etc* 'a 'b 'c 'd)
 (format nil *list-etc* 'a 'b 'c 'd 'e)
 ;; only two clauses, consumes a single argument and selects the first clause
 ;; if it is `nil' and second clause otherwise
 (format nil "~:[FAIL~;pass~]" nil)
 (format nil "~:[FAIL~;pass~]" t)
 ;; one clause, consumes one argument and, if it's not `nil', unconsumes the
 ;; argument and processes the clause with it
 (format nil "~@[x = ~a ~]~@[y = ~a~]" 10 20)
 (format nil "~@[x = ~a ~]~@[y = ~a~]" 10 nil)
 (format nil "~@[x = ~a ~]~@[y = ~a~]" nil 20)
 (format nil "~@[x = ~a ~]~@[y = ~a~]" nil nil)


 ;; iteration

 ;; ~{ and ~} (iteration over elements of a list); consumes a list and uses
 ;; the text within the directive as a control string to format elements of
 ;; the list, repeatedly, one by one
 (format nil "~{~a, ~}" (list 1 2 3))
 ;; ~^ in the directive body causes iteration to stop immediately, without
 ;; processing the rest of the control string, when no elements remain in the
 ;; list
 (format nil "~{~a~^, ~}" (list :a :b :c))
  ;; each element must itself be a list
 (format nil "~:{~a ~a ~a ~}" '((:a :b :c) (:d :e :f) (:g :h :i)))
 ;; ~* in the directive body skips or backs up over the items in the list
 (format nil "~:{~a ~*~a ~}" '((:a :b :c) (:d :e :f) (:g :h :i)))
 ;; processes the remaining arguments as a list
 (format nil "~@{~a~^, ~}" 1 2 3)
 ;; processes the remaining arguments as a list of sublists
 (format nil "~:@{~a ~a ~a ~}" '(:a :b :c) '(:d :e :f) '(:g :h :i))
 ;; limits number of iterations
 (format nil "~2{~a~^, ~}" (list 1 2 3))
 ;; # in the directive body refers to the number of items remaining to be
 ;; processed in the list
 (format nil "~{~a~#[~;, and ~:;, ~]~}" (list 1 2 3))


 ;; jumps

 ;; ~* (jump around in the list of arguments)
 ;; skips the next argument
 (format nil "~a ~* ~a" 1 2 3)
 ;; unconsume the argument consumed by the previous directive
 (format nil "~r ~:*(~d)" 1)
 (format nil "I saw ~r~:* el~[ves~;f~:;ves~]." 0)
 (format nil "I saw ~r~:* el~[ves~;f~:;ves~]." 1)
 (format nil "I saw ~r el~:*~[ves~;f~:;ves~]." 2)


 ;; TODO

 ;; ~? (substitutes the directive by the next argument)
 ;; ~/ (custom directives)
 ;; pretty-printing directives
 ;; tabular output directives

)
