(load "common")


;; list is a linked sequential data structure fit to represent any kind of
;; heterogeneous and/or hierarchical data; it's main use is to represent code

;; lists are built from cons cells --- pairs of values (`car' and `cdr')
;; created with `cons' --- linked together; the element of a list is held in
;; the `car', and link to the next cons cell is held in the `cdr'; the last
;; cell in the chain has a `cdr' of nil (the empty list)

;; so a list is either nil or a cons cell with the first element of the list
;; in the `car' and the another list in the `cdr'

;; because lists can have other lists as elements, they can be used to
;; represent trees of arbitrary depth and complexity

;; list can share structure with other lists (e.g. we can create a new list by
;; creating a cons cell and using another list as `cdr')

;; destructive operations may be side-effecting (like `setf') or recycling
;; (like `nreverse', that returns a new list made of old cons pairs in-place
;; and makes original object unusable in the process)

;; shared structure and destructive operations do not play well with each
;; other; but sometimes destructive operations can be safely confined in a
;; non-destructive function; it's best to avoid mutation altogether and only
;; add it as optimization later, if needed, and in controllable (isolated)
;; fashion

(comment

 (defparameter *list* '(1 2 3 4 5 6 7))

 ;; element access (`setf'able)

 ;; plain list functions
 (first *list*)
 (rest *list*)
 (nth 5 *list*)
 (nthcdr 5 *list*)

 ;; tree functions
 (defparameter *tree* '((1 2 (3) 4) 5 6 7))
 (caar *tree*)
 (cadr *tree*)
 (caddar *tree*)
 (cdddr *tree*)

 ;; other list functions
 (last *list*)
 (butlast *list*)
 (ldiff *list* (cdr *list*))
 (tailp (cdr *list*) *list*)
 (list* 1 2 3 nil)
 (make-list 4 :initial-element :a)
 (consp *list*)
 (atom *list*)
 (listp *list*)
 (null '())
 (revappend '(1 2 3) '(4 5 6))
 (append '(1 2 3) '(4 5 6))
 ;; destructive `append'
 (nconc '(1 2 3) '(4 5 6))

 ;; mapping

 ;; maps `car's
 (mapcar #'(lambda (x) (* 2 x)) '(1 2 3))
 (mapcar #'+ '(1 2 3) '(10 20 30))
 ;; maps whole cons cells
 (maplist #'(lambda (x) (* 2 (first x))) '(1 2 3))

 ;; constructs new lists appending function application results with `nconc'
 (defparameter *l1* '(1 2 3))
 (defparameter *l2* '(10 20 30))
 (mapcan #'(lambda (x y) (list (+ x y))) *l1* *l2*)
 (mapcon #'(lambda (x) (list (* 2 (first x)))) *l1*)

 ;; for side-effects
 (mapc #'(lambda (x) (princ x)) '(1 2 3))
 (mapl #'(lambda (x) (princ (first x))) '(1 2 3))

)
