(in-package :cl-user)
(ql:quickload :cl-ppcre)

(defpackage :hello-lisp.practical-common-lisp.pathnames
  (:use :common-lisp)
  (:export
   :list-directory
   :file-exists-p
   :directory-pathname-p
   :file-pathname-p
   :pathname-as-directory
   :pathname-as-file
   :walk-directory
   :directory-p
   :file-p))

(defpackage :hello-lisp.practical-common-lisp.spam-filter
  (:use :cl :hello-lisp.practical-common-lisp.pathnames)
  (:local-nicknames (:pcre :cl-ppcre)))

(defpackage :hello-lisp.practical-common-lisp.binary-parser
  (:use :cl)
  (:export :define-binary-class
           :define-tagged-binary-class
           :define-binary-type
           :read-value
           :write-value
           :*in-progress-objects*
           :parent-of-type
           :current-binary-object
           :+null+))
