(load "common")

(defun hello-world ()
  (format t "hello, world\n"))

(comment

 (hello-world)

)
