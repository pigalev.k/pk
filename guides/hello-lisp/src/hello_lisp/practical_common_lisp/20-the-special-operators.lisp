(load "common")


;; or special forms


;; controlling evaluation

(comment

 (quote a)

 (if t :ok :not-ok)

 (progn
   1 2 3)

)


;; manipulating the lexical environment

(comment

 ;; modifies lexical bindings
 (setq a 111)

 ;; creates new lexical bindings
 (let ((a 1)
       (b 2)
       c)
   (list a b c))

 ;; binding expressions can reference other symbols bound earlier in the same
 ;; `let*'
 (let* ((a 1)
        (b (1+ a))
        c)
   (list a b c))

 ;; the functions can be referenced in the body of `flet' only
 (flet ((len (list)
          (length list)))
   (len '(1 2 3)))

 ;; the functions can also be referenced in their definitions
 (labels ((len (list)
          (if (null list)
              0
              (1+ (len (rest list))))))
   (len '(1 2 3)))

 ;; local macros

 (macrolet ((comment (&rest body) "No comments."))
   (comment 123))

 ;; symbol macros (expand a symbol instead of a list form)
 (symbol-macrolet ((x (+ 1 2 3)))
   x)

)


;; local flow of control

(comment

 (block foo
   1 2 3
   (return-from foo :ok))

 (defparameter x 10)
 (tagbody
    (setq x 3)
  :top
    (print x)
    (decf x)
    (when (> x 0)
      (go :top)))

)


;; unwinding the stack

(comment

 ;; `go', `return' and `return-from' can jump up the stack, unwinding it (but
 ;; not down the stack, after corresponding `tagbody' or `block' have already
 ;; returned); target tags are bound lexically (and can be closed over,
 ;; e.g. to pass the closure down the stack to call later and `return'/`go' to
 ;; the tag)

 (defun foo ()
   (format t "Entering foo~%")
   (block a
     (format t " Entering BLOCK~%")
     (bar #'(lambda () (return-from a)))
     (format t " Leaving BLOCK~%"))
   (format t "Leaving foo~%"))

 (defun bar (fn)
   (format t "  Entering bar~%")
   (baz fn)
   (format t "  Leaving bar~%"))

 (defun baz (fn)
   (format t "   Entering baz~%")
   (funcall fn)
   (format t "   Leaving baz~%"))

 (foo)

 ;; `throw' and `catch' are dynamically bound counterparts of
 ;; `block'/`tagbody'; deprecated by condition system

 ;; a catch tag --- some arbitrary object
 (defparameter *obj* (cons nil nil))

 (defun foo ()
   (format t "Entering foo~%")
   ;; *obj* is evaluated at runtime (is dynamic); rebinding *obj* may cause
   ;; *`throw' to throw to another `catch'
   (catch *obj*
     (format t " Entering CATCH~%")
     (bar)
     (format t " Leaving CATCH~%"))
   (format t "Leaving foo~%"))

 (defun bar ()
   (format t "  Entering bar~%")
   (baz)
   (format t "  Leaving bar~%"))

 (defun baz ()
   (format t "   Entering baz~%")
   ;; no need to pass a closure down the stack, *obj* is in dynamic scope
   (throw *obj* nil)
   (format t "   Leaving baz~%"))

 (foo)

 ;; `unwind-protect' will always execute the cleanup code, even if stack was
 ;; unwound

 (let ((in (open "cds.db")))
   (unwind-protect
        (print (read in))
     ;; will be closed anyway
     (close in)))

)


;; multiple values

(comment

 ;; all but the primary value will be silently discarded if not explicitly
 ;; captured

 ;; returning multiple values

 (values 1 2 3)
 (values-list '(1 2 3))

 ;; capturing multiple values

 (funcall #'+ (values 1 2) (values 3 4))
 (multiple-value-call #'+ (values 1 2) (values 3 4))

 (multiple-value-bind (x y) (values 1 2)
   (+ x y))

 (multiple-value-list (values 1 2))

 ;; `values' is `setf'able
 (defparameter *x* nil)
 (defparameter *y* nil)
 (setf (values *x* *y*) (floor (/ 57 34)))

)


;; `eval-when', `locally', `the', `load-time-value', `progv'

;; maybe later
