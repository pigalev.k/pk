(load "packages")
(in-package :hello-lisp.practical-common-lisp.spam-filter)
(load "common")


;; the spam filter uses word frequencies to compute a probability that the
;; message is spam


;; classification

(defun classify (text)
  "Returns class of the TEXT, one of `:spam', `:ham', or `:unsure'."
  (classification (score (extract-features text))))

(defparameter *max-ham-score* .4)
(defparameter *min-spam-score* .6)

(defun classification (score)
  "Returns class that corresponds to SCORE, on of `:spam', `:ham', or `:unsure'."
  (values
   (cond
     ((<= score *max-ham-score*) :ham)
     ((>= score *min-spam-score*) :spam)
     (:else :unsure))
   score))


;; the feature database and total message type counts

(defun feature-database ()
  "Creates a new feature database."
  (make-hash-table :test #'equal))

(defvar *feature-database* (feature-database)
  "Global feature database.")

(defvar *total-spams* 0
  "Total count of spams seen so far.")

(defvar *total-hams* 0
  "Total count of hams seen so far.")

(defun clear-feature-database ()
  "Clears the global feature database and total type counts."
  (setf *feature-database* (feature-database)
        *total-spams* 0
        *total-hams* 0))


;; feature extraction

(defclass word-feature ()
  ((word :initarg :word
         :accessor word
         :initform (error "Must supply :word")
         :documentation "The word this feature represents.")
   (spam-count :initarg :spam-count
               :accessor spam-count
               :initform 0
               :documentation "Number of spams we have seen this feature in.")
   (ham-count :initarg :ham-count
              :accessor ham-count
              :initform 0
              :documentation "Number of hams we have seen this feature in.")))

(defmethod print-object ((object word-feature) stream)
  (print-unreadable-object (object stream :type t :identity nil)
    (with-slots (word ham-count spam-count) object
      (format stream "~s :hams ~d :spams ~d" word ham-count spam-count))))

(defun intern-feature (word)
  "Returns the feature for the WORD from the feature database, creating a new
feature if necessary."
  (or (gethash word *feature-database*)
      (setf (gethash word *feature-database*)
            (make-instance 'word-feature :word word))))

(defun extract-words (text)
  "Returns a set of words from the TEXT."
  (delete-duplicates
   (cl-ppcre:all-matches-as-strings "[a-zA-Z]{3,}" text)
   :test #'string=))

(defun extract-features (text)
  "Returns a set of features created from words in TEXT."
  (mapcar #'intern-feature (extract-words text)))


;; training functions

;; modifies the feature database and total message type counts

(defun train (text type)
  "Increments the TYPE count of all features in the TEXT as well as the global
count of texts of the TYPE processed so far."
  (dolist (feature (extract-features text))
    (increment-count feature type))
  (increment-total-count type))

(defun increment-count (feature type)
  "Increments the TYPE count of the FEATURE."
  ;; exhaustive case --- signals an error if no clauses match
  (ecase type
    (:ham (incf (ham-count feature)))
    (:spam (incf (spam-count feature)))))

(defun increment-total-count (type)
  "Increments the total count of texts classified as TYPE."
  (ecase type
    (:ham (incf *total-hams*))
    (:spam (incf *total-spams*))))


;; per-word (per-feature) statistics

;; the basic plan: classify a message by extracting the features it contains,
;; computing the individual spam probabilities for all the features, and
;; combining all the individual probabilities into a total score for the
;; message

;; biased: affected by the overall probability that any message will be a spam
;; or a ham

(defun spam-probability* (feature)
  "Returns the probability that a message containing FEATURE is a spam. The
probability is computed as the ratio of the count of spam messages with this
feature to the total count of messages with this feature."
  (with-slots (spam-count ham-count) feature
    (/ spam-count (+ spam-count ham-count))))

;; still biased: doesn't take into account the number of messages analyzed

(defun spam-probability** (feature)
  "Returns the probability that a message containing FEATURE is a spam. The
probability is computed as the ratio of the frequency of spam messages with
this feature to the total frequency of messages with this feature."
  (with-slots (spam-count ham-count) feature
    (let ((spam-frequency (/ spam-count (max 1 *total-spams*)))
          (ham-frequency (/ ham-count (max 1 *total-hams*))))
      (/ spam-frequency (+ spam-frequency ham-frequency)))))

;; based on Bayesian notion of incorporating observed data into prior
;; knowledge or assumptions; described by Gary Robinson

(defun bayesian-spam-probability (feature &optional
                                            (assumed-probability 1/2)
                                            (weight 1))
  "Returns the probability that a message containing FEATURE is a spam."
  (let ((basic-probability (spam-probability feature))
        (data-points (+ (spam-count feature) (ham-count feature))))
    (/ (+ (* weight assumed-probability)
          (* data-points basic-probability))
       (+ weight data-points))))


;; combining per-feature probabilities

;; independent probabilities can be just multiplied together to produce the
;; combined probability; there are methods to combine non-independent
;; probabilities (e.g. one proposed by R.A. Fisher)

(defun untrained-p (feature)
  "Returns true if FEATURE is untrained, i.e., its spam and ham counts are both
zero."
  (with-slots (spam-count ham-count) feature
    (and (zerop spam-count) (zerop ham-count))))

(defun inverse-chi-square (value degrees-of-freedom)
  "Returns an inverse chi square of the VALUE, as proposed by Robinson."
  (assert (evenp degrees-of-freedom))
  (min
   (loop with m = (/ value 2)
         for i below (/ degrees-of-freedom 2)
         for prob = (exp (- m)) then (* prob (/ m i))
         summing prob)
   1.0))

(defun fisher (probs number-of-probs)
  "Returns PROBS combined by Fisher method, as described by Robinson."
  (inverse-chi-square
   (* -2
      ;; can underflow
      ;; (log (reduce #'* probs))
      ;; but log of a product is a sum of logs of the factors
      (reduce #'+ probs :key #'log))
   (* 2 number-of-probs)))

(defun score (features)
  "Returns a spam score, number between 0 and 1, that combines the spam
probabilities of FEATURES using the Fisher method."
  (let ((spam-probs ()) (ham-probs ()) (number-of-probs 0))
    (dolist (feature features)
      (unless (untrained-p feature)
        (let ((spam-prob (float (bayesian-spam-probability feature) 0.0d0)))
          (push spam-prob spam-probs)
          (push (- 1.0d0 spam-prob) ham-probs)
          (incf number-of-probs))))
    (let ((h (- 1 (fisher spam-probs number-of-probs)))
          (s (- 1 (fisher ham-probs number-of-probs))))
      (/ (+ (- 1 h) s) 2.0d0))))


;; testing the filter





;; experiments

(comment

 (extract-words "foo bar baz foo bar")
 (extract-features "foo bar baz foo bar")


 *feature-database*
 (clear-feature-database)

 (train "Make money fast" :spam)
 (classify "Make money fast")
 (classify "Want to go to the movies?")

 (train "Do you have any money for the movies?" :ham)
 (classify "Make money fast")
 (classify "Want to go to the movies?")

)
