(ns hello-lisp.functions
  "Implementations of the functions from The Little Schemer and The
  Seasoned Schemer."
  (:require
   [clojure.walk :as walk]))


;; # The Little Schemer


;; ## 1. Toys

(def ^{:arglists '([x])} atom?
  "Returns true if `x` is an atom, i.e., it is not a list."
  (complement list?))

(assert (atom? 1))
(assert (atom? "a"))
(assert (atom? nil))
(assert (not (atom? '())))


;; ## 2. Do It, Do It Again, and Again, and Again...

(defn lat?
  "Returns true if `l` is a list of atoms."
  [l]
  (cond
    (atom? l)         false
    (empty? l)        true
    (atom? (first l)) (recur (rest l))
    :else             false))

(assert (lat? '()))
(assert (lat? '(1 2 3)))
(assert (not (lat? 1)))
(assert (not (lat? '(1 2 (3)))))
(assert (not (lat? "")))
(assert (not (lat? [])))
(assert (not (lat? {})))


(defn member?
  "Returns true if list of atoms `lat` contains atom `a`."
  [a lat]
  (cond
    (empty? lat) false
    :else        (or (= (first lat) a)
                     (recur a (rest lat)))))

(assert (not (member? 'a '())))
(assert (not (member? 'a '(x y z))))
(assert (member? 'a '(a b c)))


;; ## 3. Cons the Magnificent

(defn rember
  "Returns `lat` without the first occurrence of `a`."
  [a lat]
  (cond
    (empty? lat) lat
    (= (first lat) a) (rest lat)
    :else (cons (first lat) (rember a (rest lat)))))

(assert (= '()
           (rember 'a '())))
(assert (= '(b c)
           (rember 'a '(a b c))))
(assert (= '(b a c)
           (rember 'a '(a b a c))))
(assert (= '(x y z)
           (rember 'a '(x y z))))


(defn firsts
  "Returns a list that contains first elements of lists in `l`."
  [l]
  (cond
    (empty? l) l
    :else (cons (ffirst l)
                (firsts (rest l)))))

(assert (= '()
           (firsts '())))
(assert (= '(apple plum grape bean)
           (firsts '((apple peach pumpkin)
                     (plum pear cherry)
                     (grape raisin pea)
                     (bean carrot eggplant)))))
(assert (= '(five four eleven)
           (firsts
            '((five plums)
              (four)
              (eleven green oranges)))))
(assert (= '((five plums) eleven (no))
           (firsts '(((five plums) four)
                     (eleven green oranges)
                     ((no) more)))))


(defn seconds
  "Returns a list that contains second elements of lists in `l`."
  [l]
  (cond
    (empty? l) l
    :else      (cons (second (first l))
                     (seconds (rest l)))))

(assert (= '() (seconds '())))
(assert (= '(a b c) (seconds '((x a) (x b) (x c y)))))


(defn insertr
  "Returns `lat` with `new` added after the first occurrence of `old`."
  [new old lat]
  (cond
    (empty? lat) lat
    :else (cond
            (= (first lat) old) (cons old (cons new (rest lat)))
            :else (cons (first lat)
                        (insertr new old (rest lat))))))

(assert (= '()
           (insertr 'a 'b '())))
(assert (= '(x b a y)
           (insertr 'a 'b '(x b y))))


(defn insertl
  "Returns `lat` with `new` added before the first occurrence of `old`."
  [new old lat]
  (cond
    (empty? lat) lat
    :else (cond
            (= (first lat) old) (cons new (cons old (rest lat)))
            :else (cons (first lat)
                        (insertl new old (rest lat))))))

(assert (= '() (insertl 'a 'b '())))
(assert (= '(x a b y) (insertl 'a 'b '(x b y))))

(defn subst
  "Returns `lat` with `new` added instead of the first occurrence of `old`."
  [new old lat]
  (cond
    (empty? lat) lat
    :else (cond
            (= (first lat) old) (cons new (rest lat))
            :else (cons (first lat)
                        (subst new old (rest lat))))))

(assert (= '() (subst 'a 'b '())))
(assert (= '(x a y) (subst 'a 'b '(x b y))))


(defn subst2
  "Returns `lat` with `new` instead of first occurrence of `o1` or `o2`."
  [new o1 o2 lat]
  (cond
    (empty? lat) lat
    :else (cond
            (= (first lat) o1) (cons new (rest lat))
            (= (first lat) o2) (cons new (rest lat))
            :else (cons (first lat)
                        (subst2 new o1 o2 (rest lat))))))

(assert (= '() (subst2 'a 'b 'c '())))
(assert (= '(vanilla ice cream with chocolate topping)
           (subst2 'vanilla
                   'chocolate
                   'banana
                   '(banana ice cream with chocolate topping))))

(defn multirember
  "Returns `lat` without all `a`s."
  [a lat]
  (cond
    (empty? lat) lat
    :else        (cond
                   (= (first lat) a) (multirember a (rest lat))
                   :else             (cons (first lat)
                                           (multirember a (rest lat))))))

(assert (= '() (multirember 'a '())))
(assert (= '(x y z) (multirember 'a '(x y z))))
(assert (= '(b c d e) (multirember 'a '(a b c a d a e))))


(defn multiinsertr
  "Returns `lat` with `new` added after all `old`s."
  [new old lat]
  (cond
    (empty? lat) lat
    :else (cond
            (= (first lat) old)
            (cons old
                  (cons new
                        (multiinsertr new old (rest lat))))
            :else (cons (first lat)
                        (multiinsertr new old (rest lat))))))

(assert (= '() (multiinsertr 'a 'b '())))
(assert (= '(x y z) (multiinsertr 'a 'b '(x y z))))
(assert (= '(b a c b a a d) (multiinsertr 'a 'b '(b c b a d))))


(defn multiinsertl
  "Returns `lat` with `new` added before all `old`s."
  [new old lat]
  (cond
    (empty? lat) lat
    :else (cond
            (= (first lat) old)
            (cons new
                  (cons old
                        (multiinsertl new old (rest lat))))
            :else (cons (first lat)
                        (multiinsertl new old (rest lat))))))

(assert (= '() (multiinsertl 'a 'b '())))
(assert (= '(x y z) (multiinsertl 'a 'b '(x y z))))
(assert (= '(a b c a b a d) (multiinsertl 'a 'b '(b c b a d))))


(defn multisubst
  "Returns `lat` with `new` added instead of all `old`s."
  [new old lat]
  (cond
    (empty? lat) lat
    :else (cond
            (= (first lat) old) (cons new (multisubst new old (rest lat)))
            :else (cons (first lat) (multisubst new old (rest lat))))))

(assert (= '() (multisubst 'a 'b '())))
(assert (= '(x y z) (multisubst 'a 'b '(x y z))))
(assert (= '(a c a a d) (multisubst 'a 'b '(b c b a d))))


;; ## 4. Numbers Games

;; only non-negative integers (natural numbers and zero) are considered.

(defn +-1
  "Returns the sum of `n` and `m`."
  [n m]
  (cond
    (zero? m) n
    :else (inc (+-1 n (dec m)))))

(assert (= 0 (+-1 0 0)))
(assert (= 12 (+-1 4 8)))


(defn --1
  "Returns the difference between `n` and `m`."
  [n m]
  (cond
    (zero? m) n
    :else (dec (--1 n (dec m)))))

(assert (= 0 (--1 0 0)))
(assert (= 4 (--1 12 8)))


(defn *-1
  "Returns the product of `n` and `m`."
  [n m]
  (cond
    (zero? m) 0
    :else (+ n (*-1 n (dec m)))))

(assert (= 0 (*-1 0 0)))
(assert (= 0 (*-1 1 0)))
(assert (= 12 (*-1 3 4)))


(defn pow
  "Returns `n` raised to `m`th power."
  [n m]
  (cond
    (zero? m) 1
    :else (* n (pow n (dec m)))))

(assert (= 1 (pow 1 1)))
(assert (= 8 (pow 2 3)))
(assert (= 125 (pow 5 3)))


(defn length
  "Returns the number of elements in the `lat`."
  [lat]
  (cond
    (empty? lat) 0
    :else (inc (length (rest lat)))))

(assert (= 0 (length '())))
(assert (= 1 (length '(a))))
(assert (= 3 (length '(a b c))))


(defn pick
  "Returns `n`-th element of `lat` or nil, starting from 1."
  [n lat]
  (cond
    (zero? (dec n)) (first lat)
    :else           (pick (dec n) (rest lat))))

(assert (nil? (pick 1 '())))
(assert (nil? (pick 10 '(lasagna spaghetti ravioli macaroni meatball))))
(assert (= 'macaroni (pick 4 '(lasagna spaghetti ravioli macaroni meatball))))
#_(pick 0 '(a)) ; StackOverflowError


(defn rempick
  "Returns `lat` without `n`-th element, starting from 1.
  Adds nils at absent preceding indices if `n` is greater than the
  length of the `lat`."
  [n lat]
  (cond
    (zero? (dec n)) (rest lat)
    :else           (cons (first lat)
                          (rempick (dec n) (rest lat)))))

(assert (= '() (rempick 1 '())))
(assert (= '(a b c nil) (rempick 5 '(a b c))))
(assert (= '(hotdogs with mustard) (rempick 3 '(hotdogs with hot mustard))))


(defn no-nums
  "Returns `lat` without numbers."
  [lat]
  (cond
    (empty? lat) lat
    :else        (cond
                   (number? (first lat)) (no-nums (rest lat))
                   :else                 (cons (first lat)
                                               (no-nums (rest lat))))))

(assert (= '() (no-nums '())))
(assert (= '(pears prunes dates) (no-nums '(pears prunes dates))))
(assert (= '(pears prunes dates) (no-nums '(5 pears 6 prunes 9 dates))))


(defn all-nums
  "Returns a tup of all numbers from `lat`."
  [lat]
  (cond
    (empty? lat) lat
    :else        (cond
                   (number? (first lat)) (cons (first lat)
                                               (all-nums (rest lat)))
                   :else                 (all-nums (rest lat)))))

(assert (= '() (all-nums '())))
(assert (= '() (all-nums '(pears prunes dates))))
(assert (= '(5 6 9) (all-nums '(5 6 9))))
(assert (= '(5 6 9) (all-nums '(5 pears 6 prunes 9 dates))))


(defn equan?
  "Returns true if atoms `a1` and `a2` are the same."
  [a1 a2]
  (cond
    (and (number? a1)
         (number? a2)) (== a1 a2)
    (or (number? a1)
        (number? a2))  false
    :else              (= a1 a2)))

(assert (equan? 1 1))
(assert (equan? 'a 'a))
(assert (not (equan? 1 2)))
(assert (not (equan? 'a 'b)))



(defn one? [n]
  (= n 1))

(assert (not (one? 0)))
(assert (not (one? 12)))
(assert (one? 1))


;; ## 5. *Oh My Gawd*: It's Full of Stars

(defn rember*
  "Returns `l` without the first occurrence of `a`. Works on arbitrarily
  nested lists."
  [a l]
  (cond
    (empty? l)        l
    (atom? (first l)) (cond
                        (= (first l) a) (rember* a (rest l))
                        :else           (cons (first l)
                                              (rember* a (rest l))))
    :else             (cons (rember* a (first l))
                            (rember* a (rest l)))))

(assert (= '() (rember* 'a '())))
(assert (= '(((tomato)) ((bean)) (and ((flying))))
           (rember* 'sauce '(((tomato sauce))
                             ((bean) sauce)
                             (and ((flying)) sauce)))))
(assert (= '((coffee) ((tea)) (and (hick)))
           (rember* 'cup '((coffee) cup
                           ((tea) cup)
                           (and (hick)) cup))))


(defn insertr*
  "Returns `l` with `new` added after all occurrences of `old`.
  Works on arbitrarily nested lists."
  [new old l]
  (let [head (first l)
        tail (rest l)]
    (cond
      (empty? l)   l
      (atom? head) (cond
                     (= head old) (cons old
                                        (cons new (insertr* new old tail)))
                     :else        (cons head  (insertr* new old tail)))
      :else        (cons (insertr* new old head)
                         (insertr* new old tail)))))


(assert (= (insertr* 'a 'b '()) '()))
(assert (= (insertr* 'a 'b '(b c)) '(b a c)))
(assert (= (insertr* 'roast 'chuck '((how much (wood))
                                     could
                                     ((a (wood) chuck))
                                     (((chuck)))
                                     (if (a) ((wood chuck)))
                                     could chuck wood))
           '((how much (wood))
             could
             ((a (wood) chuck roast))
             (((chuck roast)))
             (if (a) ((wood chuck roast)))
             could chuck roast wood)))


(defn occur*
  "Returns total number of occurrences of `a` in `l`.
  Works on arbitrarily nested lists."
  [a l]
  (cond
    (empty? l)        0
    (atom? (first l)) (cond
                        (= (first l) a) (inc (occur* a (rest l)))
                        :else           (occur* a (rest l)))
    :else             (+ (occur* a (first l))
                         (occur* a (rest l)))))

(assert (= 0 (occur* 'a '())))
(assert (= 1 (occur* 'a '(a b c))))
(assert (= 5 (occur* 'banana '((banana)
                               (split ((((banana ice)))
                                       (cream (banana))
                                       sherbet))
                               (banana)
                               (bread)
                               (banana brandy)))))


(defn subst*
  "Returns `l` with all occurrences of `old` replaced with `new`.
  Works on arbitrarily nested lists."
  [new old l]
  (cond
    (empty? l)        l
    (atom? (first l)) (cond
                        (= (first l) old) (cons new
                                                (subst* new old (rest l)))
                        :else             (cons (first l)
                                                (subst* new old (rest l))))
    :else (cons (subst* new old (first l))
                (subst* new old (rest l)))))

(assert (= '() (subst* 'a 'b '())))
(assert (= '(a a c) (subst* 'a 'b '(a b c))))
(assert (= '((orange)
             (split ((((orange ice)))
                     (cream (orange))
                     sherbet))
             (orange)
             (bread)
             (orange brandy))
           (subst* 'orange 'banana '((banana)
                                     (split ((((banana ice)))
                                             (cream (banana))
                                             sherbet))
                                     (banana)
                                     (bread)
                                     (banana brandy)))))


(defn insertl*
  "Returns `l` with `new` added before all occurrences of `old`.
  Works on arbitrarily nested lists."
  [new old l]
  (cond
    (empty? l) l
    (atom? (first l)) (cond
                        (= (first l) old)
                        (cons new
                              (cons old
                                    (insertl* new old (rest l))))
                        :else (cons (first l)
                                    (insertl* new old (rest l))))
    :else (cons (insertl* new old (first l))
                (insertl* new old (rest l)))))

(assert (= (insertl* 'a 'b '()) '()))
(assert (= (insertl* 'a 'b '(b c)) '(a b c)))
(assert (= (insertl* 'pecker 'chuck '((how much (wood))
                                      could
                                      ((a (wood) chuck))
                                      (((chuck)))
                                      (if (a) ((wood chuck)))
                                      could chuck wood))
           '((how much (wood))
             could
             ((a (wood) pecker chuck))
             (((pecker chuck)))
             (if (a) ((wood pecker chuck)))
             could pecker chuck wood)))


(defn member*
  "Returns true if `l` contains `a`.
  Works on arbitrarily nested lists."
  [a l]
  (cond
    (empty? l) false
    (atom? (first l)) (or (= (first l) a)
                          (member* a (rest l)))
    :else (or (member* a (first l))
              (member* a (rest l)))))


(assert (not (member* 'a '())))
(assert (not (member* 'a '(b c (d) ((e) f)))))
(assert (member* 'a '(b c (d a) (a (e) f))))


(defn leftmost
  "Returns leftmost element of the `l`.
  Works on arbitrarily nested lists."
  [l]
  (cond
    (atom? (first l)) (first l)
    :else (leftmost (first l))))

(assert (nil? (leftmost '())))
(assert (nil? (leftmost '(((() four)) 17 (seventeen)))))
(assert (= 'potato (leftmost '((potato) (chips ((with) fish) (chips))))))
(assert (= 'hot (leftmost '(((hot) (tuna (and))) cheese))))


(defn eqlist?
  "Returns true if lists `l1` and `l2` are the same."
  [l1 l2]
  (cond
    (and (empty? l1) (empty? l2)) true
    (or (empty? l1) (empty? l2))  false
    (and (atom? (first l1))
         (atom? (first l2)))      (and (= (first l1) (first l2))
                                       (eqlist? (rest l1) (rest l2)))
    (or (atom? (first l1))
        (atom? (first l2)))       false
    :else                         (and (eqlist? (first l1) (first l2))
                                       (eqlist? (rest l1) (rest l2)))))

(assert (eqlist? '() '()))
(assert (eqlist? '(a b (c)) '(a b (c))))
(assert (not (eqlist? '(a b c) '(x y z))))


(defn equal?
  "Returns true if S-expressions `s1` and `s2` are the same."
  [s1 s2]
  (cond
    (and (atom? s1)
         (atom? s2)) (= s1 s2)
    (or (atom? s1)
        (atom? s2))  false
    :else            (eqlist? s1 s2)))

(assert (equal? 1 1))
(assert (equal? '() '()))
(assert (equal? '(a b (c)) '(a b (c))))
(assert (not (equal? 3 4)))
(assert (not (equal? '(a b c) '(x y z))))


;; ## 6. Shadows

(defn numbered?
  "Returns true is arithmetic expression `aexp` contains only atoms that
  are numbers and operation symbols."
  [aexp]
  (cond
    (atom? aexp) (number? aexp)
    :else (and (numbered? (first aexp))
               (numbered? (first (rest (rest aexp)))))))

(assert (not (numbered? '())))
(assert (not (numbered? '(a 3 b))))
(assert (numbered? '(1 + 3)))
(assert (numbered? '(1 + (3 * 4))))


(defn first-sub-exp
  "Returns the first arithmetic subexpression of `aexp`."
  [aexp]
  (first (rest aexp)))

(assert (nil? (first-sub-exp '())))
(assert (= 1 (first-sub-exp '(+ 1 3))))


(defn second-sub-exp
  "Returns the second arithmetic subexpression of `aexp`."
  [aexp]
  (first (rest (rest aexp))))

(assert (nil? (second-sub-exp '())))
(assert (= 3 (second-sub-exp '(+ 1 3))))


(defn operator
  "Returns the the operator of `aexp`."
  [aexp]
  (first aexp))

(assert (nil? (operator '())))
(assert (= '+ (operator '(+ 1 3))))


;; functions for numbers represented as lists

(defn *zero?
  "Returns true if `n` represents zero."
  [n]
  (empty? n))

(assert (*zero? '()))
(assert (not (*zero? '(()))))


(defn *inc
  "Returns `n` increased by 1."
  [n]
  (cons '() n))

(assert (= '(()) (*inc '())))


(defn *dec
  "Returns `n` decreased by 1."
  [n]
  (rest n))

(assert (= '(()) (*dec '(() ()))))


;; ## 7. Friends and Relations

(defn a-pair?
  "Returns true if `x` is a pair."
  [x]
  (cond
    (atom? x)                false
    (empty? x)               false
    (empty? (rest x))        false
    (empty? (rest (rest x))) true
    :else                    false))

(assert (not (a-pair? 1)))
(assert (not (a-pair? '())))
(assert (not (a-pair? '(a))))
(assert (not (a-pair? '(a b c))))
(assert (a-pair? '(a b)))
(assert (a-pair? '((a b) c)))


(defn is-set?
  "Returns true if `lat` is a set (does not allow duplicate values)."
  [lat]
  (cond
    (empty? lat)                     true
    (member? (first lat) (rest lat)) false
    :else                            (is-set? (rest lat))))

(assert (is-set? '()))
(assert (is-set? '(a b c d e)))
(assert (not (is-set? '(apple peaches apple plum))))
(assert (not (is-set? '(apple 3 pear 4 apple 3 4))))


(defn make-set
  "Returns a set built from `lat` by removing duplicate atoms."
  [lat]
  (cond
    (empty? lat) lat
    (member? (first lat) (rest lat)) (make-set (rest lat))
    :else (cons (first lat)
                (make-set (rest lat)))))

(assert (= '() (make-set '())))
(assert (= '(pear plum apple lemon peach)
           (make-set '(apple peach pear peach plum apple lemon peach))))


(defn subset?
  "Returns true if `set1` is a subset of `set2`."
  [set1 set2]
  (cond
    (empty? set1) true
    :else (and (member? (first set1) set2)
               (subset? (rest set1) set2))))

(assert (subset? '() '()))
(assert (subset? '(a b) '(a b c d e f g h)))
(assert (not (subset? '(a t) '(a b c d e f g h))))


(defn eqset?
  "Returns true if `set1` and `set2` are the same set (same elements
  disregarding order)."
  [set1 set2]
  (and (subset? set1 set2)
       (subset? set2 set1)))

(assert (eqset? '() '()))
(assert (eqset? '(a b c) '(a b c)))
(assert (not (eqset? '(a b c) '(d e f))))


(defn intersect?
  "Returns true if `set1` and `set2` intersect (have some common elements)."
  [set1 set2]
  (cond
    (empty? set1) false
    :else         (or (member? (first set1) set2)
                      (intersect? (rest set1) set2))))

(assert (not (intersect? '() '())))
(assert (not (intersect? '(a b c) '(d e f))))
(assert (intersect? '(a b c) '(c d e)))


(defn intersect
  "Returns an intersection (a set of common elements) of `set1` and `set2`."
  [set1 set2]
  (cond
    (empty? set1)               '()
    (member? (first set1) set2) (cons (first set1)
                                      (intersect (rest set1) set2))
    :else                       (intersect (rest set1) set2)))

(assert (= '() (intersect '() '())))
(assert (= '() (intersect '(a b c) '(d e f))))
(assert (= '(c) (intersect '(a b c) '(c d e))))


(defn union
  "Returns a union (a set of all elements) of `set1` and `set2`."
  [set1 set2]
  (cond
    (empty? set1)               set2
    (member? (first set1) set2) (union (rest set1) set2)
    :else                       (cons (first set1)
                                      (union (rest set1) set2))))

(assert (= '() (union '() '())))
(assert (= '(a b c d e f) (union '(a b c) '(d e f))))
(assert (= '(a b c d e) (union '(a b c) '(c d e))))


(defn difference
  "Returns a difference (a set of all elements that is in the first set
  but not in the second set) between `set1` and `set2`."
  [set1 set2]
  (cond
    (empty? set1)               '()
    (member? (first set1) set2) (difference (rest set1) set2)
    :else                       (cons (first set1)
                                      (difference (rest set1) set2))))

(assert (= '() (difference '() '())))
(assert (= '(a c) (difference '(a b c) '(b))))


(defn intersect-all
  "Returns an intersection of all sets in list `l-set`."
  [l-set]
  (cond
    (empty? (rest l-set)) (first l-set)
    :else                 (intersect (first l-set)
                                     (intersect-all (rest l-set)))))

(assert (= nil (intersect-all '())))
(assert (= '() (intersect-all '((a b c) (d e f) (g h i)))))
(assert (= '(a) (intersect-all '((a b c) (d e a) (x a y)))))


;; pair constructor and accessors

(defn *build
  "Returns a pair made of `s1` and `s2`."
  [s1 s2]
  (cons s1 (cons s2 '())))

(assert (= '(a b) (*build 'a 'b)))

(defn *first
  "Returns a first element of pair `p`."
  [p]
  (first p))

(assert (= 'a (*first '(a b))))

(defn *second
  "Returns a second element of pair `p`."
  [p]
  (first (rest p)))

(assert (= 'b (*second '(a b))))

(defn *third
  "Returns a third element of list `l`."
  [l]
  (first (rest (rest l))))

(assert (= 'c (*third '(a b c))))


(defn fun?
  "Returns true if relation `rel` is a function."
  [rel]
  (is-set? (firsts rel)))

(assert (fun? '()))
(assert (fun? '((a b) (c d))))
(assert (not (fun? '((a b) (a c)))))


(defn revpair
  "Returns `pair` reversed."
  [pair]
  (*build (*second pair) (*first pair)))

(assert (= '(nil nil) (revpair '())))
(assert (= '(b a) (revpair '(a b))))


(defn revrel
  "Returns relation `rel` reversed."
  [rel]
  (cond
    (empty? rel) rel
    :else        (cons (revpair (first rel))
                       (revrel (rest rel)))))

(assert (= '() (revrel '())))
(assert (= '((a 8) (pie pumpkin) (sick got))
           (revrel '((8 a) (pumpkin pie) (got sick)))))


(defn fullfun?
  "Returns true if second elements of all pairs in `fun` form a set."
  [fun]
  (is-set? (seconds fun)))

(assert (fullfun? '()))
(assert (fullfun? '((a b) (c d) (e f))))
(assert (not (fullfun? '((a b) (c b) (e f)))))


;; ## 8. Lambda the Ultimate

(defn rember-f
  "Returns `l` without the first occurrence of `a`, using `test?` for
  comparison."
  [test? a l]
  (cond
    (empty? l) l
    (test? (first l) a) (rest l)
    :else (cons (first l)
                (rember-f test? a (rest l)))))

(assert (= '() (rember-f = 'a '())))
(assert (= '(b c) (rember-f = 'a '(a b c))))
(assert (= '(b a c) (rember-f = 'a '(a b a c))))
(assert (= '(x y z) (rember-f = 'a '(x y z))))
(assert (= '(1 2 3) (rember-f == '0 '(0 1 2 3))))


(defn =-c
  "Returns a function that compares its argument to `a` with `=`."
  [a]
  (fn [x]
    (= x a)))

(let [=a (=-c 'a)]
  (assert (=a 'a))
  (assert (not (=a 'b))))


(defn *rember-f
  "Returns a function that returns `l` without the first occurrence of
  `a`, using `test?` for comparison."
  [test?]
  (fn [a l]
    (cond
      (empty? l) l
      (test? (first l) a) (rest l)
      :else (cons (first l)
                  ((*rember-f test?) a (rest l))))))

(def ^{:arglists '([a l])} *rember-=
  "Returns `l` without the first occurrence of `a`, using `=` for
  comparison."
  (*rember-f =))

(assert (= '() (*rember-= 'a '())))
(assert (= '(salad is good) (*rember-= 'tuna '(tuna salad is good))))


(defn seql
  "Returns `l` with `old` and `new` consed on it."
  [new old l]
  (cons new (cons old l)))

(assert (= '(a b) (seql 'a 'b '())))


(defn seqr
  "Returns `l` with `new` and `old` consed on it."
  [new old l]
  (cons old (cons new l)))

(assert (= '(b a) (seqr 'a 'b '())))


(defn seqs
  "Returns `l` with `new` consed on it."
  [new _ l]
  (cons new l))

(assert (= '(a a b c) (seqs 'a 'b '(a b c))))


(defn insert-g
  "Returns a function that inserts `new` near `old` in the `l` using
  `seq` to do actual insertion."
  [seq]
  (fn [new old l]
    (cond
      (empty? l)        l
      (= (first l) old) (seq new old (rest l))
      :else             (cons (first l)
                              ((insert-g seq) new old (rest l))))))

(let [insertl (insert-g seql)
      insertr (insert-g seqr)]
  (assert (= '(a b c d) (insertl 'a 'b '(b c d))))
  (assert (= '(b a c d) (insertr 'a 'b '(b c d)))))


(defn multirember-f
  "Returns a function that removes all occurrences of `a` from `lat`
  using `test?` to compare elements."
  [test?]
  (fn [a lat]
    (cond
      (empty? lat) lat
      (test? a (first lat)) ((multirember-f test?) a (rest lat))
      :else (cons (first lat)
                  ((multirember-f test?) a (rest lat))))))

(assert (= '(shrimp salad salad and)
           ((multirember-f =) 'tuna '(shrimp salad tuna salad and tuna))))


(defn multirember-t
  "Returns `lat` without elements, for which `test?` returns true."
  [test? lat]
  (cond
    (empty? lat)        lat
    (test? (first lat)) (multirember-t test? (rest lat))
    :else               (cons (first lat)
                              (multirember-t test? (rest lat)))))

(assert (= '() (multirember-t identity '(a b c))))
(assert (= '(a b c) (multirember-t (complement identity) '(a b c))))
(assert (= '(a b) (multirember-t #{'c} '(c a b c c))))


(defn multirember&co
  "Returns a result of calling collector function `col` with two
  arguments `newlat` and `seen` --- lists of elements of `lat` that
  are not the same as `a` (i.e., a result of call to `multirember`)
  and that are the same as `a`."
  [a lat col]
  (cond
    (empty? lat)      (col '() '())
    (= (first lat) a) (multirember&co a (rest lat)
                                      (fn [newlat seen]
                                        (col newlat
                                             (cons (first lat) seen))))
    :else             (multirember&co a (rest lat)
                                      (fn [newlat seen]
                                        (col (cons (first lat) newlat)
                                             seen)))))

(defn a-friend
  "Returns true if `matches` is the empty list. A collector function."
  [non-matches matches]
  (empty? matches))

(assert (multirember&co 'a '() a-friend))
(assert (multirember&co 'a '(b c d) a-friend))
(assert (not (multirember&co 'a '(a b c a) a-friend)))
(assert (= '((b c) (a)) (multirember&co 'a '(a b c)
                                        (fn [non-matches matches]
                                          (list non-matches matches)))))


(defn multiinsertlr
  "Returns `lat` with `new` added before all occurrences of `oldl` and
  after all occurrences of `oldr`, if `oldl` and `oldr` are different."
  [new oldl oldr lat]
  (cond
    (empty? lat)         lat
    (= (first lat) oldl) (cons new
                               (cons oldl
                                     (multiinsertlr new oldl oldr
                                                    (rest lat))))
    (= (first lat) oldr) (cons oldr
                               (cons new
                                     (multiinsertlr new oldl oldr
                                                    (rest lat))))
    :else                (cons (first lat)
                               (multiinsertlr new oldl oldr (rest lat)))))

(assert (= '() (multiinsertlr 'a 'b 'c '())))
(assert (= '(a a b c a) (multiinsertlr 'a 'b 'c '(a b c))))


(defn multiinsertlr&co
  "Returns a result of calling `col` with three arguments `newlat`, `l`
  and `r` --- a list with `new` values added as with `multiinsertlr`,
  a number of additions done before `oldl` and a number of additions
  done after `oldr`."
  [new oldl oldr lat col]
  (cond
    (empty? lat)         (col '() 0 0)
    (= (first lat) oldl) (multiinsertlr&co
                          new oldl oldr (rest lat)
                          (fn [newlat l r]
                            (col (cons new (cons oldl newlat)) (inc l) r)))
    (= (first lat) oldr) (multiinsertlr&co
                          new oldl oldr (rest lat)
                          (fn [newlat l r]
                            (col (cons oldr (cons new newlat)) l (inc r))))
    :else                (multiinsertlr&co new oldl oldr (rest lat)
                                           (fn [newlat l r]
                                             (col (cons (first lat)
                                                        newlat) l r)))))

(assert (= '(() 0 0) (multiinsertlr&co 'a 'b 'c '()
                                       (fn [newlat l r]
                                         (list newlat l r)))))
(assert (= '((a a b c a d e f) 1 1) (multiinsertlr&co 'a 'b 'c '(a b c d e f)
                                                      (fn [newlat l r]
                                                        (list newlat l r)))))


(defn evens-only*
  "Returns `l` with all non-evens removed."
  [l]
  (cond
    (empty? l)        l
    (atom? (first l)) (cond
                        (even? (first l)) (cons (first l)
                                                (evens-only* (rest l)))
                        :else             (evens-only* (rest l)))
    :else             (cons (evens-only* (first l))
                            (evens-only* (rest l)))))

(assert (= '(0 2 4 12) (evens-only* '(0 2 4 12))))
(assert (= '(0 2 4) (evens-only* '(1 0 2 4 5))))
(assert (= '((2 8) 10 (() 6) 2) (evens-only* '((9 1 2 8) 3 10 ((9 9) 7 6) 2))))


(defn evens-only*&co
  "Returns a result of calling `col` with three arguments `newl`,
  `product` and `sum` --- a list with all even numbers in `l`, a
  product of all even numbers in `l`, and a sum of all odd numbers in
  `l`."
  [l col]
  (cond
    (empty? l)        (col '() 1 0)
    (atom? (first l)) (cond
                        (even? (first l)) (evens-only*&co
                                           (rest l)
                                           (fn [newl p s]
                                             (col (cons (first l) newl)
                                                  (* (first l) p)
                                                  s)))
                        :else             (evens-only*&co
                                           (rest l)
                                           (fn [newl p s]
                                             (col newl
                                                  p
                                                  (+ (first l) s)))))
    :else (evens-only*&co (first l)
                          (fn [al ap as]
                            (evens-only*&co (rest l)
                                            (fn [dl dp ds]
                                              (col (cons al dl)
                                                   (* ap dp)
                                                   (+ as ds))))))))

(assert (= '(0 1) (evens-only*&co '()
                                  (fn the-last-friend [newl product sum]
                                    (cons sum (cons product newl))))))
(assert (= '(38 1920 (2 8) 10 (() 6) 2)
           (evens-only*&co '((9 1 2 8) 3 10 ((9 9) 7 6) 2)
                           (fn the-last-friend [newl product sum]
                             (cons sum (cons product newl))))))


;; ## 9. ...and Again, and Again, and Again,...

(defn eternity
  "Never returns."
  [x]
  (eternity x))


(defn keep-looking
  "Returns true if `a` can be found in `lat`, using `sorn` as an index
  if it is a number and comparing it with `a` if it is not, recursively."
  [a sorn lat]
  (cond
    (number? sorn) (keep-looking a (pick sorn lat) lat)
    :else          (= sorn a)))

(assert (keep-looking 'caviar 6 '(6 2 4 caviar 5 7 3)))


(defn looking
  "Returns true if `a` can be found in `lat` by `keep-looking`, starting
  at the first element of `lat`."
  [a lat]
  (keep-looking a (pick 1 lat) lat))

(assert (not (looking 'caviar '())))
(assert (looking nil '()))
(assert (looking 'caviar '(6 2 4 caviar 5 7 3)))


(defn shift
  "Returns a pair that have the first part of the first part of `pair`
  as a first part, and the pair created from the second part of the
  first part and second part of the `pair` as a second part."
  [pair]
  (*build (*first (*first pair))
          (*build (*second (*first pair))
                  (*second pair))))

(assert (= '(a (b c)) (shift '((a b) c))))
(assert (= '(nil (nil nil)) (shift '())))


(defn align
  "Returns a pair whose pair components are aligned with `shift` recursively."
  [pora]
  (cond
    (atom? pora) pora
    (a-pair? (*first pora)) (align (shift pora))
    :else (*build (*first pora)
                  (align (*second pora)))))

(assert (= '((a b) (c d)) (align '(((a b) c) d))))


(defn length*
  "Returns number of atoms in `pora`.
  Works on arbitrarily nested pairs."
  [pora]
  (cond
    (atom? pora) 1
    :else (+ (length* (*first pora))
             (length* (*second pora)))))

(assert (= 1 (length* 1)))
(assert (= 2 (length* '())))
(assert (= 3 (length* '((a b) c))))


(defn weight*
  "Returns weighted number of atoms in `pora`. Atoms in first components
  of pairs are counted with weight 2. Works on arbitrarily nested
  pairs."
  [pora]
  (cond
    (atom? pora) 1
    :else (+ (* (weight* (*first pora)) 2)
             (weight* (*second pora)))))

(assert (= 1 (weight* 1)))
(assert (= 3 (weight* '())))
(assert (= 7 (weight* '((a b) c))))


(defn *shuffle
  "Returns `pora` with components of pairs swapped when the first
  component is a pair."
  [pora]
  (cond
    (atom? pora) pora
    (a-pair? (*first pora)) (*shuffle (revpair pora))
    :else (*build (*first pora)
                  (*shuffle (*second pora)))))

(assert (= '(nil nil) (*shuffle '())))
(assert (= '((c d) (a b)) (*shuffle '((a b) (c d)))))


(defn C
  "Some mathematical function (Lothar Collatz)."
  [n]
  (cond
    (one? n) 1
    :else (cond
            (even? n) (C (/ n 2))
            :else (C (inc (* 3 n))))))

(assert (= 1 (C 100)))


(defn A
  "Some mathematical function (Wilhelm Ackermann)."
  [n m]
  (cond
    (zero? n) (inc m)
    (zero? m) (A (dec n) 1)
    :else (A (dec n)
             (A n (dec m)))))

(assert (= 2 (A 1 0)))
(assert (= 3 (A 1 1)))
(assert (= 7 (A 2 2)))


(defn Y
  "Applicative-order Y combinator."
  [le]
  ((fn [f]
     (f f))
   (fn [f]
     (le (fn [x] ((f f) x))))))


;; ## 10. What Is the Value of All of This?

(def new-entry *build)

(defn lookup-in-entry-help
  "Returns an element of `values` with the same index under which `name`
  can be found in `names`, or, if it cannot be found, a result of
  calling `entry-f` with the single argument `name`."
  [name names values entry-f]
  (cond
    (empty? names) (entry-f name)
    (= (first names) name) (first values)
    :else (lookup-in-entry-help name
                                (rest names)
                                (rest values)
                                entry-f)))

(assert (= 'e (lookup-in-entry-help 'b '(a b c) '(d e f)
                                    (constantly :not-found))))
(assert (= :not-found (lookup-in-entry-help 'x '(a b c) '(d e f)
                                            (constantly :not-found))))


(defn lookup-in-entry
  "Returns an element of `entry` that corresponds to `name`, or, if not found,
  a result of calling `entry-f` with the single argument `name`."
  [name entry entry-f]
  (lookup-in-entry-help name
                        (*first entry)
                        (*second entry)
                        entry-f))

(assert (= 'e (lookup-in-entry 'b '((a b c) (d e f))
                               (constantly :not-found))))
(assert (= :not-found (lookup-in-entry 'x '((a b c) (d e f))
                                       (constantly :not-found))))


(defn lookup-in-table
  "Returns an element of `table` that corresponds to `name`, or, if not
  found, a result of calling `entry-f` with the single argument
  `name`."
  [name table table-f]
  (cond
    (empty? table) (table-f name)
    :else (lookup-in-entry name
                           (first table)
                           (fn [name]
                             (lookup-in-table name
                                              (rest table)
                                              table-f)))))

(assert (= 'e (lookup-in-table 'b '(((a b c) (d e f))
                                    ((x y z) (g h i)))
                               (constantly :not-found))))
(assert (= :not-found (lookup-in-entry 'q '(((a b c) (d e f))
                                            ((x y z) (g h i)))
                                       (constantly :not-found))))


(defn *const
  "Returns a representation of a constant `e`."
  [e table]
  (cond
    (number? e) e
    (true? e)   true
    (false? e)  false
    :else       (*build 'primitive e)))


(def text-of *second)

(defn *quote
  "Returns a representation of a quoted expression `e`."
  [e table]
  (text-of e))


(defn initial-table
  "Returns a result of a lookup for symbol `name` in the initial context
  table."
  [name]
  (first '()))

(defn *identifier
  "Returns a value bound to identifier `e` in context of `table`."
  [e table]
  (lookup-in-table e table initial-table))


(defn *lambda
  "Returns a representation of a lambda expression `e` with context
  `table` (a closure)."
  [e table]
  (*build 'non-primitive
          (cons table (rest e))))


(def table-of *first)
(def formals-of *second)
(def body-of *third)


(def question-of *first)
(def answer-of *second)
(declare meaning)

(defn else?
  "Returns true if expression `x` is a keyword `:else`."
  [x]
  (cond
    (atom? x) (= x :else)
    :else     false))

(defn evcon
  "Returns a value of conditional expression represented by `lines` in
  context of `table`."
  [lines table]
  (cond
    (else? (question-of (first lines)))         (meaning
                                                 (answer-of (first lines))
                                                 table)
    (meaning (question-of (first lines)) table) (meaning
                                                 (answer-of (first lines)) table)
    :else                                       (evcon (rest lines) table)))

(def cond-lines-of rest)

(defn *cond [e table]
  (evcon (cond-lines-of e) table))


(defn evlis
  "Returns a value of list expression `args` in context of `table`."
  [args table]
  (cond
    (empty? args) '()
    :else         (cons (meaning (first args) table)
                        (evlis (rest args) table))))

(def function-of first)
(def arguments-of rest)
(declare *apply)

(defn *application
  "Returns a value of application `e` in context of `table`.."
  [e table]
  (*apply
   (meaning (function-of e) table)
   (evlis (arguments-of e) table)))


(defn atom-to-action
  "Returns an action that corresponds to atom `e`."
  [e]
  (cond
    (number? e)   *const
    (true? e)     *const
    (false? e)    *const
    (= e cons)    *const
    (= e first)   *const
    (= e empty?)  *const
    (= e =)       *const
    (= e atom?)   *const
    (= e zero?)   *const
    (= e inc)     *const
    (= e dec)     *const
    (= e number?) *const
    :else         *identifier))


(defn list-to-action
  "Returns an action that corresponds to list expression `e`."
  [e]
  (cond
    (atom? (first e)) (cond
                        (= (first e) 'quote)  *quote
                        (= (first e) 'lambda) *lambda
                        (= (first e) 'cond)   *cond
                        :else                 *application)
    :else             *application))


(defn expression-to-action
  "Returns an action that corresponds to arbitrary expression `e`."
  [e]
  (cond
    (atom? e) (atom-to-action e)
    :else     (list-to-action e)))


(defn meaning
  "Returns the meaning (value in context) of expression `e` in the
  context (environment) described by `table`."
  [e table]
  ((expression-to-action e) e table))


(defn value
  "Returns the value of expression `e`."
  [e]
  (meaning e '()))


(defn primitive?
  "Returns true if `l` is a primitive function."
  [l]
  (= (*first l) 'primitive))

(defn non-primitive?
  "Returns true if `l` is a non-primitive function."
  [l]
  (= (*first l) 'non-primitive))

(defn *atom?
  "Returns true if `x` is  an atomic expression (atom or function)."
  [x]
  (cond
    (atom? x)                    true
    (empty? x)                   false
    (= (first x) 'primitive)     true
    (= (first x) 'non-primitive) true
    :else                        false))


(defn apply-primitive
  "Returns a result of applying a primitive function `name` to its
  arguments `vals`."
  [name vals]
  (cond
    (= name 'cons)    (cons (*first vals) (*second vals))
    (= name 'first)   (first (*first vals))
    (= name 'rest)    (rest (*first vals))
    (= name 'empty?)  (empty? (*first vals))
    (= name '=)       (= (*first vals) (*second vals))
    (= name 'atom?)   (*atom? (*first vals))
    (= name 'zero?)   (zero? (*first vals))
    (= name 'inc)     (inc (*first vals))
    (= name 'dec)     (dec (*first vals))
    (= name 'number?) (number? (*first vals))))


(def extend-table cons)

(defn apply-closure
  "Returns a result of applying a non-primitive function `closure` to
  its arguments `vals`."
  [closure vals]
  (meaning (body-of closure)
           (extend-table (new-entry (formals-of closure) vals)
                         (table-of closure))))


(defn *apply
  "Returns a result of applying an arbitrary function `fun` to its
  arguments `vals`."
  [fun vals]
  (cond
    (primitive? fun)     (apply-primitive (*second fun) vals)
    (non-primitive? fun) (apply-closure (*second fun) vals)))



;; # The Seasoned Schemer


;; ## 11. Welcome Back to the Show

;; with simple helper

(defn is-first?
  "Returns true if `a` is equal to first element of `lat`."
  [a lat]
  (cond
    (empty? lat) false
    :else (= (first lat) a)))

(assert (not (is-first? 'a '())))
(assert (not (is-first? 'a '(x a b c))))
(assert (is-first? 'a '(a b c)))


(defn *two-in-a-row?
  "Returns true if any value in `lat` occurs at least twice successively."
  [lat]
  (cond
    (empty? lat) false
    :else (or (is-first? (first lat) (rest lat))
              (*two-in-a-row? (rest lat)))))

(assert (not (*two-in-a-row? '())))
(assert (not (*two-in-a-row? '(a b c))))
(assert (*two-in-a-row? '(a b b c)))


;; with recursive helper that stores state between calls in a parameter

(defn two-in-a-row-b?
  "Returns true if `preceding` is the first element of `lat`."
  [preceding lat]
  (cond
    (empty? lat) false
    :else (or (= (first lat) preceding)
              (two-in-a-row-b? (first lat) (rest lat)))))

(assert (not (two-in-a-row-b? 'a '())))
(assert (not (two-in-a-row-b? 'b '(a b c))))
(assert (two-in-a-row-b? 'b '(a b b c)))


(defn two-in-a-row?
  "Returns true if any value in `lat` occurs at least twice successively."
  [lat]
  (cond
    (empty? lat) false
    :else (two-in-a-row-b? (first lat) (rest lat))))

(assert (not (two-in-a-row? '())))
(assert (not (two-in-a-row? '(a b c))))
(assert (two-in-a-row? '(a b b c)))


(defn sum-of-prefixes-b
  "Returns a tup where each element is a sum of all preceding elements
  in `tup` plus `sonssf`."
  [sonssf tup]
  (cond
    (empty? tup) '()
    :else (cons (+ sonssf (first tup))
                (sum-of-prefixes-b (+ sonssf (first tup)) (rest tup)))))

(assert (= '() (sum-of-prefixes-b 1 '())))
(assert (= '(2 3 4 5) (sum-of-prefixes-b 1 '(1 1 1 1))))

(defn sum-of-prefixes
  "Returns a tup where each element is a sum of all preceding elements
  in `tup`."
  [tup]
  (sum-of-prefixes-b 0 tup))

(assert (= '() (sum-of-prefixes '())))
(assert (= '(1 2 3 4 5) (sum-of-prefixes '(1 1 1 1 1))))
(assert (= '(2 3 12 29 29) (sum-of-prefixes '(2 1 9 17 0))))


(defn scramble-b
  "Returns a scrambled `tup` given reversed prefix `rev-pre`. TODO:
  describe this weird stuff"
  [tup rev-pre]
  (cond
    (empty? tup) '()
    :else (cons (pick (first tup)
                      (cons (first tup) rev-pre))
                  (scramble-b (rest tup)
                                (cons (first tup) rev-pre)))))

(assert (= '(1 1 1 1 1 4 1 1 1 9)
           (scramble-b '(1 1 1 3 4 2 1 1 9 2) '())))
(assert (= '(1 1 1 1 1 1 1 1 1)
           (scramble-b '(1 2 3 4 5 6 7 8 9) '())))
(assert (= '(2 2 1 1 1 1 1 2 8 2)
           (scramble-b '(2 3 1 2 3 4 1 8 2 10) '(2 1))))


(defn scramble
  "Returns a scrambled `tup`. TODO: describe this weird stuff"
  [tup]
  (scramble-b tup '()))

(assert (= '(1 1 1 1 1 4 1 1 1 9)
           (scramble '(1 1 1 3 4 2 1 1 9 2))))
(assert (= '(1 1 1 1 1 1 1 1 1)
           (scramble '(1 2 3 4 5 6 7 8 9))))
(assert (= '(1 1 1 1 1 1 1 1 2 8 2)
           (scramble '(1 2 3 1 2 3 4 1 8 2 10))))



;; https://gist.github.com/michalmarczyk/486880

(defmacro letrec [bindings & body]
  (let [bcnt (quot (count bindings) 2)
        arrs (gensym "bindings_array")
        arrv `(make-array Object ~bcnt)
        bprs (partition 2 bindings)
        bssl (map first bprs)
        bsss (set bssl)
        bexs (map second bprs)
        arrm (zipmap bssl (range bcnt))
        btes (map #(walk/prewalk (fn [f]
                                   (if (bsss f)
                                     `(aget ~arrs ~(arrm f))
                                     f))
                                 %)
                  bexs)]
    `(let [~arrs ~arrv]
       ~@(map (fn [s e]
                `(aset ~arrs ~(arrm s) ~e))
              bssl
              btes)
       (let [~@(mapcat (fn [s]
                         [s `(aget ~arrs ~(arrm s))])
                       bssl)]
         ~@body))))


(defn *multirember-f
  "Returns a function that returns `l` without all occurrences of `a`,
  using `test?` for comparison."
  [test?]
  (fn [a lat]
    (cond
      (empty? lat)          '()
      (test? (first lat) a) ((*multirember-f test?) a (rest lat))
      :else                 (cons (first lat)
                                  ((*multirember-f test?) a (rest lat))))))

(let [mr (*multirember-f =)]
  (assert (= '() (mr 'a '())))
  (assert (= '(b c d) (mr 'a '(a b a c a d a)))))
