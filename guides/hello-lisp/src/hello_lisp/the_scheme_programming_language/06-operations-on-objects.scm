(load "src/hello_lisp/the_scheme_programming_language/common.scm")


;;;; constants and quotation

(comment

 3.2
 #f
 #\c
 "hi"
 #vu8(3 4 5)

 ;; quote

 (+ 2 3)
 '(+ 2 3)

 ;; quasiquote, unquote, unquote-splicing

 `(+ 2 ,(* 3 4) ,@'(6 7))

)


;;;; generic equivalence and type predicates

(comment

 ;; identity

 ;; same constants and other literal forms are usually identical

 (eq? 'a 'a)
 (eq? 1 1)
 (eq? "foo" "foo")
 (eq? '(1 2 3) '(1 2 3))

 ;; but constructed ones are distinct

 (eq? (list 1 2 3) (list 1 2 3))


 ;; equivalence: for characters (as in char=?) and numbers (as in =)

 (eqv? 1 1)
 (eqv? #\a #\a)


 ;; equivalence: the same structure and contents

 (equal? (list 1 2 3) (list 1 2 3))


 ;; type predicates

 (boolean? #t)
 (null? '())
 (pair? '(1 . 2))

 ;; numeric tower
 (integer? 1)
 (rational? 1/2)
 (real? 2.3)
 (complex? 3+2i)
 (number? 1)

 (char? #\a)
 (string? "foo")
 (vector? #(1 2 3))
 (procedure? cons)

 (use-modules (rnrs bytevectors)
              (rnrs hashtables))

 (bytevector? #vu8())
 (hashtable? (make-eq-hashtable))

)


;;;; lists and pairs

;; cons, car, cdr (and the ilk); set-car!, set-cdr!; list, length, list-ref,
;; list-tail, append; memq, memv, member (using eq?, eqv?, and equal?); memp,
;; remp, remq, remv, remove; filter, partition, find, list-sort

(comment

 ;; proper lists

 (list 1 2 3)

 ;; dotted pairs

 '(1 2 . 3)


 (list-ref '(1 2 3 4 5) 3)

 (list-tail '(1 2 3 4 5) 3)

 (append '(1 2 3) '(4 5 6))


 (use-modules (rnrs lists))

 (memp even? '(1 2 3 4))

 (remp even? '(1 2 3 4))

 (filter odd? '(1 2 3 4))

 (partition odd? '(1 2 3 4))

 (find odd? '(1 2 3 4))

 (list-sort < '(3 4 2 1 2 5))

 ;; alists: assq, assv, assoc; assp

)


;;;; numbers

(comment

 (exact? 1)
 (inexact? 1.3)
 (inexact? 1.3e-5)

 (real-part 2+12i)
 (imag-part 2+12i)
 (magnitude 2@3)
 (angle 2@3)

 (finite? 1)
 (nan? 1)
 +inf.0
 -inf.0
 +nan.0
 -nan.0

 ;; forced (in)exactness
 #e1.1e-3
 #i1

 #b101
 #o755
 #xdeadbeef

 ;; comparison: >, <, >=, <=, =

 (= 1 1.0)

 ;; arithmetic: +, -, *, /; quotient, remainder, modulo; div, mod,
 ;; div-and-mod; div0, mod0, div0-and-mod0

 (positive? 1)
 (negative? -1)
 (even? 2)
 (odd? 1)

 ;; inexact, exact; truncate, floor, ceiling, round, abs, min, gcd, lcm; expt,
 ;; sqrt; rationalize, numerator, denominator; real-part, imag-part,
 ;; magnitude, angle, make-rectangular, make-polar; exp, log, sin, cos, tan,
 ;; asin, acos, atan

 ;; bitwise: maybe TODO

 ;; conversions: string->number, number->string

)


;;;; fixnums: maybe TODO (not likely)


;;;; flonums: maybe TODO (even less likely)


;;;; characters

;; char-upcase, char-downcase, char-titlecase; char->integer, integer->char


;;;; strings

(comment

 (string #\a #\b #\c)
 (make-string 5)

 ;; maybe later

)


;;;; vectors

;; constant time access; seems like fixed-size


;;;; bytevectors (oh noes)


;;;; symbols

;; interned by reader, efficient comparison with eq?


;;;; booleans

(comment

 ;; this is it

 #t
 #f

)


;;;; hashtables

(comment

 (define h (make-eq-hashtable))

 (hashtable-mutable? h)
 (hashtable-hash-function h)
 (hashtable-equivalence-function h)

 (hashtable-set! h 'foo "bar")

 (hashtable-ref h 'foo #:nope)

)


;;;; enumerations

;; ordered sets of symbols

(comment

 (use-modules (rnrs enums))

 (define-enumeration foo
   (bar baz frob quux)
   make-foo)

 (foo bar)
 (foo frob)
 (make-foo quux baz)
 (enum-set->list (make-foo bar frob))

 ;; more set stuff here

)
