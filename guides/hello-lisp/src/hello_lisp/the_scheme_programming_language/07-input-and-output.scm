(load "src/hello_lisp/the_scheme_programming_language/common.scm")


;; all input/output operations are performed through ports (pointers to a
;; stream of data, possibly infinite), e.g. from file

;; ports can be binary (of bytes) and textual (of characters); transcoder may
;; be specified to determine how characters are encoded in the bytes from the
;; port; standard codecs are latin-1 (1 byte per character), utf-8 (1-4 bytes)
;; and utf-16 (2-4 bytes); transcoder also specifies eol (end of line) style
;; (lf, cr, nel, ls, crlf, crnel, none); and error-handling mode as well
;; (ignore, raise or replace)

;; port that has reached the end of the data stream, returns the eof object on
;; read (eof-object? predicate)

;; a port may be buffered; modes are block, line or none

;; standard ports: current input, current output, current error (textual)


;;;; transcoders

(comment

 (use-modules (rnrs io ports))

 (make-transcoder (utf-8-codec)
                  (eol-style lf)
                  (error-handling-mode replace))

)


;;;; opening files

(comment

 (use-modules (rnrs io ports))

 (file-options)
 (file-options no-create)

 (buffer-mode line)

 (define i (open-file-input-port
            "src/hello_lisp/the_scheme_programming_language/common.scm"
            (file-options no-create)
            (buffer-mode line)
            (make-transcoder (utf-8-codec)
                  (eol-style lf)
                  (error-handling-mode replace))))

 (close-input-port i)

)


;;;; standard ports

(comment

 ;; existing standard ports

 (current-input-port)
 (current-output-port)
 (current-error-port)

 ;; new (binary) standard ports

 (standard-input-port)
 (standard-output-port)
 (standard-error-port)

)


;;;; string and bytevector ports


;;;; opening custom ports


;;;; port operations

(comment

 (define ip (open-file-input-port
             "src/hello_lisp/the_scheme_programming_language/in.txt"))

 (define op (open-file-output-port
             "src/hello_lisp/the_scheme_programming_language/out.txt"))

 (port? ip)

 (input-port? ip)

 (output-port? ip)

 (binary-port? ip)

 (textual-port? ip)

 (port-position ip)


 (define tip (transcoded-port ip (make-transcoder (utf-8-codec))))

 (port-has-port-position? tip)
 (port-has-set-port-position!? tip)

)
