(load "src/hello_lisp/the_scheme_programming_language/common.scm")

;;;; syntactic extension

;; user code do not need to distinguish core syntactic forms (special forms)
;; and syntactic extensions (macros); only Scheme implementation do --- to
;; expand them until only core forms remain

;; Scheme grammar

;; <program>             => <form>*
;; <form>                => <definition> | <expression>
;; <definition>          =>  <variable definition> | (begin <definition>*)
;; <variable definition> =>  (define <variable> <expression>)
;; <expression>          =>  <constant>
;;                       |   <variable>
;;                       |   (quote <datum>)
;;                       |   (lambda <formals> <expression> <expression>*)
;;                       |   (if <expression> <expression> <expression>)
;;                       |   (set! <variable> <expression>)
;;                       |   <application>
;; <constant>            =>  <boolean> | <number> | <character> | <string>
;; <formals>             =>  <variable>
;;                       |   (<variable>*)
;;                       |   (<variable> <variable>* . <variable>)
;; <application>         =>  (<expression> <expression>*)

;; `define-syntax' associates a syntactic transformation procedure
;; (transformer) with a keyword (a name of the syntactic extension)

(define-syntax fiat
  ;; a list of auxiliary keywords, e.g., `else' in `cond'
  (syntax-rules ()
    ;; rules (pattern/template pairs); _ (underscore) means the keyword; ...
    ;; (ellipsis) means zero or more of preceding forms
    [(_ ((var expr) ...) body1 body2 ...)
     ((lambda (var ...) body1 body2 ...) expr ...)]))

(define-syntax et
  (syntax-rules ()
    [(_) #t]
    [(_ e) e]
    [(_ e1 e2 e3 ...) (if e1 (and e2 e3 ...) #f)]))

(comment

 (fiat [(x 1)
        (y 2)]
       (+ x y))

 (et 1 2 3)
 (et 1 #f 3)

)


;;;; continuations

;; during the evaluation of an expression, interpreter must keep track of 1)
;; what to evaluate and 2) what to do with the value; the latter is a
;; continuation of a computation

;; the continuation of any expression can be captured with `call/cc'

;; continuations allow the implementation of nonlocal exits, backtracking,
;; coroutines, and multitasking

(comment

 ;; `k' is the current continuation, that will be passed to the lambda

 ;; each time `k' is applied to a value, it returns the value to the
 ;; continuation of the `call/cc' application, and the value becomes the value
 ;; of the `call/cc' application

 ;; if lambda returns without invoking `k', the value returned by lambda
 ;; becomes the value of the `call/cc' application

 (call/cc
  (lambda (k)
    (* 5 4)))

 (call/cc
  (lambda (k)
    (* 5 (k 4))))

 (+ 2
    (call/cc
     (lambda (k)
       (* 5 (k 4)))))

 ;; nonlocal exit from recursion

 (define product
   (lambda (ls)
     (call/cc
      (lambda (break)
        ;; named `let'
        (let f ([ls ls])
          (cond
           [(null? ls) 1]
           [(= (car ls) 0) (break 0)]
           [else (* (car ls) (f (cdr ls)))]))))))

 (product '(1 2 3 4 5))
 (product '(7 3 8 0 1 9 5))

)


;;;; continuation passing style

;; TODO


;;;; internal definitions

;; local to the containing expression


;;;; libraries (R6RS)

;; a library exports a set of identifiers (keywords or variables), defined
;; within the library or imported from some other library

;; not working in Guile 3.0.7
(library (my-math)
  (export square dot (rename et und))
  (import (rnrs))

  (define square
    (lambda (x) (* x x)))

  (define (dot v1 v2)
    (fold-left + (map * v1 v2)))

  (define-syntax et
    (syntax-rules ()
      [(_) #t]
      [(_ e) e]
      [(_ e1 e2 e3 ...) (if e1 (and e2 e3 ...) #f)])))
