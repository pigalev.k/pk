;;;; general info

;; memory is managed automatically (garbage collector)

;; all objects are first-class

;; Scheme is a call-by-value language, but mutable objects can be pointers to
;; actual storage

;; at the heart is a small set of syntactic forms, a set of extended forms
;; derived from them, and a set of primitive functions

;; Scheme is homoiconic (code is data); a program is an object; syntactic
;; extension facilities allow the definition of new syntactic forms (in terms
;; of existing syntactic forms and procedures)

(define-syntax comment
  (syntax-rules ()
    ((_ . body) '())))

;; variables and keywords are lexically scoped, and programs are
;; block-structured

;; procedures can be recursive; tail recursion (recursive tail-call ---
;; returning result of invoking another procedure, e.g., itself) is used to
;; express iteration, it is automatically optimized

(comment

 ;; all calls to `f' are in tail position, but calls to `g' are not

 (lambda () (f (g)))
 (lambda () (if (g) (f) (f)))
 (lambda () (let ([x 4]) (f)))
 (lambda () (or (g) (f)))

)

;; arbitrary control structures can be defined with continuations;
;; continuation is a procedure that represents the remainder of a program at
;; the given point in the program


;;;; Scheme syntax

(comment

 ;; numbers

 -123
 1/2
 1.3
 1.3e5
 1.3-2.7i
 -1.2@73

 ;; characters

 #\a

 ;; strings

 "a string"

 ;; symbols
 'foo
 '>
 'nil

 ;; booleans

 #t
 #f

 ;; lists; square brackets can be used instead of parentheses

 '()
 '(1 2 3 "foo")
 '[1 2 3 "foo"]
 [+ 1 2 3]

 ;; vectors

 #(1 2 3)

 ;; #f is false, anything else is true (including 0, (), and nil)

 (if #f 'ok 'not-ok)
 (if #t 'ok 'not-ok)

 ;; comments line

 #|
 comments block (can be nested)
 |#

 #;(comments following form)

 #;#;#;(or)(several)(forms)

 ;; some objects do not have printed representation

 #'+

)


;;;; naming conventions

;; predicate?

;; type-action

;; conversion-from->to

;; side-effect!
