(load "src/hello_lisp/the_scheme_programming_language/common.scm")


;;;; procedure application

;; order of evaluation is unspecified

(comment

 (+ 3 4)

 (apply min '(1 2 -23 34 0 11))

)


;;;; sequencing

;; there is implicit begin for bodies of lambda, case-lambda, let, let*,
;; letrec, letrec*; result clauses of cond, case and do

(comment

 (begin
   (display "foo\n")
   (display "bar\n"))

)


;;;; conditionals

;; if, cond, case, when, unless; not, and, or


;;;; recursion and iteration

(comment

 ;; named let is a general-purpose iteration and recursion construct

 (define divisors
   (lambda (n)
     (let f ([i 2])
       (cond
        [(>= i n) '()]
        [(integer? (/ n i)) (cons i (f (+ i 1)))]
        [else (f (+ i 1))]))))

 (divisors 32)

 ;; do allows a restricted form of iteration

 (define divisors
   (lambda (n)
     (do ([i 2 (+ i 1)]
          [ls '()
              (if (integer? (/ n i))
                  (cons i ls)
                  ls)])
         ((>= i n) ls))))

 (divisors 32)

)


;;;; mapping and folding

(comment

 (map abs '(1 -2 3 -4 5 -6))

 (for-each display '(1 -2 3 -4 5 -6))


 (use-modules (rnrs lists))

 (exists symbol? '(1.0 #\a "hi" '()))
 (for-all symbol? '(a b c d))

 (fold-left cons '() '(1 2 3 4))

 (fold-right cons '() '(1 2 3 4))


 (use-modules (srfi srfi-43))

 (vector-map (lambda (index value) (abs value)) #(1 -2 3 -4 5 -6))

 (vector-for-each (lambda (index value) (display value)) #(1 -2 3 -4 5 -6))

 (string-for-each display "foo")

)


;;;; continuations

;; call/cc, call-with-current-continuation

;; not quite clear yet

(comment

 (define member
   (lambda (x ls)
     ;; call/cc obtains its continuation (as a procedure) and passes it to the
     ;; lambda; each time this procedure is applied to zero or more values, it
     ;; returns the values to the continuation of the call/cc application
     ;; (when break is called, it returns its arguments as the values of the
     ;; application of call/cc)
     (call/cc
      (lambda (break)
        (do ([ls ls (cdr ls)])
            ((null? ls) #f)
          (when (equal? x (car ls))
            (break ls)))))))

 (member 'd '(a b c))
 (member 'b '(a b c))

 ;; the current continuation is typically represented internally as a stack of
 ;; procedure activation records, and obtaining the continuation involves
 ;; encapsulating the stack within a procedural object


 ;; offers "protection" from continuation invocation; allows to define code
 ;; that will be performed no matter how control enters or leaves the
 ;; protected body

 (let ([p (open-input-file "input-file")])
  (dynamic-wind
    (lambda () #f)
    (lambda () (process p))
    (lambda () (close-port p))))

 (define-syntax unwind-protect
   (syntax-rules ()
     [(_ body cleanup ...)
      (dynamic-wind
        (lambda () #f)
        (lambda () body)
        (lambda () cleanup ...))]))

 ((call/cc
   (let ([x 'a])
     (lambda (k)
       (unwind-protect
        (k (lambda () x))
        (set! x 'b))))))

 ;; some more complicated stuff here; TODO: explore

)


;;;; delayed evaluation

;; delay and force; see lazy-sequences in the elisp workspace for example now
;; to implement lazy sequences with them

(comment

 (define d (delay (+ 1 2 3)))

 (force d)

)


;;;; multiple values

;; values and call-with-values

(comment

 (values 1 2 3)

 (let [(ls '(1 2 3 4 5))]
   (values (car ls) (cdr ls)))

 (call-with-values
     (lambda () (values 'bond 'james))
   (lambda (x y) (cons y x)))

 (call-with-values values list)

)


;;;; eval

(comment

 (use-modules (rnrs eval)
              (rnrs r5rs))

 (environment)

 (eval '(+ 3 4) (environment '(rnrs)))
 (eval '(+ 3 4) (scheme-report-environment 5))

)
