;;;; interacting with Scheme

;; REPL: reads forms (expressions) one by one, evaluates and prints the results

;; `M-x geiser' to start one

;; load: reads and evaluates forms from a file

(load "src/hello_lisp/the_scheme_programming_language/common.scm")

(comment

 (+ 1 2 3)

)


;;;; simple expressions

;; strings and numbers are constants (evaluate to themselves)

;; lists and symbols are evaluated as procedure applications or variables;
;; quoting allows to treat them as data (constants) instead


;;;; procedure application

;; evaluate all elements of a list form (procedure name and its argument
;; forms) to find the procedure and the argument values; order of evaluation
;; is not specified

;; apply the procedure to the argument values

;; special syntactic forms have their own different rules of evaluation


;;;; variables and `let' expressions

;; symbols are evaluated as variables, to variable values

;; `let' binds variables (symbols) to their values; scope of the binding is
;; the body of the `let' form

(comment

 (let [(x 2)
       (y 3)
       (+ *)]
   (+ x y))

)


;;;; lambda expressions

(comment

 ;; lambda expression binds its formal parameters (symbols) to the actual
 ;; arguments (values); the scope of the bindings is the lambda body

 (let [(x 2)
       (square (lambda (x) (* x x)))]
   (square x))

 ;; `x' is bound, `y' is free in the lambda expression

 (let* [(y 3)
        (foo (lambda (x) (* x y)))]
   (foo 4))

 ;; TODO: named `let'

)


;;;; top-level definitions

(define a 1)

;; the same

(define square
  (lambda (x) (* x x)))

(define (square x)
  (* x x))

(comment

 a
 (square 12)

)


;;;; conditional expressions

(comment

 ;; predicates

 (null? '())
 (number? 1.2)
 (string? "foo")
 (eqv? 'a 'a)
 (eqv? "foo" "foo")

 ;; logical operators

 (or 1 2)
 (and 1 2)
 (not 1)

 ;; conditionals

 (if (or (> 1 3 ) #t 3)
     'ok
     'not-ok)

 (define (what-kind? x)
   (cond
    [(> x 0) 'positive]
    [(< x 0) 'negative]
    [else 'zero]))

 (what-kind? 1)
 (what-kind? -1)
 (what-kind? 0)

 (let ([x 4] [y 5])
   (case (+ x y)
     [(1 3 5 7 9) 'odd]
     [(0 2 4 6 8) 'even]
     [else 'out-of-range]))

)


;;;; simple recursion

;; have at least one base case and at least one recursion step

(define count
  (lambda (ls)
    (if (null? ls)
        0
        (+ (count (cdr ls)) 1))))

(comment

 (count '(1 2 3 4 5))

 ;; `letrec'

 (letrec ([sum (lambda (ls)
                 (if (null? ls)
                     0
                     (+ (car ls) (sum (cdr ls)))))])
   (sum '(1 2 3 4 5)))

)


;;;; assignment

;; changes values of existing bindings (top-level, let-bound or lambda-bound
;; variables)

(comment

 (define abcde '(a b c d e))
 (set! abcde (cdr abcde))
 abcde

)
