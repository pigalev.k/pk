(load "src/hello_lisp/the_scheme_programming_language/common.scm")


;;;; variable references

(comment

 (define a 1)
 a

)


;;;; lambda

(comment

 ((lambda (x) (* x x)) 5)

)


;;;; case-lambda

;; allows to define multiple arities (besides varargs)

(comment

 (define make-list
   (case-lambda
     [(n) (make-list n #f)]
     [(n x)
      (do ([n n (- n 1)] [ls '() (cons x ls)])
          ((zero? n) ls))]))

 (make-list 3)
 (make-list 3 "yeah")

)


;;;; local binding

;; let, let*, letrec, letrec*


;;;; multiple values

;; like destructuring bind: let-values, let*-values

(comment

 (use-modules (srfi srfi-11))

 (let-values ([(a b) (values 1 2)]
              [c (values 1 2 3)])
   (list a b c))

 (let*-values ([(a b) (values 1 2)]
               [(a b) (values b a)])
   (list a b))

)


;;;; variable definitions

;; define


;;;; assignment

;; set!
