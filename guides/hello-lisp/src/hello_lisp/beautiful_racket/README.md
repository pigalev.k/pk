# Beatiful Racket

A follow-up on

- "Beautiful Racket: An introduction to language-oriented programming using
  Racket" by Matthew Butterick, version 1.6 https://beautifulracket.com/

## Overview

Racket allows to easily create new (domain-specific) languages that compile to
Racket. To implement one, it is usually sufficient to provide the Racket
runtime with
- **reader**, that reads program text to data structures, i.e., S-expressions;
  it puts the program into the proper form
- **expander**, that expands (transforms, e.g., using a macro) the data just
  read to a Racket program; it gives meaning to these forms --- ensures that
  each identifier has a binding, so the program can be evaluated

## Setup

- Install Racket

- Install `beautiful-racket` package

```
raco pkg install --auto beautiful-racket
```

- Test the installation

```
racket -l br/test
```

## Getting Started

Open a `.rkt` file, `C-c C-z` to start a REPL.

Use `C-c C-l` (`geiser-load-file`) for `#lang` shebangs to work, or use
`racket <file>.rkt` from the shell; just evaluating the buffer will not work.
