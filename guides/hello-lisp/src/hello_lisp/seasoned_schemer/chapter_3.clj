(ns hello-lisp.seasoned-schemer.chapter-3
  (:require
   [hello-lisp.functions :refer [letrec member? intersect intersect-all]]))


;; Source: The Seasoned Schemer

;; # 13. Hop, Skip, and Jump

(comment

  ;; 1. What is the value of

  (let [set1 '(tomatoes and macaroni)
        set2 '(macaroni and cheese)]
    (intersect set1 set2))

  ;; '(and macaroni).


  ;; 2. Is `intersect` and old acquaintance?

  ;; Yes, we have known `intersect` for as long as we have known `union`.


  ;; 3. Write `intersect`.

  (defn intersect-1 [set1 set2]
    (cond
      (empty? set1)               '()
      (member? (first set1) set2) (cons (first set1)
                                        (intersect-1 (rest set1) set2))
      :else                       (intersect-1 (rest set1) set2)))

  (intersect-1 '(tomatoes and macaroni)
               '(macaroni and cheese))


  ;; 4. What would this definition look like if we hadn't forgotten The Twelfth
  ;; Commandment?

  (defn intersect-2 [set1 set2]
    (letfn [(I [set]
              (cond
                (empty? set)               '()
                (member? (first set) set2) (cons (first set)
                                                 (I (rest set)))
                :else                      (I (rest set))))]
      (I set1)))

  (intersect-2 '(tomatoes and macaroni)
               '(macaroni and cheese))


  ;; 5. Do you also recall `intersect-all`?

  ;; Isn't that the function that `intersect`'s a list of sets?

  (defn intersect-all-1 [l-set]
    (cond
      (empty? (rest l-set)) (first l-set)
      :else                 (intersect (first l-set)
                                       (intersect-all-1 (rest l-set)))))

  (intersect-all-1 '((tomatoes and macaroni)
                     (macaroni and cheese)
                     (bread and butter with macaroni)))


  ;; 6. Why don't we ask

  (empty? l-set)

  ;; There is no need to ask this question because The Little Schemer assumes
  ;; that the list of sets for `intersect-all` is not empty.


  ;; 7. How could we write a version of `intersect-all` that makes no
  ;; assumptions about the list of sets?

  ;; That's easy: We ask (`empty?` `l-set`) and then just use the two
  ;; `cond`-lines from the earlier `intersect-all`:

  (defn intersect-all-2 [l-set]
    (cond
      (empty? l-set)        '()
      (empty? (rest l-set)) (first l-set)
      :else                 (intersect (first l-set)
                                       (intersect-all-2 (rest l-set)))))

  (intersect-all-2 '())
  (intersect-all-2 '((tomatoes and macaroni)
                     (macaroni and cheese)
                     (bread and butter with macaroni)))


  ;; 8. Are you sure that this definition is okay?

  ;; Yes? No?


  ;; 9. Are there two base cases for just one argument?

  ;; No, the first question is just to make sure that `l-set` is not empty
  ;; before the function goes through the list of sets.


  ;; 10. But once we know it isn't empty we never have to ask the question
  ;; again.

  ;; Correct, because `intersect-all` does not recur when it knows that the
  ;; `rest` of the list is empty.


  ;; 11. What should we do then?

  ;; Ask the question once and use the old version of `intersect-all` if the
  ;; list is not empty.


  ;; 12. And how would you do this?

  ;; Could we use another function?


  ;; 13. Where do we place the function?

  ;; Should we use `letrec`?


  ;; 14. Yes, the new version of `intersect-all` could hide the old one inside a
  ;; `letrec`.

  (defn intersect-all-3 [l-set]
    (letfn [(intersect-all [l-set]
              (cond
                (empty? (rest l-set)) (first l-set)
                :else                 (intersect (first l-set)
                                                 (intersect-all
                                                  (rest l-set)))))]
      (cond
        (empty? l-set) '()
        :else          (intersect-all l-set))))

  (intersect-all-3 '((tomatoes and macaroni)
                     (macaroni and cheese)
                     (bread and butter with macaroni)))

  ;; Could we have used `A` as the name of the function that we defined with
  ;; `letrec`?

  ;; Sure, `intersect-all` is just a better name, though a bit long for these
  ;; boxes.

  (defn intersect-all-4 [l-set]
    (letfn [(A [l-set]
              (cond
                (empty? (rest l-set)) (first l-set)
                :else                 (intersect (first l-set)
                                                 (A (rest l-set)))))]
      (cond
        (empty? l-set) '()
        :else          (A l-set))))

  (intersect-all-4 '((tomatoes and macaroni)
                     (macaroni and cheese)
                     (bread and butter with macaroni)))

  ;; Great! We are pleased to see that you are comfortable with `letrec`.


  ;; 15. One more time: we can use whatever name we want for such a minor
  ;; function if nobody else relies on it.

  ;; Yes, because `letrec` hides definitions, and the names matter only inside
  ;; of `letrec`.


  ;; 16. Is this similar to

  (fn [x y] M)

  ;; Yes, it is. The names `x` and `y` matter only inside of `M`, whatever `M`
  ;; is. And in

  (letrec [x F y G] M)

  ;; the names `x` and `y` matter only inside of `F`, `G`, and `M`, whatever
  ;; `F`, `G`, and `M` are.


  ;; 17. Why do we ask (`empty?` `l-set`) before we use `A`?

  ;; The question (`empty?` `l-set`) is not a part of `A`. Once we know that the
  ;; list of sets is non-empty, we need to check for only the list containing a
  ;; single set.


  ;; 18. What is

  (let [l-set '((3 mangos and)
                (3 kiwis and)
                (3 hamburgers))]
    (intersect-all l-set))

  ;; '(3).


  ;; 19. What is

  (let [l-set '((3 steaks and)
                (no food and)
                (three baked potatoes)
                (3 diet hamburgers))]
    (intersect-all l-set))

  ;; '().


  ;; 20. What is

  (let [l-set '((3 mangoes and)
                ()
                (3 diet hamburgers))]
    (intersect-all l-set))

  ;; '().


  ;; 21. Why is this?

  ;; The intersection of '(3 mangos and), '(), and '(3 diet hamburgers) is the
  ;; empty set.


  ;; 22. Why is this?

  ;; When there is an empty set in the list of sets, (`intersect-all` `l-set`)
  ;; returns the empty set.


  ;; 23. But this does not show how `intersect-all` determines that the
  ;; intersection is empty.

  ;; No, it doesn't. Instead, it keeps `intersect`ing the empty set with some
  ;; set until the list of sets is exhausted.


  ;; 24. Wouldn't it be better if `intersect-all` didn't have to `intersect`
  ;; each set with the empty set and if it could instead say "This is it: the
  ;; result is '() and that's all there is to it."

  ;; That would be an improvement. It could save us a lot of work if we need to
  ;; determine the result of (`intersect` `l-set`).


  ;; 25. Well, there actually is a way to say such things.

  ;; There is?


  ;; 26. Yes, we haven't shown you `letcc` yet.

  ;; Why haven't we mentioned it before?


  ;; 27. Because we did not need it until now.

  ;; How would `intersect-all` use `letcc`?

  ;; (Note: Clojure (i.e., JVM) does not have first-class continuations and
  ;; tail-call optimization so using continuation-passing style is not
  ;; encouraged. We will use simple workarounds instead where possible.)


  ;; 28. That's simple. Here we go:

  ;; (imagine that Clojure has `letcc` implemented)

  (defn intersect-all-5 [l-set]
    (letcc hop
           (letfn [(A [l-set]
                     (cond
                       (empty? (first l-set)) (hop '())
                       (empty? (rest l-set))  (first l-set)
                       :else                  (intersect (first l-set)
                                                         (A (rest l-set)))))])))

  ;; and an equivalent working one:

  (defn intersect-all-6 [l-set]
    (letfn [(hop [x] x)
            (A [l-set]
              (println "A called")
              (cond
                (empty? (first l-set)) (do
                                         (println "hop called")
                                         (hop '()))
                (empty? (rest l-set))  (first l-set)
                :else                  (intersect (first l-set)
                                                  (A (rest l-set)))))]
      (cond
        (empty? l-set) '()
        :else          (A l-set))))

  (intersect-all-6 '((tomatoes and macaroni)
                     (macaroni and cheese)
                     (bread and butter with macaroni)))
  (intersect-all-6 '(()
                     (tomatoes and macaroni)
                     (bread and butter with macaroni)))


  ;; Alonzo Church (1903-1995) would have written:

  ;; (imagine that Clojure has `call-with-current-continuation` implemented)

  (defn intersect-all-7 [l-set]
    (call-with-current-continuation
     (fn [hop]
       (letfn [(A [l-set]
                 (cond
                   (empty? (first l-set)) (hop '())
                   (empty? (rest l-set))  (first l-set)
                   :else                  (intersect (first l-set)
                                                     (A (rest l-set)))))]))))


  ;; 29. Doesn't this look easy?

  ;; We prefer the `letcc` version. It only has two new lines.


  ;; 30. Yes, we added one line at the beginning and one `cond`-line inside the
  ;; minor function `A`.

  ;; It really looks like _three_ lines.


  ;; 31. A line in a `cond` is not line, even if we need more than one line to
  ;; write it down. How do you like the first new line?

  ;; The first new line with `letcc` looks pretty mysterious.


  ;; 32. But the first `cond`-line in `A` should be obvious: we ask one extra
  ;; question (`empty?` (`first` `l-set`)), and if it is true, `A` uses `hop` as
  ;; if it were a function.

  ;; Correct: `A` will `hop` to the right place. How does this `hop`ping work?


  ;; 33. Now that is a different question. We could just try and see.

  ;; Why don't we try it with an example?


  ;; 34. What is the value of

  (let [l-set '((3 mangoes and)
                ()
                (3 diet hamburgers))]
    (intersect-all-6 l-set))

  ;; Yes, that is a good example. We want to know how things work when one of
  ;; the sets is empty.


  ;; 35. So how do we determine the answer for (`intersect-all-6` `l-set`)?

  ;; Well, the first thing in `intersect-all-6` is `letcc`, which looks
  ;; mysterious.


  ;; 36. Since we don't know what this line does, it is probably best to ignore
  ;; it for the time being. What next?

  ;; We ask (`empty?` `l-set`), which in this case is not true.


  ;; 37. And so we go on and...

  ;; ...determine the value of

  (A l-set)

  ;; where `l-set` is the list of sets.


  ;; 38. What is the next question?

  (empty? (first l-set))


  ;; 39. Is this true?

  ;; No, (`first` `l-set`) is the set '(3 mangos and).


  ;; 40. Is this why we ask

  (empty? (rest l-set))

  ;; Yes, and it is not true either.


  ;; 41. `:else`?

  ;; Of course.


  ;; 42. And now we recur?

  ;; Yes, we remember that (`first` `l-set`) is '(3 mangos and), and that we
  ;; must `intersect` this set with the result of (`A` (`rest` `l-set`)).


  ;; 43. How do we determine the value of (`A` `l-set`) where `l-set` is '(() (3
  ;; diet hamburgers))?

  ;; We ask

  (empty? (first l-set))


  ;; 44. Which is true.

  ;; And now we need to know the value of

  (hop '())


  ;; 45. Recall that we wanted to `intersect` the set '(3 mangos and) with the
  ;; result of the natural recursion?

  ;; Yes.


  ;; 46. And that there is `letcc` which we ignored earlier?

  ;; Yes, and

  (hop '())

  ;; seems to have something to do with this line.


  ;; 47. It does. The two lines are like a compass needle and the North
  ;; Pole. The North Pole attracts one end of a compass needle, regardless of
  ;; where in the world we are.

  ;; What does that mean?


  ;; 48. It basically means: "Forget what we had remembered to do after leaving
  ;; behind `letcc` and before encountering (`hop` `M`). And then act as if we
  ;; were to determine the value of (`letcc` `hop` `M`), whatever `M` is."

  ;; But how do we forget something?


  ;; 49. Easy: we do not do it.

  ;; You mean we do not `intersect` the set '(3 mangos and) with the result of
  ;; the natural recursion?


  ;; 50. Yes. And even better, when we need to determine the value of something
  ;; that looks like

  (letcc hop '())

  ;; we actually know its answer.

  ;; The answer should be '(), shouldn't it?


  ;; 51. Yes, it is '().

  ;; That's what we wanted.


  ;; 52. And it is what we got.

  ;; Amazing! We did not do any `intersect`ing at all.


  ;; 53. That's right: we said `hop` and arrived at the right place with the
  ;; result.

  ;; This is neat. Let's `hop` some more!


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Fourteenth Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Use `letcc` to return values abruptly and promptly.


  ;; 54. How about determining the value of

  (let [l-set '((3 steaks and)
                (no food and)
                (three baked potatoes)
                (3 diet hamburgers))]
    (intersect-all-6 l-set))

  ;; We ignore `letcc`.


  ;; 55. And then?

  ;; We determine the value of

  (A l-set)

  ;; because `l-set` is not empty.


  ;; 56. What do we ask next?

  (empty? (first l-set))

  ;; which is false.


  ;; 57. And next?

  (empty? (rest l-set))

  ;; which is false.


  ;; 58. And next?

  ;; We remember to `intersect` '(3 steaks and) with the result of the natural
  ;; recursion:

  (let [l-set '((3 steaks and)
                (no food and)
                (three baked potatoes)
                (3 diet hamburgers))]
    (A (rest l-set)))


  ;; 59. What happens now?

  ;; We ask the same questions as above and find out that we need to `intersect`
  ;; the set '(no food and) with the result of

  (let [l-set '((three baked potatoes)
                (3 diet hamburgers))]
    (A l-set))


  ;; 60. And afterward?

  ;; We ask the same questions as above and find out that we need to `intersect`
  ;; the set '(three baked potatoes) with tre result of

  (let [l-set '((3 diet hamburgers))]
    (A l-set))


  ;; 61. And then?

  ;; We ask

  (empty? (first l-set))

  ;; which is false.


  ;; 62. And then?

  ;; We ask

  (empty? (rest l-set))

  ;; which is true.


  ;; 63. And so we know what is the value of

  (let [l-set '(3 diet hamburgers)]
    (A l-set))

  ;; Yes, it is '(3 diet hamburgers).


  ;; 64. Are we done now?

  ;; No! With '(3 diet hamburgers) as the value, we now have three `intersect`s
  ;; to go back and pick up. We need to:
  ;; a. `intersect` '(three baked potatoes) with '(3 diet hamburgers);
  ;; b. `intersect` '(no food and) with the value of a;
  ;; c. `intersect` '(3 steaks and) with the value of b.
  ;; And then, at the end, we must not forget about `letcc`.


  ;; 65. Yes, so what is

  (let [set1 '(three baked potatoes)
        set2 '(3 diet hamburgers)]
    (intersect set1 set2))

  ;; '().


  ;; 66. So are we done?

  ;; No, we need to `intersect` this set with '(no food and).


  ;; 67. Yes, so what is

  (let [set1 '(no food and)
        set2 '()]
    (intersect set1 set2))

  ;; '().


  ;; 68. So are we done now?

  ;; No, we still need to `intersect` this set with '(3 steaks and).


  ;; 69. But this is also empty.

  ;; Yes, it is.


  ;; 70. So are we done?

  ;; Almost, but there is still the mysterious `letcc` that we ignored
  ;; initially.


  ;; 71. Oh, yes. We must now determine the value of

  (letcc hop '())

  ;; That's correct. But what does this line do now that we did not use `hop`?


  ;; 72. Nothing.

  ;; What do you mean, nothing?


  ;; 73. When we need to determint the value of

  (letcc hop '())

  ;; there is nothing left to do. We know the value.

  ;; You mean, it is '() again?


  ;; 74. Yes, it is '() again.

  ;; That's simple.


  ;; 75. Isn't it?

  ;; Except that we needed to `intersect` the empty set several times with a set
  ;; before we could say that the result of `intesect-all` was the empty set.


  ;; 76. Is it a mistake of `intersect-all`?

  ;; Yes, and also a mistake of `intersect`.


  ;; 77. In what sense?

  ;; We could have defined `intersect` so that it would not do anything when its
  ;; second argument is the empty set.


  ;; 78. Why its second argument?

  ;; When `set1` is finally empty, it could be because it is always empty of
  ;; because `intersect` has looked at all of its arguments. But when `set2` is
  ;; empty, `intersect` should not look at any elements in `set1` at all; it
  ;; knows the result!


  ;; 79. Should we have defined `intersect` with an extra question about `set2`?

  (defn intersect-3 [set1 set2]
    (letfn [(I [set1]
              (cond
                (empty? set1)               '()
                (member? (first set1) set2) (cons (first set1)
                                                  (I (rest set1)))
                :else                       (I (rest set1))))]
      (cond
        (empty? set2) '()
        :else         (I set1))))

  (intersect-3 '(tomatoes and macaroni)
               '(bread and butter with macaroni))
  (intersect-3 '()
               '(bread and butter with macaroni))

  ;; Yes, that helps a bit.


  ;; 80. Would it make you happy?

  ;; Actually, no.


  ;; 81. You are not easily satisfied.

  ;; Well, `intersect` would immediately return the correct result but this
  ;; still does not work right with `intersect-all`.


  ;; 82. Why not?

  ;; When one of the `intersect`s return '() in `intersect-all`, we know the
  ;; result of `intersect-all`.


  ;; 83. And shouldn't `intersect-all` say so?

  ;; Yes, absolutely.


  ;; 84. Well, we could build in a question that looks at the result of
  ;; `intersect` and `hop`s if necessary?

  ;; But somehow that looks wrong.


  ;; 85. Why wrong?

  ;; Because `intersect` asks this very same question. We would just duplicate
  ;; it.


  ;; 86. Got it. You mean that we should have a version of `intersect` that
  ;; `hop`s all the way over all the `intersect`s in `intersect-all`.

  ;; Yes, that would be great.


  ;; 87. We can have this.

  ;; Can `letcc` do this? Can we skip and jump from `intersect`?


  ;; 88. Yes, we can use `hop` even in `intersect` if we want to jump.

  ;; But how would this work? How can `intersect` know where to `hop` to when
  ;; its second set is empty?


  ;; 89. Try this first: make `intersect` a minor function of `intersect-all`
  ;; using `I` as its name.

  (defn intersect-all-8 [l-set]
    (letfn [(hop [x] x)
            (A [l-set]
              (cond
                (empty? (first l-set)) (hop '())
                (empty? (rest l-set))  (first l-set)
                :else                  (I (first l-set)
                                          (A (rest l-set)))))
            (I [s1 s2]
              (letfn [(J [s1]
                        (cond
                          (empty? s1)             '()
                          (member? (first s1) s2) (J (rest s1))
                          :else                   (cons (first s1)
                                                        (J (rest s1)))))]
                (cond
                  (empty? s2) '()
                  :else       (J s1))))]
      (cond
        (empty? l-set) '()
        :else (A l-set))))

  (intersect-all-8 '((tomatoes and macaroni)
                     (macaroni and cheese)
                     (bread and butter with macaroni)))
  (intersect-all-8 '(()
                     (tomatoes and macaroni)
                     (bread and butter with macaroni)))





)
