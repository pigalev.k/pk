(ns hello-lisp.seasoned-schemer.chapter-2
  (:require
   [hello-lisp.functions :refer [multirember Y letrec member?
                                 *rember-f *multirember-f
                                 union pick]]))


;; Source: The Seasoned Schemer

;; # 12. Take Cover

(comment

  ;; 1. What is

  (let [a   'tuna
        lat '(shrimp salad tuna salad and tuna)]
    (multirember a lat))

  ;; '(shrimp salad salad and), but we already knew that from chapter 3.


  ;; 2. Does `a` change as `multirember` traverses `lat`?

  ;; No, `a` always stands for 'tuna.


  ;; 3. Well, wouldn't it be better if we did not have to remind `multirember`
  ;; for every natural recursion that `a` still stands for 'tuna?

  ;; Yes, it sure would be a big help in reading such functions, especially if
  ;; several things don't change.


  ;; 4. That's right. Do you think the following definition of `multirember` is
  ;; correct?

  (defn multirember-1 [a lat]
    ((Y (fn [mr]
          (fn [lat]
            (cond
              (empty? lat)      '()
              (= a (first lat)) (mr (rest lat))
              :else             (cons (first lat)
                                      (mr (rest lat)))))))
     lat))

  ;; Whew, the Y combinator in the middle looks difficult.


  ;; 5. What is this function?

  (def ???
    ((fn [le]
       ((fn [f] (f f))
        (fn [f]
          (le (fn [x] ((f f) x))))))
     (fn [length]
       (fn [l]
         (cond
           (empty? l) 0
           :else      (inc (length (rest l))))))))

  ;; It is the function `length` in the style of chapter 9, using Y.

  (def length-1
    (Y (fn [length]
         (fn [l]
           (cond
             (empty? l) 0
             :else      (inc (length-1 (rest l))))))))


  ;; 6. And what is special about it?

  ;; We do not use `def`/`defn` to make `length` recursive. Using `Y` on a
  ;; function that looks like `length` creates the recursive function.


  ;; 7. So is `Y` a special version of `def`/`defn`?

  ;; Yes, that's right. But we also agreed that the definition with `defn` is
  ;; easier to read than the definition with `Y`.


  ;; 8. That's right. And we therefore have another way to write this kind of
  ;; definition.

  (defn multirember-2 [a lat]
    ((letrec [mr (fn [lat]
                   (cond
                     (empty? lat)      '()
                     (= a (first lat)) (mr (rest lat))
                     :else             (cons (first lat)
                                             (mr (rest lat)))))]
             mr)
     lat))

  (multirember-2 'a '(a b c d a e a f))

  ;; (Clojure way is to use named `fn` for self-reference and `letfn` for mutual
  ;; reference instead of `letrec`, see https://clojure.org/reference/lisps. The
  ;; text (commandments, dialogues, ...)  will be left (mostly) as it is in the
  ;; book, but implementations will use the Clojure way where possible.)

  (defn multirember-3 [a lat]
    ((fn mr [lat]
       (cond
         (empty? lat)      '()
         (= a (first lat)) (mr (rest lat))
         :else             (cons (first lat)
                                 (mr (rest lat)))))
     lat))

  (multirember-3 'a '(a b c d a e a f))

  ;; But if all we want is a recursive function `mr`, why don't we use this?

  (defn mr-1 [lat]
    (cond
      (empty? lat)      '()
      (= a (first lat)) (mr (rest lat))
      :else             (cons (first lat)
                              (mr (rest lat)))))

  (defn multirember-4 [a lat]
    (mr lat))


  ;; 9. Because `defn` does not work here.

  ;; Why not?


  ;; 10. The definition of `mr` refers to `a` which stands for the atom that
  ;; `multirember` needs to remove from `lat`.

  ;; Okay, that's true, though obviously `a` refers to the first name in the
  ;; definition of the function `multirember`.


  ;; 11. Do you remember that names don't matter?

  ;; Yes, we said in chapter 9 that all names are equal. We can even change the
  ;; names, as long as we do it consistently.


  ;; 12. Correct. If we don't like `lat`, we can use `a-lat` in the definition
  ;; of `multirember` as long as we also re-name all occurrences of `lat` in the
  ;; body of the `defn`.

  ;; Yes, we could have used the following definition and nothing would have
  ;; changed.

  (defn multirember-5 [a a-lat]
    (mr a-lat))


  ;; 13. Correct again. And this means we should also be able to use `b` instead
  ;; of `a` because

  (defn id-1 [a] a)

  ;; is the same as

  (defn id-2 [b] b)

  ;; Yet if we used `b` in the definition of `multirember`

  (defn multirember-6 [b a-lat]
    (mr a-lat))

  ;; the `a` in `mr` would no longer make any sense.


  ;; 14. Yes: the name `a` makes sense only inside the definition of
  ;; `multirember`. In general, the names for a function's arguments make sense
  ;; only inside of `fn`/`defn`.

  ;; Okay, that explains things.


  ;; 15. And that is precisely why we need named `letrec`. What do you think is
  ;; the purpose of the nested box?

  ;; It separates the two parts of a `letrec`: the naming part, which is the
  ;; nested box, and the value part, which is `mr`.


  ;; 16. Is the nested box important otherwise?

  ;; No, the nested box is merely an annotation that we use to help distinguish
  ;; the two parts of `letrec`. Once we get accustomed to the shape of `letrec`,
  ;; we will stop drawing the inner box.


  ;; 17. What do we use the naming part for?

  ;; The naming part defines a recursive function though unlike `defn`ed
  ;; functions; a function defined in the naming part of `letrec` knows all the
  ;; arguments of all the surrounding `fn`-expressions.


  ;; 18. And the value part?

  ;; It tells us what the result of the `letrec` is. It may refer to the named
  ;; recursive function.


  ;; 19. Does this mean that

  (letrec [mr ...] mr)

  ;; defines and returns a recursive function?

  ;; Precisely. Isn't that a lot of parentheses for saying just that?


  ;; 20. Yes, but they are important.

  ;; Okay, let's go on.


  ;; 21. What is the value of

  ((letrec [mr ...] mr) lat)

  ;; It is the result of applying the recursive function `mr` to `lat`.


  ;; 22. What is the value of

  (let [a   'pie
        lat '(apple custard pie linzer pie torte)]
    (multirember-3 a lat))

  ;; '(apple custard linzer torte), but we already knew this.


  ;; 23. How can we determine this value?

  ;; That's more interesting.


  ;; 24. The first line in the definition of `multirember-3` is no
  ;; longer (`cond` ...) but ((`fn` `mr` ...) `lat`). What does this mean?

  ;; We said that it defines the recursive function `mr` and applies it to
  ;; `lat`.


  ;; 25. What is the first line in `mr`?

  ;; It is something we are quite familiar with: (`cond` ...). We noe ask
  ;; questions the way we did in chapter 2.


  ;; 26. What is the first question?

  (empty? lat)

  ;; which is false.


  ;; 27. And the next question?

  (= a (first lat))

  ;; which is false.


  ;; 28. Why?

  ;; Because `a` still stands for 'pie, and (`first` `lat`) is 'apple.


  ;; 29. That's correct: `mr` always knows about `a` which doesn't change while
  ;; we look through `lat`.

  ;; Yes.


  ;; 30. Is is as if `multirember` had defined a function `mr-pie` and had used
  ;; it on `lat`?

  (defn mr-pie-1 [lat]
    (cond
      (empty? lat)         '()
      (= (first lat) 'pie) (mr-pie-1 (rest lat))
      :else                (cons (first lat)
                                 (mr-pie-1 (rest lat)))))

  ;; Correct, and the good thing is that no other function can refer to
  ;; `mr-pie`.


  ;; 31. Why is `defn` underlined?

  ;; We use `defn` to express that the underlined definition does not actually
  ;; exist, but imagining it helps our understanding.


  ;; 32. Is it all clear now?

  ;; This is easy as 'apple 'pie.


  ;; 33. Would it make any difference if we changed the definition a little bit
  ;; more like this?

  (defn multirember-7 [a lat]
    (let [mr (fn mr [lat]
               (cond
                 (empty? lat)      '()
                 (= a (first lat)) (mr (rest lat))
                 :else             (cons (first lat)
                                         (mr (rest lat)))))]
      (mr lat)))

  (multirember-7 'a '(a b c d a e a f))

  ;; The difference between this and the previous definition isn't that
  ;; big. (Look at the third and last lines.)


  ;; 34. The first line in the `multirember`'s body is now of the shape

  (let [mr ...] (mr lat))

  ;; Yes, so `multirember` first defines the recursive function `mr` that knows
  ;; about `a`.


  ;; 35. And then?

  ;; The value part (body) of `let` uses `mr` on `lat`, so from here things
  ;; proceed as before.


  ;; 36. That's correct. Isn't `let` with named `fn` easy as pie?

  ;; We prefer '(linzer torte).


  ;; 37. Is it clear now what `let` with named `fn` does?

  ;; Yes, and it is better than `Y`.


  ;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Twelfth Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Use `letrec` to remove arguments that do not change for recursive
  ;; applications.


  ;; 38. How does `rember` relate to `multirember`?

  ;; The function `rember` removes the first occurrence of some given atom in a
  ;; list of atoms; `multirember` removes all occurrences.


  ;; 39. Can `rember` also remove numbers from a list of numbers or
  ;; S-expressions from a list of S-expressions?

  ;; Not really, but in The Little Schemer we defined the function `*rember-f`,
  ;; which the right argument could create those functions:

  (defn *rember-f-1 [test?]
    (fn [a l]
      (cond
        (empty? l)          l
        (test? (first l) a) (rest l)
        :else               (cons (first l)
                                  ((*rember-f-1 test?) a (rest l))))))


  ;; 40. Give a name to the function returned by

  (*rember-f =)

  (def rember-=-1 (*rember-f =))

  (rember-=-1 'a '(a b c d a e f))


  ;; 41. Is `rember-=` really `rember`?

  ;; It is, but hold on tight; we will see more of this in a moment.


  ;; 42. Can you define the function `*multirember-f` which relates to
  ;; `multirember` in the same way `*rember-f` relates to `rember`?

  ;; That is not difficult:

  (defn *multirember-f-1 [test?]
    (fn [a lat]
      (cond
        (empty? lat)          '()
        (test? (first lat) a) ((*multirember-f-1 test?) a (rest lat))
        :else                 (cons (first lat)
                                    ((*multirember-f-1 test?) a (rest lat))))))

  ((*multirember-f-1 =) 'a '(a b c d a e f))


  ;; 43. Explain in your words what `*multirember-f` does.

  ;; Here are ours: "The function `*multirember-f` accepts a function `test?`
  ;; and returns a new function. Let us call this latter function `m-f`. The
  ;; function `m-f` takes an atom `a` and a list of atoms `lat` and traverses
  ;; the latter. Any atom `b` in `lat` for which (`test?` `b`) is true, is
  ;; removed."


  ;; 44. Is it true that during this traversal the result of (`*multirember-f`
  ;; `test?`) is always the same?

  ;; Yes, it is always the function for which we just used the name `m-f`.


  ;; 45. Perhaps `*multirember-f` should name it `m-f`?

  ;; Could we use `letrec` (or its analog) for this purpose?


  ;; 46. Yes, we could define `*multirember-f` with `letrec` so that we don't
  ;; need to re-determine the value of (`*multirember-f` `test?`)

  (defn *multirember-f-2 [test?]
    (fn m-f [a lat]
      (cond
        (empty? lat)          '()
        (test? (first lat) a) (m-f a (rest lat))
        :else                 (cons (first lat)
                                    (m-f a (rest lat))))))

  ((*multirember-f-2 =) 'a '(a b c d a e f))

  ;; Is this a new use of `letrec`?


  ;; 47. No, it still just defines a recursive function and returns it.

  ;; True enough.


  ;; 48. What is the value of

  (let [test? =]
    (*multirember-f =))

  ;; It is the function `multirember`:

  (def multirember-8
    (let [mr (fn mr [a lat]
               (cond
                 (empty? lat)      '()
                 (= (first lat) a) (mr a (rest lat))
                 :else             (cons (first lat)
                                         (mr a (rest lat)))))]
      mr))

  (multirember-8 'a '(a b c d a e f))


  ;; 49. Did you notice that no `fn` surrounds the `let`?

  ;; It looks odd, but it is correct!


  ;; 50. Could we have used another name for the function named in `let`?

  ;; Yes, `mr` is `multirember`.


  ;; 51. Is this another way of writing the definition?

  (def multirember-9
    (let [multirember (fn mr [a lat]
                        (cond
                          (empty? lat)      '()
                          (= (first lat) a) (mr a (rest lat))
                          :else             (cons (first lat)
                                                  (mr a (rest lat)))))]
      multirember))

  (multirember-9 'a '(a b c d a e f))

  ;; Yes, this defines the same function.


  ;; 52. Since `letrec` defines a recursive function and since `def`/`defn`
  ;; pairs up names with values, we could eliminate `letrec` here, right?

  ;; Yes, we could and we would get back our old friend `multirember`.

  (defn multirember-10 [a lat]
    (cond
      (empty? lat) lat
      :else        (cond
                     (= (first lat) a) (multirember-10 a (rest lat))
                     :else             (cons (first lat)
                                             (multirember-10 a (rest lat))))))

  (multirember-10 'a '(a b c d a e f))


  ;; 53. Here  is `member?` again:

  (defn member?-1 [a lat]
    (cond
      (empty? lat)      false
      (= (first lat) a) true
      :else             (member?-1 a (rest lat))))

  (member?-1 'a '(b c d a e f))

  ;; So?


  ;; 54. What is the value of

  (let [a   'ice
        lat '(salad greens with pears brie cheese frozen yogurt)]
    (member? a lat))

  ;; false, ice cream is good, too.


  ;; 55. Is it true that `a` remains the same for all natural recursions while
  ;; we determine this value?

  ;; Yes, `a` is always 'ice. Should we use The Twelfth Commandment?


  ;; 56. Yes, here is one way of using `letrec` with this function:

  (defn member?-2 [a lat]
    ((let [yes? (fn yes? [l]
                  (cond
                    (empty? l)      false
                    (= (first l) a) true
                    :else           (yes? (rest l))))]
       yes?)
     lat))

  (member?-2 'a '(b c d a e f))

  ;; Do you also like this version?

  ;; Here is an alternative:

  (defn member?-3 [a lat]
    (let [yes? (fn yes? [l]
                 (cond
                   (empty? l)      false
                   (= (first l) a) true
                   :else           (yes? (rest l))))]
      (yes? lat)))

  (member?-3 'a '(b c d a e f))


  ;; 57. Did you notice that we no longer use nested boxes for `letrec`?

  ;; Yes. We are now used to the shape of `letrec` and won't confuse the naming
  ;; part (bindings) with the value part (body) anymore.


  ;; 58. Do these lists represent sets?

  '(tomatoes and macaroni)
  '(macaroni and cheese)

  ;; Yes, they are sets because no atom occurs twice in these lists.


  ;; 59. Do you remember what is

  (let [set1 '(tomatoes and macaroni casserole)
        set2 '(macaroni and cheese)]
    (union set1 set2))

  ;; '(tomatoes casserole macaroni and cheese).


  ;; 60. Write `union`.

  (defn union-1 [set1 set2]
    (cond
      (empty? set1)               set2
      (member? (first set1) set2) (union-1 (rest set1) set2)
      :else                       (cons (first set1)
                                        (union-1 (rest set1) set2))))

  (union-1 '(tomatoes and macaroni casserole)
           '(macaroni and cheese))


  ;; 61. Is it true that the value of `set2` always stays the same when
  ;; determining the value of (`union` `set1` `set2`)?

  ;; Yes, because `union` is like `rember` and `member?` in that it takes two
  ;; arguments but only changes one when recurring.


  ;; 62. Is it true that we can rewrite `union` in the same way as we rewrote
  ;; `rember`?

  ;; Yes, and it is easy now.

  (defn union-2 [set1 set2]
    (let [U (fn U [set]
              (cond
                (empty? set)               set2
                (member? (first set) set2) (U (rest set))
                :else                      (cons (first set)
                                                 (U (rest set)))))]
      (U set1)))

  (union-2 '(tomatoes and macaroni casserole)
           '(macaroni and cheese))


  ;; 63. Could we also have written it like this?

  (defn union-3 [set1 set2]
    (let [A (fn A [set]
              (cond
                (empty? set)               set2
                (member? (first set) set2) (A (rest set))
                :else                      (cons (first set)
                                                 (A (rest set)))))]
      (A set1)))

  (union-3 '(tomatoes and macaroni casserole)
           '(macaroni and cheese))

  ;; Yes.


  ;; 64. Correct: `A` is just a name like `U`. Does it matter what name we use?

  ;; Absolutely not, but choose names that matter to you and everyone else who
  ;; wants to enjoy your definitions.


  ;; 65. So why do we choose the name `U`?

  ;; To keep the boxes from getting too wide, we use single letter names within
  ;; `letrec` for such minor functions.


  ;; 66. Can you think of a better name for `U`?

  ;; This should be an old shoe by now.


  ;; 67. Now, does it work?

  ;; It should.


  ;; 68. Explain in your words how the new version of `union` works.

  ;; Our words: "First, we define another function `U` that `rest`s down `set`,
  ;; `cons`ing up all elements that are not a member of `set2`. Eventually `U`
  ;; will `cons` all these elements onto `set2`. Second, `union` applies `U` to
  ;; `set1`."


  ;; 69. How does `U` know about `set2`?

  ;; Since `U` is defined using `letrec` inside of `union`, it knows about all
  ;; the things that `union` knows about.


  ;; 70. And does it have to pass around `set2`?

  ;; No, it does not.


  ;; 71. How does `U` know about `member?`

  ;; Everyone knows the function `member?`.


  ;; 72. Does it mean that the definition of `union` depends on the definition
  ;; of `member?`

  ;; It does, but `member?` works, so this is no problem.


  ;; 73. Suppose we had defined `member?` as follows.

  (defn member?-4 [lat a]
    (cond
      (empty? lat)      false
      (= (first lat) a) true
      :else             (member?-4 (rest lat) a)))

  ;; But this would confuse `union`!


  ;; 74. Why?

  ;; Because this `member?` takes its arguments in a different order.


  ;; 75. What changed?

  ;; Now `member?` takes a list first and an atom second.


  ;; 76. Does `member?` work?

  ;; It works in that we can still use this new definition of `member?` to find
  ;; out whether or not some atom is in a list.


  ;; 77. But?

  ;; With this new definition, `union` will no longer work.


  ;; 78. Oh?

  ;; Yes, because `union` assumes that `member?` takes its arguments in a
  ;; certain order.


  ;; 79. Perhaps we should avoid this.

  ;; How?


  ;; 80. Well, `letrec` can define more than just a single function.

  ;; Nobody said so.


  ;; 81. Didn't you notice the extra pair of parentheses around the function
  ;; definitions in `letrec`?

  ;; Yes.


  ;; 82. With `letrec` we can define more than just one function by putting more
  ;; than one function definition between the extra pair of parentheses.

  ;; This could help with `union`.


  ;; 83. Here is a skeleton:

  (defn union-4 [set1 set2]
    (let ...
      (U set1)))

  ;; Fill in the dots.


  ;; 84.

  (defn union-5 [set1 set2]
    (let [mem? (fn mem? [a lat]
                 (cond
                   (empty? lat)      false
                   (= (first lat) a) true
                   :else             (mem? a (rest lat))))
          U    (fn U [set]
                 (cond
                   (empty? set)            set2
                   (mem? (first set) set2) (U (rest set))
                   :else                   (cons (first set)
                                                 (U (rest set)))))]
      (U set1)))

  (union-5 '(tomatoes and macaroni casserole)
           '(macaroni and cheese))


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Thirteenth Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Use `letrec` (or analog) to hide and to protect functions.


  ;; 85. Could we also have written this?

  (defn union-6 [set1 set2]
    (letfn [(M? [a lat]
              (cond
                (empty? lat)      false
                (= (first lat) a) true
                :else             (M? a (rest lat))))
            (U [set]
              (cond
                (empty? set)          set2
                (M? (first set) set2) (U (rest set))
                :else                 (cons (first set)
                                            (U (rest set)))))]
      (U set1)))

  (union-6 '(tomatoes and macaroni casserole)
           '(macaroni and cheese))

  ;; Presumably.


  ;; 86. Are we happy now?

  ;; Well, almost.


  ;; 87. Almost?

  ;; The definition of `member?` inside of `union` ignores The Twelfth
  ;; Commandment.


  ;; 88. It does?

  ;; Yes, the recursive call to `member?` passes along the parameter `a`.


  ;; 89. And its value does not change?

  ;; No, it doesn't!


  ;; 90. So we can write something like this?

  (defn union-7 [set1 set2]
    (letfn [(M? ...)
            (U [set]
              (cond
                (empty? set)          set2
                (M? (first set) set2) (U (rest set))
                :else                 (cons (first set)
                                            (U (rest set)))))]
      (U set1)))

  ;; Yes, and here is how we fill in the dots:

  (defn union-8 [set1 set2]
    (letfn [(M? [a lat]
              (letfn [(N? [lat]
                        (cond
                          (empty? lat)      false
                          (= (first lat) a) true
                          :else             (N? (rest lat))))]
                (N? lat)))
            (U [set]
              (cond
                (empty? set)          set2
                (M? (first set) set2) (U (rest set))
                :else                 (cons (first set)
                                            (U (rest set)))))]
      (U set1)))

  (union-8 '(tomatoes and macaroni casserole)
           '(macaroni and cheese))


  ;; 91. Now we are happy, right?

  ;; Yes!


  ;; 92. Did you notice that `set2` is not an argument of `U`?

  ;; It doesn't have to be because `union` knows about `set2` and `U` inside of
  ;; `union`.


  ;; 93. Do we know enough about `union` now?

  ;; Yes, we do!


  ;; 94. Do we deserve a break now?

  ;; We deserve dinner or something equally substantial.


  ;; 95. True, but hold the dessert.

  ;; Why?


  ;; 96. We need to protect a few more functions.

  ;; Which ones?


  ;; 97. Do you remember `two-in-a-row?`

  ;; Sure, it is the function that checks whether some atom occurs twice in a
  ;; row in some list. It is a perfect candidate for protection.


  ;; 98. Yes, it is. Can you explain why?

  ;; Here are our words: "Auxiliary functions like `two-in-a-row-b?` are always
  ;; used on specific values that make sense for the functions we want to
  ;; define. To make sure that these minor functions always receive the correct
  ;; values, we hide such functions where they belong."


  ;; 99. So how do we hide `two-in-a-row-b?`

  ;; The same way we hide other functions:

  (defn two-in-a-row?-1 [lat]
    (letfn [(W [a lat]
              (cond
                (empty? lat) false
                :else        (or (= (first lat) a)
                                 (W (first lat) (rest lat)))))]
      (cond
        (empty? lat) false
        :else        (W (first lat) (rest lat)))))

  (two-in-a-row?-1 '(a  b c d e))
  (two-in-a-row?-1 '(a b b c d e))


  ;; 100. Does the minor function `W` need to know the argument `lat` of
  ;; `two-in-a-row?`

  ;; No, `W` also takes `lat` as an argument.


  ;; 101. Is it then okay to hide `two-in-a-row-b?` like this:

  (def two-in-a-row?-2
    (letfn [(W [a lat]
              (cond
                (empty? lat) false
                :else        (or (= (first lat) a)
                                 (W (first lat) (rest lat)))))]
      (fn [lat]
        (cond
          (empty? lat) false
          :else        (W (first lat) (rest lat))))))

  (two-in-a-row?-2 '(a  b c d e))
  (two-in-a-row?-2 '(a b b c d e))

  ;; Yes, it is a perfectly safe way to protect the minor function `W`. It is
  ;; still not visible to anybody but `two-in-a-row?` and works perfectly.


  ;; 102. Good, let's look at another pair of functions.

  ;; Let's guess: it's `sum-of-prefixes-b` and `sum-of-prefixes`.


  ;; 103. Protect `sum-of-prefixes-b`.

  (defn sum-of-prefixes-1 [tup]
    (letfn [(S [sss tup]
              (cond
                (empty? tup) '()
                :else        (cons (+ sss (first tup))
                            (S (+ sss (first tup)) (rest tup)))))]
      (S 0 tup)))

  (sum-of-prefixes-1 '(1 1 1 1 1))


  ;; 104. Is `S` similar to `W` in that it does not rely on `sum-of-prefixes`'s
  ;; argument?

  ;; It is. We can also hide it without putting it inside `defn` (using `def`)
  ;; but we don't need to practice that anymore.


  ;; 105. We should also protect `scramble-b`. Here is the skeleton:

  (defn scramble-1 [tup]
    (letfn [(P ...)]
      (P tup '())))

  ;; Fill in the dots.

  (defn scramble-2 [tup]
    (letfn [(P [tup rp]
              (cond
                (empty? tup) '()
                :else        (cons (pick (first tup)
                                         (cons (first tup) rp))
                                   (P (rest tup) (cons (first tup) rp)))))]
      (P tup '())))

  (assert (= '(1 1 1 1 1 1 1 1 1)
             (scramble-2 '(1 2 3 4 5 6 7 8 9))))


  ;; 106. Can we define `scramble` using the following skeleton?

  (def scramble-3
    (letfn [(P ...)]
      (fn [tup]
        (P tup '()))))

  ;; Yes, but can't this wait?


  ;; 107. Yes, it can. Now it _is_ time for dessert.

  ;; How about black currant sorbet?

)
