(ns hello-lisp.seasoned-schemer.chapter-1
  (:require
   [hello-lisp.functions :refer [member? is-first? pick
                                 two-in-a-row-b? two-in-a-row?
                                 sum-of-prefixes-b sum-of-prefixes
                                 scramble-b scramble]]))


;; Source: The Seasoned Schemer

;; # 11. Welcome Back to the Show

(comment

  ;; 1. Welcome back.

  ;; It's a pleasure.


  ;; 2. Have you read The Little Schemer?

  ;; false.


  ;; 3. Are you sure you haven't read The Little Schemer?

  ;; Well,...


  ;; 4. Do you know about Lambda the Ultimate?

  ;; true.


  ;; 5. Are you sure you have read that much of The Little Schemer?

  ;; Absolutely. (If you are familiar with recursion and know that functions are
  ;; values, you may continue anyway.).


  ;; 6. Are you acquainted with `member?`

  (defn member-1 [a lat]
  (cond
    (empty? lat) false
    :else        (or (= (first lat) a)
                     (recur a (rest lat)))))

  ;; Sure, `member?` is a good friend.


  ;; 7. What is the value of

  (let [a   'sardines
        lat '(Italian sardines spaghetti parsley)]
    (member? a lat))

  ;; true, but this is not interesting.


  ;; 8. What is the value of

  (let [lat '(Italian sardines spaghetti parsley)]
    (two-in-a-row? lat))

  ;; false.


  ;; 9. Are `two-in-a-row?` and `member?` related?

  ;; Yes, both visit each element of a list of atoms up to some point. One
  ;; checks whether an atom is in a list, the other checks whether any atom
  ;; occurs twice in a row.


  ;; 10. What is the value of

  (let [lat '(Italian sardines sardines spaghetti parsley)]
    (two-in-a-row? lat))

  ;; true.


  ;; 11. What is the value of

  (let [lat '(Italian sardines more sardines spaghetti parsley)]
    (two-in-a-row? lat))

  ;; false.


  ;; 12. Explain precisely what `two-in-a-row?` does.

  ;; Easy. It determines whether any atom occurs twice in a row in a list of
  ;; atoms.


  ;; 13. Is this close to what `two-in-a-row?` should look like?

  (defn two-in-a-row?-1 [lat]
    (cond
      (empty? lat) ...
      :else        ... (two-in-a-row?-1 (rest lat)
                                 ...)))

  ;; That looks fine. The dots in the first line should be replaced by false.


  ;; 14. What should we do with the dots in the second line?

  ;; We know that there is at least one element in `lat`. We must find out
  ;; whether the next element in `lat`, if there is one, is identical to this
  ;; element.


  ;; 15. Doesn't this sound like we need a function to do this? Define it.

  (defn is-first?-1 [a lat]
    (cond
      (empty? lat) false
      :else        (= (first lat) a)))


  ;; 16. Can we now complete the definition of `two-in-a-row?`

  ;; Yes, now we have all the pieces and we just need to put them together:

  (defn two-in-a-row?-2 [lat]
    (cond
      (empty? lat) false
      :else        (or (is-first? (first lat) (rest lat))
                (two-in-a-row?-2 (rest lat)))))


  ;; 17. There is a different way to accomplish the same task.

  ;; We have seen this before: most functions can be defined in more than one
  ;; way.


  ;; 18. What does `two-in-a-row?` do when `is-first?` returns false?

  ;; It continues to search for two atoms in a row in the rest of `lat`.


  ;; 19. Is it true that `is-first?` may respond with false for two different
  ;; situations?

  ;; Yes, it returns false when `lat` is empty or when the first element in the
  ;; list is different from `a`.


  ;; 20. In which of the two cases does it make sense for `two-in-a-row?` to
  ;; continue the search?

  ;; In the second one only, because the rest of the list is not empty.


  ;; 21. Should we change the definitions of `two-in-a-row?` and `is-first?` in
  ;; such a way that `two-in-a-row?` leaves the decision of whether continuing
  ;; the search is useful to the revised version of `is-first?`

  ;; That's an interesting idea.


  ;; 22. Here is a revised version of `two-in-a-row?`

  (defn two-in-a-row?-3 [lat]
    (cond
      (empty? lat) false
      :else        (is-first-b? (first lat) (rest lat))))

  ;; Can you define the function `is-first-b?`, which is like `is-first?`, but
  ;; uses `two-in-a-row?` only when it is useful to resume the search?

  ;; That's easy. If `lat` is empty, the value of (`is-first-b?` `a` `lat`) is
  ;; false. If `lat` is non-empty and if (`=` (`first` `lat`) `a`) is not true,
  ;; it determines the value of (`two-in-a-row?` `lat`).

  (defn is-first-b?-1 [a lat]
    (cond
      (empty? lat) false
      :else        (or (= (first lat) a)
                (two-in-a-row?-3 lat))))


  ;; 23. Why do we determine the value of (`two-in-a-row?` `lat`) in
  ;; `is-first-b?`

  ;; If `lat` contains at least one atom and if the atom is not the same as `a`,
  ;; we must search for two atoms in a row in `lat`. And that's the job of
  ;; `two-in-a-row?`.


  ;; 24. When `is-first-b?` determines the value of (`two-in-a-row?` `lat`),
  ;; what does `two-in-a-row?` actually do?

  ;; Since `lat` is not empty, it will request the value
  ;; of (`is-first-b?` (`first` `lat`) (`rest` `lat`)).


  ;; 25. Does this mean we could write a function like `is-first-b?` that
  ;; doesn't use `two-in-a-row?` at all?

  ;; Yes, we could. The new function would recur directly instead of through
  ;; `two-in-a-row?`.


  ;; 26. Let's use the name `two-in-a-row-b?` for the new version of
  ;; `is-first-b?`.

  ;; That sounds like a good name.


  ;; 27. How would `two-in-a-row-b?` recur?

  ;; With (`two-in-a-row-b?` (`first` `lat`) (`rest` `lat`)), because that's the
  ;; way `two-in-a-row?` used `is-first-b?`, and `two-in-a-row-b?` is used in
  ;; its place now.


  ;; 28. So what is `a` when we are asked to determine the value of

  (two-in-a-row-b? a lat)

  ;; It is the atom that precedes the atoms in `lat` in the original list.


  ;; 29. Can you fill in the does in the following definition of
  ;; `two-in-a-row-b?`

  (defn two-in-a-row-b?-1 [preceding lat]
    (cond
      (empty? lat) false
      :else        ...
      (two-in-a-row-b?-1 (first lat) (rest lat))
      ...))

  ;; That's easy. It is just like `is-first?` except that we know what to do
  ;; when (`first` `lat`) is not equal to `preceding`:

  (defn two-in-a-row-b?-2 [preceding lat]
    (cond
      (empty? lat) false
      :else        (or (= (first lat) preceding)
                (two-in-a-row-b?-2 (first lat) (rest lat)))))


  ;; 30. What is the natural recursion in `two-in-a-row-b?`

  ;; The natural recursion is

  (two-in-a-row-b? (first lat) (rest lat))


  ;; 31. Is this unusual?

  ;; Definitely: both arguments change even though the function asks questions
  ;; about its second argument only.


  ;; 32. Why does the first argument to `two-in-a-row-b?` change all the time?as
  ;; the name of the argument says, the first argument is always the atom that
  ;; precedes the current `lat` in the list of atoms that `two-in-a-row?`
  ;; received.


  ;; 33. Now that we have `two-in-a-row-b?`, can you define `two-in-a-row?` a
  ;; final time?

  ;; Trivial:

  (defn two-in-a-row?-4 [lat]
    (cond
      (empty? lat) false
      :else        (two-in-a-row-b? (first lat) (rest lat))))


  ;; 34. Let's see one more time how `two-in-a-row?` works.

  ;; Okay.


  ;; 35.

  (let [lat '(b d e i i a g)]
    (two-in-a-row? lat))

  ;; This looks like a good example. Since `lat` is not empty, we need the value
  ;; of

  (let [preceding 'b
        lat       '(d e i i a g)]
    (two-in-a-row-b? preceding lat))


  ;; 36.

  (let [lat '(d e i i a g)]
    (empty? lat))

  ;; false.


  ;; 37.

  (let [preceding 'b
        lat       '(d e i i a g)]
    (= (first lat) preceding))

  ;; false, because 'd is not 'b.


  ;; 38. And now?

  ;; Next we need to determine the value of

  (let [preceding 'd
        lat       '(e i i a g)]
    (two-in-a-row-b? preceding lat))


  ;; 39. Does it stop here?

  ;; No, it doesn't. After determining that `lat` is not empty and
  ;; that (`=` (`first` `lat`) `preceding`) is not true, we must determine the
  ;; value of

  (let [preceding 'e
        lat       '(i i a g)]
    (two-in-a-row-b? preceding lat))


  ;; 40. Enough?

  ;; Not yet. We also need to determine the value of

  (let [preceding 'i
        lat       '(i a g)]
    (two-in-a-row-b? preceding lat))


  ;; 41. And?

  ;; Now (`=` (`first` `lat`) `preceding`) is true because `preceding` is 'i and
  ;; `lat` is '(i a g).


  ;; 42. So what is the value of

  (let [lat '(b d e i i a g)]
    (two-in-a-row? lat))

  ;; true.


  ;; 43. Do we now understand how `two-in-a-row?` works?

  ;; Yes, this is clear.


  ;; 44. What is the value of

  (let [tup '(2 1 9 17 0)]
    (sum-of-prefixes tup))

  ;; '(2 3 12 29 29).


  ;; 45.

  (let [tup '(1 1 1 1 1)]
    (sum-of-prefixes tup))

  ;; '(1 2 3 4 5).


  ;; 46. Should we try our usual strategy again?

  ;; We could. The function visits the elements of a tup, so it should follow
  ;; the pattern for such functions:

  (defn sum-of-prefixes-1 [tup]
    (cond
      (empty? tup) ...
      :else        ...
      (sum-of-prefixes-1 (rest tup))
      ...))


  ;; 47. What is a good replacement for the dots in the first line?

  ;; The first line is easy again. We must replace the dots with '(), because we
  ;; are building a list.


  ;; 48. Then how about the second line?

  ;; The second line is the hard part.


  ;; 49. Why?

  ;; The answer should be the sum of all the numbers that we have seen so far
  ;; `cons`ed onto the natural recursion.


  ;; 50. Let's do it!

  ;; The function does not know what all these numbers are. So we can't form the
  ;; sum of the prefix.


  ;; 51. How do we get around this?

  ;; The trick that we just saw should help.


  ;; 52. Which trick?

  ;; Well, `two-in-a-row-b?` receives two arguments and one tells it something
  ;; about the other.


  ;; 53. What does `two-in-a-row-b?`'s first argument say about the second
  ;; argument?

  ;; Easy: the first argument, `preceding`, always occurs just before the second
  ;; argument, `lat`, in the original list.


  ;; 54. So how does this help us with `sum-of-prefixes`?

  ;; We could define `sum-of-prefixes-b`, which receives `tup` and the sum of
  ;; all the numbers that precede `tup` in the tup that `sum-of-prefixes`
  ;; received.


  ;; 55. Let's do it!

  (defn sum-of-prefixes-b-1 [sonssf tup]
    (cond
      (empty? tup) '()
      :else        (cons (+ sonssf (first tup))
                  (sum-of-prefixes-b-1 (+ sonssf (first tup)) (rest tup)))))


  ;; 56. Isn't `sonssf` a strange name?

  ;; It is an abbreviation. Expanding it helps a lot: sum of numbers seen so
  ;; far.


  ;; 57. What is the value of

  (let [sonssf 0
        tup    '(1 1 1)]
    (sum-of-prefixes-b sonssf tup))

  ;; Since `tup` is not empty, we need to determine the value of

  (let [tup '(1 1)]
    (cons 1 (sum-of-prefixes-b 1 tup)))


  ;; 58. And what do we do now?

  ;; We `cons` 2 onto the value of

  (let [tup '(1)]
    (sum-of-prefixes-b 2 tup))


  ;; 59. Next?

  ;; We need to remember to `cons` the value 3 onto

  (let [tup '()]
    (sum-of-prefixes-b 3 tup))


  ;; 60. What is left to do?

  ;; We need to:
  ;; a. `cons` 3 onto '()
  ;; b. `cons` 2 onto the result of a
  ;; c. `cons` 1 onto the result of b.
  ;; And then we are done.


  ;; 61. Is `sonssf` a good name?

  ;; Yes, every natural recursion with `sum-of-prefixes-b` uses the sum of all
  ;; the numbers preceding `tup`.


  ;; 62. Define `sum-of-prefixes` using `sum-of-prefixes-b`.

  ;; Obviously the first sum for `sonssf` must be 0:

  (defn sum-of-prefixes-1 [tup]
    (sum-of-prefixes-b 0 tup))


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The Eleventh Commandment
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; Use additional arguments when a function needs to know what other arguments
  ;; to the function have been like so far.


  ;; 63. Do you remember what a tup is?

  ;; A tup is a list of numbers.


  ;; 64. Is '(1 1 1 3 4 2 1 1 9 2) a tup?

  ;; Yes, because it is a list of numbers.


  ;; 65. What is the value of

  (let [tup '(1 1 1 3 4 2 1 1 9 2)]
    (scramble tup))

  ;; '(1 1 1 1 1 4 1 1 1 9).


  ;; 66.

  (let [tup '(1 2 3 4 5 6 7 8 9)]
    (scramble tup))

  ;; '(1 1 1 1 1 1 1 1 1).


  ;; 67.

  (let [tup '(1 2 3 1 2 3 4 1 8 2 10)]
    (scramble tup))

  ;; '(1 1 1 1 1 1 1 1 2 8 2).


  ;; 68. Have you figured out what it does yet?

  ;; It's okay if you haven't. It's kind of crazy. Here's our explanation: "The
  ;; function `scramble` takes a non-empty tup in which no number is greater
  ;; than its own index, and returns a tup of the same length. Each number in
  ;; the argument is treates as a backward index from its own position to a
  ;; point earlier in the tup. The result at each position is found by counting
  ;; backward from the current position according to this index."

  ;; 69. If `l` is '(1 1 1 3 4 2 1 1 9 2), what is the prefix of '(4 2 1 1 9 2)
  ;; in `l`?

  ;; '(1 1 1 3 4), because the prefix contains the first element, too.


  ;; 70. And if `l` is '(1 1 1 3 4 2 1 1 9 2), what is the prefix of '(2 1 1 9
  ;; 2) in `l`?

  ;; '(1 1 1 3 4 2).


  ;; 71. Is it true that `scramble` must know something about the prefix for
  ;; every element of `tup`?

  ;; We said that it needs to know the entire prefix of each element so that it
  ;; can use the first element of `tup` as a backward index to `pick` the
  ;; corresponding number from this prefix.


  ;; 72. Does this mean we have to define another function that does most of the
  ;; work for `scramble`?

  ;; Yes, because `scramble` needs to collect information about the prefix of
  ;; each element in the same manner as `sum-of-prefixes`.


  ;; 73. What is the difference between `scramble` and `sum-of-prefixes`?

  ;; The former needs to know the actual prefix, the latter needs to know the
  ;; sum of the numbers in the prefix.


  ;; 74. What is

  (let [n   4
        lat '(4 3 1 1)]
    (pick n lat))

  ;; 1.


  ;; 75. What is

  (let [n   2
        lat '(2 4 3 1 1 1)]
    (pick n lat))

  ;; 4.


  ;; 76. Do you remember `pick` from chapter 4?

  ;; If you do, have an ice-cream. If you don't, here it is:

  (defn pick-1 [n lat]
    (cond
      (zero? (dec n)) (first lat)
      :else           (pick-1 (dec n) (rest lat))))


  ;; 77. Here is `scramble-b`:

  (defn scramble-b-1 [tup rev-pre]
    (cond
      (empty? tup) '()
      :else (cons (pick (first tup)
                        (cons (first tup) rev-pre))
                  (scramble-b-1 (rest tup)
                                (cons (first tup) rev-pre)))))

  ;; How do we get `scramble-b` started?

  ;; A better question: how does it work?


  ;; 78. What does `rev-pre` abbreviate?

  ;; That is always the key to these functions. Apparently, `rev-pre` stands for
  ;; reversed-prefix.


  ;; 79. If `tup` is '(1 1 1 3 4 2 1 1 9 2) and `rev-pre` is '(), what is the
  ;; reversed prefix of (`rest` `tup`)?

  ;; It is the result of `cons`ing (`first` `tup`) onto `rev-pre`: '(1).


  ;; 80. If `tup` is '(2 1 1 9 2) and `rev-pre` is '(4 3 1 1 1), what is the
  ;; reversed prefix of '(1 1 9 2), which is (`rest` `tup`)?

  ;; Since (`first` `tup`) is 2, it is '(2 4 3 1 1 1).


  ;; 81. Do we need to know what `rev-pre` is when `tup` is '()?

  ;; No, because we know the result of `scramble-b` when `tup` is the empty
  ;; list.


  ;; 82. How does `scramble-b` work?

  ;; The function `scramble-b` receives a tup and the reverse of its proper
  ;; prefix. If the tup is empty, it returns the empty list. Otherwise, it
  ;; constructs the reverse of the complete prefix and uses the first element of
  ;; tup as a backward index into the list. It then processes the rest of the
  ;; tup and `cons`es the two results together.


  ;; 83. How does `scramble` get `scramble-b` started?

  ;; Now, it's no big deal. We just give `scramble-b` the tup and the empty
  ;; list, which represents the reverse of the proper prefix of the tup:

  (defn scramble-1 [tup]
    (scramble-b tup '()))


  ;; 84. Let's try it.

  ;; That's a good idea.


  ;; 85. The function `scramble` is an unusual example. You may want to work
  ;; through it a few more times before we have a snack.

  ;; Okay.


  ;; 86. Tea time.

  ;; Don't eat too much. Leave some room for dinner.

)
