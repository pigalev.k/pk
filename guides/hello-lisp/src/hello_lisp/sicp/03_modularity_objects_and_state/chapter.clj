(ns hello-lisp.sicp.03-modularity-objects-and-state.chapter
  (:require
   [hello-lisp.sicp.01-building-abstractions-with-procedures.chapter
    :refer [average gcd sqrt square prime?]]
   [hello-lisp.sicp.02-building-abstractions-with-data.chapter
    :refer [enumerate-interval* filterv*]]
   [hello-lisp.sicp.helpers :refer [error]]
   [vlaaad.reveal :as reveal]))


;; assignment and local state

;; local state variables

(defn use-account
  "Returns a vector of functions to get the current balance, deposit and
  withdraw the `amount` of funds (returning the current balance) to
  and from the new account with initial `balance` that defaults to 0."
  ([]
   (use-account 0))
  ([balance]
   (let [account  (atom balance)
         peek     (fn [] @account)
         deposit  (fn [amount] (swap! account + amount))
         withdraw (fn [amount]
                    (if (>= @account amount)
                      (swap! account - amount)
                      "Insufficient funds"))]
     [peek deposit withdraw])))

(comment

  (let [[peek deposit withdraw] (use-account 100)]
    (println (peek))
    (println (deposit 125))
    (println (deposit 233))
    (println (withdraw 300))
    (println (withdraw 299))
    (println (peek)))

)

(defn use-account*
  "Returns a dispatch function to invoke operations such as get the
  current balance, deposit and withdraw the `amount` of
  funds (returning the current balance) to and from the new account
  with initial `balance` that defaults to 0. Uses the message-passing
  programming style."
  ([]
   (use-account* 0))
  ([balance]
   (let [account  (atom balance)
         dispatch (fn [operation]
                    (case operation
                      peek     (fn [] @account)
                      deposit  (fn [amount] (swap! account + amount))
                      withdraw (fn [amount]
                                 (if (>= @account amount)
                                   (swap! account - amount)
                                   "Insufficient funds"))
                      (format "Unknown operation: '%s'" operation)))]
     dispatch)))

(comment

  (def a1 (use-account* 120))

  ((a1 'peek))
  ((a1 'deposit) 125)
  ((a1 'withdraw) 100)
  ((a1 'withdraw) 500)
  ((a1 'peek))

  (a1 'foo)

)


;; modeling with mutable data


;; mutable list structure

;; we do not like mutation here; also no transient lists in Clojure


;; representing queues

(declare make-queue ;; constructor
         empty-queue? front-queue ;; accessors
         insert-queue! delete-queue!) ;; mutators

(defn make-queue
  "Returns a new queue (FIFO discipline) with items of `coll` added to
  it."
  ([] (make-queue nil))
  ([coll] (atom (into [] coll))))

(defn empty-queue?
  "Returns true if `queue` is empty."
  [queue]
  (empty? @queue))

(defn front-queue
  "Returns the item at the front of the `queue`."
  [queue]
  (if (empty-queue? queue)
    (error "the queue is empty" "front-queue")
    (first @queue)))

(defn insert-queue!
  "Inserts the `item` to the back of the `queue` and returns the
  modified queue."
  [queue item]
  (swap! queue conj item))

(defn delete-queue!
  "Removes the item from the front of the `queue` and returns the
  modified queue."
  [queue]
  (if (empty-queue? queue)
    (error "the queue is empty" "front-queue")
    (swap! queue subvec 1)))

(comment

  (def q1 (make-queue))
  (def q2 (make-queue '(1 2 3)))

  (empty-queue? q1)
  (empty-queue? q2)

  (front-queue q1)
  (front-queue q2)

  (insert-queue! q1 123)
  (insert-queue! q2 12)

  (delete-queue! q1)
  (delete-queue! q2)

)


;; representing tables

;; we already have maps in Clojure, thank you; so while the topic may be of
;; interest, it is not a priority

;; two-dimensional tables

;; yeah, they can be nested too


;; a simulator for digital circuits

;; wires "hold" signals, function boxes enforce the correct relationship among
;; the signals

;; because of the delays involved, the outputs may be generated at different
;; times

;; circuit language: primitive function boxes are primitive elements, wiring
;; boxes together is means of combination, specifying wiring patterns as
;; procedures is a means of abstraction

(declare make-wire get-signal set-signal! add-action! after-delay ;; wires
         or-gate and-gate inverter ;; primitive function boxes
         inverter-delay and-gate-delay or-gate-delay
         the-agenda make-agenda empty-agenda? ;; agenda
         first-agenda-item current-time segments set-segments!
         first-segment rest-segments
         remove-first-agenda-item! add-to-agenda! set-current-time!
         make-time-segment segment-time segment-queue) ;; time segments

(defn half-adder
  "Returns a new half-adder circuit with inputs `a` and `b` (bits to
  add), outputs `s` (sum) and `c` (carry-out bit)."
  [a b s c]
  (let [d (make-wire)
        e (make-wire)]
    (or-gate a b d)
    (and-gate a b c)
    (inverter c e)
    (and-gate d e s)
    :ok))

(defn full-adder
  "Returns a new full-adder circuit with inputs `a` and `b` (bits to
  add), `c-in` (carry-out bit from the previous position), outputs
  `c-out` and `sum`."
  [a b c-in sum c-out]
  (let [s (make-wire)
        c1 (make-wire)
        c2 (make-wire)]
    (half-adder b c-in s c1)
    (half-adder a s sum c2)
    (or-gate c1 c2 c-out)
    :ok))

;; primitive function boxes

(defn logical-not
  "Returns the result of applying logical NOT operation to the input
  signal `s`."
  [s]
  (cond
    (= s 0) 1
    (= s 1) 0
    :else (error (format "invalid signal '%s'" s) "logical-not")))

(defn logical-and
  "Returns the result of applying logical AND operation to the input
  signals `s1` and `s2`."
  [s1 s2]
  (cond
    (not (every? #{0 1} [s1 s2]))
    (error (format "invalid signals '%s', '%s'" s1 s2) "logical-and")
    (= 1 s1 s2) 1
    :else       0))

(defn logical-or
  "Returns the result of applying logical OR operation to the input
  signals `s1` and `s2`."
  [s1 s2]
  (cond
    (not (every? #{0 1} [s1 s2]))
    (error (format "invalid signals '%s', '%s'" s1 s2) "logical-or")
    (= 0 s1 s2) 0
    :else       1))

(defn inverter
  "Sets the signal on the `output` wire to the inversion of the signal
  on the `input` wire."
  [input output]
  (letfn [(set-output! [_ _ _ new-input-value]
            (let [new-output-value (logical-not new-input-value)]
              (after-delay inverter-delay
                           (fn [] (set-signal! output new-output-value)))))]
    (add-action! input set-output!)
    :ok))

(defn and-gate
  "Sets the signal on the `output` wire to the logical AND of the
  signals on the wires `a1` and `a2`."
  [a1 a2 output]
  (letfn [(set-output! [_ _ _ _]
            (let [new-output-value (logical-and (get-signal a1)
                                                (get-signal a2))]
              (after-delay and-gate-delay
                           (fn [] (set-signal! output new-output-value)))))]
    (add-action! a1 set-output!)
    (add-action! a2 set-output!)
    :ok))

(defn or-gate
  "Sets the signal on the `output` wire to the logical OR of the signals
  on the wires `o1` and `o2`."
  [o1 o2 output]
  (letfn [(set-output! [_ _ _ _]
            (let [new-output-value (logical-or (get-signal o1)
                                               (get-signal o2))]
              (after-delay or-gate-delay
                           (fn [] (set-signal! output new-output-value)))))]
    (add-action! o1 set-output!)
    (add-action! o2 set-output!)
    :ok))

;; representing wires

;; a wire is a computational object with two local state variables: a signal
;; value (initially 0) and a collection of action procedures to be run when the
;; signal changes value

(defn make-wire
  "Returns a new wire with initial signal value 0 and no action
  procedures."
  []
  (atom 0))

(defn get-signal
  "Returns the current value of the signal on the `wire`."
  [wire]
  @wire)

(defn set-signal!
  "Changes the value of the signal on the `wire` to the `new-value`."
  [wire new-value]
  (reset! wire new-value))

(defn add-action!
  "Adds a watcher `proc` under the `key` that will be executed when the
  signal on the `wire` changes value, and runs it once. If key is not
  supplied, the default one (`:default`) is used."
  ([wire key proc]
   (let [val @wire]
     (add-watch wire key proc)
     (proc key wire val val)))
  ([wire proc]
   (add-action! wire :default proc)))

;; the agenda

(defn after-delay
  "Plans the `action` to be executed after `delay`. Uses the global
  agenda."
  [delay action]
  (add-to-agenda! (+ delay (current-time the-agenda))
                  action
                  the-agenda))

(defn propagate
  "Executes each action of the agenda in turn. New actions can be added
  during simulation."
  []
  (if (empty-agenda? the-agenda)
    :done
    (let [first-item (first-agenda-item the-agenda)]
      (first-item)
      (remove-first-agenda-item! the-agenda)
      (recur))))

(defn probe
  "Adds a probe watcher to the `wire` identified by `name`, that prints
  the wire name, new signal value, and current simulation time."
  [name wire]
  (add-action! wire
               :probe
               (fn [_ _ _ _]
                 (println (format "%s at time %s: %s"
                                  name
                                  (current-time the-agenda)
                                  (get-signal wire))))))

;; implementing the agenda

;; the agenda is made up of time segments; each time segment is a pair
;; consisting of a number (the simulation time) and a queue that holds
;; procedures that are scheduled to be run during that time segment

(defn make-time-segment
  "Returns a new time segment for the `time` with the `queue` of
  actions."
  [time queue]
  (list time queue))

(defn segment-time
  "Returns the time of the segment `s`."
  [s]
  (first s))

(defn segment-queue
  "Returns the action queue of the segment `s`."
  [s]
  (second s))

(defn make-agenda
  "Returns a new agenda with current time 0 and no segments."
  []
  (atom {:current-time  0
         :time-segments nil}))

(defn current-time
  "Returns the current time of the `agenda`."
  [agenda]
  (:current-time @agenda))

(defn set-current-time!
  "Sets the current time of the `agenda` to the `time`."
  [agenda time]
  (swap! agenda assoc :current-time time))

(defn segments
  "Returns segments of the `agenda`."
  [agenda]
  (:time-segments @agenda))

(defn set-segments!
  "Sets the time segments of the `agenda` to `segments`."
  [agenda segments]
  (swap! agenda assoc :time-segments segments))

(defn first-segment
  "Returns the first segment of the `agenda`."
  [agenda]
  (first (segments agenda)))

(defn rest-segments
  "Returns the rest of `agenda`'s segments."
  [agenda]
  (rest (segments agenda)))

(defn empty-agenda?
  "Returns true if the `agenda` is empty (has no time segments)."
  [agenda]
  (empty? (segments agenda)))

(defn add-to-agenda!
  "Adds the `action` to the `agenda` to be executed at `time`."
  [time action agenda]
  (letfn [(belongs-before? [segments]
            (or (empty? segments)
                (< time (segment-time (first segments)))))
          (make-new-time-segment [time action]
            (let [q (make-queue)]
              (insert-queue! q action)
              (make-time-segment time q)))
          (add-to-segments! [segments]
            (if (= (segment-time (first segments)) time)
              (insert-queue! (segment-queue (first segments))
                             action)
              (let [tail (rest segments)]
                (if (belongs-before? tail)
                  (swap! agenda update :time-segments
                         #(cons (first %)
                                (cons (make-new-time-segment time action)
                                      (rest %))))
                  (recur tail)))))]
    (let [segments (segments agenda)]
      (if (belongs-before? segments)
        (set-segments! agenda (cons (make-new-time-segment time action)
                                    segments))
        (add-to-segments! segments)))))

(defn remove-first-agenda-item!
  "Removes the action at the front of the queue in the first time
  segment of the `agenda`. If the time segment becomes empty, removes
  it from the list of segments."
  [agenda]
  (let [q (segment-queue (first-segment agenda))]
    (delete-queue! q)
    (when (empty-queue? q)
      (set-segments! agenda (rest-segments agenda)))))

(defn first-agenda-item
  "Returns the action at the front of the queue in the first time
  segment of the `agenda` and updates the agenda's current time with
  the action's segment time. Signals an error if agenda is empty."
  [agenda]
  (if (empty-agenda? agenda)
    (error "agenda is empty" "first-agenda-item")
    (let [first-seg (first-segment agenda)]
      (set-current-time! agenda
                         (segment-time first-seg))
      (front-queue (segment-queue first-seg)))))

;; a sample simulation

(comment

  (def the-agenda (make-agenda))
  (reveal/inspect @the-agenda)

  (def inverter-delay 2)
  (def and-gate-delay 3)
  (def or-gate-delay 5)

  (def input-1 (make-wire))
  (def input-2 (make-wire))
  (def sum (make-wire))
  (def carry (make-wire))

  (probe 'sum sum)
  (probe 'carry carry)

  (or-gate input-1 input-2 sum)
  (set-signal! input-1 1)
  (propagate)
  (set-signal! input-1 0)
  (propagate)

  ;; not quite working; `sum` is always 0, but `carry` works fine; maybe
  ;; derefing atoms in watchers instead of using watcher parameters causes
  ;; this? (use refs and transactions?); still more likely that something is
  ;; borked in `add-to-agenda!`, it's quite dense --- maybe revise the
  ;; representation of time segments (use a map with action queues keyed by
  ;; time?)
  (half-adder input-1 input-2 sum carry)
  (set-signal! input-1 1)
  (propagate)
  (set-signal! input-2 1)
  (propagate)
  (set-signal! input-2 0)
  (propagate)
  (set-signal! input-1 0)
  (propagate)

)


;; propagation of constraints

;; a language that enables us to work in terms of *relations* (a generalization
;; of function) between quantities

;; the primitive elements of the language are *primitive constraints* (which
;; state that certain relations hold between quantities); the means of
;; combination are *constraint networks*, in which constraints are joined by
;; *connectors* --- objects that "hold" values that may participate in one or
;; more constraints; expressing combinations as procedures automatically
;; provides the means of abstraction

;; computation process: when a connector is given a value (by a user or by a
;; constraint box to which it is linked), it awakens all of its associated
;; constraints (except for the constraint that just awakened it) to inform them
;; that it has a value; each awakened constraint box then polls its connectors
;; to see if there is enough information to determine a value for a connector;
;; if so, the box sets that connector, which then awakens all of its associated
;; constraints, and so on

;; example: Celsius-Fahrenheit conversion: 9C = 5(F - 32)


;; implementing the constraint system

;; constructors are just so nice without this "make-" prefix

(declare connector has-value? get-value set-value! forget-value! ;; connectors
         connect
         multiplier adder constant ;; constraints
         inform-about-value inform-about-no-value
         celsius-fahrenheit-converter ;; constraint network
         probe for-each-except) ;; helpers

(defn celsius-fahrenheit-converter
  "Creates the constraint network for Celsius-Fahrenheit temperature
  conversion between `c` and `f`."
  [c f]
  (let [u (connector)
        v (connector)
        w (connector)
        x (connector)
        y (connector)]
    (multiplier c w u)
    (multiplier v x u)
    (adder v y f)
    (constant 9 w)
    (constant 5 x)
    (constant 32 y)
    :ok))

(defn has-value?
  "Returns true if `connector` has a value."
  [connector]
  (connector 'has-value?))

(defn get-value
  "Returns the current value of `connector`."
  [connector]
  (connector 'value))

(defn set-value!
  "Informs the `connector` that the `informant` is requesting to set its
  value to the `new-value`."
  [connector new-value informant]
  ((connector 'set-value!) new-value informant))

(defn forget-value!
  "Informs the `connector` that the `retractor` is requesting it to
  forget its current value."
  [connector retractor]
  ((connector 'forget) retractor))

(defn connect
  "Makes the `connector` to participate in the `new-constraint`."
  [connector new-constraint]
  ((connector 'connect) new-constraint))

(defn inform-about-value
  "Informs the `constraint` that there is a new value associated with
  some of its connectors."
  [constraint]
  (constraint :i-have-a-value))

(defn inform-about-no-value
  "Informs the `constraint` that some of its connectors lost its value."
  [constraint]
  (constraint :i-have-lost-my-value))

(defn adder
  "Returns a new adder constraint among summand connectors `a1` and `a2`
  and a `sum` connector."
  [a1 a2 sum]
  (letfn [(process-new-value []
            (cond
              (and (has-value? a1)
                   (has-value? a2))  (set-value! sum
                                                 (+ (get-value a1)
                                                    (get-value a2))
                                                 me)
              (and (has-value? a1)
                   (has-value? sum)) (set-value! a2
                                                 (- (get-value sum)
                                                    (get-value a1))
                                                 me)
              (and (has-value? a2)
                   (has-value? sum)) (set-value! a1
                                                 (- (get-value sum)
                                                    (get-value a2))
                                                 me)))
          (process-forget-value []
            (forget-value! sum me)
            (forget-value! a1 me)
            (forget-value! a2 me)
            (process-new-value))
          (me [request]
            (cond
              (= request :i-have-a-value)       (process-new-value)
              (= request :i-have-lost-my-value) (process-forget-value)
              :else                             (error
                                                 (format "unknown request: '%s'"
                                                         request) "adder")))]
    (connect a1 me)
    (connect a2 me)
    (connect sum me)
    me))

(defn multiplier
  "Returns a new multiplier constraint among multiplicand connectors
  `m1` and `m2` and a `product` connector."
  [m1 m2 product]
  (letfn [(process-new-value []
            (cond
              (or (and (has-value? m1)
                       (= (get-value m1)
                          0))
                  (and (has-value? m2)
                       (= (get-value m2)
                          0)))      (set-value! product 0 me)
              (and (has-value? m1)
                   (has-value? m2)) (set-value! product
                                                (* (get-value m1)
                                                   (get-value m2))
                                                me)
              (and (has-value? product)
                   (has-value? m1)) (set-value! m2
                                                (/ (get-value product)
                                                   (get-value m1))
                                                me)
              (and (has-value? product)
                   (has-value? m2)) (set-value! m1
                                                (/ (get-value product)
                                                   (get-value m2))
                                                me)))
          (process-forget-value []
            (forget-value! product me)
            (forget-value! m1 me)
            (forget-value! m2 me)
            (process-new-value))
          (me [request]
            (cond
              (= request :i-have-a-value)       (process-new-value)
              (= request :i-have-lost-my-value) (process-forget-value)
              :else                             (error
                                                 (format "unknown request: '%s'"
                                                         request)
                                                 "multiplier")))]
    (connect m1 me)
    (connect m2 me)
    (connect product me)
    me))

(defn constant
  "Returns a new constant constraint with `value` for the `connector`."
  [value connector]
  (letfn [(me [request]
            (error (format "unknown request: '%s'" request)
                   "constant"))]
    (connect connector me)
    (set-value! connector value me)
    me))

(defn probe
  "Prints a message about setting or forgetting the value of the
  `connector`, using `label` to describe it."
  [label connector]
  (letfn [(print-probe [value]
            (println (format "Probe: %s = %s" label value)))
          (process-new-value []
            (print-probe (get-value connector)))
          (process-forget-value []
            (print-probe "?"))
          (me [request]
            (cond
              (= request :i-have-a-value)       (process-new-value)
              (= request :i-have-lost-my-value) (process-forget-value)
              :else                             (error
                                                 (format "unknown request: '%s'"
                                                         request) "probe")))]
    (connect connector me)
    me))

(defn connector
  "Returns a new connector."
  []
  (let [value       (ref false)
        informant   (ref false)
        constraints (ref #{})]
    (letfn [(set-my-value [new-value setter]
              (cond
                (not (has-value? me))      (dosync
                                           (ref-set value new-value)
                                           (ref-set informant setter)
                                           (for-each-except setter
                                                            inform-about-value
                                                            @constraints))
                (not (= @value new-value)) (error
                                            (format
                                             "contradiction: '%s' and '%s'"
                                             @value new-value))
                :else                      :ignored))
            (forget-my-value [retractor]
              (if (= retractor @informant)
                (dosync
                 (ref-set informant false)
                 (for-each-except retractor
                                  inform-about-no-value
                                  @constraints))
                :ignored))
            (connect [new-constraint]
              (when-not (contains? @constraints new-constraint)
                (dosync
                 (alter constraints conj new-constraint)))
              (when (has-value? me)
                (inform-about-value new-constraint))
              :done)
            (me [request]
              (cond
                (= request 'has-value?) (boolean @informant)
                (= request 'value)      @value
                (= request 'set-value!) set-my-value
                (= request 'forget)     forget-my-value
                (= request 'connect)    connect
                (= request 'dump)       {:value       @value
                                         :informant   @informant
                                         :constraints @constraints}
                :else                   (error
                                         (format "unknown operation: '%s'"
                                                 request) "connector")))]
      me)))

(defn for-each-except
  "Applies `proc` to all items in the `coll` save `exception`."
  [exception proc coll]
  (loop [items coll]
    (cond
      (empty? items)              :done
      (= (first items) exception) (recur (rest items))
      :else                       (do
                                    (proc (first items))
                                    (recur (rest items))))))

;; using the constraint system

(comment

  (def C (connector))
  (def F (connector))
  (celsius-fahrenheit-converter C F)

  (probe "Celsius temp" C)
  (probe "Fahrenheit temp" F)

  (has-value? C)
  (has-value? F)

  (set-value! C 25 'user)
  (has-value? C)
  (has-value? F)
  (set-value! F 212 'user)

  (forget-value! C 'user)
  (set-value! F 212 'user)

  (has-value? C)
  (has-value? F)

)


;; concurrency: time is of the essence

;; maybe will explore some lower-level examples later; Clojure already provides
;; practical means to deal with concurrency


;; streams

;; streams as delayed lists

;; delayed evaluation:

;; lazy --- does not evaluate the body expression, forced on call
(fn [] (+ 2 3))
;; similar, but the result is computed once and cached when forced
(delay (+ 2 3))

;; strange arity issues arise if the macro var is declared before functions that
;; use it and the macro itself is defined after the functions; guess the cause
;; is that macro var is evaluated when evaluating function definitions (not when
;; running function bodies), and then it is unbound
(defmacro stream-cons
  "Returns a new stream with `head` and `tail`. Stream cons cell is
  represented as a delayed 2-element vector with the `head` for the
  first element and the `tail` --- stream of the rest of the elements
  --- for the second element. The empty stream is represented as nil."
  [head & tail]
  `(delay (vary-meta [~head ~@tail] assoc :type ::stream)))

(declare stream-first stream-rest
         the-empty-stream stream-empty?)

;; stream abstract data type interface satisfies these conditions:
;; (= x (stream-first (stream-cons x y)))
;; (= y (stream-rest (stream-cons x y)))

;; we can implement the same operations for streams as for lists; will be fine
;; if we can unify them (we will, later)

(defn stream-nth
  "Returns the element of `stream` with index `n`."
  [stream n]
  (if (= n 0)
    (stream-first stream)
    (recur (stream-rest stream) (dec n))))

(defn stream-map
  "Returns a new stream, created by applying `proc` to the each element
  of the `stream`."
  [proc stream]
  (if (stream-empty? stream)
    the-empty-stream
    (stream-cons (proc (stream-first stream))
                 (stream-map proc (stream-rest stream)))))

(defn stream-filter
  "Returns a new stream created from `stream`, but without elements for
  which `pred` evaluated falsey."
  [pred stream]
  (cond
    (stream-empty? stream)       the-empty-stream
    (pred (stream-first stream)) (stream-cons (stream-first stream)
                                              (stream-filter
                                               pred
                                               (stream-rest stream)))
    :else                        (recur pred (stream-rest stream))))

(defn stream-for-each
  "Consumes the `stream`, applying `proc` to each element for side
  effects."
  [proc stream]
  (if (stream-empty? stream)
    :done
    (do
      (proc (stream-first stream))
      (recur proc (stream-rest stream)))))

(defn stream-enumerate-interval
  "Returns a stream of all integers between `low` to
  `high` (inclusive)."
  [low high]
  (cond
    (> low high) nil
    :else        (stream-cons low (stream-enumerate-interval (inc low) high))))

(defn stream-take
  "Returns a stream with (at most) first `n` elements of the `stream`."
  [n stream]
  (if (or (stream-empty? stream)
          (= 0 n))
    the-empty-stream
    (stream-cons (stream-first stream)
                 (stream-take (dec n) (stream-rest stream)))))

(defn stream-drop
  "Returns a stream without first `n` elements of the `stream`."
  [n stream]
  (if (or (stream-empty? stream)
          (= 0 n))
    stream
    (recur (dec n) (stream-rest stream))))

(defn stream-print
  "Prints the `stream`."
  [stream]
  (println (force stream)))

(defmethod print-method ::stream
  [object writer]
  (.write writer "#stream ( ")
  (stream-for-each (fn [value]
                     (.write writer (str value " "))) object)
  (.write writer ")"))

;; with ordinary lists, both `first` and `rest` are evaluated at construction
;; time (in Clojure that is not so; they operate on sequences, which are lazy,
;; and collections are coerced to sequences first); with streams, the `rest` is
;; evaluated at access time (and maybe head too, as implemented below)

;; our implementation of streams will be based on `delay` macro;
;; evaluating (delay expr) does not evaluate expr, but returns a *delayed
;; object* (like a promise to evaluate expr at some future time). There is a
;; companion procedure `force` that takes a delayed object and performs the
;; evaluation of expr (forces the `delay` to fulfill its promise).

(defn stream-first
  "Realizes and returns the first element of the `stream`; only the
  first element is evaluated."
  [stream]
  (first (force stream)))

(defn stream-rest
  "Returns the rest of the `stream`; only the first element of the rest
  is evaluated."
  [stream]
  (second (force stream)))

(defn stream-empty?
  "Returns true if the `stream` is empty."
  [stream]
  (nil? stream))

(def the-empty-stream nil)

(comment

  (def implicit-powers-of-two* (->> (stream-cons 3 nil)
              (stream-cons 2)
              (stream-cons 1)))
  (type (force implicit-powers-of-two*))
  (meta (force implicit-powers-of-two*))
  (println (force implicit-powers-of-two*))

  (stream-first implicit-powers-of-two*)
  (stream-rest implicit-powers-of-two*)
  (stream-rest (stream-rest implicit-powers-of-two*))
  (stream-rest (stream-rest (stream-rest implicit-powers-of-two*)))
  (force (stream-rest implicit-powers-of-two*))
  (stream-first (stream-rest implicit-powers-of-two*))
  (stream-for-each println implicit-powers-of-two*)
  (stream-print (stream-take 2 implicit-powers-of-two*))
  (stream-print (stream-take 10 implicit-powers-of-two*))
  (stream-print (stream-drop 2 implicit-powers-of-two*))
  (stream-print (stream-drop 10 implicit-powers-of-two*))

  (stream-print (stream-map inc implicit-powers-of-two*))
  (stream-print (stream-enumerate-interval 1 3))
  (->> (stream-enumerate-interval 1 10)
       (stream-filter odd?)
       (stream-take 3)
       (stream-print))


  ;; a vector produced by eager `filterv*` (when asked for a prime, evaluates
  ;; and filters all numbers in the interval)
  (first (rest (filterv* prime?
                         (enumerate-interval* 10000 1000000))))
  ;; a stream produced by lazy `stream-filter` (when asked for a prime, consumes
  ;; numbers from interval until gets a prime, and returns it; does not evaluate
  ;; the rest of the interval if not asked)
  (stream-first
   (stream-rest
    (stream-filter prime? (stream-enumerate-interval 10000 1000000))))

)


;; implementing `delay` and `force`

(defmacro delay*
  "Constructs an object representing delayed computation. The result of
  forced computation is memoized and is not recomputed when forced
  again."
  [expr]
  `(vary-meta (memoize (fn* [] ~expr)) assoc :type ::delay*))

(defn delay?*
  "Returns true if `x` is an object representing delayed computation."
  [x]
  (= (type x) ::delay*))

(defn force*
  "Forces the delayed computation `delayed` created by `delay*` and
  returns its value, or `delayed` itself if it is not really
  delayed (just a value)."
  [delayed]
  (if (delay?* delayed)
    (delayed)
    delayed))

;; then we can implement streams in terms of these; the emptiness concept is the
;; same

(defmacro stream-cons*
  "The same as `stream-cons`, but uses `delay*`."
  [head tail]
  `(vary-meta [(delay* ~head) (delay* ~tail)] assoc :type ::stream*))

(defn stream-first*
  "The same as `stream-first`, but uses `force*`."
  [stream]
  (force* (first stream)))

(defn stream-rest*
  "The same as `stream-rest`, but uses `force*`."
  [stream]
  (force* (second stream)))


(defn stream*
  "Returns a new stream created from `coll`."
  [coll]
  (if (empty? coll) nil
      (stream-cons* (first coll)
                    (stream* (rest coll)))))

(defn stream-into*
  "Returns `coll` with all elements of `stream` added."
  [coll stream]
  (if (stream-empty? stream)
    coll
    (recur (conj coll (stream-first* stream))
           (stream-rest* stream))))

(defn stream-nth*
  "The same as `stream-nth`, but uses `stream-first*` and
  `stream-rest*`."
  [stream n]
  (if (= n 0)
    (stream-first* stream)
    (recur (stream-rest* stream) (dec n))))

(defn stream-map*
  "The same as `stream-map`, but uses `stream-cons*`, `stream-first*`
  and `stream-rest*`."
  [proc stream]
  (if (stream-empty? stream)
    the-empty-stream
    (stream-cons* (proc (stream-first* stream))
                  (stream-map* proc (stream-rest* stream)))))

(defn stream-map**
  "The same as `stream-map*`, but can process multiple streams of equal
  length."
  [proc & streams]
  (if (stream-empty? (first streams))
    the-empty-stream
    (stream-cons* (apply proc (map stream-first* streams))
                  (apply stream-map** proc (map stream-rest* streams)))))

(defn stream-filter*
  "The same as `stream-filter`, but uses `stream-cons*`, `stream-first*`
  and `stream-rest*`."
  [pred stream]
  (cond
    (stream-empty? stream)        the-empty-stream
    (pred (stream-first* stream)) (stream-cons* (stream-first* stream)
                                                (stream-filter*
                                                 pred
                                                 (stream-rest* stream)))
    :else                         (stream-filter* pred (stream-rest* stream))))

(defn stream-enumerate-interval*
  "The same as `stream-enumerate-interval`, but uses `stream-cons*`."
  [low high]
  (cond
    (> low high) nil
    :else        (stream-cons* low
                               (stream-enumerate-interval* (inc low) high))))

(defn stream-for-each*
  "The same as `stream-for-each`, but uses `stream-first*` and
  `stream-rest*`."
  [proc stream]
  (if (stream-empty? stream)
    :done
    (do
      (proc (stream-first* stream))
      (recur proc (stream-rest* stream)))))

(defn stream-take*
  "The same as `stream-take`, but uses `stream-cons*`, `stream-first*`
  and `stream-rest*`."
  [n stream]
  (if (or (stream-empty? stream)
          (= 0 n))
    the-empty-stream
    (stream-cons* (stream-first* stream)
                  (stream-take* (dec n) (stream-rest* stream)))))

(defn stream-drop*
  "The same as `stream-take`, but uses `stream-rest*`."
  [n stream]
  (if (or (stream-empty? stream)
          (= 0 n))
    stream
    (recur (dec n) (stream-rest* stream))))

;; printing delayed objects and streams

(defmethod print-method ::delay*
  [object writer]
  (.write writer "#delay* ( ")
  (.write writer (str (force* object)))
  (.write writer " )"))

(defmethod print-method ::stream*
  [object writer]
  (.write writer "#stream* ( ")
  (stream-for-each* (fn [value]
                      (.write writer (str value " "))) object)
  (.write writer ")"))

(comment

  (def d1 (delay* (do
                    (println "forced")
                    (+ 1 2))))
  d1
  (type d1)
  (force* d1)
  (type (force* d1))

  (def s1 (->> (stream-cons* 3 nil)
               (stream-cons* 2)
               (stream-cons* 1)))
  (type s1)
  (meta s1)
  (println s1)

  (stream-nth* s1 1)

  (->> (stream-enumerate-interval* 1 10)
       (stream-map* square)
       (stream-filter* odd?)
       (stream-drop* 1)
       (stream-take* 3)
       (println))

  (println (stream-map** + s1 s1))

  (stream* '())
  (stream* '(1 2 3))
  (stream* [1 2 '(3 4) 5])
  (stream* {:id 1 :name "Joe"})

  (stream-into* [] (stream* '(1 2 3 4 5)))
  (stream-into* '() (stream* '(1 2 3 4 5)))
  (stream-into* #{} (stream* '(1 2 3 4 5)))
  (stream-into* {} (stream* '([:a 1] [:b 2])))

)


;; infinite streams

(defn integers-starting-from
  "Returns an (infinite!) stream of integers starting from `n`."
  [n]
  (stream-cons* n (integers-starting-from (inc n))))

(def integers
  "A stream of integers starting from 1."
  (integers-starting-from 1))

;; we can define infinite streams using other infinite streams

(defn divisible?
  "Returns true if `x` is divisible by `y`."
  [x y]
  (= (rem x y) 0))

(def no-sevens
  "A stream of integers not divisible by 7."
  (stream-filter* (fn [x] (not (divisible? x 7))) integers))

(defn fibgen
  "Returns a new (infinite!) stream of Fibonacci numbers starting at `a`
  and `b`."
  [a b]
  (stream-cons* a (fibgen b (+ a b))))

(def fibs
  "A stream of Fibonacci numbers starting at 0 and 1."
  (fibgen 0 1))

(defn sieve
  "Returns a new stream of prime numbers created from `stream` of
  integers, where the first element is prime."
  [stream]
  (stream-cons* (stream-first* stream)
                (sieve (stream-filter*
                        (fn [x]
                          (not (divisible? x (stream-first* stream))))
                        (stream-rest* stream)))))

(def primes
  "A stream of prime numbers."
  (sieve (integers-starting-from 2)))

(comment

  (stream-nth* integers 12)
  (stream-take* 5 (stream-drop* 100 integers))

  (stream-nth* no-sevens 100)

  (stream-take* 12 fibs)
  (stream-take* 12 primes)
  (stream-take* 5 (stream-drop* 1000 primes))
  ;; can't do 10000 primes; stack overflow (too much filters stacked?)
  (stream-take* 5 (stream-drop* 10000 primes))

  (stream-nth* primes 50)

)

;; infinite sequences using Clojure's lazy seq

(defn lazy-integers-starting-from
  "Returns an (infinite!) lazy sequence of integers starting from `n`."
  [n]
  (cons n (lazy-seq (lazy-integers-starting-from (inc n)))))

(def lazy-integers
  "The lazy sequence of integers starting from 1."
  (lazy-integers-starting-from 1))

(defn lazy-sieve
  "Returns a new lazy sequence of prime numbers created from sequence
  `s` of integers, where the first element is prime."
  [s]
  (lazy-seq (cons (first s)
                  (lazy-sieve (filter (fn [x]
                                    (not (divisible? x (first s))))
                                  (rest s))))))

(def lazy-primes
  "A sequence of prime numbers."
  (lazy-sieve (lazy-integers-starting-from 2)))

(def lazy-primes*
  "A sequence of prime numbers, generated another way."
  (lazy-sieve (iterate inc 2)))

(comment

  (take 5 lazy-integers)
  (take 5 (drop 100000 lazy-integers))

  (take 12 lazy-primes)
  (take 5 (drop 1000 lazy-primes))
  ;; no stack overflow; wonders never cease!
  (take 5 (drop 10000 lazy-primes))
  (take 5 (drop 100000 lazy-primes))
  (nth lazy-primes 50)
  (nth lazy-primes* 50)

)


;; defining streams implicitly (by combining streams, including themselves)

(defn add-streams
  "Returns a stream of sums of corresponding elements of streams `s1`
  and `s2`."
  [s1 s2]
  (stream-map** + s1 s2))

(defn scale-stream
  "Returns a stream with each element of `stream` multiplied by `factor`."
  [stream factor]
  (stream-map** (fn [x] (* x factor)) stream))

(def ones
  (stream-cons* 1 ones))

(def implicit-integers
  "A stream whose first element is 1 and the rest of which is the sum of
  `ones` and `implicit-integers`; thus, the second element of integers is 1
  plus the first element of integers, or 2; the third element of
  integers is 1 plus the second element of integers, or 3; and so on."
  (stream-cons* 1 (add-streams ones implicit-integers)))

(def implicit-fibs
  (stream-cons* 0 (stream-cons* 1 (add-streams (stream-rest* fibs) fibs))))

(def implicit-powers-of-two
  (stream-cons* 1 (scale-stream implicit-powers-of-two 2)))

(def implicit-powers-of-two*
  (stream-cons* 1 (add-streams implicit-powers-of-two* implicit-powers-of-two*)))

(declare implicit-primes)

(defn stream-prime?
  "Returns true if `n` is prime (not divisible by any prime less than or
  equal square root of `n`). Uses the stream `implicit-primes` to
  produce previous primes for the test."
  [n]
  (loop [primes implicit-primes]
    (cond
      (> (square (stream-first* primes)) n) true
      (divisible? n (stream-first* primes)) false
      :else                                 (recur
                                             (stream-rest* primes)))))

(def implicit-primes
  (stream-cons* 2 (stream-filter* stream-prime? (integers-starting-from 3))))

(comment

  (stream-take* 5 ones)

  ;; this works because, at any point, enough of the source stream(s) elements
  ;; has been generated so that we can feed it back into the definition to
  ;; produce the next element
  (stream-take* 5 implicit-integers)
  (stream-take* 12 implicit-fibs)
  (stream-take* 11 implicit-powers-of-two)
  (stream-take* 11 implicit-powers-of-two*)

  (type implicit-primes)
  (stream-first* implicit-primes)
  (stream-take* 12 implicit-primes)
  (stream-take* 5 (stream-drop* 1000 implicit-primes))
  ;; wow, no stack overflow!
  (stream-take* 5 (stream-drop* 10000 implicit-primes))
  (stream-take* 5 (stream-drop* 100000 implicit-primes))

  (stream-take* 5 implicit-powers-of-two*)
)

;; implement a lazy sequence of primes, Clojure way

(declare lazy-primes)

(defn lazy-prime?
  "Returns true if `n` is prime (not divisible by any prime less than or
  equal square root of `n`). Uses the stream `lazy-primes` to produce
  previous primes for the test."
  [n]
  (loop [primes lazy-primes]
    (cond
      (> (square (first primes)) n) true
      (divisible? n (first primes)) false
      :else                         (recur
                                     (rest primes)))))

(def lazy-primes
  (lazy-seq (cons 2 (filter lazy-prime? (drop 3 (range))))))

(comment

  (take 12 lazy-primes)
  ;; no stack overflow this time either
  (take 5 (drop 100000 lazy-primes))

)


;; exploiting the stream paradigm

;; formulating iterations as stream processes

(defn sqrt-improve
  "Returns a next guess at the square root of `x` given previous
  `guess`."
  [guess x]
  (average guess (/ x guess)))

(defn sqrt-stream
  "Returns a stream of the successive guesses of the square root of `x`,
  starting at 1."
  [x]
  ;; no `letrec` in Clojure, but we will find a way; may create multiple streams
  ;; though, see https://stackoverflow.com/q/12589843; also beware of chunking
  ;; https://hacklog.gfredericks.com/2013/07/27/self-referential-seqs-considered-dangerous.html
  (letfn [(guesses [x]
            (stream-cons* 1.0
                          (stream-map** (fn [guess]
                                          (sqrt-improve guess x)) (guesses x))))]
    (guesses x)))

(defn stream-iterate*
  "Returns a stream of `x`, (`f` `x`), (`f` (`f` `x`)) etc. `f` should
  be free of side-effects."
  [f x]
  (letfn [(stream-iteration [first-call f x]
            (if first-call
              (stream-cons* x (stream-iteration false f x))
              (let [value (f x)]
                (stream-cons* value (stream-iteration false f value)))))]
    (stream-iteration true f x)))

(defn sqrt-stream*
  "Returns a stream of successive approximations of the square root of `x`,
  starting at 1."
  [x]
  (stream-iterate* (fn [guess] (sqrt-improve guess x)) 1.0))

(defn stream-reduce*
  "Reduces `stream` to a value applying `f` to pairs of each val and
  result of the previous application, starting with `initial-value`,
  that defaults to the stream's first element."
  ([f stream]
   (stream-reduce* f (stream-first* stream) (stream-rest* stream)))
  ([f initial-value stream]
   (if (stream-empty? stream)
     initial-value
     (recur f (f initial-value (stream-first* stream))
            (stream-rest* stream)))))

(defn partial-sums
  "Returns a stream of sums of all previous elements of the `stream`."
  [stream]
  ;; take the first element of the stream and cons it to a stream created by
  (stream-cons* (stream-first* stream)
                ;; map over stream of partial sums and the rest of the stream
                ;; adding corresponding elements
                (stream-map** + (partial-sums stream) (stream-rest* stream))))

(defn pi-summands
  "Returns a stream of fractions --- elements of series approximating
  the value of pi/4, starting on a fraction with denominator `n`."
  [n]
  (stream-cons* (/ 1 n)
                (stream-map** - (pi-summands (+ n 2)))))

(def pi-stream
  "A stream of successive approximations of pi."
  (scale-stream (partial-sums (pi-summands 1)) 4.0))

(defn euler-transform
  "Returns an accelerated (converging faster) `stream` of approximations
  of partial sums of alternating series (of terms with alternating
  signs)."
  [stream]
  (let [s0 (stream-nth* stream 0)
        s1 (stream-nth* stream 1)
        s2 (stream-nth* stream 2)]
    (stream-cons* (- s2 (/ (square (- s2 s1))
                           (+ s0 (* -2 s1) s2)))
                  (euler-transform (stream-rest* stream)))))

(defn tableau
  "Returns a stream of streams where each successive stream is a
  previous stream transformed with `transform`, starting with
  `stream`."
  [transform stream]
  (stream-cons* stream (tableau transform (transform stream))))

(defn accelerated-sequence
  "Returns a stream of first elements of elements of tableau acquired by
  successively accelerating `stream` of approximations with
  `transform`."
  [transform stream]
  (stream-map** stream-first* (tableau transform stream)))

(comment

  (stream-take* 10 (stream-iterate* dec 0))
  (stream-reduce* + 0 (stream-take* 10 (stream-iterate* inc 0)))
  (stream-reduce* + (stream-take* 10 (stream-iterate* inc 0)))

  (stream-take* 10 (sqrt-stream 2))
  (stream-take* 10 (sqrt-stream* 2))

  (stream-take* 10 integers)
  (stream-take* 10 (partial-sums integers))

  (stream-take* 10 (pi-summands 1))
  ;; converges rather slowly
  (stream-take* 10 pi-stream)
  ;; converges much faster
  (stream-take* 10 (euler-transform pi-stream))
  ;; and even faster
  (stream-take* 8 (accelerated-sequence euler-transform pi-stream))

)


;; infinite streams of pairs

(defn stream-interleave*
  "Returns a stream of alternating elements of streams `s1` and `s2`."
  [s1 s2]
  (if (stream-empty? s1)
    s2
    (stream-cons* (stream-first* s1)
                  (stream-interleave* s2 (stream-rest* s1)))))

(defn pairs
  "Returns a stream of all possible pairs of elements of streams `s1`
  and `s2`."
  [s1 s2]
  (stream-cons* (list (stream-first* s1) (stream-first* s2))
                (stream-interleave*
                 (stream-map** (fn [x] (list (stream-first* s1) x))
                               (stream-rest* s2))
                 (pairs (stream-rest* s1) (stream-rest* s2)))))

(comment

  (def positive-integers (stream-iterate* inc 1))
  (def negative-integers (stream-iterate* dec -1))

  (stream-take* 10 (stream-interleave* positive-integers negative-integers))
  (stream-take* 10 (pairs positive-integers negative-integers))

)


;; streams as signals

(defn stream-repeat*
  "Returns a stream of `x`s."
  [x]
  (stream-cons* x (stream-repeat* x)))

(defn implicit-integral
  "Returns the stream of accumulating sums of `integrand` stream
  multiplied by `dt`, starting at `initial-value` (definite integrals
  of `integrand` by `dt` with constant term `initial-value`). Defined
  implicitly (in terms of other streams, i.e., itself); this
  self-reference can be interpreted as a feedback loop in a
  signal-processing system."
  [integrand initial-value dt]
  ((fn integrals [value]
     (stream-cons* value
                   (add-streams (scale-stream integrand dt)
                                (integrals value)))) initial-value))

(comment

  (stream-take* 10 (stream-repeat* 12))

  ;; f(x) = 10; F(x) = 10x + 3.0
  (stream-take* 10 (implicit-integral (stream-repeat* 10) 3.0 0.1))
  ;; f(x) = x; F(x) = x^2/2 + 3.0
  (stream-take* 10 (implicit-integral integers 3.0 0.1))

)


;; streams and delayed evaluation

;; some explicit delays may be necessary besides the ones implicit in
;; `stream-cons*`, e.g., if two streams are mutually referential

(defn delayed-integral
  ""
  [delayed-integrand initial-value dt]
  ((fn integrals [value]
     (stream-cons* value
                   (let [integrand (force* delayed-integrand)]
                     (add-streams (scale-stream integrand dt)
                                  (integrals value))))) initial-value))

(defn solve
  "Solves the differential equation d`y`/d`t` = `f`(`y`), starting at
  `y0`."
  [f y0 dt]
  ;; something like `declare` for `y`? seems like it is not quite working
  (letfn [(dy [] (stream-map** f (y)))
          (y [] (delayed-integral (delay* (dy)) y0 dt))]
    (y)))

(comment

  ;; f(x) = 10; F(x) = 10x + 3.0
  (stream-take* 10 (delayed-integral (delay* (stream-repeat* 10)) 3.0 0.1))

  ;; it is not totally clear how it works and if it works correctly at all;
  ;; seems like it is not

  ;; approximating e ≈ 2.718 by computing the value at y = 1 of the solution to
  ;; the differential equation dy/dt = y with initial condition y(0) = 1
  (stream-nth* (solve identity 1 0.001) 1000)
  (stream-take* 5 (stream-drop* 20 (solve identity 1 0.001)))

  (stream-take* 5 (solve square -1 0.1))

)


;; modularity of functional programs and modularity of objects

;; sorta like linear congruential generator: x = (ax + c) mod m,
;; https://en.wikipedia.org/wiki/Linear_congruential_generator; maybe it is not
;; a brilliant implementation, but good enough for government work
(defn rand-update
  "Returns next pseudorandom number for given pseudorandom number `x`."
  [x]
  (mod (+ 74 (* x 75))
       (inc (int (Math/pow 2 16)))))

(defn random-number-generator
  "Returns a random number generator."
  [random-seed]
  (let [x (atom random-seed)]
    (fn []
      (swap! x rand-update))))

(defn random-numbers-stream
  "Returns a stream of pseudorandom integers with seed `random-seed`."
  [random-seed]
  ((fn random-numbers [random-number]
     (let [next-random-number (rand-update random-number)]
       (stream-cons* (rand-update random-number)
                     (random-numbers-stream next-random-number)))) random-seed))

(defn random-numbers-stream*
  "Returns a stream of pseudorandom integers with seed
  `random-seed`. Slightly different implementation that is closer to
  the book, but different from the object-oriented version (includes
  `random-seed`)."
  [random-seed]
  ((fn random-numbers [random-number]
     (stream-cons* random-number
                   (stream-map** rand-update (random-numbers random-number))))
   random-seed))

;; evaluating pi using Monte Carlo method; stateless but still modular

(defn map-successive-pairs
  "Maps elements of `stream` by `f` pairwise."
  [f stream]
  (stream-cons* (f (stream-first* stream) (stream-first* (stream-rest* stream)))
                (map-successive-pairs f (stream-rest* (stream-rest* stream)))))

(defn cesaro-stream
  "Returns a stream of booleans produced from `stream` of random numbers
  by testing if they have a common divisor other than 1, pairwise."
  [random-numbers-stream]
  (map-successive-pairs (fn [r1 r2] (= (gcd r1 r2) 1))
                        random-numbers-stream))

(defn monte-carlo-stream
  "Returns a stream of estimates of probabilities that experiments with
  results in `experiment-stream` are passed (results are true), given
  initial counts of `passed` and `failed` experiments."
  [experiment-stream passed failed]
  (let [advance (fn [passed failed]
                  (stream-cons* (/ passed (+ passed failed))
                                (monte-carlo-stream
                                 (stream-rest* experiment-stream)
                                 passed failed)))]
    (if (stream-first* experiment-stream)
      (advance (inc passed) failed)
      (advance passed (inc failed)))))

(comment

  (rand-update 0)

  ;; `rng1` is an object that holds state variable `x` with a current random
  ;; number, that is produced from its previous value by a function
  ;; `rand-update`, starting at `random-seed`
  (def rng1 (random-number-generator 0))
  (def random-numbers-seq (repeatedly rng1))
  (take 10 random-numbers-seq)

  ;; no object, no state, just a stream of values produced by successive calls
  ;; to a pure function `rand-update` starting at `random-seed`
  (stream-take* 10 (random-numbers-stream 0))
  (stream-take* 10 (random-numbers-stream* 0))

  (stream-take* 10 (cesaro-stream (random-numbers-stream 0)))
  (stream-take* 10 (monte-carlo-stream
                    (cesaro-stream (random-numbers-stream 0)) 0 0))

  ;; no objects, no state, still modular (in some way)
  (def mc-pi-stream
    "A stream of approximations of pi using Monte Carlo method."
    (stream-map** (fn [p] (if (zero? p)
                            :unknown
                            (sqrt (/ 6 p))))
                  (monte-carlo-stream
                   (cesaro-stream (random-numbers-stream 0)) 0.0 0.0)))

  (stream-take* 10 (stream-drop* 10000 mc-pi-stream))

)
