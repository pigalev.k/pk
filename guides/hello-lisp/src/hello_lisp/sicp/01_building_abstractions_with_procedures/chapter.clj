(ns hello-lisp.sicp.01-building-abstractions-with-procedures.chapter
  (:require [vlaaad.reveal :as reveal]))


;; the elements of programming


;; primitive expressions

(comment

  1
  "hello"
  true
  :a
  rest

)

;; compound expressions (combinations)

(comment

  (+ 1 2)
  (* (- 12 9) (+ (/ 23 98)))

)


;; naming compound expression values

(def a-ratio (* (- 12 9) (+ (/ 23 98))))


;; compound procedures

(defn square [x]
  (* x x))

(defn cube [x]
  (* x x x))

(comment

  (square 8)
  (cube 8)

)

(defn sum-of-squares [x y]
  (+ (square x) (square y)))

(comment

  (sum-of-squares 8 12)

)


;; conditional expressions and predicates

(defn abs* [x]
  (cond
    (> x 0) x
    (= x 0) 0
    (< x 0) (- x)))

(comment

  (abs* 1)
  (abs* -13)

)


;; example: square roots by Newton's method

;; one way to compute square roots is the Newton's method of successive
;; approximations: whenever we have a guess `y` for the value of the square root
;; of a number `x`, we can get a better guess by averaging `y` with `x`/`y`;
;; continuing the process, we obtain better and better approximations to the
;; square root


(defn good-enough? [guess x]
  (< (abs (- (square guess) x)) 0.001))

(defn average [x y]
  (/ (+ x y) 2))

(defn improve [guess x]
  (average guess (/ x guess)))

(defn sqrt-iter [guess x]
  (if (good-enough? guess x)
    guess
    (sqrt-iter (improve guess x) x)))

(defn sqrt [x]
  (sqrt-iter 1.0 x))

(comment

  (sqrt 4)
  (sqrt 2)

)


;; procedures and the processes they generate

;; linear recursion and iteration

(defn factorial
  "Computes factorial recursively.
  Generates a linear recursive process."
  [n]
  (if (= n 1)
    1
    (* n (factorial (- n 1)))))

(comment

  (factorial 6)

)


(defn factorial*
  "Computes factorial recursively.
  Generates a linear iterative process."
  [n]
  (letfn [(fact-iter [product counter]
            (if (> counter n)
              product
              (recur (* counter product)
                     (+ counter 1))))]
    (fact-iter 1 1)))

(comment

  (factorial 6)

)


;; tree recursion

(defn fib
  "Computes `n`-th Fibonacci number.
  Generates a tree recursive process."
  [n]
  (cond (= n 0) 0
        (= n 1) 1
        :else   (+ (fib (- n 1))
                   (fib (- n 2)))))

(comment

  (fib 8)

)

(defn fib*
  "Computes `n`-th Fibonacci number.
  Generates a linear iterative process."
  [n]
  (letfn [(fib-iter [a b count]
            (if (= count 0)
              a
              (recur b (+ a b) (dec count))))]
    (fib-iter 0 1 n)))

(comment

  (fib 8)

)


;; example: counting change

;; how many different ways can we make change of a given amount of money in
;; dollars, given coins of 50c, 25c, 10c, 5c and 1c?

;; The problem has a simple recursive solution. Suppose the types of coins
;; available are arranged in some order. Then the following relation holds:
;; The number of ways to change amount `a` using `n` kinds of coins equals
;; - the number of ways to change `a` using all but the first kind of coin,
;;   with denomination `d`, plus
;; - the number of ways to change `a` - `d` using all `n` kinds of coins,
;; assuming that
;; - if `a` = 0, it is 1 way to make change
;; - if `a` < 0, it is 0 ways to make change
;; = if `n` = 0, it is 0 ways to make change

(defn count-change [amount]
  (letfn [(first-denomination [kinds-of-coins]
            (cond
              (= kinds-of-coins 1) 1
              (= kinds-of-coins 2) 5
              (= kinds-of-coins 3) 10
              (= kinds-of-coins 4) 25
              (= kinds-of-coins 5) 50))
          (cc [amount kinds-of-coins]
            (cond
              (= amount 0)              1
              (or (< amount 0)
                  (= kinds-of-coins 0)) 0
              :else                     (+ (cc amount
                                               (dec kinds-of-coins))
                                           (cc (- amount
                                                  (first-denomination
                                                   kinds-of-coins))
                                               kinds-of-coins))))]
    (cc amount 5)))

(comment

  (count-change 100)

)

;; it generates a tree-recursive process with a lot of redundant computations,
;; just like tree-recursive `fib`. One approach to cope with it is
;; memoization.


;; exponentiation

(defn expt
  "Computes `b` to the power of `n`.
  Generates a linear recursive process."
  [b n]
  (if (= n 0)
    1
    (* b (expt b (- n 1)))))

(comment

  (expt 2 5)

)

(defn expt*
  "Computes `b` to the power of `n`.
  Generates a linear iterative process."
  [b n]
  (letfn [(expt-iter [b counter product]
            (if (= counter 0)
              product
              (recur b (dec counter) (* b product))))]
    (expt-iter b n 1)))

(comment

  (expt* 2 5)

)

(defn expt**
  "Computes `b` to the power of `n`.
  Generates a linear recursive process. Uses successive squaring as
  optimization."
  [b n]
  (cond
    (= n 0)   1
    (even? n) (square (expt** b (/ n 2)))
    :else     (* b (expt** b (dec n)))))

(comment

  (expt** 2 10)

)


;; greatest common divisors

(defn gcd
  "Computes the greatest common divisor of `a` and `b`.
  Generates a logarithmic iterative process."
  [a b]
  (if (= b 0)
    a
    (recur b (rem a b))))

(comment

  (gcd 28 16)
  (gcd 16 28)

)


;; testing for primality

(defn smallest-divisor
  "Computes the smallest divisor of `n`.
  Uses full enumeration of potential divisors from 2 to (sqrt n)."
  [n]
  (letfn [(find-divisor [n test-divisor]
            (cond
              (> (square test-divisor) n) n
              (divides? test-divisor n)   test-divisor
              :else                       (recur n (+ test-divisor 1))))
          (divides? [a b]
            (= (rem b a) 0))]
    (find-divisor n 2)))

(comment

  (smallest-divisor 5)
  (smallest-divisor 144)
  (smallest-divisor 199)
  (smallest-divisor 1999)
  (smallest-divisor 19999)

)

(defn prime?
  "Returns true if `n` is prime."
  [n]
  (= n (smallest-divisor n)))

(comment

  (prime? 2)
  (prime? 4)
  (prime? 17)

)

(defn expmod
  "Computes the exponential `exp` of `base` modulo `m`."
  [base exp m]
  (cond
    (= exp 0)   1
    (even? exp) (rem (square (expmod base (/ exp 2) m)) m)
    :else       (rem (* base (expmod base (dec exp) m)) m)))

(defn fermat-test
  "Performs Fermat test for primality.
  Based on the Fermat's Little Theorem: if n is a prime number and a
  is any positive integer less than n, then a raised to the nth power
  is congruent to a modulo n (has the same remainder when divided by
  n)."
  [n]
  (letfn [(try-it [a]
            (= (expmod a n n) a))]
    ;; random integer from 1 to n - 1, inclusive
    (try-it (inc (rand-int (dec n))))))

(defn prime?*
  "Returns true if `n` is prime.
  Uses Fermat test for primality with `times` tries."
  [n times]
  (cond (= times 0)     true
        (fermat-test n) (recur n (dec times))
        :else           false))

(comment

  (prime?* 2 1)
  (prime?* 4 1)
  (prime?* 17 1)

  ;; Carmichael numbers (non-primes that fool Fermat test)
  (def carmichael-numbers-table
    (for [number [561 1105 1729 2465 2821 6601]]
      {:number           number
       :prime?           (prime? number)
       :fermat-test?     (prime?* number 100)
       :smallest-divisor (smallest-divisor number)}))

  #reveal/inspect carmichael-numbers-table
  (reveal/inspect carmichael-numbers-table)

)

(defn runtime
  "Returns current time in nanoseconds."
  []
  (System/nanoTime))

(comment

  (runtime)

)

(defn timed-prime-test
  "Returns true if `n` is prime.
  Prints the elapsed time in milliseconds."
  [n]
  (letfn [(start-prime-test [n start-time]
            (let [is-prime (prime? n)]
              (when is-prime
                (report-prime (- (runtime) start-time)))
              is-prime))
          (report-prime [elapsed-time]
            (println n ": " (/ elapsed-time 1000000.0)))]
    (start-prime-test n (runtime))))

(comment

  (time (prime? 99991))
  (timed-prime-test 99991)
  (doseq [n (range 100000)]
    (timed-prime-test n))
  (doseq [n (range 100000 1000000)]
    (timed-prime-test n))
  (doseq [n (range 1000000 2000000)]
    (timed-prime-test n))

)


;; formulating abstractions with higher-order procedures

;; procedures as arguments

(defn sum
  "Returns a sum of a sequence of numbers produced by applying `term` to
  elements of a sequence of integers produced by applying `next*` to
  the previous integer, starting at `a` and ending at `b`. Generates a
  linear recursive process."
  [term a next* b]
  (if (> a b)
    0
    (+ (term a)
       (sum term (next* a) next* b))))

(defn sum-integers
  "Computes sum of integers from `a` to `b`."
  [a b]
  (sum identity a inc b))

(defn sum-cubes
  "Computes sum of cubes of integers from `a` to `b`."
  [a b]
  (sum cube a inc b))

(defn sum-pi
  "Computes sum of terms of series that converges to pi/8 with indices
  from `a` to `b` with step 4."
  [a b]
  (let [pi-term (fn [x]
                  (/ 1.0 (* x (+ 2 x))))
        pi-next (fn [x]
                  (+ 4 x))]
    (sum pi-term a pi-next b)))

(defn integral
  "Computes the definite integral of function `f` from `a` to `b` with
  step `dx`."
  [f a b dx]
  (let [add-dx (fn [x] (+ x dx))]
    (* (sum f (+ a (/ dx 2.0)) add-dx b) dx)))

(comment

  (sum-integers 1 10)
  (sum-cubes 1 10)
  (* 8 (sum-pi 1 10000))

  ;; exact answer is 0.25
  (integral cube 0 1 0.01)
  (integral cube 0 1 0.001)
  ;; stack overflow with too small dx --- `sum` generates linear recursion, see
  ;; exercises for a solution

)


;; procedures as general methods

;; finding roots of equations by the half-interval method

(defn search
  "Searches for the roots of equation f(x) = 0 in the interval from `a`
  to `b`, where f(a) is negative, and f(b) is positive."
  [f neg-point pos-point]
  (let [midpoint (average neg-point pos-point)
        close-enough? (fn [x y] (< (abs (- x y)) 0.001))]
    (if (close-enough? neg-point pos-point)
      midpoint
      (let [test-value (f midpoint)]
        (cond
          (pos? test-value) (recur f neg-point midpoint)
          (neg? test-value) (recur f midpoint pos-point)
          :else midpoint)))))

(defn half-interval-method
  "Computes the roots of equation f(x) = 0 in the interval from `a` to
  `b`. Signals an error and returns nil if the method cannot be used
  with this function on this interval."
  [f a b]
  (let [a-value (f a)
        b-value (f b)]
    (cond
      (zero? a-value)                     a
      (zero? b-value)                     b
      (and (neg? a-value) (pos? b-value)) (search f a b)
      (and (neg? b-value) (pos? a-value)) (search f b a)
      :else
      (binding [*out* *err*]
        (printf "Bounds do not have the same sign: %f, %f\n",
                a-value, b-value)))))

(comment

  (half-interval-method #(Math/sin %) 2.0 4.0)
  (half-interval-method #(Math/sin %) 0.0 4.0)
  (half-interval-method #(Math/sin %) 1.0 3.0)

)


;; finding fixed points of functions

(def tolerance 0.00001)

(defn fixed-point
  "Computes fixed point of a function `f`, starting at `first-guess`."
  [f first-guess]
  (letfn [(close-enough? [v1 v2] (< (abs (- v1 v2)) tolerance))
          (try-it [guess]
            (let [next* (f guess)]
              (if (close-enough? guess next*)
                next*
                (recur next*))))]
    (try-it first-guess)))

(defn sqrt*
  "Tries to compute square root of `x`. Does not converge."
  [x]
  (fixed-point #(/ x %) 1.0))

(defn sqrt**
  "Computes square root of `x`. Uses average damping."
  [x]
  (fixed-point #(average % (/ x %)) 1.0))

(defn golden-ratio
  "Computes the golden ratio as a fixed point of x = 1 + 1/x."
  []
  (fixed-point #(+ 1 (/ 1 %)) 1.0))

(comment

  (fixed-point #(Math/cos %) 1.0)
  ;; solution of y = sin y + cos y
  (fixed-point #(+ (Math/sin %) (Math/cos %)) 1.0)

  ;; y = x/y; does not converge --- guesses varied too much
  (sqrt* 2)
  ;; y = 1/2 * (y + x/y) --- transformed y = x/y; converges
  (sqrt** 2)

  (golden-ratio)

)


;; procedures as returned values

(defn average-damp
  "Returns a function of one argument that returns an average between
  `x` and `f`(`x`.)"
  [f]
  (fn [x] (average x (f x))))

(defn sqrt***
  "Computes square root of `x`. Uses fixed-point search with average
  damping."
  [x]
  (fixed-point (average-damp (fn [y] (/ x y))) 1.0))

(defn crt*
  "Computes square root of `x`. Uses fixed-point search with average
  damping."
  [x]
  (fixed-point (average-damp (fn [y] (/ x (square y)))) 1.0))

(comment

  ((average-damp square) 10)
  (sqrt*** 2)
  (crt* 8)

)


;; Newton's method

(def dx 0.00001)

(defn deriv
  "Returns a function that computes value of derivative of `g` at `x`."
  [g]
  (fn [x] (/ (- (g (+ x dx)) (g x)) dx)))

(defn newton-transform
  "Returns a function whose fixed point is a solution of equation
  `g`(`x`) = 0."
  [g]
  (fn [x] (- x (/ (g x) ((deriv g) x)))))

(defn newtons-method
  "Returns a solution of equation `g`(x) = 0. Uses Newton's method."
  [g guess]
  (fixed-point (newton-transform g) guess))

(defn sqrt****
  "Computes square root of `x`. Uses Newton's method."
  [x]
  (newtons-method (fn [y] (- (square y) x)) 1.0))

(defn fixed-point-of-transform
  "Returns a fixed point of `transform`ed function `g`, starting at
  `guess`."
  [g transform guess]
  (fixed-point (transform g) guess))

(defn sqrt*****
  "Computes square root of `x`. Uses a general method for finding fixed
  points of functions with `average-damp` transform."
  [x]
  (fixed-point-of-transform
   #(/ x %)
   average-damp 1.0))

(defn sqrt******
  "Computes square root of `x`. Uses a general method for finding fixed
  points of functions with `newton-transform` transform."
  [x]
  (fixed-point-of-transform
   #(- (square %) x)
   newton-transform 1.0))

(comment

  ((deriv cube) 5)
  (sqrt**** 2)
  (sqrt***** 2)
  (sqrt****** 2)

)
