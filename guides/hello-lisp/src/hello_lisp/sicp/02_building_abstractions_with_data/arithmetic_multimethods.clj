(ns hello-lisp.sicp.02-building-abstractions-with-data.arithmetic-multimethods)

;; system of generic arithmetic operations implemented using Clojure
;; multimethods

;; the system supports basic arithmetic operations (add, sub, mul, div) with
;; integer, real, rational, and complex operands; other data types can be added
;; without modifying the existing code (e.g., polynomials)

;; integer and real numbers are represented as bare Clojure numbers, rational
;; numbers as Clojure ratios, other

(defmulti add (fn []))
