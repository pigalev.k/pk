(ns hello-lisp.sicp.02-building-abstractions-with-data.chapter
  (:require
   [quil.core :as q]
   [hello-lisp.sicp.helpers :refer [error]]
   [hello-lisp.sicp.01-building-abstractions-with-procedures.chapter
    :refer [gcd square fib* prime? expt* sqrt******]]
   [vlaaad.reveal :as reveal]))


;; introduction to data abstraction

;; example: arithmetic operations for rational numbers

;; all operations are defined in terms of these
(declare numer denom make-rat)

(defn add-rat
  "Returns a sum of rational numbers `x` and `y`."
  [x y]
  (make-rat (+ (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(defn sub-rat
  "Returns a result of subtraction of rational numbers `x` and `y`."
  [x y]
  (make-rat (- (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(defn mul-rat
  "Returns a product of rational numbers `x` and `y`."
  [x y]
  (make-rat (* (numer x) (numer y))
            (* (denom x) (denom y))))

(defn div-rat
  "Returns a result of division of rational numbers `x` and `y`."
  [x y]
  (make-rat (* (numer x) (denom y))
            (* (denom x) (numer y))))

(defn equal-rat?
  "Returns true if rational numbers `x` and `y` are equal."
  [x y]
  (= (* (numer x) (denom y))
     (* (numer y) (denom x))))

(defn print-rat
  "Prints the rational number `x` to stdin."
  [x]
  (printf "%d/%d\n" (numer x) (denom x)))


;; representing rational numbers: pairs

(defn make-rat
  "Returns a new rational number with numerator `n` and denominator `d`."
  [n d]
  (let [g (gcd n d)]
    (cons (/ n g) (cons (/ d g) nil))))

(defn numer
  "Returns a numerator of the rational number `x`."
  [x]
  (first x))

(defn denom
  "Returns a denominator of the rational number `x`."
  [x]
  (second x))


(comment

  (def one-half (make-rat 1 2))
  (print-rat one-half)

  (def one-third (make-rat 1 3))
  (print-rat (add-rat one-half one-third))
  (print-rat (mul-rat one-half one-third))
  (print-rat (add-rat one-third one-third))

)


;; what is meant by data?

;; an alternative implementation of pair using only procedures

(defn pair*
  "Returns a new pair of `x` and `y`."
  [x y]
  (fn dispatch [m]
    (cond (= m 0) x
          (= m 1) y,
          :else (binding [*out* *err*]
                  (printf "pair*: expected 0 or 1, got %s\n" m)))))

(defn first*
  "Returns the first element of the pair `p`."
  [p] (p 0))

(defn second*
  "Returns the second element of the pair `p`."
  [p] (p 1))

;; testing a pair interface condition

(def frst 3)
(def scnd "hello")
(def p (pair* frst scnd))

(= frst (first* p))
(= scnd (second* p))


;; extended exercise: interval arithmetic

;; an interval has two endpoints: a lower bound and upper bound.

(declare make-interval lower-bound upper-bound)

(defn add-interval
  "Returns the sum of intervals `x` and `y`."
  [x y]
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))

(defn sub-interval
  "Returns the result of subtraction of interval `y` from interval `x`."
  [x y]
  (make-interval (- (lower-bound x) (lower-bound y))
                 (- (upper-bound x) (upper-bound y))))

(defn mul-interval
  "Returns the product of intervals `x` and `y`."
  [x y]
  (let [p1 (* (lower-bound x) (lower-bound y))
        p2 (* (lower-bound x) (upper-bound y))
        p3 (* (upper-bound x) (lower-bound y))
        p4 (* (upper-bound x) (upper-bound y))]
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(defn div-interval
  "Returns the result of division of interval `x` by interval `y`."
  [x y]
  (mul-interval x (make-interval (/ 1.0 (upper-bound y))
                                 (/ 1.0 (lower-bound y)))))

;; basic representation: as bounds

(defn make-interval
  "Returns a new interval with lower bound `l` and upper bound `u`."
  [l u]
  (cons l (cons u nil)))

(defn lower-bound
  "Returns the lower bound of the interval `i`."
  [i]
  (first i))

(defn upper-bound
  "Returns the upper bound of the interval `i`."
  [i]
  (second i))

(comment

  (def i0 (make-interval 9 11))
  (lower-bound i0)
  (upper-bound i0)

)

;; alternative representation: as center and width (absolute tolerance)
;; in terms of basic representation

(defn make-center-width
  "Returns a new interval with center `c` and width `w`."
  [c w]
  (make-interval (- c w) (+ c w)))

(defn center
  "Returns center of the interval `i`."
  [i]
  (/ (+ (lower-bound i) (upper-bound i)) 2))

(defn width
  "Returns width of the interval `i`."
  [i]
  (/ (- (upper-bound i) (lower-bound i)) 2))

(comment

  (def i1 (make-center-width 10 1))
  (center i1)
  (width i1)

)


;; alternative representation: as center and percentage tolerance

(defn make-center-percent
  "Returns a new interval with center `c` and percentage tolerance `p`."
  [c p]
  (let [w (* c (/ p 100))]
    (make-center-width c w)))

(defn percent
  "Returns the percentage tolerance of the interval `i`."
  [i]
  (* 100 (/ (width i) (center i))))

(comment

  (def i2 (make-center-percent 10 10))
  (center i2)
  (width i2)
  (percent i2)

)

;; different results of equivalent interval formulae

(defn par1 [r1 r2]
  (div-interval (mul-interval r1 r2)
                (add-interval r1 r2)))

(defn par2 [r1 r2]
  (let [one (make-interval 1 1)]
    (div-interval one (add-interval (div-interval one r1)
                                    (div-interval one r2)))))

(comment

  (def r1 (make-center-percent 10 10))
  (def r2 (make-center-percent 25 5))

  (par1 r1 r2)
  (par2 r1 r2)

  ;; must be 1?
  (div-interval r1 r1)
  (div-interval r2 r2)

)


;; hierarchical data and the closure property

;; representing sequences

;; list operations

(def l1 (cons 1 (cons 2 (cons 3 nil))))
(def l2 (list 1 2 3))

(defn list-ref
  "Returns `n`-th element of the list `items`."
  [items n]
  (if (= n 0)
    (first items)
    (recur (rest items) (dec n))))

(defn length
  "Returns the length of the list `items`."
  [items]
  (if (empty? items)
    0
    (inc (length (rest items)))))

(defn length*
  "Returns the length of the list `items`."
  [items]
  (loop [l items result 0]
    (if (empty? l)
      result
      (recur (rest l) (inc result)))))

(defn append
  "Returns a new list with elements of `list2` appended to `list1`."
  [list1 list2]
  (if (empty? list1)
    list2
    (cons (first list1) (append (rest list1) list2))))

(defn reverse*
  "Returns the list `l` reversed."
  [l]
  (loop [l l result '()]
    (if (empty? l)
      result
      (recur (rest l) (cons (first l) result)))))

(defn append*
  "Returns a new list with elements of `list2` appended to `list1`."
  [list1 list2]
  (loop [l1     (reverse* list1)
         result list2]
    (if (empty? l1)
      result
      (recur (rest l1) (cons (first l1) result)))))

(comment

  (list-ref l2 1)
  (list-ref l2 10)

  ;; recursive process, consumes the stack linearly with the size of input
  (length (range 10))
  (length (range 10000))
  ;; iterative process, no stack consumption
  (length* (range 1000000))

  ;; recursive process, consumes the stack linearly with the size of
  ;; input (the first list)
  (append l2 l2)
  (append (range 10000) (range 1000 1005))
  ;; iterative processes, no stack consumption
  (reverse* l2)
  (append* l2 l2)
  (append* (range 10000) (range 1000 1005))

)


;; mapping over lists

(defn map*
  "Returns the sequence of results of applying function `f` to elements
  of the sequence `xs`."
  [f xs]
  (if (empty? xs)
    nil
    (cons (f (first xs))
          (map* f (rest xs)))))

(defn map**
  "Returns the sequence of results of applying function `f` to elements
  of the sequence `xs`."
  [f xs]
  (loop [xs xs result '()]
    (if (empty? xs)
      (reverse* result)
      (recur (rest xs) (cons (f (first xs)) result)))))

(defn map***
  "Returns the sequence of results of applying function `f` to elements
  of the sequence `xs`."
  [f xs]
  (loop [xs xs result []]
    (if (empty? xs)
      (seq result)
      (recur (rest xs) (conj result (f (first xs)))))))

(defn map****
  "Returns the sequence of results of applying function `f` to elements
  of the sequence `xs`."
  [f xs]
  (if (empty? xs)
    nil
    (lazy-seq (cons (f (first xs))
                    (map**** f (rest xs))))))


(comment

    ;; recursive process, consumes the stack linearly with the size of input
    (map* abs (list -10 2.5 -11.6 17))
    (map* inc (range 100000))
    ;; iterative processes, no stack consumption
    (map** abs (list -10 2.5 -11.6 17))
    (time (map** inc (range 100000)))
    (map*** abs (list -10 2.5 -11.6 17))
    (time (map*** inc (range 100000)))
    ;; lazy implementation; seems like iterative process under the hood, no
    ;; stack consumption
    (map**** abs (list -10 2.5 -11.6 17))
    (time (map**** inc (range 100000)))
    ;; clojure.core implementation
    (time (map inc (range 100000)))

 )


;; hierarchical structures

(defn count-leaves
  "Returns count of leaves of the tree `t`. Structure is similar to
  `length` for sequences."
  [t]
  (cond (not (seq? t)) 1
        (empty? t)     0
        :else          (+ (count-leaves (first t))
                          (count-leaves (rest t)))))

(comment

  (def t1 '(1 2 (3 4) ((5))))

  (count-leaves nil)
  (count-leaves :a)
  (count-leaves '())
  (count-leaves t1)

)


;; mapping over trees

(defn map-tree
  "Returns result of applying `f` to all leaves of the `tree`. Uses just
  recursion."
  [f tree]
  (cond (not (seq? tree)) (f tree)
        (empty? tree)     nil
        :else             (cons (map-tree f (first tree))
                                (map-tree f (rest tree)))))

(defn map-tree*
  "Returns result of applying `f` to all leaves of the `tree`. Uses
  sequence operation (map) + recursion (recursively maps subtrees)."
  [f tree]
  (map (fn [subtree]
         (if (seq? subtree)
           (map-tree* f subtree)
           (f subtree)))
       tree))

(comment

  (def t1 '(1 (2 (3 4) 5) 6 7))

  (map-tree inc t1)
  (map-tree* inc t1)
  (map-tree* str t1)
  (map-tree* double t1)

)


;; sequences as conventional interfaces

;; sequence operations

(defn filter*
  "Returns elements of sequence `xs` that satisfy `predicate`."
  [predicate xs]
  (cond
    (empty? xs) xs
    (predicate (first xs)) (cons (first xs)
                                 (filter* predicate (rest xs)))
    :else (filter predicate (rest xs))))

(defn filterv*
  "Returns a vector of elements of sequence `xs` that satisfy
  `predicate`. Eager. Generates an iterative process."
  [predicate xs]
  (loop [xs xs result []]
    (cond
      (empty? xs)            result
      (predicate (first xs)) (recur (rest xs) (conj result (first xs)))
      :else                  (recur (rest xs) result))))

(defn reduce*
  "Returns result of successively applying `op` to pairs of element of
  `xs` and intermediate result, starting with `initial` or the first
  element of `xs`, if not supplied."
  ([op xs]
   (reduce* op (first xs) (rest xs)))
  ([op initial xs]
   (if (empty? xs)
     initial
     (op (first xs)
         (reduce* op initial (rest xs))))))

(defn enumerate-interval
  "Returns a sequence of all integers between `low` to
  `high` (inclusive)."
  [low high]
  (cond
    (> low high) '()
    :else        (cons low (enumerate-interval (inc low) high))))

(defn enumerate-interval*
  "Returns a sequence of all integers between `low` to
  `high` (inclusive). Generates an iterative process."
  [low high]
  (seq
   (loop [low low result []]
     (cond
       (> low high) result
       :else (recur (inc low) (conj result low))))))

(defn enumerate-tree
  "Returns a sequence of all leaves of the `tree`."
  [tree]
  (cond (not (seq? tree)) (list tree)
        (empty? tree)     '()
        :else             (append* (enumerate-tree (first tree))
                                   (enumerate-tree (rest tree)))))

(comment

  (filter* odd? '(1 2 3 4 5))
  (filterv* odd? '(1 2 3 4 5))

  (reduce* + 0 '(1 2 3 4 5))
  (reduce* * 1 '(1 2 3 4 5))
  (reduce* cons nil '(1 2 3 4 5))

  (enumerate-interval 2 7)
  (enumerate-interval* 2 7)

  (enumerate-tree '(1 (2 (3 4) 5)))

)

(defn sum-odd-squares
  "Returns a sum of squares of odd elements of `tree`."
  [tree]
  (->> (enumerate-tree tree)
       (filter* odd?)
       (map* square)
       (reduce* + 0)))

(defn even-fibs
  "Returns all even Fibonacci numbers from 0 to `n`."
  [n]
  (->> (enumerate-interval 0 n)
       (map* fib*)
       (filter* even?)
       (reduce* cons nil)))

(defn max-num
  "Returns the maximal number from `xs`."
  [xs]
  (reduce* max xs))

(comment

  (sum-odd-squares '(1 (2 (3 4) 5)))
  (even-fibs 10)
  (max-num '(-10 2.5 -11.6 17))

)


;; nested mappings

(defn flatmap
  "Maps `proc` over `xs`, flattening nested sequences in the result."
  [proc xs]
  (reduce* append* nil (map proc xs)))

(defn prime-sum?
  "Returns true if the sum of the first and second elements of the
  `pair` is prime."
  [pair]
  (prime? (+ (first pair) (second pair))))

(defn make-pair-sum
  "Returns a triple of elements of a `pair` whose sum is prime, and this
  prime."
  [pair]
  (let [fst  (first pair)
        scnd (second pair)]
    (list fst scnd (+ fst scnd))))

(defn prime-sum-pairs
  "Returns a sequence of all ordered pairs (along with sums of their
  elements) of distinct positive integers i and j, where 1 <= j < i <=
  `n`, such that i + j is prime."
  [n]
  (map* make-pair-sum
        (filter* prime-sum?
                 (flatmap (fn [i]
                            (map* (fn [j] (list i j))
                                  (enumerate-interval 1 (dec i))))
                          (enumerate-interval 1 n)))))

(defn prime-sum-pairs*
  "Returns a sequence of all ordered pairs (along with sums of their
  elements) of distinct positive integers i and j, where 1 <= j < i <=
  `n`, such that i + j is prime. Reorganized for readability (at least
  that was the intention)."
  [n]
  (->> (enumerate-interval 1 n)
       (flatmap (fn [i]
                  (->> (enumerate-interval 1 (dec i))
                       (map* (fn [j] (list i j))))))
       (filter* prime-sum?)
       (map* make-pair-sum)))

(comment

  (map* #(list % (* % %)) '(1 2 3 4 5))
  (flatmap #(list % (* % %)) '(1 2 3 4 5))

  (prime-sum-pairs 6)
  (prime-sum-pairs* 6)

)

(defn remove*
  "Returns sequence `xs` without `x`."
  [x xs]
  (filter* (fn [y] (not= y x)) xs))

(defn permutations
  "Returns sequence of permutations of set `s`."
  [s]
  (if (empty? s)
    '(())
    (flatmap (fn [x]
               (map* (fn [p] (cons x p))
                     (permutations (remove* x s))))
             s)))

(comment

  (remove* 1 '(1 2 3 1 3 2 1))
  (permutations '(1 2 3))

)


;; example: a picture language

(defn draw
  "Draws on the sketch."
  []
  (q/background 255))

(comment

  (q/sketch
   :title "A Picture Language"
   :draw draw
   :size [512 512])

)


;; symbolic data

;; quotation

(defn memq
  "Returns the subsequence of `xs` starting at `item`, or false if not
  found."
  [item xs]
  (cond (empty? xs) false
        (= item (first xs)) xs
        :else (recur item (rest xs))))

(comment

  (memq 'apple '(pear banana prune))
  (memq 'apple '(x (apple sauce) y apple pear))

)


;; example: symbolic differentiation

;; the differentiation program with abstract data

;; addition and multiplication of two arguments;
;; dc/dx = 0; c is a constant or a variable different from x
;; dx/dx = 1
;; d(u + v)/dx = du/dx + dv/dx
;; d(uv)/dx = udv/dx + vdu/dx
;; d(u^n)/dx = nu^(n-1) * du/dx

;; assuming we already have implementations of abstract data
;; interfaces (selectors, constructors, predicates) for concepts of constant,
;; variable, sum, product

(declare variable? same-variable?
         sum? addend augend make-sum
         product? multiplier multiplicand make-product
         exponentiation? base exponent make-exponentiation)

;; we can express the differentiation rules as procedure

(defn deriv
  "Computes a derivative of the symbolic expression `exp` by variable
  `var`."
  [exp var]
  (cond (number? exp)         0
        (variable? exp)       (if (same-variable? exp var) 1 0)
        (sum? exp)            (make-sum (deriv (addend exp) var)
                                        (deriv (augend exp) var))
        (product? exp)        (make-sum
                               (make-product (multiplier exp)
                                             (deriv (multiplicand exp) var))
                               (make-product (deriv (multiplier exp) var)
                                             (multiplicand exp)))
        (exponentiation? exp) (make-product
                               (exponent exp)
                               (make-product
                                (make-exponentiation (base exp)
                                                     (make-sum (exponent exp) -1))
                                (deriv (base exp) var)))
        :else                 (binding [*err* *out*]
                                (printf "deriv: unknown expression type: '%s'\n"
                                        exp))))

;; representing algebraic expressions

;; chosen representation: Lisp combinations

(defn variable?
  "Returns true if `x` is a variable."
  [x]
  (symbol? x))

(defn same-variable?
  "Returns true if variables `v1` and `v2` are the same."
  [v1 v2]
  (and (variable? v1) (variable? v2) (= v1 v2)))

(defn make-sum
  "Returns a new sum expression for addend `a1` and augend `a2`.
  Simplifies some sums."
  [a1 a2]
  (cond
    (= a1 0)                        a2
    (= a2 0)                        a1
    (and (number? a1) (number? a2)) (+ a1 a2)
    :else                           (list '+ a1 a2)))

(defn make-product
  "Returns a new product expression for multiplier `m1` and multiplicand `m2`.
  Simplifies some products."
  [m1 m2]
  (cond
    (or (= m1 0) (= m2 0))          0
    (= m1 1)                        m2
    (= m2 1)                        m1
    (and (number? m1) (number? m2)) (* m1 m2)
    :else                           (list '* m1 m2)))

(defn make-exponentiation
  "Returns a new exponentiation expression for base `b` and exponent
  `e`. Simplifies some exponentiations."
  [b e]
  (cond
    (= e 0) 1
    (= e 1) b
    (and (number? e) (number? b)) (expt* b e)
    :else (list '** b e)))

(defn sum?
  "Returns true if `exp` is a sum."
  [exp]
  (and (list? exp) (= (first exp) '+)))

(defn addend
  "Returns the addend (left operand) of the `sum`."
  [sum]
  (second sum))

(defn augend
  "Returns the augend (right operand) of the `sum`."
  [sum]
  (nth sum 2))

(defn product?
  "Returns true if `exp` is a product."
  [exp]
  (and (list? exp) (= (first exp) '*)))

(defn multiplier
  "Returns the multiplier (left operand) of the `product`."
  [product]
  (second product))

(defn multiplicand
  "Returns the multiplicand (right operand) of the `product`."
  [product]
  (nth product 2))

(defn exponentiation?
  "Returns true if `exp` is exponentiation."
  [exp]
  (and (list? exp) (= (first exp) '**)))

(defn base
  "Returns the base (left operand) of `exponentiation`."
  [exponentiation]
  (second exponentiation))

(defn exponent
  "Returns the exponent (right operand) of `exponentiation`."
  [exponentiation]
  (nth exponentiation 2))

(defn to-infix
  "Converts `exp` to infix notation."
  [exp]
  (cond (or (number? exp)
            (variable? exp))  exp
        (sum? exp)            (list (to-infix (addend exp))
                                    (first exp)
                                    (to-infix (augend exp)))
        (product? exp)        (list (to-infix (multiplier exp))
                                    (first exp)
                                    (to-infix (multiplicand exp)))
        (exponentiation? exp) (list (to-infix (base exp))
                                    (first exp)
                                    (to-infix (exponent exp)))

        :else (printf "unknown expression: %s\n" exp)))

(comment

  (to-infix (deriv '(+ x 3) 'x))
  (to-infix (deriv '(* x y) 'x))
  (to-infix (deriv '(* (* x y) (+ x 3)) 'x))
  (to-infix (deriv '(** x 3) 'x))
  (to-infix (deriv '(** x -1) 'x))

  (deriv '(- x 3) 'x)

)


;; representing sets

;; set operations: must satisfy following conditions

;; - for any set S and object x, (element-of-set? x (adjoin-set x S)) is true
;;   --- adjoining an object to set produces the set that contains the object

;; - for any sets S and T and any object x, (element-of-set? x (intersection-set
;;   S T)) is equal to (and (element-of-set? x S) (element-of-set? x T)) --- the
;;   elements of (intersection-set S T) are the elements that are in S and in T

;; - for any sets S and T and any object x, (element-of-set? x (union-set S T))
;;   is equal to (or (element-of-set? x S) (element-of-set? x T)) --- the
;;   elements of (union-set S T) are the elements that are in S or in T

;; - for any object x, (element-of-set? x '()) is false --- no object is an
;;   element of the empty set

(declare union-set intersection-set element-of-set? adjoin-set)


;; sets as unordered lists: O(n) checking membership and adjoining, O(n^2)
;; intersection and union


;; sets as ordered lists: O(n) checking membership and adjoining, O(n)
;; intersection and union


;; sets as binary trees: O(log n) checking membership and adjoining, O(n)
;; intersection and union (if balanced); requires elements to be comparable

;; binary tree is a lower-level abstract data structure

(defn entry
  "Returns the entry (value of the root node) of the `tree`."
  [tree]
  (first tree))

(defn left-branch
  "Returns the left branch (root node of a subtree) of the `tree`."
  [tree]
  (second tree))

(defn right-branch
  "Returns the right branch (root node of a subtree) of the `tree`."
  [tree]
  (nth tree 2))

(defn make-tree
  "Returns a new binary tree with `entry`, `left` and `right` branches,
  or empty tree (empty list) if those are not provided."
  ([] '())
  ([entry left right]
   (list entry left right)))

(defn tree-contains?
  "Returns true if `tree` contains `x`."
  [tree x]
  (cond
    (empty? tree)      false
    (= x (entry tree)) true
    (< x (entry tree)) (recur (left-branch tree) x)
    (> x (entry tree)) (recur (right-branch tree) x)))

(defn tree-add-element
  "Returns the `tree` with `x` added."
  [tree x]
  (cond
    (empty? tree)      (make-tree x nil nil)
    (= x (entry tree)) tree
    (< x (entry tree)) (make-tree (entry tree)
                                  (tree-add-element (left-branch tree) x)
                                  (right-branch tree))
    (> x (entry tree)) (make-tree (entry tree)
                                  (left-branch tree)
                                  (tree-add-element (right-branch tree) x))))

(defn list->tree
  "Returns a new binary tree with elements from `xs` added to it."
  [xs]
  (reduce* #(tree-add-element %2 %1) (make-tree) xs))

(comment

  (def t (make-tree))
  (tree-contains? t 1)

  (def t1 (-> t
              (tree-add-element 1)
              (tree-add-element -1)
              (tree-add-element 12)
              (tree-add-element 14)))

  (tree-contains? t1 1)
  (tree-contains? t1 -18)

  (def t2 (list->tree '(1 10 -8 19 22 83 -12 0 46)))

  (tree-contains? t2 1)
  (tree-contains? t2 19)
  (tree-contains? t2 -18)

)


;; set operations are defined in terms of binary tree operations

(defn make-set
  "Returns a new set.
  Adds elements `xs` to it, if any."
  ([] '())
  ([& xs]
   (list->tree xs)))

(defn element-of-set?
  "Returns true if `x` is an element of set `s`."
  [s x]
  (tree-contains? s x))

(defn adjoin-set
  "Returns a set `s` with element `x` adjoined to it."
  [s x]
  (tree-add-element s x))

(comment

  (def s (make-set))
  (element-of-set? s 1)

  (adjoin-set s 1)
  (def s2
    (-> s
        (adjoin-set 1)
        (adjoin-set -1)
        (adjoin-set 12)
        (adjoin-set 14)))

)

;; example: Huffman encoding trees (for encoding and decoding messages)

;; there are two types of nodes: leaf and (general) tree

(declare make-leaf leaf? symbol-leaf weight-leaf)

(defn make-leaf
  "Returns a new leaf node of Huffman tree for `symbol` with
  `weight`."
  [sym weight]
  (list 'leaf sym weight))

(defn leaf?
  "Returns true if `object` is a leaf node of Huffman tree."
  [object]
  (= (first object) 'leaf))

(defn symbol-leaf
  "Returns the symbol of the leaf node of Huffman tree `t`."
  [t]
  (second t))

(defn weight-leaf
  "Returns the weight of the leaf node of Huffman tree `t`."
  [t]
  (nth t 2))


(declare make-code-tree tree-left-branch tree-right-branch
         tree-symbols tree-weight)

(defn make-code-tree
  "Returns a new encoding/decoding Huffman tree with `left` and `right`
  branches."
  [left right]
  (list left
        right
        (append* (tree-symbols left) (tree-symbols right))
        (+ (tree-weight left) (tree-weight right))))

;; work on any tree node, leaf or not

(defn tree-left-branch
  "Returns the left branch of the `tree`."
  [tree]
  (first tree))

(defn tree-right-branch
  "Returns the right branch of the `tree`."
  [tree]
  (second tree))

;; also work on any tree node, but differently --- generic procedures

(defn tree-symbols
  "Returns list of symbols of the `tree`."
  [tree]
  (if (leaf? tree)
    (list (symbol-leaf tree))
    (nth tree 2)))

(defn tree-weight
  "Returns weight of the `tree`."
  [tree]
  (if (leaf? tree)
    (weight-leaf tree)
    (nth tree 3)))

;; the decoding procedure

(defn decode
  "Returns a sequence of symbols decoded from `bits` with Huffman
  `tree`."
  [bits tree]
  (letfn [(choose-branch [bit branch]
            (cond
              (= bit 0) (tree-left-branch branch)
              (= bit 1) (tree-right-branch branch)
              :else     (binding [*err* *out*]
                          (printf "choose-branch: bad bit '%d'\n"))))
          (decode-1 [bits current-branch]
            (if (empty? bits)
              '()
              (let [next-branch (choose-branch (first bits) current-branch)]
                (if (leaf? next-branch)
                  (cons (symbol-leaf next-branch)
                        (decode-1 (rest bits) tree))
                  (decode-1 (rest bits) next-branch)))))]
    (decode-1 bits tree)))

;; generating an encoding/decoding tree

(defn adjoin-weighted-set
  "Returns a weight-sorted set `s` with element `x` added. Does not
  check if element is already there."
  [s x]
  (cond
    (empty? s) (list x)
    (< (tree-weight x) (tree-weight (first s))) (cons x s)
    :else (cons (first s) (adjoin-weighted-set (rest s) x))))

(defn make-leaf-set
  "Returns a new set constructed from the list of leaf nodes `pairs`."
  [pairs]
  (if (empty? pairs)
    '()
    (let [pair (first pairs)]
      (adjoin-weighted-set (make-leaf (first pair) ; symbol
                                      (second pair)) ; frequency
                           (make-leaf-set (rest pairs))))))

;; TODO
(defn generate-huffman-tree
  "Returns a new Huffman tree from a sequence of symbol-weight `pairs`."
  [pairs])

;; the encoding procedure

;; TODO
(defn encode-symbol
  "Encodes the symbol `s` using Hufmann `tree`."
  [s tree])

(defn encode
  "Returns a sequence of bits from `message` encoded with the Huffman
  `tree`."
  [message tree]
  (if (empty? message)
    '()
    (append* (encode-symbol (first message) tree)
             (encode (rest message) tree))))

(comment

  (def sample-code-tree
    (make-code-tree (make-leaf 'A 4)
                    (make-code-tree (make-leaf 'B 2)
                                    (make-code-tree
                                     (make-leaf 'D 1)
                                     (make-leaf 'C 1)))))

  (decode '(1 1 0 0 1 0 1 1 0) sample-code-tree)

)


;; multiple representations for abstract data

;; representations for complex numbers

;; rectangular form: addition reduces to addition of coordinates of complex
;; numbers (sum of corresponding coordinates)

;; polar form: multiplication reduces to stretching vector of one complex number
;; by the length of the other and rotating it through the angle of the
;; other (product of magnitudes and sum of angles)

(declare real-part imag-part magnitude angle
         make-from-real-imag make-from-mag-ang)

;; required property of this abstract data interface: for any complex number z
;; (= (make-from-real-imag (real-part z) (imag-part z))
;;    (make-from-mag-ang (magnitude z) (angle z)))

(defn add-complex
  "Returns a sum of complex numbers `z1` and `z2`."
  [z1 z2]
  (make-from-real-imag (+ (real-part z1) (real-part z2))
                       (+ (imag-part z1) (imag-part z2))))

(defn sub-complex
  "Returns a result of subtraction of complex numbers `z1` and `z2`."
  [z1 z2]
  (make-from-real-imag (- (real-part z1) (real-part z2))
                       (- (imag-part z1) (real-part z2))))

(defn mul-complex
  "Returns a product of complex numbers `z1` and `z2`."
  [z1 z2]
  (make-from-mag-ang (* (magnitude z1) (magnitude z2))
                     (+ (angle z1) (angle z2))))

(defn div-complex
  "Returns a result of division of complex numbers `z1` and `z2`."
  [z1 z2]
  (make-from-mag-ang (/ (magnitude z1) (magnitude z2))
                     (- (angle z1) (angle z2))))

;; to complete the complex-number package, we must choose a representation and
;; implement constructors and selectors in terms of primitive numbers and
;; primitive list structure

;; if several abstract data representations are used simultaneously, we need
;; some way to distinguish objects represented differently, e.g. with a type tag

(defn attach-tag
  "Returns `contents` object with `type-tag` attached."
  [type-tag contents]
  (cond
    (seq? contents)    (cons type-tag contents)
    ;; numbers do not need a tag to be recognized
    (number? contents) contents
    ;; no dotted pairs in Clojure
    :else              (list type-tag contents)))

(defn type-tag
  "Returns the `datum`'s type tag. Returns nil and signals an error if
  `datum` is not a number or list structure."
  [datum]
  (cond
    (seq? datum)    (first datum)
    (number? datum) 'number
    :else           (error (format "bad tagged datum '%s'" datum) 'type-tag)))

(defn contents
  "Returns the contents of the `datum` without type tag. Returns nil and
  signals an error if `datum` is not a list structure."
  [datum]
  (cond (seq? datum)    (rest datum)
        ;; numbers do not need and do not have a tag
        (number? datum) datum
        :else           (error (format "bad tagged datum '%s'" datum)
                               'contents)))

(defn rectangular?
  "Returns true if complex number `z` is in rectangular form."
  [z]
  (= (type-tag z) 'rectangular))

(defn polar?
  "Returns true if complex number `z` is in polar form."
  [z]
  (= (type-tag z) 'polar))

;; the representations

;; note: within a given representation implementation a complex number is an
;; untyped list ( x and y or r and a); when a generic selector operates on a
;; complex number, it strips off the type tag, and constructor adds the type tag

;; rectangular

(declare real-part-rectangular imag-part-rectangular
         magnitude-rectangular angle-rectangular
         make-from-real-imag-rectangular make-from-mag-ang-rectangular)

(defn real-part-rectangular
  "Returns the real part of complex number `z` in rectangular
  representation."
  [z]
  (first z))

(defn imag-part-rectangular
  "Returns the imaginary part of complex number `z` in rectangular
  representation."
  [z]
  (second z))

(defn magnitude-rectangular
  "Returns the magnitude of complex number `z` in rectangular
  representation."
  [z]
  (sqrt****** (+ (square (real-part-rectangular z))
                 (square (imag-part-rectangular z)))))

(defn angle-rectangular
  "Returns the angle of complex number `z` in rectangular
  representation."
  [z]
  (Math/atan (/ (imag-part-rectangular z)
                (real-part-rectangular z))))

(defn make-from-real-imag-rectangular
  "Returns a new complex number in rectangular representation from real
  an imaginary parts `x` ans `y`."
  [x y]
  (attach-tag 'rectangular (list x y)))

(defn make-from-mag-ang-rectangular
  "Returns a new complex number in rectangular representation from
  magnitude and angle `r` and `a`."
  [r a]
  (attach-tag 'rectangular
              (list (* r (Math/cos a)) (* r (Math/sin a)))))

;; polar

(declare real-part-polar imag-part-polar
         magnitude-polar angle-polar
         make-from-real-imag-polar make-from-mag-ang-polar)

(defn real-part-polar
  "Returns the real part of complex number `z` in polar representation."
  [z]
  (* (magnitude-polar z) (Math/cos (angle-polar z))))

(defn imag-part-polar
  "Returns the imaginary part of complex number `z` in polar representation."
  [z]
  (* (magnitude-polar z) (Math/sin (angle-polar z))))

(defn magnitude-polar
  "Returns the magnitude of complex number `z` in polar representation."
  [z]
  (first z))

(defn angle-polar
  "Returns the angle of complex number `z` in polar representation."
  [z]
  (second z))

(defn make-from-real-imag-polar
  "Returns a new complex number in polar representation from real and
  imaginary parts `x` and `y`."
  [x y]
  (attach-tag 'polar
              (list (sqrt****** (+ (square x) (square y)))
                    (Math/atan (/ y x)))))

(defn make-from-mag-ang-polar
  "Returns a new complex number in polar representation from magnitude
  and angle `r` and `a`."
  [r a]
  (attach-tag 'polar (list r a)))

;; we can switch on type tag in generic (dispatching) procedures `real-part`,
;; `imag-part`, `magnitude` and `angle`, but then we cannot add new
;; representations additively (must modify the generic procedures); also
;; procedure names between implementations may clash, especially if there are
;; many and no one knows them all (can be mitigated with proper namespacing, as
;; in Clojure)

;; we will use data-directed programming instead --- maintain a dispatch table
;; and use it in generic procedures to look up implementations

(declare dispatch-table-put dispatch-table-get make-dispatch-table)

(defn make-dispatch-table
  "Returns a new empty dispatch table."
  []
  (atom {}))

(def dispatch-table (make-dispatch-table))

(defn dispatch-table-put
  "Puts an entry for generic procedure `generic-fn` along with a
  `type-tag` and the implementation procedure `impl-fn` in the
  `dispatch-table`. If dispatch table is not provided, the global one
  is used."
  ([generic-fn type-tag impl-fn]
   (dispatch-table-put dispatch-table generic-fn type-tag impl-fn))
  ([dispatch-table generic-fn tag-or-list-of-tags impl-fn]
   (swap! dispatch-table assoc-in [generic-fn tag-or-list-of-tags] impl-fn)))

(defn dispatch-table-get
  "Returns an implementation procedure from the `dispatch-table` by the
  `generic-fn` symbol and `type-tag`, or nil if not found. If dispatch
  table is not provided, the global one is used."
  ([generic-fn type-tag]
   (dispatch-table-get dispatch-table generic-fn type-tag))
  ([dispatch-table generic-fn type-tag]
   (get-in @dispatch-table [generic-fn type-tag])))

(comment

  @dispatch-table
  (dispatch-table-put 'real-part '(rectangular) 'real-part-rectangular)
  (dispatch-table-put 'imag-part '(rectangular) 'imag-part-rectangular)
  (dispatch-table-put 'make-from-real-imag
                      'rectangular (fn [x y]
                                     (attach-tag 'rectangular (list x y))))
  (dispatch-table-put 'print '(polar) 'println)

  (dispatch-table-get 'real-part '(rectangular))
  (dispatch-table-get 'print '(polar))
  (dispatch-table-get 'print 'yahoo)
  (let [make-from-real-imag
        (dispatch-table-get 'make-from-real-imag 'rectangular)]
    (make-from-real-imag 23 12))

)

;; and then isolate the implementations (in self-made packages, namespaces
;; provided by the programming language, whatever)

(defn use-complex-rectangular-package
  "Installs the package with a rectangular representation of complex
  numbers."
  []
  (letfn [(real-part [z] (first z))
          (imag-part [z] (second z))
          (make-from-real-imag [x y] (list x y))
          (magnitude [z] (Math/sqrt (+ (square (real-part z))
                                       (square (imag-part z)))))
          (angle [z] (Math/atan2 (imag-part z)
                                 (real-part z)))
          (make-from-mag-ang [r a]
            (list (* r (Math/cos a)) (* r (Math/sin a))))
          (tag [x] (attach-tag 'rectangular x))]
    (dispatch-table-put 'real-part '(rectangular) real-part)
    (dispatch-table-put 'imag-part '(rectangular) imag-part)
    (dispatch-table-put 'magnitude '(rectangular) magnitude)
    (dispatch-table-put 'angle '(rectangular) angle)
    (dispatch-table-put 'make-from-real-imag 'rectangular
                        (fn [x y] (tag (make-from-real-imag x y))))
    (dispatch-table-put 'make-from-mag-ang 'rectangular
                        (fn [r a] (tag (make-from-mag-ang r a))))
    :done))

(defn use-complex-polar-package
  "Installs the package with a polar representation of complex numbers."
  []
  (letfn [(magnitude [z] (first z))
          (angle [z] (second z))
          (make-from-mag-ang [r a] (list r a))
          (real-part [z] (* (magnitude z) (Math/cos (angle z))))
          (imag-part [z] (* (magnitude z) (Math/sin (angle z))))
          (make-from-real-imag [x y]
            (list (Math/sqrt (+ (square x) (square y)))
                  (Math/atan2 y x)))
          (tag [x] (attach-tag 'polar x))]
    (dispatch-table-put 'real-part '(polar) real-part)
    (dispatch-table-put 'imag-part '(polar) imag-part)
    (dispatch-table-put 'magnitude '(polar) magnitude)
    (dispatch-table-put 'angle '(polar) angle)
    (dispatch-table-put 'make-from-real-imag 'polar
                        (fn [x y] (tag (make-from-real-imag x y))))
    (dispatch-table-put 'make-from-mag-ang 'polar
                        (fn [r a] (tag (make-from-mag-ang r a))))
    :done))

(comment

  (use-complex-rectangular-package)
  (let [make-from-real-imag (dispatch-table-get 'make-from-real-imag
                                                'rectangular)]
    (make-from-real-imag 34 -1))

  (use-complex-polar-package)
  (let [make-from-real-imag (dispatch-table-get 'make-from-real-imag
                                                'polar)]
    (make-from-real-imag 34 -1))

  (reveal/inspect @dispatch-table)

)

;; the dispatch function

(defn apply-generic
  "Applies the generic procedure `op` to `args`. The concrete
  implementation for op is looked up in the `dispatch-table`."
  [op & args]
  (let [type-tags (map*** type-tag args)
        proc      (dispatch-table-get op type-tags)]
    (if proc
      (apply proc (map contents args))
      (error (format "No method '%s' for these types: '%s'" op type-tags)
             "apply-generic"))))

;; generic selectors and constructors

(defn real-part
  "Returns the real part of the complex number `z`."
  [z]
  (apply-generic 'real-part z))

(defn imag-part
  "Returns the imaginary part of the complex number `z`."
  [z]
  (apply-generic 'imag-part z))

(defn magnitude
  "Returns the magnitude of the complex number `z`."
  [z]
  (apply-generic 'magnitude z))

(defn angle
  "Returns the angle of the complex number `z`."
  [z]
  (apply-generic 'angle z))

(defn make-from-real-imag
  "Returns a new complex number with real and imaginary parts `x` and
  `y` using the rectangular representation."
  [x y]
  ((dispatch-table-get 'make-from-real-imag 'rectangular) x y))

(defn make-from-mag-ang
  "Returns a new complex number with magnitude and angle `r` and `a`
  using the polar representation."
  [r a]
  ((dispatch-table-get 'make-from-mag-ang 'polar) r a))

(use-complex-rectangular-package)
(use-complex-polar-package)

(comment

  (reveal/inspect @dispatch-table)

  (def c1 (make-from-real-imag 4 3))
  (def c2 (make-from-mag-ang (Math/sqrt 2) (/ Math/PI 4)))

  (real-part c1)
  (imag-part c1)
  (magnitude c1)
  (angle c1)

  (magnitude c2)
  (angle c2)
  (real-part c2)
  (imag-part c2)

)

;; alternative implementation strategy of generic procedures: message passing

;; no separate dispatch table is needed, the data object itself is a dispatch
;; function that knows implementations for all generic operation names

(defn make-from-real-imag*
  "Returns a new complex number with real and imaginary parts `x` and
  `y` using the rectangular representation. Uses message passing."
  [x y]
  (letfn [(dispatch [op]
            (case op
              real-part x
              imag-part y
              magnitude (Math/sqrt (+ (square x) (square y)))
              angle     (Math/atan2 y x)
              (error (format "Unknown op: ") "make-from-real-imag*")))]
    dispatch))

(defn apply-generic*
  "Applies the generic procedure `op` to `args`. Uses message passing as
  a dispatch strategy."
  [op arg]
  (arg op))

(def real-part*
  "Returns the real part of the complex number."
  (partial apply-generic* 'real-part))

(def imag-part*
  "Returns the imaginary part of the complex number."
  (partial apply-generic* 'imag-part))

(def magnitude*
  "Returns the magnitude of the complex number."
  (partial apply-generic* 'magnitude))

(def angle*
  "Returns the angle of the complex number."
  (partial apply-generic* 'angle))

(comment

  (def c3 (make-from-real-imag* 2 4))

  (apply-generic* 'real-part c3)

  (real-part* c3)
  (imag-part* c3)
  (magnitude* c3)
  (angle* c3)

)


;; systems with generic operations

;; generic arithmetic operations

(defn add
  "Returns the sum of numbers `x` and `y`."
  [x y]
  (apply-generic 'add x y))

(defn sub
  "Returns the result of division of numbers `x` and `y`."
  [x y]
  (apply-generic 'sub x y))

(defn mul
  "Returns the product of numbers `x` and `y`."
  [x y]
  (apply-generic 'mul x y))

(defn div
  "Returns the result of division of numbers `x` and `y`."
  [x y]
  (apply-generic 'div x y))

(defn zero?*
  "Returns true if the number `x` is equal to zero."
  [x]
  (apply-generic 'zero? x))

(defn use-number-package
  "Installs the package that implements basic arithmetic operations for
  integer and real numbers."
  []
  (letfn [(tag [x] (attach-tag 'number x))]
    (dispatch-table-put 'add '(number number) (fn [x y] (tag (+ x y))))
    (dispatch-table-put 'sub '(number number) (fn [x y] (tag (- x y))))
    (dispatch-table-put 'mul '(number number) (fn [x y] (tag (* x y))))
    (dispatch-table-put 'div '(number number) (fn [x y] (tag (/ x y))))
    (dispatch-table-put 'zero? '(number) (fn [x] (zero? x)))
    (dispatch-table-put 'make 'number (fn [x] (tag x)))
    :done))

(defn make-number
  "Returns a new integer or real number `n`."
  [n]
  ((dispatch-table-get 'make 'number) n))

(use-number-package)


(defn use-rational-package
  "Installs the package that implements basic arithmetic operations for
  rational numbers."
  []
  (letfn [(numer [x] (first x))
          (denom [x] (second x))
          (make-rat [n d]
            (let [g (gcd n d)]
              (list (/ n g) (/ d g))))
          (add-rat [x y]
            (make-rat (+ (* (numer x) (denom y))
                         (* (numer y) (denom x)))
                      (* (denom x) (denom y))))
          (sub-rat [x y]
            (make-rat (- (* (numer x) (denom y))
                         (* (numer y) (denom x)))
                      (* (denom x) (denom y))))
          (mul-rat [x y]
            (make-rat (* (numer x) (numer y))
                      (* (denom x) (denom y))))
          (div-rat [x y]
            (make-rat (* (numer x) (denom y))
                      (* (denom x) (numer y))))
          (tag [x] (attach-tag 'rational x))]
    (dispatch-table-put 'add '(rational rational)
                        (fn [x y] (tag (add-rat x y))))
    (dispatch-table-put 'sub '(rational rational)
                        (fn [x y] (tag (sub-rat x y))))
    (dispatch-table-put 'mul '(rational rational)
                        (fn [x y] (tag (mul-rat x y))))
    (dispatch-table-put 'div '(rational rational)
                        (fn [x y] (tag (div-rat x y))))
    (dispatch-table-put 'zero? '(rational)
                        (fn [x] (and (zero? (numer x))
                                     (not (zero? (denom x))))))
    (dispatch-table-put 'make 'rational
                        (fn [n d] (tag (make-rat n d))))
    :done))

(defn make-rational
  "Returns a new rational number with numerator `n` and denominator
  `d`."
  [n d]
  ((dispatch-table-get 'make 'rational) n d))

(use-rational-package)


(defn use-complex-package
  "Installs the package that implements basic arithmetic operations for
  complex numbers."
  []
  (letfn [(make-from-real-imag [x y]
            ((dispatch-table-get 'make-from-real-imag 'rectangular) x y))
          (make-from-mag-ang [r a]
            ((dispatch-table-get 'make-from-mag-ang 'polar) r a))
          (add-complex [z1 z2]
            (make-from-real-imag (+ (real-part z1) (real-part z2))
                                 (+ (imag-part z1) (imag-part z2))))
          (sub-complex [z1 z2]
            (make-from-real-imag (- (real-part z1) (real-part z2))
                                 (- (imag-part z1) (imag-part z2))))
          (mul-complex [z1 z2]
            (make-from-mag-ang (* (magnitude z1) (magnitude z2))
                               (+ (angle z1) (angle z2))))
          (div-complex [z1 z2]
            (make-from-mag-ang (/ (magnitude z1) (magnitude z2))
                               (- (angle z1) (angle z2))))
          (tag [z] (attach-tag 'complex z))]
    (dispatch-table-put 'add '(complex complex)
                        (fn [z1 z2] (tag (add-complex z1 z2))))
    (dispatch-table-put 'sub '(complex complex)
                        (fn [z1 z2] (tag (sub-complex z1 z2))))
    (dispatch-table-put 'mul '(complex complex)
                        (fn [z1 z2] (tag (mul-complex z1 z2))))
    (dispatch-table-put 'div '(complex complex)
                        (fn [z1 z2] (tag (div-complex z1 z2))))
    (dispatch-table-put 'zero? '(complex)
                        (fn [z] (and (zero? (real-part z))
                                     (zero? (imag-part z)))))
    (dispatch-table-put 'make-from-real-imag 'complex
                        (fn [x y] (tag (make-from-real-imag x y))))
    (dispatch-table-put 'make-from-mag-ang 'complex
                        (fn [r a] (tag (make-from-mag-ang r a))))
    :done))

(defn make-complex-from-real-imag
  "Returns a new complex number from real and imaginary parts `x` and
  `y` in rectangular representation."
  [x y]
  ((dispatch-table-get 'make-from-real-imag 'complex) x y))

(defn make-complex-from-mag-ang
  "Returns a new complex number from magnitude and angle `r` and `a` in
  polar representation."
  [r a]
  ((dispatch-table-get 'make-from-mag-ang 'complex) r a))

(use-complex-package)


(comment

  (reveal/inspect @dispatch-table)

  (add 1 13)

  (def r1 (make-rational 1 2))
  (def r2 (make-rational 1 3))
  (sub r1 r2)

  (def c1 (make-complex-from-real-imag 3 4))
  (def c2 (make-complex-from-real-imag 1 1))
  (add c1 c2)

  (def c3 (make-complex-from-mag-ang 2 (/ Math/PI 2)))
  (def c4 (make-complex-from-mag-ang 4 (/ Math/PI 2)))
  (mul c3 c4)

  (zero?* r1)
  (zero?* c1)
  (zero?* (make-number 0))
  (zero?* (make-rational 0 12))
  (zero?* (make-complex-from-real-imag 0 0))

)


;; combining data of different types

;; coercion

;; to implicitly coerce one type to another (related) type we define coercion
;; procedures and install them into a coercion table, and modify the
;; `apply-generic` procedure

(declare coercion-table-put coercion-table-get make-coercion-table)

(defn make-coercion-table
  "Returns a new empty coercion table."
  []
  (atom {}))

(def coercion-table (make-coercion-table))

(defn coercion-table-put
  "Puts an entry for coercion procedure `coercion-fn` under keypath
  of [`source-type` `target-type`] into `coercion-table`. If coercion
  table is not provided, the global one is used."
  ([source-type target-type coercion-fn]
   (coercion-table-put coercion-table source-type target-type coercion-fn))
  ([coercion-table source-type target-type coercion-fn]
   (swap! coercion-table assoc-in [source-type target-type] coercion-fn)))

(defn coercion-table-get
  "Returns a coercion procedure from the `coercion-table` by the
  `source-type` and `target-type`, or nil if not found. If coercion
  table is not provided, the global one is used."
  ([source-type target-type]
   (coercion-table-get coercion-table source-type target-type))
  ([coercion-table source-type target-type]
   (get-in @coercion-table [source-type target-type])))

;; example coercions

(defn number->complex
  "Coerces an integer or real number `n` to a complex number."
  [n]
  (make-complex-from-real-imag (contents n) 0))

(coercion-table-put 'number 'complex number->complex)

(comment

  (reveal/inspect @coercion-table)
  (coercion-table-get 'number 'complex)

)


(defn apply-generic-with-coercion
  "Applies the generic procedure `op` to `args`. The concrete
  implementation for op is looked up in the `dispatch-table`. Uses
  implicit coercion with `coercion-table`."
  [op & args]
  (let [type-tags (map type-tag args)
        proc      (dispatch-table-get op type-tags)]
    (if proc
      (apply proc (map contents args))
      (if (= (count args) 2)
        (let [type1 (first type-tags)
              type2 (second type-tags)
              a1    (first args)
              a2    (second args)
              t1->t2 (coercion-table-get type1 type2)
              t2->t1 (coercion-table-get type2 type1)]
          (cond
            t1->t2 (apply-generic op (t1->t2 a1) a2)
            t2->t1 (apply-generic op a1 (t2->t1 a2))
            :else (error (format "No method '%s' for these types: '%s'"
                                 op (apply list type-tags)))))
        (error (format "No method '%s' for these types: '%s'"
                       op (apply list type-tags)))))))

;; generic arithmetic operations with coercion

(defn add*
  "Returns the sum of numbers `x` and `y`. Tries to coerce arguments of
  different types to one of their types."
  [x y]
  (apply-generic-with-coercion 'add x y))

(defn sub*
  "Returns the result of division of numbers `x` and `y`. Tries to
  coerce arguments of different types to one of their types."
  [x y]
  (apply-generic-with-coercion 'sub x y))

(defn mul*
  "Returns the product of numbers `x` and `y`. Tries to coerce arguments
  of different types to one of their types."
  [x y]
  (apply-generic-with-coercion 'mul x y))

(defn div*
  "Returns the result of division of numbers `x` and `y`. Tries to
  coerce arguments of different types to one of their types."
  [x y]
  (apply-generic-with-coercion 'div x y))

(comment

  (def i1 (make-number 12))
  (def c1 (make-complex-from-real-imag 1 2))

  (add* i1 c2)

)


;; hierarchies of types


;; TODO: implement `promote`, `demote`, `project`; modify `apply-generic` to use
;; them


;; example: symbolic algebra

;; an algebraic expression is a tree of operators applied to operands; they can
;; be constructed by starting with a set of primitive objects (such as constants
;; and variables), and combining these by means of algebraic operators (such as
;; addition and multiplication); typical abstractions are linear combination,
;; polynomial, rational function, or trigonometric function, that can be seen as
;; compound "types".

;; arithmetic on polynomials

;; polynomials are defined relative to certain variables (indeterminates of the
;; polynomial); a polynomial is a sum of terms, each of which is either a
;; coefficient, a power of the indeterminate, or a product of a coefficient and
;; a power of the indeterminate; a coefficient is an algebraic expression that
;; is not dependent upon the indeterminate of the polynomial.

;; polynomials are represented using a data structure `poly`, which consists of
;; a variable and a collection of terms.

;; poly: constructors, selectors, predicates

;; (declare variable term-list make-poly) --- defined in the package

;; addition and multiplication of polys

(declare add-terms mul-terms mul-term-by-all-terms)

(defn use-polynomial-package
  "Installs the package for polynomial arithmetic."
  []
  (letfn [(make-poly [variable term-list]
            (cons variable term-list))
          (variable [p] (first p))
          (term-list [p] (rest p))
          ;; procedures `variable?` and `same-variable?` were defined earlier
          (add-poly [p1 p2]
            (if (same-variable? (variable p1) (variable p2))
              (make-poly (variable p1)
                         (add-terms (term-list p1) (term-list p2)))
              (error (format
                      "polynomials are not in the same variable: '%s', '%s'"
                      p1 p2) "add-poly")))
          (mul-poly [p1 p2]
            (if (same-variable? (variable p1) (variable p2))
              (make-poly (variable p1)
                         (mul-terms (term-list p1) (term-list p2)))
              (error (format
                      "polynomials are not in the same variable: '%s', '%s'"
                      p1 p2) "mul-poly")))
          (tag [p] (attach-tag 'polynomial p))]
    (dispatch-table-put 'add '(polynomial polynomial)
                        (fn [p1 p2] (tag (add-poly p1 p2))))
    (dispatch-table-put 'mul '(polynomial polynomial)
                        (fn [p1 p2] (tag (mul-poly p1 p2))))
    (dispatch-table-put 'make 'polynomial
                        (fn [var terms] (tag (make-poly var terms))))
    :done))

(defn make-polynomial
  "Returns a new polynomial from a list of `terms` in `var`."
  [var terms]
  ((dispatch-table-get 'make 'polynomial) var terms))

;; term lists: constructors, selectors and predicates

(declare empty-termlist?
         the-empty-termlist adjoin-term
         first-term rest-terms)

;; terms: constructors, selectors and predicates

(declare order coeff make-term)

(defn add-terms
  "Returns the sum of two polynomials' term lists `l1` and `l2`."
  [l1 l2]
  (cond
    (empty-termlist? l1) l2
    (empty-termlist? l2) l1
    :else (let [t1 (first-term l1)
                t2 (first-term l2)]
            (cond
              (> (order t1) (order t2)) (adjoin-term
                                         t1 (add-terms (rest-terms l1) l2))
              (< (order t1) (order t2)) (adjoin-term
                                         t2 (add-terms l1 (rest-terms l2)))
              :else (adjoin-term (make-term (order t1)
                                            (add (coeff t1) (coeff t2)))
                                 (add-terms (rest-terms l1)
                                            (rest-terms l2)))))))

(defn mul-terms
  "Returns the product of two polynomials' term lists `l1` and `l2`."
  [l1 l2]
  (if (empty-termlist? l1)
    (the-empty-termlist)
    (add-terms (mul-term-by-all-terms (first-term l1) l2)
               (mul-terms (rest-terms l1) l2))))

(defn mul-term-by-all-terms
  "Returns the term list `l` with all terms multiplied by term `t1`."
  [t1 l]
  (if (empty-termlist? l)
    (the-empty-termlist)
    (let [t2 (first-term l)]
      (adjoin-term (make-term (+ (order t1) (order t2))
                              (mul (coeff t1) (coeff t2)))
                   (mul-term-by-all-terms t1 (rest-terms l))))))

;; representing terms and term lists

;; association list: x^100 + 2x^2 + 1 -> '((100 1) (2 2) (0 1))

(defn adjoin-term
  "Returns the `term-list` with `term` adjoined to it."
  [term term-list]
  (if (zero?* (coeff term))
    term-list
    (cons term term-list)))

(defn the-empty-termlist
  "Returns the empty term list."
  [] '())

(defn first-term
  "Returns the first (highest-order) term of the `term-list`."
  [term-list]
  (first term-list))

(defn rest-terms
  "Returns the `term-list` without the first (highest-order) term."
  [term-list]
  (rest term-list))

(defn empty-termlist?
  "Returns true if the `term-list` is empty."
  [term-list]
  (empty? term-list))

(defn make-term
  "Returns a new term of the `order` with the `coeff`."
  [order coeff]
  (list order coeff))

(defn order
  "Returns the order of the `term`."
  [term]
  (first term))

(defn coeff
  "Returns the coefficient of the `term`."
  [term]
  (second term))


(use-polynomial-package)

(comment

  (reveal/inspect @dispatch-table)

  (def p1 (make-polynomial 'x '((100 2) (2 5) (0 12))))
  (def p2 (make-polynomial 'x '((0 2))))

  (add p1 p2)
  (mul p1 p2)

)


;; hierarchies of types in symbolic algebra

;; extended exercise: rational functions

;; rational functions are "fractions" whose numerator and denominator are
;; polynomials

;; TODO: implement division of polynomials, rational functions, simplifying
;; rational functions
