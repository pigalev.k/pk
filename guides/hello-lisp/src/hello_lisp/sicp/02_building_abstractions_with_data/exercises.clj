(ns hello-lisp.sicp.02-building-abstractions-with-data.exercises
  (:require [hello-lisp.sicp.02-building-abstractions-with-data.chapter
             :refer [append append*]]))


;; Exercise 2.29: A binary mobile conststs of two branches, a left brancth and a
;; right branch. Each branch is a rod of a certain length, from which hangs
;; either a weight or another binary mobile. We can represent a binary mobile
;; using compound data by constructing it from two branches.

(defn make-mobile
  "Returns a new mobile with branches `left` and `right`."
  [left right]
  (list left right))

;; A branch is constructed from a length (which must be a number) together with
;; a structure, which may be either a number (representing a simple weight) or
;; another mobile.

(defn make-branch
  "Returns a new branch of `length` and `structure`."
  [length structure]
  (list length structure))

;; 1. Write the corresponding selectors `left-branch` and `right-branch`, which
;; return the branches of a mobile, and `branch-length` and `branch-structure`,
;; which return the components of a branch.

;; 2. Using your selectors, define a procedure `total-weight` that returns the
;; total weight of a mobile.

;; 3. A mobile is said to be balanced if the torque applied by its top-left
;; branch is equal to that applied by its top-right branch (that is, if the
;; length of the left rod multiplied by the weight hanging from that rod is
;; equal to the corresponding product for the right side) and if each of the
;; submobiles hanging off its branches is balanced. Design a predicate that
;; tests whether a binary mobile is balanced.

;; 4. Suppose we change the representation of mobiles. How much do you need to
;; change your programs to convert to the new representation?

(defn left-branch
  "Returns the left branch of the mobile `m`."
  [m]
  (first m))

(defn right-branch
  "Returns the right branch of the mobile `m`."
  [m]
  (second m))

(defn branch-length
  "Returns the length of the branch `b`."
  [b]
  (first b))

(defn branch-structure
  "Returns the structure of the branch `b` (weight number or mobile
  structure)."
  [b]
  (second b))


(declare branch-total-weight mobile-total-weight)

(defn branch-total-weight
  "Returns the total weight of the branch `b`."
  [b]
  (let [weight-or-mobile (branch-structure b)]
    (cond
      (number? weight-or-mobile) weight-or-mobile
      :else                      (mobile-total-weight weight-or-mobile))))

(defn mobile-total-weight
  "Returns the total weight of the mobile `m`."
  [m]
  (cond
    (number? m) m
    :else       (+ (branch-total-weight (left-branch m))
                   (branch-total-weight (right-branch m)))))

(defn branch-total-torque
  "Returns the total torque of the branch `b` (its weight multiplied by
  its length)."
  [m]
  (* (branch-length m) (branch-total-weight m)))

(defn mobile-balanced?
  "Returns true if the mobile `m` is balanced."
  [m]
  (cond
    (number? m) true
    :else       (and (= (branch-total-torque (left-branch m))
                        (branch-total-torque (right-branch m)))
                     (mobile-balanced? (branch-structure (left-branch m)))
                     (mobile-balanced? (branch-structure (right-branch m))))))

(comment

  ;; unbalanced on 1st and 2nd level
  (def m1 (make-mobile (make-branch 2 4)
                       (make-branch 3 (make-mobile (make-branch 1 8)
                                                   (make-branch 5 1)))))

  ;; balanced
  (def m2 (make-mobile (make-branch 2 6)
                       (make-branch 4 (make-mobile (make-branch 2 1)
                                                   (make-branch 1 2)))))

  ;; unbalanced on 2nd level only (but still)
  (def m3 (make-mobile (make-branch 2 4)
                       (make-branch 4 (make-mobile (make-branch 3 1)
                                                   (make-branch 1 1)))))

  (mobile-total-weight m1)
  (mobile-total-weight m2)
  (mobile-total-weight m3)

  (mobile-balanced? m1)
  (mobile-balanced? m2)
  (mobile-balanced? m3)

  ;; it turns out, mobile can be just a weight sometimes
  (mobile-balanced? 1)
  (mobile-total-weight 1)

)


;; Exercise 2.32: We can represent a set as a list of distincy elements, and we
;; can represent the set of all subsets of the set as a list of lists. Complete
;; the definition of a procedure that generates the set of subsets of a set and
;; give a clear explanation of why it works.

(defn subsets
  "Returns a set of all subsets of a set `s`."
  [s]
  (if (empty? s)
    '(())
    (let [tail (subsets (rest s))
          head (first s)]
      (append* tail (map #(cons head %) tail)))))

(comment

  (subsets '(3))
  (subsets '(1 2 3))

)
