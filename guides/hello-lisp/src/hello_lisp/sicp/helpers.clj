(ns hello-lisp.sicp.helpers)


(defn error
  "Prints the error message to stderr, with optional tag to distinguish
  the place where the error occurred."
  [message & [tag]]
  (binding [*out* *err*]
    (println (if tag (str tag ": " message) message))))
