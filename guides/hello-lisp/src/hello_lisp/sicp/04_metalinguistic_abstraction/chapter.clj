(ns hello-lisp.sicp.04-metalinguistic-abstraction.chapter
  (:require
   [clojure.string :as string]
   [hello-lisp.sicp.helpers :refer [error]]))


;; the metacircular evaluator

;; the core of the evaluator

;; the evaluation process is an interplay between two procedures: `eval*` and
;; `apply*`.

(declare eval* apply*)


;; `eval*` classifies the expression using case analysis; each expression type
;; has its own predicate and selectors (abstract syntax)

;; primitive expressions:

;; - self-evaluating expressions (e.g., numbers) evaluate to themselves
;; - variables evaluate to their values found in the environment

;; special forms:

;; - quoted expressions evaluate to the expression that was quoted
;; - a variable definition recursively calls `eval*` to compute the new value to
;;   be associated with the variable; the environment must be modified to create
;;   or change the binding of the variable
;; - an `if` expression has special rules of evaluation and evaluates to only
;;   one of its parts (then or else expressions) depending on the value of the
;;   value of the predicate expression
;; - a `fn` (lambda) expression must be transformed into an applicable procedure
;;   by packaging together the parameters and the body specified by the lambda
;;   expression with the environment of the evaluation
;; - a `begin` expression requires evaluating its sequence of expressions in the
;;   order in which they appear
;; - a `cond` (case analysis) is transformed into a nested `if` expressions and
;;   then evaluated

;; combinations:

;; - for a procedure application, `eval*` must recursively evaluate the operator
;;   part and the operands of the combination; the resulting procedure and
;;   arguments are passed to `apply*`, which handles the actual procedure
;;   application.

;; for extensibility (adding new types of expressions), `eval*` should be
;; implemented in a data-directed style (e.g., multimethods) instead of case
;; analysis

(declare self-evaluating? variable? quoted? assignment?
         definition? function-definition? if? lambda? begin? cond? application?)

(declare lookup-variable-value text-of-quotation operator operands
         list-of-values begin-actions lambda-parameters lambda-body cond->if
         eval-assignment eval-definition eval-if eval-sequence
         make-procedure)

(defn eval*
  "Evaluates the expression `expr` in context of the environment `env`."
  [expr env]
  (cond
    (self-evaluating? expr) expr
    (variable? expr)        (lookup-variable-value expr env)
    (quoted? expr)          (text-of-quotation expr)
    (assignment? expr)      (eval-assignment expr env)
    (definition? expr)      (eval-definition expr env)
    (if? expr)              (eval-if expr env)
    (lambda? expr)          (make-procedure (lambda-parameters expr)
                                            (lambda-body expr)
                                            env)
    (begin? expr)           (eval-sequence (begin-actions expr) env)
    (cond? expr)            (eval* (cond->if expr) env)
    (application? expr)     (apply* (eval* (operator expr) env)
                                    (list-of-values (operands expr) env))
    :else (error (format "unknown expression type: '%s'" expr) "eval*")))

(comment

  (def env (list (atom {})))

  (eval* '(+ 1 2) env)
  (eval* '(define x 1) env)

  ;; global environment contains the procedure's value, and the procedure's
  ;; value contains its environment, that contains the global environment as its
  ;; enclosing environment (misleading term, should rename); this is a circular
  ;; reference that will cause stack overflow if we try, e.g., to print the
  ;; global environment; but the repl works
  (eval* '(defun f (x) (* x x)) env)
  (keys @(first env))

)


;; `apply*` classifies procedures into two kinds:
;; - primitive procedures are just applied to their arguments
;; - compound procedures get expressions of their bodies sequentially evaluated
;;   first to construct a procedure body; the environment for the evaluation of
;;   the body of a compound procedure is constructed by extending the base
;;   environment carried by the procedure to include a frame that binds the
;;   parameters of the procedure to the arguments to which the procedure is to
;;   be applied

(declare primitive-procedure? primitive-implementation
         compound-procedure?)

(declare apply-primitive-procedure apply-compound-procedure
         procedure-parameters procedure-body procedure-environment
         extend-environment)

(defn apply*
  "Applies `procedure` to `arguments`."
  [procedure arguments]
  (cond
    (primitive-procedure? procedure)
    (apply-primitive-procedure procedure arguments)
    (compound-procedure? procedure) (eval-sequence
                                     (procedure-body procedure)
                                     (extend-environment
                                      (procedure-parameters procedure)
                                      arguments
                                      (procedure-environment procedure)))
    :else (error (format "unknown procedure type: '%s'" procedure))))


(comment

  (apply* '(primitive +) '(1 2))

)


;; evaluating procedure arguments

;; `list-of-values` is used instead of `map` to emphasize the fact that the
;; evaluator can be implemented in a language without higher-order procedures

(declare no-operands? first-operand rest-operands)

(defn list-of-values
  "Returns a list of values of `exprs` in context of `env`."
  [exprs env]
  (if (no-operands? exprs)
    '()
    (cons (eval* (first-operand exprs) env)
          (list-of-values (rest-operands exprs) env))))

(comment

  (list-of-values '((+ 1 2) a 3 4) (atom '({a 1})))

)


;; evaluating conditionals

(declare if-predicate if-consequent if-alternative true?*)

(defn eval-if
  "Returns a value of consequent clause of `expr` if its predicate
  clause evaluates to true, a value of alternative clause otherwise."
  [expr env]
  (if (true?* (eval* (if-predicate expr) env))
    (eval* (if-consequent expr) env)
    (eval* (if-alternative expr) env)))

(comment

  (def env (list (atom {'a 1 't 't 'f 'f})))
  (eval-if '(if a 'ok 'not-ok) env)
  (eval-if '(if f 'ok 'not-ok) env)

)

;; evaluating sequences of expressions

(declare last-expr? first-expr rest-exprs)

(defn eval-sequence
  "Evaluates `exprs` sequentially in context of `env` and returns a
  value of the last expression."
  [exprs env]
  (cond
    (last-expr? exprs) (eval* (first-expr exprs) env)
    :else              (do
                         (eval* (first-expr exprs) env)
                         (eval-sequence (rest-exprs exprs) env))))

(comment

  (eval-sequence '(1 2 3 a) (atom '({a 4})))

)


;; evaluating assignments and definitions

(declare assignment-variable assignment-value set-variable-value!
         definition-variable definition-value define-variable!)

(defn eval-assignment
  "Obtains the variable name and value expression from `expr`, evaluates
  the expression and binds the value to the (already existent) name in
  the `env`."
  [expr env]
  (set-variable-value! (assignment-variable expr)
                       (eval* (assignment-value expr) env)
                       env)
  :ok)

(defn eval-definition
  "Obtains the variable name and value expression from `expr`, evaluates
  the expression and binds the value to the (possibly existent) name
  in the `env`."
  [expr env]
  (define-variable! (definition-variable expr)
    (eval* (definition-value expr) env)
    env)
  :ok)

(comment

  (def e (atom '({a 1})))
  (eval-assignment '(set! a 10) e)
  (eval-definition '(define b 12) e)
  (eval-definition '(defun f (x) (* x x)) e)
  @e

)


;; representing expressions

(declare tagged-list? tagged-list-tag make-lambda make-if
         sequence->expr make-begin)

;; the only self-evaluating items are numbers and strings

(defn self-evaluating?
  "Returns true if `expr` is self-evaluating."
  [expr]
  (or (number? expr)
      (string? expr)))

(comment

  (self-evaluating? 1)
  (self-evaluating? '(+ 1 2))

)


;; variables are represented by symbols

(defn variable?
  "Returns true if `expr` is a variable name."
  [expr]
  (symbol? expr))

(comment

  (variable? 'a)
  (variable? 1)

)


;; quotations have the form (quote <text-of-quotation>)

(defn quoted?
  "Returns true if `expr` is a quoted form."
  [expr]
  (tagged-list? expr 'quote))

(defn text-of-quotation
  "Returns the expression quoted by `expr`."
  [expr]
  (first (rest expr)))

(defn tagged-list?
  "Returns true if `expr` is a list tagged with `tag` (beginning with
  it)."
  [expr tag]
  (if (seq? expr)
    (= (first expr) tag)
    false))

(defn tagged-list-tag
  "Returns the tag of the tagged list `expr`."
  [expr]
  (first expr))

(comment

  (quoted? '(quote (1 2 3)))
  (text-of-quotation '(quote (1 2 3)))

  (tagged-list? '(foo 1 2 3) 'foo)
  (tagged-list-tag '(foo 1 2 3))

)


;; assignments have the form (set! <var> <value>)

(defn assignment?
  "Returns true if `expr` is an assignment."
  [expr]
  (tagged-list? expr 'set!))

(defn assignment-variable
  "Returns the variable name of the assignment expression `expr`."
  [expr]
  (first (rest expr)))

(defn assignment-value
  "Returns the value of the assignment expression `expr`."
  [expr]
  (-> expr rest rest first))

(comment

  (assignment? '(set! a 1))
  (assignment? '(def a 1))

  (assignment-variable '(set! a 1))
  (assignment-value '(set! a 1))

)


;; definitions have the form (define <var> <value>) or the form (defun
;; <var (<parameter>*) <body>); the latter form is syntactic sugar for (define
;; <var> (fun (<parameter>*) <body>))

(defn definition?
  "Returns true if `expr` is a definition."
  [expr]
  (or (tagged-list? expr 'define)
      (tagged-list? expr 'defun)))

(defn function-definition?
  "Returns true if `expr` is a function definition."
  [expr]
  (= 'defun (tagged-list-tag expr)))

(defn definition-variable
  "Returns the variable name of the definition expression `expr`."
  [expr]
  (first (rest expr)))

(defn definition-value
  "Returns the expression that will produce a value for the definition
  expression `expr`."
  [expr]
  (if (function-definition? expr)
    (make-lambda (-> expr rest rest first)
                 (-> expr rest rest rest))
    (-> expr rest rest first)))

(comment

  (def d1 '(define v 12))
  (def d2 '(defun f (x) (* x x) (print x)))

  (definition? d1)
  (definition? d2)
  (definition? '(def a 2))

  (function-definition? d1)
  (function-definition? d2)

  (definition-variable d1)
  (definition-variable d2)

  (definition-value d1)
  (definition-value d2)

  (-> d2 rest rest rest)

)


;; lambda expressions are lists that begin with the symbol `fun`
;; example: (fun (x) (* x x))

(defn lambda?
  "Returns true if `expr` is a lambda expression."
  [expr]
  (tagged-list? expr 'fun))

(defn lambda-parameters
  "Returns the parameters list of lambda expression `expr`."
  [expr]
  (-> expr rest first))

(defn lambda-body
  "Returns the body (a sequence of expressions) of lambda expression
  `expr`."
  [expr]
  (-> expr rest rest))

(defn make-lambda
  "Returns a lambda (anonymous function) expression with `parameters`
  and `body`."
  [parameters body]
  (cons 'fun (cons parameters body)))

(comment

  (def f1 '(fun f (x) (* x x) (print x)))

  (lambda? f1)
  (lambda-parameters f1)
  (lambda-body f1)
  (make-lambda '(x y) '((print x y) (+ x y)))
  (make-lambda '(x y) '((print x y)))

)


;; conditionals begin with `if` and have a predicate, a consequent, and an
;; (optional) alternative

(defn if?
  "Returns true if `expr` is a conditional form."
  [expr]
  (tagged-list? expr 'if))

(defn if-predicate
  "Returns the predicate expression of `expr`."
  [expr]
  (first (rest expr)))

(defn if-consequent
  "Returns the consequent expression of `expr`."
  [expr]
  (-> expr rest rest first))

(defn if-alternative
  "Returns the alternative expression of `expr`, or symbol 'false if
  absent."
  [expr]
  (let [alternative (-> expr rest rest rest first)]
    (if (seq? alternative) alternative 'false)))

(defn make-if
  "Returns a new conditional expression with `predicate`, `consequent`
  and `alternative`."
  [predicate consequent alternative]
  (list 'if predicate consequent alternative))

(comment

  (def i1 '(if (zero? 0) (print ok) (print not-ok)))
  (def i2 '(if (zero? 0) (print ok)))

  (if? i1)
  (if? i2)
  (if-predicate i1)
  (if-predicate i2)
  (if-consequent i1)
  (if-consequent i2)
  (if-alternative i1)
  (if-alternative i2)

  (make-if '(neg? 1) '(print "yeah") '(print "nope"))

  ;; maybe always require an alternative?

)


;; `begin` packages a sequence of expressions into a single expression

(defn begin?
  "Returns true if `expr` is a sequence of expressions."
  [expr]
  (tagged-list? expr 'begin))

(defn begin-actions
  "Returns the sequence of expressions of `expr`."
  [expr]
  (rest expr))

(defn last-expr?
  "Returns true if `seq` contains just one expression."
  [seq]
  (empty? (rest seq)))

(defn first-expr
  "Returns the first expression in `seq`."
  [seq]
  (first seq))

(defn rest-exprs
  "Returns the rest of expressions in `seq`."
  [seq]
  (rest seq))

(defn make-begin
  "Returns sequence of expressions `seq` wrapped in a `begin`
  expression."
  [seq]
  (cons 'begin seq))

(defn sequence->expr
  "Returns the `seq` of exprs transformed into a single expression,
  using `begin` if necessary."
  [seq]
  (cond
    (empty? seq)     seq
    (last-expr? seq) (first-expr seq)
    :else            (make-begin seq)))

(comment

  (def b1 '(begin (print a) (+ 8 77) ok))
  (def b2 '(begin (print a)))

  (begin? b1)
  (begin? b2)
  (begin-actions b1)
  (begin-actions b2)

  (def a1 (begin-actions b1))
  (def a2 (begin-actions b2))
  (last-expr? a1)
  (last-expr? a2)
  (first-expr a1)
  (first-expr a2)
  (rest-exprs a1)
  (rest-exprs a2)

  (make-begin '(a (+ 1 2) (foo 3)))
  (make-begin '(a))

  (sequence->expr '(a (+ 1 2) (foo 3)))
  (sequence->expr '(a))

)


;; a procedure application is any compound expression that is not one of the
;; above expression types

(defn application?
  "Returns true if `expr` is a procedure application."
  [expr]
  (list? expr))

(defn operator
  "Returns the operator of procedure application `expr`."
  [expr]
  (first expr))

(defn operands
  "Returns the operands of procedure application `expr`."
  [expr]
  (rest expr))

(defn no-operands?
  "Returns true if list of operands `ops` is empty."
  [ops]
  (empty? ops))

(defn first-operand
  "Returns the first operand in `ops`."
  [ops]
  (first ops))

(defn rest-operands
  "Returns the rest of the operands in `ops`."
  [ops]
  (rest ops))

(comment

  (def ap1 '(+ 1 2 3))

  (application? ap1)
  (operator ap1)
  (operands ap1)

  (def ops1 (operands ap1))
  (no-operands? ops1)
  (no-operands? '())
  (first-operand ops1)
  (rest-operands ops1)

)



;; derived expressions

;; some special forms can be defined in terms of expressions involving other
;; special forms, rather than implemented directly

(declare cond-clauses cond-first-clause cond-rest-clauses
         cond-predicate cond-action cond-else-clause? expand-clauses)

(defn cond?
  "Returns true if `expr` is a case analysis expression."
  [expr]
  (tagged-list? expr 'cond))

(defn cond-clauses
  "Returns a sequence of clauses (predicate-action pairs) of the case
  analysis expression `expr`."
  [expr]
  (rest expr))

(defn cond-first-clause
  "Returns the first clause (first two forms) in `clauses`."
  [clauses]
  (list (first clauses) (second clauses)))

(defn cond-rest-clauses
  "Returns the rest of `clauses`."
  [clauses]
  (rest (rest clauses)))

(defn cond-else-clause?
  "Returns true if `clause` is a final catch-all clause."
  [clause]
  (= 'else (cond-predicate clause)))

(defn cond-predicate
  "Returns the predicate expression of the `clause`."
  [clause]
  (first clause))

(defn cond-action
  "Returns the action expression of the `clause`."
  [clause]
  (first (rest clause)))

(defn expand-clauses
  "Returns the sequence `clauses` transformed to a nested conditional
  expression."
  [clauses]
  (if (empty? clauses)
    'false
    (let [head (cond-first-clause clauses)
          tail (cond-rest-clauses clauses)]
      (if (cond-else-clause? head)
        (if (empty? tail)
          (cond-action head)
          (error (format "else clause isn't the last: '%s'" clauses)
                 "expand-clauses"))
        (make-if (cond-predicate head)
                 (cond-action head)
                 (expand-clauses tail))))))

(defn cond->if
  "Returns the case analysis expression `expr` transformed to nested
  conditional expression."
  [expr]
  (expand-clauses (cond-clauses expr)))

(comment

  (def c1 '(cond
             (empty? x) x
             (= x 2)    even
             else       (error "Boo")))

  (cond? c1)
  (cond-clauses c1)

  (def cl1 (cond-clauses c1))
  (cond-first-clause cl1)
  (expand-clauses cl1)

  (def cls1 (cond-first-clause cl1))
  (def cls2 (cond-first-clause (cond-rest-clauses (cond-rest-clauses cl1))))
  (cond-else-clause? cls1)
  (cond-else-clause? cls2)
  (cond-predicate cls1)
  (cond-action cls1)
  (cond-predicate cls2)
  (cond-action cls2)

)


;; evaluator data structures

;; the evaluator must define the data structures that it manipulates at runtime,
;; such as the representation of procedures, environments, true and false, etc

;; testing of predicates

(defn true?*
  "Returns true if `x` represents truth."
  [x]
  (not= 'f x))

(defn false?*
  "Returns true if `x` represents falsity."
  [x]
  (= 'f x))

(comment

  (true?* 'true)
  (true?* 't)
  (true?* 'f)

  (false?* 'f)

)


;; representing procedures

(defn make-procedure
  "Returns a new procedure object with `parameters`, `body` and `env`."
  [parameters body env]
  (list 'procedure parameters body env))

(defn compound-procedure?
  "Returns true if `p` is a compound procedure."
  [p]
  (tagged-list? p 'procedure))

(defn procedure-parameters
  "Returns the list of parameters of procedure `p`."
  [p]
  (first (rest p)))

(defn procedure-body
  "Returns the body of procedure `p`."
  [p]
  (-> p rest rest first))

(defn procedure-environment
  "Returns the environment of procedure `p`."
  [p]
  (-> p rest rest rest first))

(comment

  (def cp1 (make-procedure '(x y) '(* x y) (atom '({x 1}))))
  (compound-procedure? cp1)
  (procedure-parameters cp1)
  (procedure-body cp1)
  (procedure-environment cp1)

)


;; operations on environments

;; environment is an immutable sequence (list) of frames; frame is a
;; mutable (atom) associative data structure (map) mapping symbols to values

(declare the-empty-environment first-frame enclosing-environment
         make-frame frame-variables frame-values add-binding-to-frame!)


(defn lookup-variable-value
  "Returns the value that is bound to the symbol `var` in the
  environment `env` (or its enclosing environments), or signals an
  error and returns `::unbound` if unbound."
  [var env]
  (cond
    (empty? env)                       (do
                                         (error (format "unbound var: '%s'" var)
                                                "lookup-variable-value")
                                         ::unbound)
    (contains? @(first-frame env) var) (get @(first-frame env) var)
    :else                              (recur var
                                              (enclosing-environment env))))

(defn extend-environment
  "Adds a new (innermost) frame that associates `variables` with
  `values` to the `base-env`. Returns the extended environment."
  [variables values base-env]
  (if (not= (count variables) (count values))
    (error (format
            "lists of variables and values are of different length: '%s', '%s'"
            variables values) "extend-environment")
    (conj base-env (make-frame variables values))))

(defn define-variable!
  "Adds a new binding for `var` and `value` to the first (innermost)
  frame of the `env`."
  [var value [first-frame :as env]]
  (add-binding-to-frame! var value first-frame))

(defn set-variable-value!
  "Changes the binding of the `var` to `value` in the innermost frame of
  the `env`, or signals an error if unbound."
  [var value env]
  (when-not (= ::unbound (lookup-variable-value var env))
    (define-variable! var value env)))

(defn the-empty-environment
  "Returns the empty environment."
  []
  '())

(defn enclosing-environment
  "Returns the enclosing environment of `env`."
  [env]
  (rest env))

(defn first-frame
  "Returns the first frame of the `env`."
  [env]
  (first env))

(comment

  (def e1 (the-empty-environment))
  (lookup-variable-value 'a e1)
  (lookup-variable-value 'x e1)

  (def e2 (extend-environment '(n) '(3) e1))
  (define-variable! 'x 12 e2)
  (lookup-variable-value 'x e2)

  (def e3 (extend-environment '(a b c) '(1 2 3) e2))
  (lookup-variable-value 'a e3)
  (lookup-variable-value 'x e3)

  (define-variable! 'x 123 e3)
  (lookup-variable-value 'x e3)
  (set-variable-value! 'x -11 e3)
  (lookup-variable-value 'x e3)
  (set-variable-value! 'y 1 e3)

  (define-variable! 'f '(fun (x) (* x x)) e3)
  (lookup-variable-value 'f e3)

  (first-frame e3)
  (enclosing-environment e3)
  (enclosing-environment (enclosing-environment e3))

)


(defn make-frame
  "Returns a new frame with bindings of `variables` to `values`."
  [variables values]
  (atom (zipmap variables values)))

(defn add-binding-to-frame!
  "Adds a binding for `var` and `val` to the `frame`."
  [var val frame]
  (swap! frame assoc  var val))

(comment

  (def fr1 (make-frame '(a b c) '(1 2 3)))
  (add-binding-to-frame! 'x 123 fr1)

)


;; running the evaluator as a program

;; the evaluator reduces expressions to the application of primitive procedures;
;; so there should be a mechanism that calls on the underlying Lisp system to
;; model the application of primitive procedures --- a binding for each
;; primitive procedure name (and also the symbols 'true and 'false) in a global
;; environment

(declare setup-environment the-global-environment
         primitive-procedure-names primitive-procedure-objects)

(defn setup-environment
  "Returns a new environment with bindings for primitive procedure
  names and boolean values."
  []
  (let [initial-env (extend-environment (primitive-procedure-names)
                                        (primitive-procedure-objects)
                                        (the-empty-environment))]
    (define-variable! 't 't initial-env)
    (define-variable! 'f 'f initial-env)
    initial-env))

(defn primitive-procedure?
  "Returns true if `proc` is a primitive procedure."
  [proc]
  (tagged-list? proc 'primitive))

(defn primitive-implementation
  "Returns the implementation of the primitive procedure `proc`."
  [proc]
  (first (rest proc)))

(def primitive-procedures
  (list (list 'car first)
        (list 'cdr rest)
        (list 'cons cons)
        (list '+ +)
        (list '* *)
        (list 'null? empty?)))

(defn primitive-procedure-names
  "Returns names of the primitive procedures."
  []
  (map first primitive-procedures))

(defn primitive-procedure-objects
  "Returns values of the primitive procedures."
  []
  (map (fn [proc]
         (list 'primitive (second proc))) primitive-procedures))

;; to apply a primitive procedure, we simply apply the implementation procedure
;; to the arguments, using the underlying Lisp system

(defn apply-primitive-procedure
  "Applies the primitive procedure `proc` to `args`."
  [proc args]
  (apply (primitive-implementation proc) args))

(comment

  (def pp1 `(primitive ~+))
  (primitive-implementation pp1)
  (apply-primitive-procedure pp1 '(1 2 3))

)

;; a driver loop

(declare driver-loop read* print*)

(def input-prompt "> ")
(def output-prompt ";; ")

(defn driver-loop
  "Starts a read*-eval*-print*-loop of the metacircular evaluator."
  ([]
   (driver-loop {}))
  ([{:keys [input-prompt output-prompt quit-symbol]
     :or   {input-prompt  "mce/a> "
            output-prompt "=> "
            quit-symbol   'quit}
     :as   opts}]
   (let [the-global-environment (setup-environment)]
     (println (format "Metacircular evaluator (applicative), '%s' to quit"
                      quit-symbol))
     (loop []
       (print input-prompt)
       (flush)
       (let [input (read*)]
         (when-not (= input quit-symbol)
           (let [output (eval* input the-global-environment)]
             (print output-prompt)
             (print* output)
             (recur))))))))

(def read* read)

(defn print*
  "Prints the `object`. Avoids printing environments, as they are
  cyclic."
  [object]
  (cond
    (compound-procedure? object)
    (println
     (list 'compound-procedure
           (procedure-parameters object)
           (procedure-body object)
           (format "<environment, has vars: %s>"
                   (string/join ", "
                                (mapcat #(keys @%)
                                        (procedure-environment object))))))
    :else (println object)))

;; run the driver loop (see `deps.edn` for how to run it from command line)

(comment

  (driver-loop)

  ;; example inputs (without quotes)

  '(defun append (x y)
    (if (null? x)
      y
      (cons (car x) (append (cdr x) y))))
  '(append '(a b c) '(d e f))

  '(defun f (x) (* x x))
  '(f 5)
  'f

  '(defun g (x)
     (fun (y) (* y x)))
  '(g 5)
  '(define g5 (g 5))
  '(g5 7)
  'g5

  '(if t 'ok 'not-ok)
  '(if f 'ok 'not-ok)

  '(define a 7)
  'a
  '(set! a 12)
  'a

)


;; data as programs
