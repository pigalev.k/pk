# hello-lisp

Exploring fundamentals of Lisp.

## Sources

- "Structure and Interpretation of Computer Programs" by Harold Abelson and
  Gerald Jay Sussman with Julie Sussman, foreword by Alan J. Perlis, second
  edition, MIT Press, 1996 (unofficial Texinfo format, 2016).
- "Little Schemer" by Daniel P. Friedman and Matthias Felleisen, foreword by
  Gerald J. Sussman, fourth edition, MIT Press, 1996.
- "Seasoned Schemer" by Daniel P. Friedman and Matthias Felleisen, foreword and
  afterword by Guy L. Steele Jr., MIT Press, 1996.
- Practical Common Lisp https://gigamonkeys.com/book/
- "The Scheme Programming Language, fourth edition", by R. Kent Dybvig
  https://www.scheme.com/tspl4/

## Getting started

### Clojure

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.

### Fennel

Open a Fennel file, start a REPL (`C-c C-z`), explore and evaluate the code.

Compile to Lua

```
fennel --require-as-include --compile hello.fnl > hello.lua
```

### Hy

Open a Hy file, start a REPL (`C-c C-z`), explore and evaluate the code.

### Scheme

Open a Scheme file, start a REPL (`C-c C-z` or `M-x geiser`), explore and
evaluate the code.
