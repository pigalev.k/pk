(ns hello-core-async.intro
  (:require [clojure.core.async :as async
             :refer [>! <! >!! <!! go chan buffer close! alts!!]]))


;; Introduction to Asynchronous Programming in Clojure
;; https://www.bradcypert.com/clojure-async/

(def warehouse-capacity 10)
(def warehouse-channel (chan warehouse-capacity))

(def stock-map {0 :shirt
                1 :pants
                2 :socks
                3 :shoes})

(defn generate-random-items
  []
  (let [items (for [x (range warehouse-capacity)]
                (rand-int (count (keys stock-map))))]
    (map #(get stock-map %) items)))

(defn load-items-into-channel
  [items channel]
  (map #(>!! channel %) items))

(defn make-payment-channel []
  (let [payments (chan)]
    (go (while true
          (let [in (<! payments)]
            (if (number? in)
              (do (println (<!! warehouse-channel)))
              (println "We only accept numeric values!")))))
    payments))

(dorun (load-items-into-channel (generate-random-items) warehouse-channel))

(comment

  (<!! warehouse-channel)

  (def incoming (make-payment-channel))

  (>!! incoming 5)
  (>!! incoming :foo)

  (>!! warehouse-channel :banana)
  (>!! warehouse-channel :hat)

)


(defn make-payment-channel-1
  []
  (let [payments (chan)]
    (go (while true
          (let [in (<! payments)]
            (if (number? in)
              (let [[item ch] (alts!! [warehouse-channel banana-channel])]
                (println item))
              (println (str "We only accept numeric values!"
                            " No Number, No Clothes || Bananas!"))))))
    payments))


(comment

  (def banana-channel (chan (async/sliding-buffer 3)))
  (>!! banana-channel :banana)

  (def new-payments (make-payment-channel-1))

  (>!! new-payments 5)
  (>!! new-payments :foo)



  (close! banana-channel)
  (<!! banana-channel)
  (<!! banana-channel)
)
