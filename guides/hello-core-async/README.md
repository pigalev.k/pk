# hello-core-async

Exploring core.async --- facilities for async programming and communication in
Clojure.

- https://github.com/clojure/core.async
- https://clojure.github.io/core.async/

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
