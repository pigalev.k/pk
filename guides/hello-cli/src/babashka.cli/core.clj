(ns babashka.cli.core
  (:require
   [babashka.cli :as cli]
   [clojure.string :as s]))


;; basics

;; https://github.com/babashka/cli

;; - passes arguments to functions like `clj -X` does, but without so much
;;   quoting
;; - don't break on unknown extra arguments
;; - do coercion only, defer validation to the function called


;;;; parsing command-line options: `parse-opts`

(comment

  ;; options are key/value pairs (value may be implicit boolean); keys can
  ;; start with `-` (short options), `--` (long options) or `:` (short or long
  ;; options)

  ;; arguments are just values (positional)


  ;; long options

  (cli/parse-opts [":port" "1339"] {:coerce {:port :long}})
  (cli/parse-opts ["--port" "1339"] {:coerce {:port :long}})
  (cli/parse-opts ["--foo=bar"] {:coerce {:foo :keyword}})

  ;; short options

  (cli/parse-opts [":p" "1339"] {:alias {:p :port} :coerce {:port :long}})
  (cli/parse-opts ["-p" "1339"] {:alias {:p :port} :coerce {:port :long}})

  ;; args (values without keys) are ignored

  (cli/parse-opts [":port" "1339" "foo" "bar"] {:coerce {:port :long}})


  ;; coerce multiple values into a collection

  (cli/parse-opts ["--paths" "src" "--paths" "test"] {:coerce {:paths []}})
  (cli/parse-opts ["--paths" "src" "test"] {:coerce {:paths []}})
  (cli/parse-opts ["--foo" "bar" "--foo" "baz"] {:coerce {:foo [:keyword]}})


  ;; boolean options (flags); default is true

  (cli/parse-opts ["--verbose"])
  (cli/parse-opts ["--no-verbose"])
  (cli/parse-opts ["-v" "-v" "-v"] {:alias  {:v :verbose}
                                    :coerce {:verbose []}})
  (cli/parse-opts ["-abc"])


  ;; auto-coercion (uses `auto-coerce`; detects booleans, numbers, keywords)

  (cli/parse-opts ["-a" "false"])
  (cli/parse-opts ["-a" "1"])
  (cli/parse-opts ["-a" ":foo"])

  ;; moving a positional argument into the options

  (cli/parse-opts ["--force" "ssh://foo"] {:coerce     {:force :boolean}
                                           :args->opts [:url]})

  ;; moving several positional arguments into the options

  (cli/parse-opts ["arg1" "arg2" "arg3" "arg4"]
                  {:args->opts [:foo :bar :baz :quux :frob]})

  (cli/parse-opts ["arg1" "arg2" "arg3" "arg4"]
                  {:coerce {:bar []}
                   :args->opts (cons :foo (repeat :bar))})

)


;;;; parsing command-line arguments (along with options): `parse-args`

(comment

  (cli/parse-args ["-a" "1" "-b" "2" "foo" "bar"])

  (cli/parse-args ["ssh://foo" "--force"] {:coerce {:force :boolean}})

  ;; coercion information can be used to disambiguate between option value and
  ;; argument

  (cli/parse-args ["--force" "ssh://foo"] {:coerce {:force :boolean}})

  ;; or the explicit options/arguments separator

  (cli/parse-args ["--paths" "src" "test" "--" "ssh://foo"]
                  {:coerce {:paths []}})

)


;;;; requiring, restricting, validating and setting default option values

(comment

  (cli/parse-args ["--foo"] {:restrict [:bar]})
  (cli/parse-args ["--foo"] {:require [:bar]})

  (cli/parse-args ["--foo" "0"] {:validate {:foo pos?}})
  (cli/parse-args ["--foo" "0"]
                  {:validate {:foo {:pred   pos?
                                    :ex-msg (fn [m]
                                              (str "Not a positive number: "
                                                   (:value m)))}}})

  (cli/parse-args ["--foo" "0"] {:exec-args {:bar 1}})
  ;; actual arg overrides the default
  (cli/parse-args ["--foo" "0" "--bar" "42"] {:exec-args {:bar 1}})

)


;;;; handling errors

(comment

  (cli/parse-opts
   []
   {:spec {:foo {:desc    "You know what this is."
                 :require true
                 :ref     "<val>"}}
    :error-fn
    (fn [{:keys [spec type cause msg option] :as data}]
      (if (= :org.babashka/cli type)
        (case cause
          :require (println
                    (format "Missing required argument:\n%s"
                            (cli/format-opts
                             {:spec (select-keys spec [option])})))
          (println msg))
        (throw (ex-info msg data)))
      #_(System/exit 1))})

)


;;;; writing spec and formatting

(comment

  (def spec {:from   {:ref          "<format>"
                      :desc
                      "The input format. <format> can be edn, json or transit."
                      :coerce       :keyword
                      :alias        :i
                      :default-desc "edn"
                      :default      :edn}
             :to     {:ref          "<format>"
                      :desc
                      "The output format. <format> can be edn, json or transit."
                      :coerce       :keyword
                      :alias        :o
                      :default-desc "json"
                      :default      :json}
             :pretty {:desc         "Pretty-print output."
                      :alias        :p}
             :paths  {:desc         "Paths of files to transform."
                      :coerce       []
                      :default      ["src" "test"]
                      :default-desc "src test"}})

  ;; specifying order and selection of options formatted

  (println (cli/format-opts {:spec spec :order [:pretty :from :to :paths]}))


  (def spec-v [[:pretty {:desc "Pretty-print output."
                         :alias :p}]
               [:paths {:desc "Paths of files to transform."
                        :coerce []
                        :default ["src" "test"]
                        :default-desc "src test"}]])

  ;; without specifying order (formatting as is)

  (println (cli/format-opts {:spec spec-v}))

)


;;;; subcommands: `dispatch`

(comment

  (defn copy [m]
    (assoc m :fn-to-call :copy))

  (defn delete [m]
    (assoc m :fn-to-call :delete))

  (defn help [m]
    (assoc m :fn-to-call :help))

  (def table
    [{:cmds ["copy"]   :fn copy   :args->opts [:file]}
     {:cmds ["delete"] :fn delete :args->opts [:file]}
     {:cmds []         :fn help}])

  (cli/dispatch table ["copy" "file.txt"])

)
