# experiments

An area for exploration, learning and experiments with Clojure(Script) and
their ecosystem (frameworks, libs, ...), usually as `hello-<something>`
projects.

## Overview

>Even the most powerful wizard must consult grimoires as an aid against
>forgetfulness.
>
>--- source unknown
