# hello-promesa

Exploring Promesa --- a promise library & concurrency toolkit for
Clojure(Script).

- https://github.com/funcool/promesa
- https://funcool.github.io/promesa/latest/

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
