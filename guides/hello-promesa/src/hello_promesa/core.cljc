(ns hello-promesa.core
  (:require
   [clojure.core.async :as a]
   [promesa.core :as p]
   [promesa.exec :as exec]
   [promesa.exec.bulkhead :as pxb]
   [promesa.exec.csp :as sp])
  (:import (clojure.lang ExceptionInfo)
           #?(:clj (java.lang Thread))))


;; doc: https://funcool.github.io/promesa/latest/index.html


;; basics

(comment

  (->> (p/promise 1)
       (p/map inc)
       (deref))

  ;; are Java 19 Virtual Threads enabled?
  #?(:clj (when exec/virtual-threads-available?
            (let [vthread-builder (doto (Thread/ofVirtual)
                                    (.name "My Virtual Thread"))
                  task (fn []
                         (let [current-thread (Thread/currentThread)
                               thread-id (.threadId current-thread)
                               thread-name (.getName current-thread)]
                           (printf "virtual thread running: id %s, name '%s'\n"
                                   thread-id thread-name)))
                  vthread (.start vthread-builder task)]
              (printf "virtual thread started: %s\n" (.getName vthread))
              (.join vthread))))
)

;; working with promises

(comment

  ;; There are several different ways to create a promise instance. If you just
  ;; want to create a promise with a plain value, you can use the polymorphic
  ;; promise function

  ;; creates a promise from value
  (p/promise 1)

  ;; creates a rejected promise
  (p/promise (ex-info "error" {}))

  ;; it automatically coerces the provided value to the appropriate promise
  ;; instance: `rejected` when the provided value is an exception and `resolved`
  ;; in all other cases.

  ;; if you already know that the value is either resolved or rejected, you can
  ;; skip the coercion and use the resolved and rejected functions

  ;; create a resolved promise
  (p/resolved 1)

  ;; create a rejected promise
  (p/rejected (ex-info "error" {}))

  ;; another option is to create an empty promise using the deferred function
  ;; and provide the value asynchronously using `p/resolve!` and `p/reject!`
  #?(:clj (defn sleep
            [ms]
            (let [p (p/deferred)]
              (future
                (Thread/sleep ms)
                (p/resolve! p))
              p)))

  (def s (sleep 3000))

  ;; another option is using a factory function. If you are familiar with
  ;; JavaScript, this is a similar approach
  @(p/create (fn [resolve reject] (resolve 1)))

  ;; NOTE: the @ reader macro only works on JVM.

  ;; the factory will be executed synchronously (in the current thread) but if
  ;; you want to execute it asynchronously, you can provide an executor (JVM
  ;; only)
  @(p/create (fn [resolve reject] (resolve 1)) exec/default-executor)

  ;; another way to create a promise is using the `do` macro
  @(p/do
     (let [a (rand-int 10)
           b (rand-int 10)]
      (+ a b)))

  ;; the `do` macro works similarly to clojure’s `do` block, so you can provide
  ;; any expression, but only the last one will be returned. That expression can
  ;; be a plain value or another promise.

  ;; if an exception is raised inside the `do` block, it will return the
  ;; rejected promise instead of re-raising the exception on the stack.

  ;; if the do contains more than one expression, each expression will be
  ;; treated as a promise expression and will be executed sequentially, each
  ;; awaiting the resolution of the prior expression.

  ;; for example, this `do` macro
  @(p/do (+ 1 2)
         (+ 2 3)
         (+ 3 4))

  ;; is roughtly equivalent to `let` macro (explained below)
  @(p/let [_ (+ 1 2)
           _ (+ 2 3)]
     (+ 3 4))

  ;; finally, promesa exposes a `future` macro very similar to the
  ;; `clojure.core/future`
  #?(:clj @(p/future (do
                       (Thread/sleep 1000)
                       ::value)))


  ;; chaining computatons

  ;; this section explains the helpers and macros that promesa provides for
  ;; chain different (high-probably asynchonous) operations in a sequence of
  ;; operations.

  ;; `then`

  ;; the most common way to chain a transformation to a promise is using the
  ;; general purpose then function. Consists on applying the function
  @(-> (p/resolved 1)
       (p/then inc))

  ;; flatten result
  @(-> (p/resolved 1)
       (p/then (fn [x]
                 (p/resolved (inc x)))))

  ;; as you can observe in the example, then handles functions that return plain
  ;; values as well as promise instances (which will automatically be
  ;; flattened).

  ;; for performance sensitive code, consider using a more specific functions
  ;; like `map` or `mapcat`.

  ;; `chain`

  ;; if you have multiple transformations and you want to apply them in one
  ;; step, there are the `chain` and `chain'` functions
  @(-> (p/resolved 1)
       (p/chain inc inc inc))

  ;; NOTE: chain is analogous to `then` and `then'` but accept multiple
  ;; transformation functions.

  ;; `->`, `->>` and `as->` (macros)

  ;; NOTE: `->` and `->>` introduced in 6.1.431, `as->` introduced in 6.1.434.

  ;; these threading macros simplify chaining operation, removing the need of
  ;; using `then` all the time.

  ;; let's look an example using then and later see how it can be improved using
  ;; the `->` threading macro
  @(-> (p/resolved {:a 1 :c 3})
       (p/then #(assoc % :b 2))
       (p/then #(dissoc % :c)))

  ;; then, the same code can be simplified with
  @(p/-> (p/resolved {:a 1 :c 3})
         (assoc :b 2)
         (dissoc :c))

  ;; the threading macros hides all the accidental complexity of using promise
  ;; chaining.

  ;; The `->>` and `as->` are equivalent to the `clojure.core` macros, but they
  ;; work with promises in the same way as `->` example shows.

  ;; `handle`

  ;; if you want to handle rejected and resolved callbacks in one unique
  ;; callback, then you can use the `handle` chain function:
  @(-> (p/promise 1)
       (p/handle (fn [result error]
                   (if error :rejected :resolved))))

  ;; it works in the same way as `then`, if the function returns a promise
  ;; instance it will be automatically unwrapped.

  ;; See also: `hmap`, `hcat`.

  ;; `finally`

  ;; and finally if you want to attach a (potentially side-effectful) callback
  ;; to be always executed notwithstanding if the promise is rejected or
  ;; resolved
  @(-> (p/promise 1)
       (p/finally (fn [_ _]
                    (println "finally"))))

  ;; the return value of the function will be ignored and new promise instance
  ;; will be returned mirroring the original one.

  ;; `map`

  ;; returns a new promise instance which will be completed with the return
  ;; value of applying a function to the eventually successfully resolved
  ;; promise.
  @(->> (p/resolved 1)
        (p/map inc))

  ;; in contrast to `then`, there are no automatic unwrapping of nested
  ;; promises. Use `mapcat` for for handle unwrapping.

  ;; aliases: `fmap`.

  ;; `mapcat`

  ;; returns a new promise instance which will be completed with the same value
  ;; as the returning promise instance of applying a function to eventually
  ;; successfully resolved promise. The function must return a promise instance.
  @(->> (p/resolved 1)
        (p/mapcat (fn [v] (p/resolved (inc v)))))

  ;; aliases: `mcat`.

  ;; `hmap`

  ;; applies a function in the same way as map to both possible results: value
  ;; and exception. It returns a promise that will be completed with the return
  ;; value of the function.
  @(->> (p/resolved 1)
        (p/hmap (fn [v _] (inc v))))

  @(->> (p/rejected (ex-info "oops" {}))
        (p/hmap (fn [_ e] (ex-message e))))

  ;; see also: `handle`

  ;; `hcat`

  ;; applies a function in the same way as `mapcat` to both possible results:
  ;; value and exception. Function must return a promise. It returns a mirrored
  ;; promise returned by the applied function.
  @(->> (p/resolved 1)
        (p/hcat (fn [v _] (p/resolved (inc v)))))

  @(->> (p/rejected (ex-info "oops" {}))
        (p/hcat (fn [_ e] (p/resolved (ex-message e)))))


  ;; composition

  ;; `let`

  ;; the promesa library comes with convenient syntactic-sugar that allows you
  ;; to create a composition that looks like synchronous code while using the
  ;; Clojure’s familiar `let` syntax
  @(p/let [x (p/delay 1000 42)
           y (p/delay 1200 41)
           z 2]
     (+ x y z))

  ;; the `let` macro behaves identically to Clojure’s `let` with the exception
  ;; that it always returns a promise. If an error occurs at any step, the
  ;; entire composition will be short-circuited, returning exceptionally
  ;; resolved promise.

  ;; `all`

  ;; in some circumstances you will want wait for completion of several promises
  ;; at the same time. To help with that, promesa also provides the `all`
  ;; helper.
  @(let [p (p/all [(p/delay 1000 1)
                   (p/delay 1200 2)])]
     (p/then p (fn [[result1 result2]]
                 (+ result1 result2))))

  ;; it is up to the user properly handle concurrency, `p/all` does not launch
  ;; additional threads of execution.

  ;; `plet` macro

  ;; the `plet` macro combines syntax of `let` with `all`; and enables a simple
  ;; declaration of parallel operations followed by a body expression that will
  ;; be executed when all parallel operations have successfully resolved.
  @(p/plet [a (p/delay 100 1)
            b (p/delay 200 2)
            c (p/delay 120 3)]
           (+ a b c))

  ;; the plet macro is just a syntactic sugar on top of `all`. The previous
  ;; example can be written using `all` in this manner
  @(->(p/all [(p/delay 100 1)
              (p/delay 200 2)
              (p/delay 120 3)])
      (p/then (fn [[a b c]] (+ a b c))))

  ;; the real parallelism strictly depends on the underlying implementation of
  ;; the executed functions. If they do syncronous work, all the code will be
  ;; executed serially, almost identical to the standard `let`. It is the user
  ;; responsability to choose the final execution model.

  ;; `any`

  ;; there are also circumstances where you only want the first successfully
  ;; resolved promise. For this case, you can use the `any` combinator
  @(p/any [(p/delay 125 1)
           (p/delay 200 2)
           (p/delay 120 3)])


  ;; `race`

  ;; the `race` function method returns a promise that fulfills or rejects as
  ;; soon as one of the promises in an iterable fulfills or rejects, with the
  ;; value or reason from that promise
  @(p/race [(p/delay 100 1)
            (p/delay 110 2)])

  @(p/race [(p/rejected (ex-info "oops" {}))
            (p/delay 100 1)
            (p/delay 110 2)])


  ;; error handling

  ;; one of the advantages of using the promise abstraction is that it natively
  ;; has a notion of errors, so you don’t need to reinvent it. If some
  ;; computation inside the composed promise chain/pipeline raises an exception,
  ;; the pipeline short-circuits and propagates the exception to the last
  ;; promise in the chain.

  ;; `catch`

  ;; the `catch` function adds a new handler to the promise chain that will be
  ;; called when any of the previous promises in the chain are rejected or an
  ;; exception is raised. The catch function also returns a promise that will be
  ;; resolved or rejected depending on what happens inside the catch handler.
  @(-> (p/rejected (ex-info "oops" {}))
       (p/catch (fn [error]
                  (prn "error:" (ex-message error)))))

  ;; you also can filter by predicate or by class the possible exception to
  ;; handle
  @(-> (p/rejected (ex-info "oops" {}))
       (p/catch ExceptionInfo
           (fn [error]
             (prn "error:" (ex-message error)))))

  (defn ex-info?
    [o]
    (instance? ExceptionInfo o))

  (-> (p/rejected (ex-info "oops" {}))
      (p/catch ex-info? (fn [error]
                          (prn "error:" (ex-message error)))))

  ;; `merr`

  ;; in the same way as `catch` allow apply a function to the promise rejection,
  ;; this function has the parameters in inverse order, intended to be used with
  ;; `->>` in the same way as `map` and `mapcat`. The function must return a
  ;; promise instance.
  @(->> (p/rejected (ex-info "oops" {}))
        (p/merr (fn [error]
                  (p/resolved (ex-message error)))))


  ;; delays and timeouts.

  ;; javascript, due to its single-threaded nature, does not allow you to block
  ;; or sleep. But, with promises you can emulate that functionality using
  ;; `delay` like so
  @(-> (p/delay 1000 "foobar")
       (p/then (fn [v]
                 (println "Received:" v) v)))

  ;; after 1 second it will print the message to the console: "Received: foobar"

  ;; the promise library also offers the ability to add a timeout to async
  ;; operations thanks to the `timeout` function
  @(-> (p/delay 1000 "foobar")
       (p/timeout 200)
       (p/then #(println "Task finished" %))
       (p/catch (fn [e]
                  (let [ex-msg (ex-message e)]
                    (println "error: " ex-msg)
                    ex-msg))))

  ;; in this example, if the async task takes more that 200ms then the promise
  ;; will be rejected with a timeout error and then successfully captured with
  ;; the catch handler.


  ;; promise chaining & execution model

  ;; let’s try to understand how promise chained functions are executed and how
  ;; they interact with platform threads. This section is mainly affects the
  ;; JVM.

  ;; let's consider this example
  @(->> (p/delay 100 1)
        (p/map inc)
        (p/map inc))

  ;; this will create a promise that will resolve to 1 in 100ms (in a separate
  ;; thread); then the first `inc` will be executed (in the same thread), and
  ;; then another `inc` is executed (in the same thread). In total only one
  ;; thread is involved.

  ;; this default execution model is usually preferrable because it don’t abuse
  ;; the task scheduling and leverages function inlining on the JVM.

  ;; but it does have drawbacks: this approach will block the thread until all
  ;; of the chained callbacks are executed. For small chains this is not a
  ;; problem. However, if your chain has a lot of functions and requires a lot
  ;; of computation time, this might cause unexpected latency. It may block
  ;; other threads in the thread pool from doing other, maybe more important,
  ;; tasks.

  ;; For such cases, promesa exposes an additional arity for provide a
  ;; user-defined executor to control where the chained callbacks are executed
  @(->> (p/delay 100 1)
        (p/map :default inc)
        (p/map :default inc))

  ;; this will schedule a separate task for each chained callback, making the
  ;; whole system more responsive because you are no longer executing big
  ;; blocking functions; instead you are executing many small tasks.

  ;; The `:default` keyword will resolve to `exec/*default-executor*`, that is a
  ;; `ForkJoinPool` instance that is highly optimized for lots of small tasks.

  ;; on JDK19 with Preview enabled you will also have the
  ;; `exec/*vthread-executor*` (`:vthread` keyword can be used) that is an
  ;; instance of Virtual Thread per task executor.

  ;; performance overhead

  ;; promesa is a lightweight abstraction built on top of native
  ;; facilities (`CompletableFuture` in the JVM and `js/Promise` in
  ;; JavaScript). Internally we make heavy use of protocols in order to expose a
  ;; polymorphic and user friendly API, and this has little overhead on top of
  ;; raw usage of `CompletableFuture` or `js/Promise`.

  ;; for performance sensitive code, prefer using functions designed to be used
  ;; for `->>`; they are more optimized because they don’t perform automatic
  ;; unwrapping handling (unlike the then or handle functions). This applies
  ;; only to CLJ, on CLJS all they work the same way because of how the
  ;; underlying implementation works.
)


;; scheduling & executors

(comment

  ;; introduction

  ;; additionally to the promise abstraction, promesa library comes with many
  ;; helpers and factories for execution and scheduling of tasks for
  ;; asynchronous execution.

  ;; although this API works in the JS runtime and some of the function has
  ;; general utility, the main target is the JVM platform.

  ;; async tasks

  ;; firstly, let's define async task: a function that is executed out of
  ;; current flow using a different thread. Here, promesa library exposes mainly
  ;; two functions

  ;; `promesa.exec/run!`: useful when you want to run a function in a different
  ;; thread and you don’t care about the return value; it returns a promise that
  ;; will be fullfilled when the callback terminates.

  @(exec/run! (fn []
                (prn "I'm running in different thread")
                1))

  ;; `promesa.exec/submit!`: useful when you want to run a function in a
  ;; different thread and you need the return value; it returns a promise that
  ;; will be fullfilled with the return value of the function.

  @(exec/submit! (fn []
                   (prn "I'm running in different thread")
                   1))

  ;; the both functions optionally accepts as first argument an executor
  ;; instance that allows specify the executor where you want to execute the
  ;; specified function. If no executor is provided, the default one is
  ;; used (bound to the `promesa.exec/*default-executor*` dynamic var).

  ;; also, in both cases, the returned promise is cancellable, so if for some
  ;; reason the function is still not executed, the cancellation will prevent
  ;; the execution. You can cancel a cancellable promise with `p/cancel!`
  ;; function.


  ;; delayed tasks

  ;; this consists in a simple helper that allows scheduling execution of a
  ;; function after some amount of time.

  @(exec/schedule! 1000 (fn []
                          (println "hello world")))

  ;; this example shows you how you can schedule a function call to be executed
  ;; 1 second in the future. It works the same way for both Clojure and
  ;; ClojureScript.

  ;; the tasks can be cancelled using its return value

  (def task (exec/schedule! 5000 #(+ 1 2)))
  (p/cancel! task)

  ;; the execution model depends on the platform: on the JVM a default scheduler
  ;; executor is used and the the scheduled function will be executed in
  ;; different thread; on JS runtime the function will be executed in a
  ;; microtask.


  ;; executors factories

  ;; a collection of factories function for create executors instances (JVM
  ;; only)

  ;; `exec/cached-executor`: creates a thread pool that creates new threads as
  ;; needed, but will reuse previously constructed threads when they are
  ;; available.

  ;; `exec/fixed-executor`: creates a thread pool that reuses a fixed number of
  ;; threads operating off a shared unbounded queue.

  ;; `exec/single-executor`: creates an Executor that uses a single worker
  ;; thread operating off an unbounded queue

  ;; `exec/scheduled-executor`: creates a thread pool that can schedule commands
  ;; to run after a given delay, or to execute periodically.

  ;; `exec/forkjoin-executor`: creates a thread pool that maintains enough
  ;; threads to support the given parallelism level, and may use multiple queues
  ;; to reduce contention.

  ;; Since v9.0.x there are new factories that uses the JDK>=19 preview API

  ;; `exec/thread-per-task-executor`: creates an Executor that starts a new
  ;; Thread for each task.

  ;; `exec/vthread-per-task-executor`: creates an Executor that starts a new
  ;; virtual Thread for each task.


  ;; helpers

  ;; `pmap` (experimental)

  ;; this is a simplified `clojure.core/pmap` analogous function that allows
  ;; execute a potentially computationally expensive or io bound functions in
  ;; parallel.

  ;; it returns a lazy chunked seq (uses the clojure’s default chunk size: 32)
  ;; and the maximum parallelism is determined by the provided executor.
  #?(:clj (defn slow-inc
            [x]
            (Thread/sleep 1000)
            (inc x)))

  (time
   (doall
    (exec/pmap slow-inc (range 10))))

  (time
   (doall
    (map slow-inc (range 10))))

  ;; `with-executor` macro (experimental)

  ;; this allows run a scoped code with the `exec/*default-executor*` bound to
  ;; the provided executor. The provided executor can be a function for lazy
  ;; executor instantiation.

  ;; It optionally accepts metadata on the executor part toindicate
  ;; `^:shutdown`: shutdown the pool before return
  ;; `^:interrupt`: shutdown and interrupt before return

  (time
   (exec/with-executor ^:shutdown (exec/fixed-executor :parallelism 2)
     (doall (exec/pmap slow-inc (range 10)))))
)


;; channels (CSP pattern)

(comment

  ;; an implementation of channel abstraction and CSP patterns for Clojure; is a
  ;; `core.async` alternative implementation that leverages JDK19 Virtual
  ;; Threads.

  ;; you can read the `core.async` rationale for better understanding of the
  ;; main ideas of the CSP pattern.

  ;; the promesa channel and csp patterns implementation do not intend to be a
  ;; replacement for `core.async`, and there are cases where `core.async` is
  ;; preferable; the main usage target for promesa channels and csp patterns
  ;; implementation is for JVM based backends with JDK>=19.

  ;; NOTE: Although the main focus is the use in JVM, where is all the
  ;; potential; the channel implementation and all internal buffers are
  ;; implemented in CLJC. This means that, if there is interest, we can think
  ;; about exposing channels API using promises. In any case, the usefulness of
  ;; channel implementation in CLJS remains to be seen.


  ;; differences with `core.async`

  ;; 1. there are no macro transformations, the `go` macro is a convenient alias
  ;; for `p/vthread` (or `p/thread` when vthreads are not available); there are
  ;; no limitation on using blocking calls inside go macro neither many other
  ;; inconveniences of core.async go macro, mainly thanks to the JDK19 with
  ;; preview enabled Virtual Threads.

  ;; 2. no callbacks, functions returns promises or blocks; you can use the
  ;; promise composition API or thread blocking API, whatever you wants.

  ;; 3. no take/put limits; you can attach more than 1024 pending tasks to a
  ;; channel.

  ;; 4. simpler mental model, there are no differences between parking and
  ;; blocking operations.

  ;; 5. analogous performance; in my own stress tests it has the same
  ;; performance as `core.async`.

  ;; there are also some internal differences that you should know

  ;; the promesa implementation cancels immediately all pending puts when
  ;; channel is closed in contrast to `core.async` that leaves them operative
  ;; until all puts are succeded.

  ;; the promesa implementation takes a bit less granular locking than
  ;; `core.async`, but in the end it should not have any effect on the final
  ;; performance or usability.

  ;; the `promesa.exec.csp/pipe` helper does not uses go-blocks, so it can be
  ;; safely used with no-vthreads support because it will not create additional
  ;; platform threads.


  ;; getting started

  ;; this documentation supposes you have some knowledge of `core.async` API.

  ;; working with channels API

  ;; create a channel

  (def ch (sp/chan 2))

  ;; perform a blocking put operation using a blocking operation

  (sp/put! ch :a)

  ;; or perform a blocking put operation using `put` function that returns a
  ;; promise/future-like object (`CompletableFuture`)

  @(sp/put ch :b)

  ;; retrieve data from the channel

  ;; using a blocking helper, analogous to `clojure.core.async/<!!`

  (sp/take! ch)

  ;; or blocking on promise

  @(sp/take ch)

  ;; you also can `take` with timeout

  @(sp/take ch 1000 :no-value-in-channel)

  ;; for convenience and `core.async` familiarity, there are also `<!` and `>!`
  ;; functions that have the same api as their counterpart `take!` and `put!`


  ;; the `go` blocks

  ;; in contrast to `core.async`, the promesa go blocks are just virtual
  ;; threads (or standard threads if the vthreads are not available) so there
  ;; are no macro limitations nor blocking/parking differences.

  ;; the promesa go blocks returns promises (`CompletableFuture`’s) instead of
  ;; channels. This is because the code on go block can fail and channels are
  ;; bad abstraction for represent a computation result that can fail.

  @(sp/go
     (sp/<! ch 1000 :timeout))

  ;; But if you need a channel, there are `go-chan` macro. The go + loop macro
  ;; is also available as `go-loop`.


  ;; multiple operations

  ;; if you want perform multiple operations on the same or mutliple
  ;; channels. In the same line as `clojure.core.async/alts!!`, this library
  ;; exposes the `promesa.exec.csp/alts!` macro that has the same API

  (let [c1 (sp/chan)
        c2 (sp/chan)]
    (sp/go-loop []
      (let [[v ch] (sp/alts! [c1 c2])]
        (when v
          (println "Read" v "from" ch)
          (recur))))

    @(sp/go
       (sp/>! c1 "hi")
       (sp/>! c2 "there")
       (sp/close! c1)
       (sp/close! c2)))

  ;; for completeness, there are also `alts` function, that returns a
  ;; `CompletableFuture` instead of blocking the current thread.


  ;; channel multiplexing

  ;; there are some situations when you want multiple readers on the same data
  ;; or implement some kind of pub/sub. For this reason we have the multiplexed
  ;; channel constructors: `mult` and `mult*`.

  (def mx (sp/mult))

  (a/go
    (let [ch (sp/chan)]
      (sp/tap! mx ch)
      (println "go 1:" (sp/<! ch))
      (sp/close! ch)))

  (a/go
    (let [ch (sp/chan)]
      (sp/tap! mx ch)
      (println "go 2:" (sp/<! ch))
      (sp/close! ch)))

  (sp/>! mx :a)

  ;; the `mult` constructor returns a muliplexer, and as it implements the
  ;; channel API, you can put values in directly. For the cases when you already
  ;; have a channel that you want multiplexed, just use the `mult*`.

  ;; the `mult*` works in the same way as `clojure.core.async/mult`. There are
  ;; also `untap!` function for removing the channel from the multiplexer.

  ;; closed channels are automatically removed from the multiplexer.

)


;; bulkhead (concurrency limiter)

(comment

  ;; in general, the goal of the bulkhead pattern is to avoid faults in one part
  ;; of a system to take the entire system down. The bulkhead implementation in
  ;; promesa limits the number of concurrent calls.

  ;; This SO answer explains the concept very well:
  ;; https://stackoverflow.com/questions/30391809/what-is-bulkhead-pattern-used-by-hystrix

  ;; all parameters are optional and have default value

  (def instance (pxb/create :concurrency 1
                            :queue-size 16
                            :executor exec/*default-executor*))

  #?(:clj @(exec/submit! instance (fn []
                                    (Thread/sleep 1000)
                                    1)))

  ;; at first glance, this seems like an executor instance because it resembles
  ;; the same API (`exec/submit!` call).

  ;; when you submits a task to it, it does the following:

  ;; 1. checks if concurrency limit is not reached, if not, proceed to execute
  ;; the function in the underlying executor.

  ;; 2. if concurrency limit is reached, it queues the execution until other
  ;; tasks are finished.

  ;; 3. if queue limit is reached, the returned promise will be automatically
  ;; rejected with an exception indicating that queue limit reached.

  ;; this allows control the concurrency and the queue size on access to some
  ;; resource.

  ;; NOTES:

  ;; As future improvements we consider adding an option for delimit the max
  ;; wait and cancel/reject tasks after some timeout.

  ;; For now it is implemented only on JVM but I think is pretty easy to
  ;; implement on CLJS, so if there are some interest on it, feel free to open
  ;; and issue for just show interest or discuss how it can be contributed.

)
