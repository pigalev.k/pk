;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; add the development project config (paths etc) and test paths at default REPL
((clojure-mode . ((cider-clojure-cli-global-options . "-A:dev:test:java-preview-features"))))
