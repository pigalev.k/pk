# hello-malli

Exploring Malli --- high-performance Data-Driven Data Specification Library for
Clojure/Script.

- https://github.com/metosin/malli

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
