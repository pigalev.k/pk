(ns hello-malli.core
  (:require
   [clojure.pprint :refer [pprint]]
   [clojure.string :as string]
   [clojure.test.check.generators :as gen]
   [criterium.core :as cc]
   [jsonista.core :as json]
   [malli.clj-kondo :as mc]
   [malli.core :as m]
   [malli.destructure :as md]
   [malli.dev :as dev]
   [malli.dev.pretty :as pretty]
   [malli.edn :as edn]
   [malli.error :as me]
   [malli.experimental :as mx]
   [malli.generator :as mg]
   [malli.instrument :as mi]
   [malli.json-schema :as json-schema]
   [malli.provider :as mp]
   [malli.registry :as mr]
   [malli.swagger :as swagger]
   [malli.transform :as mt]
   [malli.util :as mu]))


;; malli supports Vector (default), Map and Lite syntaxes

(comment

;;; schema basics

  ;; just a type (String)
  (def string :string)

  ;; type with properties
  (def short-string [:string {:min 1, :max 10}])

  ;; type with properties and children
  (def location-tuple [:tuple {:title "location"} :double :double])

  ;; a function schema of `:int` -> `:int`
  (def int-int-function [:=> [:cat :int] :int])

  (def Address
    [:map
     [:id string?]
     [:tags [:set keyword?]]
     [:address
      [:map
       [:street string?]
       [:city string?]
       [:zip int?]
       [:lonlat [:tuple double? double?]]]]])

  ;; schemas can have properties
  (def Age
    [:and
     {:title "Age"
      :description "It's an age"
      :json-schema/example 20}
     :int [:> 18]])
  (m/properties Age)


  ;; schema usage

  (def non-empty-string
    (m/schema [:string {:min 1}]))
  (m/schema? non-empty-string)
  (m/validate non-empty-string "")
  (m/validate non-empty-string "kikka")
  (m/form non-empty-string)

  ;; validation
  (m/validate [:and :int [:> 6]] 7)
  (m/validate [:qualified-keyword {:namespace :aaa}] :aaa/bbb)

  ;; optimized (pure) validation function for best performance
  (def valid?
    (m/validator
     [:map
      [:x :boolean]
      [:y {:optional true} :int]
      [:z :string]]))

  (valid? {:x true, :z "kikka"})


  ;; schema definitions

  ;; maps

  ;; maps are open by default
  (m/validate
   [:map [:x :int]]
   {:x 1, :extra "key"})

  ;; but can be closed with `:closed` property
  (m/validate
   [:map {:closed true} [:x :int]]
   {:x 1, :extra "key"})

  ;; map keys are not limited to keywords
  (m/validate
   [:map
    ["status" [:enum "ok"]]
    [1 :any]
    [nil :any]
    [::a :string]]
   {"status" "ok"
    1 'number
    nil :yay
    ::a "properly awesome"})

  ;; most core-predicates are mapped to Schemas
  (m/validate string? "kikka")

  ;; using registry references; references must be either qualified keywords or
  ;; strings
  (m/validate
   [:map {:registry {::id int?
                     ::country string?}}
    ::id
    [:name string?]
    [::country {:optional true}]]
   {::id 1
    :name "kikka"})

  ;; map as a homogeneous index: all key-value pairs have the same type
  (m/validate
   [:map-of :string [:map [:lat number?] [:long number?]]]
   {"oslo" {:lat 60 :long 11}
    "helsinki" {:lat 60 :long 24}})


  ;; sequences

  ;; homogeneous: `:sequential`, `:vector`, `:set`
  (m/validate [:sequential any?] (list "this" 'is :number 42))
  (m/validate [:vector int?] [1 2 3])
  (m/validate [:vector int?] (list 1 2 3))
  (m/validate [:set int?] #{1 2 3})

  ;; heterogeneous, fixed length: `:tuple`
  (m/validate [:tuple keyword? string? number?] [:bing "bang" 42])


  ;; sequence regexes:

  ;; concatenation: `:cat`, `:catn`
  (m/validate [:cat string? int?] ["foo" 0])
  (m/validate [:catn [:s string?] [:n int?]] ["foo" 0])

  ;; alternatives: `:alt`, `:altn`
  (m/validate [:alt keyword? string?] ["foo"])
  (m/validate [:altn [:kw keyword?] [:s string?]] ["foo"])

  ;; repetition: `:?`, `:*`, `:+`, `:repeat`
  (m/validate [:? int?] [])
  (m/validate [:? int?] [1])
  (m/validate [:? int?] [1 2])

  (m/validate [:* int?] [])
  (m/validate [:* int?] [1 2 3])

  (m/validate [:+ int?] [])
  (m/validate [:+ int?] [1])
  (m/validate [:+ int?] [1 2 3])

  (m/validate [:repeat {:min 2, :max 4} int?] [1])
  (m/validate [:repeat {:min 2, :max 4} int?] [1 2])
  (m/validate [:repeat {:min 2, :max 4} int?] [1 2 3 4])
  (m/validate [:repeat {:min 2, :max 4} int?] [1 2 3 4 5])

  ;; naming of subsequences/alternatives (`:catn`, `:altn`)
  (m/explain
   [:* [:catn [:prop string?] [:val [:altn [:s string?] [:b boolean?]]]]]
   ["-server" "foo" "-verbose" 11 "-user" "joe"])
  (m/explain
   [:* [:cat string? [:alt string? boolean?]]]
   ["-server" "foo" "-verbose" 11 "-user" "joe"])

  ;; seqex operators take any non-seqex child schema to mean a sequence of one
  ;; element that matches that schema; to force that behaviour for a seqex child
  ;; `:schema` can be used

  (m/validate
   [:cat [:= :names] [:schema [:* string?]] [:= :nums] [:schema [:* number?]]]
   [:names ["a" "b"] :nums [1 2 3]])

  (m/validate
   [:cat [:= :names] [:* string?] [:= :nums] [:* number?]]
   [:names "a" "b" :nums 1 2 3])

  ;; although a lot of effort has gone into making the seqex implementation fast,
  ;; it is always better to use less general tools whenever possible
  (let [valid? (m/validator [:* int?])]
    (cc/quick-bench (valid? (range 10))))

  (let [valid? (m/validator [:sequential int?])]
    (cc/quick-bench (valid? (range 10))))


  ;; string schemas

  ;; using a predicate
  (m/validate string? "kikka")
  ;; using `:string` Schema
  (m/validate :string "kikka")
  (m/validate [:string {:min 1, :max 4}] "")
  ;; using regular expressions
  (m/validate #"a+b+c+" "abbccc")
  ;; `:re` with string
  (m/validate [:re ".{3,5}"] "abc")
  ;; `:re` with regex
  (m/validate [:re #".{3,5}"] "abc")
  ;; NB: `re-find` semantics
  (m/validate [:re #"\d{4}"] "1234567")
  ;; anchor with ^...$ if you want to strictly match the whole string
  (m/validate [:re #"^\d{4}$"] "1234567")


  ;; maybe schemas

  ;; use `:maybe` to express that an element should match some schema OR be nil
  (m/validate [:maybe string?] "bingo")
  (m/validate [:maybe string?] nil)
  (m/validate [:maybe string?] :bingo)


  ;; fn schemas

  ;; `:fn` allows any predicate function to be used:
  (def my-schema
    [:and
     [:map
      [:x int?]
      [:y int?]]
     [:fn (fn [{:keys [x y]}] (> x y))]])

  (m/validate my-schema {:x 1, :y 0})
  (m/validate my-schema {:x 1, :y 2})


  ;; error messages

  ;; detailed errors with `m/explain`

  (m/explain
   Address
   {:id "Lillan"
    :tags #{:artesan :coffee :hotel}
    :address {:street "Ahlmanintie 29"
              :city "Tampere"
              :zip 33100
              :lonlat [61.4858322, 23.7854658]}})

  (pprint (m/explain
           Address
           {:id "Lillan"
            :tags #{:artesan "coffee" :garden}
            :address {:street "Ahlmanintie 29"
                      :zip 33100
                      :lonlat [61.4858322, nil]}}))

  (pprint (mu/explain-data
           Address
           {:id "Lillan"
            :tags #{:artesan "coffee" :garden}
            :address {:street "Ahlmanintie 29"
                      :zip 33100
                      :lonlat [61.4858322, nil]}}))


  ;; humanized error messages

  ;; explain results can be humanized with `malli.error/humanize`
  (-> Address
      (m/explain
       {:id "Lillan"
        :tags #{:artesan "coffee" :garden}
        :address {:street "Ahlmanintie 29"
                  :zip 33100
                  :lonlat [61.4858322, nil]}})
      (me/humanize))


  ;; custom error messages

  ;; error messages can be customized with `:error/message` and `:error/fn`
  ;; properties
  (-> [:map
       [:id int?]
       [:size [:enum {:error/message "should be: S|M|L"}
               "S" "M" "L"]]
       [:age [:fn {:error/fn (fn [{:keys [value]} _]
                               (str value ", should be > 18"))}
              (fn [x] (and (int? x) (> x 18)))]]]
      (m/explain {:size "XL", :age 10})
      (me/humanize
       {:errors (-> me/default-errors
                    (assoc ::m/missing-key
                           {:error/fn (fn [{:keys [in]} _]
                                        (str "missing key " (last in)))}))}))

  ;; messages can be localized
  (-> [:map
       [:id int?]
       [:size [:enum {:error/message {:en "should be: S|M|L"
                                      :fi "pitäisi olla: S|M|L"}}
               "S" "M" "L"]]
       [:age [:fn {:error/fn
                   {:en (fn [{:keys [value]} _]
                          (str value ", should be > 18"))
                    :fi (fn [{:keys [value]} _]
                          (str value ", pitäisi olla > 18"))}}
              (fn [x] (and (int? x) (> x 18)))]]]
      (m/explain {:size "XL", :age 10})
      (me/humanize
       {:locale :fi
        :errors (-> me/default-errors
                    (assoc-in ['int? :error-message :fi] "pitäisi olla numero")
                    (assoc ::m/missing-key
                           {:error/fn
                            {:en (fn [{:keys [in]} _]
                                   (str "missing key " (last in)))
                             :fi (fn [{:keys [in]} _]
                                   (str "puuttuu avain " (last in)))}}))}))

  ;; top-level humanized map-errors are under `:malli/error`
  (-> [:and [:map
             [:password string?]
             [:password2 string?]]
       [:fn {:error/message "passwords don't match"}
        (fn [{:keys [password password2]}]
          (= password password2))]]
      (m/explain {:password "secret"
                  :password2 "faarao"})
      (me/humanize))

  ;; errors can be targeted using `:error/path` property
  (-> [:and [:map
             [:password string?]
             [:password2 string?]]
       [:fn {:error/message "passwords don't match"
             :error/path [:password2]}
        (fn [{:keys [password password2]}]
          (= password password2))]]
      (m/explain {:password "secret"
                  :password2 "faarao"})
      (me/humanize))

  ;; by default, only direct erroneous schema properties are used
  (-> [:map
       [:foo {:error/message "entry-failure"} :int]]
      (m/explain {:foo "1"})
      (me/humanize))

  ;; looking up humanized errors from parent schemas with custom :resolve
  (-> [:map
       [:foo {:error/message "entry-failure"} :int]]
      (m/explain {:foo "1"})
      (me/humanize {:resolve me/-resolve-root-error}))


  ;; spell-checking

  ;; for closed schemas, key spelling can be checked with
  (-> [:map [:address [:map [:street string?]]]]
      (mu/closed-schema)
      (m/explain
       {:name "Lie-mi"
        :address {:streetz "Hämeenkatu 14"}})
      (me/with-spell-checking)
      (me/humanize))


  ;; values in error

  ;; just to get parts of the value that are in error
  (-> Address
      (m/explain
       {:id "Lillan"
        :tags #{:artesan "coffee" :garden "ground"}
        :address {:street "Ahlmanintie 29"
                  :zip 33100
                  :lonlat [61.4858322, "23.7832851,17"]}})
      (me/error-value))

  ;; masking irrelevant parts
  (-> Address
      (m/explain
       {:id "Lillan"
        :tags #{:artesan "coffee" :garden "ground"}
        :address {:street "Ahlmanintie 29"
                  :zip 33100
                  :lonlat [61.4858322, "23.7832851,17"]}})
      (me/error-value {::me/mask-valid-values '...}))


  ;; pretty errors

  ;; for pretty development-time error printing, try `malli.dev.pretty/explain`

  (-> Address
      (pretty/explain
       {:id "Lillan"
        :tags #{:artesan "coffee" :garden "ground"}
        :address {:street "Ahlmanintie 29"
                  :zip 33100
                  :lonlat [61.4858322, "23.7832851,17"]}})
      (me/error-value))


  ;; value transformation

  ;; two-way schema-driven value transformations with `m/decode` and `m/encode`
  ;; using a `Transformer` instance

  ;; simple usage
  (m/decode int? "42" mt/string-transformer)
  (m/encode int? 42 mt/string-transformer)

  ;; for performance, precompute the transformations with `m/decoder` and
  ;; `m/encoder`
  (def decode (m/decoder int? mt/string-transformer))
  (decode "42")

  (def encode (m/encoder int? mt/string-transformer))
  (encode 42)


  ;; coercion

  ;; for both decoding + validating the results (throwing exception on error),
  ;; there is `m/coerce` and `m/coercer`
  (m/coerce :int "42" mt/string-transformer)
  ((m/coercer :int mt/string-transformer) "42")
  (m/coerce :int "invalid" mt/string-transformer)

  ;; coercion can be applied without transformer, doing just validation
  (m/coerce :int 42)
  (m/coerce :int "42")

  ;; exception-free coercion with continuation-passing style
  (m/coerce :int "fail" nil (partial prn "success:") (partial prn "error:"))


  ;; advanced transformations

  ;; transformations are recursive
  (m/decode
   Address
   {:id "Lillan",
    :tags ["coffee" "artesan" "garden"],
    :address {:street "Ahlmanintie 29"
              :city "Tampere"
              :zip 33100
              :lonlat [61.4858322 23.7854658]}}
   mt/json-transformer)

  ;; transform map keys
  (m/encode
   Address
   {:id "Lillan",
    :tags ["coffee" "artesan" "garden"],
    :address {:street "Ahlmanintie 29"
              :city "Tampere"
              :zip 33100
              :lonlat [61.4858322 23.7854658]}}
   (mt/key-transformer {:encode name}))

  ;; transforming homogenous `:enum` or `:=s` (supports automatic type detection
  ;; of `:keyword`, `:symbol`, `:int` and `:double`)
  (m/decode [:enum :kikka :kukka] "kukka" mt/string-transformer)

  ;; transformers can be composed with `mt/transformer`
  (def strict-json-transformer
    (mt/transformer
     mt/strip-extra-keys-transformer
     mt/json-transformer))

  (m/decode
   Address
   {:id "Lillan",
    :EVIL "LYN"
    :tags ["coffee" "artesan" "garden"],
    :address {:street "Ahlmanintie 29"
              :DARK "ORKO"
              :city "Tampere"
              :zip 33100
              :lonlat [61.4858322 23.7854658]}}
   strict-json-transformer)

  ;; schema properties can be used to override default transformations
  (m/decode
   [string? {:decode/string string/upper-case}]
   "kerran" mt/string-transformer)
  (m/decode
   [string? {:decode {:string clojure.string/upper-case}}]
   "kerran" mt/string-transformer)

  ;; decoders and encoders as interceptors (with `:enter` and `:leave` stages)
  (m/decode
   [string? {:decode/string {:enter clojure.string/upper-case}}]
   "kerran" mt/string-transformer)

  (m/decode
   [string? {:decode/string {:enter #(str "olipa_" %)
                             :leave #(str % "_avaruus")}}]
   "kerran" mt/string-transformer)

  ;; to access Schema (and options) use `:compile`
  (m/decode
   [int? {:math/multiplier 10
          :decode/math
          {:compile (fn [schema _]
                      (let [multiplier (:math/multiplier (m/properties schema))]
                        (fn [x] (* x multiplier))))}}]
   12
   (mt/transformer {:name :math}))

  ;; going crazy
  (m/decode
   [:map
    {:decode/math {:enter #(update % :x inc)
                   :leave #(update % :x (partial * 2))}}
    [:x [int? {:decode/math {:enter (partial + 2)
                             :leave (partial * 3)}}]]]
   {:x 1}
   (mt/transformer {:name :math}))


  ;; to and from JSON

  ;; the `m/encode` and `m/decode` functions work on clojure data. To go from
  ;; clojure data to JSON, you need a JSON library like `jsonista`. Additionally,
  ;; since `m/decode` doesn't check the schema, you need to run `m/validate` (or
  ;; `m/explain`) if you want to make sure your data conforms to your schema.

  ;; to JSON
  (def Tags
    (m/schema [:map
               {:closed true}
               [:tags [:set :keyword]]]))

  (json/write-value-as-string
   (m/encode Tags
             {:tags #{:bar :quux}}
             mt/json-transformer))

  ;; from JSON without validation
  (m/decode Tags
            (jsonista.core/read-value "{\"tags\":[\"bar\",[\"quux\"]]}"
                                      jsonista.core/keyword-keys-object-mapper)
            mt/json-transformer)

  ;; from JSON with validation
  (m/explain
   Tags
   (m/decode Tags
             (jsonista.core/read-value "{\"tags\":[\"bar\",[\"quux\"]]}"
                                       jsonista.core/keyword-keys-object-mapper)
             mt/json-transformer))

  (m/validate
   Tags
   (m/decode
    Tags
    (jsonista.core/read-value "{\"tags\":[\"bar\",\"quux\"]}"
                              jsonista.core/keyword-keys-object-mapper)
    mt/json-transformer))

  ;; for performance, it's best to prebuild the validator, decoder and explainer

  (def validate-Tags (m/validator Tags))
  (def decode-Tags (m/decoder Tags mt/json-transformer))
  (-> (jsonista.core/read-value "{\"tags\":[\"bar\",\"quux\"]}"
                                jsonista.core/keyword-keys-object-mapper)
      decode-Tags
      validate-Tags)


  ;; default values

  ;; applying default values
  (m/decode [:and {:default 42} int?]
            nil
            mt/default-value-transformer)

  (m/decode [:int {:default 42}]
            nil
            mt/default-value-transformer)

  ;; with custom key and type defaults
  (m/decode
   [:map
    [:user [:map
            [:name :string]
            [:description {:ui/default "-"} :string]]]]
   nil
   (mt/default-value-transformer
    {:key :ui/default
     :defaults {:map (constantly {})
                :string (constantly "")}}))

  ;; with custom function
  (m/decode
   [:map
    [:os [:string {:property "os.name"}]]
    [:timezone [:string {:property "user.timezone"}]]]
   {}
   (mt/default-value-transformer
    {:key :property
     :default-fn (fn [_ x] (System/getProperty x))}))

  ;; optional Keys are not added by default
  (m/decode
   [:map
    [:name [:string {:default "kikka"}]]
    [:description {:optional true} [:string {:default "kikka"}]]]
   {}
   (mt/default-value-transformer))

  ;; adding optional keys too via `::mt/add-optional-keys` option
  (m/decode
   [:map
    [:name [:string {:default "kikka"}]]
    [:description {:optional true} [:string {:default "kikka"}]]]
   {}
   (mt/default-value-transformer {::mt/add-optional-keys true}))

  ;; single sweep of defaults & string encoding
  (m/encode
   [:map {:default {:id 1}}
    [:a [int? {:default 1}]]
    [:b [:vector {:default [1 2 3]} int?]]
    [:c [:map {:default {}}
         [:x [int? {:default 42}]]
         [:y int?]]]
    [:d [:map
         [:x [int? {:default 42}]]
         [:y int?]]]
    [:e int?]]
   nil
   (mt/transformer
    mt/default-value-transformer
    mt/string-transformer))


  ;; programming with schemas

  ;; updating Schema properties
  (mu/update-properties [:vector int?] assoc :min 1)

  ;; lifted `clojure.core` function to work with schemas: `select-keys`, `dissoc`,
  ;; `get`, `assoc`, `update`, `get-in`, `assoc-in`, `update-in`
  (mu/get-in Address [:address :lonlat])
  (mu/update-in Address [:address] mu/assoc :country [:enum "fi" "po"])

  (-> Address
      (mu/dissoc :address)
      (mu/update-properties assoc :title "Address"))

  ;; making keys optional or required
  (mu/optional-keys [:map [:x int?] [:y int?]])
  (mu/required-keys [:map [:x {:optional true} int?] [:y int?]])

  ;; closing and opening all `:map` schemas recursively
  (def abcd
    [:map {:title "abcd"}
     [:a int?]
     [:b {:optional true} int?]
     [:c [:map
          [:d int?]]]])

  (mu/closed-schema abcd)

  (-> abcd
      mu/closed-schema
      mu/open-schema)

  ;; merging Schemas (last value wins)
  (mu/merge
   [:map
    [:name string?]
    [:description string?]
    [:address
     [:map
      [:street string?]
      [:country [:enum "finland" "poland"]]]]]
   [:map
    [:description {:optional true} string?]
    [:address
     [:map
      [:country string?]]]])

  ;; with `:and`, first child is used in `merge`
  (mu/merge
   [:and {:type "entity"}
    [:map {:title "user"}
     [:name :string]]
    map?]
   [:map {:description "aged"} [:age :int]])

  ;; schema unions (merged values of both schemas are valid for union schema)
  (mu/union
   [:map
    [:name string?]
    [:description string?]
    [:address
     [:map
      [:street string?]
      [:country [:enum "finland" "poland"]]]]]
   [:map
    [:description {:optional true} string?]
    [:address
     [:map
      [:country string?]]]])

  ;; adding generated example values to Schemas
  (m/walk
   [:map
    [:name string?]
    [:description string?]
    [:address
     [:map
      [:street string?]
      [:country [:enum "finland" "poland"]]]]]
   (m/schema-walker
    (fn [schema]
      (mu/update-properties
       schema
       assoc
       :examples
       (mg/sample schema {:size 2, :seed 20})))))

  ;; finding first value (prewalk)
  (mu/find-first
   [:map
    [:x int?]
    [:y [:vector [:tuple
                  [:or [:and {:salaisuus "turvassa"} boolean?] int?]
                  [:schema {:salaisuus "vaarassa"} false?]]]]
    [:z [:string {:salaisuus "piilossa"}]]]
   (fn [schema _ _]
     (-> schema m/properties :salaisuus)))

  ;; finding all subschemas with paths, retaining order
  (def Schema
    (m/schema
     [:maybe
      [:map
       [:id string?]
       [:tags [:set keyword?]]
       [:address
        [:and
         [:map
          [:street {:optional true} string?]
          [:lonlat {:optional true} [:tuple double? double?]]]
         [:fn (fn [{:keys [street lonlat]}] (or street lonlat))]]]]]))

  (mu/subschemas Schema)

  ;; collecting unique value paths and their schema paths
  (->> Schema
       (mu/subschemas)
       (mu/distinct-by :id)
       (mapv (juxt :in :path)))

  ;; schema paths can be converted into value paths
  (mu/get-in Schema [0 :address 0 :lonlat])
  (mu/path->in Schema [0 :address 0 :lonlat])

  ;; and back, returning all paths
  (mu/in->paths Schema [:address :lonlat])

  ;; declarative schema transformation

  ;; there are also declarative versions of schema transforming utilities in
  ;; `malli.util/schemas`. These include `:merge`, `:union` and `:select-keys`
  (def registry (merge (m/default-schemas) (mu/schemas)))

  (def Merged
    (m/schema
     [:merge
      [:map [:x :string]]
      [:map [:y :int]]]
     {:registry registry}))

  Merged
  (m/deref Merged)
  (m/validate Merged {:x "kikka", :y 6})


  ;; persisting schemas

  ;; writing and reading schemas as EDN, no eval needed.

  ;; following example requires sci as external dependency because it includes a
  ;; function definition. See Serializable functions.
  (-> [:and
       [:map
        [:x int?]
        [:y int?]]
       [:fn '(fn [{:keys [x y]}] (> x y))]]
      (edn/write-string)
      (doto prn)
      (edn/read-string)
      (doto (-> (m/validate {:x 0, :y 1}) prn))
      (doto (-> (m/validate {:x 2, :y 1}) prn)))


  ;; multi schemas

  ;; closed dispatch with `:multi` schema and `:dispatch` property
  (m/validate
   [:multi {:dispatch :type}
    [:sized [:map [:type keyword?] [:size int?]]]
    [:human [:map
             [:type keyword?]
             [:name string?]
             [:address [:map [:country keyword?]]]]]]
   {:type :sized, :size 10})

  ;; default branch with `::m/default`
  (def valid1?
    (m/validator
     [:multi {:dispatch :type}
      ["object" [:map-of :keyword :string]]
      [::m/default :string]]))

  (valid1? {:type "object", :key "1", :value "100"})
  (valid1? "SUCCESS!")
  (valid1? :failure)

  ;; Any function can be used for `:dispatch`
  (m/validate
   [:multi {:dispatch first}
    [:sized [:tuple keyword? [:map [:size int?]]]]
    [:human [:tuple keyword? [:map [:name string?] [:address [:map [:country keyword?]]]]]]]
   [:human {:name "seppo", :address {:country :sweden}}])

  (m/validate
   [:multi {:dispatch first}
    [:sized [:tuple keyword? [:map [:size int?]]]]
    [:human [:tuple keyword? [:map [:name string?] [:address [:map [:country keyword?]]]]]]]
   [:sized {:name "seppo", :address {:country :sweden}}])

  ;; `:dispatch` values should be decoded before actual values
  (m/decode
   [:multi {:dispatch :type
            :decode/string #(update % :type keyword)}
    [:sized [:map [:type [:= :sized]] [:size int?]]]
    [:human [:map [:type [:= :human]] [:name string?] [:address [:map [:country keyword?]]]]]]
   {:type "human"
    :name "Tiina"
    :age "98"
    :address {:country "finland"
              :street "this is an extra key"}}
   (mt/transformer mt/strip-extra-keys-transformer mt/string-transformer))


  ;; recursive schemas

  ;; to create a recursive schema, introduce a local registry and wrap all
  ;; recursive positions in the registry with `:ref`. Now you may reference the
  ;; recursive schemas in the body of the schema.

  ;; for example, here is a recursive schema using `:schema` for singly-linked
  ;; lists of positive integers
  (m/validate
   [:schema {:registry {::cons [:maybe [:tuple pos-int? [:ref ::cons]]]}}
    [:ref ::cons]]
   [16 [64 [26 [1 [13 nil]]]]])

  ;; without the `:ref` keyword, malli eagerly expands the schema until a stack
  ;; overflow error is thrown

  (m/validate
   [:schema {:registry {::cons [:maybe [:tuple pos-int? ::cons]]}}
    ::cons]
   [16 [64 [26 [1 [13 nil]]]]])

  ;; technically, you only need the `:ref` in recursive positions. However, it
  ;; is best practice to `:ref` all references to recursive variables for
  ;; better-behaving generators

  ;; note:
  [:schema {:registry {::cons [:maybe [:tuple pos-int? [:ref ::cons]]]}}
   ::cons]
  ;; produces the same generator as the "unfolded"
  [:maybe [:tuple pos-int? [:schema {:registry {::cons [:maybe [:tuple pos-int? [:ref ::cons]]]}} ::cons]]]
  ;; while
  [:schema {:registry {::cons [:maybe [:tuple pos-int? [:ref ::cons]]]}}
   [:ref ::cons]]
  ;; has a direct correspondance to the following generator:
  (gen/recursive-gen
   (fn [rec] (gen/one-of [(gen/return nil) (gen/tuple rec)]))
   (gen/return nil))

  ;; mutual recursion works too. Thanks to the `:schema` construct, many schemas
  ;; could be defined in the local registry, the top-level one being promoted by
  ;; the `:schema` second parameter
  (m/validate
   [:schema {:registry {::ping [:maybe [:tuple [:= "ping"] [:ref ::pong]]]
                        ::pong [:maybe [:tuple [:= "pong"] [:ref ::ping]]]}}
    ::ping]
   ["ping" ["pong" ["ping" ["pong" ["ping" nil]]]]])

  ;; nested registries, the last definition wins
  (m/validate
   [:schema {:registry {::ping [:maybe [:tuple [:= "ping"] [:ref ::pong]]]
                        ::pong any?}} ;; effectively unreachable
    [:schema {:registry {::pong [:maybe [:tuple [:= "pong"] [:ref ::ping]]]}}
     ::ping]]
   ["ping" ["pong" ["ping" ["pong" ["ping" nil]]]]])


  ;; value generation

  ;; Schemas can be used to generate values

  ;; random
  (mg/generate keyword?)

  ;; using seed
  (mg/generate [:enum "a" "b" "c"] {:seed 42})

  ;; using seed and size
  (mg/generate pos-int? {:seed 10, :size 100})

  ;; regexes work too (only clj and if [`com.gfredericks/test.chuck` "0.2.10"+]
  ;; available)
  (mg/generate
   [:re #"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,63}$"]
   {:seed 42, :size 10})

  ;; `:gen/elements` (note, are not validated)
  (mg/generate
   [:and {:gen/elements ["kikka" "kukka" "kakka"]} string?]
   {:seed 10})


  ;; `:gen/fmap`
  (mg/generate
   [:and {:gen/fmap (partial str "kikka_")} string?]
   {:seed 10, :size 10})

  ;; portable :gen/fmap (requires `org.babashka/sci` dependency to work)
  (mg/generate
   [:and {:gen/fmap '(partial str "kikka_")} string?]
   {:seed 10, :size 10})

  ;; `:gen/schema`
  (mg/generate
   [:any {:gen/schema [:int {:min 10, :max 20}]}]
   {:seed 10})

  ;; `:gen/min` & `:gen/max` for numbers and collections
  (mg/generate
   [:vector {:gen/min 4, :gen/max 4}
    [:int {:gen/min -4, :gen/max 4}]]
   {:seed 1})

  ;; `:gen/infinite?` & `:gen/NaN?` for `:double`
  (mg/generate
   [:double {:gen/infinite? true, :gen/NaN? true}]
   {:seed 1})

  ;; gen/gen (note, not serializable)
  (mg/generate
   [:sequential {:gen/gen (gen/list gen/neg-int)} int?]
   {:size 42, :seed 42})

  ;; generated values are valid
  (mg/generate Address {:seed 123, :size 4})
  (m/validate Address (mg/generate Address))


  ;; sampling values

  ;; sampling
  (mg/sample [:and int? [:> 10] [:< 100]] {:seed 123})

  ;; integration with `test.check`
  (gen/sample (mg/generator pos-int?))


  ;; inferring schemas

  ;; inspired by F# Type providers
  (def samples
    [{:id "Lillan"
      :tags #{:artesan :coffee :hotel}
      :address {:street "Ahlmanintie 29"
                :city "Tampere"
                :zip 33100
                :lonlat [61.4858322, 23.7854658]}}
     {:id "Huber",
      :description "Beefy place"
      :tags #{:beef :wine :beer}
      :address {:street "Aleksis Kiven katu 13"
                :city "Tampere"
                :zip 33200
                :lonlat [61.4963599 23.7604916]}}])

  (mp/provide samples)

  ;; all samples are valid against the inferred schema
  (every? (partial m/validate (mp/provide samples)) samples)

  ;; for better performance, use `mp/provider`
  (cc/bench (mp/provide samples))
  (let [provider (mp/provider)]
    (cc/bench (provider samples)))

  ;; `:map-of` inferring

  ;; by default, `:map-of` is not inferred
  (mp/provide
   [{"1" [1]}
    {"2" [1 2]}
    {"3" [1 2 3]}])

  ;; with `::mp/map-of-threshold` option
  (mp/provide
   [{"1" [1]}
    {"2" [1 2]}
    {"3" [1 2 3]}]
   {::mp/map-of-threshold 3})

  ;; sample-data can be type-hinted with `::mp/hint`
  (mp/provide
   [^{::mp/hint :map-of}
    {:a {:b 1, :c 2}
     :b {:b 2, :c 1}
     :c {:b 3}
     :d nil}])

  ;; `:tuple` inferring

  ;; by default, tuples are not inferred
  (mp/provide
   [[1 "kikka" true]
    [2 "kukka" true]
    [3 "kakka" true]])

  ;; with `::mp/tuple-threshold` option

  (mp/provide
   [[1 "kikka" true]
    [2 "kukka" true]
    [3 "kakka" false]]
   {::mp/tuple-threshold 3})

  ;; sample-data can be type-hinted with `::mp/hint`
  (mp/provide
   [^{::mp/hint :tuple}
    [1 "kikka" true]
    ["2" "kukka" true]])

  ;; value decoding in inferring

  ;; by default, no decoding is applied for (leaf) values
  (mp/provide
   [{:id "caa71a26-5fe1-11ec-bf63-0242ac130002"}
    {:id "8aadbf5e-5fe3-11ec-bf63-0242ac130002"}])

  ;; Adding custom decoding via `::mp/value-decoders` option
  (mp/provide
   [{:id "caa71a26-5fe1-11ec-bf63-0242ac130002"
     :time "2021-01-01T00:00:00Z"}
    {:id "8aadbf5e-5fe3-11ec-bf63-0242ac130002"
     :time "2022-01-01T00:00:00Z"}]
   {::mp/value-decoders {:string {:uuid mt/-string->uuid
                                   'inst? mt/-string->date}}})


  ;; destructuring

  ;; schemas can also be inferred from Clojure Destructuring Syntax.
  (def infer (comp :schema md/parse))

  (infer '[a b & cs])

  ;; malli also supports adding type hints as an extension to the normal Clojure
  ;; syntax (enabled by default), inspired by Plumatic Schema.
  (infer '[a :- :int, b :- :string & cs :- [:* :boolean]])

  ;; pulling out function argument schemas from Vars
  (defn kikka
    ([a] [a])
    ([a b & cs] [a b cs]))

  (md/infer #'kikka)

  ;; md/parse uses the following options:
  ;;   key 	                description
  ;; ::md/inline-schemas 	support plumatic-style inline schemas (true)
  ;; ::md/sequential-maps 	support sequential maps in non-rest position (true)
  ;; ::md/references 	    qualified schema references used (true)
  ;; ::md/required-keys 	destructured keys are required (false)
  ;; ::md/closed-maps 	    destructured maps are closed (false)

  ;; a more complete example
  (infer '[a [b c & rest :as bc]
           & {:keys [d e]
              :demo/keys [f]
              g :demo/g
              [h] :h
            :or {d 0}
              :as opts}])


  ;; parsing values

  ;; Schemas can be used to parse values using `m/parse` and `m/parser`

  ;; m/parse for one-time things
  (m/parse
   [:* [:catn
        [:prop string?]
        [:val [:altn
               [:s string?]
               [:b boolean?]]]]]
   ["-server" "foo" "-verbose" true "-user" "joe"])

  ;; `m/parser` to create an optimized parser
  (def Hiccup
    [:schema
     {:registry {"hiccup" [:orn
                           [:node [:catn
                                   [:name keyword?]
                                   [:props [:? [:map-of keyword? any?]]]
                                   [:children [:* [:schema [:ref "hiccup"]]]]]]
                           [:primitive [:orn
                                        [:nil nil?]
                                        [:boolean boolean?]
                                        [:number number?]
                                        [:text string?]]]]}}
     "hiccup"])

  (def parse-hiccup (m/parser Hiccup))

  (parse-hiccup
   [:div {:class [:foo :bar]}
    [:p "Hello, world of data"]])

  ;; parsing returns tagged values for `:orn`, `:catn`, `:altn` and `:multi`
  (def Multi
    [:multi {:dispatch :type}
     [:user [:map [:size :int]]]
     [::m/default :any]])

  (m/parse Multi {:type :user, :size 1})
  (m/parse Multi {:type "sized", :size 1})


  ;; unparsing values

  ;; the inverse of parsing, using `m/unparse` and `m/unparser`
  (->> [:div {:class [:foo :bar]}
        [:p "Hello, world of data"]]
       (m/parse Hiccup)
       (m/unparse Hiccup))

  ;; serializable functions

  ;; enabling serializable function schemas requires `sci` as external
  ;; dependency. If it is not present, the malli function evaluator throws
  ;; `:sci-not-available` exception.

  ;; for ClojureScript, you also need to require `sci.core` manually, either
  ;; directly or via `:preloads`.

  ;; for GraalVM, you need to require `sci.core` manually, before requiring any
  ;; malli namespaces.

  (def my-schema
    [:and
     [:map
      [:x int?]
      [:y int?]]
     [:fn '(fn [{:keys [x y]}] (> x y))]])

  (m/validate my-schema {:x 1, :y 0})
  (m/validate my-schema {:x 1, :y 2})

  ;; NOTE: sci is not termination safe so be wary of sci functions from
  ;; untrusted sources. You can explicitly disable sci with option
  ;; `::m/disable-sci` and set the default options with `::m/sci-options`.
  (m/validate [:fn 'int?] 1 {::m/disable-sci true})


  ;; Schema AST

  ;; implemented with protocol `malli.core/AST`. Allows lossless round-robin
  ;; with faster schema creation.

  ;; NOTE: for now, the AST syntax in considered as internal, e.g. don't use it
  ;; as a database persistency model.
  (def ?schema
    [:map
     [:x boolean?]
     [:y {:optional true} int?]
     [:z [:map
          [:x boolean?]
          [:y {:optional true} int?]]]])

  (m/form ?schema)
  (m/ast ?schema)
  (-> ?schema
      (m/schema) ;; 3.4µs
      (m/ast)
      (m/from-ast) ;; 180ns (18x, lazy)
      (m/form)
      (= (m/form ?schema)))


  ;; Schema transformation

  ;; Schemas can be transformed using post-walking, e.g. the Visitor Pattern.

  ;; the identity walker
  (m/walk
   Address
   (m/schema-walker identity))

  ;; adding `:title` property to schemas
  (m/walk
   Address
   (m/schema-walker #(mu/update-properties % assoc :title (name (m/type %)))))

  ;; transforming schemas into maps
  (m/walk
   Address
   (fn [schema _ children _]
     (-> (m/properties schema)
         (assoc :malli/type (m/type schema))
         (cond-> (seq children) (assoc :malli/children children)))))


  ;; JSON Schema

  ;; transforming Schemas into JSON Schema
  (json-schema/transform Address)

  ;; custom transformation via `:json-schema` namespaced properties
  (json-schema/transform
   [:enum
    {:title "Fish"
     :description "It's a fish"
     :json-schema/type "string"
     :json-schema/default "perch"}
   "perch" "pike"])

  ;; full override with :`json-schema` property
  (json-schema/transform
   [:map {:json-schema {:type "file"}}
    [:file any?]])


  ;; Swagger2

  ;; transforming Schemas into Swagger2 Schema
  (swagger/transform Address)

  ;; custom transformation via `:swagger` and `:json-schema` namespaced
  ;; properties
  (swagger/transform
   [:enum
    {:title "Fish"
     :description "It's a fish"
     :swagger/type "string"
     :json-schema/default "perch"}
    "perch" "pike"])

  ;; full override with `:swagger` property
  (swagger/transform
   [:map {:swagger {:type "file"}}
    [:file any?]])


  ;; custom schema types

  ;; Schema Types are described using `m/IntoSchema` protocol, which has a
  ;; factory method (-into-schema [this properties children options]) to create
  ;; the actual Schema instances. See `malli.core` for example implementations.

  ;; simple schema

  ;; for simple cases, there is `m/-simple-schema`
  (def Over6
    (m/-simple-schema
     {:type :user/over6
      :pred #(and (int? %) (> % 6))
      :type-properties {:error/message "should be over 6"
                        :decode/string mt/-string->long
                        :json-schema/type "integer"
                        :json-schema/format "int64"
                        :json-schema/minimum 6
                        :gen/gen (gen/large-integer* {:min 7})}}))

  (m/into-schema? Over6)

  ;; `m/IntoSchema` can be both used as Schema (creating a Schema instance with
  ;; nil properties and children) and as Schema type to create new Schema
  ;; instances without needing to register the types
  (m/schema? (m/schema Over6))
  (m/schema? (m/schema [Over6 {:title "over 6"}]))

  ;; `:pred` is used for validation
  (m/validate Over6 2)
  (m/validate Over6 7)

  ;; `:type-properties` are shared for all schema instances and are used just
  ;; like Schema (instance) properties by many Schema applications, including
  ;; error messages, value generation and json-schema transformations.
  (json-schema/transform Over6)
  (json-schema/transform [Over6 {:json-schema/example 42}])

  ;; content dependent simple schema

  ;; You can also build content-dependent schemas by using a callback function
  ;; of properties children -> opts instead of static opts
  (def Over
    (m/-simple-schema
     (fn [{:keys [value]} _]
       (assert (int? value))
       {:type :user/over
        :pred #(and (int? %) (> % value))
        :type-properties {:error/fn
                          (fn [error _]
                            (str "should be over " value ", was " (:value error)))
                          :decode/string mt/-string->long
                          :json-schema/type "integer"
                          :json-schema/format "int64"
                          :json-schema/minimum value
                          :gen/gen (gen/large-integer* {:min (inc value)})}})))

  (-> [Over {:value 12}]
      (m/explain 10)
      (me/humanize))


  ;; Schema registry

  ;; Schemas are looked up using a `malli.registry/Registry` protocol, which is
  ;; effectively a map from schema type to a schema recipe (Schema, IntoSchema
  ;; or vector-syntax schema). Maps can also be used as a registry.

  ;; custom Registry can be passed into all/most malli public APIs via the
  ;; optional options map using `:registry` key. If omitted,
  ;; `malli.core/default-registry` is used.

  ;; the default registry
  (m/validate [:maybe string?] "kikka")

  ;; registry as explicit options
  (m/validate [:maybe string?] "kikka" {:registry m/default-registry})

  ;; the default immutable registry is merged from multiple parts, enabling easy
  ;; re-composition of custom schema sets. See built-in schemas for list of all
  ;; Schemas.


  ;; custom registry

  ;; here's an example to create a custom registry without the default core
  ;; predicates and with `:neg-int` and `:pos-int` Schemas
  (def registry
    (merge
     (m/class-schemas)
     (m/comparator-schemas)
     (m/base-schemas)
     {:neg-int (m/-simple-schema {:type :neg-int, :pred neg-int?})
      :pos-int (m/-simple-schema {:type :pos-int, :pred pos-int?})}))

  (m/validate [:or :pos-int :neg-int] 'kikka {:registry registry})
  (m/validate [:or :pos-int :neg-int] 123 {:registry registry})

  ;; we did not register normal predicate schemas
  (m/validate pos-int? 123 {:registry registry})


  ;; local registry

  ;; any schema can define a local registry using `:registry` schema property
  (def Adult
    [:map {:registry {::age [:and int? [:> 18]]}}
     [:age ::age]])

  (mg/generate Adult {:size 10, :seed 1})

  ;; local registries can be persisted
  (-> Adult
      (malli.edn/write-string)
      (malli.edn/read-string)
      (m/validate {:age 46}))

  ;; see also Recursive Schemas.

  ;; changing the default registry

  ;; passing in custom options to all public methods is a lot of
  ;; boilerplate. For the lazy, there is an easier way - we can swap
  ;; the (global) default registry

  ;; the default registry
  (-> m/default-registry (mr/schemas) (count))

  ;; global side-effects! free since 0.7.0!
  (mr/set-default-registry!
   {:string (m/-string-schema)
    :maybe (m/-maybe-schema)
    :map (m/-map-schema)})

  (-> m/default-registry (mr/schemas) (count))

  (m/validate
   [:map [:maybe [:maybe :string]]]
   {:maybe "sheep"})

  (m/validate :int 42)

  ;; NOTE: `mr/set-default-registry!` is an imperative api with global
  ;; side-effects. Easy, but not simple. If you want to disable the api, you can
  ;; define the following compiler/jvm bootstrap:
  ;; cljs: :closure-defines {malli.registry/mode "strict"}
  ;; clj: :jvm-opts ["-Dmalli.registry/mode=strict"]

  ;; DCE and schemas

  ;; the default schema registry is defined as a Var, so all Schema
  ;; implementation (100+) are dragged in. For ClojureScript, this means the
  ;; schemas implementations are not removed via Dead Code Elimination (DCE),
  ;; resulting a large (37KB, zipped) js-bundle.

  ;; malli allows the default registry to initialized with empty schemas, using
  ;; the following compiler/jvm bootstrap:
  ;; cljs: :closure-defines {malli.registry/type "custom"}
  ;; clj: :jvm-opts ["-Dmalli.registry/type=custom"]

  ;; with the flag set on
  (-> m/default-registry (mr/schemas) (count))

  ;; with this, you can register just what you need and rest are DCE'd. The
  ;; previous example results in just a 3KB gzip bundle.


  ;; registry implementations

  ;; malli supports multiple type of registries.

  ;; immutable registry: just a Map.
  (mr/set-default-registry!
   {:string (m/-string-schema)
    :maybe (m/-maybe-schema)
    :map (m/-map-schema)})

  (m/validate
   [:map [:maybe [:maybe :string]]]
   {:maybe "sheep"})

  ;; mutable registry

  ;; `clojure.spec` introduces a mutable global registry for specs. The mutable
  ;; registry in malli forced you to bring in your own state atom and functions
  ;; how to work with it:

  ;; using a custom registry atom
  (def registry*
    (atom {:string (m/-string-schema)
           :maybe (m/-maybe-schema)
           :map (m/-map-schema)}))

  (defn register! [type ?schema]
    (swap! registry* assoc type ?schema))

  (mr/set-default-registry!
   (mr/mutable-registry registry*))

  (register! :non-empty-string [:string {:min 1}])
  (m/validate :non-empty-string "malli")

  ;; the mutable registry can also be passed in as an explicit option
  (def registry (mr/mutable-registry registry*))
  (m/validate :non-empty-string "malli" {:registry registry})

  ;; dynamic registry

  ;; if you know what you are doing, you can also use dynamic scope to pass in
  ;; default schema registry
  (mr/set-default-registry!
   (mr/dynamic-registry))

  (binding [mr/*registry* {:string (m/-string-schema)
                           :maybe (m/-maybe-schema)
                           :map (m/-map-schema)
                           :non-empty-string [:string {:min 1}]}]
    (m/validate :non-empty-string "malli"))

  ;; lazy registries

  ;; you can provide schemas at runtime using `mr/lazy-registry` - it takes a
  ;; local registry and a provider function of type registry -> schema as
  ;; arguments
  (def registry
    (mr/lazy-registry
     (m/default-schemas)
     (fn [type registry]
       ;; simulates pulling CloudFormation Schemas when needed
       (let [lookup {"AWS::ApiGateway::UsagePlan"
                     [:map {:closed true}
                      [:Type [:= "AWS::ApiGateway::UsagePlan"]]
                      [:Description {:optional true} string?]
                      [:UsagePlanName {:optional true} string?]]
                     "AWS::AppSync::ApiKey" [:map {:closed true}
                                             [:Type [:= "AWS::AppSync::ApiKey"]]
                                             [:ApiId string?]
                                             [:Description {:optional true} string?]]}]
         (println "... loaded" type)
         (some-> type lookup (m/schema {:registry registry}))))))

  ;; lazy multi, doesn't realize the schemas
  (def CloudFormation
    (m/schema
     [:multi {:dispatch :Type, :lazy-refs true}
      "AWS::ApiGateway::UsagePlan"
      "AWS::AppSync::ApiKey"]
     {:registry registry}))

  ;; side-effecting (console printing) only once
  (m/validate
   CloudFormation
   {:Type "AWS::ApiGateway::UsagePlan"
    :Description "laiskanlinna"})

  (m/validate
   CloudFormation
   {:Type "AWS::ApiGateway::UsagePlan"
    :Description "laiskanlinna"})

  ;; composite registry

  ;; registries can be composed, a full example
  (def registry (atom {}))

  (defn register! [type schema]
    (swap! registry assoc type schema))

  (mr/set-default-registry!
   ;; linear search
   (mr/composite-registry
    ;; immutable registry
    {:map (m/-map-schema)}
    ;; mutable (spec-like) registry
    (mr/mutable-registry registry)
    ;; on the perils of dynamic scope
    (mr/dynamic-registry)))

  (register! :maybe (m/-maybe-schema))

  ;; ☆.。.:*・°☆.。.:*・°☆.。.:*・°☆.。.:*・°☆
  (binding [mr/*registry* {:string (m/-string-schema)}]
    (m/validate
     [:map [:maybe [:maybe :string]]]
     {:maybe "sheep"}))


  ;; function Schemas

  ;; functions

  ;; in Clojure, functions are first-class. Here's a simple function
  (defn plus [x y]
    (+ x y))

  (plus 1 2)


  ;; predicate Schemas

  ;; simplest way to describe function values with malli is to use predefined
  ;; predicate schemas `fn?` and `ifn?`
  (m/validate fn? plus)
  (m/validate ifn? plus)

  ;; note that `ifn?` also accepts many data-structures that can be used as
  ;; functions

  (m/validate ifn? :kikka)
  (m/validate ifn? {})

  ;; but, neither of the predefined function predicate schemas can validate
  ;; function arity, function arguments or return values. As it stands, there is
  ;; no robust way to programmatically check function arity at runtime. Enter
  ;; function schemas.

  ;; function Schemas

  ;; function values can be described with `:=>` and `:function` schemas. They
  ;; allows description of both function arguments (as sequence schemas) and
  ;; function return values.

  ;; examples of function definitions

  ;; no args, no return
  [:=> :cat :nil]

  ;; int -> int
  [:=> [:cat :int] :int]

  ;; x:int, xs:int* -> int
  [:=> [:catn
        [:x :int]
        [:xs [:+ :int]]] :int]

  ;; multi-arity function
  [:function
   [:=> [:cat :int] :int]
   [:=> [:cat :int :int [:* :int]] :int]]

  ;; function definition for the plus looks like this
  (def =>plus [:=> [:cat :int :int] :int])

  ;; let's try:
  (m/validate =>plus plus)

  ;; but, wait, as there was no way to know the function arity & other
  ;; information at runtime, so how did the validation work? Actually, it
  ;; didn't. By default. `:=>` validation just checks that it's a `fn?`, so this
  ;; holds too. Enter generative testing.
  (m/validate =>plus str)

  ;; generative testing

  ;; like clojure.spec demonstrated, we can use `test.check` to check the
  ;; functions at runtime. For this, there is `:malli.core/function-checker`
  ;; option.
  (def =>plus
    (m/schema
     [:=> [:cat :int :int] :int]
     {::m/function-checker mg/function-checker}))

  (m/validate =>plus plus)
  (m/validate =>plus str)

  ;; explanation why it is not valid
  (m/explain =>plus str)

  ;; smallest failing invocation is (str 0 0), which returns "00", which is not
  ;; an `:int`. Looks good.

  ;; but, why `mg/function-checker` is not enabled by default? The reason is
  ;; that it uses generative testing, which is orders of magnitude slower than
  ;; normal validation and requires an extra dependency to `test.check`, which
  ;; would make `malli.core` much heavier. This would be expecially bad for CLJS
  ;; bundle size.

  ;; generating functions

  ;; we can also generate function implementations based on the function
  ;; schemas. The generated functions check the function arity and arguments at
  ;; runtime and return generated values.
  (def plus-gen (mg/generate =>plus))

  (plus-gen 1)
  (plus-gen 1 "2")
  (plus-gen 1 2)

  ;; multi-arity functions

  ;; multi-arity functions can be composed with `:function`

  ;; multi-arity fn with function checking always on
  (def =>my-fn
    (m/schema
     [:function {:registry {::small-int [:int {:min -100, :max 100}]}}
      [:=> [:cat ::small-int] :int]
      [:=> [:cat ::small-int ::small-int [:* ::small-int]] :int]]
     {::m/function-checker mg/function-checker}))

  (m/validate
   =>my-fn
   (fn
     ([x] x)
     ([x y & z] (apply - (- x y) z))))

  (m/validate
   =>my-fn
   (fn
     ([x] x)
     ([x y & z] (str x y z))))

  (m/explain
   =>my-fn
   (fn
     ([x] x)
     ([x y & z] (str x y z))))

  ;; generating multi-arity functions
  (def my-fn-gen (mg/generate =>my-fn))

  (my-fn-gen)
  (my-fn-gen 1)
  (my-fn-gen 1 2)
  (my-fn-gen 1 2 3 4)


  ;; instrumentation

  ;; besides testing function schemas as values, we can also intrument functions
  ;; to enable runtime validation of arguments and return values.

  ;; simplest way to do this is to use `m/-instrument` which takes options map
  ;; and a function and returns a instrumented function. Valid options include
  ;; key        description
  ;; `:schema` 	function schema
  ;; `:scope` 	optional set of scope definitions, defaults to #{:input :output}
  ;; `:report`    optional side-effecting function of key data -> any to report
  ;; problems, defaults to `m/-fail!`
  ;; `:gen`       optional function of schema -> schema -> value to be invoked on
  ;; the args to get the return value

  ;; instrumenting a function with input & return constraints
  (def pow
    (m/-instrument
     {:schema [:=> [:cat :int] [:int {:max 6}]]}
     (fn [x] (* x x))))

  (pow 2)
  (pow "2")
  (pow 4)
  (pow 4 2)

  ;; example of a multi-arity function with instrumentation scopes and custom
  ;; reporting function
  (def multi-arity-pow
    (m/-instrument
     {:schema [:function
               [:=> [:cat :int] [:int {:max 6}]]
               [:=> [:cat :int :int] [:int {:max 6}]]]
      :scope #{:input :output}
      :report println}
     (fn
       ([x] (* x x))
       ([x y] (* x y)))))

  (multi-arity-pow 4)
  (multi-arity-pow 5 0.1)

  ;; with `:gen` we can omit the function body. Here's an example to generate
  ;; random values based on the return schema
  (def pow-gen
    (m/-instrument
     {:schema [:function
               [:=> [:cat :int] [:int {:max 6}]]
               [:=> [:cat :int :int] [:int {:max 6}]]]
      :gen mg/generate}))

  (pow-gen 10)
  (pow-gen 10 20)
  (pow-gen 10 20 30)


  ;; defn Schemas

  ;; defining function Schemas

  ;; there are three ways to add function schemas to function Vars (e.g. defns)
  ;;   Function Schema Annotation with `m/=>`
  ;;   Function Schema Metadata via `:malli/schema`
  ;;   Function Inline Schemas with `mx/defn`

  ;; function Schema annotations

  ;; `m/=>` macro takes the Var name and the function schema and stores the var
  ;; -> schema mappings in a global registry.
  (def small-int [:int {:max 6}])

  (defn plus1 [x] (inc x))
  (m/=> plus1 [:=> [:cat :int] small-int])

  ;; the order doesn't matter, so this also works

  (m/=> plus1 [:=> [:cat :int] small-int])
  (defn plus1 [x] (inc x))

  ;; listing the current accumulation of function (Var) schemas
  (m/function-schemas)

  ;; without instrumentation turned on, there is no schema enforcement
  (plus1 10)

  ;; turning instrumentation on
  (mi/instrument!)

  (plus1 10)


  ;; function Schema metadata

  ;; defn schemas can be defined with standard Var metadata. It allows defn
  ;; schema documentation and instrumentation without dependencies to malli
  ;; itself from the functions. It's just data.

  (defn minus
    "a normal clojure function, no dependencies to malli"
    {:malli/schema [:=> [:cat :int] small-int]}
    [x]
    (dec x))

  ;; to collect instrumentation for the defn, we need to call `mi/collect!`. It
  ;; reads all public vars from a given namespace and registers function schemas
  ;; from `:malli/schema` metadata.
  (mi/collect! {:ns *ns*})

  (m/function-schemas)

  ;; we'll also have to reinstrument the new var
  (mi/instrument!)

  (minus 8)

  ;; all Var metadata keys with malli namespace are used. The list of relevant
  ;; keys
  ;; key 	        description
  ;; `:malli/schema` 	function schema
  ;; `:malli/scope` 	optional set of scope definitions, defaults to
  ;; #{:input :output}
  ;; `:malli/report` 	optional side-effecting function of key data -> any
  ;; to report problems, defaults to `m/-fail!`
  ;; `:malli/gen` 	    optional value true or function of schema -> schema ->
  ;; value to be invoked on the args to get the return value

  ;; setting `:malli/gen` to true while function body generation is enabled with
  ;; `mi/instrument!` allows body to be generated, to return valid generated
  ;; data.


  ;; function inline Schemas

  ;; malli also supports Plumatic Schema-style schema hints via
  ;; `malli.experimental` ns
  (mx/defn times :- :int
    "x times y"
    [x :- :int, y :- small-int]
    (* x y))

  ;; function schema is registered automatically
  (m/function-schemas)

  ;; ... but not instrumented
  (times 10 10)

  ;; with instrumentation
  (mi/instrument!)

  (times 10 10)


  ;; defn instrumentation

  ;; the function (Var) registry is passive and doesn't do anything by
  ;; itself. To instrument the Vars based on the registry, there is the
  ;; `malli.instrument` namespace. Var instrumentations focus is for development
  ;; time, but can also be used for production builds.

  ;; Vars can be instrumented with `mi/instrument!` and the instrumentation can
  ;; be removed with `mi/unstrument!`.
  (m/=> power [:=> [:cat :int] [:int {:max 6}]])
  (defn power [x] (* x x))

  (power 6)

  ;; instrument all registered vars
  (mi/instrument!)
  (power 6)
  (mi/unstrument!)
  (power 6)

  ;; instrumentation can be configured with the same options as `m/-instrument`
  ;; and with a set of `:filters` to select which Vars should be instrumented.
  (mi/instrument!
   {:filters [;; everything from user ns
              (mi/-filter-ns 'user)
              ;; ... and some vars
              (mi/-filter-var #{#'power})
              ;; all other vars with :always-validate meta
              (mi/-filter-var #(-> % meta :always-validate))]
    ;; scope
    :scope #{:input :output}
    ;; just print
    :report println})

  (power 6)


  ;; defn checking

  ;; we can also check the defn schemas against their function implementations
  ;; using `mi/check`. It takes same options as `mi/instrument!`.

  ;; checking all registered schemas
  (mi/check)

  ;; It reports that the `plus1` is not correct. It accepts `:int` but promises
  ;; to return [:int {:max 6}]. Let's fix the contract by constraining the input
  ;; values.
  (m/=> plus1 [:=> [:cat [:int {:max 5}]] [:int {:max 6}]])
  (mi/check)

  ;; all good! But, it's still wrong as the actual implementation allows invalid
  ;; inputs resulting in invalid outputs (e.g. 6 -> 7). We could enable
  ;; instrumentation for the function to fail on invalid inputs at runtime - or
  ;; write similar range checks ourselves into the function body.

  ;; a pragmatically correct schema for `plus1` would
  ;; be [:=> [:cat :int] [:int]]. It also checks, but would fail on
  ;; Long/MAX_VALUE as input. Fully correct schema would be [:=> [:cat [:int
  ;; {:max (dec Long/MAX_VALUE)}] [:int]]]. Generative testing is best effort,
  ;; not a silver bullet.

  ;; we redefined `plus1` function schema and the instrumentation is now out of
  ;; sync. We have to call `mi/instrument!` to re-instrument it correctly.

  ;; the old schema & old error
  (plus1 6)
  (mi/instrument!)

  ;; the new schema & new error
  (plus1 6)

  ;; this is not good developer experience. We can do much better.


  ;; development instrumentation

  ;; for better DX, there is `malli.dev` namespace.

  ;; it's main entry point is `dev/start!`, taking same options as
  ;; `mi/instrument!`. It runs `mi/instrument!` and `mi/collect!` (for all
  ;; loaded namespaces) once and starts watching the function registry for
  ;; changes. Any change that matches the filters will cause automatic
  ;; re-instrumentation for the functions. `dev/stop!` removes all
  ;; instrumentation and stops watching the registry.
  (defn plus1 [x] (inc x))
  (m/=> plus1 [:=> [:cat :int] [:int {:max 6}]])

  (dev/start!)

  (plus1 "6")
  (plus1 6)
  (m/=> plus1 [:=> [:cat :int] :int])
  (plus1 6)

  (dev/stop!)

  ;; pretty errors

  ;; for prettier runtime error messages, we can swap the default error printer
  ;; / thrower.
  (defn plus1 [x] (inc x))
  (m/=> plus1 [:=> [:cat :int] [:int {:max 6}]])
  (dev/start! {:report (pretty/reporter)})

  (plus1 "2")

  ;; to throw the prettified error instead of just printing it
  (dev/start! {:report (pretty/thrower)})

  ;; Pretty printer uses fipp under the hood and has lot of configuration
  ;; options
  (dev/start! {:report (pretty/reporter (pretty/-printer {:width 70
                                                          :print-length 30
                                                          :print-level 2
                                                          :print-meta true}))})

  (dev/stop!)


  ;; clj-kondo

  ;; clj-kondo is a linter for Clojure code that sparks joy.

  ;; given functions and function Schemas
  (defn square [x] (* x x))
  (m/=> square [:=> [:cat int?] nat-int?])

  (defn plus
    ([x] x)
    ([x y] (+ x y)))

  (m/=> plus [:function
              [:=> [:cat int?] int?]
              [:=> [:cat int? int?] int?]])

  ;; generating clj-kondo configuration from current namespace

  (-> (mc/collect *ns*) (mc/linter-config))

  ;; emitting confing into ./.clj-kondo/configs/malli/config.edn
  (mc/emit!)

)
