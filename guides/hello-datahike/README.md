# hello-datahike

Explore datahike --- Datomic-like triple store and durable Datalog
implementation.

Also includes a Datalog tutorial.

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.

## Database backends

To use certain database backends, e.g. relational ones, you need to start
database services first:

```
podman-compose -f db.yml up -d [db-backend-name*]
```

if `db-backend-name`(s) are not specified, all will be started.

## ClojureScript with IndexedDB backend

Experimental (not quite working yet). Add `:cljs` alias when starting a REPL.
