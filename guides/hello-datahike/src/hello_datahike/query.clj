(ns hello-datahike.query
  (:require
   [datahike.api :as d]
   [hello-datahike.common :as c]))


;; query API

;; https://docs.datomic.com/on-prem/query/query.html


;; prepare the db (in-memory)

(comment

  (def cfg {:store              {:backend :mem
                                 :id      "query-api-tutorial"}
            :schema-flexibility :write
            :keep-history?      true
            :attribute-refs?    true})

  ;; create the db and connect to it
  (def conn (c/connect! cfg {:delete-db false}))

  ;; there are 20 movies in the db now
  (count (d/q '[:find ?title
                :where
                [_ :movie/title ?title]]
              @conn))

  )


;; overview

(comment

  ;; connection and database value

  ;; to query a database, we must first get a value of the database from the
  ;; connection:

  ;; Datomic: (db conn)

  @conn

  ;; a database is an immutable value, and will never change. If we use db for
  ;; several queries we will know the answers are based upon exactly the same
  ;; data from a single point in time.


  ;; `q`

  ;; accepts a query in the list form or map form (see examples below) and
  ;; variable quantity of args.

  (def find-movies-by-director-query-list
    '[:find ?title
      :in $ ?name
      :where
      [?d :person/name ?name]
      [?m :movie/director ?d]
      [?m :movie/title ?title]])

  (def find-movies-by-director-query-map
    '{:find [?title]
      :in [$ ?name]
      :where [[?d :person/name ?name]
              [?m :movie/director ?d]
              [?m :movie/title ?title]]})

  ;; `q` also can take a single map argument with keys `:query` and `:args`, and
  ;; optional keys `:offset`, `:limit` and `:timeout`. Maps are more convenient
  ;; for programmatic query building and modification.

  ;; note: `:timeout` may not be supported by Datahike (yet?)

  ;; all the same

  (d/q find-movies-by-director-query-list @conn "James Cameron")
  (d/q find-movies-by-director-query-map @conn "James Cameron")
  (d/q {:query find-movies-by-director-query-list
        :args [@conn "James Cameron"]})
  (d/q {:query find-movies-by-director-query-map
        :args [@conn "James Cameron"]})

  ;; limit and offset

  ;; note that sets do not preserve order

  (d/q {:query find-movies-by-director-query-list
        :limit 1
        :offset 1
        :timeout 100
        :args [@conn "James Cameron"]})

  (d/q {:query find-movies-by-director-query-map
        :limit 1
        :offset 2
        :timeout 100
        :args [@conn "James Cameron"]})

)


;; reference

(comment

  ;; basics

  ;; database is a universal relation (set) of datoms, i.e. 5-tuples of the
  ;; form: `[entity attribute value transaction added?]`. For queries, any
  ;; relation of any arity will do instead of database.

  (def db '[[sally :age 21]
            [fred :age 42]
            [ethel :age 42]
            [fred :likes pizza]
            [sally :likes opera]
            [ethel :likes sushi]])

  ;; the following query has one variable `?e`, and will take one input, a
  ;; collection of tuples with at least three components. This clause `[?e :age
  ;; 42]` is called a *data clause*. A data clause consists of constants and/or
  ;; variables, and a tuple satisfies a clause if its constants match. Variables
  ;; in the data pattern are then bound to the corresponding part of the
  ;; matching tuple. All of this matching happens by position.

  (d/q '[:find ?e
         :where
         [?e :age 42]]
       db)


  ;; blanks

  ;; sometimes we don't care about certain components of the tuples in a query,
  ;; but must put something in the clause in order to get to the positions we
  ;; care about. The underscore symbol `_` is blank placeholder, matching
  ;; anything without binding or unifying.

  ;; anything that is liked, without caring who does the liking
  (d/q '[:find ?x
         :where
         [_ :likes ?x]]
       db)

  ;; do not use a dummy variable instead of the blank. This will make the query
  ;; engine do extra work, tracking binding and unification for a variable that
  ;; you never intend to use. It will also make human readers do extra work,
  ;; puzzling out that a variable is intentionally not used.


  ;; implicit blanks

  ;; in data patterns, you can always elide any trailing components you don't
  ;; care about, rather than explicitly padding with blanks:

  ;; who likes something
  (d/q '[:find ?e
         :where
         [?e :likes]]
       db)


  ;; inputs

  ;; by default, queries expect a single input, a database whose name is the
  ;; dollar sign `$`. Also by default, data patterns refer to a database named
  ;; `$`.

  ;; queries can choose to explicitly name their inputs via an `:in` clause.

  ;; database is explicitly specified as input
  (d/q '[:find ?e
         :in $
         :where
         [?e :age 42]]
       db)

  ;; data pattern uses a leading `$` to choose the database it matches against
  (d/q '[:find ?e
         :in $
         :where
         [$ ?e :age 42]]
       db)


  ;; multiple inputs

  ;; most real-world queries are parameterized at runtime with *variable
  ;; bindings*.

  (d/q '[:find ?e
         :in $ ?age
         :where
         [?e :age ?age]]
       db 42)


  ;; pattern inputs

  ;; an input can be a *pull pattern*, which can be named by a symbol in the
  ;; `:in` clause, and that name can be used in pull expressions in the `:find`
  ;; clause. Pull patterns require an actual database as input; see `pull.clj`
  ;; for examples.

  ;; separation of concerns

  ;; the pull API separates the process of finding entities and acquiring
  ;; information about the entities. Pull expressions allow you to utilize
  ;; queries to find entities and return an explicit map with the desired
  ;; information about each entity, i.e., the same query will return different
  ;; results when provided with different pull patterns.


  ;; bindings

  ;; input shapes can be bound as follows:
  ;; - `?a`: scalar
  ;; - `[?a ?b]`: tuple
  ;; - `[?a ...]`: collection
  ;; - `[[?a ?b]]`: relation

  ;; tuple binding

  ;; a tuple binding binds a set of variables to a single value each, passed in
  ;; as a collection. This can be used to ask "and" questions, i.e. what movies
  ;; are directed by James Cameron and starring Arnold Schwarzenegger:

  (def find-movie-by-director-and-actor
    '[:find ?title
      :in $ [?director ?actor]
      :where
      [?m :movie/cast ?a]
      [?m :movie/director ?d]
      [?m :movie/title ?title]
      [?a :person/name ?actor]
      [?d :person/name ?director]])

  (d/q find-movie-by-director-and-actor
       @conn
       ["James Cameron" "Arnold Schwarzenegger"])

  ;; collection binding

  ;; a collection binding binds a single variable to multiple values passed in
  ;; as a collection. This can be used to ask "or" questions, i.e. what movies
  ;; are associated with either James Cameron or Ridley Scott?

  (def find-movies-by-director-query-list
    '[:find ?title
      :in $ [?director ...]
      :where
      [?m :movie/director ?d]
      [?m :movie/title ?title]
      [?d :person/name ?director]])

  (d/q find-movies-by-director-query-list
       @conn
       ["James Cameron" "Ridley Scott"])

  ;; relation binding

  ;; a relation binding is fully general, binding multiple variables
  ;; positionally to a relation (collection of tuples) passed in. This can be
  ;; used to ask "or" questions involving multiple variables. For example, what
  ;; actors were playing in James Cameron's movies from 1986 or Ridley Scott's
  ;; movies from 1979?

  (def find-actors-by-director-and-year
    '[:find ?actor
      :in $ [[?director ?year]]
      :where
      [?d :person/name ?director]
      [?m :movie/director ?d]
      [?m :movie/year ?year]
      [?m :movie/cast ?a]
      [?a :person/name ?actor]])

  (d/q find-actors-by-director-and-year
       @conn
       [["James Cameron" 1984]
        ["Ridley Scott" 1979]])


  ;; find specifications

  ;; where bindings control inputs, find specifications control results.

  ;; find spec	      returns        Java type returned
  ;; ---------------------------------------------
  ;; `:find ?a ?b`:   relation       Collection of Lists
  ;; `:find [?a ...]` collection     Collection
  ;; `:find [?a ?b]`  single tuple   List
  ;; `:find ?a .`     single scalar  Scalar Value

  ;; the relation find spec is the most common, and the most general. It will
  ;; return a tuple for each result, with values in each tuple matching the
  ;; named variables. All of the examples so far have used the relation find
  ;; spec. The example below finds a relation spec with two variables and
  ;; returns a relation of 2-tuples:

  (def find-movies-with-years-by-director
    '[:find ?title ?year
      :in $ [?director ...]
      :where
      [?m :movie/director ?d]
      [?m :movie/title ?title]
      [?m :movie/year ?year]
      [?d :person/name ?director]])

  (d/q find-movies-with-years-by-director
       @conn
       ["James Cameron" "Ridley Scott"])

  ;; the collection find spec is useful when you are only interested in a single
  ;; variable. The form `[?title ...]` below returns values for `?title`, not
  ;; wrapped in a one-tuple:

  (def find-movie-titles-by-director
    '[:find [?title ...]
      :in $ [?director ...]
      :where
      [?m :movie/director ?d]
      [?m :movie/title ?title]
      [?d :person/name ?director]])

  (d/q find-movie-titles-by-director
       @conn
       ["James Cameron" "Ridley Scott"])

  ;; the single tuple find spec is useful when you are interested in multiple
  ;; variables, but expect only a single result. The form `[?title ?year
  ;; ?director]` below returns a single triple, not wrapped in a relation.

  (def find-one-movie-with-year-and-director-by-actor
    '[:find [?title ?year ?director]
      :in $ ?actor
      :where
      [?a :person/name ?actor]
      [?m :movie/cast ?a]
      [?m :movie/title ?title]
      [?m :movie/year ?year]
      [?m :movie/director ?d]
      [?d :person/name ?director]])

  (d/q find-one-movie-with-year-and-director-by-actor
       @conn
       "Arnold Schwarzenegger")

  ;; the scalar find spec is useful when you want to return a single value of a
  ;; single variable. The form `?year` below returns a single scalar value:

  (def find-year-by-movie-title
    '[:find ?year .
      :in $ ?title
      :where
      [?m :movie/title ?title]
      [?m :movie/year ?year]])

  (d/q find-year-by-movie-title
       @conn
       "Mad Max")


  ;; return maps

  ;; supplying a return-map will cause the query to return maps instead of
  ;; tuples. Each entry in the `:keys`/`:strs`/`:syms` clause will become a key
  ;; mapped to the corresponding item in the find clause.

  ;; keyword  symbols become
  ;; `:keys`: keyword keys
  ;; `:strs`: string keys
  ;; `:syms`: symbol keys

  ;; in the example below, the `:keys` `title` and `year` are used to construct
  ;; a map for each row returned.

  (def find-movies-with-years-by-director-as-maps
    '[:find ?title ?year
      :in $ [?director ...]
      :keys title year
      :where
      [?m :movie/director ?d]
      [?m :movie/title ?title]
      [?m :movie/year ?year]
      [?d :person/name ?director]])

  (d/q find-movies-with-years-by-director-as-maps
       @conn
       ["James Cameron" "Ridley Scott"])

  ;; in Datomic, return maps also preserve the order of the find clause. In
  ;; particular, return maps

  ;; - implement `clojure.lang.Indexed`
  ;; - support `nth`
  ;; - support vector style destructuring

  ;; for example, the first result from the previous query can be destructured
  ;; in two ways:

  (def a-movie (first (d/q find-movies-with-years-by-director-as-maps
                           @conn
                           ["James Cameron" "Ridley Scott"])))

  ;; in Datahike, this is an ordinary persistent map
  (type a-movie)

  ;; so this is not working in Datahike
  (let [[title year] a-movie]
    (str title " " year))

  ;; only this
  (let [{:keys [title year]} a-movie]
    (str title " " year))


  ;; not clauses

  ;; not clauses allow you to express that one or more logic variables inside a
  ;; query must not satisfy all of a set of predicates. A not clause is written
  ;; as:

  ;; `(src-var? 'not' clause+)`

  ;; and removes already-bound tuples that satisfy the clauses. not clauses
  ;; target a source named `$` unless you specify an explicit src-var.

  ;; the following query uses a not clause to find all James Cameron movies that
  ;; are not "The Terminator":

  (def find-james-cameron-movies-not-terminator
    '[:find ?title
      :where
      [?d :person/name "James Cameron"]
      [?m :movie/director ?d]
      [?m :movie/title ?title]
      (not [?m :movie/title "The Terminator"])])

  (def find-james-cameron-movies
    '[:find ?title
      :where
      [?d :person/name "James Cameron"]
      [?m :movie/director ?d]
      [?m :movie/title ?title]])

  (count (d/q find-james-cameron-movies-not-terminator @conn))
  ;; should be one more than the previous
  (count (d/q find-james-cameron-movies @conn))

  ;; all variables used in a not clause will unify with the surrounding
  ;; query. This includes both the arguments to nested expression clauses as
  ;; well as any bindings made by nested function expressions. Datomic will
  ;; attempt to push the not clause down until all necessary variables are
  ;; bound, and will throw an exception if that is not possible.

  ;; a not-join clause allows you to specify which variables should unify with
  ;; the surrounding clause; only this list of variables needs binding before
  ;; the clause can run.

  ;; a not-join clause is written as:

  ;; `(src-var? 'not-join' [var+] clause+)`

  ;; where var specifies which variables should unify.

  ;; In this next query, which returns directors who didn't release a movie in
  ;; 1979, `?m` is used only inside the not clause and doesn't need to unify
  ;; with the outer clause. not-join is used to specify that only `?d` needs
  ;; unifying.

  ;; when more than one clause is supplied to not, you should read the clauses
  ;; as if they are connected by 'and', just as they are in `:where`.

  (def find-directors-that-not-released-a-movie-in-1979
    '[:find ?director
      :where
      [?d :person/name ?director]
      [?m :movie/director ?d]
      (not-join [?d]
                [?m :movie/director ?d]
                [?m :movie/year 1979])])

  (def find-directors-that-released-a-movie-in-1979
    '[:find ?director
      :where
      [?d :person/name ?director]
      [?m :movie/director ?d]
      [?m :movie/year 1979]])

  (def find-directors
    '[:find ?director
      :where
      [?d :person/name ?director]
      [?m :movie/director ?d]])

  (count (d/q find-directors-that-not-released-a-movie-in-1979 @conn))
  (count (d/q find-directors-that-released-a-movie-in-1979 @conn))
  ;; should be the sum of the two previous
  (count (d/q find-directors @conn))

  ;; how not clauses work

  ;; one can understand not clauses as if they turn into subqueries where all of
  ;; the variables and sources unified by the negation are propagated to the
  ;; subquery. The results of the subquery are removed from the enclosing query
  ;; via set difference. Note that, because they are implemented using set
  ;; logic, not clauses can be much more efficient than building your own
  ;; expression predicate that executes a query, as expression predicates are
  ;; run on each tuple in turn.


  ;; or clauses

  ;; or clauses allow you to express that one or more logic variables inside a
  ;; query satisfy at least one of a set of predicates. An or clause is written
  ;; as:

  ;; `(src-var? 'or' (clause | and-clause)+)`

  ;; and constrains the result to tuples that satisfy at least one of the
  ;; clauses in the or clause. or clauses target a source named `$` unless you
  ;; specify an explicit src-var.

  ;; the following query uses an or clause to find all movies either by James
  ;; Cameron or by Ridley Scott:

  (def find-movie-titles-by-james-cameron-or-ridley-scott
    '[:find ?title
      :where
      [?m :movie/director ?d]
      [?m :movie/title ?title]
      (or [?d :person/name "James Cameron"]
          [?d :person/name "Ridley Scott"])])

  (d/q find-movie-titles-by-james-cameron-or-ridley-scott @conn)

  ;; inside the or clause, you may use an and clause to specify
  ;; conjunction. This clause is not available outside of an or clause, since
  ;; conjunction is the default in other clauses.

  ;; the following query uses an and clause inside the or clause to find either
  ;; directors or deceased actors

  (def find-either-directors-or-deceased-actors
    '[:find ?name
      :where
      [?p :person/name ?name]
      (or [_ :movie/director ?p]
          (and [_ :movie/cast ?p]
               [?p :person/death]))])

  (def find-directors
    '[:find ?name
      :where
      [?p :person/name ?name]
      [_ :movie/director ?p]])

  (def find-deceased-actors
    '[:find ?name
      :where
      [?p :person/name ?name]
      [?p :person/death]
      [_ :movie/cast ?p]])

  (count (d/q find-directors @conn))
  (count (d/q find-deceased-actors @conn))
  ;; should be the sum of the two previous
  (count (d/q find-either-directors-or-deceased-actors @conn))

  ;; all clauses used in an or clause must use the same set of variables, which
  ;; will unify with the surrounding query. This includes both the arguments to
  ;; nested expression clauses as well as any bindings made by nested function
  ;; expressions. Datomic will attempt to push the or clause down until all
  ;; necessary variables are bound, and will throw an exception if that is not
  ;; possible.


  ;; or-join clause

  ;; an or-join is similar to an or clause, but it allows you to specify which
  ;; variables should unify with the surrounding clause; only this list of
  ;; variables needs binding before the clause can run. The variables specifies
  ;; which variables should unify.

  ;; an or-join clause is written as:
  ;; `(src-var? 'or-join' [variable+] (clause | and-clause)+)`

  ;; in this query, which returns the names of persons that are either directors
  ;; or deceased actors, `?m` is only used inside the or clause and doesn't need
  ;; to unify with the outer clause. or-join is used to specify that only `?p`
  ;; needs unifying.

  ;; in this example, it means that directors and deceased actors are searched
  ;; across all movies, not just movies with sequels.

  (def find-either-directors-or-deceased-actors-1
    '[:find ?name
      :where
      [?p :person/name ?name]
      ;; will restrict results if unification of `?m` between outer where clause
      ;; and or clause was allowed
      [?m :movie/sequel]
      (or-join [?p]
               [?m :movie/director ?p]
               (and [?m :movie/cast ?p]
                    [?p :person/death]))])

  (count (d/q find-directors @conn))
  (count (d/q find-deceased-actors @conn))
  ;; should be the sum of the two previous
  (count (d/q
          find-either-directors-or-deceased-actors-1 @conn))

  ;; how or clauses work

  ;; one can imagine or clauses turn into an invocation of an anonymous rule
  ;; whose predicates comprise the or clauses. As with rules, src-vars are not
  ;; currently supported within the clauses of or, but are supported on the or
  ;; clause as a whole at top level.


  ;; expression clauses

  ;; expression clauses allow arbitrary functions to be used inside of Datalog
  ;; queries. Any functions or methods you use in expression clauses must be
  ;; pure, i.e. they must be free of side effects and always return the same
  ;; thing given the same arguments. Expression clauses have one of two basic
  ;; shapes:

  ;; - `[(predicate ...)]`
  ;; [(function ...) bindings]'

  ;; the first item in an expression clause is a list designating a function or
  ;; method call.


  ;; predicate expressions

  ;; if no bindings are provided, the function is presumed to be a predicate
  ;; returning a truth value: null and false are treated as false, anything else
  ;; is treated as true.

  ;; in the example below, the built-in expression predicate `<` limits the
  ;; results to movies released before 1980:

  (def find-old-movies
    '[:find ?title
      :where
      [?m :movie/title ?title]
      [?m :movie/year ?year]
      [(< ?year 1980)]])

  (d/q find-old-movies @conn)


  ;; function expressions

  ;; functions behave similarly, except that their return values are used not as
  ;; predicates, but to bind other variables. In the example below, the built-in
  ;; expression function `-` converts movie release years in their ages:

  (def find-movie-ages
    '[:find ?title ?age
      :in $ ?now
      :where
      [?m :movie/title ?title]
      [?m :movie/year ?year]
      [(- ?now ?year) ?age]])

  (d/q find-movie-ages @conn 2023)

  ;; expression clauses do not nest. Instead, multi-step calculations must be
  ;; performed with separate expressions:

  (def find-movie-ages-as-strings
    '[:find ?title ?age-str
      :in $ ?now
      :where
      [?m :movie/title ?title]
      [?m :movie/year ?year]
      [(- ?now ?year) ?age]
      [(str ?age) ?age-str]])

  (d/q find-movie-ages-as-strings @conn 2023)


  ;; built-in expression functions and predicates

  ;; Datomic provides the following built-in expression functions and
  ;; predicates (different set of functions may be available in Datahike):

  ;; - two argument comparison predicates `=`, `!=`, `<`, `<=`, `>`, and `>=`.
  ;; - two-argument mathematical operators `+`, `-`, `*`, and `/`.
  ;;   - Datomic's `/` operator works similar to Clojure's `/` in terms
  ;;     of promotion and contagion with a notable exception: Datomic's `/`
  ;;     does not return a `clojure.lang.Ratio` to callers. Instead, it
  ;;     returns a quotient as per `quot`.
  ;; - all of the functions from the clojure.core namespace of Clojure,
  ;;   except `eval`.
  ;; - a set of functions and predicates that are aware of Datomic data
  ;;   structures, documented below.

  ;; `get-else`

  ;; `[(get-else src-var ent attr default) ?val-or-default]`

  ;; takes a database, an entity identifier, a cardinality-one attribute, and a
  ;; default value. It returns that entity's value for the attribute, or the
  ;; default value if entity does not have a value.

  ;; the query below reports "N/A" whenever a movie does not have trivia:

  (def find-movies-with-trivia
    '[:find ?title ?trivia
      :in $
      :where
      [?m :movie/title ?title]
      [(get-else $ ?m :trivia "N/A") ?trivia]])

  (d/q find-movies-with-trivia @conn)

  ;; `get-some`

  ;; `[(get-some src-var ent attr+) [?attr ?val]]`

  ;; takes a database, an entity identifier, and one or more cardinality-one
  ;; attributes, returning a tuple of the entity id and value for the first
  ;; attribute possessed by the entity.

  ;; the query below tries to find a `:trivia` for a movie, and then falls back
  ;; to `:movie/title`:

  (def find-movies-with-trivia-1
    '[:find ?title ?trivia
      :in $
      :where
      [?m :movie/title ?title]
      [(get-some $ ?m :trivia :movie/title) ?trivia]])

  (d/q find-movies-with-trivia-1 @conn)

  ;; `ground`

  ;; `[(ground const) binding]`

  ;; takes a single argument, which must be a constant, and returns that same
  ;; argument. Programs that know information at query time should prefer ground
  ;; over e.g. identity, as the former can be used inside the query engine to
  ;; enable optimizations.

  (def find-movies-by-years
    '[:find ?title
      :where
      [?m :movie/title ?title]
      [?m :movie/year ?year]
      [(ground [1979 1980 1981]) [?year ...]]])

  (d/q find-movies-by-years @conn)

  ;; `fulltext`

  ;; `[(fulltext src-var attr search) [[?ent ?val ?tx ?score]]]`

  ;; takes a database, an attribute, and a search expression, and returns a
  ;; relation of four-tuples: entity, value, transaction, and score.

  ;; the following query finds all the movies whose names include "The":

  ;; seems like it is not implemented in Datahike
  (def find-movies-by-partial-name
    '[:find ?name
      :in $ ?pattern
      :where
      [(fulltext $ :movie/title ?pattern) [?entity ?name ?tx ?score]]])

  (d/q find-movies-by-partial-name @conn "The")

  ;; `missing?`

  ;; `[(missing? src-var ent attr)]`

  ;; takes a database, entity, and attribute, and returns true if the entity has
  ;; no value for attribute in the database.

  ;; the following query finds all directors who are still alive.

  (def find-directors-still-alive
    '[:find ?name
      :in $
      :where
      [?p :person/name ?name]
      [_ :movie/director ?p]
      [(missing? $ ?p :person/death)]])

  (d/q find-directors-still-alive @conn)

  ;; `tuple`

  ;; `[(tuple ?a ...) ?tup]`

  ;; given one or more values, returns a tuple containing each value. See also
  ;; `untuple`.

  (def find-a-tuple
    '[:find ?t
      :in ?a ?b
      :where
      [(tuple ?a ?b) ?t]])

  (d/q find-a-tuple 1 2)

  ;; `tx-ids`

  ;; `[(tx-ids ?log ?start ?end) [?tx ...]]`

  ;; given a database log, start, and end, tx-ids returns a collection of
  ;; transaction ids. Start and end can be specified as database t, transaction
  ;; id, or instant in time, and can be nil.

  ;; the following query finds transactions from time t 1000 through 1050:

  ;; seems like it is not implemented in Datahike, and so is db.log
  (def find-transactions
    '[:find [?tx ...]
      :in ?db-log
      :where
      [(tx-ids ?db-log 1000 1050) [?tx ...]]])

  (d/q find-transactions @conn)

  ;; `tx-data`

  ;; `[(tx-data ?log ?tx) [[?e ?a ?v _ ?op]]]`

  ;; given a database log and a transaction id, returns a collection of the
  ;; datoms added by that transaction. You should not bind the transaction
  ;; position of the result, as the transaction is already bound on input.

  ;; the following query finds the entities referenced by transaction id:

  ;; seems like it is not implemented in Datahike, and so is db.log
  (def find-entities-by-transactions
    '[:find [?e ...]
      :in ?db-log ?tx
      :where
      [(tx-data ?db-log ?tx) [[?e]]]])

  (d/q find-entities-by-transactions @conn)

  ;; `untuple`

  ;; `[(untuple ?tup) [?a ?b]]`

  ;; given a tuple, can be used to name each element of the tuple. See also
  ;; `tuple`.

  (def find-scalars
    '[:find ?b
      :in ?t
      :where
      [(untuple ?t) [?a ?b]]])

  (d/q find-scalars [1 2])


  ;; calling Java methods

  ;; Java methods can be used as query expression functions and predicates, and
  ;; can be type hinted for performance. Java code used in this way must be on
  ;; the Java process classpath.


  ;; calling static methods

  ;; Java static methods can be called with the `(ClassName/methodName ...)`
  ;; form. For example, the following code calls `System.getProperties`, binding
  ;; property names to `?k` and property values to `?v`.

  ;; seems like it is not working in Datahike
  (def find-properties
    '[:find ?k ?v
      :where
      [(System/getProperties) [[?k ?v]]]])

  (d/q find-properties)


  ;; calling instance methods

  ;; Java instance methods can be called with the `(.methodName obj ...)`
  ;; form. For example, the following code calls `String.endsWith`:

  (def find-strings
    '[:find ?s
      :in [?s ...]
      :where
      [(.endsWith ?s "!")]])

  (d/q find-strings ["one" "two" "three!" "four!!" "five!!!"])


  ;; type hinting for performance

  ;; the current version of Datomic performs reflective lookup for Java
  ;; interop. You can significantly improve performance by type hinting
  ;; objects, allowing the query engine to make direct method invocations. Type
  ;; hints take the form of `^ClassName` preceding an argument, so the previous
  ;; example becomes

  (def find-strings-no-reflection
    '[:find ?s
      :in [?s ...]
      :where
      [(.endsWith ^String ?s "!")]])

  (set! *warn-on-reflection* true)

  ;; seems like in Datahike this do not use reflection anyway
  (d/q find-strings ["one" "two" "three!" "four!!" "five!!!"])
  (d/q find-strings-no-reflection ["one" "two" "three!" "four!!" "five!!!"])

  ;; note that type hints outside `java.lang` will need to be fully qualified,
  ;; and that complex method signatures may require more than one hint to be
  ;; unambiguous.


  ;; calling Clojure functions

  ;; Clojure functions can be used as query expression functions and
  ;; predicates. Clojure code used in this way must be on the Clojure process
  ;; classpath. The example below uses subs as an expression function to
  ;; extract prefixes of words:

  (def find-prefixes
    '[:find [?prefix ...]
      :in [?word ...]
      :where [(subs ?word 0 5) ?prefix]])

  (d/q find-prefixes ["hello" "antidisestablishmentarianism"])


  ;; the implicit data source: `$`

  ;; often you will have only a single, or primary, data source (usually a
  ;; database). In this case you can call that data source `$`, and elide it in
  ;; the data clauses:

  ;; `[:find ?e :in $ ?age :where [?e :age ?age]]`

  ;; same as

  ;; `[:find ?e :in $data ?age :where [$data ?e :age ?age]]`


  ;; rules

  ;; Datomic datalog allows you to package up sets of where clauses into named
  ;; *rules*. These rules make query logic reusable, and also composable,
  ;; meaning that you can bind portions of a query's logic at query time.

  ;; a rule is a named group of clauses that can be plugged into the `:where`
  ;; section of your query. For example, here is a rule that checks if a person
  ;; was deceased:

  (def deceased-rule
    '[(deceased ?p)
      [?p :person/death]])

  (def find-deceased-actors
    '[:find ?name
      :in $ %
      :where
      [?p :person/name ?name]
      [_ :movie/cast ?p]
      (deceased ?p)])

  (d/q find-deceased-actors @conn [deceased-rule])

  ;; as with transactions and queries, rules are described using data
  ;; structures. A rule is a list of lists. The first list in the rule is the
  ;; head. It names the rule and specifies its parameters. The rest of the
  ;; lists are clauses that make up the body of the rule. In this rule, the
  ;; name is "deceased", the variable `?p` is an input argument, and the body
  ;; is data clauses testing whether the person `?p` is deceased.

  ;; if rule has no output argument, then it is a predicate rule that will
  ;; evaluate to true or false, indicating whether its input matches the
  ;; specified criteria.

  ;; However, rules with more than one argument can be used to bind output
  ;; variables that can be subsequently used elsewhere in the query.

  (def actor-rule
    '[(actor ?m ?p)
      [?m :movie/cast ?p]])

  (def deceased-rule
    '[(deceased ?p)
      [?p :person/death]])

  (def find-deceased-actors-1
    '[:find ?name
      :in $ %
      :where
      [?p :person/name ?name]
      (actor _ ?p)
      (deceased ?p)])

  (d/q find-deceased-actors-1
       @conn [actor-rule
              deceased-rule])

  ;; in the `actor` rule above, we could bind either `?m` or `?p` at invocation
  ;; time, and the other variable would be bound to the output of the rule.

  ;; we can require that variables need binding at invocation time by enclosing
  ;; the required variables in a vector or list as the first argument to the
  ;; rule. If the required variables are not bound, an exception will be
  ;; thrown. The next example rewrites the previous rule to require `?m`:

  ;; note: this seems not to work as described in Datahike

  (def movie-actor-rule
    '[(actor [?m] ?p)
      [?m :movie/cast ?p]])

  (def find-movie-actors
    '[:find ?name
      :in $ % ?title
      :where
      [?m :movie/title ?title]
      [?p :person/name ?name]
      (actor ?m ?p)])

  (d/q find-movie-actors
       @conn
       [movie-actor-rule]
       "Mad Max")

  (def find-actor-movies
    '[:find ?title
      :in $ % ?name
      :where
      [?m :movie/title ?title]
      [?p :person/name ?name]
      (actor ?m ?p)])

  (d/q find-actor-movies
       @conn
       [movie-actor-rule]
       "Mel Gibson")

  ;; individual rule definitions are combined into a set of rules. A set of
  ;; rules is simply another list containing some number of rule definitions:

  (def actor-rules
    '[[(actor ?m ?p)
       [?m :movie/cast ?p]]
      [(deceased ?p)
       [?p :person/death]]])

  (def find-deceased-actors-2
    '[:find ?name
      :in $ %
      :where
      [?p :person/name ?name]
      (actor _ ?p)
      (deceased ?p)])

  (d/q find-deceased-actors-2
       @conn
       actor-rules)

  ;; you have to do two things to use a rule set in a query. First, you have to
  ;; pass the rule set as an input source and reference it in the :in section
  ;; of your query using the '%' symbol. Second, you have to invoke one or more
  ;; rules from the :where section of your query. You do this by adding a rule
  ;; invocation clause.

  ;; a rule invocation is a list containing a rule-name and one or more
  ;; arguments, either variables or constants, as defined in the rule
  ;; head. It's idiomatic to use parenthesis instead of square brackets to
  ;; represent a rule invocation in literal form, because it makes it easier to
  ;; differentiate from a data clause. However, this is not a requirement.

  ;; As with other where clauses, you may specify a database before the
  ;; rule-name to scope the rule to that database. Databases cannot be used as
  ;; arguments in a rule.

  ;; rules with multiple definitions will evaluate them as different logical
  ;; paths to the same conclusion (i.e. logical OR).

  ;; For example, here is a rule that finds sequels of a movie, including
  ;; indirect ones, i.e., sequels of sequels:

  (def sequel-rules
    '[[(sequels ?m1 ?m2)
       [?m1 :movie/sequel ?m2]]
      [(sequels ?m1 ?m2)
       [?m3 :movie/sequel ?m2]
       (sequels ?m1 ?m3)]])

  (def find-sequels
    '[:find ?sequel
      :in $ % ?title
      :where
      [?m :movie/title ?title]
      (sequels ?m ?s)
      [?s :movie/title ?sequel]])

  (def find-prequels
    '[:find ?prequel
      :in $ % ?title
      :where
      [?m :movie/title ?title]
      (sequels ?p ?m)
      [?p :movie/title ?prequel]])

  (d/q find-sequels
       @conn
       sequel-rules "Mad Max")

  (d/q find-prequels
       @conn
       sequel-rules "Mad Max 2")


  ;; the "sequels" rule has two definitions, one testing whether a second movie
  ;; is a direct sequel of the first, and another testing if there is a third
  ;; movie that is sequel of the first and whose sequel is the second. When a
  ;; given pair of movies is tested, the rule will be true if either of the
  ;; definitions is true. In other words, using rules, we can implement logical
  ;; OR in queries.

  ;; in all the examples above, the body of each rule is made up solely of data
  ;; clauses. However, rules can contain any type of clause: data, expression,
  ;; or even other rule invocations.


  ;; aggregates

  ;; Datomic's aggregate syntax is incorporated in the find clause. The list
  ;; expressions are aggregate expressions. Query variables not in aggregate
  ;; expressions will group the results and appear intact in the result. Thus,
  ;; the query below binds `?a` `?b` `?c` `?d`, then groups by `?a` and `?c`,
  ;; and produces a result for each aggregate expression for each group,
  ;; yielding 5-tuples.

  (def find-aggregate
    '[:find ?a (min ?b) (max ?b) ?c (sample 3 ?d) (count ?d)
      :in [[?a ?b ?c ?d]]])

  (d/q find-aggregate [[1 2 3 4]
                       [1 2 3 7]
                       [0 2 3 5]])

  ;; control grouping via `:with`

  ;; unless otherwise specified, Datomic's datalog returns sets, and you will
  ;; not see duplicate values. This is often undesirable when producing
  ;; aggregates. Consider the following query, which attempts to return the
  ;; total number of heads possessed by a set of mythological monsters:

  (def find-head-count
    '[:find (sum ?heads) .
      :in [[_ ?heads]]])

  (def monsters-heads
    [["Cerberus" 3]
     ["Medusa" 1]
     ["Cyclops" 1]
     ["Chimera" 1]])

  (d/q find-head-count monsters-heads)

  ;; the monsters clearly have six total heads, but set logic coalesces Medusa,
  ;; the Cyclops, and the Chimera together, since each has one head.

  ;; the solution to this problem is the `:with` clause, which considers
  ;; additional variables when forming the basis set for the query result. The
  ;; `:with` variables are then removed, leaving a bag (not a set!) of values
  ;; available for aggregation.

  (def find-head-count-with
    '[:find (sum ?heads) .
      :with ?monster
      :in [[?monster ?heads]]])

  (d/q find-head-count-with monsters-heads)


  ;; aggregates returning a single value (may not be all available in Datahike)

  (def xs [1 1 1 13 18 27 42 45 46 48 73 98])
  (def xs-indexed (mapv vector (range) xs))

  ;; `(min ?xs)`

  (d/q '[:find (min ?xs)
         :in [?xs ...]] xs)

  ;; `(max ?xs)`

  (d/q '[:find (max ?xs)
         :in [?xs ...]] xs)

  ;; min and max support all database types (via comparators), not just numbers.

  ;; `(count ?xs)`

  (d/q '[:find (count ?xs)
         :with ?idx
         :in [[?idx ?xs]]]
       xs-indexed)

  ;; note the use of `:with` so that equal elements do not coalesce.

  ;; `(count-distinct ?xs)`

  (d/q '[:find (count-distinct ?xs)
         :in [?xs ...]]
       xs)

  ;; will count distinct values regardless of `:with`

  ;; `(sum ?xs)`

  (d/q '[:find (sum ?xs)
         :in [?xs ...]] xs)

  ;; `(avg ?xs)`

  (d/q '[:find (avg ?xs)
         :in [?xs ...]] (map double xs))

  ;; `(median ?xs)`

  (d/q '[:find (median ?xs)
         :in [?xs ...]] (map double xs))

  ;; `(variance ?xs)`

  (d/q '[:find (variance ?xs)
         :in [?xs ...]] (map double xs))

  ;; `(stddev ?xs)`

  (d/q '[:find (stddev ?xs)
         :in [?xs ...]] (map double xs))

  ;; calculate statistics of the movie title length by year

  (d/q '[:find ?year (median ?len) (avg ?len) (stddev ?len)
         :with ?title
         :keys year median avg stddev
         :where
         [?m :movie/title ?title]
         [?m :movie/year ?year]
         [(count ?title) ?len]]
       @conn)

  ;; aggregates returning collections

  (def xs [1 1 1 13 18 27 42 45 46 48 73 98])
  (def xs-indexed (mapv vector (range) xs))

  ;; `(distinct ?xs)`

  (d/q '[:find (distinct ?xs)
         :in [?xs ...]] xs)

  ;; `(min n ?xs)`

  (d/q '[:find (min 3 ?xs)
         :in [?xs ...]] xs)

  ;; `(max n ?xs)`

  (d/q '[:find (max 3 ?xs)
         :in [?xs ...]] xs)

  ;; `(rand n ?xs)`: exactly `n`, with possible duplicates

  (d/q '[:find (rand 3 ?xs)
         :in [?xs ...]] xs)

  ;; `(sample n ?xs)`: up to `n`, without duplicates

  (d/q '[:find (sample 3 ?xs)
         :in [?xs ...]] xs)

  ;; where `n` is specified, fewer than `n` items may be returned if not enough
  ;; items are available.


  ;; custom aggregates

  ;; you may call an arbitrary Clojure function as an aggregation function as
  ;; follows:

  ;; - use the fully qualified name of the function.
  ;; - load the namespace before using the function.
  ;; - the one and only aggregated variable must be the last argument to
  ;;   the function.
  ;; - other arguments to the function must be constants in the query.

  ;; the aggregated variable will be passed as a partial implementation of
  ;; `java.util.List` - only `size()`, `iterator()`, and `get(i)` are
  ;; implemented.

  ;; for example, you might implement your own mode function to calculate the
  ;; mode as follows:

  (defn mode
  [vals]
  (->> (frequencies vals)
       (sort-by (comp - second))
       ffirst))

  ;; with mode in hand, you can answer the question "what is the most common
  ;; movie title length?"

  (def find-movie-title-mode
    '[:find (hello-datahike.query/mode ?len) .
      :with ?m
      :where
      [?m :movie/title ?title]
      [(count ?title) ?len]])

  (d/q find-movie-title-mode @conn)


  ;; pull expressions

  ;; pull expressions can used in a `:find` clause. See `pull.clj` for
  ;; description and more examples.

  (def pull-movies-from-1979
    '[:find (pull ?m [:movie/title
                      {:movie/director [:person/name]}])
      :where
      [?m :movie/year 1979]])

  (d/q pull-movies-from-1979 @conn)

  ;; a pull expression can only be applied to any specific entity var `?e` a
  ;; single time.


  ;; timeouts

  ;; probably do not work in Datahike (yet?)

  ;; you can configure a query to abort if it takes too long to run using
  ;; Datomic's timeout functionality. Note: timeout is approximate. It is meant
  ;; to protect against long running queries, but is not guaranteed to stop
  ;; after precisely the duration specified.

  ;; in Java, do this by building a `QueryRequest` object with a timeout and
  ;; passing it to `Peer.query`.

  ;; CopyQueryRequest qr = QueryRequest.create(query, inputs...).timeout(1000);
  ;; Peer.query(qr);

  ;; here, we are creating a `QueryRequest` by calling `QueryRequest.create` and
  ;; passing a query and inputs as described in `Peer.query`. Then, on the
  ;; `QueryRequest` object, call timeout passing `timeoutMsec` in milliseconds.

  ;; in Clojure, do this by passing a query-map to the new query function.

  ;; (d/query {:query query :args args :timeout timeout-in-milliseconds})

  ;; here, we are passing a map where query is in the same format as in `q`,
  ;; args is in the same format as inputs in `q`, and an optional timeout in
  ;; milliseconds.

)
