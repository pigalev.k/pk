(ns hello-datahike.datalog
  (:require
   [datahike.api :as d]
   [hello-datahike.common :as c]))


;; Datalog tutorial (Datomic dialect)

;; https://www.learndatalogtoday.org/
;; https://github.com/jonase/learndatalogtoday


;; prepare the db (in-memory)

(comment

  (def cfg {:store              {:backend :mem
                                 :id      "datalog-tutorial"}
            :schema-flexibility :write
            :keep-history?      true
            :attribute-refs?    true})

  ;; create the db and connect to it
  (def conn (c/connect! cfg))

  ;; there are 20 movies in the db now
  (count (d/q '[:find ?title
                :where
                [_ :movie/title ?title]]
              @conn))

)


;; 1. basic queries

(comment

  ;; the data model in Datomic is based around atomic facts called *datoms*. A
  ;; *datom* is a 5-tuple consisting of
  ;; - Entity ID
  ;; - Attribute
  ;; - Value
  ;; - Transaction ID
  ;; - Operation (assertion or retraction)

  (def find-all-facts
    '[:find ?e ?a ?v ?t ?op
      :where
      [?e ?a ?v ?t ?op]])

  (first (d/q find-all-facts @conn))

  ;; you can think of the database as a flat set of datoms of the form

  ;; [<e-id>  <attribute>      <value>          <tx-id> <operation>]
  ;; ...
  ;; [ 167    :person/name     "James Cameron"  102     true       ]
  ;; [ 234    :movie/title     "Die Hard"       102     true       ]
  ;; [ 234    :movie/year      1987             102     true       ]
  ;; [ 235    :movie/title     "Terminator"     102     true       ]
  ;; [ 235    :movie/director  167              102     true       ]

  ;; note that the last two datoms share the same entity ID, which means they
  ;; are facts about the same movie. Note also that the last datom's value is
  ;; the same as the first datom's entity ID, i.e. the value of the
  ;; `:movie/director` attribute is itself an entity. All the datoms in the
  ;; above set were added to the database in the same transaction, so they share
  ;; the same transaction ID.

  ;; there are two possible operations: *assertion* and
  ;; *retraction* (represented as `true` and `false` correspondingly).

  ;; a query is represented as a vector starting with the keyword `:find`
  ;; followed by one or more *pattern variables* (symbols starting with `?`,
  ;; e.g. `?title`). After the find clause comes the `:where` clause which
  ;; restricts the query to datoms that match the given *data patterns*.

  ;; for example, this query finds all entity-ids that have the attribute
  ;; `:person/name` with a value of "Ridley Scott":

  (def find-ridley-scott
    '[:find ?e
     :where
     [?e :person/name "Ridley Scott"]])

  (d/q find-ridley-scott @conn)

  ;; a data pattern is a datom with some parts replaced with pattern
  ;; variables. It is the job of the query engine to figure out every possible
  ;; value of each of the pattern variables and return the ones that are
  ;; specified in the `:find` clause.

  ;; the symbol `_` can be used as a wildcard for the parts of the data pattern
  ;; that you wish to ignore. You can also elide trailing values in a data
  ;; pattern. Therefore, the previous query is equivalent to this next query,
  ;; because we ignore the transaction and operation parts of the datoms.

  (def find-ridley-scott-full
    '[:find ?e
     :where
     [?e :person/name "Ridley Scott" _ _]])

  (d/q find-ridley-scott-full @conn)

  ;; actually we do not need any database at all --- queries will work on any
  ;; sets of tuples (supposedly of arbitrary arity):

  (def box-office-earnings
    [["Die Hard" 140700000]
     ["Alien" 104931801]
     ["Lethal Weapon" 120207127]
     ["Commando" 57491000]])

  (def movie-ratings
    [["Die Hard" 8.3]
     ["Alien" 8.5]
     ["Lethal Weapon" 7.6]
     ["Commando" 6.5]
     ["Mad Max Beyond Thunderdome" 6.1]
     ["Mad Max 2" 7.6]
     ["Rambo: First Blood Part II" 6.2]
     ["Braveheart" 8.4]
     ["Terminator 2: Judgment Day" 8.6]
     ["Predator 2" 6.1]
     ["First Blood" 7.6]
     ["Aliens" 8.5]
     ["Terminator 3: Rise of the Machines" 6.4]
     ["Rambo III" 5.4]
     ["Mad Max" 7.0]
     ["The Terminator" 8.1]
     ["Lethal Weapon 2" 7.1]
     ["Predator" 7.8]
     ["Lethal Weapon 3" 6.6]
     ["RoboCop" 7.5]])

  ;; arbitrary relations as regular (relation) inputs
  (d/q '[:find ?title1 ?rating ?earning
         :in [[?title1 ?earning]] [[?title2 ?rating]]
         :where
         [(= ?title1 ?title2)]]
       box-office-earnings
       movie-ratings)

  ;; arbitrary relations as databases
  (d/q '[:find ?title1 ?rating ?earning
         :in $earnings $ratings
         :where
         [$earnings ?title1 ?earning]
         [$ratings ?title2 ?rating]
         [(= ?title1 ?title2)]]
       box-office-earnings
       movie-ratings)


  ;; exercises

  ;; 1. find the entity ids of movies made in 1987

  (d/q '[:find ?m
         :where
         [?m :movie/year 1987]]
       @conn)

  ;; 2. find the entity-id and titles of movies in the database

  (d/q '[:find ?m ?title
         :where
         [?m :movie/title ?title]]
       @conn)

  ;; 3. find the names of all people in the database

  (d/q '[:find ?name
         :where
         [_ :person/name ?name]]
       @conn)

)


;; 2. data patterns

(comment

  ;; there can be many data patterns in a `:where` clause:

  (def find-1987
    '[:find ?title
      :where
      [?m :movie/year 1987]
      [?m :movie/title ?title]])

  ;; the important thing to note here is that the pattern variable `?e` is used
  ;; in both data patterns. When a pattern variable is used in multiple places,
  ;; the query engine requires it to be bound to the same value in each
  ;; place. Therefore, this query will only find movie titles for movies made in
  ;; 1987.

  (d/q find-1987 @conn)

  ;; the order of the data patterns does not matter (aside from performance
  ;; considerations --- to minimize the amount of work, most restrictive,
  ;; narrowing patterns should come first; each clause should join along at
  ;; least one variable that has been bound in the preceding clauses).

  ;; let's say we want to find out who starred in "Lethal Weapon". We will need
  ;; three data patterns for this. The first one finds the entity ID of the
  ;; movie with "Lethal Weapon" as the title, the second finds the cast members
  ;; of that movie, and a third finds names of these cast members:


  (def find-lethal-weapon-cast-names
    '[:find ?name
      :where
      [?m :movie/title "Lethal Weapon"]
      [?m :movie/cast ?p]
      [?p :person/name ?name]])

  (d/q find-lethal-weapon-cast-names @conn)


  ;; exercises

  ;; 1. Find movie titles made in 1985

  ;; note: `_` is not acceptable instead of `?e` --- will represent any entity
  ;; ID

  (d/q '[:find ?title
         :where
         [?m :movie/title ?title]
         [?m :movie/year 1985]]
       @conn)

  ;; 2. what year was "Alien" released?

  (d/q '[:find ?year
         :where
         [?m :movie/year ?year]
         [?m :movie/title "Alien"]]
       @conn)

  ;; 3. who directed RoboCop?

  (d/q '[:find ?name
         :where
         [?m :movie/title "RoboCop"]
         [?m :movie/director ?p]
         [?p :person/name ?name]]
       @conn)

  ;; 4. find directors who have directed Arnold Schwarzenegger in a movie.

  (d/q '[:find ?name
         :where
         [?p :person/name "Arnold Schwarzenegger"]
         [?m :movie/cast ?p]
         [?m :movie/director ?d]
         [?d :person/name ?name]]
       @conn)

)


;; 3. parameterized queries

(comment

  ;; looking at the query like this

  ;; '[:find ?title
  ;;   :where
  ;;   [?p :person/name "Sylvester Stallone"]
  ;;   [?m :movie/cast ?p]
  ;;   [?m :movie/title ?title]]

  ;; it would be great if we could reuse this query to find movie titles for any
  ;; actor and not just for "Sylvester Stallone". This is possible with an `:in`
  ;; clause, which provides the query with input parameters, much in the same
  ;; way that function or method arguments does in your programming language.

  ;; here's that query with an input parameter for the actor:

  (def find-movies-by-actor
    '[:find ?title
      :in $ ?name
      :where
      [?p :person/name ?name]
      [?m :movie/cast ?p]
      [?m :movie/title ?title]])

  ;; this query takes two arguments: `$` is the database itself (implicit, if no
  ;; `:in` clause is specified) and `?name`, which presumably will be the name
  ;; of some actor.

  ;; the above query is executed like `(d/q query db "Sylvester Stallone")`,
  ;; where `query` is the query we just saw, and `db` is a database value. You
  ;; can have any number of inputs to a query.

  ;; in the above query, the input pattern variable `?name` is bound to a
  ;; *scalar* --- a string in this case. There are four different kinds of
  ;; input: scalars, tuples, collections and relations.

  (d/q find-movies-by-actor
       @conn
       "Sylvester Stallone")

  ;; the `$` parameter is a database value. A data pattern is actually datom
  ;; with the implicit $ prepended, that can be made explicit:

  (def find-movies-by-actor-explicit-db
    '[:find ?title
      :in $ ?name
      :where
      [$ ?p :person/name ?name]
      [$ ?m :movie/cast ?p]
      [$ ?m :movie/title ?title]])

  (d/q find-movies-by-actor-explicit-db
       @conn
       "Sylvester Stallone")

  ;; if we pass no database value as a parameter to query, it will never find
  ;; anything

  (d/q find-movies-by-actor)
  (d/q find-movies-by-actor-explicit-db)


  ;; tuples

  ;; a tuple input is written as e.g. `[?name ?age]` and can be used when you
  ;; want to destructure an input. Let's say you have the vector `["James
  ;; Cameron" "Arnold Schwarzenegger"]` and you want to use this as input to
  ;; find all movies where these two people collaborated:

  (def find-movies-by-director-and-actor-tuple
    '[:find ?title
      :in $ [?director ?actor]
      :where
      [?d :person/name ?director]
      [?a :person/name ?actor]
      [?m :movie/director ?d]
      [?m :movie/cast ?a]
      [?m :movie/title ?title]])

  (d/q find-movies-by-director-and-actor-tuple
       @conn
       ["James Cameron" "Arnold Schwarzenegger"])

  ;; of course, in this case, you could just as well use two distinct inputs
  ;; instead:

  (def find-movies-by-director-and-actor
    '[:find ?title
      :in $ ?director ?actor
      :where
      [?d :person/name ?director]
      [?a :person/name ?actor]
      [?m :movie/director ?d]
      [?m :movie/cast ?a]
      [?m :movie/title ?title]])

  (d/q find-movies-by-director-and-actor
       @conn
       "James Cameron"
       "Arnold Schwarzenegger")


  ;; collections

  ;; you can use collection destructuring to implement a kind of logical `or` in
  ;; your query. Say you want to find all movies directed by either James
  ;; Cameron or Ridley Scott:

  (def find-movies-by-some-director
    '[:find ?title
      :in $ [?director ...]
      :where
      [?p :person/name ?director]
      [?m :movie/director ?p]
      [?m :movie/title ?title]])

  (d/q find-movies-by-some-director
       @conn
       ["James Cameron" "Ridley Scott"])

  ;; here, the `?director` pattern variable is initially bound to both "James
  ;; Cameron" and "Ridley Scott". Note that the ellipsis following `?director`
  ;; is a literal, not elided code.


  ;; relations

  ;; relations --- sets of tuples --- are the most interesting and powerful of
  ;; input types, since you can join external relations with the datoms in your
  ;; database.

  ;; as a simple example, let's consider a relation with tuples `[movie-title
  ;; box-office-earnings]`:

  (def box-office-earnings
    [["Die Hard" 140700000]
     ["Alien" 104931801]
     ["Lethal Weapon" 120207127]
     ["Commando" 57491000]])

  ;; let's use this data and the data in our database to find box office
  ;; earnings for a particular director:

  (def find-movie-earnings-by-director
    '[:find ?title ?box-office
      :in $ ?director [[?title ?box-office]]
      :where
      [?p :person/name ?director]
      [?m :movie/director ?p]
      [?m :movie/title ?title]])

  (d/q find-movie-earnings-by-director
       @conn
       "Ridley Scott"
       box-office-earnings)

  ;; note that the `?box-office` pattern variable does not appear in any of the
  ;; data patterns in the `:where` clause.


  ;; exercises

  ;; 1. find movie title by year.

  (d/q '[:find ?title
         :in $ ?year
         :where
         [?m :movie/title ?title]
         [?m :movie/year ?year]]
       @conn
       1988)

  ;; 2. given a list of movie titles, find the title and the year that movie was
  ;; released.

  (d/q '[:find ?title ?year
         :in $ [?title ...]
         :where
         [?m :movie/title ?title]
         [?m :movie/year ?year]]
       @conn
       ["Lethal Weapon" "Lethal Weapon 2" "Lethal Weapon 3"])

  ;; 3. find all movie titles where the actor and the director has worked
  ;; together

  (d/q '[:find ?title
         :in $ ?actor ?director
         :where
         [?m :movie/cast ?a]
         [?m :movie/director ?d]
         [?a :person/name ?actor]
         [?d :person/name ?director]
         [?m :movie/title ?title]]
       @conn
       "Michael Biehn"
       "James Cameron")

  ;; 4. write a query that, given an actor name and a relation with
  ;; movie-title/rating, finds the movie titles and corresponding rating for
  ;; which that actor was a cast member.

  (def movie-ratings
    [["Die Hard" 8.3]
     ["Alien" 8.5]
     ["Lethal Weapon" 7.6]
     ["Commando" 6.5]
     ["Mad Max Beyond Thunderdome" 6.1]
     ["Mad Max 2" 7.6]
     ["Rambo: First Blood Part II" 6.2]
     ["Braveheart" 8.4]
     ["Terminator 2: Judgment Day" 8.6]
     ["Predator 2" 6.1]
     ["First Blood" 7.6]
     ["Aliens" 8.5]
     ["Terminator 3: Rise of the Machines" 6.4]
     ["Rambo III" 5.4]
     ["Mad Max" 7.0]
     ["The Terminator" 8.1]
     ["Lethal Weapon 2" 7.1]
     ["Predator" 7.8]
     ["Lethal Weapon 3" 6.6]
     ["RoboCop" 7.5]])

  (d/q '[:find ?title ?rating
         :in $ ?name [[?title ?rating]]
         :where
         [?p :person/name ?name]
         [?m :movie/cast ?p]
         [?m :movie/title ?title]]
       @conn
       "Mel Gibson"
       movie-ratings)

)


;; 4. more queries

(comment

  ;; a datom, as described earlier, is the 5-tuple `[eid attr val tx op]`. So
  ;; far, we have only asked questions about values and/or entity-ids. It's
  ;; important to remember that it's also possible to ask questions about
  ;; attributes and transactions.


  ;; attributes

  ;; for example, say we want to find all attributes that are associated with
  ;; person entities in our database. We know for certain that `:person/name` is
  ;; one such attribute, but are there others we have not yet seen?

  (def find-attribute-ids-of-person
    '[:find ?attr
      :where
      [?p :person/name]
      [?p ?attr]])

  (d/q find-attribute-ids-of-person @conn)

  ;; the above query returns a set of entity ids referring to the attributes we
  ;; are interested in. To get the actual keywords we need to look them up using
  ;; the `:db/ident` attribute:

  (def find-attribute-names-of-person
    '[:find ?attr
      :where
      [?p :person/name]
      [?p ?a]
      [?a :db/ident ?attr]])

  (d/q find-attribute-names-of-person @conn)

  ;; this is because attributes are also entities in the database.


  ;; transactions

  ;; it's also possible to run queries to find information about transactions,
  ;; such as:

  ;; - when was a fact asserted?
  ;; - when was a fact retracted?
  ;; - which facts were part of a transaction?
  ;; - etc.

  ;; the transaction entity is the fourth element in the datom vector. The only
  ;; attribute associated with a transaction (by default) is `:db/txInstant`
  ;; which is the instant in time when the transaction was committed to the
  ;; database.

  ;; here's how we use the fourth element to find the time that "James Cameron"
  ;; was set as the name for that person entity:

  (def find-name-assertion-time-of-james-cameron
    '[:find ?timestamp
      :where
      [?p :person/name "James Cameron" ?tx]
      [?tx :db/txInstant ?timestamp]])

  (d/q find-name-assertion-time-of-james-cameron @conn)


  ;; exercises

  ;; 1. what attribute names are associated with a given movie?

  (d/q '[:find ?attr
         :in $ ?title
         :where
         [?m :movie/title ?title]
         [?m ?a]
         [?a :db/ident ?attr]]
       @conn
       "Commando")

  ;; 2. find the names of all people associated with a particular
  ;; movie (i.e. both the actors and the directors).

  (d/q '[:find ?name
         :in $ ?title [?attr ...]
         :where
         [?m :movie/title ?title]
         [?a :db/ident ?attr]
         [?p :person/name ?name]
         [?m ?a ?p]]
       @conn
       "Die Hard"
       [:movie/cast :movie/director])

  ;; 3. find all available attributes, their type and their cardinality. This is
  ;; essentially a query to find the schema of the database. To find all
  ;; installed attributes you must use the `:db.install/attribute`
  ;; attribute. You will also need to use the `:db/valueType` and
  ;; `:db/cardinality` attributes as well as `:db/ident`.

  ;; correct for Datomic, but not for Datahike, probably due to implementation
  ;; differences in attribute storage system
  #_(d/q '[:find ?attr ?type ?card
           :where
           [_ :db.install/attribute ?a]
           [?a :db/valueType ?t]
           [?a :db/cardinality ?c]
           [?a :db/ident ?attr]
           [?t :db/ident ?type]
           [?c :db/ident ?card]]
         @conn)

  ;; datahike solution (probably correct)
  (d/q '[:find ?attr ?type ?card
         :where
         [?i :db.install/_attribute]
         [?i :db/valueType ?t]
         [?i :db/cardinality ?c]
         [?i :db/ident ?attr]
         [?c :db/ident ?card]
         [?t :db/ident ?type]]
       @conn)

  ;; result
  #_#{[:movie/year :db.type/long :db.cardinality/one]
      [:db/cardinality :db.type/cardinality :db.cardinality/one]
      [:movie/sequel :db.type/ref :db.cardinality/one]
      [:trivia :db.type/string :db.cardinality/many]
      [:movie/title :db.type/string :db.cardinality/one]
      [:person/death :db.type/instant :db.cardinality/one]
      [:db/valueType :db.type/valueType :db.cardinality/one]
      [:movie/director :db.type/ref :db.cardinality/many]
      [:person/name :db.type/string :db.cardinality/one]
      [:db/doc :db.type/string :db.cardinality/one]
      [:db/txInstant :db.type/instant :db.cardinality/one]
      [:db.install/attribute :db.type.install/attribute :db.cardinality/one]
      [:db/unique :db.type/unique :db.cardinality/one]
      [:db/ident :db.type/keyword :db.cardinality/one]
      [:movie/cast :db.type/ref :db.cardinality/many]
      [:db/index :db.type/boolean :db.cardinality/one]
      [:person/born :db.type/instant :db.cardinality/one]
      [:db/noHistory :db.type/boolean :db.cardinality/one]}

  ;; 4. when was the seed data imported into the database? Grab the transaction
  ;; of any datom in the database, e.g., `[_ :movie/title _ ?tx]` and work from
  ;; there.

  (d/q '[:find ?inst
         :where
         [_ :movie/title _ ?tx]
         [?tx :db/txInstant ?inst]]
       @conn)

)


;; 5. predicates

(comment

  ;; so far, we have only been dealing with data patterns: `[?m :movie/year
  ;; ?year]`. We have not yet seen a proper way of handling questions like "Find
  ;; all movies released before 1984". This is where predicate clauses come into
  ;; play.

  ;; let's start with the query for the question above:
  (def find-movies-released-before-1984
    '[:find ?title
      :where
      [?m :movie/title ?title]
      [?m :movie/year ?year]
      [(< ?year 1984)]])

  (d/q find-movies-released-before-1984 @conn)

  ;; the last clause, `[(< ?year 1984)]`, is a *predicate clause*. The predicate
  ;; clause filters the result set to only include results for which the
  ;; predicate returns a "truthy" (non-nil, non-false) value. You can use any
  ;; Clojure function or Java method as a predicate function:

  (def find-persons-with-name-starting-with-M
    '[:find ?name
      :where
      [?p :person/name ?name]
      [(.startsWith ?name "M")]])

  (d/q find-persons-with-name-starting-with-M @conn)

  ;; clojure functions must be fully namespace-qualified, so if you have defined
  ;; your own predicate `awesome?` you must write it as `(my.namespace/awesome?
  ;; ?movie)`. Some ubiquitous predicates can be used without namespace
  ;; qualification: `<`, `>`, `<=`, `>=`, `=`, `not=` and so on.


  (defn >1990
    [x]
    (> 1990 x))

  ;; will throw if predicate is unqualified
  (d/q '[:find ?title
         :where
         [?m :movie/title ?title]
         [?m :movie/year ?year]
         [(hello-datahike.datalog/>1990 ?year)]]
       @conn)

  ;; seems to work too (autoqualifies symbols); not always though
  (d/q `[:find ?title
         :where
         [?m :movie/title ?title]
         [?m :movie/year ?year]
         [(>1990 ?year)]]
       @conn)

  (d/q '[:find ?title
         :where
         [?m :movie/title ?title]
         [(clojure.core/re-find #"er" ?title)]]
       @conn)


  ;; exercises

  ;; 1. find movies older than a certain year (inclusive).

  (d/q '[:find ?title
         :in $ ?year
         :where
         [?m :movie/title ?title]
         [?m :movie/year ?year]
         [(<= ?year 1979)]]
       @conn
       1979)

  ;; 2. find actors older than Danny Glover.

  (d/q '[:find ?actor
         :where
         [?p :person/name "Danny Glover"]
         [?p :person/born ?danny-born]
         [_ :movie/cast ?a]
         [?a :person/born ?actor-born]
         [?a :person/name ?actor]
         [(< ?actor-born ?danny-born)]]
       @conn)

  ;; 3. find movies newer than given year (inclusive) and has a rating higher
  ;; than the one supplied.

  (def movie-ratings
    [["Die Hard" 8.3]
     ["Alien" 8.5]
     ["Lethal Weapon" 7.6]
     ["Commando" 6.5]
     ["Mad Max Beyond Thunderdome" 6.1]
     ["Mad Max 2" 7.6]
     ["Rambo: First Blood Part II" 6.2]
     ["Braveheart" 8.4]
     ["Terminator 2: Judgment Day" 8.6]
     ["Predator 2" 6.1]
     ["First Blood" 7.6]
     ["Aliens" 8.5]
     ["Terminator 3: Rise of the Machines" 6.4]
     ["Rambo III" 5.4]
     ["Mad Max" 7.0]
     ["The Terminator" 8.1]
     ["Lethal Weapon 2" 7.1]
     ["Predator" 7.8]
     ["Lethal Weapon 3" 6.6]
     ["RoboCop" 7.5]])

  (d/q '[:find ?title
         :in $ ?min-year ?min-rating [[?title ?rating]]
         :where
         [?m :movie/title ?title]
         [?m :movie/year ?year]
         [(>= ?year ?min-year)]
         [(> ?rating ?min-rating)]]
       @conn
       1990
       8.0
       movie-ratings)

)


;; 6. transformation functions

(comment

  ;; *transformation functions* are pure (= side-effect free) functions or
  ;; methods which can be used in queries to transform values and bind their
  ;; results to pattern variables. Say, for example, there exists an attribute
  ;; `:person/born` with type `:db.type/instant`. Given the birthday, it's easy
  ;; to calculate the (very approximate) age of a person:

  (defn age
    "Approximate period in years between two dates."
    [birthday today]
    (quot (- (.getTime today)
             (.getTime birthday))
          (* 1000 60 60 24 365)))

  ;; with this function, we can now calculate the age of a person inside the
  ;; query itself (function name must also be fully qualified):

  ;; note: syntax-quoting is not working here; maybe it qualifies too many
  ;; symbols, perhaps $
  (def find-age-of-person
    '[:find ?age
      :in $ ?name ?today
      :where
      [?p :person/name ?name]
      [?p :person/born ?born]
      [(hello-datahike.datalog/age ?born ?today) ?age]])

  (def arnold "Arnold Schwarzenegger")
  (def today (Date.))

  (d/q find-age-of-person @conn arnold today)

  ;; a transformation function clause has the shape `[(<fn> <arg1> <arg2> ...)
  ;; <result-binding>]` where `<result-binding>` can be the same binding forms
  ;; as we saw in chapter 3:

  ;; - scalar: `?age`
  ;; - tuple: `[?foo ?bar ?baz]`
  ;; - collection: `[?name ...]`
  ;; - relation: `[[?title ?rating]]`

  ;; one thing to be aware of is that transformation functions can't be
  ;; nested. You can't write `[(f (g ?x)) ?a]`; instead, you must bind
  ;; intermediate results in temporary pattern variables:
  ;; '[(g ?x) ?t][(f ?t) ?a]`


  ;; exercises

  ;; 1. find people by age. Use the function `hello-datahike.datalog/age` to
  ;; find the age given a birthday and a date representing "today".

  (d/q '[:find ?name
         :in $ ?age ?today
         :where
         [?p :person/born ?born]
         [?p :person/name ?name]
         [(hello-datahike.datalog/age ?born ?today) ?age]]
       @conn
       63
       #inst "2013-08-02T00:00:00.000-00:00")

  ;; 2. find people younger than Bruce Willis and their ages.

  (d/q '[:find ?name ?age
         :in $ ?today
         :where
         [?bruce-id :person/name "Bruce Willis"]
         [?bruce-id :person/born ?bruce-born]
         [(hello-datahike.datalog/age ?bruce-born ?today) ?bruce-age]
         [?p :person/name ?name]
         [?p :person/born ?born]
         [(hello-datahike.datalog/age ?born ?today) ?age]
         [(< ?age ?bruce-age)]]
       @conn
       #inst "2013-08-02T00:00:00.000-00:00")

  ;; 3. the birthday paradox states that in a room of 23 people there is a 50%
  ;; chance that someone has the same birthday. Write a query to find who has
  ;; the same birthday. Use the `<` predicate on the names to avoid duplicate
  ;; answers. You can use (the deprecated) `.getDate` and `.getMonth` java Date
  ;; methods.

  (d/q '[:find ?name-1 ?name-2
         :where
         [?p1 :person/born ?born-1]
         [?p1 :person/name ?name-1]
         [?p2 :person/born ?born-2]
         [?p2 :person/name ?name-2]
         [(.getMonth ?born-1) ?month-1]
         [(.getDate ?born-1) ?date-1]
         [(.getMonth ?born-2) ?month-2]
         [(.getDate ?born-2) ?date-2]
         [(= ?month-1 ?month-2)]
         [(= ?date-1 ?date-2)]
         [(< ?name-1 ?name-2)]]
       @conn)

)


;; 7. aggregates

(comment

  ;; aggregate functions such as `sum`, `max` etc. are readily available in
  ;; Datomic's Datalog implementation. They are written in the `:find` clause in
  ;; your query:

  (def find-latest-movie-year
    '[:find (max ?year)
      :where
      [?m :movie/year ?year]])

  (d/q find-latest-movie-year @conn)

  ;; an aggregate function collects values from multiple datoms and returns

  ;; - a single value: `min`, `max`, `sum`, `avg`, etc.
  ;; - a collection of values: `(min n ?d)`, `(max n ?d)`, `(sample n ?e)` etc.
  ;;   where `n` is an integer specifying the size of the collection.

  (def find-avg-movie-year
    '[:find (avg ?year)
      :where
      [?m :movie/year ?year]])

  (d/q find-avg-movie-year @conn)

  (def find-sample-of-movie-years
    '[:find (sample ?n ?year)
      :in $ ?n
      :where
      [?m :movie/year ?year]])

  (d/q find-sample-of-movie-years @conn 3)

)


;; 8. rules

(comment

  ;; many times over the course of this tutorial, we have had to write the
  ;; following three lines of repetitive query code:

  ;; [?p :person/name ?name]
  ;; [?m :movie/cast ?p]
  ;; [?m :movie/title ?title]

  ;; *rules* are the means of abstraction in Datalog. You can abstract away
  ;; reusable parts of your queries into rules, give them meaningful names and
  ;; forget about the implementation details, just like you can with functions
  ;; in your favorite programming language. Let's create a rule for the three
  ;; lines above:

  (def actor-movie-rule
    '[(actor-movie ?name ?title)
      [?p :person/name ?name]
      [?m :movie/cast ?p]
      [?m :movie/title ?title]])

  ;; the first vector is called the *head* of the rule where the first symbol is
  ;; the *name* of the rule. The rest of the rule is called the *body*.

  ;; it is possible to use `(...)` or `[...]` to enclose it, but it is
  ;; conventional to use `(...)` to aid the eye when distinguishing between the
  ;; rule's head and its body, and also between rule invocations and normal data
  ;; patterns, as we'll see below.

  ;; you can think of a rule as a kind of function, but remember that this is
  ;; logic programming, so we can use the same rule to:
  ;; - find movie titles given an actor name, and
  ;; - find actor names given a movie title.

  ;; put another way, we can use both `?name` and `?title` in `(actor-movie
  ;; ?name ?title)` for input as well as for output. If we provide values for
  ;; neither, we'll get all the possible combinations in the database. If we
  ;; provide values for one or both, it'll constrain the result returned by the
  ;; query as you'd expect.

  ;; to use the above rule, you simply write the head of the rule instead of the
  ;; data patterns. Any variable with values already bound will be input, the
  ;; rest will be output.

  ;; the query to find cast members of some movie, for which we previously had
  ;; to write:

  (def find-actor-names-of-terminator-movie
    '[:find ?name
      :where
      [?p :person/name ?name]
      [?m :movie/cast ?p]
      [?m :movie/title "The Terminator"]])

  (d/q find-actor-names-of-terminator-movie @conn)

  ;; now becomes:

  (def find-actor-names-of-terminator-movie-1
    '[:find ?name
      :in $ %
      :where
      [actor-movie ?name "The Terminator"]])

  (d/q find-actor-names-of-terminator-movie-1
       @conn [actor-movie-rule])

  ;; the `%` symbol in the :in clause represent the rules. You can write any
  ;; number of rules, collect them in a vector, and pass them to the query
  ;; engine like any other input:

  ;; [[(rule-a ?a ?b)
  ;;   ...]
  ;;  [(rule-b ?a ?b)
  ;;   ...]]

  ;; you can use data patterns, predicates, transformation functions and calls
  ;; to other rules in the body of a rule.

  ;; finding actors by movie and movies by actor with the same rule:

  (def find-actors-by-movie
    '[:find ?name
      :in $ % ?title
      :where
      [actor-movie ?name ?title]])

  (d/q find-actors-by-movie
       @conn
       [actor-movie-rule]
       "Alien")

  (def find-movies-by-actor
    '[:find ?title
      :in $ % ?name
      :where
      (actor-movie ?name ?title)])

  (d/q find-movies-by-actor
       @conn
       [actor-movie-rule]
       "Bruce Willis")

  ;; rules can also be used as another tool to write logical OR queries, as the
  ;; same rule name can be used several times:

  (def person-movie-association-rules
    '[[(associated-with ?person ?movie)
       [?movie :movie/cast ?person]]
      [(associated-with ?person ?movie)
       [?movie :movie/director ?person]]])

  ;; subsequent rule definitions will only be used if the ones preceding it
  ;; aren't satisfied.

  ;; using this rule, we can find both directors and cast members very easily:

  (def find-crew-members-by-movie
    '[:find ?name
      :in $ % ?title
      :where
      [?m :movie/title ?title]
      (associated-with ?p ?m)
      [?p :person/name ?name]])

  (d/q find-crew-members-by-movie
       @conn
       person-movie-association-rules
       "Alien")

  ;; given the fact that rules can contain calls to other rules, what would
  ;; happen if a rule called itself? Interesting things, it turns out, but let's
  ;; find out in the exercises.


  ;; exercises

  ;; 1. write a rule `[movie-year ?title ?year]` where `?title` is the title of
  ;; some movie and `?year` is that movies release year.

  (def movie-year-rule
    '[(movie-year ?title ?year)
      [?m :movie/year ?year]
      [?m :movie/title ?title]])

  (d/q '[:find ?title
         :in $ %
         :where
         [movie-year ?title 1991]]
       @conn
       [movie-year-rule])

  ;; 2. two people are friends if they have worked together in a movie. Write a
  ;; rule `[friends ?p1 ?p2]` where `?p1` and `?p2` are person entities. Try
  ;; with a few different `?name` inputs to make sure you got it right. There
  ;; might be some edge cases here.

  (def friend-rules-flat
    '[[(friends ?p1 ?p2)
       [?m :movie/cast ?p1]
       [?m :movie/director ?p2]
       [(not= ?p1 ?p2)]]
      [(friends ?p1 ?p2)
       [?m :movie/director ?p1]
       [?m :movie/cast ?p2]
       [(not= ?p1 ?p2)]]
      [(friends ?p1 ?p2)
       [?m :movie/director ?p1]
       [?m :movie/director ?p2]
       [(not= ?p1 ?p2)]]
      [(friends ?p1 ?p2)
       [?m :movie/cast ?p1]
       [?m :movie/cast ?p2]
       [(not= ?p1 ?p2)]]])

  (def friend-rules
    '[[(associated-with ?person ?movie)
       [?movie :movie/cast ?person]]
      [(associated-with ?person ?movie)
       [?movie :movie/director ?person]]
      [(friends ?p1 ?p2)
       [?m :movie/title]
       (associated-with ?p1 ?m)
       (associated-with ?p2 ?m)
       [(not= ?p1 ?p2)]]])

  (def find-friends
    '[:find ?friend
      :in $ % ?name
      :where
      [?p1 :person/name ?name]
      (friends ?p1 ?p2)
      [?p2 :person/name ?friend]])

  (d/q find-friends
       @conn
       friend-rules-flat
       "Sigourney Weaver")

  (d/q find-friends
       @conn
       friend-rules
       "Sigourney Weaver")

  ;; 3. write a rule `[sequels ?m1 ?m2]` where `?m1` and `?m2` are movie
  ;; entities. You'll need to use the attribute `:movie/sequel`. To implement
  ;; this rule correctly you can think of the problem like this: A movie `?m2`
  ;; is a sequel of `?m1` if either

  ;; - `?m2` is the "direct" sequel of m1 or
  ;; - `?m2` is the sequel of some movie `?m` and that movie `?m` is the
  ;;    sequel to `?m1`.

  ;; there are (at least) three different ways to write the above query. Try to
  ;; find all three solutions.

  (def sequel-rules
    '[[(sequels ?m1 ?m2)
       [?m1 :movie/sequel ?m2]]
      [(sequels ?m1 ?m2)
       [?m3 :movie/sequel ?m2]
       (sequels ?m1 ?m3)]])

  (def find-sequels
    '[:find ?sequel
      :in $ % ?title
      :where
      [?m :movie/title ?title]
      (sequels ?m ?s)
      [?s :movie/title ?sequel]])

  (d/q find-sequels
       @conn
       sequel-rules "Mad Max")

)
