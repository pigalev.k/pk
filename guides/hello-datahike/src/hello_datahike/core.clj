(ns hello-datahike.core
  (:require
   [datahike.api :as d]
   [datahike.migrate :as m]))


;;https://github.com/replikativ/datahike/blob/main/examples/basic/src/examples/core.clj

;; - Conditional Queries in Datomic https://grishaev.me/en/datomic-query/
;; - lambdaforge blog https://lambdaforge.io/articles


;; basics

;; using file db backend

(comment

  ;; use the filesystem as storage medium
  (def cfg {:store {:backend :file :path "/tmp/example"}})

  ;; create a database at this place, per default configuration we enforce a
  ;; strict schema and keep all historical data
  (d/create-database cfg)

  (def conn (d/connect cfg))

  ;; the first transaction will be the schema we are using you may also add this
  ;; within database creation by adding `:initial-tx` to the configuration
  (d/transact conn [{:db/ident :name
                     :db/valueType :db.type/string
                     :db/cardinality :db.cardinality/one }
                    {:db/ident :age
                     :db/valueType :db.type/long
                     :db/cardinality :db.cardinality/one }])

  ;; lets add some data and wait for the transaction
  (d/transact conn [{:name  "Alice", :age   20 }
                    {:name  "Bob", :age   30 }
                    {:name  "Charlie", :age   40 }
                    {:age 15 }])

  ;; search the data

  ;; query may be in the list form
  (d/q '[:find ?e ?n ?a
         :where
         [?e :name ?n]
         [?e :age ?a]]
       @conn)

  ;; or the map form
  (d/q '{:find [?e ?n ?a]
         :where [
         [?e :name ?n]
         [?e :age ?a]]}
       @conn)

  ;; add new entity data using a hash map
  (d/transact conn {:tx-data [{:db/id 3 :age 25}]})

  ;; if you want to work with queries like in
  ;; https://grishaev.me/en/datomic-query/,
  ;; you may use a hashmap
  (d/q {:query '{:find [?e ?n ?a ]
                 :where [[?e :name ?n]
                         [?e :age ?a]]}
        :args [@conn]})

  ;; query the history of the data
  (d/q '[:find ?a
         :where
         [?e :name "Alice"]
         [?e :age ?a]]
       (d/history @conn))

  ;; you might need to release the connection for specific stores, e.g. for
  ;; leveldb
  (d/release conn)

  ;; clean up the database if it is not needed anymore
  (d/delete-database cfg)

)


;; migration & backup

(comment

  ;; dump the db contents into a file
  (m/export-db conn "/tmp/eavt-dump")

  ;; ... setup new connection to db with correct schema
  (def new-cfg {:store {:backend :file :path "/tmp/example-imported"}})
  (d/create-database new-cfg)
  (def new-conn (d/connect new-cfg))

  ;; then import the dump
  (m/import-db new-conn "/tmp/eavt-dump")

  ;; query the new db to verify that export and import worked
  (d/q '[:find ?e ?n ?a
         :where
         [?e :name ?n]
         [?e :age ?a]]
       @new-conn)

)
