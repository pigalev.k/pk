(ns hello-datahike.schema
  (:require
   [datahike.api :as d]))


;; db schema

;; https://github.com/replikativ/datahike/blob/main/doc/schema.md
;; https://github.com/replikativ/datahike/blob/main/examples/basic/src/examples/schema.clj
;; https://docs.datomic.com/on-prem/schema/schema.html
;; https://github.com/kristianmandrup/datascript-tutorial/blob/master/create_schema.md


;; Datahike

;; there are two approaches for imposing integrity constraints on structured
;; data collections: schema-on-read or schema-on-write. Datahike supports both.

;; the schema-on-read approach assumes an implicit structure of the data where
;; the structure is only interpreted at read level (MongoDB, Redis).

;; the schema-on-write approach has an explicit assumption of the data model
;; where the database ensures that all data written is conform to a defined data
;; model (PostgreSQL, Cassandra).



;; overview

(comment

  ;; database and datom

  ;; the facts that a Datalog database stores are represented by datoms. Each
  ;; datom is an addition or retraction of a relation between an entity, an
  ;; attribute, a value, and a transaction. The set of possible attributes a
  ;; datom can specify is defined by a database's schema.

  ;; each database has a schema that describes the set of attributes that can be
  ;; associated with entities. A schema only defines the characteristics of the
  ;; attributes themselves. It does not define which attributes can be
  ;; associated with which entities. Decisions about which attributes apply to
  ;; which entities are made by an application.

  ;;  This gives applications a great degree of freedom to evolve over time. For
  ;;  example, an application that wants to model a person as an entity does not
  ;;  have to decide up front whether the person is an employee or a
  ;;  customer. It can associate a combination of attributes describing
  ;;  customers and attributes describing employees with the same entity. An
  ;;  application can determine whether an entity represents a particular
  ;;  abstraction, customer or employee, simply by looking for the presence of
  ;;  the appropriate attributes.


  ;; attributes

  ;; schema attributes are defined using the same data model used for
  ;; application data. That is, attributes are themselves entities with
  ;; associated attributes. Datomic defines a set of built-in system attributes
  ;; that are used to define new attributes.

  ;; Datomic schemas require the following attributes:

  ;; - `:db/ident`:        attribute identifier (name)
  ;; - `:db/valueType`:    value type
  ;; - `:db/cardinality`:  cardinality (can hold one or many values)

  ;; attribute installation is atomic. All the required information for an
  ;; attribute must be specified in a single transaction, after which the
  ;; attribute will be immediately available for use.


  ;; attribute id

  ;; `:db/ident` specifies the unique name of an attribute. It's value is a
  ;; namespaced keyword with the lexical form `:<namespace>/<name>`.

  ;; it is possible to define a name without a namespace, as in `:<name>`, but
  ;; the namespaced form is preferred in order to avoid naming collisions.

  ;; namespaces can be hierarchical, with segments separated by ".", as in
  ;; `:<namespace>.<nested-namespace>/<name>`. The `:db` namespace is reserved
  ;; for use by Datomic itself.


  ;; value types

  ;; the Datomic value types map to Java equivalents.

  ;; basic

  ;; - `:db.type/keyword` a keyword, `:color/yellow`
  ;; - `:db.type/string` a string, "Hello"
  ;; - `:db.type/symbol` a symbol, foo
  ;; - `:db.type/boolean` a boolean, true|false

  ;; numbers

  ;; - `:db.type/long` a long integer, 42
  ;; - `:db.type/bigint` a big integer, 42N
  ;; - `:db.type/float` a floating point number, 42.0
  ;; - `:db.type/double` a double-precision floating-point number, 42.0
  ;; - `:db.type/bigdec` big decimal, 42.0M

  ;; special

  ;; - `:db.type/ref` an entity reference (entity id), 42
  ;; - `:db.type/instant` a time instant, #inst "2017-09-16T11:43:32.450-00:00"
  ;; - `:db.type/uuid` an UUID, #uuid "f40e770e-9ad5-11e7-abc4-cec278b6b50a"
  ;; - `:db.type/uri` an URI, https://www.datomic.com/details.html
  ;; - `:db.type/bytes` small binary data, `(byte-array (map byte [1 2 3]))`
  ;; - `:db.type/tuple` a tuple of scalar values, [42 12 "foo"]

  ;; see the original Datomic reference for notes and limitations on value
  ;; types.


  ;; cardinality

  ;; - `:db.cardinality/one` reference one value
  ;; - `:db.cardinality/many` reference many values

  ;; transactions can add or retract individual values for multi-valued
  ;; attributes.


  ;; tuples

  ;; tuples can be used to create multi-attribute unique keys on domain
  ;; entities. Tuples can be used to optimize queries that otherwise would have
  ;; to join two or more high-population attributes.

  ;;  a tuple is a collection of 2-8 scalar values, represented in memory as a
  ;;  Clojure vector. There are three kinds of tuples:

  ;; - *composite tuples* are derived from other attributes of the same
  ;;   entity. Composite tuple types have a `:db/tupleAttrs` attribute,
  ;;   whose value is 2-8 keywords naming other attributes.
  ;; - *heterogeneous fixed length tuples* have a `:db/tupleTypes` attribute,
  ;;   whose value is a vector of 2-8 scalar value types.
  ;; - *homogeneous variable length tuples* have a `:db/tupleType` attribute,
  ;;   whose value is a a vector of 2-8 keywords naming a scalar value type.

  ;; the following types are considered scalar types suitable for use in a tuple:

  ;; `:db.type/bigdec` `:db.type/bigint` `:db.type/boolean` `:db.type/double`
  ;; `:db.type/instant` `:db.type/keyword` `:db.type/long` `:db.type/ref`
  ;; `:db.type/string` `:db.type/symbol` `:db.type/uri` `:db.type/uuid`

  ;; string values within a tuple are limited to 256 characters.

  ;; nil is a legal value for any slot in a tuple. This facilitates using tuples
  ;; in range searches, where nil sorts lowest.

  ;; datomic includes the query helpers `tuple` and `untuple` to support storing
  ;; same values both in a tuple and in separate attributes.


  ;; composite tuples

  ;; composite tuples are applicable:

  ;; - when a domain entity has a multi-attribute key
  ;; - to optimize a query that joins more than one high-population attributes
  ;;   on the same entity

  ;; composite attributes are entirely managed by Datomic–you never assert or
  ;; retract them yourself. Whenever you assert or retract any attribute that is
  ;; part of a composite, Datomic will automatically populate the composite
  ;; value.

  ;; if the current value of an entity does not include all attributes of a
  ;; composite, the missing attributes will be nil. Note that nil sorts lower
  ;; than all other values, so tuples with trailing nils can be useful for range
  ;; queries.

  ;; if you retract all constituents of a composite, Datomic will retract the
  ;; composite.


  ;; adding composites to existing entities

  ;; adding a composite tuple to a database that contains existing data using
  ;; those attributes will not immediately generate values for the new
  ;; tuple. The composite tuple will be created the next time any of the
  ;; composite member attributes are transacted. This includes "no-op"
  ;; transactions of the same attribute value. This design allows you to add
  ;; composite tuples in a systematic and paced manner, so as not to overwhelm a
  ;; running system.


  ;; heterogeneous tuples

  ;; heterogenous tuples have a `:db/tupleTypes` attribute, with a value
  ;; specified as a vector of 2-8 scalar types.


  ;; homogeneous tuples

  ;; homogeneous tuples provide variable length composites of a single attribute
  ;; type. The `:db.type` of a homogeneous tuple is specified by the keyword
  ;; attribute `:db/tupleType`.

  ;; Datomic itself includes a good example of homogeneous tuples in the
  ;; definitions of the other tuple types. Both `:db/tupleTypes`
  ;; and `:db/tupleAttrs` are declared as homogeneous tuples
  ;; of `:db/tupleType` `:db.type/keyword`.


  ;; optional schema attributes

  ;; in addition to these three required attributes, there are several optional
  ;; attributes that can be associated with an attribute definitions:

  ;; - `:db/doc` documentation string
  ;; - `:db/unique` uniqueness constraint for the values of an attribute; also
  ;;   implies `:db/index`. Set to `:db.unique/value` (duplicate inserts will
  ;;   fail) or `:db.unique/identity` (duplicate inserts will update the
  ;;   existing entity's attribute instead)
  ;; - `:db/index` to index this attribute (boolean)
  ;; - `:db.attr/preds` to ensure the value of an attribute with one or more
  ;;   predicates that you supply
  ;; - `:db/fulltext` fulltext search index (boolean) (Datomic only)
  ;; - `:db/isComponent` indicates that `db.type/ref` refers to a subcomponent
  ;;   of the entity and should apply cascading (recursive) retraction (boolean)
  ;; - `:db/noHistory` whether past values of an attribute should not be
  ;;   retained (boolean)

  ;; all booleans are by default false (i.e., not enabled).


  ;; external keys

  ;; all entities in a database have an internal key, the entity id. You can use
  ;; `:db/unique` to define an attribute to represent an external key. An entity
  ;; may have any number of external keys. External keys must be single
  ;; attributes, multi-attribute keys are not supported.


  ;; attribute predicates

  ;; you may want to constrain an attribute value by more than just its
  ;; storage/representation type. For example, an email address is not just a
  ;; string, but a string with a particular format. In Datomic, you can assert
  ;; attribute predicates about an attribute. Attribute predicates are asserted
  ;; via the `:db.attr/preds` attribute, and are fully-qualified symbols that
  ;; name a predicate of a value. Predicates return true (and only true) to
  ;; indicate success. All other values indicate failure and are reported back
  ;; as transaction errors.

  ;; inside transactions, Datomic will call all attribute predicates on all
  ;; assertions for attribute values, and abort a transaction if any predicate
  ;; fails.

  ;; for example, the following function validates that a user-name has a
  ;; particular length:

  ;; (ns datomic.samples.attr-preds)
  ;; (defn user-name?
  ;;   [s]
  ;;   (<= 3 (count s) 15))

  ;; to install the user-name? predicate, add a `:db.attr/preds` value to an
  ;; attribute, e.g.

  ;; {:db/ident :user/name,
  ;;  :db/valueType :db.type/string,
  ;;  :db/cardinality :db.cardinality/one,
  ;;  :db.attr/preds 'datomic.samples.attr-preds/user-name?}

  ;; a transaction that includes an invalid user-name will result in an anomaly
  ;; whose ex-data includes

  ;; - the entity id
  ;; - the attribute name
  ;; - the attribute value
  ;; - the name of the failed predicate
  ;; - the predicate return in :db.error/pred-return

  ;; attribute predicates must be on the classpath of a process that is
  ;; performing a transaction.

  ;; attribute predicates can be asserted or retracted at any time, and will be
  ;; enforced starting on the transaction after they are asserted. Asserting or
  ;; retracting an attribute predicate has no effect on attribute values that
  ;; already exist in the database.


  ;; entity specs

  ;; you may want to ensure properties of an entity being asserted, for example

  ;; - required keys
  ;; - the creation of composite tuples
  ;; - satisfaction of properties that cut across attributes and the db

  ;; an entity spec is a Datomic entity having one or more of

  ;; - (usually) a `:db/ident`
  ;; - `:db.entity/attrs` naming required attributes
  ;; - `:db.entity/preds` naming entity predicates

  ;; you can then ensure an entity spec by asserting the `:db/ensure` attribute
  ;; for an entity. For example, the following transaction data ensures
  ;; entity "new-account-1" with entity spec `:new-account`

  ;; {:db/id "new-account-1"
  ;;  :db/ensure :new-account
  ;;  ;; other data that must be a valid :new-account
  ;;  }

  ;; `:db/ensure` is a virtual attribute. It is not added in the database;
  ;; instead it triggers checkes based on the named entity.

  ;; entity predicates must be on the classpath of a process that is performing
  ;; a transaction.

  ;; entity specs can be asserted or retracted at any time, and will be enforced
  ;; starting on the transaction after they are asserted. Asserting or
  ;; retracting an entity spec has no effect on entities that already exist in
  ;; the database.

  ;; entity specs and `:db/ensure` are not analogous to traditional SQL
  ;; constraints:

  ;; - specs are more flexible, enforcing arbitrary shapes on particular
  ;;   entities without imposing an overall structure on the database.
  ;; - specs can do more, allowing arbitrary functions of an entity and
  ;;   a database value.
  ;; - specs must be requested explicitly per transaction+entity, and
  ;;   enforcement is never automatic or retroactive.

  ;; required attributes

  ;; the `:db.entity/attrs` attribute is a multi-valued attribute of keywords,
  ;; where each keyword names a required attribute.

  ;; for example, the following transaction data creates a spec that requires
  ;; a `:user/name` and `:user/email`:

  ;; {:db/ident :user/validate
  ;;  :db.entity/attrs [:user/name :user/email]}

  ;; the `:user/validate` entity can then be used in later transaction to ensure
  ;; that all required attribute are present. For example, the following
  ;; transaction would fail

  ;; {:user/name "John Doe"
  ;;  :db/ensure :user/validate}

  ;; when a required attribute is missing, Datomic will throw an anomaly whose
  ;; `ex-data` includes the failing entity and the name of the spec.

  ;; entity predicates

  ;; the `:db.entity/preds` attribute is a multi-valued attribute of symbols,
  ;; where each symbol names a predicate of database value and entity id. Inside
  ;; a transaction, Datomic will call all predicates, and abort the transaction
  ;; if any predicate returns a value that is not true.

  ;; entity predicates must be on the classpath of a process that is performing
  ;; a transaction.

  ;; for example, given the following predicate:

  ;; (ns datomic.samples.entity-preds
  ;;   (:require [datomic.api :as d]))
  ;; (defn scores-are-ordered?
  ;;   [db eid]
  ;;   (let [m (d/pull db [:score/low :score/high] eid)]
  ;;     (<= (:score/low m) (:score/high m))))

  ;; you could install the predicate on a guard entity:

  ;; {:db/ident :score/guard
  ;;  :db.entity/attrs [:score/low :score/high] ;; spec
  ;;  :db.entity/preds 'datomic.samples.entity-preds/scores-are-ordered?} ;; pred


  ;; partitions

  ;; all entities created in a database reside within a partition. Partitions
  ;; group data together, providing locality of reference when executing queries
  ;; across a collection of entities. In general, you want to group entities
  ;; based on how you'll use them. Entities you'll often query across - like the
  ;; community-related entities in our sample data - should be in the same
  ;; partition to increase query performance. Different logical groups of
  ;; entities should be in different partitions.

  ;; there are three partitions built into Datomic.
  ;; Partition 	       Purpose
  ;; - `:db.part/db`   system partition, used for schema
  ;; - `:db.part/tx`   transaction partition
  ;; - `:db.part/user` user partition, for application entities

  ;; the attributes you create for your schema must live in the `:db.part/db`
  ;; partition.

  ;; the `:db.part/tx` partition is used only for transaction entities, which
  ;; are created automatically by transactions.

  ;; you can use `:db.part/user` for your own entities.

  ;; your schema can create one or more partitions appropriate for your
  ;; application.

  ;; creating new partitions

  ;; a partition is represented by an entity with a `:db/ident` attribute, as
  ;; shown below. A partition can be referred to by either its entity id or
  ;; ident.

  ;; {:db/ident :communities}

  ;; as with attribute definitions, there are two steps to installing a new
  ;; partition in a database. The first is to create the partition entity. The
  ;; second is to associate the new partition entity with the built-in system
  ;; partition entity, `:db.part/db`. Specifically, the new partition must be
  ;; added as a value of the `:db.part/db` entity's `:db.install/partition`
  ;; attribute. This linkage is what tells the database that the new entity is
  ;; actually a partition definition.

  ;; here is a complete transaction for installing the `:communities`
  ;; partition. It includes the map that defines the partition, followed by the
  ;; statement adding it as a value of `:db.part/db`'s `:db.install/partition`
  ;; attribute.

  ;; [{:db/id "communities"
  ;;   :db/ident :communities}
  ;;  [:db/add :db.part/db :db.install/partition "communities"]]


  ;; lookup refs

  ;; in many databases, entities have unique identifiers from the problem domain
  ;; like an email address or an order number. Applications often need to find
  ;; entities based on these external keys. You can do this with query, but it's
  ;; easier to use a lookup ref such as:

  ;; `[:person/email "joe@example.com"]`

  ;; you can also use lookup refs to refer to existing entities, such as in:

  ;; `{:db/id [:person/email "joe@example.com"]
  ;;  :person/loves :pizza}`

  ;; lookup refs have the following restrictions:

  ;; - the specified attribute must be defined as either `:db.unique/value`
  ;;   or `:db.unique/identity`.
  ;; - lookup refs cannot be used in the body of a query. (Datomic constraint
  ;;   only?)

  ;; Datomic performs automatic resolution of entity identifiers, so that you
  ;; can generally use entity ids, idents, and lookup refs interchangeably.


  ;; unique values and identities

  ;; unique identity is specified through an attribute with `:db/unique` set
  ;; to `:db.unique/identity`

  ;; a unique identity attribute is always indexed by value, in order to support
  ;; uniqueness checks. Specifying `:db/index` true is redundant and not
  ;; recommended.

  ;; uniqueness checks are per-attribute, and do not prevent you from using the
  ;; same value with a different attribute elsewhere.

  ;; unique value is specified through an attribute with `:db/unique` set to
  ;; `:db.unique/value`. A unique value represents a database-wide value that
  ;; can be asserted only once.


  ;; schema alteration

  ;; because Datomic maintains a single set of physical indexes, and supports
  ;; query across time, a db value utilizes the single schema associated with
  ;; its basis, i.e. before any call to `as-of`/`since`/`history`, for all views
  ;; of that db (including `as-of`, `since` and `history`). Thus travelling back
  ;; in time does not take the working schema back in time, as the
  ;; infrastructure to support it may no longer exist.

  ;; you can only rename the `:db/ident` or alter certain schema attributes of
  ;; attributes.


  ;; renaming an identity

  ;; to rename a `:db/ident`, submit a transaction with the `:db/id` and the
  ;; value of the new `:db/ident`.

  ;; the supported attributes to alter include `:db/cardinality`,
  ;; `:db/isComponent`, `:db/noHistory`, `:db/index` and `:db/unique`. You
  ;; cannot alter `:db/valueType` or `:db/fulltext`.

  ;; alter the `:db/ident` of `:person/name` to be `:person/full-name`:

  ;; `[{:db/id :person/name
  ;;    :db/ident :person/full-name}]`

)


;; Datascript overview

(comment

  ;; Datascript schemas are a little different. We can define a schema using a
  ;; map as follows:

  ;; `{:aka {:db/cardinality :db.cardinality/many}}`

  ;; here :aka is the name of the entity, similar to
  ;; `:db/ident`. `{:db/cardinality :db.cardinality/many}` creates a constraint
  ;; that `:aka` can refer to multiple values.

  ;; notice that `:aka` doesn't have a value type as required by Datomic. This
  ;; is because we are in Javascript land and things are a bit more flexible
  ;; and "typeless". You can still add the value type if you like.

  ;; Datomic uses a special attribute `:db/ident` to identify attributes. In
  ;; this (Datomic) example we define an attribute named `:person/name` of type
  ;; `:db.type/string` with `:db.cardinality/one` that is intended to capture a
  ;; person's name.

  ;; `{:db/id #db/id [:db.part/db]
  ;;   :db/ident :person/name
  ;;   :db/valueType :db.type/string
  ;;   :db/cardinality :db.cardinality/one
  ;;   :db/doc "A person's name"
  ;;   :db.install/_attribute :db.part/db}`

  ;; in Datascript we can simply use the nested map form: `{:aka { <schema> }}`
  ;; as demonstrated above. This Datomic example in Datascript would be
  ;; simplified to:

  ;; (def schema {:person/name {:db/valueType :db.type/string
  ;;                            :db/cardinality :db.cardinality/one
  ;;                            :db/doc "A person's name"}})

  ;; `:db/ident` is just a regular attribute, but we can give it "special
  ;; powers" as per our own conventions if we choose.

  ;; schema is applied when connection is created
  ;; `(def conn (d/create-conn schema))`


  ;; identity via `:db/ident`

  ;; we can use `:db/ident` as a convention for lookup refs, a more semantic
  ;; kind of entity ids.

  ;; first we must set the schema of `:db/ident` to be a `:db/unique` of
  ;; `:db.unique/identity` to ensure the attribute must have unique values. This
  ;; is often useful for attributes like `:person/email` or `:person/phone`. We
  ;; could then reuse the same values in similar attributes such as
  ;; `:company/phone` without conflict.

  ;; we can similarly use `:db.unique/value` for identity attributes that should
  ;; be guaranteed to be unique cross the entire DB, such as Social security or
  ;; Passport numbers.

  ;; registering in schema
  ;; `(def schema {:person/email {:db/unique :db.unique/identity}})`

  ;; we can now add an entity with `:db/ident` set to a unique key of our
  ;; choosing

  ;; specifying ident on an entity
  ;; `(d/transact! conn [[:db/add <eid> :db/ident 'my-unique-key']])`

  ;; then look up an entity by `:db/ident` value

  ;; `(d/entity @conn [:db/ident 'my-unique-key'])`

  ;; add a new entity using the lookup ref 'my-unique-key' as an implicit entity
  ;; id.

  ;; `(d/transact! conn [[:db/add [:db/ident 'my-unique-key'] <attr> <value>]])`

  ;; then find entity attributes `?a` and values `?v` given the lookup ref.

  ;; `(d/q '[:find ?a ?v
  ;;         :where
  ;;         [[:db/ident 'my-unique-key'] ?a ?v]]
  ;;       @conn)`

)


;; Datahike: schema-on-write

(comment

  ;; define data model
  (def schema [{:db/ident :contributor/name
                :db/valueType :db.type/string
                :db/unique :db.unique/identity
                :db/index true
                :db/cardinality :db.cardinality/one
                :db/doc "a contributor's name"}
               {:db/ident :contributor/email
                :db/valueType :db.type/string
                :db/cardinality :db.cardinality/many
                :db/doc "a contributor's email"}
               {:db/ident :repository/name
                :db/valueType :db.type/string
                :db/unique :db.unique/identity
                :db/index true
                :db/cardinality :db.cardinality/one
                :db/doc "a repository's name"}
               {:db/ident :repository/contributors
                :db/valueType :db.type/ref
                :db/cardinality :db.cardinality/many
                :db/doc "the repository's contributors"}
               {:db/ident :repository/public
                :db/valueType :db.type/boolean
                :db/cardinality :db.cardinality/one
                :db/doc "toggle whether the repository is public"}
               {:db/ident :repository/tags
                :db/valueType :db.type/ref
                :db/cardinality :db.cardinality/many
                :db/doc "the repository's tags"}
               {:db/ident :language/clojure}
               {:db/ident :language/rust}])

  ;; define configuration
  (def cfg {:store {:backend :mem
                    :id "schema-intro"}
            :schema-flexibility :write})

  ;; cleanup any previous database
  (d/delete-database cfg)

  ;; create the in-memory database
  (d/create-database cfg)

  ;; connect to it
  (def conn (d/connect cfg))

  ;; add the schema
  (d/transact conn schema)

  ;; let's insert our first user
  (d/transact conn
              [{:contributor/name "alice" :contributor/email "alice@exam.ple"}])

  ;; let's find her with a query
  (def find-name-email
    '[:find ?e ?n ?em
      :where
      [?e :contributor/name ?n]
      [?e :contributor/email ?em]])

  (d/q find-name-email @conn)

  ;; let's find her directly, as contributor/name is a unique, indexed identity
  (d/pull @conn '[*] [:contributor/name "alice"])

  ;; add a second email, as we have a many cardinality, we can have several ones
  ;; as a user
  (d/transact conn
              [{:db/id [:contributor/name "alice"]
                :contributor/email "alice@test.test"}])

  ;; let's see both emails
  (d/q find-name-email @conn)

  ;; try to add something completely not defined in the schema
  (d/transact conn [{:something "different"}])
  ;; => Exception shows missing schema definition

  ;; try to add wrong contributor values
  (d/transact conn [{:contributor/email :alice}])
  ;; => Exception shows what value is expected

  ;; add another contributor by using a the alternative transaction schema that
  ;; expects a hash map with tx-data attribute
  (d/transact conn
              {:tx-data
               [{:contributor/name "bob" :contributor/email "bob@ac.me"}]})

  (d/q find-name-email @conn)

  (d/pull @conn '[*] [:contributor/name "bob"])

  ;; change bob's name to bobby
  (d/transact conn
              [{:db/id [:contributor/name "bob"] :contributor/name "bobby"}])

  ;; check it
  (d/q find-name-email @conn)

  (d/pull @conn '[*] [:contributor/name "bobby"])

  ;; Bob is not related anymore as index
  (d/pull @conn '[*] [:contributor/name "bob"])
  ;; will give an exception

  ;; create a repository, with refs from uniques, and an ident as enum
  (d/transact conn
              [{:repository/name "top secret"
                :repository/public false
                :repository/contributors [[:contributor/name "bobby"]
                                          [:contributor/name "alice"]]
                :repository/tags :language/clojure}])

  ;; let's search with pull inside the query
  (def find-repositories
    '[:find (pull ?e [*])
      :where [?e :repository/name ?n]])

  ;; looks good
  (d/q find-repositories @conn)

  ;; let's go further and fetch the related contributor data as well
  (def find-repositories-with-contributors
    '[:find (pull ?e [* {:repository/contributors [*] :repository/tags [*]}])
      :where [?e :repository/name ?n]])

  (d/q find-repositories-with-contributors @conn)

  ;; the schema is part of the index, so we can query them too.  Let's find all
  ;; attribute names and their description.
  (d/q '[:find ?a ?t ?d
         :where
         [?e :db/ident ?a]
         [?e :db/valueType ?t]
         [?e :db/doc ?d]] @conn)

  ;; cleanup the database
  (d/delete-database cfg)

)


;; Datahike: schema-on-read

(comment

  ;; let's create another database that can hold any arbitrary data
  (def cfg {:store {:backend :mem
                    :id "schemaless"}
            :schema-flexibility :read})

  ;; cleanup any previous database
  (d/delete-database cfg)

  ;; create the in-memory database
  (d/create-database cfg)

  ;; connect to it
  (def conn (d/connect cfg))

  ;; now we can go wild and transact anything
  (d/transact conn [{:any "thing"}])

  ;; use simple query on this data
  (d/q '[:find ?v
         :where
         [_ :any ?v]]
       @conn)

  ;; be aware: although there is no schema, you should tell the database if some
  ;; attributes can have specific cardinality or indices.  You may add that as
  ;; schema transactions like before
  (d/transact conn
              [{:db/ident :any :db/cardinality :db.cardinality/many}])

  ;; let's add more data to the first any entity
  (def any-eid (d/q
                '[:find ?e .
                  :where
                  [?e :any "thing"]]
                @conn))

  (d/transact conn [{:db/id any-eid :any "thing else"}])

  (d/q '[:find ?v
         :where
         [_ :any ?v]]
       @conn)

)
