(ns hello-datahike.entity
  (:require
   [datahike.api :as d]
   [datahike.impl.entity :as de]
   [hello-datahike.common :as c]))


;; pull API

;; https://docs.datomic.com/on-prem/overview/entities.html


;; prepare the db (in-memory)

(comment

  (def cfg {:store              {:backend :mem
                                 :id      "entity-api-tutorial"}
            :schema-flexibility :write
            :keep-history?      true
            :attribute-refs?    true})

  ;; create the db and connect to it
  (def conn (c/connect! cfg))

  ;; there are 20 movies in the db now
  (count (d/q '[:find ?title
                :where
                [_ :movie/title ?title]]
              @conn))

)


;; `d/entity`: fetch an entity by its id from database. Entities are lazy
;; map-like structures.

(comment

  ;; a Datomic entity provides a lazy, associative view of all the information
  ;; that can be reached from a Datomic entity id.

  ;; the entity interface provides associative access to

  ;; - all the attribute/value pairs associated with an entity id E
  ;; - all other entities reachable as values V from E
  ;; - all other entities that can reach E through their values V

  ;; navigation to other entities is recursive, so with thoroughly connected
  ;; data such a social network graph it may be possible to navigate an entire
  ;; dataset starting with a single entity.

  ;; the Entity interface is completely generic, and can navigate any and all
  ;; Datomic data.


  ;; basics

  ;; get an entity, given an entity id or lookup ref:

  (def rambo (d/entity @conn 100))

  ;; given an entity, you can then navigate to any of that entity's attributes:

  (get rambo :movie/title)
  (:movie/title rambo)

  (:movie/year rambo)

  ;; the special attribute :db/id provides access to the entity's own id:

  (:db/id rambo)

  ;; if an attribute points to another entity through a cardinality-one
  ;; attribute, get will return another Entity instance.

  (def rambo-sequel (:movie/sequel rambo))

  (:movie/title rambo-sequel)

  ;; if an attributes points to another entity through a cardinality-many
  ;; attribute, get will return a set of entity instances.

  (:movie/cast rambo)
  (map :person/name (:movie/cast rambo))

  ;; if you precede an attribute's local name with an underscore (`_`), `get`
  ;; will navigate backwards, returning the set of entities that point to the
  ;; current entity.

  (def stallone (d/entity @conn 53))

  (:movie/_cast stallone)
  (map :movie/title (:movie/_cast stallone))

  ;; entities do not "exist" in any particular place; they are merely an
  ;; associative view of all the datoms about some E at a point in time. If
  ;; there are no facts currently available about an entity, `d/entity` will
  ;; return an empty entity, having only a `:db/id`.


  ;; laziness and caching

  ;; entity attributes are accessed lazily as you request them. Once you access
  ;; a particular attribute, the attribute will be cached for the lifetime of
  ;; that entity instance.

  ;; in Datomic, the Entity.keySet method will return the keys available for an
  ;; entity, without caching their values. In Datahike, no such method is
  ;; available, and `keys` caches all attribute values.

  ;; only `:db/id` is present immediately
  (def rambo (d/entity @conn 100))
  rambo
  (keys rambo)
  rambo

  ;; in Datomic, the Entity.touch method is a convenience that will access and
  ;; cache all the attributes of an entity. In addition, touch will also access
  ;; and cache the attributes of any other entities reachable through an
  ;; attribute whose schema definition includes `:isComponent` true. In
  ;; Datahike, there is `de/touch` function, but probably it is implementation
  ;; detail.

  (def rambo (d/entity @conn 100))
  rambo
  (de/touch rambo)
  rambo

  ;; as an example, consider a database of orders, line items, and products. A
  ;; line item is a component of an order. A product is related to an order, but
  ;; is not a component of the order. In such a database, touching an order will
  ;; access the order's attributes, and recursively access all the order's line
  ;; items. Touching an order will not access the line items' products.

  ;; the touch method is a convenience, suitable for interactive exploration, or
  ;; for when you know that you will need all of an entity's information. When
  ;; this is not the case, it can be more efficient to be explicit, and ask for
  ;; precisely what you need.

  ;; note that none of the entity APIs mandate any communication with a
  ;; server. Entities are lazy values, backed by a lazy database value in an
  ;; application process. In particular, entities are not subject to the N+1
  ;; select problem, which is an artifact of client/server query architectures.


  ;; entities and time (probably Datomic-only)

  ;; entities know the point in time on which they are based. Given an entity,
  ;; you can access the database on which the entity is based

  ;; from the database you can then find the database's time basis T, the
  ;; transactions that contributed to the entity, etc.

  ;; entities represent only a point in time, not the database across
  ;; time. Notice that the "across time" aspect of the database vanishes when
  ;; making entities (entity contains only eav, no tx and op).

  ;; the point-in-time nature of entities means that entities can be derived
  ;; from any point-in-time view of a database, i.e. the return values of
  ;; Connection.db, Database.asOf, Database.since, and Database.with.

  ;; entities cannot be derived from with any time-spanning view of a database,
  ;; i.e. the return value of Database.history.


  ;; usage considerations

  ;; entities are always consistent.

  ;; entities are lazy, immutable values. They are thread safe and do not
  ;; require coordination with any other concurrent uses of the same
  ;; information.

  ;; entities are based on a point in time of the database. Navigation from an
  ;; entity will always and only reach other entities with that same time basis,
  ;; allowing extended calculations to be performed without coordination.

  ;; entities are not a mapping layer between databases and application
  ;; code. Entities are a direct, mechanical translation from database
  ;; information to associative application access.

  ;; entities are not suitable for existence tests, which should typically be
  ;; performed via lookup of a unique identity.

)
