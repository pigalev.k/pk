(ns hello-datahike.common
  (:require
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [datahike.api :as d])
  (:import (java.io PushbackReader IOException)))


;; db preparation helpers for use in tutorials and guides

(defn load-edn
  "Read data from a EDN file or resource.
  Preserve unknown custom tagged literals as is."
  [source]
  (try
    (with-open [rdr (io/reader source)]
      (edn/read {:default tagged-literal}
                (PushbackReader. rdr)))
    (catch IOException e
      (printf "Couldn't open '%s': %s\n" source (.getMessage e)))
    (catch RuntimeException e
      (printf "Error parsing edn file '%s': %s\n" source (.getMessage e)))))

(defn connect!
  "Connect to Datahike database, deleting and recreating it if requested, and
  populating with schema and data, if supplied. Returns a connection to the
  created database."
  [db-spec & {:keys [schema-file data-file delete-db]
              :or   {schema-file "schema.edn"
                     data-file   "data.edn"
                     delete-db   true}
              :as   options}]
  (d/delete-database db-spec)
  (d/create-database db-spec)
  (let [conn (d/connect db-spec)]
    (when schema-file
      (let [schema (load-edn (io/resource schema-file))]
        (d/transact conn schema)))
    (when data-file
      (let [data (load-edn (io/resource data-file))]
        (d/transact conn data)))
    conn))

(defn pull
  "Return a query to pull an entity by the value of the attribute."
  [db attribute value]
  {:query '{:find [(pull ?e [*])]
            :in [$ ?attr ?val]
            :where [[?e ?attr ?val]]}
   :args [db attribute value]})

(defn pull-all
  "Return a query to pull all entities that have a given attribute."
  [db attribute]
  {:query '{:find [(pull ?e [*])]
            :in [$ ?attr]
            :where [[?e ?attr]]}
   :args [db attribute]})
