(ns hello-datahike.pull
  (:require
   [datahike.api :as d]
   [hello-datahike.common :as c]))


;; pull API

;; https://docs.datomic.com/on-prem/query/pull.html


;; prepare the db (in-memory)

(comment

  (def cfg {:store              {:backend :mem
                                 :id      "pull-api-tutorial"}
            :schema-flexibility :write
            :keep-history?      true
            :attribute-refs?    true})

  ;; create the db and connect to it
  (def conn (c/connect! cfg))

  ;; there are 20 movies in the db now
  (count (d/q '[:find ?title
                :where
                [_ :movie/title ?title]]
              @conn))

)


;; overview

(comment

  ;; pull is a declarative way to make hierarchical (and possibly nested)
  ;; selections of information about entities. Pull applies a pattern to a
  ;; collection of entities, building a map for each entity.

  ;; pull is available as a standalone API (`d/pull`, `d/pull-many`) and as a
  ;; find specification in the query API (`d/q`).

  ;; patterns support forward and reverse attribute navigation, wildcarding,
  ;; nesting, recursion, naming control, defaults, and limits on the results
  ;; returned. Entities can be passed to pull by any kind of entity identifier:
  ;; entity ids, idents, or lookup refs.


  ;; `pull`

  ;; fetch data from database using recursive declarative description. Returns
  ;; plain maps, not lazy. Takes three arguments:

  ;; - a database
  ;; - a pattern
  ;; - an entity identifier

  ;; and returns a map of data about an entity as specified by the pattern.

  (d/pull @conn '[*] 101)
  (d/pull @conn '[:movie/title :movie/year] 101)


  ;; `pull-many`

  ;; is similar, except that it takes a collection of entity identifiers, and
  ;; returns a collection of maps.

  (d/pull-many @conn '[:movie/title :movie/year] [101 102 103])

)


;; reference

(comment

  ;; patterns

  ;; a pattern is a list of *attribute specifications*. An attribute spec
  ;; specifies an attribute to be returned, and (optionally) how that attribute
  ;; should be returned. Attribute specs can be attribute names, wildcards, map
  ;; specs, or attribute expressions.


  ;; attribute names
  ;; - attr-name: an edn keyword that names an attr

  ;; an attribute spec names an attribute, with an optional leading underscore
  ;; on the local name to reverse the direction of navigation.

  (d/pull @conn '[:movie/title] 101)
  (d/pull @conn '[:movie/cast] 101)


  ;; reverse lookup

  ;; an underscore prefix (`_`) on the local name component of an attribute name
  ;; causes the attribute to be navigated in reverse. Note: Attribute names must
  ;; not have a leading underscore. Attributes with a leading underscore can not
  ;; be used with reverse lookup.

  ;; cast for movie
  (d/pull @conn '[:movie/cast] 101)
  ;; movies for cast member (actor)
  (d/pull @conn '[:person/name :movie/_cast] 53)


  ;; map specifications

  ;; - map-spec: { ((attr-name | limit-expr) (pattern | recursion-limit))+ }
  ;; - limit-expr: [("limit" | 'limit') attr-name (positive-number | nil)]
  ;; - recursion-limit: positive-number | '...'

  ;; you can explicitly specify the handling of referenced entities by using a
  ;; map instead of just an attribute name. The simplest map specification is a
  ;; map specifying a specific pattern for a particular attr-name.

  (d/pull @conn '[:movie/title {:movie/cast [:db/id :person/name]}] 101)

  ;; map specs can nest arbitrarily.

  (d/pull @conn
          '[:movie/title
            :movie/year
            {:movie/cast [:person/name]}
            {:movie/sequel [:movie/title
                            :movie/year
                            {:movie/cast [:person/name]}]}]
          100)


  ;; attribute specifications

  ;; the attribute specification provides control of various aspects of the
  ;; values returned by the pull of an attribute.

  ;; the `:as` option allows replacement of the key for an attribute result map
  ;; with an arbitrary value. Note that the pattern appears in a seq. This
  ;; necessitates that the whole clause be quoted or that the pattern is in a
  ;; vector.

  (d/pull @conn '[(:movie/title :as :film)] 101)

  ;; the `:limit` option controls how many values will be returned for a
  ;; cardinality-many attribute. A limit can be a positive number or nil. A nil
  ;; limit causes all values to be returned, and should be used with caution.

  ;; in the absence of an explicit limit, pull will return the first 1000 values
  ;; for a cardinality-many attribute.

  (d/pull @conn '[:movie/title [:movie/cast :as :crew :limit 2]] 101)

  (d/pull @conn '[:movie/title [:movie/cast :as :crew :limit nil]] 101)

  (d/pull @conn '[[:movie/title :as :film]
                  {[:movie/cast :as :crew :limit 2] [:person/name]}] 101)

  ;; the `:default` option specifies a value to use if an attribute is not
  ;; present for an entity.

  (d/pull @conn '[:movie/title
                  [:trivia :default "nothing interesting here"]] 101)

  ;; the `:xform` option provides the ability to transform the value returned by
  ;; pull for an attribute.

  ;; seems like Datahike does not support `:xform` (yet?)

  ;; (d/pull @conn '[:movie/year] 101)
  ;; (d/pull @conn '[[:movie/year :xform str]] 101)

  ;; the wildcard specification `*` pulls all attributes of an entity, and
  ;; recursively pulls any *component attributes*:

  (d/pull @conn '[*] 101)

  ;; a map specification can be used in conjunction with the wildcard to provide
  ;; subpatterns for specific attributes.

  (d/pull @conn '[* {:movie/director [:person/name]}] 101)

  ;; a map specification can govern recursion. If a map specification has a
  ;; numeric value, then the selector containing that specification will be
  ;; applied recursively up to N times. The ellipsis symbol (`...`) will follow
  ;; recursive references to arbitrary depth, and should be used with caution!

  ;; if a recursive subselect encounters an entity that it has already seen, it
  ;; will not apply the pattern, instead returning only the :db/id of the
  ;; entity. Thus recursive select is safe in the presence of cycles.

  (d/pull @conn '[:movie/title {:movie/sequel 1}] 100)
  (d/pull @conn '[:movie/title {:movie/sequel ...}] 100)


  ;; empty results

  ;; if there is no match between a pattern and an entity, then pull will
  ;; return nil (not an empty map):

  (d/pull @conn '[:weird-attr] 100)

  ;; non-matching results will be removed entirely from collections.

  (d/pull @conn '[:movie/title {:movie/cast [:person/name]}] 100)
  (d/pull @conn '[:movie/title {:movie/cast [:weird-attr]}] 100)


  ;; pull results

  ;; if a pull attr-name names a *reference attribute*, pull will return a map
  ;; for the referenced value. If the attribute is a *component attribute*, the
  ;; map will contain all attributes of the related entity as well.

  ;; a movie has no component attributes, only references
  (d/pull @conn '[*] 101)

  ;; so do a person
  (d/pull @conn '[* :movie/_cast] 53)

  ;; multiple results

  ;; if navigating an attribute might lead to more than one value, the pull
  ;; result will be a list of the values found. These cases include:
  ;; - all forward cardinality-many references
  ;; - reverse references for non-component attributes

  (d/pull @conn '[:movie/cast] 100)
  (d/pull @conn '[:movie/_cast] 53)

  ;; missing attributes

  ;; in the absence of a default, attribute specifications that do not match an
  ;; entity are omitted from that entity's result map, rather than
  ;; e.g. appearing with a nil value.

  (d/pull @conn '[:movie/title :weird-attr?] 100)


  ;; pull API vs. entity API

  ;; the pull API has two important advantages over the entity API:

  ;; pull uses a declarative, data-driven spec, whereas entity encourages
  ;; building results via code. Data-driven specs are easier to build,
  ;; compose, transmit and store. Pull patterns are smaller than entity code
  ;; that does the same job, and can be easier to understand and maintain.

  ;; pull API results match standard collection interfaces (e.g. Java maps) in
  ;; programming languages, where Entity results do not. This eliminates the
  ;; need for an additional allocation/transformation step per entity.

)


;; `d/q`: pull patterns in queries

(comment

  ;; an input can be a *pull pattern*, which can be named by a symbol in the
  ;; `:in` clause, and that name can be used in pull expressions in the `:find`
  ;; clause.

  (def movie-title-only-pattern
    '[:movie/title])

  (def movie-with-director-pattern
    '[:movie/title {:movie/director [:person/name :person/born]}])

  (def movie-with-cast
    '[:movie/title {:movie/cast [:person/name]}])

  (def movie-with-cast-transformed
    '[[:movie/title :as :title]
      {[:movie/cast :as :actors] [[:person/name :as :name]]}])


  (def movies-by-year-query
    '[:find (pull ?m pattern)
      :in $ ?year pattern
      :where
      [?m :movie/year ?year]])

  (d/q movies-by-year-query
       @conn
       1979
       movie-title-only-pattern)

  (d/q movies-by-year-query
       @conn
       1979
       movie-with-director-pattern)

  (d/q movies-by-year-query
       @conn
       1979
       movie-with-cast)

  (d/q movies-by-year-query
       @conn
       1979
       movie-with-cast-transformed)

  ;; separation of concerns

  ;; the pull API separates the process of finding entities and acquiring
  ;; information about the entities. Pull expressions allow you to utilize
  ;; queries to find entities and return an explicit map with the desired
  ;; information about each entity, i.e., the same query will return different
  ;; results when provided with different pull patterns.

)
