(ns hello-datahike.api
  (:require
   [datahike.api :as d]
   [hello-datahike.common :as c])
  (:import (java.util Date)))


;; Datahike API


;; prepare the db instance for examples

;; define the db configuration
(def cfg {:store              {:backend :mem
                               :id      "datahike-api-reference"}
          :schema-flexibility :write
          :keep-history?      true
          :attribute-refs?    false})

;; create the db and connect to it
(def conn (c/connect! cfg {:schema-file "schema.person.edn"
                           :data-file nil}))


;; managing database instance and connection

;; - `database-exists?`: check if database instance already exists
;; - `create-database`: create a database instance
;; - `delete-database`: delete a database instance
;; - `connect`: get connection to a database instance (a reference to the latest
;;   database value)
;; - `release`: release connection to a database instance

(comment

  (d/create-database cfg)

  (def conn (d/connect cfg))

  (d/release conn)

  (d/delete-database cfg)

)


;; transacting data

;; - `transact`: apply transaction data to db instance, update the connection
;;   to refer to the result of the transaction (latest database value)
;; - `tempid`: get temporary entity id (negative integer). For Datomic API
;;   compatibility, prefer using negative integers directly
;; - `listen`: add listener for transactions (tx-reports) on the connection.
;; - `unlisten`: remove the listener

(comment

  (def add-bob-tx-data
    [{:db/id          -1
      :person/name    "Bob"
      :person/email   "bob@example.com"
      :person/born    (Date. 107 7 23)
      :person/aliases ["Bobbie" "Curly" "Rob"]}])

  (d/listen conn :some-key println)

  (def add-bob-tx-report (d/transact conn add-bob-tx-data))

  (d/unlisten conn :some-key)

  (def add-bob-tx-id (-> add-bob-tx-report :db-after :max-tx))
  (def bob-id (d/q '[:find ?p .
                     :where
                     [?p :person/email "bob@example.com"]]))

  (d/tempid :db/part.user)
  (d/tempid :db/part.user -1)

)


;; handling db values (acquiring, filtering, getting info about etc.)

;; - `db`: get latest db value from connection (same as @conn). For Datomic
;;   API compatibility, prefer @conn.
;; - `with`: as `transact`, but does not affect the db state
;; - `db-with`: get db value with tx-data applied to another db value
;;   (as if db has additional facts)

;; - `history`: get db value with all historical data
;; - `as-of`: get db value at a given time (inst or tx id)
;; - `since`:  get db value since a given time (inst or tx id)
;; - `filter`:
;; - `is-filtered`: check if db was filtered using `filter`

(comment

  ;; acquiring db values

  @conn
  (d/db conn)

  (def alice-tx-data {:tx-data [{:db/id -1
                                 :person/name "Alice"
                                 :person/email "alice@example.com"
                                 :person/born (Date. 119 4 12)
                                 :person/aliases "Foxy"}]})
  (d/with @conn alice-tx-data)

  (def with-alice-db (d/db-with @conn alice-tx-data))
  (d/q (c/pull-all with-alice-db :person/name))

  ;; filtering db

  (d/history @conn)

  (d/as-of @conn add-bob-tx-id)

  (d/since @conn add-bob-tx-id)

  (def no-bob-db (d/filter @conn (fn [db datom]
                                   (not= bob-id (.e datom)))))
  (d/is-filtered no-bob-db)

  (d/q (c/pull-all no-bob-db :person/name))

  ;; getting schema of the db

  (d/schema @conn)

  (d/reverse-schema no-bob-db)

  ;; getting metrics on the latest (unfiltered) db

  (d/metrics @conn)

)


;; querying the database

;; - `q`: executes a datalog query (usually to find some entities in db)
;; - `query-stats`: same as `q`, but also returns some execution details
;; - `pull`: get entity data from using declarative description of it's shape
;; - `pull-many`: same as `pull`, but for many entities at once
;; - `entity`: get a lazy reference to entity

(comment

  (def bob-id-query '[:find ?p .
                      :in $ ?email
                      :where
                      [?p :person/email ?email]])

  (def bob-id (d/q bob-id-query @conn "bob@example.com"))

  (d/query-stats bob-id-query @conn "bob@example.com")

  (d/pull @conn [:person/name :person/aliases] bob-id)

  (d/pull-many @conn [:person/name :person/aliases] [bob-id])

  (def bob (d/entity @conn bob-id))
  (:person/name bob)
  (:person/born bob)
  (:person/aliases bob)

)


;; accessing database indexes

;; - `datoms`: get a sequence of datoms from given index and components (e, a, v)
;; - `seek-datoms`: like `datoms`, but get datoms starting  from given
;;   components to the rest of the db

(comment

  (d/datoms @conn :eavt 6 :person/name)

  (d/seek-datoms with-alice-db :aevt :person/name 7 "Alice")

)
