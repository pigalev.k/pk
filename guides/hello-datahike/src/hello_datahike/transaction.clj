(ns hello-datahike.transaction
  (:require
   [datahike.api :as d]
   [hello-datahike.common :as c])
  (:import (java.util Date)))


;; transactions API

;; https://docs.datomic.com/on-prem/transactions/transactions.html


;; prepare the db (in-memory)

(comment

  (def cfg {:store              {:backend :mem
                                 :id      "transaction-api-tutorial"}
            :schema-flexibility :write
            :keep-history?      true
            :attribute-refs?    false})

  ;; create the db and connect to it
  ;; apply the schema, but do not load the data
  (def conn (c/connect! cfg {:schema-file "schema.person.edn"
                             :data-file nil}))

  ;; there are no persons in the db now
  (count (d/q '[:find ?p
                :where
                [?p :person/name]]
              @conn))

  ;; but the person schema is present
  (:person/name (d/schema @conn))

)


;; overview

(comment

  ;; transactions

  ;; all writes to Datomic databases are protected by ACID
  ;; transactions. Transactions are submitted to the system's Transactor
  ;; component, which processes them serially and reflects changes out to all
  ;; connected peers.


  ;; transaction structure

  ;; Datomic represents transaction requests as data structures. This is a
  ;; significant difference from SQL database, where requests are submitted as
  ;; strings. Using data instead of strings makes it easier to build requests
  ;; programmatically.

  ;; transaction data is simply a list of lists and/or maps, each of which is a
  ;; statement in the transaction.


  ;; `transact`

  ;; transactions are executed by `transact` function, that accepts a
  ;; connection and transaction data.

)


;; reference

(comment

  ;; transaction data


  ;; list form

  ;; each list a transaction contains represents either the addition or
  ;; retraction of a specific fact about an entity, attribute, and value, or the
  ;; invocation of a data function, as shown below.

  ;; `[:db/add entity-id attribute value]`

  ;; `[:db/retract entity-id attribute value]`
  ;; `[:db/retract entity-id attribute]`

  ;; `[data-fn args*]`

  (def add-alice-tx-data
    [[:db/add -1 :person/name "Alice"]
     [:db/add -1 :person/email "alice@example.com"]
     [:db/add -1 :person/born (Date. 119 4 12)]
     [:db/add -1 :person/aliases "Foxy"]])

  (d/transact conn add-alice-tx-data)

  ;; test that the person was added
  (d/q (c/pull @conn :person/name "Alice"))


  ;; map form

  ;; each map a transaction contains is equivalent to a set of one or more
  ;; `:db/add` operations. The map may include a `:db/id` key identifying the
  ;; entity data that the map refers to. It may additionally include any number
  ;; of attribute, value pairs. Cannot be used for retraction.

  ;; `{:db/id entity-id
  ;;   attribute value
  ;;   attribute value
  ;;   ... }`

   (def add-bob-tx-data
    [{:db/id          -1
      :person/name    "Bob"
      :person/email   "bob@example.com"
      :person/born    (Date. 107 7 23)
      :person/aliases ["Bobbie" "Curly" "Rob"]}])

  (d/transact conn add-bob-tx-data)

  ;; test that the person was added
  (d/q (c/pull @conn :person/name "Bob"))

  ;; internally, the map structure gets transformed to the list structure. Each
  ;; attribute/value pair becomes a `:db/add` list. If you did not provide an
  ;; `entity-id`, Datomic will generate a temporary entity-id for you.

  ;; `[:db/add entity-id attribute value]`
  ;; `[:db/add entity-id attribute value]`
  ;; ...

  ;; the map structure is supported as a convenience when adding data. As a
  ;; further convenience, the attribute keys in the map may be either keywords
  ;; or strings.

  ;; in Datahike `transact` can accept a single map argument like this:

  (def add-charlie-tx-data
    {:tx-data
     [{:db/id          -1
       :person/name    "Charlie"
       :person/email   "charlie@example.com"
       :person/born    (Date. 94 11 11)
       :person/aliases ["Choo-Choo"]}]})

  (d/transact conn add-charlie-tx-data)

  ;; test that the person was added
  (d/q (c/pull @conn :person/name "Charlie"))


  ;; identifying entities

  ;; there are three ways to specify an entity id:

  ;; - a temporary id for a new entity being added to the database
  ;; - an existing id for an entity that's already in the database
  ;; - an identifier for an entity that's already in the database


  ;; temporary ids

  ;; when you are adding data to a new entity, you identify it using a temporary
  ;; id. Temporary ids get resolved to actual entity ids when a transaction is
  ;; processed.


  ;; creating tempids

  ;; the simplest kind of temporary id is a string that follows these rules:

  ;; - a temporary id cannot begin with a colon (:) character
  ;; - the temporary id "datomic.tx" always identifies the current transaction
  ;; - other strings beginning with "datomic" are reserved for future use
  ;;   by Datomic

  ;; in the transaction data below, the temporary id "dana" is used to indicate
  ;; that the two datoms are both about the same entity:

  (def add-dana-tx-data
    [[:db/add "dana" :person/name "Dana"]
     [:db/add "dana" :person/email "dana@example.com"]
     [:db/add "dana" :person/born (Date. 89 3 5)]])

  (d/transact conn add-dana-tx-data)

  ;; test that the person was added
  (d/q (c/pull @conn :person/name "Dana"))


  ;; temporary id resolution

  ;; when a transaction containing temporary ids is processed, each unique
  ;; temporary id is mapped to an actual entity id. If a given temporary id is
  ;; used more than once in a given transaction, all instances are mapped to the
  ;; same actual entity id.

  ;; in general, unique temporary ids are mapped to new entity ids. However,
  ;; there is one exception. When you add a fact about a new entity with a
  ;; temporary id, and one of the attributes you specify is defined as
  ;; `:db/unique` `:db.unique/identity`, the system will map your temporary id
  ;; to an existing entity if one exists with the same attribute and
  ;; value (update) or will make a new entity if one does not
  ;; exist (insert). All further adds in the transaction that apply to that same
  ;; temporary id are applied to the "upserted" entity.


  ;; existing entity ids

  ;; to add, modify or retract data about existing entities in a transaction,
  ;; you must know the entity id. You can retrieve a specific entity id by
  ;; querying the database for an external key (see Schema for information about
  ;; external keys).

  ;; for example, this query retrieves the id of an existing entity based on an
  ;; email address.

  (def alice-id
    (d/q '[:find ?e .
           :in $ ?email
           :where
           [?e :person/email ?email]]
         @conn
         "alice@example.com"))

  ;; if the entity id returned by the query is `alice-id` (e.g. 5), the
  ;; following transaction data will add the value to the entity's
  ;; `:person/aliases` attribute:

  (d/transact conn [{:db/id          alice-id
                     :person/aliases "Allie"}])

  ;; test that the value was updated
  (d/q (c/pull @conn :person/name "Alice"))

  ;; if the entity in question has a unique identifier, you can specify the
  ;; entity id by using a lookup ref. Rather than querying the database, you can
  ;; provide the unique attribute, value pair corresponding to the entity you
  ;; want to assert or retract a fact for. Note that a lookup ref specified in a
  ;; transaction will be resolved by the transactor.

  (d/transact conn [{:db/id          [:person/email "alice@example.com"]
                     :person/aliases "Bunny"}])

  ;; test that the value was updated
  (d/q (c/pull @conn :person/name "Alice"))


  ;; entity identifiers (probably Datomic-only)

  ;; the system defines a special attribute, `:db/ident`, that can be used to
  ;; assign an keyword identifier to a given entity. If an entity has a
  ;; `:db/ident` attribute, its value can be used in place of the entity's id.

  ;; this mechanism is what allows you to refer to attributes, partitions,
  ;; types, etc., by specifying keywords.

  ;; you can also use `:db/ident` to define entities representing enumerated
  ;; values that can then be referred to by name (as described in Schema).

  ;; in the example below, the entity is specified by the ident `:person/SSN`:

  ;; `[:db/add :person/SSN :db/doc "A person's Social Security Number"]`


  ;; building transactions

  ;; this section explains how to build transactions to add, modify and retract
  ;; facts. Each example shows a single transaction, but you can combine adding
  ;; and retracting in a single transaction if desired.


  ;; adding data to a new entity

  ;; to add data to a new entity, build a transaction using `:db/add` implicitly
  ;; with the map structure (or explicitly with the list structure), a temporary
  ;; id, and the attributes and values being added.

  ;; this example builds a transaction that creates a new entity with two
  ;; attributes, `:person/name` and `:person/email`.

  (def add-eve-tx-data
    [{:person/name  "Eve"
      :person/email "eve@example.com"}])

  (d/transact conn add-eve-tx-data)

  ;; test that the entity was created
  (d/q (c/pull @conn :person/name "Eve"))

  ;; note that there is no requirement about which attributes are added to which
  ;; entities, this is left entirely up to your application. This provides a
  ;; great deal of flexibility as your system evolves.


  ;; adding data to an existing entity

  ;; to add data to an existing entity, build a transaction using `:db/add`
  ;; implicitly with the map structure (or explicitly with the list structure),
  ;; an existing entity id or entity identifier, and the attributes and values
  ;; being added.

  ;; this example queries for an existing entity id and uses it in a new
  ;; transaction to change the value of the entity's `:person/name` attribute
  ;; to "Robert".

  (def bob-id (d/q '[:find ?e .
                     :in $ ?email
                     :where [?e :person/email ?email]]
                   @conn
                   "bob@example.com"))

  (def update-bob-name-tx-data
    [{:db/id       bob-id
      :person/name "Robert"}])

  (d/transact conn update-bob-name-tx-data)

  ;; test that the name was changed
  (d/q (c/pull @conn :person/email "bob@example.com"))

  ;; the following example uses a lookup ref to perform the same transaction.

  (def update-robert-name-tx-data
    [{:db/id       [:person/email "bob@example.com"]
      :person/name "Bob"}])

  (d/transact conn update-robert-name-tx-data)

  ;; test that the name was changed again
  (d/q (c/pull @conn :person/email "bob@example.com"))


  ;; adding entity references

  ;; attributes of reference type allow entities to refer to other
  ;; entities. When a transaction adds an attribute of reference type to an
  ;; entity, it must specify an entity id as the attribute's value. The entity
  ;; id specified for the value may be a temporary id (if the entity being
  ;; referred to is being created by the same transaction) or a real entity
  ;; id (if the entity being referred to already exists in the database).

  ;; this example shows the literal representation of a transaction that creates
  ;; two new entities, people named "Bob" and "Alice". Each entity has a
  ;; reference to the other, connected using temporary ids, "bobid"
  ;; and "aliceid", respectively. All instances of a given temporary id within a
  ;; transaction will resolve to a single entity id.

  ;; `[{:db/id         "bobid"
  ;;    :person/name   "Bob"
  ;;    :person/spouse "aliceid"}
  ;;   {:db/id         "aliceid"
  ;;    :person/name   "Alice"
  ;;    :person/spouse "bobid"}]`

  (def add-fannie-and-george-tx-data
    [{:db/id          "fannie"
      :person/name    "Fannie"
      :person/email   "fannie@example.com"
      :person/friends ["george"
                       [:person/email "eve@example.com"]]}
     {:db/id          "george"
      :person/name    "George"
      :person/email   "george@example.com"
      :person/friends ["fannie"]}])

  (d/transact conn add-fannie-and-george-tx-data)

  ;; test that the entities was added correctly
  (d/q (c/pull @conn :person/email "fannie@example.com"))
  (d/q (c/pull @conn :person/email "george@example.com"))
  (d/q '[:find (pull ?p [[:person/name :as :name]
                         {[:person/friends :as :friends]
                          [[:person/name :as :name]]}])
         :in $ [?email ...]
         :where
         [?p :person/email ?email]]
       @conn
       ["fannie@example.com" "george@example.com"])


  ;; cardinality many transactions

  ;; you can transact multiple values for a `:db.cardinality/many` attribute at
  ;; one time using a list. The following example transacts a person named "Bob"
  ;; with multiple aliases:

  (def add-bob-more-aliases-tx-data
    [{:db/id          [:person/email   "bob@example.com"]
      :person/aliases ["Bert" "Bobert" "RobboCop"]}])

  (d/transact conn add-bob-more-aliases-tx-data)

  ;; test that values were added
  (d/q (c/pull @conn :person/email "bob@example.com"))


  ;; nested maps in transactions

  ;; often, a group of related entities are created or modified in the same
  ;; transaction. Nested maps allow you to specify these related entities
  ;; together in a single map. If an entity map contains a nested map as a value
  ;; for a reference attribute, Datomic will expand the nested map into its own
  ;; entity. Nested map expansion is governed by two rules:

  ;; - if the nested map does not include a `:db/id`, Datomic will assign
  ;;   a `:db/id` automatically, using the same partition as the `:db/id`
  ;;   of the outer entity.
  ;; - either the reference to the nested map must be a component attribute,
  ;;   or the nested map must include a unique attribute. This constraint
  ;;   prevents the accidental creation of easily-orphaned entities that
  ;;   have no identity or relation to other entities.

  ;; as an example, the following data uses nested maps to specify two friends
  ;; along with a person:

  ;; `[{:db/id           order-id
  ;;    :order/lineItems [{:lineItem/product  chocolate
  ;;                       :lineItem/quantity 1}
  ;;                      {:lineItem/product  whisky
  ;;                       :lineItem/quantity 2}]}]`

  (def add-henry-and-his-friends-tx-data
    [{:db/id          -1
      :person/name    "Henry"
      :person/email   "henry@example.com"
      :person/aliases ["Sage" "Junkie"]
      :person/friends [{:person/name    "Ivan"
                        :person/email   "ivan@example.com"
                        :person/aliases ["Jr." "Sonny"]}
                       {:person/name    "Jude"
                        :person/email   "jude@example.com"
                        :person/aliases ["Hey"]}]}])

  (d/transact conn add-henry-and-his-friends-tx-data)

  ;; test that values were added
  (d/q (c/pull @conn :person/email "ivan@example.com"))
  (d/q (c/pull @conn :person/email "jude@example.com"))
  (d/q '[:find (pull ?p [[:person/name :as :name]
                         {[:person/friends :as :friends]
                          [[:person/name :as :name]]}])
         :in $ [?email ...]
         :where
         [?p :person/email ?email]]
       @conn ["henry@example.com"])

  ;; notice that the two child entities do not need to specify a `:db/id`, and
  ;; will automatically get ids in the same partition as the new parent entity.

  ;; nested maps are often much more convenient than their equivalent flat
  ;; expansions. In addition to being more verbose, this form requires
  ;; additional work to explicitly manage child entity ids and their connections
  ;; to the parent entity.


  ;; retracting data

  ;; to retract data from an existing entity, build a transaction using
  ;; `:db/retract`, an existing entity id or entity identifier, the attribute,
  ;; and optionally value being retracted. If a value is not provided, all
  ;; values for the provided entity and attribute will be retracted.

  ;; this example queries for an existing entity id and uses it in a new
  ;; transaction to retract the value of the entity's `:person/aliases`
  ;; attribute.

  (def bob-id (d/q '[:find ?e .
                     :in $ ?email
                     :where [?e :person/email ?email]]
                   @conn
                   "bob@example.com"))

  (def retract-bob-alias-tx-data
    [[:db/retract bob-id
      :person/aliases "Curly"]])

  (d/transact conn retract-bob-alias-tx-data)
  ;; test that value was retracted
  (d/q (c/pull @conn :person/email "bob@example.com"))

  ;; the following example accomplishes the same thing with a lookup ref.

  (def retract-bob-another-alias-tx-data
    [[:db/retract [:person/email "bob@example.com"]
      :person/aliases "Bobby"]])

  (d/transact conn retract-bob-another-alias-tx-data)
  ;; test that value was retracted
  (d/q (c/pull @conn :person/email "bob@example.com"))

  ;; and the following retracts all values of the entity's `:person/aliases`
  ;; attribute.

  (def retract-all-ivan-aliases-tx-data
    [[:db/retract [:person/email "ivan@example.com"]
      :person/aliases]])

  (d/transact conn retract-all-ivan-aliases-tx-data)

  ;; test that values were retracted
  (d/q (c/pull @conn :person/email "ivan@example.com"))

  ;; Datomic keeps the values of data over time and allows you to query the
  ;; value of the database as of a point in time. That means it's possible to
  ;; recover data even after it has been retracted, simply by querying a
  ;; database value from the past.

  (d/q (c/pull (d/history @conn) :person/email "ivan@example.com"))


  ;; the Tempid Data Structure (Datomic only)

  ;; note: probably not relevant to Datahike; use negative numbers as tempids

  ;; as an alternative to string tempids, peers (but not clients) can choose to
  ;; make a structural tempid, allowing them to explicitly specify a partition
  ;; for the entity.

  ;; you can make a temporary id by calling the `datomic.Peer.tempid`
  ;; method. The first argument to `Peer.tempid` is the name of the partition
  ;; where the new entity will reside as an argument. There are three partitions
  ;; built into Datomic.

  ;; Partition 	       Purpose
  ;; - `:db.part/db`   schema partition, used only for attributes and partition
  ;;    entities
  ;; - `:db.part/tx`   transaction partition, used only for transaction entities
  ;; - `:db.part/user` user partition, for application entities

  ;; the `:db.part/db` partition should only be used for schema entities, like
  ;; attributes and partitions, not for application data.

  ;; the `:db.part/tx` partition should only be used for transaction entities,
  ;; which are created automatically for each committed transaction.

  ;; you should use `:db.part/user` for your application's entities, or you
  ;; should create one or more partitions of your own, as described in Schemas.

  ;; this code gets a new temporary id in the `:db.part/user` partition.

  ;; `temp_id = Peer.tempid(":db.part/user");`

  ;; each call to tempid produces a unique temporary id.

  ;; there is an overloaded version of tempid that takes a negative number as a
  ;; second argument. This version of tempid creates a temporary id based on the
  ;; number you pass as input. If you invoke it multiple times with the same
  ;; partition and negative number, each invocation will return the same
  ;; temporary id. This can be useful when constructing transactions that add
  ;; references between entities, as explained below.

  ;; in some cases, you may want to store the literal representation of a
  ;; transaction in a file. The literal form can be read with
  ;; `datomic.Util.read` and submitted as a transaction. This is a useful way to
  ;; store a schema definition, for example.

  ;; you can insert temporary ids into the literal representation of a
  ;; transaction using the following syntax:

  ;; `#db/id[partition-name value*]`

  ;; where `partition-name` is the name of a partition in the system and the
  ;; value is an optional negative number.

  ;; when the literal representation of a transaction is parsed, this syntax is
  ;; interpreted and a temporary id is generated.


  ;; default partition (Datomic only)

  ;; string tempids create entities in the `default-partition` for the
  ;; transactor process. You can set the default partition by editing the
  ;; transactor properties file:

  ;; `default-partition=:my.namespace/my-partition`

  ;; if no default partition is specified, the transactor will use the built-in
  ;; `:db.part/user` partition.

)


;; transaction functions

(comment

  ;; transaction functions

  ;; Datomic can invoke your functions as part of transaction
  ;; processing. Functions written for this purpose are called transaction
  ;; functions.


  ;; `:db/retractEntity`

  ;; takes an entity id as an argument. It retracts all the attribute values
  ;; where the given id is either the entity or value, effectively retracting
  ;; the entity's own data and any references to the entity as well. Entities
  ;; that are components of the given entity are also recursively retracted.

  ;; the following example transaction data retracts an entity by a lookup
  ;; ref. Note that references to retracted entity in other entities will also
  ;; be retracted (see Henry example above).

  (def retract-ivan-tx-data
    [[:db/retractEntity [:person/email "ivan@example.com"]]])

  (d/transact conn retract-ivan-tx-data)

  ;; test that the entity was retracted
  (d/q (c/pull @conn :person/email "ivan@example.com"))


  ;; `:db/cas` (compare-and-swap)

  ;; takes four arguments: an entity id, an attribute, an old value, and a new
  ;; value. The attribute must be `:db.cardinality/one`. If the entity has the
  ;; old value for attribute, then the new value will be asserted. Otherwise,
  ;; the transaction will abort and throw an exception.

  ;; you can use nil for the old value to specify that the new value should be
  ;; asserted only if no value currently exists.

  ;; the following example transaction data sets Bob's `:person/name`
  ;; to "Robert", if and only if `:person/name` is "Bob" at the time the
  ;; transaction executes:

  (def update-bob-name-cas-fail-tx-data
    [[:db/cas [:person/email "bob@example.com"]
      :person/name "Nope" "Robert"]])

  ;; throws, no changes
  (d/transact conn update-bob-name-cas-fail-tx-data)

  (def update-bob-name-cas-success-tx-data
    [[:db/cas [:person/email "bob@example.com"]
      :person/name "Bob" "Robert"]])

  ;; success
  (d/transact conn update-bob-name-cas-success-tx-data)

  ;; test that the name was changed
  (d/q (c/pull @conn :person/email "bob@example.com"))


  ;; custom transaction functions (Datomic-only)

  ;; in addition to using the built-in transactions below, you can also write
  ;; your own transaction functions.


  ;; creating and installing a transaction function

  ;; transaction functions must adhere to the following requirements:

  ;; - must be pure functions, i.e. free of side effects.
  ;; - must expect to be passed a database value as its first argument.
  ;;   This is to allow transaction function to issue queries etc.
  ;; - must return transaction data in the same form as expected by
  ;;   `transact`.
  ;; - if a transaction function throws an exception, Datomic will abort
  ;;   the entire transaction.


  ;; using transaction functions

  ;; a transaction function call is a vector whose first element is the name of
  ;; the transaction function, and whose subsequent elements are the function's
  ;; arguments.

  ;; the following example installs and invokes a trivial database transaction
  ;; function:

  ;; tx-data to install the function
  ;; `[{:db/ident :add-doc
  ;;    :db/fn (d/function
  ;;            {:lang "clojure"
  ;;             :params '[db e doc]
  ;;             :code [[:db/add 'e :db/doc 'doc]]})}]`

  ;; tx-data to call the function
  ;; `[[:add-doc "foo" "this is foo's doc"]]`

  ;; the example below installs and invokes an equivalent classpath transaction
  ;; function:

  ;; put this on the transactor's classpath
  ;; `(ns my.fns)
  ;;  (defn add-doc
  ;;    [db e doc]
  ;;    [[:db/add e :db/doc doc]])`

  ;; and then put this tx-data in a transaction
  ;; `[[my.fns/add-doc "foo" "this is foo's doc"]]`


  ;; processing transaction functions

  ;; the transaction processor will lookup the function in its `:db/fn`
  ;; attribute, and then invoke it, passing the value of the db (currently, as
  ;; of the beginning of the transaction), followed by the arguments -
  ;; e.g. f.invoke(db, :foo, "this is foo's doc"). It will then take the result
  ;; of the call (which is a list of transaction data), and 'splice' it into the
  ;; transaction where the call was made. The result might contain several
  ;; transaction entries, and some of them may be transaction function
  ;; calls. The transaction processor will call these in turn, until the
  ;; expansion consists only of `:db/add`s and `:db/retract`s.


  ;; uses for transaction functions

  ;; transaction functions run on the transactor inside of transactions, and
  ;; thus can atomically analyze and transform database values. You can use them
  ;; to ensure atomic read-modify-update processing, and integrity
  ;; constraints. (To abort a transaction, simply throw an exception). If you
  ;; frequently need to create entities with a particular 'shape' you can make
  ;; constructor-like transaction functions. A transaction function can issue
  ;; queries on the db value it is passed, and can perform arbitrary logic in
  ;; the programming language. Note, however, that transaction functions must be
  ;; pure functions and cannot be used to produce effects on the transactor.


  ;; limitations of transaction functions

  ;; - transaction functions must be pure functions and cannot be used to
  ;;   produce effects on the transactor.
  ;; - Transaction functions are serialized by design. To achieve best
  ;;   performance, limit the work of transaction functions to only things
  ;;   that require transaction-time access to the current value of the database.
  ;; - fressian serialization between tx and peer guarantees only the Java
  ;;   collection interfaces

)
