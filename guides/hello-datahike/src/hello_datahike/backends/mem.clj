(ns hello-datahike.backends.mem
  (:require
   [datahike.api :as d]
   [datahike.migrate :as m]))


;; in-memory db backend (the default)

;; basics

(comment

  ;; use the default configuration:
  ;; `{:store {:backend :mem :id "default"}}`

  ;; create a database, per default configuration we enforce a strict schema and
  ;; keep all historical data
  (d/create-database)

  (def conn (d/connect))

  ;; the first transaction will be the schema we are using you may also add this
  ;; within database creation by adding `:initial-tx` to the configuration
  (d/transact conn [{:db/ident :name
                     :db/valueType :db.type/string
                     :db/cardinality :db.cardinality/one }
                    {:db/ident :age
                     :db/valueType :db.type/long
                     :db/cardinality :db.cardinality/one }])

  ;; lets add some data and wait for the transaction
  (d/transact conn [{:name  "Alice", :age   20 }
                    {:name  "Bob", :age   30 }
                    {:name  "Charlie", :age   40 }
                    {:age 15 }])

  ;; search the data
  (d/q '[:find ?e ?n ?a
         :where
         [?e :name ?n]
         [?e :age ?a]]
       @conn)

  ;; add new entity data using a hash map
  (d/transact conn {:tx-data [{:db/id 3 :age 25}]})

  ;; if you want to work with queries like in
  ;; https://grishaev.me/en/datomic-query/,
  ;; you may use a hashmap
  (d/q {:query '{:find [?e ?n ?a ]
                 :where [[?e :name ?n]
                         [?e :age ?a]]}
        :args [@conn]})

  ;; query the history of the data
  (d/q '[:find ?a
         :where
         [?e :name "Alice"]
         [?e :age ?a]]
       (d/history @conn))

  ;; you might need to release the connection for specific stores, e.g. for
  ;; leveldb
  (d/release conn)

  ;; clean up the database if it is not needed anymore
  (d/delete-database)

)

;; migration & backup

(comment

  ;; dump the db contents into a file before deleting the db
  (m/export-db conn "/tmp/eavt-dump-mem")

  ;; ... setup new connection to db with correct schema
  (d/create-database)
  (def new-conn (d/connect))

  ;; then import the dump
  (m/import-db new-conn "/tmp/eavt-dump-mem")

  ;; query the new db to verify that export and import worked
  (d/q '[:find ?e ?n ?a
         :where
         [?e :name ?n]
         [?e :age ?a]]
       @new-conn)

)
