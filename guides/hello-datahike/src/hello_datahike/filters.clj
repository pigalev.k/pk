(ns hello-datahike.filters
    (:require
     [datahike.api :as d]
     [hello-datahike.common :as c])
    (:import (java.util Date)))


;; filters

;; https://docs.datomic.com/on-prem/time/filters.html


;; prepare the db (in-memory)

;; prepare the db (in-memory)

(comment

  (def cfg {:store              {:backend :mem
                                 :id      "filters-tutorial"}
            :schema-flexibility :write
            :keep-history?      true
            :attribute-refs?    false})

  ;; create the db and connect to it
  ;; apply the schema, but do not load the data
  (def conn (c/connect! cfg {:schema-file "schema.person.edn"
                             :data-file nil}))

  ;; there are no persons in the db now
  (count (d/q '[:find ?p
                :where
                [?p :person/name]]
              @conn))

  ;; but the person schema is present
  (:person/name (d/schema @conn))

)


;; overview

(comment

  ;; database filters

  ;; Filters take a database value and return a new database value that exposes
  ;; only datoms that satisfy a predicate. This makes it possible to have a
  ;; single set of queries and index traversals that can be used without change
  ;; against different filtered views of your data.

  ;; Datomic databases can be filtered with the time-based predicates `as-of` and
  ;; `since`. Or, you can bring your own predicate with `filter`. In addition,
  ;; you can get an unfiltered view of all history via `history`.

  (def add-alice-tx-data
    [{:db/id  -1 :person/name "Alice"
      :person/email "alice@example.com"
      :person/born (Date. 119 4 12)
      :person/aliases "Foxy"}])

  (def add-alice-tx-report (d/transact conn add-alice-tx-data))
  (def add-alice-tx-id (get-in add-alice-tx-report [:db-after :max-tx]))

  (def add-bob-tx-data
    [{:db/id          -1
      :person/name    "Bob"
      :person/email   "bob@example.com"
      :person/born    (Date. 107 7 23)
      :person/aliases ["Bobbie" "Curly" "Rob"]}])

  (def add-bob-tx-report (d/transact conn add-bob-tx-data))
  (def add-bob-tx-id (get-in add-bob-tx-report [:db-after :max-tx]))

  (def add-charlie-tx-data
    {:tx-data
     [{:db/id          -1
       :person/name    "Charlie"
       :person/email   "charlie@example.com"
       :person/born    (Date. 94 11 11)
       :person/aliases ["Choo-Choo"]}]})

  (def add-charlie-tx-report (d/transact conn add-charlie-tx-data))
  (def add-charlie-tx-id (get-in add-charlie-tx-report [:db-after :max-tx]))

  ;; Alice, Bob And Charlie are all here

  (d/q (c/pull-all @conn :person/name))


  ;; `as-of`

  ;; an `as-of` filter returns a database "as of" a particular point in time,
  ;; ignoring any transactions after that point. The time specification can be
  ;; any time-point, i.e.

  ;; - a Datomic transaction id. Use a transaction id when you want
  ;;   a database as of a specific transaction.
  ;; - a Datomic point in time. Use with, e.g., the basis-t value of the
  ;;   `:db-after` returned by derefing the future returned by transact
  ;;   (Datomic only).
  ;; - an instant in time (a `java.util.Date`). An instant should be used
  ;;   when you have a wall clock time for the database you want but do not
  ;;   have a transaction id or basis-t value, as an instant is not as
  ;;   precise as Datomic's t or tx values.

  ;; only that data is visible that were added *before* the Alice was added,
  ;; including her

  (def before-alice-added (d/as-of @conn add-alice-tx-id))

  ;; only Alice is here

  (d/q (c/pull-all before-alice-added :person/name))


  ;; `since`

  ;; a `since` filter is the opposite of `as-of`. Taking the same point-in-time
  ;; arguments, `since` returns a value of the database that includes only
  ;; datoms added by transactions after that point in time.

  ;; there is an important subtlety to consider when using `since`. Typically,
  ;; the identifying information used to lookup an entity is established in the
  ;; first transaction about that entity, so a `since` view may not be able to
  ;; see the information needed for lookup.

  ;; most callers of `since` will refer to the database twice: one reference to
  ;; the default "now" db to find entities, and a second reference to the since
  ;; db to shave off the past, e.g. "now" db is used to resolve the lookup ref,
  ;; and the since db is then used to build the entity.

  ;; (d/q ('[:find ?count
  ;;         :in $ $since ?id
  ;;         :where [$ ?e :item/id ?id]
  ;;         [$since ?e :item/count ?count]])
  ;;      db-now db-since-2014 "DLC-042")

  ;; only that data is visible that were added *after* the Bob was added,
  ;; inclusive

  (def after-bob-added (d/since @conn add-bob-tx-id))

  ;; Alice is not here

  (d/q (c/pull-all after-bob-added :person/name))


  ;; `history`

  ;; the history view of the database includes the present and all of the past,
  ;; unfiltered. This makes the history view ideal for querying the complete
  ;; history of an entity, or group of entities.

  ;; simplified views of data that do not account for time are incompatible with
  ;; history, since a history database can see multiple different values for
  ;; the "same" fact at different times. In particular, an entity is a
  ;; point-in-time view and cannot be created from a history database.

  (def retract-charlie-tx-data
    [[:db/retractEntity [:person/email "charlie@example.com"]]])

  (d/transact conn retract-charlie-tx-data)

  ;; Charlie is gone from current db

  (d/q (c/pull-all @conn :person/name))

  ;; but still can be found in history

  (d/q (c/pull-all (d/history @conn) :person/name))


  ;; `filter`

  ;; a filter view allows you to produce values of the database filtered by your
  ;; own domain-specific predicates.


  ;; filtering errors

  ;; a filter can be used to e.g. remove datoms that are incorrect, not
  ;; applicable at a point in time, or not available for security reasons.

  ;; for example, suppose that the person with alias "Choo-Choo" was added by
  ;; mistake. This person's entity could be identified and filtered out:

  (defn correct?
    [db datom]
    (let [entity-id (.e datom)
          charlie-id (d/q '[:find ?p .
                            :in $
                            :where [?p :person/aliases "Choo-Choo"]]
                          db)]
      (not (= entity-id charlie-id))))

  (def corrected-db (d/filter (d/history @conn) correct?))

  ;; now, the corrected database value can be passed to all our existing
  ;; queries, which will never see or consider the filtered out datoms.

  ;; no Charlie

  (d/q (c/pull-all corrected-db :person/name))


  ;; filtering for security

  ;; imagine that you want to exclude all values of an attribute entirely from
  ;; consideration.

  (defn secure?
    [_ datom]
    (not (= (.a datom) :person/born)))

  (def secure-db (d/filter (d/history @conn) secure?))

  ;; no birth date

  (d/q (c/pull-all secure-db :person/name))


  ;; joining different filters of the same database

  ;; it is idiomatic to join different filters of the same database. One
  ;; motivating case is performance: Since filtering is not free, it makes sense
  ;; to filter only the parts of the query that need filtering. For example, the
  ;; query below uses an unfiltered database to find entities, and then a
  ;; filtered version of the same database to limit the attributes visible
  ;; through the entity() call:

  ;; [:find ?ent
  ;;  :in $plain $filtered ?email
  ;;  :where
  ;;  [$plain ?e :user/email ?email]
  ;;  [(datomic.api/entity $filtered ?e) ?ent]]

  ;; filtering on transaction attributes

  ;; filters can do more than look at single datoms. They can also consider
  ;; datoms in the context of the entire database. For example, imagine that you
  ;; mark the transactions in your system with a :source/confidence field that
  ;; indicates your confidence in the source, on a scale from 0 to 100. You
  ;; could then filter the database by confidence value.

  ;; queries using this filter can focus on finding data of interest, without
  ;; worrying about the cross-cutting concern of how trusted the data is.


  ;; usage considerations

  ;; `as-of` is not a branch

  ;; filters are applied to an unfiltered database value obtained from `db` or
  ;; `with`. In particular, the combination of `with` and `as-of` means "with
  ;; followed by as-of", regardless of which API call you make first. `with`
  ;; plus `as-of` lets you see a speculative db with recent datoms filtered out,
  ;; but it does not let you branch the past.

  ;; the present pays no penalty

  ;; the original database value returned from connection does no filtering. It
  ;; sees only the present, without having to filter the past. So queries
  ;; about "now" are as efficient as possible–they do not consider history and
  ;; pay no penalty for history, no matter how much history is stored in the
  ;; system.

  ;; filter or log?

  ;; filters are applied to database indexes, filtering out data that does not
  ;; match a predicate. Filters cannot do anything that could not be done
  ;; directly with indexes, but they do allow you to separate some aspect of a
  ;; query, reusing query or entity logic across different filtered views.

  ;; consider the following two queries:

  ;; - "What happened between 8:00 and 9:00 this morning?"
  ;; - "What did entity 42 look like at 8:00 this morning?"

  ;; The first query is limited *only* by time. Such a query can be answered
  ;; directly from the log, which is a time index. Answering the same question
  ;; via `as-of` and `since` would require a filtering scan of the entire
  ;; database.

  ;; the second query is limited by both entity and by time. Thus it makes sense
  ;; to augment indexes with a filter, calling entity on an `as-of` view of the
  ;; database. The entity call will use the EAVT index to jump straight to the
  ;; requested datoms, which are then filtered to exclude the most recent
  ;; datoms. Because all the time-related work is done by `as-of`, the call to
  ;; entity can be time-agnostic and can work with any point-in-time. You can
  ;; use the same approach in your own queries, writing queries that are
  ;; agnostic about time (or about some domain value), and using filters to
  ;; allow reuse of those queries across different domain values.

)
