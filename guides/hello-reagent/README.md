# hello-reagent

Exploring Reagent --- a minimalistic ClojureScript interface to React.js.

- https://github.com/reagent-project/reagent

## Getting started

Start a cljs REPL in terminal

```
clj -M -m cjls.main
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
