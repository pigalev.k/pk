(ns hello-reagent.utils
  (:require
   [reagent.cookies :as c]
   [reagent.crypt :as crypt]
   [reagent.format :as f]
   [reagent.session :as s]
   [reagent.validation :as v]))


;; Reagent utils: a collection of helper functions for use with Reagent

;; https://github.com/reagent-project/reagent-utils


(enable-console-print!)
(println "Utils: ns loaded")

;; `reagent.cookies`

(comment

  ;; sets a cookie, the max-age for session cookie

  ;; following optional parameters may be passed in as a map:
  ;; - `:max-age`: defaults to -1
  ;; - `:path`: path of the cookie, defaults to the full request path
  ;; - `:domain`: domain of the cookie, when null the browser will use the full
  ;;   request host name
  ;; - `:secure?`: boolean specifying whether the cookie should only be sent
  ;;   over a secure channel
  ;; - `:raw?`: boolean specifying whether content should be stored raw, or as
  ;;   EDN

  (c/set! :yeah 'sure)
  (c/set! :foo {:x 1 :now (js/Date.)})
  (c/set! :bar {:y 2 :now (js/Date.)} {:raw? true :secure? true})


  ;; returns the number of cookies

  (c/count)


  ;; returns all the keys for the cookies

  (c/keys)


  ;; returns cookie values (as edn)

  (c/vals)


  ;; returns cookie values (as strings)

  (c/raw-vals)


  ;; true if no cookies are set

  (c/empty?)


  ;; is the key present in the cookies

  (c/contains-key? :foo)
  (c/contains-key? :ring-session)


  ;; is the value present in the cookies (as string)

  (c/contains-val? 'sure)

  ;; gets the value at the key (as edn), optional default when value is not found

  (c/get :bar)


  ;; gets the value at the key (as string), optional default when value is not
  ;; found

  (c/get-raw :bar)


  ;; removes a cookie, optionally for a specific path and/or domain

  (c/remove! :foo)

  ;; removes all cookies

  (c/clear!)

)


;; `reagent.crypt`

(comment

  (def s "hello")

  (def b (crypt/string->bytes s))

  (crypt/bytes->hex b)

  (crypt/hash s :sha1)
  (crypt/hash s :sha1 true)

  (crypt/hash-bytes s :md5)

  (crypt/digest (goog.crypt.Sha1.) b)
  (crypt/digest (goog.crypt.Md5.) b)

)


;; `reagent.format`

(comment

  (def s "hell'o")

  (f/add-slashes s)

  (f/capitalize-words s)

  (f/center s 19)


  ;; formats currency using the current locale
  ;; to change locale set `goog.i18n.NumberFormatSymbols`, e.g.:
  ;; (set! goog.i18n.NumberFormatSymbols goog.i18n.NumberFormatSymbols_it_IT)

  ;; don't know how to use
  (f/currency-format 12.10)

  (f/date-format (js/Date.) "yyyy.MM.dd")

  (f/encode-uri "http://example.com?name=Joe")

  (f/line-numbers "first line\nsecond line\nthird line")


  ;; formats a string using `goog.string.format`

  (f/format "Cost: %.2f" 10.0234)


  ;; pluralizes the word based on the number of items

  (f/pluralize ["John"] "lad")
  (f/pluralize ["John" "James"] "lad")
  (f/pluralize ["Alice"] "lad" "y" "ies")


  ;; prints formatted output, as per format

  (f/printf "Cost: %.2f" 10.0234)


  ;; removes specified tags

  (f/remove-tags "<p>foo bar</p>" "p")

)


;; `reagent.session`

(comment

  @s/state

  (s/put! :foo :bar)

  (s/get :foo)

  (s/get! :foo)

  (s/get-in [:a :b])

  (s/get-in! [:a :b])

  (s/reset! nil)

  (s/swap! assoc :x 1)

  (s/update! :x inc)

  (s/update-in! [:a :b] (fnil inc 0))

  (s/remove! :foo)

)


;; `reagent.validation`

(comment

  (v/equal-to? "1" 1)
  (v/equal-to? "1.0" 1.2)

  (v/greater-than? "1" 0)
  (v/less-than? "1" 3)

  (v/max-length? "hello" 3)
  (v/max-length? "hello" 7)
  (v/min-length? "hello" 3)
  (v/min-length? "hello" 7)

  (v/has-value? "")
  (v/has-value? "hello")

  (v/has-values? ["0" "1" "2"])

  (v/is-email? "a@b")
  (v/is-email? "a@b.c")

  (v/not-nil? nil)
  (v/not-nil? 1)

  (v/valid-number? 1)

)
