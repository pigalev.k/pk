(ns hello-reagent.updating-components
  (:require
   [hello-reagent.basics :as basics]
   [reagent.core :as r]))


;; When do components update?

;; https://cljdoc.org/d/reagent/reagent/1.2.0/doc/tutorials/when-do-components-update-

(enable-console-print!)
(println "Updating components: ns loaded")

(comment

  ;; components are reactive

  ;; Reagent components are reactive in the following way:

  ;; - each component has a render function
  ;; - this render function turns input data into Hiccup (HTML)
  ;; - render functions are rerun when their input data changes, producing new
  ;;   Hiccup
  ;; - that new Hiccup is interpreted by Reagent and ultimately results in new
  ;;   HTML

  ;; it is this whole re-running the render function thing that makes a
  ;; component reactive. It reacts to changes in its inputs, producing a new
  ;; output.


  ;; reactive to what?

  ;; we start by looking at the inputs to the process. What things, when they
  ;; change value, trigger a re-run of a component's renderer?

  ;; short answer is that there's two kinds of input data:

  ;; - props
  ;; - ratoms

  ;; as we'll soon see, these two kinds of input are not quite equal. There are
  ;; differences in the way they trigger.


  ;; props

  ;; consider this example component:

  (defn greet
    [name]
    [:div "Hello " name])

  ;; `name` is a prop (short for property). In this example, it is a string
  ;; value. In our ClojureScript/Reagent world, it takes the form of a parameter
  ;; to the component renderer, `greet`.

  ;; each time the value of name changes over time, `greet` will rerender.

  ;; wait, what? How exactly can the value of name change over time --- isn't it
  ;; just a parameter? Don't parameters only ever get one value, when the
  ;; function is called?

  ;; well, you'll remember from previous tutorials that `greet` is going to
  ;; be "promoted" to be the render function of a component. As a component
  ;; renderer, it will get called at least once, but probably many, many
  ;; times. So there will be the opportunity for name to have a different value
  ;; each time greet is called and, in that sense, it is a value which can
  ;; change over time.

  ;; to understand further, imagine we had a parent component, which uses
  ;; `greet`:

  (defn greet-family
    []
    [:div
     [greet "Dad"]
     [greet (str "Bro-" (rand-int 10))]])

  (basics/render [greet-family])

  ;; when Reagent interprets the hiccup returned by greet-family, it will create
  ;; 3 further components:

  ;; - a `:div` component, with two `greet` children
  ;; - the 1st `greet` child will always be given the name "Dad". Always the
  ;;   same prop
  ;; - the 2nd `greet` child will likely have a different value for name each
  ;;   time that `greet-family` renders. Perhaps "Bro-1" one time and "Bro-5"
  ;;   the next. Only 1 time in 10 will it be the same as last time.

  ;; after a component's renderer runs and produces Hiccup, Reagent interprets
  ;; it. When it processes the output of `greet-family`, it will check to see if
  ;; these 3 rerendered components themselves need rerendering. The test Reagent
  ;; uses is a simple one: for each component, are the newly supplied props
  ;; different to those supplied in the last render. Have they "changed"?

  ;; if the props are different, then that component's render will be called to
  ;; create new Hiccup. But if the props to that component are the same as last
  ;; time, then no need to rerender it.

  ;; obviously, the [greet "Dad"] component is rendered by `greet-family` the
  ;; same way each time, and will get the same props every time and, so, it will
  ;; not need re-rendering. It will render once, at the beginning, but never
  ;; again, no matter how many times its parent `greet-family` is rerendered.

  ;; on the other hand, [greet (str "Bro-" (rand-int 10))] will often render a
  ;; different name prop. So, if `greet-family` rerenders, then that child
  ;; component will often re-render too... although about 1 time in 10 the prop
  ;; this time will be the same as last time, and Reagent will determine that it
  ;; doesn't need to be rerendered.

  ;; which means we can now answer the question posed above --- how can the
  ;; value of name change over time for a given greet component? Answer: when
  ;; the parent component re-renders, and supplies a new value as the prop.

  ;; props flow from the parent. A component can't get new props unless its
  ;; parent rerenders.


  ;; ratoms

  (def a-name (r/atom "Bear"))

  (defn ask-for-forgiveness
    ;; no props
    []
    ;; notice that @
    [:div "Please " @a-name " with me"])

  (ask-for-forgiveness)

  ;; we can see that `ask-for-forgiveness` will return the
  ;; hiccup [:div "Please " "Bear" " with me"]. Well, initially anyway, because
  ;; initially `a-name` contains the string value "Bear".

  ;; data is flowing into the render function via this `a-name` ratom. Reagent
  ;; will detect that this renderer has a ratom input, and it will watch that
  ;; ratom for changes.

  ;; if I suddenly got all scientific, and did this: (reset! a-name "Ursidae"),
  ;; Reagent would detect the change in name, and it would re-run any Component
  ;; renderer which is dependent upon it. That means ask-for-forgiveness is
  ;; re-run, producing the new hiccup [:div "Please " "Ursidae" " with me"].

  (basics/render [ask-for-forgiveness])
  (reset! a-name "Ursidae")

  ;; just so we're clear: a "data input" changes (the value in a ratom) and,
  ;; then, the renderer is rerun to produce new Hiccup. The component is
  ;; reactive to the ratoms it derefs.


  ;; a combination

  ;; let's now look at how these things can combine. We're going to consider a
  ;; case involving two child components, and a parent.

  ;; child Component 1:

  (defn greet-number
    "I say hello to an integer"
    [num]
    [:div (str "Hello #" num)])

  ;; child component 2:

  (defn more-button
    "I'm a button labelled 'More' which increments counter when clicked"
    [counter]
    [:button  {:class    "button-class"
               :on-click #(swap! counter inc)}
     "More"])

  ;; and, finally, a form-2 parent Component which uses these two child
  ;; components:

  (defn parent
    []
    (let [counter (reagent.ratom/atom 1)]
      (fn  parent-renderer
        []
        [:div
         ;; notice the @. The prop is an int
         [greet-number @counter]
         ;; no @ on counter
         [more-button counter]])))

  (basics/render [parent])

  ;; - Reagent will notice that `counter` has changed and that is an input ratom
  ;;   to `parent-renderer`, and it will rerun that renderer.
  ;; - Reagent will interpret the Hiccup returned by `parent-renderer`, and it
  ;;   will determine that a new (integer) prop has been supplied in the
  ;;   [greet-number @counter] component, and it will then rerender that
  ;;   component too.

  ;; wait. Is that it? Why doesn't the [more-button counter] component rerender
  ;; too? After all, its prop counter has changed???

  ;; no, I promise it won't rerender. But why not? The answer is a bit subtle.

  ;; you see, counter itself hasn't changed. It is still the same ratom it was
  ;; before. The value in counter has been incremented, but counter itself is
  ;; still the same ratom. So from Reagent's point of view [more-button counter]
  ;; involves the same prop as "last time" and it concludes that there's no need
  ;; for a rerender of that component.

  ;; had `more-button` dereferenced the counter ratom THEN the change in counter
  ;; should have triggered a rerender of more-button. But if you look at
  ;; more-button you'll see no `@counter`. There is no dereference.

  ;; if you truly understand this example, then you've gone a long way to
  ;; officially getting it.


  ;; different

  ;; although they are both ways to trigger a reactive re-render, the two kinds
  ;; of inputs have different properties:

  ;; - the definition of "changed" applied
  ;; - treatment of lifecycle functions


  ;; changed?

  ;; till now, I've said a renderer will be re-run when an input
  ;; value "changed". But I've been carefully avoiding any definition
  ;; of "changed".

  ;; you see, there's at least two definitions: `=` and `identical?`

  ;; at time 1, x has this value
  (def x1 {:a 42 :b 45})
  ;; at time 2, x has this value
  (def x2 {:a 42 :b 45})

  ;; is x the same, or has it changed?
  (= x1 x2)

  ;; is x the same, or has it changed?
  (identical? x1 x2)

  ;; so we can see different answers to the question "has x changed?" for the
  ;; same values, depending on the function we use.

  ;; for props, `=` is used to determine if a new value have changed with regard
  ;; to an old value.

  ;; for ratoms, `identical?` is used (on the value inside the ratom) to
  ;; determine if a new value has changed with regard to an old value.

  ;; so, it is only when values are deemed to have "changed", that a re-run is
  ;; triggered, but the inputs use different definitions of "changed". This can
  ;; be confusing.

  ;; the `identical?` version is very fast. It is just a single reference check.

  ;; the `=` version is more accurate, more intuitive, but potentially more
  ;; expensive. Although, as I'm writing this I notice that `=` uses
  ;; `identical?` when it can.


  ;; update:

  ;; as of Reagent 0.6.0, ratoms use `=` (instead of `identical?`) is to
  ;; determine if a new value is different to an old value. So, ratoms and props
  ;; now have the same changed? semantics.


  ;; efficient re-renders

  ;; it's only via rerenders that a UI will change. So re-rendering is pretty
  ;; essential.

  ;; on the other hand, unnecessary re-rendering should be avoided. In the worst
  ;; case, it could lead to performance problems. By unnecessary rendering, I
  ;; mean rerenders which result in unchanged HTML. That's a whole lot of work
  ;; for no reason.

  ;; so this notion of "changed" is pretty important. It controls if we are
  ;; doing unnecessary, performance-sapping re-rendering work.


  ;; lifecycle functions

  ;; when props change, the entire underlying React machinery is
  ;; engaged. Reagent components can have lifecycle methods like
  ;; `component-did-update` and these functions will get called, just as they
  ;; would if you were dealing with a React component.

  ;; but... when the re-render occurs because an input ratom changed, lifecycle
  ;; functions are not run. So, for example, `component-did-update` will not be
  ;; called on the component.

  ;; careful of this one. It trips people up.


  ;; appendix #1

  ;; in the Using components tutorial, we looked at the difference between `()`
  ;; and `[]`.

  ;; towards the end, I claimed that using `[]` was more efficient at "re-render
  ;; time". Hopefully, after the tutorial above, our knowledge is a bit deeper
  ;; and we can now better appreciate the truth in this claim.

  (defn greet-family-square
    [member1 member2 member3]
    [:div
     [greet member1]
     [greet member2]
     [greet member3]])

  ;; now, imagine it used like this:

  ;; the 3rd member of the family to greet

  (def extra (r/atom "Aunt Edith"))

  (defn top-level-component
    []
    [greet-family-square  "Mum" "Dad" @extra])

  (basics/render [top-level-component])

  ;; the first time the page is rendered, the DOM created will greet three
  ;; cherished people. All good. At this point, the round and square versions of
  ;; greet-family would be equally good at getting the initial DOM into our
  ;; browser.

  ;; but then, out of nowhere, comes information that our rich and eccentric
  ;; Uncle John is rewriting his Last Will And Testament, and we need a fast,
  ;; realtime change in our page. Luckily we have a repl handy, and we
  ;; type (reset! extra "Uncle John"). We've changed that extra `r/atom` which
  ;; holds the 3rd cherished family member --- sorry "Aunt Edith", you're out.

  ;; what happens next?

  ;; - Reagent will recognize that a top-level-component component relies on
  ;;   extra which has changed.
  ;; - so it will rerender that component. In the Hiccup produced (by the
  ;;   rerender), it will see that `greet-family-square` has a new 3rd prop.
  ;;   And, by that, I mean that the value for the 3rd prop ("Uncle John") will
  ;;   not compare `=` to the value last rendered ("Aunt Edith").
  ;; - so Reagent will trigger a rerender of `greet-family-square` with the new
  ;;   props ("Mum" "Dad" and "Uncle John")
  ;; - in the Hiccup produced by this rerender, Reagent will notice that the
  ;;   first two greet components have the same prop as that last rendered
  ;;   ("Mum" and "Dad") but that the 3rd greet component has a new prop value
  ;;   ("Uncle John").
  ;; - so it will NOT rerender the first two greets, but the renderer for the
  ;;   3rd will be rerun.

  ;; as you can see, only the right parts of the tree are re-rendered. Nothing
  ;; unnecessary is done.

  ;; in the alternative `greet-family-round` version we looked at, the one which
  ;; used `()` instead of `[]`, that efficiency is not possible.

  ;; a re-render of `greet-family-round` always triggers three calls to `greet`,
  ;; no matter what, accumulating a large amount of hiccup for
  ;; `greet-family-round` to return. It would then be left to React to diff all
  ;; this new DOM with existing DOM and for it to work out that, in fact, parts
  ;; of the tree (the first two greet parts) remain the same, and should be
  ;; ignored. Which is a whole lot of unnecessary work!

  ;; when we use `[]`, we get independent React components which will only be
  ;; re-rendered if their props change (or ratoms change). More efficient, more
  ;; minimal re-renderings.

)
