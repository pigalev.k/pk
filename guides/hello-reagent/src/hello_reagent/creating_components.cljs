(ns hello-reagent.creating-components
  (:require
   [hello-reagent.basics :as basics]
   [reagent.core :as r]))


;; Creating Reagent components

;; https://cljdoc.org/d/reagent/reagent/1.2.0/doc/tutorials/creating-reagent-components

(enable-console-print!)
(println "Creating components: ns loaded")

(comment

  ;; in Reagent, the fundamental building block is a component.

  ;; your reagent app will typically have many components --- say, more than 5,
  ;; but less than 100 --- and the overall UI of a reagent app is the
  ;; stitched-together-output from all of them, each contributing part of the
  ;; overall HTML, typically in a hierarchical arrangement.


  ;; the core of a component

  ;; at the core of any component is a render function.

  ;; a render function is the backbone, mandatory part of a component. In fact,
  ;; as you'll soon see, components will often collapse down to be nothing more
  ;; than a render function.

  ;; a render function turns data into HTML. Data is supplied via the function
  ;; parameters, and HTML is the return value. Data in, HTML out.

  ;; much of the time, a render function will be a pure function. If you pass
  ;; the same data into a render function, then it will return the same HTML,
  ;; and it won't side effect.

  ;; note: ultimately, the surrounding Reagent/React framework will cause
  ;; non-pure side-effects because the returned HTML will be spliced into the
  ;; DOM (mutating global state!), but here, for the moment, all we care about
  ;; is the pureness of the render function itself.


  ;; the Three Ways

  ;; there are three ways to create a component. Ordered by increasing
  ;; complexity, they are:

  ;; - via a simple render function: data in as parameters, and it returns HTML.
  ;; - via a function which returns the render function: the returned function
  ;;   is the render function.
  ;; - via a map of functions, one of which is the render, and the rest are
  ;;   React lifecycle methods which allow for some more advanced interventions.

  ;; in all three cases, a render function is provided --- that's the
  ;; backbone. The three creation methods differ only in terms of what they
  ;; supply over and above a renderer.


  ;; form-1: a simple function

  ;; in the simplest case, a component collapses down to only be a render
  ;; function. You just write a regular clojurescript function which takes data
  ;; as parameters and produces HTML (in Hiccup format, see Describing HTML).

  (defn greet
    [name]
    [:div "Hello " name])

  (greet)
  (basics/render [greet])

  ;; hiccup uses vectors to represent HTML elements, and maps to represent an
  ;; element's attributes.

  ;; rookie mistake

  ;; at some point, you'll probably try to return sibling HTML elements in a
  ;; normal cljs vector:

  (defn wrong-component
    [name]
    [[:div "Hello"] [:div name]])

  ;; throws
  (basics/render [wrong-component "Joe"])

  ;; that isn't valid Hiccup and you'll get a slightly baffling error. You'll
  ;; have to correct this mistake by wrapping the two siblings in a parent
  ;; `:div`:

  (defn right-component
    [name]
    [:div
     [:div "Hello"]
     [:div name]])

  (basics/render [right-component "Joe"])

  ;; alternatively, you could return a React Fragment. In reagent, a React
  ;; Fragment is created using the `:<>` Hiccup form.

  (defn right-component
    [name]
    [:<>
     [:div "Hello"]
     [:div name]])

  (basics/render [right-component "Joe"])


  ;; form-2: a function returning a function

  ;; sometimes a component requires some setup, or some local state; and of
  ;; course a renderer. The first two are optional, the last is not.

  ;; form-2 components are written as an outer function which returns an inner
  ;; render.

  (defn timer-component []
    ;; setup, and local state
    (let [seconds-elapsed (r/atom 0)]
      ;; inner render function is returned
      (fn []
        (js/setTimeout #(swap! seconds-elapsed inc) 1000)
        [:div "Seconds Elapsed: " @seconds-elapsed])))

  (basics/render [timer-component])

  ;; as before, the job of the render function is to turn data into HTML. Its
  ;; just that Form-2 allows your renderer to close over some state created and
  ;; initialised by the outer.

  ;; - `timer-component` is called once per component instance (and will create
  ;;   the state for that instance)
  ;; - the render function it returns will potentially be called many, many
  ;;   times. In fact, it will be called each time Reagent detects a possible
  ;;   difference in that component's inputs.

  ;; rookie mistake

  ;; when starting out, everyone makes this mistake with the Form-2 construct:
  ;; they forget to repeat the parameters in the inner, anonymous render
  ;; function.

  (defn outer
    [a b c]            ;; <--- parameters
    (fn [a b c]        ;; <--- forgetting to repeat them, is a rookie mistake
      [:div
       (str a b c)]))

  (basics/render [outer 1 2 3])

  ;; remember, outer is called once per component instance. Each time, the
  ;; parameters to outer will hold the initial parameter values. The renderer on
  ;; the other hand, will be called by Reagent many times and, each time,
  ;; potentially with alternative parameter values, but unless you repeat the
  ;; parameters on the renderer it will close over those initial values in
  ;; outer. As a result, the component renderer will stubbornly only ever render
  ;; the original parameter values, not the updated ones, which can be baffling
  ;; for a beginner.


  ;; form-3: a class with lifecycle methods

  ;; you'll probably use Form-3 components only when you want to use a js
  ;; library like D3 or introduce some hand-crafted optimisations.

  ;; while the critical part of a component is its render function, sometimes we
  ;; need to perform actions at various critical moments in a component's
  ;; lifetime, like when it is first created, or when its about to be
  ;; destroyed (removed from the DOM), or when its about to be updated, etc.

  ;; with Form-3 components, you can nominate lifecycle methods. reagent
  ;; provides a very thin layer over React's own lifecycle methods.

  ;; because React's lifecycle methods are object-oriented, they presume the
  ;; ability to access `this` to obtain the current state of the
  ;; component. Accordingly, the signatures of the corresponding Reagent
  ;; lifecycle methods all take a reference to the reagent component as the
  ;; first argument. This reference can be used with `r/props`, `r/children`,
  ;; and `r/argv` to obtain the current props/arguments. There are some
  ;; unexpected details with these functions described below. You may also find
  ;; `r/dom-node` helpful, as a common use of form-3 components is to draw into
  ;; a canvas element, and you will need access to the underlying DOM element to
  ;; do so.

  (defn my-component
    [x y z]
    ;; state being closed over by lifecycle fns
    (let [some (str :local :but :shared :state)
          can  (str :go :here)]
      ;; expects a map of functions
      (r/create-class
       ;; for more helpful warnings & errors
       {:display-name "my-component"
        ;; the name of a lifecycle function
        :component-did-mount
        ;; your implementation
        (fn [this]
          (println "component-did-mount"))
        :component-did-update
        ;; reagent provides you the entire "argv", not just the "props"
        (fn [this old-argv]
          (let [new-argv (r/argv this)
                props    (r/props this)
                children (r/children this)]
            (println "old and new argv" old-argv new-argv)
            (println "props: " props)
            (println "children: " children)))

        ;; other lifecycle funcs can go in here

        ;; note: it is not `:render`
        :reagent-render
        ;; remember to repeat parameters
        (fn [x y z]
          [:<>
           [:div some can]
           [:div (str x " " y " " z)]])})))

  (basics/render [my-component {:x :y} 1 2 3])

  ;; or as a child in a larger Reagent component

  (defn homepage []
    [:div
     [:h1 "Welcome"]
     ;; be sure to put the Reagent class in square brackets to force it to
     ;; render!
     [my-component {:a :b} 4 5 6]])

  (basics/render [homepage])

  ;; note the `old-argv` above in the signature for
  ;; `:component-did-update`. Many of these Reagent lifecycle method analogs
  ;; take `prev-argv` or `old-argv` (see the docstring for `r/create-class` for
  ;; a full listing). These argv arguments include the component constructor as
  ;; the first argument, which should generally be ignored. This is the same
  ;; format returned by `(r/argv this)`.

  ;; alternately, you can use `(r/props this)` and `(r/children this)`, but,
  ;; conceptually, these don't map as clearly to the argv concept. Specifically,
  ;; the arguments to your render function are actually passed as children (not
  ;; props) to the underlying React component, unless the first argument is a
  ;; map. If the first argument is a map, then that map is passed as props, and
  ;; the rest of the arguments are passed as children. Using props and children
  ;; may read a bit cleaner, but you do need to pay attention to whether you're
  ;; passing a props map or not.

  ;; finally, note that some React lifecycle methods take `prevState` and
  ;; `nextState`. Because Reagent provides its own state management system,
  ;; there is no access to these parameters in the lifecycle methods.

  ;; it is possible to create Form-3 components using `with-meta`. However,
  ;; `with-meta` is a bit clumsy and has no advantages over the above method,
  ;; but be aware that an alternative way exists to achieve the same outcome.

  ;; rookie mistake

  ;; in the code sample above, notice that the renderer function is identified
  ;; via an odd keyword in the map given to `r/create-class`. It's called
  ;; `:reagent-render` rather than the shorter, more obvious `:render`.

  ;; its a trap to mistakenly use `:render` because you won't get any errors,
  ;; except the function you supply will only ever be called with one parameter,
  ;; and it won't be the one you expect.

  ;; rookie mistake

  ;; while you can override `:component-should-update` to achieve some
  ;; performance improvements, you probably shouldn't unless you really, really
  ;; know what you are doing. Resist the urge. Your current performance is just
  ;; fine.

  ;; rookie mistake

  ;; leaving out the `:display-name` entry. If you leave it out, Reagent and
  ;; React have no way of knowing the name of the component causing a
  ;; problem. As a result, the warnings and errors they generate won't be as
  ;; informative.


  ;; final note

  ;; Above I used the terms form-1, form-2 and form-3, but there's actually only
  ;; one kind of component. It is just that there's 3 different ways to create a
  ;; component.

  ;; at the end of the day, no matter how it is created, a component will end up
  ;; with a render function and some life-cycle methods. A component created via
  ;; form-1 has the same basic structure as one created via form-3 because
  ;; underneath they are all just React components.


  ;; appendix A: lifting the lid slightly

  ;; here's some further notes about Reagent's mechanics:

  ;; - when you provide a function as the first element of a hiccup
  ;;   vector [my-func 1 2 3], Reagent will say "hey I have been given a render
  ;;   function". That function might be form-1 or form-2, but it doesn't know
  ;;   at that point. It just sees a function.
  ;; - a render function by itself is not enough to make a React Component. So,
  ;;   Reagent takes this render function and "merges" it with default
  ;;   lifecycle functions to form a React component. (form-3, of course,
  ;;   allows you to supply your own lifecycle functions)
  ;; - some time later, when Reagent first wants to render this component, it
  ;;   will, unsurprisingly, call the render function which you supplied
  ;;   (my-func in the snippet above). It will pass in the "props" (parameters)
  ;;   supplied by the rendering parent (1 2 3 in the snippet above).
  ;; - if this first call to the render function returns hiccup (a vector of
  ;;   stuff):
  ;;   - Reagent will just interpret it. So this is what happens in the case of
  ;;     a form-1 function.
  ;;   - if, however, this render function returns another function --- i.e.,
  ;;     it is a form-2 outer function returning the inner function --- then
  ;;     Reagent knows to replace the component's render function with the newly
  ;;     returned inner function forever thereafter. So the outer will have
  ;;     been called once but, from that point forward, the inner function will
  ;;     be used for all further rendering. In fact, Reagent will instantly call
  ;;     the inner function after the outer returns it, because Reagent wants a
  ;;     first rendering (hiccup) for the component.
  ;; - so, in the case of form-2, the outer function is called once and once
  ;;   only (with initial props/parameters), and the inner is called at least
  ;;   once (with initial props/parameters), but probably many, many times
  ;;   thereafter. Both will be called with the same arrangement of
  ;;   props/parameters --- although the inner render function will see
  ;;   different values in those props/parameters, over time.


  ;; appendix B: `with-let` macro

  ;; the `r/with-let` macro looks just like `let`, but the bindings only execute
  ;; once, and it takes an optional `finally` clause, that runs when the
  ;; component is no longer rendered. This can be particularly useful because it
  ;; can prevent the need for a form-2 component in many instances (like
  ;; creating a local reagent atom in your component).

  ;; for example: here's a component that sets up an event listener for mouse
  ;; moves, and stops listening when the component is removed.

  (defn mouse-pos-comp []
    (r/with-let [pointer (r/atom nil)
                 handler #(swap! pointer assoc
                                 :x (.-pageX %)
                                 :y (.-pageY %))
                 _ (.addEventListener js/document "mousemove" handler)]
      [:div
       "Pointer moved to: "
       (str @pointer)]
      (finally
        (.removeEventListener js/document "mousemove" handler))))

  (basics/render [mouse-pos-comp])

  ;; the same thing could of course be achieved with React lifecycle methods,
  ;; but that would be a lot more verbose.

  ;; `with-let` can also be combined with track (and other Reactive
  ;; contexts). For example, the component above could be written as:

  (defn mouse-pos []
    (r/with-let [pointer (r/atom nil)
                 handler #(swap! pointer assoc
                                 :x (.-pageX %)
                                 :y (.-pageY %))
                 _ (.addEventListener js/document "mousemove" handler)]
      @pointer
      (finally
        (.removeEventListener js/document "mousemove" handler))))

  (defn tracked-pos []
    [:div
     "Pointer moved to: "
     (str @(r/track mouse-pos))])

  (basics/render [tracked-pos])

  ;; the `finally` clause will run when `mouse-pos` is no longer tracked
  ;; anywhere, i.e in this case when `tracked-pos` is unmounted.

)
