(ns hello-reagent.describing-html
  (:require
   [hello-reagent.basics :as basics]
   [reagent.dom :as rdom]))


;; Describing HTML

;; https://cljdoc.org/d/reagent/reagent/1.2.0/doc/tutorials/using-hiccup-to-describe-html

(enable-console-print!)
(println "Describing HTML: ns loaded")

(comment

  ;; using Hiccup to describe HTML

  ;; Reagent uses a datastructure known as Hiccup to describe HTML. Hiccup
  ;; describes HTML elements and user-defined components as a nested
  ;; ClojureScript vector:

  (defn component
    []
    [:div {:class "parent"}
     [:p {:id "child-one"} "I'm first child element."]
     [:p "I'm the second child element."]])

  (basics/render component)


  ;; Reagent provides a number of extensions and conveniences to Hiccup, but the
  ;; general rules of Hiccup are as follows:

  ;; 1. the first element is either a keyword or a symbol
  ;;    - if it is a keyword, the element is an HTML element where
  ;;      `(name keyword)` is the tag of the HTML element.
  ;;    - if it is a symbol, reagent will treat the vector as a component, as
  ;;      described in the next section.
  ;; 2. if the second element is a map, it represents the attributes to the
  ;;    element. The attribute map may be omitted.
  ;; 3. any additional elements must either be Hiccup vectors representing
  ;;    child nodes or string literals representing child text nodes.


  ;; special treatment of nil child nodes

  ;; Reagent and React ignore nil nodes, which allow conditional logic in Hiccup
  ;; forms:

  (defn my-div
    [child?]
    [:div
     "Parent Element"
     (when child? [:div "Child element"])])

  (my-div false)
  (my-div true)

  (basics/render [:<>
                  [my-div false]
                  [my-div true]])


  ;; special interpretation of :`style` attribute

  ;; the `:style` attribute can be written a string or as a map. The following
  ;; two are equivalent:

  (defn a-component-1
    []
    [:div {:style "color: red; font-weight: bold"} "Alert"])

  (a-component-1)

  (defn a-component-2
    []
    [:div {:style {:color "red" :font-weight "bold"}} "Alert"])

  (a-component-2)

  (basics/render [:div
                  ;; oops, React 17 complains about this: `style` attr should
                  ;; be a map
                  #_[a-component-1]
                  [a-component-2]])

  ;; the map form is the same as React's style attribute, except that when using
  ;; the map form of the style attribute, the keys should be the same as the CSS
  ;; attribute as shown in the example above (not in camel case as is required
  ;; by JavaScript).


  ;; special interpretation of `:class` attribute

  ;; in JavaScript, `class` is a reserved keyword, so React uses the `className`
  ;; to specify class attibutes. Reagent just uses `:class`.

  ;; the class attribute accepts a collection of classes and will remove any nil
  ;; value:

  (defn a-component []
    [:div {:class ["a-class" (when active? "active") "b-class"]}
     "A div with classes"])

  (a-component)

  (basics/render [a-component])


  ;; special notation for `id` and `class`

  ;; the id of an element can be indicated with a hash (`#`) after the name of
  ;; the element. This

  (defn a-component-1 []
    [:div#my-id "Div with id"])

  ;; is the same as this:

  (defn a-component-2 []
    [:div {:id "my-id"} "Div with id"])

  (a-component-1)
  (a-component-2)

  (basics/render [:div
                  [a-component-1]
                  [a-component-2]])

  ;; one or more classes can be indicated for an element with a `.` and the
  ;; class-name like this:

  (defn a-component-1 []
    [:div.my-class.my-other-class.etc "Div with classes"])

  ;; which is the same as:

  (defn a-component-2 []
    [:div {:class ["my-class" "my-other-class" "etc"]} "Div with classes"])

  (a-component-1)
  (a-component-2)

  (basics/render [:div
                  [a-component-1]
                  [a-component-2]])

  ;; special notations for id and classes can be used together. The id must be
  ;; listed first:

  (defn a-component-1 []
    [:div#my-id.my-class.my-other-class "Div with id and classes"])

  ;; which is the same as:

  (defn a-component-2 []
    [:div {:id "my-id" :class ["my-class" "my-other-class"]}
     "Div with id and classes"])

  (a-component-1)
  (a-component-2)

  (basics/render [:div
                  [a-component-1]
                  [a-component-2]])


  ;; special notation for nested elements

  ;; Reagent extends standard Hiccup in one way: it is possible to stack
  ;; elements together by using a `>` character. This

  (defn a-component-1 []
    [:div
     [:p
      [:b "Nested Element"]]])

  ;; is the same as:

  (defn a-component-2 []
    [:div>p>b "Nested Element"])

  (a-component-1)
  (a-component-2)

  (basics/render [:div
                  [a-component-1]
                  [a-component-2]])


  ;; rendering Hiccup

  ;; the primary entrypoint to the reagent library is `reagent.dom/render`.

  (defn render-simple []
    (rdom/render [:div [:p "Hello world!"]]
                 (.querySelector js/document "#app")))

  (render-simple)

  ;; this render function expects one of two things:

  ;; - a React Element, which will just be passed to React as is.
  ;; - a ClojureScript vector (i.e., a Hiccup form).

  ;; if it encounters a ClojureScript vector, it will interpret it as
  ;; Hiccup. Reagent expects one of two things in the first position of the
  ;; vector:

  ;; - a keyword like `:div` or `:span`, which it will create using
  ;;   `React.createElement`
  ;; - a symbol like `my-component`.

  ;; if it's a symbol, then reagent will evaluate a function by that
  ;; name. Reagent expects one of three things from this function:

  ;; - a Hiccup vector. Reagent creates a React component with the function
  ;;   as its render method and uses the Hiccup vector for the initial render.
  ;; - a ClojureScript function. Reagent will then create a React component
  ;;   with this inner function as the render method and will then call the
  ;;   inner function for the initial render.
  ;; - a React component. Reagent will render this using `React.createElement`.
  ;;   Note, this could be a result of calling `reagent.core/create-class` or
  ;;   it could be a React component you have imported from a JavaScript library.

)
