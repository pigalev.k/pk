(ns hello-reagent.react-interop
  (:require
   [hello-reagent.basics :as basics]
   [react-flip-move]
   [react-sortable-hoc]
   [react-virtualized-auto-sizer]
   [reagent.core :as r]))


;; Interop with React

;; https://cljdoc.org/d/reagent/reagent/1.2.0/doc/tutorials/interop-with-react

(enable-console-print!)
(println "Interop with React: ns loaded")

(comment

  ;; creating React elements directly

  ;; the `reagent.core/create-element` function simply calls React's
  ;; `createElement` function (and therefore, it expects either a string
  ;; representing an HTML element or a React Component).

  ;; as an example, here are four ways to create the same element:

  (defn integration []
    [:div
     [:div.foo "Hello " [:strong "world"]]
     (r/create-element "div"
                       #js{:className "foo"}
                       "Hello "
                       (r/create-element "strong"
                                         #js{}
                                         "world"))
     (r/create-element "div"
                       #js{:className "foo"}
                       "Hello "
                       (r/as-element [:strong "world"]))
     [:div.foo "Hello " (r/create-element "strong"
                                          #js{}
                                          "world")]])

  (basics/render [integration])

  ;; this works because `reagent.dom/render` itself expects (1) a React element
  ;; or (2) a Hiccup form. If passed an element, it just uses it. If passed a
  ;; Hiccup, it creates a (cached) React component and then creates an element
  ;; from that component.


  ;; creating React elements from Hiccup forms

  ;; the `reagent.core/as-element` function creates a React element from a
  ;; Hiccup form. In the previous section, we discussed how `reagent.dom/render`
  ;; expects either (1) a Hiccup form or (2) a React element. If it encounters a
  ;; Hiccup form, it calls `r/as-element` on it. When you have a React component
  ;; that wraps children, you can pass Hiccup forms to it wrapped in
  ;; `r/as-element`.


  ;; creating Reagent components from React components

  ;; the function `reagent.core/adapt-react-class` will turn a React component
  ;; into something that can be placed into the first position of a Hiccup form,
  ;; as if it were a Reagent function. Take, for example, the `react-flip-move`
  ;; library and assume that it has been properly imported as a React component
  ;; called `FlipMove`. By wrapping `FlipMove` with `adapt-react-class`, we can
  ;; use it in a Hiccup form:

  (def articles [:div
                 [:p "hello one"]
                 [:p "hello two"]
                 [:p "hello three"]])

  (defn top-articles [articles]
    [(r/adapt-react-class js/FlipMove)
     {:duration 750
      :easing   "ease-out"}
     articles])

  (basics/render [top-articles])
  (basics/render [top-articles articles])

  ;; there is also a convenience mechanism `:>` (colon greater-than) that
  ;; shortens this and avoid some parenthesis:

  (defn top-articles [articles]
    [:> js/FlipMove
     {:duration 750
      :easing   "ease-out"}
     articles])

  (basics/render [top-articles])
  (basics/render [top-articles articles])

  ;; this is the equivalent JavaScript:

  ;; const TopArticles = ({ articles }) => (
  ;;   <FlipMove duration={750} easing="ease-out">
  ;;     {articles}
  ;;   </FlipMove>
  ;; );


  ;; creating React components from Reagent components

  ;; the `reagent.core/reactify-component` will take a form-1, form-2, or form-3
  ;; reagent component. For example:

  (defn exported [props]
    [:div "Hi, " (:name props)])

  (def react-comp (r/reactify-component exported))

  (defn could-be-jsx []
    (r/create-element react-comp #js{:name "world"}))

  (basics/render [could-be-jsx])

  ;; note: `r/adapt-react-class` and `r/reactify-component` are not perfectly
  ;; symmetrical, because `r/reactify-component` requires that the reagent
  ;; component accept everything in a single props map, including its children.


  ;; example: decorator higher-order components

  ;; some React libraries use the decorator pattern: a React component which
  ;; takes a component as an argument and returns a new component as its result.
  ;; We will need to use both `r/adapt-react-class` and `r/reactify-component`
  ;; to move back and forth between React and Reagent:

  (defn hello [props]
    [:div "Hi, " (:name props) " " (:index props)])

  (def react-sorted-component
    (let [decorator (.-SortableContainer js/SortableHOC)]
      (r/adapt-react-class
       (decorator (r/reactify-component hello)))))

  (basics/render [react-sorted-component {:name "Joe" :index 1}])

  ;; this is the equivalent JavaScript (not tested):

  ;; import {SortableElement} from 'react-sortable-hoc'
  ;; class Hello {
  ;;   /* ... */
  ;; }
  ;; export default SortableComponent(Hello);


  ;; example: function-as-child components

  ;; some React components expect a function as their only child. React
  ;; AutoSizer is one such example.

  (defn function-as-child-component []
    [:div {:style {:box-sizing :border-box
                   :height     100
                   :width      100
                   :resize     :both
                   :overflow   :auto
                   :border     "1px solid"}}
     [:> js/AutoSizer
      {}
      (fn [dims]
        (let [{:keys [height width] :as dims}
              (js->clj dims :keywordize-keys true)]
          (println dims)
          (r/as-element [:div (str width "x" height)])))]])

  (basics/render [function-as-child-component])


  ;; getting props and children of current component

  ;; because you just pass arguments to reagent functions, you typically don't
  ;; need to think about "props" and "children" as distinct things. But Reagent
  ;; does make a distinction and it is helpful to understand this, particularly
  ;; when interoperating with native elements and React libraries.

  ;; specifically, if the first argument to your Reagent function is a map, that
  ;; is assigned to `this.props` of the underlying Reagent component. All other
  ;; arguments are assigned as children to `this.props.children`.

  ;; when interacting with native React components, it may be helpful to access
  ;; props and children, which you can do with
  ;; `reagent.core/current-component`. This function returns an object that
  ;; allows you retrieve the props and children passed to the current component.

  ;; beware that `r/current-component` is only valid in component functions, and
  ;; must be called outside of e.g. event handlers and `for` expressions, so
  ;; it's safest to always put the call at the top, as in `my-div` here:

  (defn my-div []
    (let [this (r/current-component)]
      (into [:div.custom (r/props this)]
            (r/children this))))

  (defn call-my-div []
    [:div
     [my-div "Some text."]
     [my-div {:style {:font-weight 'bold}}
      [:p "Some other text in bold."]]])

  (basics/render [call-my-div])

)
