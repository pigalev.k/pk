(ns hello-reagent.using-components
  (:require
   [hello-reagent.basics :as basics]
   [reagent.core :as r]))


;; Using Reagent components

;; https://cljdoc.org/d/reagent/reagent/1.2.0/doc/tutorials/using-square-brackets-instead-of-parentheses-

(enable-console-print!)
(println "Using components: ns loaded")

(comment

  ;; this is a quick tutorial regarding the use of () and [] in Reagent
  ;; renderers.


  ;; components

  ;; in Creating Reagent Components, we saw that the centerpiece of a
  ;; Reagent/React component is a render function. Optionally, a component might
  ;; have other lifecycle functions, but a render function is central and
  ;; mandatory.

  ;; we also saw that Reagent render functions turn data into hiccup: data ->
  ;; hiccup

  ;; just a function:

  (defn greet
    [name]
    [:div  "Hello " name])

  ;; only when a function is used in a particular way, it is "promoted" to
  ;; become the render function for a Reagent component. There are two ways to
  ;; use render functions.


  ;; using via `()`

  ;; if you just call the function via (), it returns a vector:

  (greet "You")
  (first (greet "You"))
  (second (greet "You"))

  ;; so, simply calling such function certainly doesn't magically create a
  ;; Reagent component. It just returns a vector.

  ;; what if we call greet within another function?

  (defn greet-family-round
    [member1 member2 member3]
    [:div
     ;; return value put into the vector
     (greet member1)
     ;; and again
     (greet member2)
     ;; and again
     (greet member3)])

  ;; `greet-family-round` returns a 4-element vector. And what's in that vector
  ;; if we call it like this?

  (greet-family-round "Mum" "Dad" "Aunt Edith")


  ;; using via `[]`

  ;; let's keep `greet` the same, but change the way it is used:

  (defn greet-family-square
    [member1 member2 member3]
    [:div
     ;; not using ()
     [greet member1]
     [greet member2]
     [greet member3]])

  ;; let's be crystal clear: [greet member1] is a two element vector, just
  ;; like [1 2].

  ;; and, what would this call return?

  (greet-family-square "Mum" "Dad" "Aunt Edith")

  ;; so, `greet` is not called inside of `greet-family-square`. Instead, it is
  ;; placed into a vector.


  ;; the difference between `()` and `[]`

  ;; here is the hiccup returned by `greet-family-round`:

  [:div
   [:div  "Hello " "Mum"]          ;; the return value of greet put in here
   [:div  "Hello " "Dad"]          ;; and again
   [:div  "Hello " "Aunt Edith"]]  ;; and again

  ;; you'll notice this hiccup contains no references to `greet`. Only
  ;; the (hiccup) values returned by calls to `greet`.

  ;; on the other hand, there are references to `greet` in the hiccup returned by
  ;; `greet-family-square`:

  ;; [:div
  ;;  [#object[hello_reagent$using_components$greet] "Mum"]
  ;;  [#object[hello_reagent$using_components$greet] "Dad"]
  ;;  [#object[hello_reagent$using_components$greet] "Aunt Edith"]]


  ;; the interpretation of Hiccup

  ;; after renderers return Hiccup, Reagent interprets it.

  ;; as it does this interpretation, if Reagent sees a vector where the first
  ;; element is a function, for example [greet "Mum"], it interprets that
  ;; function as a renderer and it builds a React component around that
  ;; renderer.

  ;; let's pause and remember that a renderer function is the key, mandatory,
  ;; central part of a component. Defaults can be supplied for the other React
  ;; lifecycle functions, like `component-should-update`, but a renderer must be
  ;; supplied.

  ;; so Reagent recognises `greet` as a candidate renderer function and, if it
  ;; is found in the right place (1st element of a vector), Reagent will mix it
  ;; with other default lifecycle functions to form a full React/Reagent
  ;; component. It gives `greet` a, er, promotion.

  ;; the other elements of the vector, after `greet`, are interpreted as
  ;; parameters to the render function --- in React terms, props.


  ;; which and why?

  ;; so, which variation of greet-family (square vs round) should I choose, and
  ;; why?

  ;; The answer to "which?" is easy: you almost certainly want the square
  ;; version. "why?" takes more explanation...

  ;; first off, let's acknowledge that both variations will ultimately produce
  ;; the same DOM, so in that respect they are the same.

  ;; despite this identical outcome, they differ in one significant way:

  ;; - the square version will create each `greet` child as a distinct React
  ;;   component, each with its own React lifecycle, allowing them to re-render
  ;;   independently of siblings.
  ;; - the round version causes the `greet` Hiccup for all children to be
  ;;   incorporated into the Hiccup returned by the parent, forming one large
  ;;   data structure, parent and children all in together. So, each time the
  ;;   parent re-renders, all the greet children are effectively re-rendered
  ;;   too. React must then work out what, in this tree, has changed.

  ;; as a result, the square version will be more efficient at "re-render
  ;; time". Only the DOM which needs to be re-rendered will be done. At our toy
  ;; scale in this tutorial it hardly matters but, if `greet` was a more
  ;; substantial child component, this gain in efficiency could be significant.


  ;; a further, significant "why"

  ;; in the examples above, we've explored form-1 components --- the simplest
  ;; kind --- and we've seen we have some choice regarding use of `()` or
  ;; `[]`. Eventually, I claim that `[]` is much preferred, but you can get away
  ;; with (), up to a point.

  ;; but... the moment you start using form-2 or form-3 components, you
  ;; absolutely must be using `[]`. No choice. Using `()` just won't work at
  ;; all. Given the explanations above, I'm hoping you can work out why. Either
  ;; that or just shrug and use `[]` forever more.


  ;; appendix #1

  ;; Hiccup can be created like any normal cljs data structure. You don't have
  ;; to use literals.

  ;; our version of greet-family-round from above returns something of a 4
  ;; element vector literal:

  (defn greet-family-round
    [member1 member2 member3]
    [:div
     (greet member1)
     (greet member2)
     (greet member3)])

  (basics/render [greet-family-round 1 2 3])

  ;; here's a rewrite in which the hiccup is less literal and more generated:

  (defn greet-family-round-2
    [& members]
    (into [:div] (map greet members)))

  (basics/render [greet-family-round-2 4 5 6])

  ;; when called with 3 parameters, both versions of this function return the
  ;; same hiccup:

  (= (greet-family-round   "Mum" "Dad" "Aunt Edith")
     (greet-family-round-2 "Mum" "Dad" "Aunt Edith"))


  ;; appendix #2

  ;; when interpreting Hiccup, Reagent regards vectors as special, and it has
  ;; some demands about their 1st element.

  ;; in Reagent Hiccup, the 1st element of a vector must always be something it
  ;; can use to build a component.

  ;; Reagent can use `greet` to build a component, so that works. So does `:div`
  ;; because Reagent knows what component you mean. And there are a few other
  ;; options.

  ;; so this is okay: [greet ...] and so is this [:div ...]

  ;; but if your hiccup contains a vector like [1 2 3], then you'll get an error
  ;; because Reagent can't use 1 to build a component.

  ;; this code has a problem:

  (defn greet-v
    [v]
    (into [:div] (map greet v)))

  (defn greet-family
    []
    [greet-v ["Mum" "Dad" "Aunt Edith"]])

  ;; seems to actually work in Reagent 1.2.0
  (basics/render [greet-family])

  ;; notice the vector ["Mum" "Dad" "Aunt Edith"] in the hiccup. Reagent will
  ;; try to build a component using "Mum" (1st element in a vector) and, when
  ;; that doesn't work, it will report an error.

)
