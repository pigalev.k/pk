
(ns ^:figwheel-hooks hello-reagent.core
  (:require
   [goog.dom :as gdom]
   [reagent.core :as r]
   [reagent.dom :as rdom]))


;; a basic live example of ClojureScript app using Reagent and built with
;; figwheel-main

(enable-console-print!)
(println "This text is printed from `hello_reagent.core`. Go ahead and edit it
and see reloading in action.")


;; define some functions

(defn multiply [a b] (* a b))

;; define your app state so that it doesn't get over-written on reload

(defonce app-state (r/atom {:text "Hello world!"}))

;; find the root DOM node and mount the app into it

(defn get-app-element []
  (gdom/getElement "app"))

(defn hello-world []
  [:div
   [:h1 (:text @app-state)]
   [:h3 "Edit this in `hello_reagent.core` and watch it change!"]])

(defn mount [el]
  (rdom/render [hello-world] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app

(mount-app-element)

;; specify reload hook with ^;after-load metadata

(defn ^:after-load on-reload []
  (mount-app-element)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
