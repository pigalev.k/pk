(ns hello-reagent.react-features
  (:require
   [hello-reagent.basics :as basics]
   ["react" :as react]
   ["react-dom" :as react-dom]
   [reagent.core :as r]))


;; React features

;; https://cljdoc.org/d/reagent/reagent/1.2.0/doc/tutorials/react-features

(enable-console-print!)
(println "React features: ns loaded")

(comment

  ;; most React features should be usable from Reagent, even if Reagent doesn't
  ;; provide functions to use them directly.


  ;; fragments

  ;; JSX:

  ;; function example() {
  ;;   return (
  ;;     <React.Fragment>
  ;;       <ChildA />
  ;;       <ChildB />
  ;;       <ChildC />
  ;;     </React.Fragment>
  ;;   );
  ;; }

  ;; Reagent:

  (do
    (defn child-a []
      [:div "a"])

    (defn child-b []
      [:div "b"])

    (defn child-c []
      [:div "c"]))

  (defn example []
    [:<>
     [child-a]
     [child-b]
     [child-c]])

  (basics/render [example])

  ;; Reagent syntax follows React Fragment short syntax.


  ;; context

  (def my-context (react/createContext "default"))

  (def Provider (.-Provider my-context))
  (def Consumer (.-Consumer my-context))

  (defn context-component
    []
    [:> Provider {:value "bar"}
     [:> Consumer {}
      (fn [v]
        (r/as-element [:div "Context: " v]))]])

  (basics/render [context-component])

  ;; Context example project better explains how `:>` or adapt-react-class
  ;; convert the properties to JS objects, and shows how to use cljs values with
  ;; context.

  ;; alternatively you can use the static `contextType` property

  (def my-context (react/createContext "default"))

  (def Provider (.-Provider my-context))

  (defn show-context []
    (r/create-class
     {:context-type my-context
      :reagent-render (fn []
                        [:p (.-context (r/current-component))])}))

  (basics/render [show-context])
  (basics/render [:> Provider {:value "bar"}
                  [show-context]])

  ;; alternatively with metadata on a form-1 component:

  (def show-context
    ^{:context-type my-context}
    (fn []
      [:p (.-context (reagent.core/current-component))]))

  (basics/render [show-context])
  (basics/render [:> Provider {:value "quux"}
                  [show-context]])

  ;; context value can also be retrieved using `useContext` hook:

  (defn show-context []
    (let [v (react/useContext my-context)]
      [:p v]))

  ;; throws: hooks can only be called inside of the body of a function component
  (basics/render [show-context])
  ;; ok
  (basics/render [:> Provider {:value "frob"}
                  [:f> show-context]])

  ;; tests contain example of using old React lifecycle Context
  ;; API (context-wrapper function).


  ;; error boundaries

  ;; TODO: elaborate, not quite working

  ;; you can use `getDerivedStateFromError` (since React 16.6.0 and Reagent
  ;; 0.9) (and `ComponentDidCatch`) lifecycle method with `r/create-class`:

  (defn oops []
    [:div "something went wrong: "
     (str (ex-message @error) (ex-data @error))
     [:button {:on-click #(reset! error nil)} "Try again"]])

  (defn not-ok []
    (let [state (react/useNope 0)]
      [:div "ok"]))

  (defn error-boundary [component]
    (let [error (r/atom nil)]
      (r/create-class
       {:component-did-catch (fn [this e info])
        :get-derived-state-from-error (fn [e]
                                        (reset! error e)
                                        #js {})
        :reagent-render
        (fn [component]
          (if @error oops component))})))

  (basics/render [error-boundary [not-ok]])

  ;; alternatively, one could use React state instead of ratom to keep track of
  ;; error state, which can be more obvious with the new
  ;; `getDerivedStateFromError` method:

  (defn error-boundary [comp]
    (r/create-class
     {:constructor (fn [this props]
                     (set! (.-state this) #js {:error nil}))
      :component-did-catch (fn [this e info])
      :get-derived-state-from-error (fn [error] #js {:error error})
      :render (fn [this]
                (r/as-element
                 (if-let [error (.. this -state -error)]
                   [:div
                    "Something went wrong."
                    [:button {:on-click #(.setState
                                          this #js {:error nil})} "Try again"]]
                   comp)))}))

  (basics/render [error-boundary [not-ok]])

  ;; as per React docs, `getDerivedStateFromError` is what should update the
  ;; state after error, it can be also used to update ratom as in Reagent the
  ;; Ratom is available in function closure even for static
  ;; methods. `ComponentDidCatch` can be used for side-effects, like logging the
  ;; error.


  ;; function components

  ;; JavaScript functions are valid React components, but Reagent implementation
  ;; by default turns the ClojureScript functions referred in Hiccup-vectors to
  ;; Class components.

  ;; however, some React features, like hooks, only work with functional
  ;; components. There are several ways to use functions as components with
  ;; Reagent:

  ;; calling `r/create-element` directly with a ClojureScript function doesn't
  ;; wrap the component in any Reagent wrappers, and will create functional
  ;; components. In this case you need to use `r/as-element` inside the function
  ;; to convert Hiccup-style markup to elements, or just return React elements
  ;; yourself. You also can't use Ratoms here, as ratom implementation requires
  ;; that the component is wrapped by Reagent.

  ;; `:r>` shortcut can be used to create components similar to
  ;; `r/create-element`, and the children Hiccup forms are converted to React
  ;; element automatically.

  ;; using `r/adapt-react-class` or `:>` is also calls `r/create-element`, but
  ;; that also does automatic conversion of ClojureScript parameters to JS
  ;; objects, which isn't usually desired if the component is ClojureScript
  ;; function.

  ;; new way is to configure Reagent Hiccup-compiler to create functional
  ;; components (see Reagent compiler).

  ;; `:f>` shortcut can be used to create function components from Reagent
  ;; components (functions), where both ratoms and hooks work.


  ;; hooks

  ;; this is used with `:f>` so both hooks and ratoms work here:

  (defn example []
    (let [[count set-count] (react/useState 0)]
      [:div
       [:p "You clicked " count " times"]
       [:button
        {:on-click #(set-count inc)}
        "Click"]]))

  (defn root []
    [:div
     [:f> example]])

  (basics/render [root])

  ;; pre-1.0 workaround

  ;; NOTE: This section still refers to workaround using hooks inside class
  ;; components, read the previous section to create functional components.

  ;; hooks can't be used inside class components, and Reagent implementation
  ;; creates a class component from every function (i.e. Reagent component).

  ;; however, you can use React components using hooks inside Reagent, or use hx
  ;; components inside Reagent. Also, it is possible to create React components
  ;; from Reagent quite easily, because React function component is just a
  ;; function that happens to return React elements, and `r/as-element` does
  ;; just that:

  ;; this is React function component. Can't use ratoms here!

  (defn example []
    (let [[count set-count] (react/useState 0)]
      (r/as-element
       [:div
        [:p "You clicked " count " times"]
        [:button
         {:on-click #(set-count inc)}
         "Click"]])))

  ;; Reagent component

  (defn reagent-component []
    [:div
     ;; Note `:>` to use a function as a Reagent component
     [:> example]])

  (basics/render [reagent-component])

  ;; if you need to pass ratom state into these components, dereference them in
  ;; the Reagent components and pass the value (and if needed, function to
  ;; update them) as properties into the React function component.


  ;; portals

  (defn portal-component []
    [:div
     "Portal:"
     [:div.portal "Empty"]])

  (defn reagent-component []
    (let [el (.. js/document (querySelector ".portal"))]
      (.createPortal js/ReactDOM (r/as-element [:div "foo"]) el)))

  ;; TODO: elaborate, seems not quite right

  ;; create portal
  (basics/render [portal-component [reagent-component]])
  ;; render a component into it
  (basics/render [reagent-component] (.. js/document (querySelector ".portal")))


  ;; hydrate

  ;; TODO: elaborate

  (react-dom/hydrate (r/as-element [main-component]) container)


  ;; component classes

  ;; TODO: elaborate

  ;; for interop with React libraries, you might need to pass Component classes
  ;; to other components as parameter. If you have a Reagent component (a
  ;; function) you can use `r/reactify-component` which creates a Class from the
  ;; function.

  ;; if the parent Component awaits classes with some custom methods or
  ;; properties, you need to be careful and probably should use
  ;; `r/create-class`. In this case you don't want to use `r/reactify-component`
  ;; with a function (even if the function returns a class) because
  ;; `r/reactify-component` wraps the function in another Component class, and
  ;; parent Component doesn't see the correct class.

  ;; correct way

  (def editor
    (r/create-class
     {:get-input-node (fn [this] ...)
      :reagent-render (fn [] [:input ...])}))

  [:> SomeComponent
   {:editor-component editor}]

  ;; often incorrect way

  (defn editor [parameter]
    (r/create-class
     {:get-input-node (fn [this] ...)
      :reagent-render (fn [] [:input ...])}))

  [:> SomeComponent
   {:editor-component (r/reactify-component editor)}]

  ;; in the latter case, :editor-component is a Reagent wrapper class component,
  ;; which doesn't have the `getInputNode` method and is rendered using the
  ;; Component created by create-class and which has the method.

  ;; if you need to add static methods or properties, you need to modify
  ;; `r/create-class` return value yourself. The function handles the built-in
  ;; static-methods (`:childContextTypes`, `:contextTypes`, `:contextType`,
  ;; `:getDerivedStateFromProps`, `:getDerivedStateFromError`), but not others.

  (let [klass (r/create-class ...)]
    (set! (.-static-property klass) "foobar")
    (set! (.-static-method klass) (fn [param] ...))
    klass)

)
