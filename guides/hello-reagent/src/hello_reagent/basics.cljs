(ns hello-reagent.basics
  (:require
   [clojure.string :as str]
   [goog.dom :as gdom]
   ["react" :as react]
   [reagent.core :as r]
   [reagent.dom :as rdom]))


;; Introduction to Reagent

;; http://reagent-project.github.io/


;; setup

(enable-console-print!)
(println "Reagent basics: cljs REPL started")


;; essential API

;; Reagent supports most of React's API, but there is really only one entry
;; point that is necessary for most applications: `reagent.dom/render`.

(defn render
  "Mount reagent component `c` into DOM and render it."
  [c & [target]]
  (rdom/render c (or target (gdom/getElement "app"))))


;; basics

(comment

  ;; Reagent provides a minimalistic interface between ClojureScript and
  ;; React. It allows you to define efficient React components using nothing but
  ;; plain ClojureScript functions and data, that describe your UI using a
  ;; Hiccup-like syntax.


  ;; defining and using components


  ;; a HTML element

  (render [:div "a div"])

  ;; a very basic Reagent component:

  (defn simple-component []
    [:div
     [:p "I am a component!"]
     [:p.someclass
      "I have " [:strong "bold"]
      [:span {:style {:color "red"}} " and red "] "text."]])

  (render [simple-component])

  ;; you can build new components using other components as building blocks:

  (defn simple-parent []
    [:div
     [:p "I include simple-component."]
     [simple-component]])

  (render simple-parent)
  (render [:div "A nested component"
           [simple-component]])

  ;; data is passed to child components using plain old Clojure data types:

  (defn hello-component [name]
    [:p "Hello, " name "!"])

  (defn say-hello []
    [hello-component "world"])

  (render say-hello)

  ;; note: in the example above, `hello-component` might just as well have been
  ;; called as a normal Clojure function instead of as a Reagent component, i.e
  ;; with parenthesis instead of square brackets. The only difference would have
  ;; been performance, since "real" Reagent components are only re-rendered when
  ;; their data have changed. More advanced components though (see below) must
  ;; be called with square brackets.

  ;; a seq of components:

  (defn lister [items]
    [:ul
     (for [item items]
       ^{:key item} [:li "Item " item])])

  (defn lister-user []
    [:div
     "Here is a list:"
     [lister (range 3)]])

  (render lister-user)

  ;; note: the ^{:key item} part above isn't really necessary in this simple
  ;; example, but attaching a unique key to every item in a dynamically
  ;; generated list of components is good practice, and helps React to improve
  ;; performance for large lists. The key can be given either (as in this
  ;; example) as meta-data, or as a `:key` item in the first argument to a
  ;; component (if it is a map).


  ;; managing state

  ;; the easiest way to manage state in Reagent is to use Reagent's own version
  ;; of atom. It works exactly like the one in `clojure.core`, except that it
  ;; keeps track of every time it is `deref`ed. Any component that uses an atom
  ;; is automagically re-rendered when its value changes.

  (def click-count (r/atom 0))

  (defn counting-component []
    [:div
     "The atom " [:code "click-count"] " has value: "
     @click-count ". "
     [:input {:type     "button" :value "Click me!"
              :on-click #(swap! click-count inc)}]])

  (counting-component)
  [counting-component]
  ;; no visible updates on click: hiccup is rendered once with initial value
  (render counting-component)
  ;; visible updated on click: hiccup is rerendered on atom value change
  (render [counting-component])

  ;; sometimes you may want to maintain state locally in a component. That is
  ;; easy to do with an `r/atom` as well.

  (defn timer-component []
    (let [seconds-elapsed (r/atom 0)]
      (fn []
        (js/setTimeout #(swap! seconds-elapsed inc) 1000)
        [:div
         "Seconds Elapsed: " @seconds-elapsed])))

  (render [timer-component])

  ;; by simply passing an atom around you can share state management between
  ;; components:

  (defn atom-input [value]
    [:input {:type      "text"
             :value     @value
             :on-change #(reset! value (-> % .-target .-value))}])

  (defn shared-state []
    (let [val (r/atom "foo")]
      (fn []
        [:div
         [:p "The value is now: " @val]
         [:p "Change it here: " [atom-input val]]])))

  (render [shared-state])

  ;; note: component functions can be called with any arguments – as long as
  ;; they are immutable. You could use mutable objects as well, but then you
  ;; have to make sure that the component is updated when your data
  ;; changes. Reagent assumes by default that two objects are equal if they are
  ;; the same object.

)


;; examples

;; BMI calculator

(comment

  (defn calc-bmi [{:keys [height weight bmi] :as data}]
    (let [h (/ height 100)]
      (if (nil? bmi)
        (assoc data :bmi (/ weight (* h h)))
        (assoc data :weight (* bmi h h)))))

  (def bmi-data (r/atom (calc-bmi {:height 180 :weight 80})))

  (defn slider [param value min max invalidates]
    [:input {:type "range" :value value :min min :max max
             :style {:width "100%"}
             :on-change (fn [e]
                          (let [new-value (js/parseInt (.. e -target -value))]
                            (swap! bmi-data
                                   (fn [data]
                                     (-> data
                                         (assoc param new-value)
                                         (dissoc invalidates)
                                         calc-bmi)))))}])

  (defn bmi-component []
    (let [{:keys [weight height bmi]} @bmi-data
          [color diagnose] (cond
                             (< bmi 18.5) ["orange" "underweight"]
                             (< bmi 25) ["inherit" "normal"]
                             (< bmi 30) ["orange" "overweight"]
                             :else ["red" "obese"])]
      [:div
       [:h3 "BMI calculator"]
       [:div
        "Height: " (int height) "cm"
        [slider :height height 100 220 :bmi]]
       [:div
        "Weight: " (int weight) "kg"
        [slider :weight weight 30 150 :bmi]]
       [:div
        "BMI: " (int bmi) " "
        [:span {:style {:color color}} diagnose]
        [slider :bmi bmi 10 50 :weight]]]))

  (render [bmi-component])

)


;; clock

(comment

  (defonce timer (r/atom (js/Date.)))

  (defonce time-color (r/atom "#f34"))

  (defonce time-updater (js/setInterval
                         #(reset! timer (js/Date.)) 1000))

  (defn greeting [message]
    [:h1 message])

  (defn clock []
    (let [time-str (-> @timer .toTimeString (str/split " ") first)]
      [:div.example-clock
       {:style {:color @time-color}}
       time-str]))

  (defn color-input []
    [:div.color-input
     "Time color: "
     [:input {:type "text"
              :value @time-color
              :on-change #(reset! time-color (-> % .-target .-value))}]])

  (defn simple-example []
    [:div
     [greeting "Hello world, it is now"]
     [clock]
     [color-input]])

  ;; root never rerenders, only subcomponents
  (render simple-example)
  ;; root could rerender, if it had props that can change
  (render [simple-example])

)


;; todo list

(comment

  (defonce todos (r/atom (sorted-map)))

  (defonce counter (r/atom 0))

  (defn add-todo [text]
    (let [id (swap! counter inc)]
      (swap! todos assoc id {:id id :title text :done false})))

  (defn toggle [id] (swap! todos update-in [id :done] not))
  (defn save [id title] (swap! todos assoc-in [id :title] title))
  (defn delete [id] (swap! todos dissoc id))

  (defn mmap [m f a] (->> m (f a) (into (empty m))))
  (defn complete-all [v] (swap! todos mmap map #(assoc-in % [1 :done] v)))
  (defn clear-done [] (swap! todos mmap remove #(get-in % [1 :done])))

  (defonce init (do
                  (add-todo "Rename Cloact to Reagent")
                  (add-todo "Add undo demo")
                  (add-todo "Make all rendering async")
                  (add-todo "Allow any arguments to component functions")
                  (complete-all true)))

  (defn todo-input [{:keys [title on-save on-stop input-ref]}]
    (let [val (r/atom title)]
      (fn [{:keys [id class placeholder]}]
        (let [stop (fn [_e]
                     (reset! val "")
                     (when on-stop (on-stop)))
              save (fn [e]
                     (let [v (-> @val str str/trim)]
                       (when-not (empty? v)
                         (on-save v))
                       (stop e)))]
          [:input {:type "text"
                   :value @val
                   :ref input-ref
                   :id id
                   :class class
                   :placeholder placeholder
                   :on-blur save
                   :on-change (fn [e]
                                (reset! val (-> e .-target .-value)))
                   :on-key-down (fn [e]
                                  (case (.-which e)
                                    13 (save e)
                                    27 (stop e)
                                    nil))}]))))

  (defn todo-edit [props]
    (let [ref (react/useRef)]
      (react/useEffect (fn []
                         (.focus (.-current ref))
                         js/undefined))
      [todo-input (assoc props :input-ref ref)]))

  (defn todo-stats [{:keys [filt active done]}]
    (let [props-for (fn [name]
                      {:class (when (= name @filt) "selected")
                       :on-click #(reset! filt name)})]
      [:div
       [:span#todo-count
        [:strong active] " " (case active 1 "item" "items") " left"]
       [:ul#filters
        [:li [:a (props-for :all) "All"]]
        [:li [:a (props-for :active) "Active"]]
        [:li [:a (props-for :done) "Completed"]]]
       (when (pos? done)
         [:button#clear-completed {:on-click clear-done}
          "Clear completed " done])]))

  (defn todo-item []
    (let [editing (r/atom false)]
      (fn [{:keys [id done title]}]
        [:li
         {:class [(when done "completed ")
                  (when @editing "editing")]}
         [:div.view
          [:input.toggle
           {:type "checkbox"
            :checked done
            :on-change #(toggle id)}]
          [:label
           {:on-double-click #(reset! editing true)}
           title]
          [:button.destroy {:on-click #(delete id)}]]
         (when @editing
           [:f> todo-edit {:class "edit"
                           :title title
                           :on-save #(save id %)
                           :on-stop #(reset! editing false)}])])))

  (defn todo-app []
    (let [filt (r/atom :all)]
      (fn []
        (let [items (vals @todos)
              done (->> items (filter :done) count)
              active (- (count items) done)]
          [:div
           [:section#todoapp
            [:header#header
             [:h1 "todos"]
             [todo-input {:id "new-todo"
                          :placeholder "What needs to be done?"
                          :on-save add-todo}]]
            (when (-> items count pos?)
              [:div
               [:section#main
                [:input#toggle-all {:type "checkbox" :checked (zero? active)
                                    :on-change #(complete-all (pos? active))}]
                [:label {:for "toggle-all"} "Mark all as complete"]
                [:ul#todo-list
                 (for [todo (filter (case @filt
                                      :active (complement :done)
                                      :done :done
                                      :all identity) items)]
                   ^{:key (:id todo)} [todo-item todo])]]
               [:footer#footer
                [todo-stats {:active active :done done :filt filt}]]])]
           [:footer#info
            [:p "Double-click to edit a todo"]]]))))

  (render [todo-app])

)
