(ns hello-reagent.managing-state
  (:require
   [hello-reagent.basics :as basics]
   [reagent.core :as r]
   [reagent.ratom :as ra]))


;; Managing state

;; https://cljdoc.org/d/reagent/reagent/1.2.0/doc/tutorials/-wip-managing-state-atoms-cursors-reactions-and-tracking

(enable-console-print!)
(println "Managing state: ns loaded")

(comment

  ;; atoms, cursors, Reactions, and tracking

  ;; Although it is possible to update reagent components by remounting the
  ;; entire component tree with `reagent.dom/render`, Reagent comes with a
  ;; sophisticated state management library based on
  ;; `reagent.core/atom` (ratom), which allows components to track application
  ;; state and update only when needed. Reagent also provides cursors, which are
  ;; like ratoms but can be constructed from portions of one or more other
  ;; ratoms to limit or expand which ratoms a component watches. Finally,
  ;; Reagent provides a set of tracking primitives called reactions and a set of
  ;; utility functions to build more customized state management.


  ;; ratoms

  ;; Reagent provides an implementation of atom that you can create with
  ;; `reagent.core/atom` and use just like a normal Clojure atom, which are
  ;; often referred to as "ratoms" to distinguish from normal atoms. Reagent
  ;; tracks any dereferences to ratoms made during a component's render
  ;; function.

  (def click-count (r/atom 0))

  (defn counting-component []
    [:div
     "The atom " [:code "click-count"] " has value: "
     @click-count ". "
     [:input {:type     "button" :value "Click me!"
              :on-click #(swap! click-count inc)}]])

  (basics/render [counting-component])

  ;; mutating a ratom

  ;; using the standard `reset!` and `swap!` functions.

  (def state-atom (r/atom nil))

  (reset! state-atom {:counter 0})
  (swap! state-atom assoc :counter 15)
  (swap! state-atom update :counter inc)

  ;; dereferencing a ratom

  (:counter (deref state-atom))
  (:counter @state-atom)

  ;; the effect of dereferencing a ratom

  ;; dereferencing the ratom inside the render function will cause that
  ;; component to re-render whenever any part of that ratom is updated.

  ;; dereferencing a ratom in a callback or event handler after the render
  ;; function has run will not make the component react to any changes to the
  ;; ratom (though of course any changes to the ratom made in an event handler
  ;; will make any watching components re-render).

  ;; `rswap!`

  ;; `rswap!` works like standard `swap!` except that it

  ;; - always returns nil
  ;; - allows recursive applications of `rswap!` on the same atom

  ;; that makes `rswap!` especially suited for event handling.

  (def app-state (r/atom {1 {:name "Joe"}
                          2 {:name "Molly"}
                          3 {:name "Eddie"}}))

  (defn person [id]
    (get @app-state id))

  (defn person-keys []
    (keys @app-state))

  (defn name-list
    []
    (interpose ", " (map :name (vals @app-state))))

  (defn event-handler [state [event-name id value]]
    (case event-name
      :set-name   (assoc-in state [id :name] value)
      :add-person (let [new-key (->> (person-keys) (apply max) inc)]
                    (assoc state new-key {:name ""}))
      state))

  (defn emit [e]
    (js/console.log "Handling event" (str e))
    (r/rswap! app-state event-handler e))

  (defn name-edit [id]
    (let [p @(r/track person id)]
      [:div
       [:input {:value     (:name p)
                :on-change #(emit [:set-name id (.. % -target -value)])}]]))

  (defn edit-fields []
    (let [ids @(r/track person-keys)]
      [:div
       [name-list]
       (for [i ids]
         ^{:key i} [name-edit i])
       [:input {:type     'button
                :value    "Add person"
                :on-click #(emit [:add-person])}]
       [:br]
       @app-state]))

  (basics/render [edit-fields])

  ;; all events are passed through the `emit` function, consisting of a trivial
  ;; application of `rswap!` and some optional logging. This is the only place
  ;; where application state actually changes –-- the rest is pure functions.

  ;; the actual event handling is done in `event-handler`, which takes state and
  ;; event as parameters, and returns a new state (events are represented by
  ;; vectors here, with an event name in the first position).

  ;; all the UI components have to do is then just to return some markup, and
  ;; set up routing of events through the emit function.

  ;; this architecture basically divides the application into two logical
  ;; functions:

  ;; - the first takes state and an event as input, and returns the next state.
  ;; - The other takes state as input, and returns a UI definition.

  ;; this simple application could probably just as well use the common `swap!`
  ;; instead of `rswap!`, but using `swap!` in React’s event handlers may
  ;; trigger warnings due to unexpected return values, and may cause severe
  ;; headaches if an event handler called by `emit` itself emits a new
  ;; event (that would result in lost events, and much confusion).

  ;; for a more structured version of a similar approach, see the excellent
  ;; `re-frame` framework.


  ;; cursors

  ;; any component that dereferences a state atom will update whenever any part
  ;; of it is updated. If you are storing all state in a single atom (not
  ;; uncommon), it will cause every component to update whenever the state is
  ;; updated. Performance-wise, this may be acceptable, depending on how many
  ;; elements you have and how often your state updates, because React itself
  ;; will not manipulate the DOM unless the components actually change.

  ;; Reagent provides cursors, which behave like atoms but operate like pointers
  ;; into a larger atom (or into multiple parts of multiple atoms).

  ;; cursors are created with `reagent.core/cursor`, which takes a ratom and a
  ;; keypath (like `get-in`):

  ;; first create a ratom

  (def state (r/atom {:foo  {:bar "BAR"}
                      :baz  "BAZ"
                      :quux "QUUX"}))

  ;; now create a cursor

  (def bar-cursor (r/cursor state [:foo :bar]))

  (defn quux-component []
    (js/console.log "quux-component is rendering")
    [:div (:quux @state)])

  (defn bar-component []
    (js/console.log "bar-component is rendering")
    [:div @bar-cursor])

  (defn mount-root []
    (basics/render [:div [quux-component] [bar-component]])
    (js/setTimeout (fn [] (swap! state assoc :baz "NEW BAZ")) 1000)
    (js/setTimeout (fn [] (swap! state assoc-in [:foo :bar] "NEW BAR")) 2000))

  ;; both `bar-component` and `quux-component` update whenever their respective
  ;; cursors/atoms update, but because `bar-component`'s cursor is limited only
  ;; to the relevant portion of the app state, it only re-renders when
  ;; `[:foo :bar]` updates, whereas `quux-component` updates each time app state
  ;; changes, even though `:quux` never changes.

  ;; more general cursors

  ;; the cursor mechanism is more general than described above. Instead of a
  ;; keypath, you can pass it a function that performs arbitrary transformations
  ;; on one or more atoms.

  ;; wrap

  ;; Reagent also provides the `reagent.core/wrap` mechanism, which also derives
  ;; a new ratom but provides more general functionality. Where a cursor will
  ;; always update the ratom from which it was derived, `reagent.core/wrap`
  ;; takes a ratom and a callback that will be called whenever the derived ratom
  ;; is updated. Replacing `(r/cursor n [:first-name])`
  ;; with `(r/wrap (:first-name @n) swap! n assoc :first-name)` gives
  ;; essentially the same results.


  ;; reactions

  ;; reactions are like cursors called with a function.

  ;; when reactions produce a new result (as determined by `=`), they cause
  ;; other dependent reactions and components to update.

  ;; the function `reagent.ratom/make-reaction`, and its macro
  ;; `reagent.ratom/reaction` are used to create a Reaction, which is a type that
  ;; implements a number of protocols such as `IWatchable`, `IAtom`,
  ;; `IReactiveAtom`, `IDeref`, `IReset`, `ISwap`, `IRunnable`, etc. which make
  ;; it atom-like: ie it can be watched, derefed, reset, swapped on, and
  ;; additionally, tracks its derefs, behave reactively, and so on.

  ;; reactions are what give `r/atom`, `r/cursor`, and `r/wrap` their power.

  ;; `make-reaction` takes one argument, `f`, and an optional options map. The
  ;; options map specifies what happens to `f`:

  ;; - `:auto-run` (boolean) specifies whether `f` runs on change
  ;; - `:on-set` and `:on-dispose` are run when the reaction is set and unset
  ;;   from the DOM

  ;; reactions are very useful when

  ;; - you need a way in which a component only updates based on part of the
  ;;   ratom state. (`r/cursor` can also be used for this scenario)
  ;; - you want to combine two ratoms and produce a result
  ;; - you want the component to use some transformed value of ratom

  (def app-state (r/atom {:state-var-1 {:var-a 2
                                        :var-b 3}
                          :state-var-2 {:var-a 7
                                        :var-b 9}}))

  (def app-var2a-reaction (ra/make-reaction
                           #(get-in @app-state [:state-var-2 :var-a])))

  (defn component-using-make-reaction []
    [:div
     [:div "component-using-make-reaction"]
     [:div "state-var-2 - var-a : " @app-var2a-reaction]])

  (basics/render [component-using-make-reaction])
  (swap! app-state assoc-in [:state-var-2 :var-a] 12)

  ;; example below uses `reagent.ratom/reaction` macro, which provides syntactic
  ;; sugar compared to using plain `make-reaction`:

  (def username (r/atom ""))
  (def password (r/atom ""))
  (def fields-populated? (ra/reaction
                          (every? not-empty [@username @password])))

  @fields-populated?
  (reset! username "Joe")
  (reset! password "secret")

  ;; reactions are executed asynchronously, so be sure to call `r/flush` if you
  ;; depend on reaction side effects.


  ;; `track`

  ;; `reagent.core/track` takes a function, and optional arguments for that
  ;; function, and gives a derefable (i.e "atom-like") value, containing
  ;; whatever is returned by that function. If the tracked function depends on a
  ;; Reagent atom, it is called again whenever that atom changes --– just like a
  ;; Reagent component function. If the value returned by track is used in a
  ;; component, the component is re-rendered when the value returned by the
  ;; function changes.

  ;; in other words, `@(r/track foo x)` gives the same result as `(foo x)` --– but
  ;; in the first case, `foo` is only called again when the atom(s) it depends on
  ;; changes.

  (defonce app-state (r/atom {:people
                              {1 {:name "John Smith"}
                               2 {:name "Maggie Johnson"}}}))

  (defn people []
    (:people @app-state))

  (defn person-keys []
    (-> @(r/track people)
        keys
        sort))

  (defn person [id]
    (-> @(r/track people)
        (get id)))

  (defn name-comp [id]
    (let [p @(r/track person id)]
      [:li
       (:name p)]))

  (defn name-list []
    (let [ids @(r/track person-keys)]
      [:ul
       (for [i ids]
         ^{:key i} [name-comp i])]))

  ;; here, the `name-list` component will only be re-rendered if the keys of the
  ;; `:people` map changes. Every `name-comp` only renders again when needed,
  ;; etc.

  ;; use of `track` can improve performance in three ways:

  ;; - it can be used as a cache for an expensive function, that is
  ;;   automatically updated if that function depends on Reagent atoms (or other
  ;;   tracks, cursors, etc).
  ;; - it can also be used to limit the number of times a component is
  ;;   re-rendered. The user of track is only updated when the function's
  ;;   result changes. In other words, you can use track as a kind of
  ;;   generalized, read-only cursor.
  ;; - every use of track with the same arguments will only result in one
  ;;   execution of the function. E.g the two uses of @(r/track people) in the
  ;;   example above will only result in one call to the `people` function
  ;;   (both initially, and when the state atom changes).

  ;; note: `reaction` and `track` are similar. The main differences are that
  ;; `track` uses named functions and variables, rather than depending on
  ;; closures, and that you don’t have to manage their creation manually (since
  ;; tracks are automatically cached and reused).

  ;; note: the first argument to track `should` be a named function, i.e not an
  ;; anonymous one. Also, beware of lazy data sequences: don't use `deref` (i.e
  ;; ”@”) with the for macro, unless wrapped in `doall` (just like in Reagent
  ;; components).


  ;; `track!`

  ;; `track!` works just like `track`, except that the function passed is
  ;; invoked immediately, and continues to be invoked whenever any atoms used
  ;; within it changes.

  ;; for example, given this function:

  (def app-state (r/atom {}))

  (defn log-app-state []
    (prn @app-state)
    @app-state)

  ;; you could use `(defonce logger (r/track! log-app-state))` to monitor
  ;; changes to app-state. `log-app-state` would continue to run until you stop
  ;; it, using `(r/dispose! logger)`.

  (def logger (r/track! log-app-state))

  (r/rswap! app-state assoc :x 1)

  (log-app-state)

  @logger

  ;; not clear what use it all have, should reread later

)
