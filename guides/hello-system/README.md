# hello-system

Exploring libraries and frameworks for building systems.

- Mount: managing Clojure(Script) app state https://github.com/tolitius/mount
- Component: managed lifecycle of stateful objects in Clojure(Script)
  https://github.com/stuartsierra/component
- Integrant: micro-framework for data-driven architecture
  https://github.com/weavejester/integrant
- Duct: server-side application framework for Clojure
  https://github.com/duct-framework/duct

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
