(ns hello-system.mount.web
  (:require
   [mount.core :as mount :refer [defstate]]
   [ring.adapter.jetty :refer [run-jetty]]
   [hello-system.mount.config :refer [config]]))


(defn app [request]
  {:status 200 :body (str request)})

(defstate ^{:on-reload :noop} web-server
  :start (let [{jetty-options :jetty} config]
           (run-jetty #'app jetty-options))
  :stop (.stop web-server))
