(ns hello-system.mount.config
  (:require [mount.core :as mnt]
            [clojure.edn :as edn]
            [clojure.tools.logging :refer [info]]
            [clojure.spec.alpha :as s]))

(s/def ::config map?)

(defn load-config [filename]
  (info "Config loaded from" filename)
  (-> filename
      slurp
      edn/read-string
      (as-> config
          (s/conform ::config config))))

(mnt/defstate config
  "Global application config."
  :start (load-config "resources/mount/config.edn"))
