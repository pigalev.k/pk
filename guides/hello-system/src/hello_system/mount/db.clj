(ns hello-system.mount.db
  (:require
   [mount.core :as mount :refer [defstate]]
   [hikari-cp.core :as cp]
   [hello-system.mount.config :refer [config]]))

(defstate db
  :start
  (let [{pool-options :pool} config
        datasource (cp/make-datasource pool-options)]
    {:datasource datasource})
  :stop
  (-> db :datasource cp/close-datasource))
