(ns hello-system.mount.core
  (:require [mount.core :as mnt]
            [clojure.tools.namespace.repl :as tn]
            [hello-system.mount.config]
            [hello-system.mount.web]
            [hello-system.mount.db]))


(defn start []
  (mnt/start)
  :started)

(defn stop []
  (mnt/stop)
  :stopped)

(defn reset []
  (mnt/stop)
  (tn/refresh :after `start)
  :restarted)


(comment

  (start)
  (stop)
  (reset)

)
