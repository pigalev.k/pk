(ns hello-system.duct.core
  (:require
   [duct.core :as duct]
   [integrant.core :as ig]))


;; basics

;; https://github.com/duct-framework/core
;; https://github.com/duct-framework/docs/blob/master/GUIDE.rst
;; https://cljdoc.org/d/sweet-tooth/endpoint/0.10.10/doc/duct-tutorial-

(comment

  ;; ordinary Integrant stuff

  (defmethod ig/init-key ::message-store [_ {:keys [message]}]
    (atom message))

  (defmethod ig/init-key ::printer [_ {:keys [store]}]
    (prn (format "%s says: %s" ::printer @store)))

  (derive ::message-store ::store)


  ;; duct

  ;; `duct/load-hierarchy` looks for files named `duct_hierarchy.edn` on your
  ;; classpath and uses them to establish keyword hierarchies

  (duct/load-hierarchy)

  ;; `duct/prep-config` takes a duct config as its argument and returns an
  ;; integrant config. How does a duct config differ from an integrant config?

  ;; - the keys for duct configs name either *duct profiles* or *duct
  ;;   modules*. The keys for integrant configs name *integrant components*.
  ;; - duct configs are meant to be passed to `duct/prep-config`, which returns
  ;;   an integrant config. Integrant configs are meant to be passed to
  ;;   `ig/init`, which initializes and returns a system.

  (def duct-config
    {:duct.profile/base {::message-store {:message "love yourself, homie"}
                         ::printer       {:store (ig/ref ::store)}}})

  (def integrant-config
    (duct/prep-config duct-config))

  (ig/init integrant-config)


  ;; TODO: follow through
)
