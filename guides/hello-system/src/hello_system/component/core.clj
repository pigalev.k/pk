(ns hello-system.component.core
  (:require
   [com.stuartsierra.component :as component]))


(defn connect-to-database
  "Create a database connection."
  [host port]
  (println ";; Opening database connection...")
  (reify java.io.Closeable
    (close [this]
      (println ";; Closing database connection..."))))


;; 1. To create a component, define a Clojure record that implements the
;; Lifecycle protocol.

(defrecord Database [host port connection]
  ;; Implement the Lifecycle protocol
  component/Lifecycle

  (start [component]
    (println ";; Starting database")
    ;; In the 'start' method, initialize this component
    ;; and start it running. For example, connect to a
    ;; database, create thread pools, or initialize shared
    ;; state.
    (let [conn (connect-to-database host port)]
      ;; Return an updated version of the component with
      ;; the run-time state assoc'd in.
      (assoc component :connection conn)))

  (stop [component]
    (println ";; Stopping database")
    ;; In the 'stop' method, shut down the running
    ;; component and release any external resources it has
    ;; acquired.
    (.close connection)
    ;; Return the component, optionally modified. Remember that if you
    ;; dissoc one of a record's base fields, you get a plain map.
    (assoc component :connection nil)))


;; 2. Optionally, provide a constructor function that takes arguments for the
;; essential configuration parameters of the component, leaving the runtime
;; state blank.

(defn new-database
  [host port]
  (map->Database {:host host :port port}))


;; 3. Define the functions implementing the behavior of the component to take an
;; instance of the component as an argument.

(defn get-user [database username]
  {:username username :id 1})

(defn add-user [database username favorite-color]
  {:result "ok"})


;; 4. Define other components in terms of the components on which they
;; depend.

(defrecord ExampleComponent [options cache database scheduler]
  component/Lifecycle
  (start [this]
    (println ";; Starting ExampleComponent")
    ;; In the 'start' method, a component may assume that its
    ;; dependencies are available and have already been started.
    (assoc this :admin (get-user database "admin")))
  (stop [this]
    (println ";; Stopping ExampleComponent")
    ;; Likewise, in the 'stop' method, a component may assume that its
    ;; dependencies will not be stopped until AFTER it is stopped.
    this))

;; Do not pass component dependencies in a constructor. Systems are
;; responsible for injecting runtime dependencies into the components they
;; contain.

(defn example-component [config-options]
  (map->ExampleComponent {:options config-options
                          :cache (atom {})}))


;; 5. Components are composed into systems. A system is a component which knows
;; how to start and stop other components. It is also responsible for injecting
;; dependencies into the components which need them.

;; The easiest way to create a system is with the `system-map` function, which
;; takes a series of key/value pairs just like the `hash-map` or `array-map`
;; constructors. Keys in the system map are keywords. Values in the system map
;; are instances of components, usually records or maps.

(defn example-system [config-options]
  (let [{:keys [host port]} config-options]
    (component/system-map
     :db (new-database host port)
     :scheduler "scheduler"
     :app (component/using
           (example-component config-options)
           ;; or like that, if keys are the same in component and system:
           ;; [:database :scheduler]
           {:database  :db
            :scheduler :scheduler}))))

;; 6. Start and stop the system.

(defonce state (atom nil))
(def config {:host "localhost"
             :port 3345})

(defn start-system! []
  (reset! state (component/start (example-system config))))

(defn stop-system! []
  (reset! state (component/stop @state)))


(comment

  (start-system!)
  (stop-system!)

)
