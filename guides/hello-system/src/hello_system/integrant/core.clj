(ns hello-system.integrant.core
  (:require
   [clojure.spec.alpha :as s]
   [integrant.core :as ig]
   [ring.adapter.jetty :as jetty]))


;; configuration

(s/def :handler/greet map?)
(s/def :adapter.jetty/port (s/int-in 1 65536))
(s/def :adapter.jetty/join? boolean?)
(s/def :adapter.jetty/handler fn?)
(s/def :adapter/jetty (s/keys :req-un [:jetty/port :jetty/handler]))

(def config
  {:adapter/jetty {:port    8080
                   :join?   false
                   :handler (ig/ref :handler/greet)}
   :handler/greet {:name "Alice"}})


;; alternatively, you can specify your configuration as pure edn:
;; {:adapter/jetty {:port 8080, :handler #ig/ref :handler/greet}
;;  :handler/greet {:name "Alice"}}

;; and load it with Integrant's version of read-string:
;; (def config
;;   (ig/read-string (slurp "config.edn")))


;; define initialization/deinitialization of the configuration keys to turn the
;; config into a configured and running system

(defmethod ig/init-key :handler/greet
  [key {:keys [name] :as greet}]
  (constantly {:status 200 :body (str "Hello, " name)}))

(defmethod ig/halt-key! :handler/greet
  [key handler] nil)

(defmethod ig/pre-init-spec :handler/greet
  [_] :handler/greet)


(defmethod ig/init-key :adapter/jetty
  [key {:keys [handler] :as options}]
  (jetty/run-jetty handler options))

(defmethod ig/halt-key! :adapter/jetty
  [key server]
  (.stop server))

(defmethod ig/pre-init-spec :adapter/jetty
  [_] :adapter/jetty)



;; define, start and stop a system

(defonce system (atom nil))

(defn start-system!
  "Start a system with given configuration."
  [config]
  (reset! system (ig/init config)))

(defn stop-system!
  "Stop the system."
  [system]
  (ig/halt! @system)
  (reset! system nil))


(comment

  (start-system! config)
  @system

  (stop-system! system)
  @system

)
