(ns hello-transit-clj.core
  (:require
   [cognitect.transit :as t])
  (:import
   (java.io ByteArrayInputStream ByteArrayOutputStream)))


;; intro

(comment

  ;; write data to a stream
  (def out (ByteArrayOutputStream. 4096))
  (def writer (t/writer out :json))
  (t/write writer "foo")
  (t/write writer {:a [1 2]})

  ;; Take a peek at the JSON
  (.toString out)
  ;; => "{\"~#'\":\"foo\"} [\"^ \",\"~:a\",[1,2]]"

  ;; Read data from a stream
  (def in (ByteArrayInputStream. (.toByteArray out)))
  (def reader (t/reader in :json))
  (prn (t/read reader))
  (prn (t/read reader))

)
