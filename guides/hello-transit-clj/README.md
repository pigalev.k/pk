# hello-transit-clj

Exploring transit-clj --- Clojure implementation of the transit data interchange
format.

- https://github.com/cognitect/transit-clj
- https://github.com/cognitect/transit-format

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
