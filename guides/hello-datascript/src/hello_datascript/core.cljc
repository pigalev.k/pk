(ns hello-datascript.core
  (:require
   [datascript.core :as d]))


;; https://github.com/kristianmandrup/datascript-tutorial
;; https://tonsky.me/blog/datascript-internals/


;; basics:
;; - create a new db connection (schemaless)
;; - transact some datoms into db
;; - execute db queries to fetch a result

(defn id
  "Get an entity id in given db by specified attribute and value."
  [db attr val]
  (d/q '[:find ?p .
         :in $ ?attr ?val
         :where
         [?p ?attr ?val]]
       db attr val))

(comment

  ;; define some schema

  (def schema {:name    {:db/doc         "The person name."
                         :db/unique      :db.unique/identity
                         :db/cardinality :db.cardinality/one}
               :age     {:db/doc         "The person age."
                         :db/cardinality :db.cardinality/one}
               :likes   {:db/doc         "A list of things the person likes."
                         :db/index       true
                         :db/cardinality :db.cardinality/many}
               :friends {:db/doc         "Friends of the person."
                         :db/valueType   :db.type/ref
                         :db/cardinality :db.cardinality/many}})

  ;; create a Datascript connection (an atom with the current db value)

  (def conn (d/create-conn schema))


  ;; define some datoms

  (def datoms [{:db/id -1
                :name "Bob"
                :age 30
                :likes ["pizza" "coffee"]}
               {:db/id -2
                :name "Sally"
                :age 15
                :likes ["candy"]}
               {:db/id -3
                :name "Eddie"
                :age 24
                :friends [-1 -2]
                :likes ["talk" "jokes"]}])

  ;; add the datoms to db in one transaction

  (d/transact! conn datoms)

  ;; query db to find names for entities (people) whose age is less than 18

  (def pull-young-people-query
    '[:find (pull ?p [*])
      :in $ ?min-age
      :where
      [?p :age ?a]
      [(< ?a ?min-age)]])

  (d/q pull-young-people-query @conn 18)

  ;; history is manual in Datascript; take a reference to db at this point

  (defonce history (atom @conn))

  ;; retract an entity by id

  (def retract-young-people-tx-data
    [[:db/retractEntity (id @conn :name "Sally")]])

  ;; see transaction report

  (def retract-sally-tx-report
    (d/transact! conn retract-young-people-tx-data))

  (:tx-data retract-sally-tx-report)

  ;; retract attribute from an entity

  (def retract-age-attribute-tx-data
    [[:db.fn/retractAttribute (id @conn :name "Eddie") :age]])

  (def retract-age-tx-report
    (d/transact! conn retract-age-attribute-tx-data))

  (first (:tx-data retract-age-tx-report))

  ;; find all people in current db; Sally is gone, and Eddie is ageless

  (d/q '[:find (pull ?p [*])
         :where
         [?p :name]]
       @conn)

  ;; find all people in history snapshot; Sally is here, so is Eddie's age

  (d/q '[:find (pull ?p [*])
         :where
         [?p :name]]
       @history)

  ;; entities

  (:name (d/entity @conn 1))

  ;; datoms: direct index access

  (d/datoms @conn :eavt)
  (d/datoms @conn :aevt)
  (d/datoms @conn :avet)

  ;; filtering: only datoms with `:name` attribute are visible

  (def name-db (d/filter @conn (fn has-name? [db datom]
                                 (= :name (second datom)))))
  (d/q '[:find (pull ?p [*])
         :where
         [?p :name]]
       name-db)

  (d/datoms name-db :eavt)
  (d/datoms name-db :aevt)
  (d/datoms name-db :avet)
  (:eavt @conn)

  ;; schema: a separate map
  (keys @conn)
  (:schema @conn)
  (:rschema @conn)

)
