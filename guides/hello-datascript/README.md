# hello-datascript

Exploring Datascript --- an immutable database and Datalog query engine for
Clojure(Script) and JS.

## Getting started

Start a cljs REPL in terminal

```
clj -M -m cjls.main
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
