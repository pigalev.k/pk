# hello-clojurecl

Exploring clojurecl --- a library for parallel computations with OpenCL 2.0 in
Clojure.

- https://github.com/uncomplicate/clojurecl
- https://clojurecl.uncomplicate.org/

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
