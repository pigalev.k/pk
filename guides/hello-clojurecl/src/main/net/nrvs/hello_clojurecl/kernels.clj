(ns net.nrvs.hello-clojurecl.kernels
  (:require
   [net.nrvs.hello-clojurecl.tools :as tools]
   [uncomplicate.clojurecl.core :refer [platforms devices context command-queue cl-buffer work-size-1d set-arg! enq-kernel! enq-read! kernel build-program! program-with-source]]
   [uncomplicate.commons.core :refer [release]]))


;; Loading and using custom kernels

(def my-amd-platform (first (platforms)))
(def my-amd-gpu (first (devices my-amd-platform)))
(def ctx (context [my-amd-gpu]))
(def queue (command-queue ctx my-amd-gpu))

(def hello-kernel-source (tools/load-classpath-resource "kernels/hello.cl"))
(def hello-program (build-program!
                    (program-with-source ctx [hello-kernel-source])))
(def hello-kernel (kernel hello-program "hello_kernel"))

(def data (cl-buffer ctx 16 :read-write))
(def result (byte-array 16))

(set-arg! hello-kernel 0 data)
(enq-kernel! queue hello-kernel (work-size-1d 16))
(enq-read! queue data result)

(apply str (map char result))
;; => "Hello kernel!!!"


;; Clean up

(release data)
(release hello-program)
(release queue)
(release ctx)
