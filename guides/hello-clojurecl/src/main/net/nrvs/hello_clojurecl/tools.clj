(ns net.nrvs.hello-clojurecl.tools
  (:require [clojure.java.io :as io]))


(defn load-classpath-resource
  "Load a classpath resource into a string. Throw if loading fails."
  [resource-name]
  (if-let [resource (io/resource resource-name)]
    (slurp resource)
    (throw (ex-info (str "Can't open classpath resource: " resource-name) {}))))
