(ns main.net.nrvs.hello-clojurecl.core
  (:require [uncomplicate.clojurecl.core :refer [platforms devices context cl-buffer command-queue enq-write! enq-read! work-size-1d set-arg! enq-kernel! kernel build-program! program-with-source]]
            [uncomplicate.clojurecl.info :refer [name-info]]
            [uncomplicate.commons.core :refer [info release]]))


;; https://dragan.rocks/articles/18/Interactive-GPU-Programming-2-Hello-OpenCL
;; https://bashbaug.github.io/opencl/2019/07/06/OpenCL-On-Linux.html
;; http://www.jocl.org/


;; General info

(def my-platforms-info (map info (platforms)))
(def my-devices-info (map info (devices (first (platforms)))))


;; Platform ans device handles

(def my-amd-platform (first (platforms)))
(def my-amd-gpu (first (devices my-amd-platform)))


(def my-amd-platform-name (name-info my-amd-platform))
(def my-amd-gpu-name (name-info my-amd-gpu))


;; Context is like a process but for GPU(s)
(def ctx (context [my-amd-gpu]))

;; Allocate memory on GPU
(def gpu-array (cl-buffer ctx 1024 :read-write))

;; Create the data
(def main-array (float-array (range 256)))

;; Load data onto GPU
(def queue (command-queue ctx my-amd-gpu))
(enq-write! queue gpu-array main-array)

;; Ensure that data were indeed loaded (read it back)
(def roundtrip-array (float-array 256))
(enq-read! queue gpu-array roundtrip-array)
(take 12 roundtrip-array)


;; Compute something
(def kernel-source
      "__kernel void mul10(__global float *a) {
         int i = get_global_id(0);
         a[i] = a[i] * 10.0f;
       };")

(def hello-program (build-program! (program-with-source ctx [kernel-source])))
(def mul10 (kernel hello-program "mul10"))
(def result (float-array 256))
(set-arg! mul10 0 gpu-array)
(enq-kernel! queue mul10 (work-size-1d 256))
(enq-read! queue gpu-array result)

(take 12 result)


;; Clean up

(release gpu-array)
(release hello-program)
(release queue)
(release ctx)
