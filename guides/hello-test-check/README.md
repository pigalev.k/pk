# hello-test-check

Exploring test.check --- a property-based testing library for clojure, inspired
by QuickCheck.

- https://clojure.org/guides/test_check_beginner

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
