(ns hello-test-check.core
  (:require
   [clojure.test.check :as tc]
   [clojure.test.check.generators :as gen]
   [clojure.test.check.properties :as prop]
   [clojure.test.check.clojure-test :as tcct]))


(comment

  ;; define a property

  (def sort-nondiminishing-property
    (prop/for-all [v (gen/vector gen/small-integer)]
                  (let [s (sort v)]
                    (and (= (count v) (count s))
                         (or (empty? s)
                             (apply <= s))))))

  ;; test the property

  (tc/quick-check 100 sort-nondiminishing-property)


  (def bad-property
    (prop/for-all [v (gen/vector gen/small-integer)]
                  (or (empty? v) (apply <= v))))

  (tc/quick-check 100 bad-property)


  ;; `clojure.test` integration

  (tcct/defspec first-element-is-min-after-sorting ;; the name of the test
    100 ;; the number of iterations for `test.check` to test
    (prop/for-all [v (gen/not-empty (gen/vector gen/small-integer))]
                  (= (apply min v)
                     (first (sort v)))))

  (tcct/defspec last-element-is-max-after-sorting ;; the name of the test
    100 ;; the number of iterations for test.check to test
    (prop/for-all [v (gen/not-empty (gen/vector gen/small-integer))]
                  (= (apply max v)
                     (last (sort v)))))

)
