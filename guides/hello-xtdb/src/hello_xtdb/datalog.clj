(ns hello-xtdb.datalog
  "A guide on XTDB dialect of Datalog."
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]
            [xtdb.api :as xt])
  (:import [java.io PushbackReader]))



;; XTDB Datalog dialect

;; https://nextjournal.com/try/learn-xtdb-datalog-today/learn-xtdb-datalog-today


;; overview, starting the db instance

(comment

  ;; Learn XTDB Datalog Today is derived from the
  ;; classic [learndatalogtoday.org](http://learndatalogtoday.org) tutorial
  ;; materials, but adapted to focus on the [xtdb](https://xtdb.com) API and
  ;; unique Datalog semantics, and extended with additional topics and
  ;; exercises. It is an interactive tutorial designed to teach you the xtdb
  ;; dialect of Datalog. Datalog is a declarative database query language with
  ;; roots in logic programming. Datalog has similar expressive power to SQL.


  ;; runtime setup

  ;; you need to get xtdb running before you can use it. Here we are using
  ;; Clojure on the JVM and running xtdb locally, as an embedded in-memory
  ;; library. However, the Datalog query API is identical for all clients and
  ;; all these queries would work equally well over the network via HTTP and the
  ;; Java/Clojure client library.

  ;; note that xtdb also has a JSON-over-HTTP API that naturally supports JSON
  ;; documents, this is possible because JSON can be very simply mapped to a
  ;; subset of edn.

  ;; to start an in-memory instance of xtdb, you can use the `start-node`
  ;; function like so:

  (def xtdb-node (xt/start-node {}))

)


;; prepare the data

(comment

  ;; next we need some data. XTDB interprets maps as "documents." These require
  ;; no pre-defined schema -- they only need a valid ID attribute. In the data
  ;; below (which is defined using edn, and fully explained in the section) we
  ;; are using negative integers as IDs. Any top-level attributes that refer to
  ;; these integers can be interpreted in an ad-hoc way and traversed by the
  ;; query engine -- this capability is known as "schema-on-read".

  (defn read-edn-file
  "Read EDN data from file."
    [filename]
    (with-open [reader (io/reader filename)]
      (edn/read (PushbackReader. reader))))

  (def docs (read-edn-file "resources/data.edn"))

  ;; this vector of maps contains two kinds of documents: documents relating to
  ;; people (actors and directors) and documents relating to movies. As a
  ;; convention to aid human interpretation, all persons have IDs like `-1XX`
  ;; and all movies have IDs like `-2XX`. Many ID value types are supported,
  ;; such as strings and UUIDs, which may be more appropriate in a real
  ;; application.

  ;; loading the small amount of data we defined in `docs` above can be
  ;; comfortably done in a single transaction. In practice you will often find
  ;; benefit to batch `put` operations into groups of 1000 at a time. The
  ;; following code maps over the docs to generate a single transaction
  ;; containing one `:xtdb.api/put` operation per document, then submits the
  ;; transaction. Finally we call the `sync` function to ensure that the
  ;; documents are fully indexed (and that the transaction has succeeded) before
  ;; we attempt to run any queries -- this is necessary because of xtdb's
  ;; asynchronous design.

  ;; transact the data in

  (xt/submit-tx xtdb-node
                (for [doc docs] [:xtdb.api/put doc]))
  (xt/sync xtdb-node)

)


;; query the db

(comment

  ;; With xtdb running and the data loaded, you can now execute a query, which
  ;; is a Clojure map (or vector), by passing it to xtdb's `q` API, which takes
  ;; the result of a `db` call as it's first value.

  ;; in this tutorial we will use the map format. Also note that xtdb does not
  ;; require logical variables to be preceded by `?`, although you may use this
  ;; convention if you wish.


  ;; to simplify this `xt/q` call throughout the rest of the tutorial we can
  ;; define a new `q` function that saves us a few characters and visual
  ;; clutter.

  (defn q [query & args]
    (apply xt/q (xt/db xtdb-node) query args))


  ;; basic queries

  ;; the example database we'll use contains *movies* mostly, but not
  ;; exclusively, from the 80s. You'll find information about movie titles,
  ;; release year, directors, cast members, etc. As the tutorial advances we'll
  ;; learn more about the contents of the database and how it's organized.

  ;; the data model in xtdb is based around *atomic collections of facts.* Those
  ;; atomic collections of facts are called *documents.* The facts are called
  ;; triples. A triple is a 3-tuple consisting of:

  ;; - Entity ID
  ;; - Attribute
  ;; - Value

  ;; although it is the document which is atomic in xtdb (and not the triple),
  ;; you can think of the database as a flat **set of triples** of the form:

  ;; [<e-id>  <attribute>      <value>          ]
  ;; ...
  ;; [ -167    :person/name     "James Cameron" ]
  ;; [ -234    :movie/title     "Die Hard"      ]
  ;; [ -234    :movie/year      1987            ]
  ;; [ -235    :movie/title     "Terminator"    ]
  ;; [ -235    :movie/director  -167            ]
  ;; ...

  ;; note that the last two triples share the same entity ID, which means they
  ;; are facts about the same movie (one *document*). Note also that the last
  ;; triple's value is the same as the first triple's entity ID, i.e. the value
  ;; of the `:movie/director` attribute is itself an entity.

  ;; a query is represented as a map with at least two key-value pairs. In the
  ;; first pair, the key is the keyword `:find`, and the value is a vector of
  ;; one or more **logic variables** (symbols, e.g. `?title` or `e`). The other
  ;; key-value pair is the `:where` keyword key with a vector of clauses which
  ;; restrict the query to triples that match the given **data patterns**.

  (def movie-titles
    (q '{:find [?title]
         :where [[_ :movie/title ?title]]}))

  (count movie-titles)

  (def movies
    (q '{:find [(pull ?e [*])]
         :where [[?e :movie/title _]]}))

  (count movies)


  ;; for example, this query finds all entity-ids that have the attribute
  ;; `:person/name` with a value of `"Ridley Scott"`:

  (def ridley-scott-movie-ids
    (q '{:find [?e]
         :where [[?e :person/name "Ridley Scott"]]}))

  (count ridley-scott-movie-ids)

  ;; the simplest data pattern is a triple with some parts replaced with logic
  ;; variables. It is the job of the query engine to figure out every possible
  ;; value of each of the logic variables and return the ones that are specified
  ;; in the `:find` clause.

  ;; the symbol `_` can be used as a wildcard for the parts of the data pattern
  ;; that you wish to ignore. You can also elide trailing values in a data
  ;; pattern. Therefore, the following two queries are equivalent.

  (q '{:find [?e]
       :where [[?e :person/name _]]})

  (q '{:find [?e]
       :where [[?e :person/name]]})


  ;; data patterns

  ;; **data patterns** are vectors within the `:where` vector, such as
  ;; `[e :movie/title "Commando"]`. There can be many data patterns in a
  ;; `:where` clause:

  (q '{:find [?title]
       :where [[?e :movie/year 1987]
               [?e :movie/title ?title]]})

  ;; the important thing to note here is that the logic variable `?e` is used in
  ;; both data patterns. When a logic variable is used in multiple places, the
  ;; query engine requires it to be bound to the same value in each
  ;; place. Therefore, this query will only find movie titles for movies made in
  ;; 1987.

  ;; the order of the data patterns does not matter. XTDB ignores the
  ;; user-provided clause ordering so the query engine can optimize query
  ;; execution. Thus, the previous query could just as well have been written
  ;; this way:

  (q '{:find [?title]
       :where [[?e :movie/title ?title]
               [?e :movie/year 1987]]})

  ;; let's say we want to find out who starred in "Lethal Weapon". We will need
  ;; three data patterns for this. The first one finds the entity ID of the
  ;; movie with "Lethal Weapon" as the title. Using the same entity ID at `?m`,
  ;; we can find person IDs of the cast members. Then we can grab the actual
  ;; names using the person IDs:

  (def cast-names-from-lethal-weapon-movie-title
    (q '{:find [?name]
         :where [[?m :movie/title "Lethal Weapon"]
                 [?m :movie/cast ?p]
                 [?p :person/name ?name]]}))

  cast-names-from-lethal-weapon-movie-title

  (def alien-movie-release-year
    (q '{:find [?year]
         :where [[?m :movie/title "Alien"]
                 [?m :movie/year ?year]]}))

  (def robocop-movie-director-name
    (q '{:find [?name]
         :where [[?m :movie/title "RoboCop"]
                 [?m :movie/director ?p]
                 [?p :person/name ?name]]}))

  (def arnold-schwarzenegger-movies-director-names
    (q '{:find [?name]
         :where [[?p :person/name "Arnold Schwarzenegger"]
                 [?m :movie/cast ?p]
                 [?m :movie/director ?d]
                 [?d :person/name ?name]]}))


  ;; parameterized queries

  ;; it would be great if we could reuse a query to find movie titles for any
  ;; actor and not just for "Sylvester Stallone". This is possible with an `:in`
  ;; clause, which provides the query with input parameters, much in the same
  ;; way that function or method arguments do in your programming language.


  ;; scalars

  ;; here's that query with an input parameter for the actor:

  (def sylvester-stallone-movie-titles
    "Using a scalar input parameter."
    (q '{:find [?title]
         :in [?name]
         :where [[?p :person/name ?name]
                 [?m :movie/cast ?p]
                 [?m :movie/title ?title]]}
       "Sylvester Stallone"))

  sylvester-stallone-movie-titles

  ;; the above query is executed like `(q db query "Sylvester Stallone")`, where
  ;; `query` is the query we just saw, and `db` is a database value. You can
  ;; have any number of inputs to a query.

  ;; in the above query, the input logic variable `?name` is bound to a scalar -
  ;; a string in this case. There are four different kinds of input: scalars,
  ;; tuples, collections, and relations.

  ;; note that an implicit first argument `$` is also available, should it ever
  ;; be needed, which is the value of database `db` itself. You can use this in
  ;; conjunction with more advanced features like subqueries and custom Clojure
  ;; predicates.


  ;; tuples

  ;; a tuple input is written as e.g. `[?name ?age]` and can be used when you
  ;; want to destructure an input. Let's say you have the vector `["James
  ;; Cameron" "Arnold Schwarzenegger"]` and you want to use this as input to
  ;; find all movies where these two people collaborated:

  (def james-cameron-arnold-schwarzenegger-movie-titles
    "Using a tuple input parameter."
    (q '{:find [?title]
         :in [[?director ?actor]]
         :where [[?d :person/name ?director]
                 [?a :person/name ?actor]
                 [?m :movie/director ?d]
                 [?m :movie/cast ?a]
                 [?m :movie/title ?title]]}
       ["James Cameron" "Arnold Schwarzenegger"]))

  james-cameron-arnold-schwarzenegger-movie-titles

  ;; of course, in this case, you could just as well use two distinct inputs
  ;; instead.


  ;; collections

  ;; you can use collection destructuring to implement a kind of *logical OR* in
  ;; your query. Say you want to find all movies directed by either James
  ;; Cameron **or** Ridley Scott:

  (def directed-by-james-cameron-or-ridley-scott-movie-titles
    "Using a collection input parameter."
    (q '{:find [?title]
         :in [[?director ...]]
         :where [[?p :person/name ?director]
                 [?m :movie/director ?p]
                 [?m :movie/title ?title]]}
       ["James Cameron" "Ridley Scott"]))

  directed-by-james-cameron-or-ridley-scott-movie-titles

  ;; here, the `?director` logic variable is initially bound to both "James
  ;; Cameron" and "Ridley Scott". Note that the ellipsis following `?director`
  ;; is a literal, not elided code.


  ;; relations

  ;; relations - sets of tuples - are the most interesting and powerful of input
  ;; types, since you can join external relations with the triples in your
  ;; database.

  ;; lAs a simple example, let's consider a relation with tuples `[?movie-title
  ;; ?box-office-earnings]`:

  ;; [...
  ;;  ["Die Hard" 140700000]
  ;;  ["Alien" 104931801]
  ;;  ["Lethal Weapon" 120207127]
  ;;  ["Commando" 57491000]
  ;;  ...]

  ;; let's use this data and the data in our database to find box office
  ;; earnings for a particular director:

  (def ridley-scott-movies-box-office-earnings
    "Using a relation input parameter."
    (q '{:find [?title ?box-office]
         :in [?director [[?title ?box-office]]]
         :where [[?p :person/name ?director]
                 [?m :movie/director ?p]
                 [?m :movie/title ?title]]}
       "Ridley Scott"
       [["Die Hard" 140700000]
        ["Alien" 104931801]
        ["Lethal Weapon" 120207127]
        ["Commando" 57491000]]))

  ridley-scott-movies-box-office-earnings

  ;; note that the `?box-office` logic variable does not appear in any of the
  ;; data patterns in the `:where` clause.

  (def arnold-schwarzenegger-movie-titles-and-ratings
    (q '{:find [?title ?rating]
         :in [?name [[?title ?rating]]]
         :where [[?m :movie/title ?title]
                 [?m :movie/cast ?p]
                 [?p :person/name ?name]]}
       "Arnold Schwarzenegger"
       [["The Terminator" 4.5]
        ["Terminator 2: Judgment Day" 5]
        ["Alien" 3]
        ["Aliens" 4]]))


  ;; predicates

  ;; so far, we have only been dealing with **data patterns**: `[?m :movie/year
  ;; ?year]`. We have not yet seen a proper way of handling questions
  ;; like "*Find all movies released before 1984*". This is where **predicate
  ;; clauses** come into play.

  ;; let's start with the query for the question above:

  (q '{:find [?title]
     :where [[?m :movie/title ?title]
             [?m :movie/year ?year]
             [(< ?year 1984)]]})

  ;; the last clause, `[(< ?year 1984)]`, is a predicate clause. The predicate
  ;; clause filters the result set to only include results for which the
  ;; predicate returns a "truthy" (non-nil, non-false) value. You can use any
  ;; Clojure function as a predicate function:

  (q '{:find [?name]
       :where [[?p :person/name ?name]
               [(clojure.string/starts-with? ?name "M")]]})

  ;; Clojure functions must be fully namespace-qualified, so if you have defined
  ;; your own predicate `awesome?` you must write it as `(my.namespace/awesome?
  ;; movie)`. All `clojure.core/*` functions may be used as predicates without
  ;; namespace qualification: `<, >, <=, >=, =, not=` and so on. XTDB provides
  ;; a "Predicate AllowList" feature to restrict the exact set of predicates
  ;; available to queries.

  (def before-given-year-movie-titles
    (q '{:find [?title]
         :in [?before-year]
         :where [[?m :movie/year ?year]
                 [?m :movie/title ?title]
                 [(< ?year ?before-year)]]}
       1984))

  before-given-year-movie-titles

  (def person-names-starting-with-m
    (q '{:find [?name]
         :where [[?p :person/name ?name]
                 [(clojure.string/starts-with? ?name "M")]]}))

  person-names-starting-with-m

  (def actors-older-than-danny-glover-names
    (q '{:find [?name]
         :where [[?g :person/name "Danny Glover"]
                 [?g :person/born ?glover-born]
                 [?p :person/born ?person-born]
                 [?p :person/name ?name]
                 [_ :movie/cast ?p]
                 [(< ?person-born ?glover-born)]]}))

  actors-older-than-danny-glover-names


  ;; transformation functions

  ;; **transformation functions** are pure (side-effect free) functions which
  ;; can be used in queries as "function expression" predicates to transform
  ;; values and bind their results to new logic variables. Say, for example,
  ;; there exists an attribute `:person/born` with type
  ;; `:db.type/instant`. Given the birthday, it's easy to calculate the (very
  ;; approximate) age of a person:

  (defn age
    "Calculate person's approximate age."
    [^java.util.Date birthday ^java.util.Date today]
    (quot (- (.getTime today)
             (.getTime birthday))
          (* 1000 60 60 24 365)))

  ;; with this function, we can now calculate the age of a person **inside the
  ;; query itself**:

  (def tina-turner-age
    (q '{:find [?age]
         :in [?name ?today]
         :where [[?p :person/name ?name]
                 [?p :person/born ?born]
                 [(hello-xtdb.datalog/age ?born ?today) ?age]]}
       "Tina Turner"
       (java.util.Date.)))

  tina-turner-age

  ;; a transformation function clause has the shape `[(<fn> <arg1> <arg2> ...)
  ;; <result-binding>]` where `<result-binding>` can be the same binding forms
  ;; as input parameters:
  ;; - Scalar: `?age`
  ;; - Tuple: `[?foo ?bar ?baz]`
  ;; - Collection: `[?name ...]`
  ;; - Relation: `[[?title ?rating]]`

  ;; one thing to be aware of is that transformation functions can't be
  ;; nested. For example, you can't write:

  ;; [(f (g ?x)) ?a]

  ;; instead, you must bind intermediate results in temporary logic variables:

  ;; [(g ?x) ?t]
  ;; [(f ?t) ?a]


  ;; aggregates

  ;; aggregate functions such as `sum`, `max` etc. are readily available in
  ;; xtdb's Datalog implementation. They are written in the `:find` clause in
  ;; your query:

  (def movies-count
    (q '{:find [(count ?m)]
         :where [[?m :movie/title]]}))

  movies-count

  ;; an aggregate function collects values from multiple triples and returns
  ;; - a single value: `min`, `max`, `sum`, `avg`, etc.
  ;; - a collection of values: `(min n d)` `(max n d)` `(sample n e)` etc. where
  ;;   `n` is an integer specifying the size of the collection.

  (def oldest-person-born-date
    (q '{:find [(min ?date)]
         :where [[_ :person/born ?date]]}))

  oldest-person-born-date

  (def actors-average-rating
    (q '{:find [?name (avg ?rating)]
         :in [[?name ...] [[?title ?rating]]]
         :where [[?p :person/name ?name]
                 [?m :movie/cast ?p]
                 [?m :movie/title ?title]]}
       ["Sylvester Stallone" "Arnold Schwarzenegger" "Mel Gibson"]
       [["Die Hard" 8.3]
        ["Alien" 8.5]
        ["Lethal Weapon" 7.6]
        ["Commando" 6.5]
        ["Mad Max Beyond Thunderdome" 6.1]
        ["Mad Max 2" 7.6]
        ["Rambo: First Blood Part II" 6.2]
        ["Braveheart" 8.4]
        ["Terminator 2: Judgment Day" 8.6]
        ["Predator 2" 6.1]
        ["First Blood" 7.6]
        ["Aliens" 8.5]
        ["Terminator 3: Rise of the Machines" 6.4]
        ["Rambo III" 5.4]
        ["Mad Max" 7.0]
        ["The Terminator" 8.1]
        ["Lethal Weapon 2" 7.1]
        ["Predator" 7.8]
        ["Lethal Weapon 3" 6.6]
        ["RoboCop" 7.5]]))

  actors-average-rating


  ;; rules

  ;; many times over the course of this tutorial, we have had to write the
  ;; following three lines of repetitive query code:

  ;; [?p :person/name ?name]
  ;; [?m :movie/cast ?p]
  ;; [?m :movie/title ?title]

  ;; **rules** are the means of abstraction in Datalog. You can abstract away
  ;; reusable parts of your queries into rules, give them meaningful names and
  ;; forget about the implementation details, just like you can with functions
  ;; in your favorite programming language. Let's create a rule for the three
  ;; lines above:

  '[(actor-movie ?name ?title)
    [?p :person/name ?name]
    [?m :movie/cast ?p]
    [?m :movie/title ?title]]

  ;; the first vector is called the *head* of the rule where the first symbol is
  ;; the name of the rule. The rest of the rule is called the *body*.

  ;; you can think of a rule as a kind of function, but remember that this is
  ;; logic programming, so we can use the same rule to:
  ;; - find movie titles given an actor name, and
  ;; - find actor names given a movie title.

  ;; put another way, we can use both `?name` and `?title` in `(actor-movie
  ;; ?name ?title)` for input as well as for output. If we provide values for
  ;; neither, we'll get all the possible combinations in the database. If we
  ;; provide values for one or both, it will constrain the result returned by
  ;; the query as you'd expect.

  ;; to use the above rule, you simply write the head of the rule instead of the
  ;; data patterns. Any variable with values already bound will be input, the
  ;; rest will be output.

  ;; the query to find cast members of some movie, for which we previously had
  ;; to write:

  (q '{:find [?name]
       :where [[?p :person/name ?name]
               [?m :movie/cast ?p]
               [?m :movie/title "The Terminator"]]})

  ;; now becomes:

  (q '{:find  [?name]
       :where [(actor-movie ?name "The Terminator")]
       :rules [[(actor-movie ?name ?title)
                [?p :person/name ?name]
                [?m :movie/cast ?p]
                [?m :movie/title ?title]]]})

  ;; you can write any number of rules, collect them in a vector, and pass them
  ;; to the query engine using the `:rules` key, as above.

  ;; you can use data patterns, predicates, transformation functions and calls
  ;; to other rules in the body of a rule.

  ;; rules can also be used as another tool to write *logical OR* queries, as
  ;; the same rule name can be used several times:

  (def assoc-with-rules '[[(associated-with ?person ?movie)
                           [?movie :movie/cast ?person]]
                          [(associated-with ?person ?movie)
                           [?movie :movie/director ?person]]])

  ;; subsequent rule definitions will only be used if the ones preceding it
  ;; aren't satisfied.

  ;; using this rule, we can find both directors and cast members very easily:

  (def actors-and-directors-in-predator-movie
    "Several rules with the same name represents logical OR."
    (q {:find  '[?name]
        :where '[[?m :movie/title "Predator"]
                 (associated-with ?p ?m)
                 [?p :person/name ?name]]
        :rules assoc-with-rules}))

  actors-and-directors-in-predator-movie

  ;; given the fact that rules can contain calls to other rules, what would
  ;; happen if a rule called itself? Interesting things, it turns out, but let's
  ;; find out in the exercises.

  (def arnold-schwarzenegger-friends
    (q '{:find [?friend]
         :in [?name]
         :where [[?p1 :person/name ?name]
                 (friends ?p1 ?p2)
                 [?p2 :person/name ?friend]]
         :rules [[(friends ?p1 ?p2)
                  [?m :movie/cast ?p1]
                  [?m :movie/cast ?p2]
                  [(not= ?p1 ?p2)]]
                 [(friends ?p1 ?p2)
                  [?m :movie/cast ?p1]
                  [?m :movie/director ?p2]
                  [(not= ?p1 ?p2)]]
                 [(friends ?p1 ?p2)
                  [?m :movie/director ?p1]
                  [?m :movie/cast ?p2]
                  [(not= ?p1 ?p2)]]
                 [(friends ?p1 ?p2)
                  [?m :movie/director ?p1]
                  [?m :movie/director ?p2]
                  [(not= ?p1 ?p2)]]]}
       "Arnold Schwarzenegger"))

  arnold-schwarzenegger-friends

)
