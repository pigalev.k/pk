(ns hello-xtdb.api
  (:require
   [xtdb.api :as xt])
  (:import (java.util Date)
           (java.time Duration)))


;; Clojure Client API

;; https://docs.xtdb.com/clients/clojure/


;; lifecycle API
;; node

(comment

  ;; `start-node`: returns an XTDB node.

  (def xtdb-node (xt/start-node {}))


  ;; `new-api-client`: returns a new remote API client.

  ;; start a local HTTP server first, see `api.org`
  (def xtdb-client (xt/new-api-client "http://localhost:3000"))
  (xt/submit-tx xtdb-client [[::xt/put {:xt/id "hi2him" :user/name "jey"}]])
  (xt/pull (xt/db xtdb-client) [:user/name] "hi2him")


  ;; `new-submit-client`: returns a submit client for transacting into XTDB
  ;; without running a full local node with index.

  (def xtdb-submit-client (xt/new-submit-client {}))

)



;; node API (`PXtdb`, `PXtdbSubmitClient`, `DBProvider`)
;; represents a db node.

(comment

  ;; `db`: returns a DB snapshot at the given time. The snapshot is not
  ;; thread-safe.

  (xt/db xtdb-node)


  ;; `open-db`: opens a DB snapshot at the given time.

  (xt/open-db xtdb-node)


  ;; `status`: returns the status of this node as a map.

  (xt/status xtdb-node)


  ;; `submit-tx`: writes transactions to the log.

  (def before-all-transactions (Date.))
  (def tx-report
    (xt/submit-tx xtdb-node [[::xt/put {:xt/id "hi2u" :user/name "finli"}]]))


  ;; `tx-committed?`: checks if a submitted tx was successfully committed.

  (xt/tx-committed? xtdb-node tx-report)


  ;; `await-tx`: blocks until the node has indexed a transaction that is at or
  ;; past the supplied tx.

  (xt/await-tx xtdb-node 1)


  ;; `await-tx-time`: blocks until the node has indexed a transaction that is
  ;; past the supplied txTime.

  (xt/await-tx-time xtdb-node before-all-transactions)
  (xt/await-tx-time xtdb-node (Date.) (Duration/ofSeconds 1))


  ;; `sync`: blocks until the node has caught up indexing to the latest tx
  ;; available at the time this method is called.

  (xt/sync xtdb-node)
  (xt/sync xtdb-node (Duration/ofSeconds 1))


  ;; `listen`: attaches a listener to XTDB's event bus.

  (with-open [listener (xt/listen xtdb-node
                                {::xt/event-type ::xt/indexed-tx}
                                println)]
    (println "Listener registered: " (hash listener))
    (xt/submit-tx xtdb-node [[::xt/put {:xt/id "hi2all" :user/name "pimli"}]])
    (xt/sync xtdb-node)
    (println "Unregistering listener: " (hash listener)))


  ;; `open-tx-log`: reads the transaction log.

  (xt/open-tx-log xtdb-node 1 false)


  ;; `latest-completed-tx`: returns the latest transaction to have been indexed
  ;; by this node.

  (xt/latest-completed-tx xtdb-node)


  ;; `latest-submitted-tx`: returns the latest transaction to have been
  ;; submitted to this cluster.

  (xt/latest-submitted-tx xtdb-node)


  ;; `attribute-stats`: returns frequencies of indexed attributes.

  (xt/attribute-stats xtdb-node)


  ;; `active-queries`: returns a list of currently running queries.

  (xt/active-queries xtdb-node)


  ;; `recent-queries`: returns a list of recently completed/failed queries.

  (xt/recent-queries xtdb-node)


  ;; `slowest-queries`: returns a list of slowest completed/failed queries ran
  ;; on the node.

  (xt/slowest-queries xtdb-node)

)


;; db API (`PXtdbDatasource`)
;; represents the database as of a specific valid and transaction time.

;; https://docs.xtdb.com/language-reference/datalog-queries

(comment

  ;; setup

  (def db (xt/db xtdb-node))


  ;; `entity`: queries a document map for an entity.

  (xt/entity db "hi2all")


  ;; `entity-tx`: returns the transaction details for an entity.

  (xt/entity-tx db "hi2all")


  ;; `q`: query a db eagerly.

  (xt/q db '{:find [?e] :where [[?e :user/name]]})


  ;; `open-q`: query a db lazily (return a cursor).

  (with-open [result (xt/open-q db '{:find [?e] :where [[?e :user/name]]})]
    (doseq [row (iterator-seq result)]
      (println row)))


  ;; `pull`: returns the requested data for the given entity ID, based on the
  ;; projection spec.

  (xt/pull db [:user/name] "hi2u")


  ;; `pull-many`: returns the requested data for the given entity IDs, based on
  ;; the projection spec.

  (xt/pull-many db '[(:user/name {:as :nomen})] ["hi2u" "hi2all"])


  ;; `entity-history`: eagerly retrieves entity history for the given entity.


  (xt/entity-history db "hi2all" :asc)


  ;; `open-entity-history`: lazily retrieves entity history for the given entity.

  (with-open [result (xt/open-entity-history db "hi2all" :asc)]
    (doseq [row (iterator-seq result)]
      (println row)))


  ;; `db-basis`: returns the basis of this db snapshot - a map containing
  ;; `::xt/valid-time` and `::xt/tx`.

  (xt/db-basis db)


  ;; `valid-time`: returns the valid time of the db.

  (xt/valid-time db)


  ;; `transaction-time`: returns the time of the latest transaction applied to
  ;; this db value.

  (xt/transaction-time db)


  ;; `with-tx`: returns a new db value with the tx-ops speculatively applied.

  (def with-lamla-db
    (xt/with-tx db [[::xt/put {:xt/id "hi2me" :user/name "lamla"}]]))

  (xt/pull with-lamla-db [:user/name] "hi2me")

)
