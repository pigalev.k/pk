(ns hello-xtdb.core
  (:require [clojure.java.io :as io]
            [xtdb.api :as xt]))


;; XTDB Quickstart

;; https://docs.xtdb.com/guides/quickstart/


;; intro

(defn start-xtdb!
  "Start the XTDB instance."
  []
  (letfn [(kv-store [dir]
            {:kv-store {:xtdb/module 'xtdb.rocksdb/->kv-store
                        :db-dir      (io/file dir)
                        :sync?       true}})]
    (xt/start-node
     {:xtdb/tx-log         (kv-store "data/dev/tx-log")
      :xtdb/document-store (kv-store "data/dev/doc-store")
      :xtdb/index-store    (kv-store "data/dev/index-store")})))

(defn stop-xtdb!
  "Stop the XTDB instance."
  [xtdb-node]
  (.close xtdb-node))

(comment

  ;; start XTDB instance

  (def xtdb-node (start-xtdb!))

  ;; get latest db

  (def db-latest (xt/db xtdb-node))

  ;; transact

  (xt/submit-tx xtdb-node [[::xt/put
                            {:xt/id     "hi2u"
                             :user/name "finli"}]])

  ;; query

  (xt/q (xt/db xtdb-node) '{:find  [e]
                            :where [[e :user/name "finli"]]})


  ;; stop XTDB instance

  (stop-xtdb! xtdb-node)

)
