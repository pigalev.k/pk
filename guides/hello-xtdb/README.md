# hello-xtdb

Exploring XTDB --- general-purpose bitemporal document database for SQL, Datalog
& graph queries.

- https://xtdb.com/
- https://github.com/xtdb/xtdb

## Getting Started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.

## XTDB REST API

See `api.org` for the setup and usage examples.
