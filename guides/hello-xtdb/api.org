* XTDB REST API                                                        :verb:
  # Start local HTTP server:
  # clojure -M -m xtdb.main

  # https://docs.xtdb.com/clients/http/

  # All of the REST endpoints return `application/edn`, `application/json` and
  # `application/transit+json`. Individual endpoints may return additional
  # types - see their docs below, or see the OpenAPI Reference.

  # All endpoints with query-parameters accept them in both a kebab-case and
  # camel cased format, (ie: if `valid-time` is taken, `validTime` will also be
  # taken)

  template http://localhost:3000/_xtdb
  Content-Type: application/json; charset=utf-8
  Accept: application/json

** Transact
   template /submit-tx

*** Put entity
    post

    {
      "tx-ops": [
        [
          "put",
          {
            "xt/id": "president",
            "name": "Joe",
            "last-name": "Biden"
          }
        ]
      ]
    }

*** Put entity for past valid time
    post

    {
      "tx-ops": [
        [
          "put",
          {
            "xt/id": "president",
            "name": "Donald",
            "last-name": "Trump"
          },
          "2017-01-20"
        ]
      ]
    }

** Query
*** GET
**** Make a Datalog query
     # takes a datalog query and returns its results. Results are also available
     # in `text/csv` and `text/tsv` formats (can force negotiation of these by
     # using the `/query.csv` and `/query.tsv` endpoints respectively).

     # get all document ids
     get /query?query-edn={%3Afind+[e]+%3Awhere+[[e+%3Axt/id+_]]}

**** Make a Datalog query (EDN result)
     get /query?query-edn={%3Afind+[e]+%3Awhere+[[e+%3Axt/id+_]]}
     Accept: application/edn

**** Make a Datalog query (CSV result)
     get /query.csv?query-edn={%3Afind+[e+v]+%3Awhere+[[e+%3Axt/id+v]]}

*** POST
    # currently the only supported Content-Type for posting queries is
    # `application/edn`.
    template
    Content-Type: application/edn

**** Make a Datalog query
     # takes a datalog query and returns its results. Results are also available
     # in text/csv and text/tsv formats (can force negotiation of these by using
     # the `/query.csv` and `/query.tsv` endpoints respectively).
     post /query

     #+begin_src clojure
       {:query {:find [?e ?name] :where [[?e :name ?name]]}}
     #+end_src

**** Make a Datalog query (pull)
     post /query

     #+begin_src clojure
       {:query {:find [(pull ?e [*])] :where [[?e :name]]}}
     #+end_src

**** Make a Datalog query (EDN result)
     post /query
     Accept: application/edn

     #+begin_src clojure
       {:query {:find [?e ?name] :where [[?e :name ?name]]}}
     #+end_src

**** Make a Datalog query (CSV result)
     post /query.tsv

     #+begin_src clojure
       {:query {:find [?e ?name] :where [[?e :name ?name]]}}
     #+end_src

** Entity
*** Get entity
    # returns the document map for a particular entity.
    get /entity?eid=president

*** Get entity with past valid time
    # filter results by valid-time
    get /entity?eid=president&valid-time=2020-10-10

*** Get entire history for entity
    # returns the entire history of a particular entity.
    get /entity?eid=president&history=true&sort-order=desc&with-docs=true

*** Get entity transaction data
    # returns the transaction details for an entity - returns a map containing
    # the tx-id and tx-time.
    get /entity-tx?eid=president
** Info and Utils
*** Node status
    # returns the current status information of the node.
    get /status

*** Attribute stats
    # returns frequencies of indexed attributes.
    get /attribute-stats
*** Sync
    # wait until the Kafka consumer’s lag is back to 0 (i.e. when it no longer
    # has pending transactions to write). Returns the transaction time of the
    # most recent transaction.
    get /sync?timeout=500

*** Await the transaction by tx-id
    # waits until the node has indexed a transaction that is at or past the
    # supplied `tx-id`. Returns the most recent tx indexed by the node.
    get /await-tx?tx-id=1

*** Await the transaction by tx-time
    # blocks until the node has indexed a transaction that is past the supplied
    # `tx-time`. The returned date is the latest index time when this node has
    # caught up as of this call.
    get /await-tx-time?tx-time=2020-10-16T14:29:35Z

*** Get transaction log
    # returns a list of all transactions, from oldest to newest transaction
    # time - optionally including documents.
    get /tx-log

*** Check if transaction was committed
    # checks if a submitted tx was successfully committed, returning a map with
    # tx-committed and either true or false (or a `NodeOutOfSyncException`
    # exception response if the node has not yet indexed the transaction).
    get /tx-committed?tx-id=1

*** Get the latest completed transaction
    # returns the latest transaction to have been indexed by this node.
    get /latest-completed-tx

*** Get the latest transaction submitted to this cluster
    # returns  the latest transaction to have been submitted to this cluster.
    get /latest-submitted-tx
*** Get the list of active queries
    # returns a list of currently running queries.
    get /active-queries

*** Get the list of recent queries
    # returns a list of recently completed/failed queries.
    get /recent-queries

*** Get the list slowest queries
    # returns a list of slowest completed/failed queries ran on the node.
    get /slowest-queries
