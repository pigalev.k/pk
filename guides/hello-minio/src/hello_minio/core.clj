(ns hello-minio.core
  (:require
   [clojure.java.io :as io]
   [clojure.string :as s]
   [minio-clj.core :as m]))


;; read variables from the `.env` file

(def dotenv-xform
  "Transformations for the sequence of lines read from the file containing
  environment variables. Result is a sequence of vectors (name-value pairs)
  ready to be placed into a map."
  (comp
   ;; remove comments from the lines
   (map #(s/replace % #"#.*" ""))
   ;; trim the lines
   (map s/trim)
   ;; extract variable names and values from the lines
   (map #(next (re-find #"(.*)=(.*)" %)))
   ;; remove nils (empty extraction results)
   (keep identity)
   ;; wrap name-value pairs in vectors
   (map vec)))

(defn dotenv
  "Read environment variables from the file."
  [filename]
  (let [dotenv (line-seq (io/reader filename))]
    (into {} dotenv-xform dotenv)))

(def env (dotenv ".env"))

(def minio-url (env "MINIO_URL"))
(def minio-access-key (env "MINIO_ACCESS_KEY"))
(def minio-secret-key (env "MINIO_SECRET_ACCESS_KEY"))

(comment

  ;; returns demo connection to public minio server https://play.minio.io:9000
  ;; (def conn (m/connect))

  ;; connection to local MinIO instance
  (def conn (m/connect minio-url minio-access-key minio-secret-key))

  ;; create a bucket; returns bucket name
  ;; do nothing if the bucket already exists
  (m/make-bucket conn "my.bucket")

  ;; list buckets
  (m/list-buckets conn)

  ;; upload a file; returns map of {:keys [bucket name]}
  (def upload-result (m/put-object conn "my.bucket" "deps.edn"))

  ;; get a presigned temporary download url for this object with 7day expiration
  (let [{:keys [bucket name]} upload-result]
    (m/get-download-url conn bucket name))

  ;; get a presigned and named upload url for direct upload from the client
  (let [{:keys [bucket name]} upload-result]
    (m/get-upload-url conn bucket name))

  ;; list objects in the bucket
  (def objects (m/list-objects conn "my.bucket"))
  (count objects)

  ;; get the object metadata
  (let [{:keys [bucket name]} upload-result]
    (m/get-object-meta conn bucket name))

  ;; get the object; should return Clojure `IBufferedReader`, but returns
  ;; `okio.RealBufferedSource$1` instead
  ;; could be read once
  (def deps (let [{:keys [bucket name]} upload-result]
              (m/get-object conn bucket name)))

  ;; that can be used with `spit`/`io/copy` and other Clojure functions that take
  ;; readers
  ;; not working (spit "deps-copy.edn" deps)
  (def deps-lines (line-seq (io/reader deps)))

  ;; download the object to local file
  (let [{:keys [bucket name]} upload-result]
    (m/download-object conn bucket name "deps-copy-1.edn"))

  ;; set bucket policy; see MinIO/AWS docs on policies
  ;; (m/set-bucket-policy conn {})




  ;; remove the object
  (let [{:keys [bucket name]} upload-result]
    (m/remove-object! conn bucket name))

  ;; remove the bucket (only existing and empty buckets can be removed)
  (m/remove-bucket! conn "my.bucket")

)
