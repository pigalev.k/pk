# hello-<topic>

Exploring Datomic --- a transactional database with a flexible data model,
elastic scaling, and rich queries.

- https://www.datomic.com/

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
