(ns hello-datomic-free.functional-database-talk
  (:require [datomic.api :refer [q db] :as d]
            [clojure.pprint :only [pprint]]))

(defn tempid [] (d/tempid :db.part/user))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create the db
(def uri "datomic:mem://talk")
(d/create-database uri)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; get a connection
(def conn (d/connect uri))


(def schema {:db/id                 (d/tempid :db.part/db),
             :db/ident              :email,
	         :db/valueType          :db.type/string,
	         :db/cardinality        :db.cardinality/one,
	         :db/unique             :db.unique/identity,
	         :db.install/_attribute :db.part/db})

(def schema-ret @(d/transact conn [schema]))
(keys schema-ret)

(def newdb (:db-after schema-ret))
(= newdb (:db-before schema-ret))
(= newdb (d/db conn))


(def query '[:find ?e ?email
             :where [?e :email ?email]])

(d/q query newdb)

;; add a user and email
(def fred-ret @(d/transact conn [{:db/id (tempid)
	                              :email "fred@email.com"}]))

(keys fred-ret)

;; get fred's id
(:tempids fred-ret)

(def fred-id (-> fred-ret :tempids first val))

(def an-id #(-> % :tempids first val))


(def fred-db (:db-after fred-ret))

(d/q query fred-db)

;; add ethel
(def ethel-ret
  @(d/transact conn
               [{:db/id (tempid)
                 :email "ethel@email.com"}]))

(def ethel-db (:db-after ethel-ret))

(d/q query ethel-db)

(d/q query (d/db conn))


;; speculative changes - fred changes name to freddy
(def freddy-tx [{:db/id (tempid)
                 :email "freddy@email.com"}])

freddy-tx

(def freddy-db (-> (d/db conn)        ;; get the current value of db
                   (d/with freddy-tx) ;; with dbval tx -> dbval
                   :db-after))

(d/q query freddy-db)


(d/q query (d/db conn))  ;; no harm done

;; better, use fred's id, not new id
(def freddy-tx [{:db/id fred-id
                 :email "freddy@email.com"}])

(def freddy-db (-> (d/db conn)
                   (d/with freddy-tx)
                   :db-after))

(d/q query freddy-db)


;; now transact for real
(def freddy-db (-> conn
                   (d/transact freddy-tx)
                   deref
                   :db-after))

(d/q query freddy-db)


;; an information system retains history
(def latest-db (d/db conn))
(d/q query latest-db)
(d/q query (d/history latest-db))


;; enhance query to grab txes
(def tquery '[:find ?e ?email ?tx
              :where [?e :email ?email ?tx]])

(d/q tquery (d/history latest-db))


;; get assertion/retraction status too
(def full-query '[:find ?e ?email ?tx ?added
                  :where [?e :email ?email ?tx ?added]])

(d/q full-query (d/history latest-db))


;; stable, conveyable basis
(def fred-t (d/basis-t fred-db))
fred-t
(d/t->tx fred-t)

;; even the latest db can give us any prior point in time
(d/q query (d/as-of latest-db fred-t))

(d/q query (d/since latest-db fred-t))


;; query 2 dbs
(def query2 '[:find ?e ?email
              :in $d1 $d2    ;; in allows named args
	          :where
	          [$d1 ?e :email ?email]
	          [$d2 ?e :email ?email]])

;; who had the same email then and now?
(d/q query2 ethel-db latest-db)


;; data is as good as db
(d/q query2
     '[[lucy :email "lucy@email.com"]]
     '[[lucy :email "lucy@email.com"]
       [ricky :email "ricky@email.com"]])
