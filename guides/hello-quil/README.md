# hello-quil

Exploring Quil --- a Clojure(Script) library for creating interactive drawings
and animations.

- https://github.com/quil/quil
- http://www.quil.info/

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
