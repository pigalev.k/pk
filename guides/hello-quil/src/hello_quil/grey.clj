(ns hello-quil.grey
  (:require
   [quil.core :as q]))

(defn setup []
  ;; set framerate to 1 FPS
  (q/frame-rate 1)
  ;; set the background colour to a nice shade of grey
  (q/background 200))

(defn draw []
  ;; set the stroke colour to a random grey
  (q/stroke (q/random 255))
  ;; set the stroke thickness randomly
  (q/stroke-weight (q/random 10))
  ;; set the fill colour to a random grey
  (q/fill (q/random 255))
  ;; set the diameter to a value between 0 and 100
  (let [diam (q/random 100)
        ;; set the x coord randomly within the sketch
        x    (q/random (q/width))
        ;; set the y coord randomly within the sketch
        y    (q/random (q/height))]
    ;; draw a circle at x y with the correct diameter
    (q/ellipse x y diam diam)))

(comment

  (q/defsketch example                  ;; define a new sketch named example
    :title "Oh so many grey circles"    ;; set the title of the sketch
    :settings #(q/smooth 2)             ;; turn on anti-aliasing
    :setup setup                        ;; specify the setup fn
    :draw draw                          ;; specify the draw fn
    :size [323 200])                    ;; you struggle to beat the golden ratio

)
