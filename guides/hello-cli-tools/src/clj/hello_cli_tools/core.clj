(ns hello-cli-tools.core
  (:require
   [clojure.edn :as edn]
   [clojure.tools.cli :as cli])
  (:import (java.util Date))
  (:gen-class))


;; domain functions

(defn hello
  "Make a greeting."
  [name]
  (str "Hello, " (or name "world") "!"))

(defn print-hello
  "Print a greeting."
  [{:keys [name] :as opts}]
  (println (hello name)))


(defn now
  "Return current date and time."
  []
  (str "Now: " (Date.)))

(defn dot
  "Compute a dot product of two vectors."
  [xs ys]
  (reduce + (map * xs ys)))


;; application entry point

(def cli-options
  [["-h" "--help"]])

(defn print-help
  "Print a brief help."
  [options-summary]
  (println (str "Dot product of two numeric vectors.\n"
                "Usage: <program> [options] <vector> <vector>\n"
                "Options:\n"
                options-summary)))

(defn -main
  "Application entry point."
  [& args]
  (let [parsed-args    (cli/parse-opts args cli-options)
        vector-strings (get-in parsed-args [:arguments])
        help?          (or (not= 2 (count vector-strings))
                           (get-in parsed-args [:options :help]))
        [xs ys]        (map edn/read-string vector-strings)]
    (cond
      help? (print-help (get-in parsed-args [:summary]))
      :else (printf "Dot product of '%s' and '%s': %s\n" xs ys (dot xs ys)))))


;; experiments and tests

(comment

  (def args ["-h" "[1 2 3]" "[4 5 6]"])
  (def args1 ["[1 2 3]" "[4 5 6]"])
  (cli/parse-opts args cli-options)

  (-main)
  (apply -main args)
  (apply -main args1)

  (print-help (:summary (cli/parse-opts args cli-options)))

  (hello nil)
  (hello "user")
  (print-hello {})
  (print-hello {:name "Joe"})

  (now)
)
