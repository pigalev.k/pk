# hello-cli-tools

Explore Clojure CLI tools.

Includes `clojure.tools.cli` basic usage.

## Getting Started

See comments for `:aliases` in `deps.edn`.
