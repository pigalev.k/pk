(ns hello-observability.profiling.tufte
  (:require
   [aleph.http :as http]
   [cheshire.core :as json]
   [clojure.core.match :refer [match]]
   [com.brunobonacci.mulog :as mu]
   [integrant.core :as ig]
   [portal.api :as pa]
   [reitit.ring :as ring]
   [ring.middleware.params :as params]
   [taoensso.tufte :as t]))


;; Clojure bites - Profiling with Tufte
;; https://fpsd.codes/blog/clojure-bites-profiling/

;; TODO: Embedded high-precision Clojure profiler
;; https://github.com/clojure-goes-fast/clj-async-profiler


(defn fizz-buzz-match
  [n]
  (match [(mod n 3) (mod n 5)]
         [0 0] "FizzBuzz"
         [0 _] "Fizz"
         [_ 0] "Buzz"
         :else n))

(defn fizz-buzz-cond
  [n]
  (let [m3 (mod n 3)
        m5 (mod n 5)]
    (cond
      (and m3 m5) "FizzBuzz"
      m3          "Fizz"
      m5          "Buzz"
      :else       n)))


;; printing profile data to stdout

(comment

  ;; send `profile` stats to `println`
  (t/add-basic-println-handler! {})

  (t/profile {}
    (dotimes [_ 50]
      (t/p :fizz-buzz-match (fizz-buzz-match 10000))
      (t/p :fizz-buzz-cond (fizz-buzz-cond 10000))))

)


;; collecting the profile data into a data structure

(comment

  (def res (t/profiled  {}
             (dotimes [_ 50]
               (t/p :fizz-buzz-match (fizz-buzz-match 10000))
               (t/p :fizz-buzz-cond (fizz-buzz-cond 10000)))))

  ;; inspect the results of evaluation and the profile data

  (first res)
  @(second res)


  ;; using Portal

  (def p (pa/open))
  (add-tap #'pa/submit)
  (tap> @(second res))
  (remove-tap #'pa/submit)
  (pa/close)

)


;; accumulating the profile data

(comment

  (def accumulated-stats (t/add-accumulating-handler! {}))

  (t/profile {:id :testing-1}
    (dotimes [_ 50]
      (t/p :fizz-buzz-match (fizz-buzz-match 10000))
      (t/p :fizz-buzz-cond (fizz-buzz-cond 10000))))

  (t/profile {:id :testing-2}
    (dotimes [_ 50]
      (t/p :fizz-buzz-match (fizz-buzz-match 20000))
      (t/p :fizz-buzz-cond (fizz-buzz-cond 20000))))

  ;; accumulator is drained on deref
  (doseq [[p-id pstats] @accumulated-stats]
    (tap> [p-id @pstats]))

)


;; sending example data to an OpenSearch instance (see profiling.yml for how
;; to start one)

(comment

  (def mulog-opensearch-publisher-config
    {:type      :elasticsearch
     :url       "https://localhost:9200/"
     :http-opts {:insecure?  true
                 :basic-auth ["admin" "admin"]}})

  (mu/start-publisher! mulog-opensearch-publisher-config)

  (mu/log ::hello :to "New World!")
  (mu/trace ::availability
    []
    (fizz-buzz-match 10000))

  ;; open the OpenSearch dashboard (http://localhost:5601) with the default
  ;; credentials, create `mulog*` index and browse the data

)


;; profiling the example webapp and sending the profile data to an OpenSearch
;; instance

;; web server setup with profiling middleware

(defn request->endpoint-name
  [request]
  (-> request :reitit.core/match :data (get (:request-method request)) :name))

(defn profiler-middleware
  "Middleware that wraps handlers with a call to `t/profile` using the
  endpoint name as the id of the profile."
  [wrapped]
  (fn [request]
    (t/profile {:id :ring-handler}
      (t/p (or (request->endpoint-name request) :unnamed-handler)
           (wrapped request)))))

(defn fizz-buzz-match-handler
  "Ring handler that takes the number from the request path-params and
  returns a JSON with the result of calling `fizz-buzz-match`."
  [request]
  (try
    (let [number (-> request
                     :path-params
                     :number
                     Integer/parseInt)]
      (mu/log :fizz-buzz-handler :argument number)
      {:status 200
       :body (json/generate-string {:result (for [n (range 1 (inc number))]
                                              (fizz-buzz-match n))})
       :headers {"Content-Type" "application/json"}})
    (catch NumberFormatException e
      (mu/log :fizz-buzz-handler :error (str e))
      {:status 400
       :body (json/generate-string {:error (str "Invalid numeric parameter " e)})
       :headers {"Content-Type" "application/json"}})))

(defn create-app
  "Return a ring handler that will route /events to the SSE handler
   and that will servr  static content form project's resource/public directory"
  []
  (ring/ring-handler
   (ring/router
    [["/fizz-buzz/:number" {:get {:handler fizz-buzz-match-handler
                                  :name ::fizz-buzz}}]]
    {:data {:middleware [profiler-middleware
                         params/wrap-params]}})))

;; integrant components

(defmethod ig/init-key :profiler/accumulator [_ config]
  (t/add-accumulating-handler! {}))

(defmethod ig/init-key :mulog/publisher [_ config]
  (mu/start-publisher! config))

(defmethod ig/init-key :profiler/drain [_ {:keys [delay accumulator]}]
  (future
    (while true
      (Thread/sleep delay)
      ;; drain pstats if any
      (when-let [stats @accumulator]
        (doseq [[p-id pstats] stats]
          (doseq [[handler handler-stats] (:stats @pstats)]
            (let [{:keys [loc min max mad p95 p99]} handler-stats]
              (mu/log :accumulator-drain :perf-id p-id :handler handler)
              (mu/log p-id
                :handler handler :loc loc :min min :max max
                :mad mad :p95 p95 :p99 p99))))))
    (mu/log :accumulator-drain-finished)))

(defmethod ig/halt-key! :profiler/drain [_ pstat-future]
  (future-cancel pstat-future))

(defmethod ig/init-key :http/server [_ config]
  (http/start-server (create-app) config))

(defmethod ig/halt-key! :http/server [_ server]
  (.close server))

;; and the configuration for all components of the system being basically
;; all "raw" data; it can be read from an external source like an edn file
(def config {:profiler/accumulator {}
             :profiler/drain       {:delay       5000
                                    :accumulator (ig/ref :profiler/accumulator)}
             :mulog/publisher      {:type      :elasticsearch
                                    :url       "https://localhost:9200/"
                                    :http-opts {:insecure?  true
                                                :basic-auth ["admin" "admin"]}}
             :http/server {:port  8080
                           :join? false}})

;; the system

(defonce system (atom nil))

(defn start!
  "Starts the application."
  []
  (reset! system (ig/init config)))

(defn stop!
  "Stops the application."
  []
  (ig/halt! @system)
  (reset! system nil))

(comment

  (start!)

  ;; poke some endpoints and see the results in the OpenSearch dashboard (see
  ;; apis.org)

  (stop!)

)
