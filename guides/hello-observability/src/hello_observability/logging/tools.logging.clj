(ns hello-observability.logging.tools.logging
  (:require
   [clojure.tools.logging :as log]))


;; basics

;; - README https://github.com/clojure/tools.logging
;; - API documentation https://clojure.github.io/tools.logging

(comment

  ;; logging occurs with the `log` macro, or the level-specific convenience
  ;; macros (e.g., `debug`, `debugf`). Only when the specified logging level is
  ;; enabled will the message arguments be evaluated and the underlying logging
  ;; implementation be invoked. By default, that invocation will occur via an
  ;; agent when inside a running STM transaction.

  (log/log :info "will loggins")

  (log/debug "that is easier")
  (log/warn "sure")
  (log/error (Exception. "Bummer!") "wasted")
  (log/error (ex-info "Bummer!" {:foo "bar"}) "wasted")
  (log/fatal (ex-info "Full stop" {}) "wasted")


  ;; namespacing of log entries

  ;; unless otherwise specified, the current namespace (as identified by `*ns*`)
  ;; will be used as the "logger name" when interacting with logging
  ;; implementations. Most logging implementations allow for varying
  ;; configuration by logger name.

  ;; note: you should configure your logging implementation to display the
  ;; logger name that was passed to it. If instead the logging implementation
  ;; performs stack-inspection you'll see some ugly and unhelpful text in your
  ;; logs.


  ;; redirecting output to logs

  ;; you can redirect all Java writes of `System.out` and `System.err` to the
  ;; log system by calling `log/log-capture!`. To bind `*out*` and `*err*` to
  ;; the log system invoke `log/with-logs`. In both cases a logger name must be
  ;; provided in lieu of using *ns*.

  ;; need more prodding to work, probably

  (log/log-capture! "an-ns")
  (.println System/out "Howdya?")
  (log/log-uncapture!)

  (log/with-logs "an-ns"
    (println "Howdya?"))


  ;; configuration

  ;; note: logging configuration (e.g., setting of logging levels, formatting)
  ;; is specific to the underlying logging implementation, and is out of scope
  ;; for this library.

  ;; selecting a logging implementation

  ;; to control which logging implementation is used, set the
  ;; `clojure.tools.logging.factory` system property to the fully-qualified name
  ;; of a no-arg function that returns an instance of
  ;; `clojure.tools.logging.impl/LoggerFactory`. There are a number of factory
  ;; functions provided in the `clojure.tools.logging.impl` namespace.

  ;; java
  ;; -Dclojure.tools.logging.factory=clojure.tools.logging.impl/slf4j-factory ...

  ;; if the system property is unset, an implementation will be automatically
  ;; chosen based on whichever of the following implementations is successfully
  ;; loaded first:

  ;; - SLF4J
  ;; - Apache Commons Logging
  ;; - Log4J 2
  ;; - Log4J
  ;; - java.util.logging

  ;; the above approach is problematic given that applications often
  ;; inadvertently pull in multiple logging implementations as transitive
  ;; dependencies. As such, it is strongly advised that you set the system
  ;; property.


  ;; log4j2

  ;; a simple Log4j2 configuration:

  ;; status = warn
  ;; monitorInterval = 5
  ;; appender.console.type = Console
  ;; appender.console.name = STDOUT
  ;; appender.console.layout.type = PatternLayout
  ;; appender.console.layout.pattern = %date %level %logger %message%n%throwable
  ;; rootLogger.level = info
  ;; rootLogger.appenderRef.stdout.ref = STDOUT

  ;; note: the above pattern explicitly uses %throwable so that
  ;; clojure.lang.ExceptionInfo exceptions will be printed with their data
  ;; maps. If either %xThrowable (the default) or %rThrowable is used, the data
  ;; maps will not be printed.


  ;; FAQ

  ;; - Q: when logging an `ex-info` exception, why isn't the data map printed?
  ;; - A: this is likely because the logging implementation is printing the
  ;;   contents of `Throwable.getMessage()`, which returns just the message
  ;;   arg of `ex-info`.

  ;; logging implementations that print the contents of `toString()` or use
  ;; `Throwable.printStackTrace(...)` will end up printing the data map.

)
