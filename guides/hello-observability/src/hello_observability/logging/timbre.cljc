(ns hello-observability.logging.timbre
  (:require
   [taoensso.timbre :as timbre]
   [taoensso.timbre.appenders.core :as appenders]
   #?@(:clj  [[taoensso.timbre.appenders.carmine :as carm]
              [taoensso.timbre.appenders.postal :as post]])))


;; basics

;;l https://github.com/ptaoussanis/timbre


(comment

  ;; by default, Timbre gives you basic `println` and `js/console` output for
  ;; all logging calls of at least `:debug` log level:

  (timbre/info "This will print")

  ;; spies

  (timbre/spy :info (* 5 4 3 2 1))

  ;; lexical env

  (defn my-mult [x y]
    (timbre/info "Lexical env:" (timbre/get-env)) (* x y))

  (my-mult 4 7)

  (timbre/trace "This won't print due to insufficient log level")


  ;; first-argument exceptions will also generate a stack trace:

  (timbre/info #?(:clj (Exception. "Oh no")
                  :cljs (js/Error. "Oh no")) "arg1" "arg2")


  ;; set the minimum logging level

  ;; a Timbre logging call will be disabled (noop) when the call's
  ;; level (e.g. (timbre/info ...) is less than the active minimum
  ;; level (e.g. `:warn`).

  ;; levels: `:trace` < `:debug` < `:info` < `:warn` < `:error` < `:fatal`
  ;; < `:report`

  ;; call (set-min-level! <min-level>) to set the minimum level for all
  ;; namespaces.

  ;; call (set-ns-min-level! <min-level>) to set the minimum level for the
  ;; current namespace only.

  (timbre/set-min-level! :warn)
  (timbre/info "nope")

  (timbre/set-ns-min-level! :debug)
  (timbre/debug "yeah")


  ;; architecture

  ;; Timbre's inherently a simple design, no magic. It's just Clojure data and
  ;; functions.

  ;; here's the flow for an (timbre/info ...) logging call:

  ;; - dynamic `*config*` is used as the current active config.
  ;; - is `:info` < the active minimum level? If so, end here and noop.
  ;; - is the current namespace filtered? If so, end here and noop.
  ;; - prepare a log data map of interesting info incl. all logging arguments.
  ;; - pass the data map through any middleware fns: (fn [data]) -> ?data.
  ;;   These may transform the data. If returned data is nil, end here and noop.
  ;; - pass the data map to all appender fns: (fn [data]) -> ?effects. These may
  ;;   print output, save the data to a DB, trigger admin alerts, etc.


  ;; configuration

  ;; Timbre's behaviour is controlled by the single dynamic `*config*` map,
  ;; fully documented here.

  timbre/*config*

  ;; its default value can be easily overridden by:

  ;; - an edn file on your resource path.
  ;; - a symbol defined by an an environment variable or JVM property.
  ;; - a variety of provided utils.
  ;; - standard Clojure utils (`binding`, `alter-var-root!`/`set!`).

  ;; sophisticated behaviour is achieved through normal fn composition, and the
  ;; power of arbitrary Clojure fns: e.g. write to your database, send a message
  ;; over the network, check some other state (e.g. environment config) before
  ;; making a choice, etc.

  ;; advanced minimum levels and namespace filtering

  ;; The `*config*` `:min-level` and `:ns-filter` values both support
  ;; sophisticated pattern matching, e.g.:

  ;; - `:min-level`: [[#{\"taoensso.*\"} :error] ... [#{\"*\"} :debug]].
  ;; - `:ns-filter`: {:allow #{"*"} :deny #{"taoensso.*"}}.

  ;; note that both `:min-level` and `:ns-filter` may also be easily overridden
  ;; on a per-appender basis.

  ;; compile-time elision

  ;; by setting the relevant JVM properties or environment variables, Timbre can
  ;; actually entirely exclude the code for disabled logging calls at
  ;; compile-time, e.g.:

  ;; #!/bin/bash

  ;; # elide all lower-level logging calls:
  ;; export TAOENSSO_TIMBRE_MIN_LEVEL_EDN=':warn'

  ;; # elide all other ns logging calls:
  ;; export TAOENSSO_TIMBRE_NS_PATTERN_EDN='{:allow #{"my-app.*"} :deny \
  ;; #{"my-app.foo" "my-app.bar.*"}}'

  ;; lein cljsbuild once # compile js with appropriate logging calls excluded
  ;; lein uberjar        # compile jar

  ;; disable stacktrace colors

  ;; ansi colors are enabled by default for Clojure stacktraces. To turn these
  ;; off (e.g. for log files or emails), you can add the following entry to your
  ;; top-level config or individual appender map/s:

  ;; `:output-opts` {`:stacktrace-fonts` {}}

  ;; and/or you can set the:

  ;; - `taoensso.timbre.default-stacktrace-fonts.edn` JVM property, or
  ;; - `TAOENSSO_TIMBRE_DEFAULT_STACKTRACE_FONTS_EDN` environment variable.

)



(comment

  #?(:clj

     ;; included appenders (mostly Clojure only)


     ;; basic file appender

     (timbre/merge-config!
      {:appenders {:spit (appenders/spit-appender
                          {:fname "resources/timbre.example.log"})}})

     (timbre/info "logged to file")

     ;; disable

     (timbre/merge-config! {:appenders {:spit {:enabled? false}}})

     ;; remove entirely

     (timbre/merge-config! {:appenders {:spit nil}})


     ;; Carmine (Redis) appender

     (timbre/merge-config! {:appenders {:carmine (carm/carmine-appender)}})

     ;; start Redis before use: `podman-compose up -d`

     (timbre/info "logged to Redis")
     (timbre/info "logged to Redis once more")
     (timbre/info {:just "data"})

     (timbre/warn (Exception. "Bummer!") "exception logged to Redis")
     (timbre/error (ex-info "Bummer!" {:foo 1 :bar "quux"})
                   "ex-info logged to Redis")

     ;; seems like there is no way to retrieve for all levels at once

     (count (carm/query-entries nil :debug))
     (count (carm/query-entries nil :info))
     (count (carm/query-entries nil :warn))
     (count (carm/query-entries nil :error))

     ;; this gives us a high-performance Redis appender:

     ;; - all raw logging args are preserved in serialized form (even errors).
     ;; - configurable number of entries to keep per log level.
     ;; - only the most recent instance of each unique entry is kept.
     ;; - resulting log is just a Clojure value: a vector of log entries (maps).

     (timbre/merge-config! {:appenders {:carmine nil}})


     ;; Postal (email) appender

     ;; [com.draines/postal <latest-version>] ; Add to project.clj deps
     ;; (:require [taoensso.timbre.appenders (postal :as postal-appender)]) ; Add
     ;; to ns

     (timbre/merge-config!
      {:appenders
       {:postal
        (post/postal-appender
         ^{:host "mail.isp.net" :user "jsmith" :pass "sekrat!!1"}
         {:from "me@draines.com" :to "foo@example.com"})}})

     ;; I don't think so

     (timbre/info "logged to email")


     ;; community appenders

     ;; a number of community appenders are included with Timbre.
     ;; https://github.com/ptaoussanis/timbre/tree/master/src/taoensso/timbre/appenders/community

   )
)
