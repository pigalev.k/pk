(ns hello-observability.logging.cambium
  (:require
   [cambium.codec :as codec]
   [cambium.codec.util :as util]
   [cambium.core :as log]
   [cambium.logback.core.strategy :as strategy]
   [cambium.logback.json.flat-layout :as flat]
   [cambium.mdc :as mlog]
   [clojure.edn :as edn])
  (:import (java.util Map)
           (org.slf4j MDC)))


;; Quickstart

;; https://cambium-clojure.github.io/

(comment

  ;; requires different Logback configurations for text and data logs, not sure
  ;; how to handle both in single project


  ;; text logs

  ;; without Logback config, data argument will not be printed

  (log/info "Application started")
  (log/info {:args ["-a" 1234] :argc 2} "Arguments received")


  ;; JSON logs

  ;; configure backend with the codec

  (flat/set-decoder! codec/destringify-val)


  ;; namespace-based loggers

  ;; a namespace-based logger uses the namespace where the log event is
  ;; originated as the logger name. Like clojure.tools.logging/<log-level>,
  ;; Cambium defines namespace loggers for various levels: trace, debug, info,
  ;; warn, error, fatal.

  ;; simple message logging

  (log/info "Application started")

  ;; context and message

  (log/info {:args ["-a" 1234] :argc 2} "Arguments received")
  (log/info {:latency-ms 331 :module "registration"} "App registered")
  (log/debug {:module "order-processing"} "sequence-id verified")

  ;; context, exception, message

  (log/error {:module "user-feedback"}
             (Exception. "oops") "Email notification failed")


  ;; custom loggers

  ;; you can define custom loggers (with predefined logger name) that you can
  ;; use from any namespace as follows:

  (log/deflogger metrics "METRICS")
  (log/deflogger txn-log "TXN-LOG" :info :fatal)

  ;; simple message logging

  (txn-log "Order processed")

  ;; context and message

  (metrics {:latency-ms 331 :module "registration"} "app.register.success")

  ;; context, exception, message

  (txn-log {:module "order-processing"}
           (Exception. "oops") "Stock unavailable")


  ;; context propagation

  ;; value based context can be propagated as follows:

  ;; propagate specified context in current thread

  (log/with-logging-context {:user-id "X1234"}
    (log/info {:job-id 89} "User was assigned a new job"))

  ;; wrap an existing fn with specified context

  (defn task []
    (log/info "Job is done!"))

  (def wrapped-task
    (log/wrap-logging-context {:user-id "X1234"} task))

  (task)
  (wrapped-task)


  ;; MDC propagation

  ;; unlike value-based propagation MDC propagation happens wholesale, i.e. the
  ;; entire current MDC map is replaced with a new map. Also, no conversion is
  ;; applied to MDC; they are required to have string keys and values.

  ;; propagate specified context in current thread

  (mlog/with-raw-mdc {"userid" "X1234"}
    (log/info {:job-id 89} "User was assigned a new job"))

  ;; wrap an existing fn with specified context

  (defn task []
    (log/info "Job is done!"))

  (def wrapped-mdc-task (mlog/wrap-raw-mdc task))
  (wrapped-mdc-task)

  (def wrapped-mdc-task (mlog/wrap-raw-mdc {"userid" "X1234"} task))
  (wrapped-mdc-task)

  ;; the most common use of MDC propagation is to pass the logging context to
  ;; child threads in a concurrent scenario.


  ;; redacting context attributes

  ;; context transformation support is no-op by default. You can setup
  ;; context-filtering for various use cases.

  (alter-var-root #'cambium.core/transform-context
                  (fn [_]
                    (fn [context]
                      (-> context
                          ;; redact sensitive attributes
                          (dissoc :password :token)
                          (assoc :email "<hidden>")
                          ;; also from nested context
                          (util/dissoc-in [:config :db :jdbc-url])
                          (assoc :app-version "1.16.202")))))

  (log/info {:user "joe" :email "joe@example.com"
             :password "secret" :token "12345af"}
            "user authenticated")

  ;; context filtering is applied every time you set context. You should weigh
  ;; the performance overhead carefully for this operation.


  ;; caller metadata

  ;; by default all log events include the MDC/context attributes `:ns`, `:line`
  ;; and `:column`. This metadata is obtained at compile time, hence the
  ;; overhead is negligible. However, you may override this default behavior by
  ;; setting the system property `cambium.caller.meta.in.context`. For example,
  ;; the following command would disable caller metadata in all Cambium log
  ;; events:

  ;; java -Dcambium.caller.meta.in.context=false -jar foo-bar.jar

  ;; see also: `cambium.core/caller-meta-in-context?`


  ;; codec

  ;; a Cambium codec governs how the log attributes are encoded and decoded
  ;; before they are effectively sent to a log appender. A codec constitutes the
  ;; following:

  ;; var                     type                description
  ;; `codec/nested-nav?`     boolean             should context read/write be
  ;;                                             nesting-aware?
  ;; `codec/stringify-key`   (fn [k]) -> String  encodes log attribute key as
  ;;                                             string
  ;; `codec/stringify-val`   (fn [v]) -> String  encodes log attribute value as
  ;;                                             string
  ;; `codec/destringify-val` (fn [s]) -> value   decodes log attribute from
  ;;                                             string form

  ;; note: You need only one codec implementation in a project.

  ;; Cambium comes with two codec modules, `cambium.codec-simple` and
  ;; `cambium.codec-cheshire`. While codec-simple is a very simple codec that
  ;; converts everything to string, codec-cheshire is a nesting-aware codec that
  ;; preserves types as long as they are valid JSON.


  ;; nested context

  ;; sometimes context values may be nested and need manipulation. Cambium
  ;; requires nesting-aware codec for dealing with nested context. Almost all
  ;; nesting-aware backends need to be configured to use the codec before
  ;; logging any events:

  (flat/set-decoder! codec/destringify-val)

  ;; see nesting-navigation example below:

  (log/with-logging-context {:order {:client "XYZ Corp"
                                     :item-count 10}}
    (log/with-logging-context {[:order :id] "F-123456"}
      ;; here the context will be {"order" {"client" "XYZ Corp" "item-count"
      ;; 10 "id" "F-123456"}}
      (log/info "Order processed successfully")))

  ;; the logging API processes nested MDC correctly when the codec is
  ;; nesting-capable
  (log/info {:order {:event-id "foo"}} "Foo happened")


  ;; Logback backend

  ;; overriding log level at runtime

  ;; the `cambium.logback.core` and `cambium.logback.json` modules provide
  ;; support for overriding log levels at runtime using a strategy-based
  ;; `TurboFilter`.

  ;; one-time setup

  ;; add the following Logback configuration (or equivalent) to `logback.xml`:

  ;; <turboFilter class="cambium.logback.core.StrategyTurboFilter">
  ;;   <name>mdcStrategy</name>
  ;; </turboFilter>
  ;; <turboFilter class="cambium.logback.core.StrategyTurboFilter">
  ;;   <name>multiStrategy</name>
  ;; </turboFilter>


  ;; configure log level overrides

  ;; configure strategy:

  ;; set new log level to be determined by MDC attribute `forcelevel`

  (strategy/set-mdc-strategy! "mdcStrategy" "forcelevel")

  ;; set levels for logger names for next 15 seconds, root logger being set to
  ;; ERROR

  (strategy/set-multi-strategy! "multiStrategy" "error"
                                (strategy/multi-millis-validator 15000))
  (strategy/set-log-level! "multiStrategy" ["com.foo" "com.bar"] "debug")

  (log/error {:anomaly {:event-id "foo"}} "Foo happened")


  ;; generate log events

  (log/debug
   "If this code is in com.foo.* or com.bar.* ns this event will be logged.")

  ;; force new level to be DEBUG

  (MDC/put "forcelevel" "debug")
  (log/debug
   "This event will be logged overriding any existing INFO/WARN/ERROR level")


  ;; remove log level overrides

  ;; you can remove log level overrides at any time:

  (strategy/remove-strategy! "mdcStrategy")
  (strategy/remove-strategy! "multiStrategy")


  ;; configuring `cambium.logback.json.FlatJsonLayout`

  ;; the `cambium.logback.json.FlatJsonLayout` class is designed to accommodate
  ;; customizations to preserve types/nesting of MDC attributes and wholesale
  ;; MDC transformation.

  ;; setting MDC value decoder

  ;; setting a value decoder is useful when you encode MDC values (for
  ;; preserving data types/nesting etc) and want them to be decoded before they
  ;; are sent to the appender.

  ;; assume EDN-encoded value, so parse using EDN reader

  (flat/set-decoder! edn/read-string)

  ;; seems not to work, or maybe it should be used differently?

  (log/info "[:a :b :c]")
  (log/info {:id 1 :tags "[:a :b :c]"} "oops")


  ;; setting MDC map transformer

  ;; in a large number of cases this may not be necessary. However, applying
  ;; arbitrary transformation to the MDC map is useful at times to backfill
  ;; custom attributes, or to normalize certain attributes.

  ;; insert an extra attribute to the MDC

  (flat/set-transformer! (fn [^Map m] (.put m "extra-attr" "some value") m))

  (log/info {:id 1 :tags "[:a :b :c]"} "oops")

)
