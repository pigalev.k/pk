(ns hello-observability.monitoring.riemann
  (:require
   [riemann.client :as r]))


;; basics

;; https://github.com/riemann/riemann-clojure-client

(comment

  ;; create client

  (def c (r/tcp-client {:host "127.0.0.1"}))

  ;; send an event, get promise

  (r/send-event c {:service "foo" :state "ok" :metric (rand-int 100)})

  ;; send an event, get result

  (-> c (r/send-event {:service "foo" :state "ok" :metric (rand-int 100)})
      (deref 5000 ::timeout))

  @(r/send-event c {:service "bar" :state "oops" :metric 119})



  ;; query the server for events

  (count @(r/query c "state = \"ok\""))
  @(r/query c "service = \"bar\"")

)


;; The Art of Monitoring: Introducing Riemann

;; https://thenewstack.io/introducing-riemann/

(comment

  ;; Riemann is a monitoring tool that aggregates events (from hosts and
  ;; applications) and feed them into a stream processing language to be
  ;; manipulated, summarized, or actioned.

  ;; Riemann can also track the state of events and react to certain sequences
  ;; or combinations of events; it provides notifications, the ability to send
  ;; events to other services and into storage.

  ;; it is fast and highly configurable. Throughput depends on what you do with
  ;; each event, but stock Riemann on commodity x86 hardware can handle
  ;; millions of events per second at sub-millisecond latencies.


  ;; events, streams, and the index

  ;; Riemann is an event processing engine. There are three concepts we need to
  ;; understand if we’re going to make use of Riemann: events, streams, and the
  ;; index.


  ;; events

  ;; the event is the base construct of Riemann. Events flow into Riemann and
  ;; can be processed, counted, collected, manipulated, or exported to other
  ;; systems. A Riemann event is a struct that Riemann treats as an immutable
  ;; map.

  (def event {:host        "riemann-a"
              :service     "riemann test event"
              :state       "ok"
              :description nil
              :metric      (* 100 (rand))
              :tags        ["riemann" "test"]
              :time        355740372471/250,
              :ttl         20})

  (r/send-event c event)

  @(r/query c "service = \"riemann test event\"")

  ;; each event generally contains the following fields:

  ;; - `host`: a hostname, e.g. "riemann-a"
  ;; - `service`: the service, e.g. "riemann test event".
  ;; - `state`: a string describing state, e.g. "ok", "warning", "critical".
  ;; - `time`: the time of the event in Unix epoch seconds.
  ;; - `description`: freeform description of the event.
  ;; - `tags`: freeform list of tags.
  ;; - `metric`: a number associated with this event, e.g. the number of
  ;;   reqs/sec.
  ;; - `ttl`: a floating-point time in seconds, for which this event is valid.


  ;; streams

  ;; each arriving event is added to one or more streams. You define streams in
  ;; the (streams ...) section of your Riemann configuration. Streams are
  ;; functions you can pass events to for aggregation, modification, or
  ;; escalation. Streams can also have child streams that they can pass events
  ;; to. This allows for filtering or partitioning of the event stream, such as
  ;; by only selecting events from specific hosts or services.

  (streams
   (childstream
    (childstream)))

  ;; you can have as many streams as you like and Riemann provides a powerful
  ;; stream processing language that allows you to select the events relevant to
  ;; a specific stream. For example, you could select events from a specific
  ;; host or service that meets some other criteria.

  ;; streams are designed for events to flow through them and for limited or no
  ;; state to be retained. For many purposes, however, we do need to retain some
  ;; state. To manage this state Riemann has the index.


  ;; the index

  ;; the index is a table of the current state of all services being tracked by
  ;; Riemann. You tell Riemann to specifically index events that you wish to
  ;; track. Riemann creates a new service for each indexed event by mapping its
  ;; `:host` and `:service` fields. The index then retains the most recent event
  ;; for that service. You can think about the index as Riemann’s worldview and
  ;; source of truth for state. You can query the index from streams or even
  ;; from external services.

  ;; we saw in our event definition above that each event can contain a TTL or
  ;; Time-to-Live field. This field measures the amount of time for which an
  ;; event is valid.

  ;; events in the index longer than their TTL are expired and deleted. For each
  ;; expiration, a new event is created for the indexed service with its
  ;; `:state` field set to expired. The new event is then injected back into the
  ;; stream.

  (def event {:host        "www"
              :service     "apache connections"
              :state       nil
              :description nil
              :metric      100.0
              :tags        ["www"]
              :time        466741572492
              :ttl         20})

  ;; if we index this event, then Riemann will create a service by mapping "www"
  ;; and "apache connections". If events keep coming into Riemann, then the
  ;; index will track the latest event from this service. If the events stop
  ;; flowing then sometime after 20 seconds have passed, the event will be
  ;; expired in the index.

  ;; a new event will be generated for this service with a `:state`
  ;; of "expired", like so:

  {:host        "www"
   :service     "apache connections"
   :state       "expired"
   :description nil
   :metric      100.0
   :time        466741573456
   :ttl         20}

  ;; this event will then be injected back into streams where we can make use of
  ;; it. This behavior is going to be pretty useful to us as we use Riemann for
  ;; monitoring our applications and services. Instead of polling or checking
  ;; for failed services, we'll monitor for services whose events have expired.

)
