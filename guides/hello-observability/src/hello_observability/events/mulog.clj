(ns hello-observability.events.mulog
  (:require
   [clj-http.client :as http]
   [com.brunobonacci.mulog :as mu]
   [com.brunobonacci.mulog.core :as mc]))


;; basics

;; https://github.com/BrunoBonacci/mulog


(comment

  ;; 1. start an event publisher. You will not be able to see any events until
  ;; you add a publisher which will take your events and send them to a
  ;; distributed logger or your local console (if you are developing).

  ;; you can add as many key-value pairs as you deem useful to express the event
  ;; in your system.


  (def stop-console-edn-publisher (mu/start-publisher! {:type :console}))

  (def stop-console-json-publisher
    (mu/start-publisher!
     {:type    :console-json
      :pretty? false}))

  (def stop-file-json-publisher
    (mu/start-publisher! {:type     :file-json
                          :filename "resources/mulog.example.log.json"}))

  (def stop-file-edn-publisher
    (mu/start-publisher! {:type     :simple-file
                          :filename "resources/mulog.example.log.edn"}))

  (stop-console-edn-publisher)
  (stop-console-json-publisher)
  (stop-file-json-publisher)
  (stop-file-edn-publisher)

  ;; seems like there is no (official) way to stop all started publishers

  @mc/publishers

  ;; but maybe this will work? yeah, it does

  (->> (mc/registered-publishers)
       (map :id)
       (map mc/stop-publisher!))


  ;; 2. instrument your code with the log you deem useful. The general structure
  ;; is `(mu/log event-name, key1 value1, key2 value2, ... keyN valueN)`

  ;; good to use namespaced keywords for the event-name

  (mu/log ::hello :to "New World!")

  (mu/log ::exception
          :name "Oops!"
          :cause "none"
          :trace (Exception. "oops"))

  (mu/log ::result
          :operation "eval"
          :facility "REPL"
          :value {:id 1 :name "joe"})

  (mu/log ::user-logged
          :user-id "1234567"
          :remote-ip "1.2.3.4"
          :auth-method :password-login)

  (mu/log ::invalid-request
          :exception (RuntimeException. "Boom!"),
          :user-id "123456789",
          :items-requested 47)

)


;; context

(comment

  ;; set global context

  (mu/set-global-context! {:app-name "mulog-demo",
                           :version "0.1.0",
                           :env "local"})

  (mu/log ::system-started :init-time 32)


  ;; set local context

  (mu/with-context {:order "abc123"}
    (mu/log ::item-processed :item-id "sku-123" :qt 2))

  ;; the local context can be nested:

  (mu/with-context {:transaction-id "tx-098765"}
    (mu/with-context {:order "abc123"}
      (mu/log ::item-processed :item-id "sku-123" :qt 2)))

  ;; local context works across function boundaries:

  (defn process-item [sku quantity]
    (mu/log ::item-processed :item-id sku :qt quantity))

  (mu/with-context {:order "abc123"}
    (process-item "sku-123" 2))

)


;; best practices

(comment

  ;; here some best practices to follow while logging events:

  ;; - use namespaced keywords or qualified strings for the event name
  ;; - log plain values, not opaque objects, objects will be turned into strings
  ;;   which diminishes their value
  ;; - do not log mutable values, since rendering is done asynchronously you
  ;;   could be logging a different state. If values are mutable capture the
  ;;   current state (deref) and log it.
  ;; - avoid logging deeply nested maps, they are hard to query.
  ;; - log timestamps with milliseconds precision.
  ;; - use global context to enrich events with application name (:app-name),
  ;;   version (:version), environment (:env), host, OS pid, and other useful
  ;;   information so that it is always possible to determine the source of the
  ;;   event.
  ;; - if you have to log an error/exception put the exception object under
  ;;   `:exception` key. For example:

  (try
    (/ 1 0)
    (catch Exception x
      (mu/log ::action-x :exception x :status :failed)))

  ;;   it will be easier to search for all the error in Elasticsearch just by
  ;;   looking the presence of the exception key (Elasticsearch query example:
  ;;   "exception:*")

)


;; mu/trace

(comment

  ;; mu/trace (pronounced: /mjuːtrace/) is a micro distributed tracing library
  ;; with the focus on tracking data with custom attributes.


  ;; mu/trace is a subsystem of mu/log and it relies heavily on it. While the
  ;; objective of mu/log is to record and publish a event which happens in a
  ;; single point in time, the objective of mu/trace is to record and publish an
  ;; event that spans over a short period of time, and potentially, spans across
  ;; multiple systems.

  ;; mu/trace can be used within a single system and it will provide accurate
  ;; data around instrumented operation of that system. mu/trace can also be used
  ;; in a distributed setup and in conjunction with other distributed tracers
  ;; such as Zipkin and participate into distributed traces.

  ;; mu/trace data points are not confined to distributed tracers, but the data
  ;; can be used and interpreted in Elasticsearch, in real-time streaming system
  ;; which use Apache Kafka etc.


  ;; assume that you have a complex operation which you want to track the rate,
  ;; the outcome, the latency and have contextual information about the call.

  ;; one example of such calls is the call to an external service or database to
  ;; retrieve the current product availability.

  ;; here an example of such call:

  (def availability-service "http://localhost:8888")

  (defn product-availability [product-id]
    (try
      (http/get availability-service {:product-id product-id})
      (catch Exception e
        {:result  :error
         :message (ex-message e)})))

  ;; we want to track how long this operation takes and if it fails, what's the
  ;; reason. With mu/trace we can instrument the request as follow:

  ;; wrap the call to the `product-availability` function with mu/trace

  (mu/trace ::availability
    []
    (product-availability "1"))

  ;; mu/trace will start a timer before calling (product-availability product-id)
  ;; and when the execution completes it will log an event using mu/log. To the
  ;; caller it will be like calling (product-availability product-id) directly
  ;; as the caller will receive the evaluation result of the body. However,
  ;; mu/log will publish the event.

  ;; there are a few things to notice here:

  ;; - firstly, it inherited the global context which we set for mu/log
  ;;   (:app-name, :version and :env)
  ;; - next, we have the same keys which are available in mu/log events, such
  ;;   as: `:mulog/event-name`, `:mulog/timestamp`, `:mulog/namespace` and
  ;;   `:mulog/trace-id`.
  ;; - in addition to the `:mulog/trace-id`, which identified this particular
  ;;   trace event, we have two more IDs. One called `:mulog/root-trace` and
  ;;   the second one called `:mulog/parent-trace`. The latter one is missing
  ;;   because this trace doesn't have a parent mu/trace block. The
  ;;   `:mulog/root-trace` is the id of the originating trace which could be
  ;;   coming from another system. The `:mulog/root-trace` is the same as the
  ;;   `:mulog/trace-id` because, in this example, this trace is the first one
  ;;   (and the only one) of the stack.
  ;; - next, we have `:mulog/duration` which is the duration of the evaluation
  ;;   of the body (the `product-availability` call) expressed in nanoseconds
  ;; - whether the call succeeded or failed, this is specified in
  ;;   `:mulog/outcome` which it can be `:ok` or `:error`. The latter will be
  ;;   set in case an exception is raised, and in this case, an additional
  ;;   `:exception` property will be added with the actual exception. In case of
  ;;   errors, the exception will be thrown back to the caller for further
  ;;   handling.

  ;; in the above example we are missing some contextual information. For
  ;; example, we know that someone is enquiring about product availability but
  ;; we don't know about which product. This information is available at the
  ;; point of call, it would be nice to be able to see this information in the
  ;; trace as well. That's easily done.

  ;; like mu/log events, we can add key/value pairs to the trace as well:

  (mu/trace ::availability
    [:product-id 1]
    (product-availability 1))

  ;; note that within square brackets we have added the info we need. But we can
  ;; go one step further. Let's assume that we had the order-id and the user-id
  ;; who is enquiring about the availability as local context then we would have
  ;; the following trace event.

  (def product-id "2345-23-545")
  (def order-id   "34896-34556")
  (def user-id    "709-6567567")

  (mu/with-context {:order order-id, :user user-id}
    (mu/trace ::availability
      [:product-id product-id]
      (product-availability product-id)))

  ;; one important difference between `with-context` and the `mu/trace` pairs is
  ;; that with-context will propagate that information to all nested calls while
  ;; the `mu/trace` pairs will be only added to that specific trace event and
  ;; not the nested ones.

  ;; if we had the following set of nested calls:

  ;; (process-order)
  ;; └── (availability)
  ;;     ├── (warehouse-availability)
  ;;     ├── (shopping-carts)
  ;;     └── (availability-estimator)

  ;; where `process-order` check the availability of each product, and to check
  ;; the availability of each product you need to verify what is available in
  ;; the warehouse as well as how many items are locked in in-flight shopping
  ;; carts and have this information provided to an estimator, we would end-up
  ;; with similarly looking nested traces.


  ;; publishers

  ;; publishers allow to send the events to external system where they can be
  ;; stored, indexed, transformed or visualised.

  ;; most of the publishers are in separated modules to reduce the risk of
  ;; dependencies clash. Please see the specific publisher documentation for the
  ;; name of the module to add in your dependencies.

  ;; modules can be started as follow:

  (def pub (mu/start-publisher! {:type :console :pretty? true}))

  (mu/log ::foo :bar "quux")

  ;; the map contains the configuration which is specific to the publisher.

  ;; it returns a function with no arguments which when called stops the
  ;; publisher and flushes the records currently present in the buffer. Finally,
  ;; if the publisher implements the java.io.Closeable it will call the close
  ;; method to release/close external resources.

  (pub)

  ;; Here the list of all available publishers:

  ;; publishers

  ;; - Simple Console Publisher
  ;; - Simple File Publisher
  ;; - Advanced Console Publisher
  ;; - Cloudwatch Logs Publisher
  ;; - Elasticsearch Publisher
  ;; - Jaeger Publisher
  ;; - Kafka Publisher
  ;; - Kinesis Publisher
  ;; - Prometheus Publisher
  ;; - Slack Publisher
  ;; - Zipkin Publisher

  ;; special publishers

  ;; - Inline Publishers
  ;; - Custom Publishers
  ;; - Multi Publisher

  ;; samplers

  ;; - JVM Metrics Sampling
  ;; - Filesystem Metrics Sampling

)
