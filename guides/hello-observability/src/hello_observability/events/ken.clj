(ns hello-observability.events.ken
  (:require
   [clojure.pprint :refer [pprint]]
   [ken.context :as kc]
   [ken.core :as k]
   [ken.tap :as kt]
   [manifold.deferred :as d]))


;; basics

;; https://github.com/amperity/ken

(comment

  ;; purpose and goals

  ;; this library provides a set of general-purpose observability tools which
  ;; can be used to instrument your code in order to better understand it.

  ;; in particular, it attempts to satisfy the following goals:

  ;; - define a general "observability event" shape decoupled from any one
  ;;   consumer.
  ;; - be able to report events from anywhere in the code, without worrying
  ;;   about wiring in a component dependency.
  ;; - provide an open extension point for collecting rich context data from an
  ;;   event's source. Don't couple to sources.
  ;; - provide an open extension point for subscribing to observability events.
  ;;   Don't couple to sinks.
  ;; - support distributed tracing by tracking trace/span identifiers and
  ;;   context.
  ;; - build up events over time with annotations.


  ;; concepts

  ;; - `event`: a description of something that happened in the code.
  ;; - `context`: a source of contextual information used to enrich the events
  ;;   being observed.
  ;; - `subscription`: a handler registered to handle some events.
  ;; - `trace`: a call graph of spans (units of work).


  ;; events

  ;; an event is a map of Clojure keys and values which represent a thing that
  ;; happened in your code. Events will typically contain a selection of keys
  ;; from `ken.event` to provide a basic foundation:

  ;; to see the observed events, explained in Subscriptions below

  (kt/subscribe! :console-print pprint)

  (def event {:ken.event/label    "the thing"
              :ken.event/time     #inst "2020-03-27T21:22:27.003Z"
              :ken.event/duration 44.362681
              :ken.event/message  "Perform some routine activity"
              :ken.event/ns       'my.foo.thing})

  (k/observe event)


  ;; here we have an event about some labeled process which started at the given
  ;; time, lasted 44.3 milliseconds, and had some associated human-friendly
  ;; metadata. Events may have many other attributes as well, including
  ;; authentication context, custom identifiers, inputs and outputs, and more.


  ;; contexts

  ;; a context collector in ken is a source of contextual information used to
  ;; enrich the events being observed. Useful data sources could include
  ;; properties about the running process such as the build number and
  ;; deployment environment, dynamic information about request-specific
  ;; properties like the authenticated user, and even local attributes that are
  ;; breadcrumbs for later debugging.

  ;; register a static context which is known at process startup:


  (let [java-info {::java-version (System/getProperty "java.version")}]
    (kc/register-static! ::java-version java-info)
    (k/observe {}))

  ;; context collection happens at the call location, so you can also pull
  ;; dynamic properties:

  (def ^:dynamic *user-info* {:user/id       1
                              :user/name     "Joe"
                              :user/email    "joe@example.com"
                              :user/password "topsecret"})

  (defn user-context
    []
    (select-keys *user-info* [:user/id :user/name :user/email]))

  (kc/register! ::user-info user-context)

  (k/observe {:ken.event/label "a thing", :my/key 123})

  ;; contexts can be removed with `kc/unregister!` and the keyword id, or reset
  ;; entirely with `kc/clear!`.

  (kc/unregister! ::java-version)
  (kc/unregister! ::user-info)
  (kc/clear!)

  ;; if you have a service-oriented architecture, a rich source of additional
  ;; data can be the broadcast context which is transmitted through the whole
  ;; request graph.


  ;; subscriptions

  ;; this library adopts the idea of the `tap` introduced in Clojure 1.10. This
  ;; is a global queue which may be sent events from anywhere in the code. Note
  ;; that ken has its own queue, it does not utilize the built-in `tap` in order
  ;; to avoid polluting local debugging usage.

  ;; in order for those events to be useful, they must be handled by functions
  ;; which are subscribed to the tap. As an example, we can pretty-print all
  ;; events to our console for inspection:

  (kt/subscribe! :console-print pprint)

  ;; subscribed functions will be called with every event sent to the tap and
  ;; should not block for significant periods of time or they may cause event
  ;; loss. Subscriptions can be removed with `kt/unsubscribe!` or reset entirely
  ;; with `kt/clear!`.

  (kt/unsubscribe! :console-print)
  (kt/clear!)

  ;; no output

  (k/observe {})


  ;; tracing

  ;; finally, ken uses a standard model for distributed tracing of
  ;; events. Events are grouped together under a top-level trace which
  ;; identifies an entire unit of work. Each event within this may be a span
  ;; which represents a subunit of work covering some duration. Spans may be
  ;; children of other spans, meaning they represent more fine-grained bits of
  ;; work in turn. Linking all the spans in a trace together forms a tree,
  ;; sometimes called a "call graph" which represents the observations collected
  ;; about the unit of work which was traced.

  ;; using the library will automatically capture and extend the tracing
  ;; identifiers where needed, which show up in the observed
  ;; events (`:ken.trace/trace-id`, `:ken.trace/span-id`).

  {:ken.event/label    "A"
   :ken.event/time     #inst "2020-03-27T21:22:27.003Z"
   :ken.event/duration 44.362681
   :ken.trace/trace-id "bplzs2gajkfcbojkspx7"
   :ken.trace/span-id  "cuoclyafu4"}

  {:ken.event/label     "B"
   :ken.event/time      #inst "2020-03-27T21:22:27.005Z",
   :ken.event/duration  41.805794
   :ken.trace/trace-id  "bplzs2gajkfcbojkspx7"
   :ken.trace/parent-id "cuoclyafu4"
   :ken.trace/span-id   "cjatpftw5j"}

  ;; above are two related spans A and B, the second nested inside the first.

)


;; usage

;; tagged literal printing of ken dates

(defmethod print-method java.time.Instant
  [^java.time.Instant d, ^java.io.Writer w]
  (.write w "#inst \"")
  (.write w (.toString d))
  (.write w "\""))

(comment

  (kt/subscribe! :console-print pprint)


  ;; direct observation

  ;; the most direct way to use the library is to call the `k/observe` macro in
  ;; your code in order to send events.

  (k/observe {:ken.event/label "a thing", ::my-key 123})

  ;; this will collect the available context and trace data, add it to the
  ;; event, then send it to the tap for publishing. By default, this returns
  ;; immediately (non-blocking), but you can specify a timeout in milliseconds
  ;; if you would like to wait for the event to be accepted. Either way, this
  ;; returns true if the event was queued and false if it was rejected.


  ;; watching Spans

  ;; the most common way to generate events is by describing spans which cover
  ;; some work happening. You can use the `k/watch` macro for this:

  (defn crunch-numbers
    [x y]
    (Thread/sleep 500)
    (* x y))

  (defn think-heavily
    [topic]
    (Thread/sleep 1000)
    :dunno)

  (k/watch "a thing happening"
           (crunch-numbers 2.17 3.14)
           (think-heavily "what am I?"))

  ;; this will instrument the body of expressions and observes an event at the
  ;; end which includes tracing and timing information. The watch form will
  ;; return the value of the final expression. This also works for asynchronous
  ;; values, so the following code will only record the event once the deferred
  ;; chain completes:

  (k/watch "another thing"
           (d/chain
            (d/future
              (crunch-numbers 8675309 12345))
            println))

  ;; for richer event data, you can specify a map --- the string versions above
  ;; automatically expand into `:ken.event/label` entries:

  (k/watch {:ken.event/label "either foo or bar"
            ::foo            123
            ::bar            'baz}
           (think-heavily "foo or bar?"))

  ;; if you nest calls to `k/watch`, the nested spans will automatically link to
  ;; the enclosing watch as the parent span.

  (defn inner
    []
    (k/watch "inner"
             (+ 1 2)))

  (k/watch "outer"
           (inner))


  (k/watch "outer"
           (k/watch "middle"
                    (k/watch "inner")))


  ;; annotations

  ;; for data you don't necessarily know until you've started doing some work,
  ;; you can annotate spans by adding additional properties to the events. When
  ;; code is executing inside a watch, you can use the `k/annotate`, `k/time`,
  ;; and `k/error` tools:

  (defn think-heavily!
    [topic]
    (Thread/sleep 1000)
    (throw (ex-info "..." {:answer :dunno})))

  (defn foo?
    [x]
    (boolean (:foo x)))

  (defn task
    [x]
    (k/watch "a thing"
             (try
               (when (foo? x)
                 (k/annotate {::foo? true}))
               (k/time ::thinking
                       (think-heavily! "what is consciousness?"))
               (catch Exception ex
                 (k/error ex)))))

  (task 1)
  (task {:foo 1})

  ;; this would produce a span event labeled "a thing" with a few potential
  ;; additional attributes --- a `::foo?` key set to true, a `:ken.event/error`
  ;; key with the caught exception, and a `::thinking` key holding the number of
  ;; milliseconds spent in the `think-heavily!` call.


  ;; local Context

  ;; in addition to the registered context collector functions, ken comes with a
  ;; built-in local context which you can bind anywhere in your code to provide
  ;; additional information. Unlike adding keys directly to `k/observe` and
  ;; `k/watch` calls, these values are propagated to all events inside the
  ;; block:

  ;; all events observed inside this block will include the `:foo` and `:bar`
  ;; keys

  (k/with-context {:foo 123
                   :bar "baz"}
    (k/watch :with-local-context-foo-bar
             (crunch-numbers 1 2)))


  ;; sampling

  ;; sampling is the act of selecting a subset of events from a large collection
  ;; of events. Not everything needs to be sampled, but if you have high
  ;; frequency events and most of them are very similar, sampling them can be a
  ;; good way to reducing your total event volume.

  ;; sampling is controlled by two tracing keys, which can be specified in the
  ;; initial `k/watch` or in a later `k/annotate` call.

  ;; in order to opt into sampling for a specific span, you can set the
  ;; `:ken.event/sample-rate` key. This is an integer value n that will cause,
  ;; on average, about 1/n of the events to be sampled. The rest will be marked
  ;; to be discarded by consumers.

  ;; when a span has been marked for sampling, it will set the second tracing
  ;; key `:ken.trace/keep?` on the resulting event. The keep key can have one of
  ;; three possible states:

  ;; - `nil` or absent: the span will be kept and forwarded along. This is the
  ;;   default behavior.
  ;; - `false`: the span will be marked to be dropped and the decision will
  ;;   propagate down to its child spans.
  ;; - `true`: the span and its children will be kept. This decision overrides
  ;;   sampling logic in child spans.

  ;; the `:ken.trace/keep?` key can also be set directly; for example, if you
  ;; encounter an error and want to ensure that a span and its (subsequent)
  ;; children are recorded, you can use annotate to set the flag to true.

  ;; for additional reading on sampling best practices, see Honeycomb's article
  ;; on the topic: https://docs.honeycomb.io/manage-data-volume/sampling/

  ;; note: events which have been "sampled away" are still reported to tap
  ;; subscribers! It is up to the individual subscribed functions to decide to
  ;; drop the events or not.

  (k/watch {:ken.event/label       "sampling"
            :ken.event/sample-rate 2}
           (* 123 456))


  ;; integrations

  ;; - `ken-honeycomb` integrates with honeycomb.io

)
