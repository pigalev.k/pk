# hello-monitoring

Exploring application observability --- collecting, visualizing and analyzing
events (logs, metrics, traces).

## Events

General observability events, that can represent logs, metrics, traces, etc.

- Mulog: a micro-logging library that logs events and data
  https://github.com/BrunoBonacci/mulog
- Ken: observability library to instrument Clojure code
  https://github.com/amperity/ken

## Logging

- Timbre: pure Clojure(Script) logging library
  https://github.com/ptaoussanis/timbre
- Cambium: structured logging (logs as data) for Clojure
  https://cambium-clojure.github.io/
- Pedestal logging facility http://pedestal.io/api/io.pedestal.log.html
- clojure.tools.logging: https://github.com/clojure/tools.logging

## Tracing

TODO

## Monitoring

- Riemann: monitors distributed systems https://riemann.io/

## Profiling

- Simple performance monitoring for Clojure/Script applications
  https://github.com/taoensso/tufte

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.

## Dependencies

Start necessary services

```
podman-compose -f base.yml [-f <service>.yml]* up -d
```

For example, start an OpenSearch instance to collect profiling data

```
podman-compose -f base.yml -f profiling.yml up -d
```
