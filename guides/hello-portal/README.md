# hello-portal

Exploring Portal --- a Clojure tool to navigate through your data.

- https://github.com/djblue/portal

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
