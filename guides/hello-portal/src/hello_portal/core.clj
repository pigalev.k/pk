(ns hello-portal.core
  (:require
   ;; for node and jvm
   [portal.api :as pa]
   ;; for web
   ;; NOTE: you might need to enable popups for the portal ui to work in the
   ;; browser.
   #_[portal.web :as pw]))


(comment

  ;; open a new inspector
  (def p (pa/open))

  ;; or with an extension installed, do:
  ;; (def p (p/open {:launcher :vs-code}))  ; jvm / node only
  ;; (def p (p/open {:launcher :intellij})) ; jvm / node only

  ;; add portal as a tap> target
  (add-tap #'pa/submit)

  ;; start tapping out values
  (tap> :hello)
  (tap> p)

  ;; clear all values
  (pa/clear)
  (reset! p 0)
  (swap! p inc)

  ;; tap out more values
  (tap> :world)
  (tap> {:id 1, :name "Ololosha"})
  (tap> [1 2 3 "frob" true {:id 2}])

  ;; use viewers
  (tap> ^{:portal.viewer/default :portal.viewer/hiccup} [:h1 "hello, world"])

  (def md-string "# hello, world")
  (tap>
   (with-meta
     [:portal.viewer/markdown md-string]
     {:portal.viewer/default :portal.viewer/hiccup}))

  ;; register a command
  (pa/register! #'identity)


  ;; bring selected value back into repl
  (prn @p)

  ;; remove portal from tap> targetset
  (remove-tap #'pa/submit)

  ;; close the inspector when done
  (pa/close)

)
