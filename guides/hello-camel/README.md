# hello-camel

Exploring Apache Camel --- an open source integration framework.

- https://github.com/apache/camel/
- https://camel.apache.org/

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
