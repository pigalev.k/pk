(ns hello-camel.core
  (:import [org.apache.camel.impl DefaultCamelContext]
           [org.apache.camel.builder RouteBuilder]))


(def c (DefaultCamelContext.))

(.start c)

(-> (.findComponents c)
    first)
;; => #object[java.util.TreeMap$Entry 0x43547eff "bean={version=2.24.1, projectName=Camel :: Core, name=bean, groupId=org.apache.camel, components=rest xslt direct-vm properties controlbus browse rest-api ref timer log dataset seda saga language bean class binding scheduler vm direct stub mock dataformat validator test file, class=org.apache.camel.component.bean.BeanComponent, projectDescription=The Core Camel Java DSL based router, artifactId=camel-core}"]

(-> (.findEips c)
    first)
;; => #object[java.util.TreeMap$Entry 0x9eb825f "aggregate={label=eip,routing, name=aggregate, description=Aggregates many messages into a single message, class=org.apache.camel.model.AggregateDefinition, title=Aggregate}"]

(-> (.getComponentNames c))
;; => []

(-> (.getDataFormats c))
;; => {}

(-> (.getEndpoints c))
;; => []

(-> (.getRoutes c))

(def route-builder
  (proxy [RouteBuilder] []
    (configure []
      (println "Route built")
      (.. this
          (from "file:inputfile")
          (to "file:outputfile")))))

(.addRoutes c route-builder)
