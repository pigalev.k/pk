(ns nrvs.hello-kit.env
  (:require
    [clojure.tools.logging :as log]
    [nrvs.hello-kit.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init       (fn []
                 (log/info "\n-=[nrvs.hello-kit starting using the development or test profile]=-"))
   :start      (fn []
                 (log/info "\n-=[nrvs.hello-kit started successfully using the development or test profile]=-"))
   :stop       (fn []
                 (log/info "\n-=[nrvs.hello-kit has shut down successfully]=-"))
   :middleware wrap-dev
   :opts       {:profile       :dev
                :persist-data? true}})
