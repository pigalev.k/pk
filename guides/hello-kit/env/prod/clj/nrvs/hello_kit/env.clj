(ns nrvs.hello-kit.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init       (fn []
                 (log/info "\n-=[nrvs.hello-kit starting]=-"))
   :start      (fn []
                 (log/info "\n-=[nrvs.hello-kit started successfully]=-"))
   :stop       (fn []
                 (log/info "\n-=[nrvs.hello-kit has shut down successfully]=-"))
   :middleware (fn [handler _] handler)
   :opts       {:profile :prod}})
