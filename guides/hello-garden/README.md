# hello-garden

Exploring Garden --- a library for generating CSS in Clojure(Script).

- https://github.com/noprompt/garden

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
