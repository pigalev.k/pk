(ns hello-garden.core
  (:require
   [hello-garden.arithmetic :as a]
   [hello-garden.colors :as c]
   [hello-garden.units :as u]
   [garden.core :refer [css]]
   [garden.stylesheet :refer [at-media]]
   [garden.units :refer [px]]))


#?(:cljs (enable-console-print!))

;; the namespace is built as a ESM module in cljs, and it is it's init-fn and
;; it's only export, too

(defn hello []
  (println "Hello garden!"))


;; basics

(comment

  ;; `css` function takes an optional map of compiler flags, any number of
  ;; rules, and returns a string of compiled CSS.
  (println (css [:body {:font-size "16px"}]
                [:p {:font-weight      "bold"
                     :font-size        u/px16
                     :width            a/a-width
                     :height           a/a-height
                     :margin           a/a-margin
                     :background-color c/dark-red
                     :color            c/red
                     :border-color     c/light-red
                     :outline-color    a/a-color}]))

  ;; vectors represent rules in CSS. The first n non-collection elements of a
  ;; vector depict the rule's selector where n > 0. When n = 0 the rule is not
  ;; rendered.
  (println (css [:h1 :h2 {:font-weight "none"}]))
  (println (css [{:font-weight "none"}]))

  ;; nested vectors target child selectors
  (println (css [:h1 [:a {:text-decoration "none"}]]))
  (println (css [:h1 :h2 [:a {:text-decoration "none"}]]))

  ;; selectors prefixed with the & character target a parent selector
  (println (css [:a
                 {:font-weight     'normal
                  :text-decoration 'none}
                 [:&:hover
                  {:font-weight     'bold
                   :text-decoration 'underline}]]))

  ;; media queries

  (println
   (css
    (at-media {:screen true} [:h1 {:font-weight "bold"}])))

  (println
   (css
    (at-media {:min-width (px 768) :max-width (px 979)}
              [:container {:width (px 960)}])))

  (println
   (css [:a {:font-weight "normal"}
         [:&:hover {:color "red"}]
         (at-media {:screen true
                    :braille false}
                   [:&:hover {:color "pink"}])]))

)


;; compiler

(comment

  ;; compressing

  (println (css {:pretty-print? true} [:body {:background "#ff0000"}]))
  (println (css {:pretty-print? false} [:body {:background "#ff0000"}]))

  ;; autoprefixing
  (println (css
            {:vendors ["webkit"]
             :auto-prefix #{:border-radius}}
            [:.foo {:border-radius (px 3)}]))

)
