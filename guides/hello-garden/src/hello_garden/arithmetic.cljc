(ns hello-garden.arithmetic
  (:refer-clojure :exclude [+ - * /])
  (:require
   [garden.arithmetic :refer [+ - * /]]
   [garden.color :refer [rgb hsl]]
   [garden.units :refer [px em vw]]))


#?(:cljs (enable-console-print!))


;; CSS spec outlines a few categories of units but only absolute (px, pt, pc,
;; in, cm, and mm), angle (deg, grad, rad, turn), time (s, ms), and
;; frequency (Hz, kHz) units may be freely converted and only between their
;; respective groups.


(def a-color (+ 20 (hsl 0 0 0) 1 (rgb 255 0 0)))
(def a-width (* 2 (+ 24 (px 8) (px 16))))
(def a-height (* 10 (/ (em 48) 2)))
(def a-margin (vw 10))
