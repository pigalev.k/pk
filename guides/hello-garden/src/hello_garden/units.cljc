(ns hello-garden.units
  (:require
   [garden.units :as u :refer [px px+ px* px- px-div pt]]))


;; units

(def px16 (px 16))
(def px16+16 (px+ 16 16))
(def px16*4 (px* 16 4))
(def px16-8 (px- 16 8))
(def px32div2 (px-div 32 2))


;; unit conversions

;; since the arithmetic functions use the primary unit functions in their
;; definitions, conversion occurs seamlessly (when possible)
(def px2*pt1 (px* 2 (pt 1)))

;; CSS spec outlines a few categories of units but only absolute (px, pt, pc,
;; in, cm, and mm), angle (deg, grad, rad, turn), time (s, ms), and
;; frequency (Hz, kHz) units may be freely converted and only between their
;; respective groups.
