(ns hello-garden.colors
  (:require
      [garden.color :as color :refer [hsl rgb]]))


#?(:cljs (enable-console-print!))


;; colors

(def red (hsl 0 100 50))

red
(def dark-red (color/darken red 25))
(def light-red (color/lighten red 25))
