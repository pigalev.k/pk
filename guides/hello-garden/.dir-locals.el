;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; configure default Cider cljs REPL
((nil . ((cider-shadow-default-options . "hello")
         (cider-default-cljs-repl . shadow)
         (cider-clojure-cli-global-options . "-A:dev:cljs"))))
