# hello-spec

Exploring clojure.spec --- a Clojure library to describe the structure of data
and functions.

- https://clojure.org/guides/spec

## Getting started

Start a clj REPL in terminal

```
clj
```

or cljs REPL

```
clj -M -m cjls.main
```

or either of them in your editor of choice.

Require namespaces, explore and evaluate the code.
