(ns hello-spec.alpha2
  (:require
   [clojure.alpha.spec :as s]
   [clojure.alpha.spec.gen :as gen]))


;; differences from `clojure.spec.alpha`

;; "Maybe Not" by Rich Hickey https://www.youtube.com/watch?v=YR5WdGrpoug


;; summary

(comment

  ;; symbolic specs

  ;; spec 1 defined a language of spec forms (like `s/and`) to define
  ;; specs. These specs were implemented as macros taking both evaluated
  ;; objects (symbols or functions) and nested spec forms. The api call `s/form`
  ;; would produce a symbolic spec from a spec object. One downside of this
  ;; approach is that the reliance on macros made it challenging to
  ;; programmatically construct spec forms without using `eval` or additional
  ;; macros.

  ;; in spec 2, we are more strictly separating the worlds of symbolic specs and
  ;; spec objects. Symbolic specs consist only of:

  ;; - spec forms (lists/seqs), with spec op in function position, composed of
  ;;   other symbolic specs
  ;; - qualified keywords (names that can be looked up in the registry)
  ;; - qualified symbols (predicate function references)
  ;; - sets of constant values (an enumeration)

  ;; the function `s/resolve-spec` takes symbolic specs and returns spec
  ;; objects (extensions of the `Spec` protocol) for runtime use. `s/form` is
  ;; the inverse operation, which takes a spec object and returns a symbolic
  ;; spec.

  (s/def :user/id uuid?)
  (s/def :user/name string?)
  (s/def ::user (s/schema {:id :user/id :name :user/name}))

  (s/resolve-spec ::user)
  (s/form (s/resolve-spec ::user))

  ;; the spec forms are themselves macros, which expand to a call to
  ;; `resolve-spec` on themselves. The only modification made is that spec form
  ;; macros accept symbols that are not fully-qualified and will
  ;; qualify ("explicate") them in the namespace context where they are
  ;; invoked (at compile-time). This is a convenience for people writing spec
  ;; forms.

  ;; the spec registry is a stateful runtime construct that provides a mapping
  ;; from spec names (either qualified keywords or qualified symbols) to spec
  ;; objects. A new function `s/register` has been added that adds a mapping
  ;; from name to spec object. `s/def` is a helpful wrapper macro for
  ;; `s/register` that understands how to interpret all kinds of symbolic
  ;; specs (not just spec forms), including symbols and sets, which will be
  ;; wrapped in the helper `s/spec` spec op.

  ;; the spec API functions (`valid?`, `conform`, `explain`, etc) now accept
  ;; only keywords (spec names), and spec objects. Spec 1 accepted things like
  ;; function objects (evaluated symbols or anonymous functions) as well ---
  ;; those now must be explicitly wrapped in `s/spec` forms.


  ;; spec additions and changes

  ;; nonflowing `s/and-` (new)

  ;; in spec 1, `s/and` will flow the conformed value through
  ;; validation/conforming/unform (in reverse), etc. In spec 2, we've added
  ;; `s/and-` for non-flowing and:

  ;; - `s/valid`: validates all preds on original value
  ;; - `s/conform`: returns `s/conform` of first pred on value (+ validates
  ;;   other preds)
  ;; - `s/unform`: returns `s/unform` of first pred on value
  ;; - `s/gen`: generates from first pred, filters only those valid on all
  ;;   subsequent preds (same as `s/and`)

  (s/def ::x (s/and- (s/cat :i1 int? :i2 int?) #(apply distinct? %)))
  (s/valid? ::x [1 2])
  (s/valid? ::x [1 1])

  (s/conform ::x [1 2])
  (s/explain ::x [1 1])

  (gen/sample (s/gen ::x))

  (s/unform ::x {:i1 1, :i2 2})

  ;; we may rename these functions in spec 2: `s/and` will be nonflowing and
  ;; `s/and->` will be flowing.


  ;; `s/cat`

  ;; `s/cat` continues to accept all sequential collections but will now
  ;; generate both sequences and vectors. You can combine `s/cat` with `s/and-`
  ;; to narrow this further:

  (s/def ::vcat (s/and- (s/cat :i int?) vector?))
  (s/valid? ::vcat [1])
  (s/valid? ::vcat '(1))

  (s/conform ::vcat [1])

  (s/explain ::vcat '(1))

  (gen/sample (s/gen ::vcat))


  ;; nested regex contexts

  ;; regex specs (`cat`, `alt`, `*`, `+`, `?`, etc) combine to describe a single
  ;; sequential collection. If the collection contains a nested collection,
  ;; something needs to be inserted to prevent that combination. In spec 1, this
  ;; was done with the `s/spec` macro. In spec 2, a new `s/nest` operation
  ;; serves this purpose and is used only for this.


  ;; creating specs programmatically

  ;; the new `s/resolve-spec` functional entry point can be used to construct
  ;; spec objects from spec forms without invoking the spec op macros or using
  ;; `eval`:

  (defn or-of
    "Make `or` spec where tags match names"
    [& spec-names]
    (let [tag-names (->> spec-names (map name) (map keyword))]
      (s/resolve-spec (cons `s/or (interleave tag-names spec-names)))))

  (s/def ::a int?)
  (s/def ::b keyword?)
  (s/conform (or-of ::a ::b) 100)

  ;; register using the functional `s/register`, not `s/def`

  (s/register ::x (or-of ::a ::b))
  (s/form ::x)


  ;; implementing custom specs

  ;; simple spec ops

  ;; it's common to need a parameterized custom spec op and these can now easily
  ;; be created with `s/defop`. Ops created with `s/defop` are defined by a
  ;; parameterized specs, have the form you'd expect, and can optionally provide
  ;; a custom generator (otherwise will use gen from the spec provided). They
  ;; conform/unform based on the spec definition.

  (s/defop bounded-string
    "Specs a string with bounded size (<= min (count string) max)"
    [min max]
    (s/and string? #(<= min (count %) max)))

  (s/def ::first-name (bounded-string 1 20))
  (s/form ::first-name)
  (s/conform ::first-name "Homer")
  (s/explain ::first-name "")
  (gen/sample (s/gen ::first-name))


  ;; full spec ops

  ;; custom spec ops can be created and installed with a two-step
  ;; process. First, create a spec op macro that explicates (fully-qualifies) a
  ;; form and invokes the functional interface:

  (defmacro my-spec
    [& opts]
    `(s/resolve-spec '~(s/explicate (ns-name *ns*) `(my-spec ~@opts))))

  ;; second, calls to `resolve-spec` get routed (via the spec name) to the
  ;; multimethod `create-spec` that actually creates the spec by reifying the
  ;; `Spec` protocol:

  ;; (defmethod create-spec 'my-ns/my-spec
  ;;   [[_ & opts]]
  ;;   (reify Spec
  ;;     ...))

  ;; because this requires implementing the full `Spec` protocol, this is a
  ;; significantly higher effort and something that should only be used for new
  ;; spec ops that can't easily be created as combinations of the core ops.


  ;; closed spec checking

  ;; spec is primarily concerned with an "open" approach to map
  ;; validation. `s/keys` (and now `s/schema`) defined a set of attributes that
  ;; can co-occur. `s/keys` (and now `s/select`) can be used to specify
  ;; requirements of maps, but not negative constraints (maps can only contain
  ;; these keys, and no others). Allowing for open maps allows specs to evolve
  ;; over time. There are more changes coming to better talk about the
  ;; requires/provide split at the function level.

  ;; however, there are a variety of situations where it is useful to do closed
  ;; spec checking --- calling an API known to be fragile in the face of
  ;; unexpected attributes, verifying user inputs according to a specification,
  ;; or even just checking for attribute typos. In these cases, you can now
  ;; enable "closed spec" checking where unspecified keys fail validation. All
  ;; of the conforming api calls (`s/valid?`, `s/conform`, `s/explain` and
  ;; similar) now optionally take an additional settings argument. The settings
  ;; are a map of setting to configuration for that setting.

  ;; in the case of closed specs, use `:settings` and a set of schema specs to
  ;; close:

  (s/def ::f string?)
  (s/def ::l string?)
  (s/def ::s (s/schema [::f ::l]))

  ;; "extra" keys are ok normally - open maps are the default

  (s/valid? ::s {::f "Bugs" ::l "Bunny" ::x 10})

  ;; but closed spec checking can be more restrictive

  (s/valid? ::s {::f "Bugs" ::l "Bunny" ::x 10} {:closed #{::s}})

  (s/explain ::s {::f "Bugs" ::l "Bunny" ::x 10} {:closed #{::s}})

)


;; `schema` and `select`

(comment

  ;; schemas

  ;; it is common to have aggregates of keys that work together to describe the
  ;; attributes of an entity (User, Company, Order, etc). In `spec.alpha`,
  ;; `s/keys` was used to define required and optional attributes. In spec 2,
  ;; the optionality of these attributes comes out of the selection and schemas
  ;; are used to aggregate attributes into groups that "travel together" to
  ;; describe one thing. Schemas do not define "required" or "optional" --- it's
  ;; up to selections to declare this in a context.

  ;; schemas have both a symbolic form, and an object form (same as
  ;; specs). Schemas are also named with fully-qualified keywords and schema
  ;; objects are managed at runtime in the registry. Schemas are also specs and
  ;; can be used as such in the API. In spec 1 terms, they are similar to an
  ;; `s/keys` with only `:opt` or `:opt-un` keys (no `:req` or `:req-un`).


  ;; literal schemas

  ;; the simplest form of a literal schema is a vector of qualified keywords,
  ;; which should refer to specs in the registry.

  [::street ::city ::state ::zip]

  ;; additionally, you may include a map of unqualified keys to the specs or spec names that should be used for them:

  [::a ::b {:c (s/spec int?)}]

  ;; typically you will either see all qualified keywords (use the vector form)
  ;; or all unqualified keywords (when spec'ing JSON, etc). For the latter case,
  ;; you can just use a map:

  {:first ::fname :last ::lname}


  ;; schema forms

  ;; the above literal forms can be used directly in a spec (like `s/select`),
  ;; but must be wrapped in an `s/schema` form to serve as a symbolic
  ;; schema (similar to the use of `s/spec` for wrapping a symbolic predicate).

  ;; for example, to define and store schemas in the registry:

  (s/def ::street string?)
  (s/def ::city string?)
  (s/def ::state string?) ;; simplified
  (s/def ::zip int?) ;; simplified
  (s/def ::addr (s/schema [::street ::city ::state ::zip]))

  (s/def ::id int?)
  (s/def ::first string?)
  (s/def ::last string?)
  (s/def ::user (s/schema [::id ::first ::last ::addr]))


  ;; unions

  ;; in addition to `s/schema`, you can use `s/union` to combine schemas, which
  ;; are either schema names, literal schemas, or schema forms:

  (s/def ::company string?)
  (s/def ::suite string?)
  (s/def ::company-addr (s/union ::addr [::company ::suite]))


  ;; schema gen

  ;; in the case of a schema, all elements are optional, and the generator will
  ;; produce any combination:

  (gen/sample (s/gen ::user) 5)


  ;; unqualified keys

  ;; nested unqualified schemas are supported as well:

  (s/def ::order
    (s/schema {:purchaser  string?
               :due-date   inst?
               :line-items (s/coll-of (s/schema {:item-id  pos-int?
                                                 :quantity nat-int?})
                                      :kind vector?
                                      :min-count 1
                                      :gen-max 3)}))

  (gen/sample (s/gen ::order) 5)


  ;; helper functions

  ;; some helper functions have been added (these are largely analogous to the
  ;; similar functions for symbolic specs and spec objects):

  ;; - `schema*`: takes an explicated symbolic schema and returns a schema object
  ;; - `schema?`: checks whether an object is a schema object

  (s/schema* ::order)
  (s/schema? (s/schema* ::order))

  ;; there is also a new protocol `clojure.alpha.spec.protocols/Schema`.


  ;; select

  ;; `s/select` is a spec op that uses a schema to define the world of possible
  ;; keys and a selection pattern to specify the particular keys (and sub-keys
  ;; for nested maps) are required in a particular context.

  ;; general form: `(s/select schema selection)`

  ;; - schema (required): can be a registered schema name, schema form
  ;;   (like s/union), or literal schema
  ;; - selection (required): vector of
  ;;   - `*`: the wildcard symbol
  ;;   - required keys (qualified or unqualified keywords)
  ;;   - optional subselections (maps of optional keyword to a selection pattern)

  ;; consider the example where you need to know only a user's id and zip code
  ;; for lookup. The selection pattern here requires that both `::id` and
  ;; `::addr` exist, and if `::addr` exists, it must contain `::zip`.

  (s/def ::movie-times-user
    (s/select ::user [::id ::addr {::addr [::zip]}]))

  (s/valid? ::movie-times-user {::id 1 ::addr {::zip 90210}})

  (s/explain ::movie-times-user {})
  (s/explain ::movie-times-user {::id 1})
  (s/explain ::movie-times-user {::id 10 ::addr {}})

  ;; and these selects can also generate examples that conform to the selection:

  (gen/sample (s/gen ::movie-times-user) 5)

  ;; note that in all examples, the user has `::id` and `::addr`, which has a
  ;; `::zip`. Other elements from the schema and nested schema may optionally
  ;; appear.

  ;; another example is for a user placing an order, where the name and full
  ;; nested address is required, but the rest is not. Note this uses the same
  ;; schemas but selects different keys and sub-keys.

  (s/def ::place-order
    (s/select ::user [::first ::last ::addr
                      {::addr [::street ::city ::state ::zip]}]))

  (s/valid? ::place-order {::first "Alex" ::last "Miller"
                           ::addr  {::street "123 Elm" ::city "Springfield"
                                    ::state  "IL"      ::zip  12345}})

  (s/explain ::place-order
             {::first "Alex" ::last "Miller" ::addr {::state "IL"}})

  ;; and it generates as well (notice differences from previous):

  (gen/sample (s/gen ::place-order) 3)


  ;; wildcard

  ;; the wildcard selection pattern indicates that all keys are required:

  (gen/sample (s/gen (s/select ::user [*])) 3)

  ;; and a nested example:

  (gen/sample (s/gen (s/select ::user [* {::addr [*]}])) 3)

  ;; unqualified keys

  ;; as mentioned above in the schema selection, the schema can supply a set of
  ;; unqualified keys and the specs to use with them as well:

  (gen/sample (s/gen (s/select {:a int? :b keyword?} [:a])) 5)

)


;; experiments

(comment

  (s/def ::street string?)
  (s/def ::city string?)
  (s/def ::state string?)
  (s/def ::zip int?)
  (s/def ::addr (s/schema [[::street ::city ::state ::zip]]))

  (s/def ::id int?)
  (s/def ::first string?)
  (s/def ::last string?)
  (s/def ::user (s/schema [[::id ::first ::last ::addr]]))


  (s/def ::user-for-greeting (s/select ::user [:id :first]))
  (s/def ::user-for-greeting-full (s/select ::user [:id :first :last]))
  (s/def ::user-generic (s/select ::user [*]))


  (s/conform ::user-for-greeting {:id 1 :first "Joe"})
  (s/exercise ::user-for-greeting 1)

  (s/conform ::user-for-greeting-full {:id 1 :first "Joe"})
  (s/exercise ::user-for-greeting-full 1)

  (s/conform ::user-generic {:id 1 :first "Joe"})
  (s/exercise ::user-generic 1)

  (s/conform ::user {:id 1 :first "Joe"})
  (s/exercise ::user 10)

)
