(ns hello-spec.generators
  (:require
   [clojure.test.check.generators :as gen]))


;; generating data from specs


;; guide

(comment

  (def five-through-nine (gen/choose 5 9))
  (gen/sample five-through-nine)

  (def languages (gen/elements ["clojure" "haskell" "erlang" "scala" "python"]))
  (gen/sample languages)

  (def int-or-nil (gen/one-of [gen/small-integer (gen/return nil)]))
  (gen/sample int-or-nil)

  (def mostly-ints (gen/frequency [[9 gen/small-integer] [1 (gen/return nil)]]))
  (gen/sample mostly-ints)

  (def even-and-positive (gen/fmap #(* 2 %) gen/nat))
  (gen/sample even-and-positive 20)

  (def powers-of-two (gen/fmap #(int (Math/pow 2 %)) gen/nat))
  (gen/sample powers-of-two)

  (def sorted-vec (gen/fmap (comp vec sort) (gen/vector gen/small-integer)))
  (gen/sample sorted-vec)

  (def int-and-boolean (gen/tuple gen/small-integer gen/boolean))
  (gen/sample int-and-boolean)

  (def anything-but-five (gen/such-that #(not= % 5) gen/small-integer))
  (gen/sample anything-but-five)

  (def vector-and-elem (gen/bind (gen/not-empty (gen/vector gen/small-integer))
                                 #(gen/tuple (gen/return %) (gen/elements %))))
  (gen/sample vector-and-elem)

)


;; cheatsheet

(comment

  (def v [1 2 3])

  (gen/sample (gen/return 10))
  (gen/sample gen/boolean)
  (gen/sample gen/uuid)
  (gen/sample (gen/elements v))
  (gen/sample (gen/shuffle v))
  (gen/sample gen/any)
  (gen/sample gen/any-printable)
  (gen/sample gen/any-equatable)
  (gen/sample gen/any-printable-equatable)
  (gen/sample gen/simple-type)
  (gen/sample gen/simple-type-printable)
  (gen/sample gen/simple-type-equatable)
  (gen/sample gen/simple-type-printable-equatable)

  (gen/sample gen/nat)
  (gen/sample gen/small-integer)
  (gen/sample gen/large-integer)
  (gen/sample gen/size-bounded-bigint)
  (gen/sample gen/double)
  (gen/sample gen/ratio)
  (gen/sample gen/byte)
  (gen/sample (gen/choose 0 1000))

  (gen/sample gen/char)
  (gen/sample gen/char-ascii)
  (gen/sample gen/char-alphanumeric)
  (gen/sample gen/char-alpha)
  (gen/sample gen/string)
  (gen/sample gen/string-ascii)
  (gen/sample gen/string-alphanumeric)
  (gen/sample gen/keyword)
  (gen/sample gen/keyword-ns)
  (gen/sample gen/symbol)
  (gen/sample gen/symbol-ns)

  (gen/sample (gen/tuple gen/nat gen/boolean))
  (gen/sample (gen/hash-map :flag gen/boolean :cost gen/double))

  (gen/sample (gen/vector gen/nat))
  (gen/sample (gen/list gen/nat))
  (gen/sample (gen/set gen/nat))
  (gen/sample (gen/map gen/keyword gen/string))
  (gen/sample gen/bytes)

  (gen/generate (gen/let [n gen/nat]
                  [n (* 2 n)]))
  (gen/generate (gen/fmap #(vector % (* 2 %)) gen/nat))
  (gen/generate (gen/bind
                 gen/nat #(gen/tuple (gen/return %) (gen/return (* 2 %)))))
  (gen/generate (gen/such-that even? gen/nat))
  (gen/generate (gen/not-empty (gen/vector gen/nat)))
  (gen/sample (gen/frequency [[2 gen/boolean] [1 gen/string]]))
  (gen/sample (gen/recursive-gen gen/vector gen/nat))
  (gen/sample (gen/resize 2000 gen/nat))
  (gen/sample (gen/scale inc gen/nat))
  (gen/sample (gen/no-shrink gen/nat))

)
