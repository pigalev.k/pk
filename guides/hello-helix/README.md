# hello-helix

Exploring Helix --- a simple, easy to use library for React development in
ClojureScript.

- https://github.com/lilactown/helix

## Getting started

Start a cljs REPL in terminal, build and load some example code

```
shadow-cljs watch app
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
