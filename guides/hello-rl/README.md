# hello-rl

Exploring concepts of reinforcement learning.

- https://en.wikipedia.org/wiki/Reinforcement_learning

## Getting started

Start a clj REPL in terminal

```
clj
```

or in your editor of choice.

Require namespaces, explore and evaluate the code.
