(ns hello-k8s-api.core
  (:require [kubernetes-api.core :as k8s]))

;; local minikube cluster

(def cluster "https://192.168.59.101:8443")
(def credentials {:ca-cert "/home/nervous/.minikube/ca.crt"
                  :client-cert "/home/nervous/.minikube/profiles/minikube/client.crt"
                  :client-key "/home/nervous/.minikube/profiles/minikube/client.key"})

(def k8s (k8s/client cluster credentials))
