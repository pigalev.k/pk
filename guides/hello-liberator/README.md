# hello-liberator

An example project using
[liberator](https://github.com/clojure-liberator/liberator) - a Clojure library
for building RESTful applications.

## Getting started

Start Ring server

```
clojure -M:start
```

Build uberjar

```
clojure -X:uberjar
```
