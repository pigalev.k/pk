(ns hello-liberator.core
  (:require [liberator.core :refer [resource defresource]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.resource :refer [wrap-resource]]
            [compojure.core :refer [defroutes ANY PUT]]
            [ring.adapter.jetty :as jetty]
            [environ.core :refer [env] :as e])
  (:gen-class))

;; resource definitions

(defresource hello-world
  :available-media-types ["text/plain"]
  :handle-ok "Hello, world!")

(defresource parameter [text]
  :allowed-methods [:post :get]
  :available-media-types ["text/plain"]
  :handle-ok (fn [_] (format "The text is %s" text)))

;; HTTP server stuff: routes, handler

(defroutes app
  (ANY "/" [] hello-world)
  (ANY "/message/:text" [text] (parameter text)))

(def handler
  (-> app
      (wrap-resource "public")
      wrap-params))

;; the app entry point

(defn -main
  "Application entry point."
  [& args]
  (let [port (or (Integer/parseUnsignedInt (env :port)) 3000)]
    (println "Hello liberator!")
    (println (format "Starting HTTP server at http://localhost:%s" port))
    (jetty/run-jetty handler {:port  port
                              :join? true})))
